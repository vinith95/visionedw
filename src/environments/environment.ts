// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const environment = {
  production: false,
  getNotification: "edwApiConfig/notification",
  getNotificationDetails: "edwApiConfig/getNotificationDetails",
  getNotificationDetailsView: "edwApiConfig/getNotificationDetailsView",
  
  updateNotification: "updateNotification",
  get_visionMenu_page_value: "visionMenu/pageLoadValues",
  get_menuGroup_list: "visionMenu/getAllVisionMenuGroup",
  add_menuGroup: "visionMenu/addMenu",
  update_menuGroup: "visionMenu/modifyMenu",
  delete_menu: "visionMenu/deleteMenu",
  reject_menu: "visionMenu/rejectMenu",
  approve_menu: "visionMenu/approveMenu",
  get_menuGroup_grid_list: "visionMenu/getAllVisionMenu ",
  saveAppTheme: "commonCompApi/saveAppTheme",
  edwApiConfig_updateNotification:"edwApiConfig/updateNotification" ,

  get_product_matrix_mapping_Page_Value: "productMatrix/pageLoadValues",
  get_Branch_List: "magnifierControls/magnifierQuery",
  get_product_matrix_mapping_list: "productMatrix/getAllQueryResults",
  add_product_matrix_mapping: "productMatrix/addMatrix",
  update_product_matrix_mapping: "productMatrix/modifyMatrix",

  get_segmentation_Page_Value: "segmentConfig/pageLoadValues",
  get_segmentation_records: "segmentConfig/getAllQueryResults",
  add_segmentation_records: "segmentConfig/addSegmentRuleConfig",
  update_segmentation_records: "segmentConfig/modifySegmentRuleConfig",
  delete_segmentation_records: "segmentConfig/deleteSegmentRuleConfig",

  get_segmentation_list: "customerSegmentMapping/getAllQueryResults",
  get_process: "customerSegmentMapping/startSegmentProcess ",
  get_mapping_deatail: "customerSegmentMapping/getAllQueryResultsDetail",
  save_process: "customerSegmentMapping/addSegmentMapping ",
  reject_mapping: "customerSegmentMapping/approveSegmentMapping",
  authorize_mapping: "customerSegmentMapping/approveDetailSegmentMapping",

  get_global_cif_page_value: "cifMasters/getAllQueryResults",
  get_global_cif_customer_details: "cifMasters/getCustomerDetails",
  get_global_cif_account_officer_details: "cifMasters/getAccountDetails",
  get_cif_id: "cifMasters/getGcifId",
  get_non_group_cif: "cifMasters/getNonGroupCif",
  save_non_cif: "cifMasters/saveGcifMaster",
  edit_non_cif: "cifMasters/modifyGroupCif",

  get_budget_configuration_data: "branchRMTarget/getAllQueryResults",
  get_page_load_value: "branchRMTarget/pageLoadValues",
  add_target: "branchRMTarget/addTarget",
  get_target_deatails: "branchRMTarget/getQueryResultsDetail",
  modify_target: "branchRMTarget/modifyTarget",
  delete_target: "branchRMTarget/deleteTarget",

  get__confidence_level_page_load_value: "dealConfig/pageLoadValues",
  confidence_level_data: "dealConfig/getAllQueryResults",
  save_confidence_level_data: "dealConfig/addDealConfig",
  modify_confidence_level_data: "dealConfig/modifyDealConfig",
  edit_confidence_level_data: "dealConfig/getQueryResultsDetail",
  delete_confidence_level_data: "dealConfig/deleteDealConfig ",

  get_leadEntry_page_load_value: "leadEntry/pageLoadValues",
  generate_leadId: "leadEntry/getNewLeadId",
  leadEntry_data: "leadEntry/getAllQueryResults",
  add_leadEntry: "leadEntry/addLeadEntry",
  modify_leadEntry: "leadEntry/modifyLeadEntry",
  query_results_leadEntry: "leadEntry/getQueryResultsDetail",
  delete_leadEntry: "leadEntry/deleteLeadEntry",
  update_leadEntry: "leadEntry/updateLeadStageLevel",
  uploadLeadFile: "leadEntry/uploadFiles",
  download_leadEntry: "leadEntry/downloadFile",

  get_dealId: "dealEntry/getNewDealId",
  get_dealEntry_pageLoadValues: "dealEntry/pageLoadValues",
  get_dealEntry_QueryList: "dealEntry/getAllQueryResults",
  uploadReport: "dealEntry/uploadCallReport",
  addDealEntry: "dealEntry/addDealEntry",
  updateDealEntry: "dealEntry/modifyDealEntry",
  deleteDealEntry: "dealEntry/deleteDealEntry",
  updateDealStageLevel: "dealEntry/updateDealStageLevel",
  addCallLogs: "dealEntry/addCallLogs",
  getAoCallHistory: "dealEntry/getAoCallHistory",
  download_dealEntry: "dealEntry/downloadCallReport",

  getReport: "mdmreports/getReportData",
  getReportPrompts: "mdmreports/getReportPrompts",
  getReportPrompts1: "mdmreports/getGroupReportDetail",

  getRMReport: "reports/getReportData",
  getRMreportExcelExport: "reports/reportExcelExport",
  getRMreportPdfExport: "reports/reportPdfExport",

  multiplePdfExport: "reports/MultiPdf",
  multipleExcelExport: "reports/MultiExcel",
  pdfExport: "mdmreports/reportPdfExport",
  excelExport: "mdmreports/reportExcelExport",
  groupReportExport: "mdmreports/GroupReportExport",
  getDashboardDetails: "dashboards/getMdmDashboardOnLoad",
  getMdmDashboardTabData: "dashboards/getMdmDashboardTabData",
  getDrillDownData: "dashboards/getDashboardDrillDownData",
  getTileData: "dashboards/getDashboardResultData",
  getDashboardGroupList: "dashboards/getDashboardList",

  aoLeaveMgmtPageLoadValues: "aoLeaveMgmt/pageLoadValues",
  aoLeaveManagementList: "aoLeaveMgmt/getAllQueryResults",
  getNewLeaveSequence: "aoLeaveMgmt/getNewLeaveSequence",
  saveLeaveManagement: "aoLeaveMgmt/addAoLeaveMgmt",
  edit: "aoLeaveMgmt/getQueryResultsDetail",
  updateLeaveManagement: "aoLeaveMgmt/modifyAoLeaveMgmt",
  deleteLeaveMamagement: "aoLeaveMgmt/deleteAoLeaveMgmt",
  rejectAoLeaveMgmt: "aoLeaveMgmt/rejectAoLeaveMgmt",
  approveAoLeaveMgmt: "aoLeaveMgmt/approveAoLeaveMgmt",

  profile_page_load_values: "adminProfileSetup/pageLoadValues",
  profile_get_all_values: "adminProfileSetup/getAllQueryResults",
  profile_add: "adminProfileSetup/addProfileRa ",
  profile_modify: "adminProfileSetup/modifyProfileRa ",
  profile_delete: "adminProfileSetup/deleteProfileRa",

  getReportList: "reports/getReportMaster",
  getReportFilterList: "reports/getReportFilter",
  getReportChildDependentFilter: "reports/getChildDependentFilter",
  getTreeDataPromptList: "reports/getTreePromptData",
  getInterActive: "reports/getInteractiveReportsDetail",
  getCBReportExport: "reports/getCBreport",
  getReportChildFilter: "reports/getReportChildDependentFilter",
  getDashboardOnLoad: "dashboards/getDashboardOnLoad",
  reportExportToCsv: "reports/reportExportToCsv",
  get_dataConnector_shareDet: "dataSourceConnector/getLevelOfDisplay",
  post_saveDataSource: "dataSourceConnector/saveLevelOfDisplay",
  get_designAnalysis_Render: "designAnalysis/loadReportMetaData",
  delete_designAnalysis: "designAnalysis/deleteDesignQuery",
  get_designAnalysis_ExecuteSaved: "designAnalysis/executeSaved",
  get_designAnalysis_magData: "designAnalysis/getMagnefierData",
  get_userGroupProfile: "levelOfDisplay/userGroupProfile",
  get_designAnalysis_Execute: "designAnalysis/execute",
  get_designAnalysis_Download: "designAnalysisExport/download",
  get_designAnalysis_Hash: "designAnalysis/getHashVariableList",
  get_designAnalysis_Save: "designAnalysis/save",
  get_all_designQuery: "designAnalysis/getAll",
  designAnalysis_search: "designAnalysis/smartSearchFilter",
  get_listOf_catalog: "designAnalysis/listCatalog",
  getDynamicDate: "designAnalysis/generateDynamicDate",
  get_designAnalysis_Load: "designAnalysis/getReportListCatalogBased",
  get_data_design_analysis_par: "designAnalysis/getQueryTree",
  get_data_design_analysis_child: "designAnalysis/getQueryTreeColumns",
  get_designAnalysis_DownloadSaved: "designAnalysisExport/downloadSavedReport",
  getWidgetMaster: "reports/getWidgetMaster",
  saveUserWidget: "reports/saveUserWidget",
  deleteUserWidget: "reports/deleteUserWidget",
  widgetExport: "reports/widgetExport",
  burstFlagpageLoadValues: "burstFlag/pageLoadValues",
  burstFlagQueryResults: "burstFlag/getAllQueryResults",
  getSequenceNumber: "burstFlag/getSequenceNumber",
  addBurstId: "burstFlag/addBurstId",
  getQueryDetailsburst: "burstFlag/getQueryDetails",
  modifyBurstId: "burstFlag/modifyBurstId",
  rejectBurstId: "burstFlag/rejectBurstId",
  approveBurstId: "burstFlag/approveBurstId",
  reviewBurstId: "burstFlag/review",
  deleteBurstId: "burstFlag/deleteBurstId",
  bulkRejectBurstId: "burstFlag/bulkReject",
  bulkApproveBurstId: "burstFlag/bulkApprove",
  saveUserSettingforReport: "reports/saveUserSettingforReport",
  dashboardExcelDataforGrid: "dashboards/dashboardExcelDataforGrid",
  dashboardPdfExportforGrid: "dashboards/dashboardPdfExportforGrid",

  //ermodule

  getAllTree: "edwERSummary/pageLoadValues",
  gettreetables: "edwERSummary/getTableDetails",

  getTablecolumns: "edwERSummary/getTableColumns",
  updateDisplayColumn: "edwERSummary/updateDisplayColumn",
  addModifyERDiagram: "edwERSummary/addModifyERDiagram",
  exportDataToExcel: "edwERSummary/exportDataToExcel",
  exportDataToHtml: "edwERSummary/exportDataToHtml",
  exportDataToPdf: "edwERSummary/exportDataToPdf",
  exportTableScript: "edwERSummary/exportTableScript",
  getErDiagramByModule: "edwERSummary/getErDiagramByModule",
  updateRelations: "edwERSummary/updateRelations",
  reloadMissingObjects : "edwERSummary/reloadMissingObjects",
  getRelationSuggestions : "edwERSummary/getRelationSuggestions",
  edwObjinsertRelationsData: "edwObj/insertRelationsData",
  addMissionObjects: "edwObj/addMissionObjects",
  addMissionMultipleObjects: "edwObj/addMissionMultipleObjects",
  edwERSummary_getColumnDataForNewTables : "edwERSummary/getColumnDataForNewTables",
  edwERSummary_updateErSummaryXYAxis: "edwERSummary/updateErSummaryXYAxis",
  
  //policy-setup-apis start

  policy_Setup_PageLoadValues: "edwPolConfig/pageLoadValues",
  policy_Setup_getAllQueryResults: "edwPolConfig/getAllQueryResults",
  policy_Setup_GetQueryDetails: "edwPolConfig/getQueryDetails",
  policy_Setup_AddEdwPolConfig: "edwPolConfig/addEdwPolConfig",
  policy_Setup_ModifyEdwPolConfig: "edwPolConfig/modifyEdwPolConfig",
  policy_Setup_DeleteEdwPolConfig: "edwPolConfig/deleteEdwPolConfig",
  policy_Setup_ApproveEdwPolConfig: "edwPolConfig/approveEdwPolConfig",
  policy_Setup_RejectEdwPolConfig: "edwPolConfig/rejectEdwPolConfig",
  policy_Setup_BulkApproveEdwPolConfig: "edwPolConfig/bulkApproveEdwPolConfig",
  policy_Setup_BulkRejectEdwPolConfig: "edwPolConfig/bulkRejectEdwPolConfig",
  policy_Setup_ReviewEdwPolConfig: "edwPolConfig/reviewEdwPolConfig",
  policy_Setup_childReviewEdwPolConfig: "edwPolConfig/childReviewEdwPolConfig",
  policy_cornStatus: "edwPolConfig/startOrStopPolicyCron",
  columnsForPolicyCondition:"edwPolConfig/getTableColumnsForPolicy",
  edwPolicy_setupConfig_validateArchQuery:"edwPolConfig/validateArchQuery",
                            

  //policy-setup-apis ended

  pageLoadValues: "visionUserSetup/pageLoadValues",
  getAllQueryResults: "visionUserSetup/getAllQueryResults",
  getQueryDetails: "visionUserSetup/getQueryDetails",
  addVisionUserSetup: "visionUserSetup/addVisionUserSetup",
  modifyVisionUserSetup: "visionUserSetup/modifyVisionUserSetup",
  deleteVisionUserSetup: "visionUserSetup/deleteVisionUserSetup",
  approveVisionUserSetup: "visionUserSetup/approveVisionUserSetup",
  rejectVisionUserSetup: "visionUserSetup/rejectVisionUserSetup",
  userSetupUnLock: "visionUserSetup/userSetupUnLock",
  unLockUser: "visionUserSetup/unLockUser",
  uploadUserPhoto: "visionUserSetup/uploadUserPhoto",
  downloadUserPhoto: "visionUserSetup/downloadUserPhoto",

  // Event Configutation
  event_pageLoadValues: "edwEvtConfig/pageLoadValues",
  event_getAllQueryResults: "edwEvtConfig/getAllQueryResults",
  event_getQueryDetails: "edwEvtConfig/getQueryDetails",
  event_addEdwEvtConfig: "edwEvtConfig/addEdwEvtConfig",
  event_modifyEdwEvtConfig: "edwEvtConfig/modifyEdwEvtConfig",
  event_deleteEdwEvtConfig: "edwEvtConfig/deleteEdwEvtConfig",
  eventApi_getQueryDetails: "edwApiConfig/getQueryDetails",
  eventApi_addEdwEvtConfig: "edwApiConfig/addEdwApiConfig",
  eventApi_modifyEdwEvtConfig: "edwApiConfig/modifyEdwApiConfig",
  eventApi_getMailIds: "edwApiConfig/getVisionUsers",
  eventApi_getCornStatus: "edwEvtConfig/startOrStopEventCron",
  prdCronControl_eventApi_getCornStatus: "prdCronControl/startOrStopAllCronStatus",
  eventApi_testSqlQuery: "edwEvtConfig/testSqlQuery",
 

  //Audit Trail
  edwAudit_PageLoad: "edwAuditTrail/pageLoadValues",
  edw_getPolicyDeatils: "edwAuditTrail/getQueryPolicyDetails",
  edw_getEventDeatils: "edwAuditTrail/getQueryEventDetatils",
  

  edw_getPolicy: "edwAuditTrail/getQueryPolicies",
  edw_getEvent: "edwAuditTrail/getQueryEvents",

  //metdata
  edwTable_pageLoadValues: "edwObj/pageLoadValues",
  edwTable_getAllQueryResults: "edwObj/getAllQueryResults",
  edwTable_getQueryDetails: "edwObj/getQueryDetails",
  edwTable_refreshHistoryAudit: "edwObj/refreshHistoryAudit",
  edwTable_addEdwObj: "edwObj/addEdwObj",
  edwTable_modifyEdwObj: "edwObj/modifyEdwObj", // Table Add/Modify

  edwTable_getColumnDetails: "edwObj/getColumnDetails", // Get Table Columns
  edwTable_modifyColumnData: "edwObj/modifyColumnData", // Columns Add/Modify

  edwTable_getRelationsDetails: "edwObj/getRelationsDetails", // Get Table Relations
  edwTable_modifyRelationsData: "edwObj/modifyRelationsData", // Relations Add/Modify

  edwTable_getIndexDetails: "edwObj/getIndexDetails", // Get Table Index
  edwTable_modifyIndexData: "edwObj/modifyIndexData", // Index Add/Modify
  edwObj_getColumnByTable: "edwObj/getColumnByTable",

  edwTable_deleteRelationsData: "edwObj/deleteRelationsData", // Relations Delete

  //corn

  cron_getAllQueryResults: "prdCronControl/getAllQueryResults",
  cron_startOrStopEventCron: "prdCronControl/startOrStopCronStatus",

  // Business Glossary
  businessGlossary_pageLoadValues: "edwBusGlossary/pageLoadValues",
  businessGlossary_getAllQueryResults: "edwBusGlossary/getAllQueryResults",
  businessGlossary_getQueryDetails: "edwBusGlossary/getQueryDetails",

  businessGlossary_addEdwEvtConfig: "edwBusGlossary/addEdwBusGlossary",
  businessGlossary_modifyEdwEvtConfig: "edwBusGlossary/modifyEdwBusGlossary",
  businessGlossary_deleteEdwEvtConfig: "edwBusGlossary/deleteEdwBusGlossary",
  businessGlossary_getTableObjects: "edwBusGlossary/getTableObjects",
  editBusGlossary_getQueryDetails: "edwBusGlossary/getQueryDetails",

  // Business Gloasary New
  businessGlossaryNew_pageLoadValues: "edwBusGlossaryNew/pageLoadValues",
  businessGlossaryNew_getAllQueryResults: "edwBusGlossaryNew/getAllQueryResults",
  businessGlossaryNew_getUsedTablesByColumn: "edwBusGlossaryNew/getUsedTablesByColumn",
  businessGlossaryNew_getUsedTablesByRepository: "edwBusGlossaryNew/getTablesByRepository",
  edwBusGlossaryNew_modifyColumnData:"edwBusGlossaryNew/modifyColumnData",

  accountBucketsPageload: 'accountBuckets/pageLoadValues',
  accountBucketsAllQueryResults: 'accountBuckets/getAllQueryResults',
  accountBucketsQueryDetails: 'accountBuckets/getQueryDetails',

  bulkApproveAccountBuckets: 'accountBuckets/bulkApproveAccountBuckets',
  bulkRejectAccountBuckets: 'accountBuckets/bulkRejectAccountBuckets',
  deleteAccountBuckets: 'accountBuckets/deleteAccountBuckets',
  modifyAccountBuckets: 'accountBuckets/modifyAccountBuckets',
  addAccountBuckets: 'accountBuckets/addAccountBuckets',
  reviewAccountBuckets: 'accountBuckets/reviewAccountBuckets',

  updateHomeDashboard: "raProfileSetup/updateHomeDashboard",
  public_key:
    "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC3lNQKgVZJaTPu7DwlUcKLyOVr\n" +
    "fmHCpbMhf0b555sQodNqWaNJTO2dDzKrpuL5C829OOqIwrPMEAlQOjzUraJDa18/\n" +
    "lIrkY5jUCf18Ie6eTRXgBqzu84ZInDI2EqKJiFLR+ciIMAdPSRDXCsrtWfEuNUIV\n" +
    "jPFjU48MhnrBeIKJRQIDAQAB",
  private_key:
    "MIICXQIBAAKBgQC3lNQKgVZJaTPu7DwlUcKLyOVrfmHCpbMhf0b555sQodNqWaNJTO2dDzKrpuL5C829OOqIwrPMEAlQOjzUraJDa18/lIrkY5jUCf18Ie6eTRXgBqzu\n" +
    "84ZInDI2EqKJiFLR+ciIMAdPSRDXCsrtWfEuNUIVjPFjU48MhnrBeIKJRQIDAQAB\n" +
    "AoGBAIA/4MJWCMnQTQ+kyX/o1liBSIUIDZPF3paQLC15t8KuytuJM6GJtJX8gBGa\n" +
    "zQXfUSPYF0DjhGgS6+64QLbzdl8kIUtYmMNKgEtQp25BhNg0GsSLt8ZBpNUowhV0\n" +
    "vpSc/xbxTCgAMX1HDQ911/DqQ0Jcx5yFBSkZZJOoLxEa0FoBAkEA34OL+usZW556\n" +
    "lMRTqTOaGFpHm0aNKyhfNKd64PR1tvrHhv0lvI4yBnZISnNjrc1DT1oYUQB6ncTJ\n" +
    "0l8MW4TVsQJBANJDe3N9X2Njv+7g3RfebzL4vXdGQOWaqghyRd6AQ7Sa+3YK5X2p\n" +
    "4tZibXRkBl9P/0uo9HDZb3NJcxOj3SM+zdUCQEeujoQIRfemGbYASLxo+jB5M6HO\n" +
    "vJNsvSvL0pK2k9H9F2RpZ2LTT6PW51RJMVyaCc43HLlNqAVVCUzYgsQPE+ECQDFK\n" +
    "6LIWfjpKtEBILwzddKw6bBB+Q9CQBbSdNYwH/ddlTfZKb6qaHTetM4PJv0Sw+GTA\n" +
    "Yy3hi6M3YOcFz13n51ECQQDGmpuPTKKyIyiyVf3A324qb01ogIeTPs7Z6uAcY+gA\n" +
    "4s/goOlfHWo46lVdHBdUgCtHpSko5vKB0NW1EO3KXLMh",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
