export const smallSvg = `<svg viewBox="0 0 512 512" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
 <g>
  <title>background</title>
  <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1"/>
 </g>
 <g>
 <!--  <title>Layer 1</title> -->
  <rect stroke="#000000" rx="12" id="svg_21" height="486.428203" width="486.428212" y="13.455557" x="13.500176" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_23" y2="82.026926" x2="497.993753" y1="82.026926" x1="21.357306" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_24" y2="232.026818" x2="493.708042" y1="232.026818" x1="17.071594" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_25" y2="367.026711" x2="492.279472" y1="367.026711" x1="15.643024" stroke-width="23" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 403.1211853027344,290.9589538574219) " stroke-linecap="null" stroke-linejoin="null" id="svg_26" y2="290.958955" x2="608.221334" y1="290.958955" x1="198.02105" stroke-width="23" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 305.9811706542969,291.6690979003906) " stroke-linecap="null" stroke-linejoin="null" id="svg_27" y2="291.669097" x2="511.799601" y1="291.669097" x1="100.162636" stroke-width="23" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 107.4098358154297,291.6690979003906) " stroke-linecap="null" stroke-linejoin="null" id="svg_28" y2="291.669097" x2="321.08547" y1="291.669097" x1="-90.551495" stroke-width="23" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 205.98129272460938,290.2405090332031) " stroke-linecap="null" stroke-linejoin="null" id="svg_29" y2="290.240527" x2="411.799682" y1="290.240527" x1="0.162717" stroke-width="23" fill="none"/>
 </g>
</svg>`;