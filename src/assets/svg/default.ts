export const defaultSvg = `<svg viewBox="0 0 512 512" width="20" height="20" xmlns="http://www.w3.org/2000/svg">

 <g>
  <title>background</title>
  <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1"/>
 </g>
 <g>
 <!--  <title>Layer 1</title> -->
  <rect stroke="#000000" rx="12" id="svg_21" height="486.428203" width="486.428212" y="13.455557" x="13.500176" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_23" y2="97.026914" x2="498.708038" y1="97.026914" x1="22.071591" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_24" y2="232.026818" x2="493.708042" y1="232.026818" x1="17.071594" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_25" y2="367.026711" x2="492.279472" y1="367.026711" x1="15.643024" stroke-width="23" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 345.9783630371093,290.9589538574218) " stroke-linecap="null" stroke-linejoin="null" id="svg_26" y2="290.958955" x2="551.078524" y1="290.958955" x1="140.878239" stroke-width="23" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 166.69554138183597,291.6690979003906) " stroke-linecap="null" stroke-linejoin="null" id="svg_22" y2="291.669097" x2="372.514" y1="291.669097" x1="-39.122965" stroke-width="23" fill="none"/>
 </g>
</svg>`;