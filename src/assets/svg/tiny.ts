export const tinySvg = `<svg viewBox="0 0 512 512" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
 <g>
  <title>background</title>
  <rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1"/>
 </g>
 <g>
 <!--  <title>Layer 1</title> -->
  <rect stroke="#000000" rx="12" id="svg_21" height="486.428203" width="486.428212" y="13.455557" x="13.500176" stroke-width="23" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_23" y2="82.741212" x2="497.993753" y1="82.741212" x1="21.357306" stroke-width="10" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_24" y2="429.584614" x2="494.007228" y1="429.584614" x1="17.370781" stroke-width="10" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_25" y2="367.026711" x2="492.279472" y1="367.026711" x1="15.643024" stroke-width="10" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 401.99182128906244,292.0883483886718) " stroke-linecap="null" stroke-linejoin="null" id="svg_26" y2="292.088339" x2="607.09195" y1="292.088339" x1="196.891666" stroke-width="10" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 305.5660095214843,292.79849243164057) " stroke-linecap="null" stroke-linejoin="null" id="svg_27" y2="292.798482" x2="511.384502" y1="292.798482" x1="99.747537" stroke-width="10" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 114.85189056396482,292.7984924316406) " stroke-linecap="null" stroke-linejoin="null" id="svg_28" y2="292.798482" x2="320.670371" y1="292.798482" x1="-90.966594" stroke-width="10" fill="none"/>
  <line stroke="#000000" transform="rotate(-89.78325653076172 205.56610107421875,291.3699035644531) " stroke-linecap="null" stroke-linejoin="null" id="svg_29" y2="291.369911" x2="411.384583" y1="291.369911" x1="-0.252382" stroke-width="10" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_30" y2="233.156202" x2="494.007228" y1="233.156202" x1="17.370781" stroke-width="10" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_31" y2="157.441977" x2="494.007228" y1="157.441977" x1="17.370781" stroke-width="10" fill="none"/>
  <line stroke="#000000" stroke-linecap="null" stroke-linejoin="null" id="svg_32" y2="300.299004" x2="494.007228" y1="300.299004" x1="17.370781" stroke-width="10" fill="none"/>
 </g>
</svg>`;