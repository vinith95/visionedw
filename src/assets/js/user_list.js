$(document).ready(function () {
    //$('#myElement').simplebar();
    // contact form animations
    $('#contact').click(function () {
        $('#contactForm').fadeToggle();
    })
    $(document).mouseup(function (e) {
        var container = $("#contactForm");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.fadeOut();
        }
    });
    const userGroupElem = document.getElementById('userGroup');
    const userProfileElem = document.getElementById('userProfile');
    const option = document.createElement('option');
    const userGroupData = ["- Select -", "System Administrator", "Audit", "Branch", "Business Controllers", "Central Marketing Unit", "Credit", "CSP", "Finance Team", "Human resource", "Information security", "Operations Department", "Project department", "Risk Management", "Settlements", "Vision"];
    const userProfileData = ["- Select -", "Admin User", "Analyst", "CFOs", "Finance Team", "Maker", "Management", "MIS Team", "Report analysis", "Supervisor", "Trainee", "Verifier"];
    userProfileData.forEach((item, index) => {
        $('#userProfile').append(`<option value=${item}>${item}</option>`);
    });
});

function createNode(element) {
    return document.createElement(element);
}

function append(parent, el) {
    return parent.appendChild(el);
}
const table = document.getElementById('user-account');

const addRecords = document.getElementById('addRecords');
addRecords.addEventListener('click', function () {
    document.getElementById('lastNode').style.display = 'table-row';
});