 function getScript(url, callback) {
   var script = document.createElement('script');
   script.type = 'text/javascript';
   script.src = url;

   script.onreadystatechange = callback;
   script.onload = callback;

   document.getElementsByTagName('head')[0].appendChild(script);
} 
var db_source=[
            {
                "tableName":"Customer_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Customer_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "PERM_ADDRESS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "137",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "COMM_ADDRESS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "137",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "WORK_ADDRESS_1",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "HOME_PHONE_1",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "BUSINESS_PHONE_1",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PERSONAL_EMAIL_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "50",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "DATE_OF_BIRTH",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "CIF_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Transaction_Detail",
                "tableAliasName":"",
                "tableNameDescription":"Description for Transaction_Detail",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "OFFICE_ACCOUNT",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "19",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_CCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "3",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "VALUE_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "9",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "MIS_CURRENCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "3",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Staff_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Staff_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "STAFF_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "EMP_TITLE",
                                "attribute_type": "NVARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "STAFF_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "EMP_PAYROLL_NO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "EMP_GENDER",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "EMP_JOIN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "EMP_DEPT_ID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "EMPLOYMENT_TYPE",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "EMP_BIRTH_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "EMP_MARITALSTATUS_ID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "EMP_NATIONALITY_ID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "ICARDNO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "DESIG_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "SUBDEPARTMENT_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":15,
                                "attribute_alias":"",
                                "attribute_name": "REPORTING_LINE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                        ]
            },
                        {
                "tableName":"Transaction_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Transaction_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "VALUE_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "TRANSACTION_CHANNEL",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "TRANSACTION_CODE",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PART_TRAN_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_PARTICULAR",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "55",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_RMKS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "55",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "ENTRY_USER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Customer_Info",
                "tableAliasName":"",
                "tableNameDescription":"Description for Customer_Info",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "80",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ACRONYM",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "25",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "CB_ORG_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "CB_ECONOMIC_ACT_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "NAT_ID_CARD_NUM",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "16",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PRIMARY_CID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "PARENT_CID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "ULTIMATE_PARENT",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_SEX",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_OPEN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "CUST_TYPE_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "SEGMENTATION_CLASS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "SUBSEGMENT",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":15,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":16,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Contracts_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Contracts_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "16",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "SCHEDULE_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "PAYMENT_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "REPAYMENT_FREQUENCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "2",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "EMI_AMOUNT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "DUE_AMOUNT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PRINCIPAL_AMOUNT_PAID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "ADD_INT_PAID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "INT_AMOUNT_PAID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "PRODUCT",
                                "attribute_type": "CHAR",
                                "attribute_precision": "7",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Atm_Transaction",
                "tableAliasName":"",
                "tableNameDescription":"Description for Atm_Transaction",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "BUSINESS_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "CARD_NO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "21",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "ACCOUNT_NO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "21",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "MSGTIME",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "MSGAMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "23",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "TRACE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "TERMINAL",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "TERMINALNAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "100",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "CURRENCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "3",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Contracts_Info",
                "tableAliasName":"",
                "tableNameDescription":"Description for Contracts_Info",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "TOD_SEQUENCE_NUMBER",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "5",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "LIMIT_SANC_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "LIMIT_EXP_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "SANCTION_LIMIT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_TYPE",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_CATGR",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_REGLR_IND",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_REGLR_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "REGULARISED_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "AVAILED_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "ENTITY_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "SCHM_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "5",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "SCHM_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":15,
                                "attribute_alias":"",
                                "attribute_name": "FEED_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":16,
                                "attribute_alias":"",
                                "attribute_name": "PRODUCT",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "30",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":17,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":18,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },

        ];

var cleaner_source=[
            {
                "tableName":"Customer_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Customer_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "PERM_ADDRESS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "137",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "COMM_ADDRESS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "137",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "WORK_ADDRESS_1",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "HOME_PHONE_1",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "BUSINESS_PHONE_1",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PERSONAL_EMAIL_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "50",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "DATE_OF_BIRTH",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "CIF_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
             {
                "tableName":"Transaction_Detail",
                "tableAliasName":"",
                "tableNameDescription":"Description for Transaction_Detail",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "OFFICE_ACCOUNT",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "19",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_CCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "3",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "VALUE_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "9",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "MIS_CURRENCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "3",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Staff_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Staff_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "STAFF_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "EMP_TITLE",
                                "attribute_type": "NVARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "STAFF_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "EMP_PAYROLL_NO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "EMP_GENDER",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "EMP_JOIN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "EMP_DEPT_ID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "EMPLOYMENT_TYPE",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "EMP_BIRTH_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "EMP_MARITALSTATUS_ID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "EMP_NATIONALITY_ID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "ICARDNO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "DESIG_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "SUBDEPARTMENT_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "45",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":15,
                                "attribute_alias":"",
                                "attribute_name": "REPORTING_LINE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Transaction_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Transaction_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "VALUE_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "TRANSACTION_CHANNEL",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "TRANSACTION_CODE",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PART_TRAN_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_PARTICULAR",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "55",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "TRAN_RMKS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "55",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "ENTRY_USER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Customer_Info",
                "tableAliasName":"",
                "tableNameDescription":"Description for Customer_Info",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_NAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "80",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_ACRONYM",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "25",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "CB_ORG_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "CB_ECONOMIC_ACT_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "NAT_ID_CARD_NUM",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "16",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PRIMARY_CID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "PARENT_CID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "ULTIMATE_PARENT",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "14",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_SEX",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "CUSTOMER_OPEN_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "CUST_TYPE_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "SEGMENTATION_CLASS",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "SUBSEGMENT",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":15,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":16,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Contracts_Master",
                "tableAliasName":"",
                "tableNameDescription":"Description for Contracts_Master",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "16",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "SCHEDULE_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "PAYMENT_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "REPAYMENT_FREQUENCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "2",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "EMI_AMOUNT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "DUE_AMOUNT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "PRINCIPAL_AMOUNT_PAID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "ADD_INT_PAID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "INT_AMOUNT_PAID",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "PRODUCT",
                                "attribute_type": "CHAR",
                                "attribute_precision": "7",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Atm_Transaction",
                "tableAliasName":"",
                "tableNameDescription":"Description for Atm_Transaction",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "BUSINESS_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "CARD_NO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "21",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "ACCOUNT_NO",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "21",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "MSGTIME",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "MSGAMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "23",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "TRACE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "TERMINAL",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "15",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "TERMINALNAME",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "100",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "CURRENCY",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "3",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            },
            {
                "tableName":"Contracts_Info",
                "tableAliasName":"",
                "tableNameDescription":"Description for Contracts_Info",
                "columns":[
                            {
                                "attribute_id":1,
                                "attribute_alias":"",
                                "attribute_name": "CONTRACT_ID",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "20",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":2,
                                "attribute_alias":"",
                                "attribute_name": "TOD_SEQUENCE_NUMBER",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "5",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":3,
                                "attribute_alias":"",
                                "attribute_name": "LIMIT_SANC_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":4,
                                "attribute_alias":"",
                                "attribute_name": "LIMIT_EXP_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "SANCTION_LIMIT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":5,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_TYPE",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":6,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_CATGR",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":7,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_REGLR_IND",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "1",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":8,
                                "attribute_alias":"",
                                "attribute_name": "DISCRET_ADVN_REGLR_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":9,
                                "attribute_alias":"",
                                "attribute_name": "REGULARISED_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":10,
                                "attribute_alias":"",
                                "attribute_name": "AVAILED_AMT",
                                "attribute_type": "NUMBER",
                                "attribute_precision": "21",
                                "attribute_scale": "5"
                            },
                            {
                                "attribute_id":11,
                                "attribute_alias":"",
                                "attribute_name": "ENTITY_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":12,
                                "attribute_alias":"",
                                "attribute_name": "SCHM_TYPE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "5",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":13,
                                "attribute_alias":"",
                                "attribute_name": "SCHM_CODE",
                                "attribute_type": "VARCHAR2",
                                "attribute_precision": "10",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":14,
                                "attribute_alias":"",
                                "attribute_name": "FEED_DATE",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":15,
                                "attribute_alias":"",
                                "attribute_name": "PRODUCT",
                                "attribute_type": "VARCHAR2 ",
                                "attribute_precision": "30",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":16,
                                "attribute_alias":"",
                                "attribute_name": "DATE_LAST_MODIFIED",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                            {
                                "attribute_id":17,
                                "attribute_alias":"",
                                "attribute_name": "DATE_CREATION",
                                "attribute_type": "DATE",
                                "attribute_precision": "",
                                "attribute_scale": ""
                            },
                        ]
            }
        ];
var exp_fun_list= ["ANY","AVG","COUNT","FIRST","LAST","MAX (Date)","MAX (Number)","MEDIAN","MIN (Date)","MIN (Number)",
"PERCENTILE",
"STDDEV",
"SUM",
"VARIANCE",
"ASCII",
"CHR",
"CONCAT",
"INITCAP",
"INSTR",
"LENGTH",
"LOWER",
"LPAD",
"LTRIM",
"REPLACECHR",
"REPLACESTR",
"RPAD",
"RTRIM",
"SUBSTR",
"UPPER",
"DIACRITIC",
"NONLETTER",
"ARRAY",
"SIZE",
"TO_BIGINT",
"TO_CHAR(Number)",
"TO_DATE",
"TO_INTEGER",
"GREATEST",
"IN",
"IS_DATE",
"IS_NUMBER",
"IS_SPACES",
"ISNULL",
"LEAST",
"REG_EXTRACT",
"REG_MATCH",
"REG_REPLACE",
"SQL_LIKE",
"TO_CHAR",
"TO_DECIMAL",
"TO_FLOAT",
"ADD_TO_DATE",
"DATE_COMPARE",
"DATE_DIFF",
"GET_DATE_PART",
"LAST_DAY",
"MAKE_DATE_TIME",
"MAX",
"MIN",
"ROUND(Date)",
"SET_DATE_PART",
"SYSTIMESTAMP",
"TO_CHAR(Date)",
"TRUNC(Date)",
"ABS",
"CEIL",
"CUME",
"FLOOR",
"MOD",
"MOVINGAVG",
"MOVINGSUM",
"POWER",
"RAND",
"ROUND",
"SIGN",
"SQRT",
"TRUNC",
"DECODE",
"IF",
"CHOOSE",
"REVERSE"
    ];