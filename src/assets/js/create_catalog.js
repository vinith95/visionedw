
$(document).ready(function () {
    $().ready(function () {
        $sidebar = $('.sidebar');
        $sidebar_img_container = $sidebar.find('.sidebar-background');
        $full_page = $('.full-page');
        $sidebar_responsive = $('body > .navbar-collapse');
        window_width = $(window).width();
        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
            if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                $('.fixed-plugin .dropdown').addClass('open');
            }
        }
        $('.fixed-plugin a').click(function (event) {
            // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
            if ($(this).hasClass('switch-trigger')) {
                if (event.stopPropagation) {
                    event.stopPropagation();
                } else if (window.event) {
                    window.event.cancelBubble = true;
                }
            }
        });
        $('.fixed-plugin .active-color span').click(function () {
            $full_page_background = $('.full-page-background');
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('color');
            if ($sidebar.length != 0) {
                $sidebar.attr('data-color', new_color);
            }
            if ($full_page.length != 0) {
                $full_page.attr('filter-color', new_color);
            }
            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.attr('data-color', new_color);
            }
        });
        $('.fixed-plugin .background-color .badge').click(function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var new_color = $(this).data('background-color');
            if ($sidebar.length != 0) {
                $sidebar.attr('data-background-color', new_color);
            }
        });
        $('.fixed-plugin .img-holder').click(function () {
            $full_page_background = $('.full-page-background');
            $(this).parent('li').siblings().removeClass('active');
            $(this).parent('li').addClass('active');
            var new_image = $(this).find("img").attr('src');
            if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                $sidebar_img_container.fadeOut('fast', function () {
                    $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                    $sidebar_img_container.fadeIn('fast');
                });
            }
            if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
                $full_page_background.fadeOut('fast', function () {
                    $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                    $full_page_background.fadeIn('fast');
                });
            }
            if ($('.switch-sidebar-image input:checked').length == 0) {
                var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
                var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
            }
            if ($sidebar_responsive.length != 0) {
                $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
            }
        });
        $('.switch-sidebar-image input').change(function () {
            $full_page_background = $('.full-page-background');
            $input = $(this);
            if ($input.is(':checked')) {
                if ($sidebar_img_container.length != 0) {
                    $sidebar_img_container.fadeIn('fast');
                    $sidebar.attr('data-image', '#');
                }
                if ($full_page_background.length != 0) {
                    $full_page_background.fadeIn('fast');
                    $full_page.attr('data-image', '#');
                }
                background_image = true;
            } else {
                if ($sidebar_img_container.length != 0) {
                    $sidebar.removeAttr('data-image');
                    $sidebar_img_container.fadeOut('fast');
                }
                if ($full_page_background.length != 0) {
                    $full_page.removeAttr('data-image', '#');
                    $full_page_background.fadeOut('fast');
                }
                background_image = false;
            }
        });
        $('.switch-sidebar-mini input').change(function () {
            $body = $('body');
            $input = $(this);
            if (md.misc.sidebar_mini_active == true) {
                $('body').removeClass('sidebar-mini');
                md.misc.sidebar_mini_active = false;
                $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
            } else {
                $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');
                setTimeout(function () {
                    $('body').addClass('sidebar-mini');
                    md.misc.sidebar_mini_active = true;
                }, 300);
            }
            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);
            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
    });
    // Javascript method's body can be found in assets/js/demos.js
    md.initDashboardPageCharts();
   // md.initVectorMap();
});
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        factory(require('jquery'), window, document);
    } else {
        factory(window.jQuery, window, document);
    }
}(function ($, window, document, undefined) {


    /**
		* Calculate scrollbar width
		*
		* Called only once as a constant variable: we assume that scrollbar width never change
		*
		* Original function by Jonathan Sharp:
		* http://jdsharp.us/jQuery/minute/calculate-scrollbar-width.php
	*/
    var SCROLLBAR_WIDTH;

    function scrollbarWidth() {
        // Append a temporary scrolling element to the DOM, then measure
        // the difference between its outer and inner elements.
        var tempEl = $('<div class="scrollbar-width-tester" style="width:50px;height:50px;overflow-y:scroll;top:-200px;left:-200px;"><div style="height:100px;"></div>'),
		width = 0,
		widthMinusScrollbars = 0;

        $('body').append(tempEl);

        width = $(tempEl).innerWidth(),
		widthMinusScrollbars = $('div', tempEl).innerWidth();

        tempEl.remove();

        return (width - widthMinusScrollbars);
    }

    var IS_WEBKIT = 'WebkitAppearance' in document.documentElement.style;



    // SimpleBar Constructor
    function SimpleBar(element, options) {
        this.el = element,
		this.$el = $(element),
		this.$track,
		this.$scrollbar,
		this.dragOffset,
		this.flashTimeout,
		this.$contentEl = this.$el,
		this.$scrollContentEl = this.$el,
		this.scrollDirection = 'vert',
		this.scrollOffsetAttr = 'scrollTop',
		this.sizeAttr = 'height',
		this.scrollSizeAttr = 'scrollHeight',
		this.offsetAttr = 'top';

        this.options = $.extend({}, SimpleBar.DEFAULTS, options);
        this.theme = this.options.css;

        this.init();
    }

    SimpleBar.DEFAULTS = {
        wrapContent: true,
        autoHide: true,
        css: {
            container: 'simplebar',
            content: 'simplebar-content',
            scrollContent: 'simplebar-scroll-content',
            scrollbar: 'simplebar-scrollbar',
            scrollbarTrack: 'simplebar-track'
        }
    };
    SimpleBar.prototype.init = function () {
        // Measure scrollbar width
        if (typeof SCROLLBAR_WIDTH === 'undefined') {
            SCROLLBAR_WIDTH = scrollbarWidth();
        }
        // If scrollbar is a floating scrollbar, disable the plugin
        if (SCROLLBAR_WIDTH === 0) {
            this.$el.css('overflow', 'auto');
            return;
        }
        if (this.$el.data('simplebar-direction') === 'horizontal' || this.$el.hasClass(this.theme.container + ' horizontal')) {
            this.scrollDirection = 'horiz';
            this.scrollOffsetAttr = 'scrollLeft';
            this.sizeAttr = 'width';
            this.scrollSizeAttr = 'scrollWidth';
            this.offsetAttr = 'left';
        }
        if (this.options.wrapContent) {
            this.$el.wrapInner('<div class="' + this.theme.scrollContent + '"><div class="' + this.theme.content + '"></div></div>');
        }
        this.$contentEl = this.$el.find('.' + this.theme.content);
        this.$el.prepend('<div class="' + this.theme.scrollbarTrack + '"><div class="' + this.theme.scrollbar + '"></div></div>');
        this.$track = this.$el.find('.' + this.theme.scrollbarTrack);
        this.$scrollbar = this.$el.find('.' + this.theme.scrollbar);
        this.$scrollContentEl = this.$el.find('.' + this.theme.scrollContent);
        this.resizeScrollContent();
        if (this.options.autoHide) {
            this.$el.on('mouseenter', $.proxy(this.flashScrollbar, this));
        }
        this.$scrollbar.on('mousedown', $.proxy(this.startDrag, this));
        this.$scrollContentEl.on('scroll', $.proxy(this.startScroll, this));

        this.resizeScrollbar();

        if (!this.options.autoHide) {
            this.showScrollbar();
        }
    };
    /**
		* Start scrollbar handle drag
	*/
    SimpleBar.prototype.startDrag = function (e) {
        // Preventing the event's default action stops text being
        // selectable during the drag.
        e.preventDefault();

        // Measure how far the user's mouse is from the top of the scrollbar drag handle.
        var eventOffset = e.pageY;
        if (this.scrollDirection === 'horiz') {
            eventOffset = e.pageX;
        }
        this.dragOffset = eventOffset - this.$scrollbar.offset()[this.offsetAttr];

        $(document).on('mousemove', $.proxy(this.drag, this));
        $(document).on('mouseup', $.proxy(this.endDrag, this));
    };
    /**
		* Drag scrollbar handle
	*/
    SimpleBar.prototype.drag = function (e) {
        e.preventDefault();
        // Calculate how far the user's mouse is from the top/left of the scrollbar (minus the dragOffset).
        var eventOffset = e.pageY,
		dragPos = null,
		dragPerc = null,
		scrollPos = null;
        if (this.scrollDirection === 'horiz') {
            eventOffset = e.pageX;
        }
        dragPos = eventOffset - this.$track.offset()[this.offsetAttr] - this.dragOffset;
        // Convert the mouse position into a percentage of the scrollbar height/width.
        dragPerc = dragPos / this.$track[this.sizeAttr]();
        // Scroll the content by the same percentage.
        scrollPos = dragPerc * this.$contentEl[0][this.scrollSizeAttr];

        this.$scrollContentEl[this.scrollOffsetAttr](scrollPos);
    };
    /**
		* End scroll handle drag
	*/
    SimpleBar.prototype.endDrag = function () {
        $(document).off('mousemove', this.drag);
        $(document).off('mouseup', this.endDrag);
    };
    /**
		* Resize scrollbar
	*/
    SimpleBar.prototype.resizeScrollbar = function () {
        if (SCROLLBAR_WIDTH === 0) {
            return;
        }
        var contentSize = this.$contentEl[0][this.scrollSizeAttr],
		scrollOffset = this.$scrollContentEl[this.scrollOffsetAttr](), // Either scrollTop() or scrollLeft().
		scrollbarSize = this.$track[this.sizeAttr](),
		scrollbarRatio = scrollbarSize / contentSize,
		// Calculate new height/position of drag handle.
		// Offset of 2px allows for a small top/bottom or left/right margin around handle.
		handleOffset = Math.round(scrollbarRatio * scrollOffset) + 2,
		handleSize = Math.floor(scrollbarRatio * (scrollbarSize - 2)) - 2;
        if (scrollbarSize < contentSize) {
            if (this.scrollDirection === 'vert') {
                this.$scrollbar.css({ 'top': handleOffset, 'height': handleSize });
            } else {
                this.$scrollbar.css({ 'left': handleOffset, 'width': handleSize });
            }
            this.$track.show();
        } else {
            this.$track.hide();
        }
    };
    /**
		* On scroll event handling
	*/
    SimpleBar.prototype.startScroll = function (e) {
        // Simulate event bubbling to root element
        this.$el.trigger(e);

        this.flashScrollbar();
    };
    /**
		* Flash scrollbar visibility
	*/
    SimpleBar.prototype.flashScrollbar = function () {
        this.resizeScrollbar();
        this.showScrollbar();
    };
    /**
		* Show scrollbar
	*/
    SimpleBar.prototype.showScrollbar = function () {
        this.$scrollbar.addClass('visible');

        if (!this.options.autoHide) {
            return;
        }
        if (typeof this.flashTimeout === 'number') {
            window.clearTimeout(this.flashTimeout);
        }
        this.flashTimeout = window.setTimeout($.proxy(this.hideScrollbar, this), 1000);
    };
    /**
		* Hide Scrollbar
	*/
    SimpleBar.prototype.hideScrollbar = function () {
        this.$scrollbar.removeClass('visible');
        if (typeof this.flashTimeout === 'number') {
            window.clearTimeout(this.flashTimeout);
        }
    };

    /**
		* Resize content element
	*/
    SimpleBar.prototype.resizeScrollContent = function () {
        if (IS_WEBKIT) {
            return;
        }
        if (this.scrollDirection === 'vert') {
            this.$scrollContentEl.width(this.$el.width() + SCROLLBAR_WIDTH);
            this.$scrollContentEl.height(this.$el.height());
        } else {
            this.$scrollContentEl.width(this.$el.width());
            this.$scrollContentEl.height(this.$el.height() + SCROLLBAR_WIDTH);
        }
    };
    /**
		* Recalculate scrollbar
	*/
    SimpleBar.prototype.recalculate = function () {
        this.resizeScrollContent();
        this.resizeScrollbar();
    };
    /**
		* Getter for original scrolling element
	*/
    SimpleBar.prototype.getScrollElement = function () {
        return this.$scrollContentEl;
    };
    /**
		* Getter for content element
	*/
    SimpleBar.prototype.getContentElement = function () {
        return this.$contentEl;
    };
    /**
		* Data API
	*/
    $(window).on('load', function () {
        $('[data-simplebar-direction]').each(function () {
            $(this).simplebar();
        });
    });
    /**
		* Plugin definition
	*/
    var old = $.fn.simplebar;

    $.fn.simplebar = function (options) {
        var args = arguments,
		returns;

        // If the first parameter is an object (options), or was omitted,
        // instantiate a new instance of the plugin.
        if (typeof options === 'undefined' || typeof options === 'object') {
            return this.each(function () {

                // Only allow the plugin to be instantiated once,
                // so we check that the element has no plugin instantiation yet
                if (!$.data(this, 'simplebar')) { $.data(this, 'simplebar', new SimpleBar(this, options)); }
            });

            // If the first parameter is a string
            // treat this as a call to a public method.
        } else if (typeof options === 'string') {
            this.each(function () {
                var instance = $.data(this, 'simplebar');

                // Tests that there's already a plugin-instance
                // and checks that the requested public method exists
                if (instance instanceof SimpleBar && typeof instance[options] === 'function') {

                    // Call the method of our plugin instance,
                    // and pass it the supplied arguments.
                    returns = instance[options].apply(instance, Array.prototype.slice.call(args, 1));
                }

                // Allow instances to be destroyed via the 'destroy' method
                if (options === 'destroy') {
                    $.data(this, 'simplebar', null);
                }
            });

            // If the earlier cached method
            // gives a value back return the value,
            // otherwise return this to preserve chainability.
            return returns !== undefined ? returns : this;
        }
    };
    $.fn.simplebar.Constructor = SimpleBar;
    /**
		* No conflict
	*/
    $.fn.simplebar.noConflict = function () {
        $.fn.simplebar = old;
        return this;
    };
}));



window.setTimeout(function () {
    $(".alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 16000);

$(function () {
    $('#myElement').simplebar();
     $('#colSwap').simplebar();
    // contact form animations
    $('#contact').click(function () {
        $('#contactForm').fadeToggle();
    })
    $("#treeview").hummingbird();
    $("#treeview1").hummingbird();
    $("#checkAll").click(function () {
        $("#treeview").hummingbird("checkAll");
        
    });
    $("#uncheckAll").click(function () {
        $("#treeview").hummingbird("uncheckAll");
    });
    $("#collapseAll").click(function () {
        $("#treeview").hummingbird("collapseAll");
    });
    $("#checkNode").click(function () {
        $("#treeview").hummingbird("checkNode", { attr: "id", name: "node-0-2-2", expandParents: false });
    });
    $(".openTableProperty").on("click", function () {
        $("#columnPropertyContainer, #relationContainer").fadeOut(200);
        setTimeout(function () {
            $('#tablePropertyContainer').fadeIn();
        }, 300);
    });
    $(".openColumnProperty").on("click", function () {
        $('#tablePropertyContainer, #relationContainer').fadeOut(200);
        setTimeout(function () {
            $("#columnPropertyContainer").fadeIn();
        }, 300);
    });

    $(".openTableRelations").on("click", function () {
        $("#tablePropertyContainer, #columnPropertyContainer").fadeOut(200);
        setTimeout(function () {
            $("#relationContainer").fadeIn();
        }, 300)
    });

    $(".accessCategoryListings").on('change', function () {
        var thisValue = $(".accessCategoryListings option:selected").val();
        if (thisValue != "") {
            $(this).parents(".accessCategoryHold").find(".selectColumnValue").show();
        }
    });

    $(".addColumnValue").on('click', function () {
        $(this).hide();
        $(this).parents('.row').next('.row').removeClass('hidden');
    });
    //Toggle access control modal content
    $(".showAccessControllerListings").on('click', function () {
        if ($(this).find('input[type=checkbox]').is(":checked")) {
            $(".accessControlContainer").removeClass('hidden');
        } else {
            $(".accessControlContainer").addClass('hidden');
        }
    });

    //Toggle magnifier modal content
    $(".showMagnifier").on('click', function () {
        if ($(this).find('input[type=checkbox]').is(":checked")) {
            $(".magnifierControlContainer").removeClass('hidden');
        } else {
            $(".magnifierControlContainer").addClass('hidden');
        }
    });
    //Toggle masking modal content
    $(".showMasking").on('click', function () {
        if ($(this).find('input[type=checkbox]').is(":checked")) {
            $(".maskingControlContainer").removeClass('hidden');
        } else {
            $(".maskingControlContainer").addClass('hidden');
        }
    });
    //Toggle property icon on hover
    $(".childLi>label").mouseenter(function () {
        $(this).children(".openColumnProperty").stop().fadeIn(300);
    });
    $(".childLi>label").mouseleave(function () {
        $(this).children(".openColumnProperty").stop().fadeOut(300);
    });

    $(".parentLi>label").mouseenter(function () {
        $(this).children(".openTableProperty, .openTableRelations").stop().fadeIn(300);
    });
    $(".parentLi>label").mouseleave(function () {
        $(this).children(".openTableProperty, .openTableRelations").stop().fadeOut(300);
    });

    $(document).mouseup(function (e) {
        var container = $("#contactForm");

        if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.fadeOut();
        }
    });

});



/************************* Add datasource section ****************************/
var dataSourceHtml = '<select data-placeholder="Choose a Country" class="chosen-select" multiple tabindex="1">' +
'<option data-brackets-id="810" disabled=""> Choose city</option>' +
'<option data-brackets-id="811" value="2">Paris </option>' +
'<option data-brackets-id="812" value="3">Bucharest</option>' +
'<option data-brackets-id="813" value="4">Rome</option>' +
'<option data-brackets-id="814" value="5">New York</option>' +
'<option data-brackets-id="815" value="6">Miami </option>' +
'<option data-brackets-id="816" value="7">Piatra Neamt</option>' +
'<option data-brackets-id="817" value="8">Paris </option>' +
'<option data-brackets-id="818" value="9">Bucharest</option>' +
'<option data-brackets-id="819" value="10">Rome</option>' +
'<option data-brackets-id="820" value="11">New York</option>' +
'<option data-brackets-id="821" value="12">Miami </option>' +
'<option data-brackets-id="822" value="13">Piatra Neamt</option>' +
'<option data-brackets-id="823" value="14">Paris </option>' +
'<option data-brackets-id="824" value="15">Bucharest</option>' +
'<option data-brackets-id="825" value="16">Rome</option>' +
'<option data-brackets-id="826" value="17">New York</option>' +
'<option data-brackets-id="827" value="18">Miami </option>' +
'<option data-brackets-id="828" value="19">Piatra Neamt</option>' +
'</select>';

