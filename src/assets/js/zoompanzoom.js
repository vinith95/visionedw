(function($) {

    $.fn.zoompanzoom = function(options) {

        var settings = $.extend({
            animationSpeed: "fast",
            zoomfactor: .1,
            maxZoom: 2.5,
            minZoom: 0.5,
            disablePan: true
        }, options);

        var zoomdiv = this;
        currentZoom = 1.0;
        currentZoom1 = 1.0;
        if (!settings.disablePan) {
            $(this).draggable();
        }

        jQuery('#zoom_in').click(
            function($event) {
                if($event.currentTarget.classList.contains('dashboard-menu')){
                    console.log($event);
                    console.log(currentZoom);
                    if(zoomdiv.selector == '#panZoomEle')  {
                        if (currentZoom < settings.maxZoom) {
                            jQuery(zoomdiv).animate({
                                'zoom': currentZoom += settings.zoomfactor
                            }, settings.animationSpeed);
                        }
                    }             
                }
            })
        jQuery('#zoom_out').click(
            function($event) {
                if($event.currentTarget.classList.contains('dashboard-menu')){
                    if(zoomdiv.selector == '#panZoomEle')  {
                        if (currentZoom > settings.minZoom) {
                            jQuery(zoomdiv).animate({
                                'zoom': currentZoom -= settings.zoomfactor
                            }, settings.animationSpeed);
                        }
                    }
                }
            })
        $(".moduleBoxZoom").on('click', function(event){
            event.stopPropagation();
            let ids=event.currentTarget.dataset.id
            let idName=event.currentTarget.id.slice(0, -1);
            if(zoomdiv.selector == '#gridsterContainer'+ids){
                if(idName == 'zoom_in'){
                    if (currentZoom1 < settings.maxZoom) {
                        jQuery(zoomdiv).animate({
                            'zoom': currentZoom1 += settings.zoomfactor
                        }, settings.animationSpeed);
                    }
                }
                if(idName == 'zoom_out'){
                    if (currentZoom1 > settings.minZoom) {
                        jQuery(zoomdiv).animate({
                            'zoom': currentZoom1 -= settings.zoomfactor
                        }, settings.animationSpeed);
                    }
                }
            }

        })  
        jQuery('#zoom_reset').click(
            function() {
                currentZoom = 1.0;
                jQuery(zoomdiv).animate({
                    'zoom': 1
                }, settings.animationSpeed);
            })
    };

}(jQuery));