$(document).ready(function() {
    let authDropValue;
    $('#authorize_dropdown').on('change', function() {
        authDropValue = $(this).val();
        if (authDropValue.length) {
            $('#approve_selected, #reject_selected').css('visibility', 'visible');
        } else {
            $('#approve_selected, #reject_selected').css('visibility', 'hidden');
        }
    });
    //$('body').on('click', '#authorize_drop', function () {
    //    $('.approved').parents('tr').hide();
    //});
    $('body').on('click', '#authorize_drop_cont a', function() {
        authDropValue = $(this).text().trim();
        $('#authorize_dropdown').fadeOut(200, function() {
            $('#approve_authorized').fadeIn('300');
        });
    });
    $('body').on('click', '#approve_authorized', function() {
        if (authDropValue == 'Modify pending') {
            $('.modifyPending').text('Approved');
            $('.modifyPending').removeClass('brown-sbg');
            $('.modifyPending').addClass('green-sbg approved');
            $('.modifyPending').removeClass('modifyPending');
        } else if (authDropValue == 'Add pending') {
            $('.addPending').text('Approved');
            $('.addPending').removeClass('blue-sbg');
            $('.addPending').addClass('green-sbg approved');
            $('.addPending').removeClass('addPending');
        } else if (authDropValue == 'Delete pending') {
            $('.deletePending').text('Approved');
            $('.deletePending').removeClass('red-sbg');
            $('.deletePending').addClass('red-sbg approved');
            $('.deletePending').removeClass('deletePending');
        } else if (authDropValue == 'Approve all') {
            $('.modifyPending, .addPending, .deletePending').text('Approved');
            $('.modifyPending').removeClass('brown-sbg');
            $('.addPending').removeClass('blue-sbg');
            $('.deletePending').removeClass('red-sbg');

            $('.modifyPending').addClass('green-sbg approved');
            $('.addPending').addClass('green-sbg approved');
            $('.deletePending').addClass('red-sbg approved');

            $('.modifyPending').removeClass('modifyPending');
            $('.addPending').removeClass('addPending');
            $('.deletePending').removeClass('deletePending');

            $('.approved').parents('tr').show();
        }
        //$(this).hide();
        //$('#authorize_drop').hide();
        //$('#authorize').show();
    });
    // $('body').on('click', '.selectAll', function() {
    //     if ($(this).is(':checked')) {
    //         $('.selectRow').prop('checked', true);
    //         $('#approve_selected, #reject_selected').css('visibility', 'visible');
    //     } else {
    //         $('.selectRow').prop('checked', false);
    //         $('#approve_selected, #reject_selected').css('visibility', 'hidden');
    //     }
    // });

    $('body').on('click', '#hide_tree .slide_open', function() {
        $(this).parent().toggleClass('nowidth');
        $(this).parent().next().toggleClass('expandTo100percen');

    });


    toggleAuthorizeButton();
    $('body').on('click', '.selectRow', function() {
        toggleAuthorizeButton();
    });

    function toggleAuthorizeButton() {
        const getCheckedLength = $('.selectRow:checked').length;
        if (getCheckedLength) {
            $('#approve_selected, #reject_selected').css('visibility', 'visible');
        } else {
            $('#approve_selected, #reject_selected').css('visibility', 'hidden');
        }
    }
});

function getSelectValue(tarElem) {
    //let selectValue = (tarElem.value || tarElem.options[tarElem.selectedIndex].value);
    let selectValue = (tarElem.value || tarElem.options[tarElem.selectedIndex].value);
    const targetTable = document.getElementById('transactionConfigTbl');
    const targetTr = targetTable.querySelectorAll('tr');
    if (selectValue !== 'all') {
        targetTr.forEach((elem, index) => {
            targetTr[index].children[5].classList.add('hidden');
            targetTr[index].children[6].classList.add('hidden');
            targetTr[index].children[7].classList.add('hidden');
        });
    } else {
        targetTr.forEach((elem, index) => {
            targetTr[index].children[5].classList.remove('hidden');
            targetTr[index].children[6].classList.remove('hidden');
            targetTr[index].children[7].classList.remove('hidden');
        });
    }
}

function setHeightForTableandPagenation() {
    const materialTableContHoldLength = document.getElementById('materialTableContHold');
    if (materialTableContHoldLength) {
        const matTblContHeight = document.getElementById('materialTableContHold').clientHeight;
    }
}

document.addEventListener('readystatechange', event => {
    if (event.target.readyState === "interactive") { //same as:  ..addEventListener("DOMContentLoaded".. and   jQuery.ready
        //alert("All HTML DOM elements are accessible");
    }

    if (event.target.readyState === "complete") {
        //alert("Now external resources are loaded too, like css,src etc... ");
        setHeightForTableandPagenation();
    }

});

window.addEventListener("resize", setHeightForTableandPagenation);