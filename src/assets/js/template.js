   var MYLIBRARY = MYLIBRARY || (function(){
    var _args = {}; // private
    return {
        init : function(Args) {
            _args = Args;
            MYLIBRARY.output=_args;
        }

    };
}());
   if(MYLIBRARY.output == undefined){
    var table_id='';
    var table_name='';
    var mapping_id='';
    var mapping_name='';
    var ctable_name='';
   }
   else if(MYLIBRARY.output[0].table_name){
    var table_id=MYLIBRARY.output[0].table_id;
    var table_name=MYLIBRARY.output[0].table_name;
   }
   else if(MYLIBRARY.output[0].mapping_name){
    var mapping_id=MYLIBRARY.output[0].mapping_id;
    var mapping_name=MYLIBRARY.output[0].mapping_name;
   }
   else if(MYLIBRARY.output[0].ctable_name){
    var ctable_name=MYLIBRARY.output[0].ctable_name;
    var topPosition=MYLIBRARY.output[0].topPosition;
    var leftPosition=MYLIBRARY.output[0].leftPosition;
    var read_or_write=MYLIBRARY.output[0].read_or_write;
    var mapping_id=MYLIBRARY.output[0].mapping_id;
    var property=MYLIBRARY.output[0].property;
   }


if(table_name!=''){

    var selected=JSON.parse(localStorage.getItem("selected"));   
    for (var z = 0; z < selected.length; z++) {
        if(selected[z].tableAliasName == table_name){
           var selected_one= selected[z];
           var index_arr=z;
        }
    };

var htmlTemplate =`<div class="tab-pane  flex-con-col findThis1" id="caps${table_id}" style="height:100%">
                            <div class="color-ico " id="color-ico1"><i class="fa fa-th " style="display: inline-block;" aria-hidden="true"></i>
                                <h5 class="text-cap"> General</h5>
                            </div>
                            <div class="pull-right ico-top-pos overview_resize" id="overview_resize">
<i class="fa fa-expand" aria-hidden="true"></i>
                            </div>
                            <div class="mb-3 " id="tab2">
                                <div class="block-header col-md-2">Name:</div>
                                <div class="header-desc">${selected_one.tableAliasName}</div>
                                <br>
                                <div class="block-header col-md-2">Description:</div>
                                <div class="header-desc">${selected_one.tableNameDescription}</div>
                            </div>

            
                         <div class="color-ico " id="color-ico2">
                                <i class="fa fa-th " style="display: inline-block;" aria-hidden="true"></i>
                                <h5 class="text-cap"> Columns</h5>
                            </div>
                        <div class="preview_table content"  id="preview_table_example${table_id}" style="height:54%;position:relative;">
                            <table class="table table-striped table-hover my-custom-table"  id="example${table_id}" style="font-size: small !important;position:absolute;height:100%;" cellspacing="0" width="100%">
                            </table>
                        </div>

                    </div>`;

var tabHtmlTemplate=`<div class="tab-pane findThis hgt100" id="capss${table_id}">
                                <div class='hgt100' style="position:relative">
                                    <ul class="nav nav-pills nav-pills-rose nav-pills-icons less-pad " role="tablist" >
                                        <li class="nav-item disabled"> <a class="nav-link" data-toggle="tab" href="#link3" role="tablist"> Properties </a> </li>
                                        <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#link4" role="tablist"> Data Viewer </a> </li>
                                    </ul> 
                                    <div class="col-md-4 show_reco bmd-form-group" id="drop-hide">
                                        <label><span class="label-pad">Show</span>
                                            <select class="data_viewer" data-style="select-with-transition" title="Select" data-size="7" onchange="showRecordButton(this.value,'${selected_one.tableName}',${index_arr})" id="proper_change">
                                                <option value="50">1 to 50 </option>
                                                <option value="100">51 to 100 </option>
                                                <option value="150">101 to 150 </option>
                                                <option value="200">151 to 200 </option>
                                            </select>
                                      <span class="label-pad">&nbsp; records out of 200 records</span>
                                        </label>    
                                    </div>

<div class="pull-right ico-top-pos" id="data_viewer_resize">
<i class="fa fa-expand" aria-hidden="true"></i>
                            </div>

                                    <div class="tab-content overflowAuto height87" style="height:87%">
                                        <div class="tab-pane hgt100" id="link3">
                                        </div>
                                        <div class="tab-pane active  show hgt100" id="link4">
                                            <div id="show_record_table${table_id}" style="position:relative;height:100%"></div>
                                        </div>
                                    </div>
                              </div>
                        </div>`;


var lihtml=`<div class="chip" id="table_overview${table_id}">

        <i class="fa fa-database"></i><a href="javascript:void(0);"  onclick="showThisTab(${table_id},'table')" class="chip_name">
            ${selected_one.tableAliasName}
        </a>
        <a href="javascript:void(0);"  onclick="closeThisTab(${table_id},'','')">
            <i class="material-icons close" >close</i>
        </a>
    </div>`;

var tableHtml='';

tableHtml+='<thead><tr><th>Name</th><th>Native Type</th><th>Precision</th><th>Scale</th></tr></thead><tbody>';

for (var k = 0; k < selected_one.columns.length; k++) {
   tableHtml+='<tr><td>'+selected_one.columns[k].attribute_name+'</td><td>'+selected_one.columns[k].attribute_type+'</td><td>'+selected_one.columns[k].attribute_precision+'</td><td>'+selected_one.columns[k].attribute_scale+'</td></tr>';
};

tableHtml+='</tbody>';

}


//Cleaner template start
if(mapping_name != ""){

    var mapping=JSON.parse(localStorage.getItem("mapping"));   
    for (var z = 0; z < mapping.length; z++) {
        if(mapping[z].mapping_name == mapping_name){
           var mapping_one= mapping[z];
           var mapping_index=z;
        }
    };
var connection=[];
    var mappingTab=`<div class="chip" id="mapping_overview${mapping_id}">
        <a href="javascript:void(0);"  onclick="showThisTab(${mapping_id},'mapping')" class="chip_name">
           <img src="images/mapping.png" alt="mapping" class="map_ico">&nbsp;${mapping_one.mapping_name}
        </a>
        <a href="javascript:void(0);"  onclick="closeThisMappingTab(${mapping_id})">
            <i class="material-icons close" >close</i>
        </a>
    </div>`;

    var mappingHtmlTemplate =`<div class="tab-pane flex-con-col findThis1" id="map_caps${mapping_id}" style="height:100%">
    <div class="pull-right ico-top-pos overview_resize" id="overview_resize">
        <i class="fa fa-expand" aria-hidden="true"></i>
    </div>
        <div class="jtk-page-container" id="jtk-page-container" style="height: 99%;">
          <div class="jtk-container" id="jtk-container" style="height: 100%;">
            <div class="jtk-demo-main" id="jtk-demo-main" style="height: 100%;">
              <div class="jtk-demo-canvas" id="canvas" style="height: 100%;">
                                              <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  hide-preview-pane nopadding" rel="tooltip" title="Transformations" data-placement="right" id="hide_pane"></a>
              <div class="tool-box hide">
                                    <div class="text-center transformation-icon">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Case Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 94 94" style="enable-background:new 0 0 94 94;" xml:space="preserve" class="">
                                                            <g transform="matrix(1.295 0 0 1.295 -13.8649 -13.8649)">
                                                                <g>
                                                                    <g>
                                                                        <polygon points="32.697,36.083 31.345,36.091 26.966,49.274 37.054,49.274   " data-original="#000000" class="active-path" fill="#000000" />
                                                                        <path d="M62.268,55.57c0,2.465,1.645,3.665,3.793,3.665c2.403,0,4.362-1.579,4.994-3.54c0.127-0.507,0.191-1.074,0.191-1.644    v-3.353C66.188,50.637,62.268,51.84,62.268,55.57z" data-original="#000000" class="active-path" fill="#000000" />
                                                                        <path d="M0,12.887v68.227h94V12.887H0z M40.826,65.305l-1.929-7.346h-13.77l-1.984,7.346H12.956l12.347-38.002H38.45    l12.502,38.002H40.826z M72.383,65.305l-0.568-3.098h-0.189C69.602,64.674,66.441,66,62.773,66c-6.258,0-9.987-4.551-9.987-9.482    c0-8.029,7.206-11.887,18.144-11.822v-0.443c0-1.643-0.886-3.982-5.627-3.982c-3.16,0-6.512,1.076-8.535,2.34l-1.77-6.195    c2.148-1.201,6.385-2.719,12.012-2.719c10.305,0,13.592,6.068,13.592,13.34v10.747c0,2.971,0.125,5.815,0.442,7.522L72.383,65.305    L72.383,65.305z" data-original="#000000" class="active-path" fill="#000000" />
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" onclick="create_table('Exp_',320,36,'','',${mapping_id},'express')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Expression Transformation" data-placement="right">
                                                      <svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M299.396 368.773H43.416c-7.094 0-13.55-3.54-16.566-9.063s-2.03-12.05 2.52-16.74l127.37-131.534L29.37 79.92c-4.55-4.705-5.52-11.233-2.52-16.755 3.017-5.523 9.47-9.063 16.566-9.063h255.98c10.094 0 18.287 7.032 18.287 15.733v47.2c0 8.7-8.193 15.736-18.287 15.736s-18.283-7.035-18.283-15.736V85.57H82.453L194.6 201.352c5.65 5.837 5.65 14.315 0 20.155L82.453 337.305h198.66v-31.468c0-8.7 8.192-15.733 18.283-15.733s18.287 7.032 18.287 15.733v47.2c0 8.7-8.193 15.736-18.287 15.736zm0 0"/></svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Filter Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 402.577 402.577" style="enable-background:new 0 0 402.577 402.577;" xml:space="preserve" class="">
                                                            <g transform="matrix(0.950515 0 0 0.950515 9.96082 9.96082)">
                                                                <g>
                                                                    <path d="M400.858,11.427c-3.241-7.421-8.85-11.132-16.854-11.136H18.564c-7.993,0-13.61,3.715-16.846,11.136   c-3.234,7.801-1.903,14.467,3.999,19.985l140.757,140.753v138.755c0,4.955,1.809,9.232,5.424,12.854l73.085,73.083   c3.429,3.614,7.71,5.428,12.851,5.428c2.282,0,4.66-0.479,7.135-1.43c7.426-3.238,11.14-8.851,11.14-16.845V172.166L396.861,31.413   C402.765,25.895,404.093,19.231,400.858,11.427z" data-original="#000000" class="active-path" data-old_color="#EODDDD" fill="#E0DDDD" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Sorting Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24" enable-background="new 0 0 24 24" width="512px" height="512px" class="">
                                                            <g transform="matrix(0.99 0 0 0.99 0.12 0.12)">
                                                                <g>
                                                                    <path d="M9,0C8.7,0,8.5,0.1,8.3,0.3l-8,8C0.1,8.5,0,8.7,0,9c0,0.3,0.1,0.5,0.3,0.7l1.4,1.4c0.2,0.2,0.5,0.3,0.7,0.3   c0.3,0,0.5-0.1,0.7-0.3l0,0l2-2C5.4,8.8,6,9,6,9.5V23c0,0.5,0.5,1,1,1h2c0.5,0,1-0.5,1-1V1c0-0.3-0.1-0.5-0.3-0.7   C9.5,0.1,9.3,0,9,0z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#E0DDDD" />
                                                                    <path d="m23.7,14.3l-1.4-1.4c-0.2-0.2-0.5-0.3-0.7-0.3-0.3,0-0.5,0.1-0.7,0.3l-2,2c-0.3,0.3-0.9,0.1-0.9-0.4v-13.5c0-0.5-0.5-1-1-1h-2c-0.5,0-1,0.5-1,1v22c0,0.3 0.1,0.5 0.3,0.7s0.5,0.3 0.7,0.3c0.3,0 0.5-0.1 0.7-0.3l8-8c0.2-0.2 0.3-0.4 0.3-0.7 0-0.3-0.1-0.5-0.3-0.7z" data-original="#000000" class="active-path" data-old_color="#000000" fill="#E0DDDD" />
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Analyse  Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="453.543" height="453.543" viewBox="-60 -60 120 120" style="width: 22px; height: 21px;
"><g transform="matrix(2.58315 0 0 2.49774 -63.811742 -60.568216)" fill="#000" stroke="#000" stroke-width=".041"><circle cx="33.59" cy="14.82" r="2.31"></circle><path d="M20 16.86A1.38 1.38 0 1 0 21.56 18 1.38 1.38 0 0 0 20 16.86zm3.7-8.66a.91.91 0 0 0 .76-1 .92.92 0 0 0-1.05-.76.93.93 0 0 0-.76 1 .92.92 0 0 0 1.05.76z"></path><path d="M42 6.17C35.8-1.67 22.7.2 22.7.2 14.12.2 7.17 8.54 7.17 18.83a22.94 22.94 0 0 0 .2 3s-4.7 5.28-4.3 6.7 3.6 1.16 3.6 1.16 1 .9 1 1.67-.9 2.3-.25 3a2.23 2.23 0 0 0 1 .71s-1.2 1.56-.48 1.93c1.6.72 1 2.35.72 3.6-.45 2.38-.15 4.72 2.6 5.36s5.1-.63 7.22-2.1a.29.29 0 0 0 .06.16c.32.38.4 4 .4 4l18.44-1.48a9.16 9.16 0 0 1-1.57-6c.07-2.16 1.1-3 2.23-4.8.7-1.1 1.4-2.22 2-3.37C44 25.1 47.8 13.54 42 6.17zm-21.7.34l.3-.8.65.25a2.24 2.24 0 0 1 .31-.43L21.12 5l.68-.54.43.53a2.5 2.5 0 0 1 .48-.21l-.1-.7.85-.13.1.7a3 3 0 0 1 .53 0l.25-.64.8.3-.24.7a3.11 3.11 0 0 1 .43.31l.53-.44.55.68-.54.45a2 2 0 0 1 .21.49l.7-.1.14.85-.7.1a3.31 3.31 0 0 1-.06.53l.65.24-.3.8-.65-.25a2.24 2.24 0 0 1-.31.43l.44.54-.68.55-.44-.54a3 3 0 0 1-.48.21l.1.68-.85.13-.1-.67a3.19 3.19 0 0 1-.54-.09l-.24.65-.76-.3.25-.65a2.61 2.61 0 0 1-.43-.31l-.54.44-.54-.68.53-.43a3.33 3.33 0 0 1-.27-.48l-.68.1-.13-.85.67-.1a2.18 2.18 0 0 1 .06-.52zm4.83 12.9l-.47 1.22-1-.38a4.24 4.24 0 0 1-.46.65l.66.8-1 .82-.67-.8a3 3 0 0 1-.73.32l.16 1-1.28.2-.16-1a4.35 4.35 0 0 1-.79-.09l-.37 1-1.22-.46.37-1a5 5 0 0 1-.64-.47l-.82.66-.82-1 .8-.66a4.26 4.26 0 0 1-.32-.73l-1 .17-.2-1.3 1-.16a4.09 4.09 0 0 1 .08-.8l-1-.37.47-1.22 1 .37a4.3 4.3 0 0 1 .47-.65l-.65-.8 1-.82.65.8a4.7 4.7 0 0 1 .73-.33l-.16-1 1.28-.2.17 1a4.17 4.17 0 0 1 .8.08l.37-1 1.23.47-.37 1a4.21 4.21 0 0 1 .64.47l.8-.65.82 1-.8.66a4 4 0 0 1 .3.79l1-.16.2 1.28-1 .17a5.29 5.29 0 0 1-.08.79zm16.33-1.3l-1.1 1.9-1.53-.88a7 7 0 0 1-.94.94l.88 1.53-1.9 1.1-.85-1.5a5.87 5.87 0 0 1-1.3.34v1.78h-2.15v-1.8a6.06 6.06 0 0 1-1.32-.34l-.9 1.52-1.9-1.1.88-1.53a7 7 0 0 1-.95-.94l-1.52.87-1.1-1.9 1.52-.87A6.61 6.61 0 0 1 26.9 16h-1.67v-2.22h1.67a6.21 6.21 0 0 1 .34-1.32l-1.5-.9 1.1-1.9 1.5.87a8.52 8.52 0 0 1 .95-.95l-.87-1.5L30.33 7l.9 1.5a6.3 6.3 0 0 1 1.33-.35v-1.7h2.12v1.67a6.11 6.11 0 0 1 1.33.35L36.86 7l1.9 1.1-.87 1.5a7.84 7.84 0 0 1 1 1l1.5-.87 1.1 1.9-1.52.88a5.66 5.66 0 0 1 .35 1.32h1.78V16h-1.84a6.59 6.59 0 0 1-.34 1.27z"></path></g></svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Merge Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512.188 512.188" style="enable-background:new 0 0 512.188 512.188;" xml:space="preserve" width="512px" height="512px">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <path d="M498.773,229.854l-99.789-72.073c-7.834-5.658-18.039-6.426-26.641-2.039c-8.602,4.403-13.943,13.133-13.943,22.801    v43.418h-76.8v-68.267c0-61.167-49.766-110.933-110.933-110.933h-28.578C129.186,15.923,105.148,0.094,76.8,0.094    C34.458,0.094,0,34.551,0,76.894s34.458,76.8,76.8,76.8c28.348,0,52.386-15.829,65.289-42.667h28.578    c23.526,0,42.667,19.14,42.667,42.667v68.267h-71.245c-12.902-26.837-36.941-42.667-65.289-42.667    c-42.342,0-76.8,34.458-76.8,76.8s34.458,76.8,76.8,76.8c28.348,0,52.386-15.829,65.289-42.667h71.245v68.267    c0,23.526-19.14,42.667-42.667,42.667h-28.578c-12.902-26.837-36.941-42.667-65.289-42.667c-42.342,0-76.8,34.458-76.8,76.8    s34.458,76.8,76.8,76.8c28.348,0,52.386-15.829,65.289-42.667h28.578c61.167,0,110.933-49.766,110.933-110.933v-59.733h76.8    v34.884c0,9.668,5.342,18.398,13.943,22.801c8.602,4.395,18.825,3.627,26.641-2.039l99.789-72.073    c8.525-6.161,13.414-15.727,13.414-26.24S507.298,236.015,498.773,229.854z" data-original="#000000" class="active-path" fill="#000000" />
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Standardizer Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="453.543" height="453.543" viewBox="-60 -60 120 120" fill="#000" stroke="#000" stroke-width=".132" style="
    width: 20px;
    height: 20px;
"><path d="M.06 59.713c-32.716 0-59.336-26.697-59.336-59.5s26.62-59.5 59.336-59.5S59.396-32.607 59.396.205 32.777 59.713.06 59.713zm-.265-106.47C-28.5-46.758-47.8-26.85-47.8 1.527S-28.236 48.225.06 48.225 47.93 28.847 47.93.47 28.084-46.758-.204-46.758zm5.805 10.4H-5.362c-4.54 0-8.218 3.757-8.218 8.383S-9.9-19.6-5.362-19.6H5.6c4.54 0 8.218-3.757 8.218-8.383s-3.68-8.383-8.218-8.383zm0 11.18H-5.362c-1.506 0-2.737-1.257-2.737-2.797s1.23-2.797 2.737-2.797H5.6c1.506 0 2.737 1.257 2.737 2.797A2.77 2.77 0 0 1 5.6-25.176zm0 16.767H-5.362c-4.54 0-8.218 3.757-8.218 8.383s3.68 8.383 8.218 8.383H5.6c4.54 0 8.218-3.757 8.218-8.383S10.14-8.4 5.6-8.4zm0 11.18H-5.362A2.77 2.77 0 0 1-8.098-.026c0-1.54 1.23-2.797 2.737-2.797H5.6c1.506 0 2.737 1.257 2.737 2.797A2.77 2.77 0 0 1 5.6 2.771zm0 16.767H-5.362c-4.54 0-8.218 3.757-8.218 8.383s3.68 8.383 8.218 8.383H5.6c4.54 0 8.218-3.757 8.218-8.383s-3.68-8.383-8.218-8.383zm0 11.18H-5.362a2.77 2.77 0 0 1-2.737-2.797c0-1.54 1.23-2.797 2.737-2.797H5.6c1.506 0 2.737 1.257 2.737 2.797A2.77 2.77 0 0 1 5.6 30.718zm-30.135-5.594c-2.93 0-5.68-1.168-7.75-3.273a11.25 11.25 0 0 1-3.204-7.907c0-3 1.142-5.795 3.204-7.907a10.78 10.78 0 0 1 7.75-3.273h5.48v-5.586h-5.48c-9.078 0-16.436 7.506-16.436 16.767S-33.613 30.7-24.535 30.7v5.586l8.218-8.383-8.218-8.383zM24.78-30.77H19.3v5.586h5.48c2.93 0 5.68 1.168 7.75 3.273 2.077 2.113 3.204 4.917 3.204 7.907S34.592-8.21 32.53-6.097c-2.077 2.105-4.82 3.273-7.75 3.273V-8.4L16.563-.026l8.218 8.383V2.77A16.15 16.15 0 0 0 34.788-.694c3.9-3.065 6.43-7.883 6.43-13.303 0-9.26-7.358-16.774-16.436-16.774z"></path></svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Router  Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;width: 20px;height: 20px;" xml:space="preserve"> <path style="fill:#000000;" d="M406.069,238.345H273.655V132.414c0-9.754-7.901-17.655-17.655-17.655s-17.655,7.901-17.655,17.655 v105.931H105.931c-29.255,0-52.966,23.711-52.966,52.965v88.276c0,9.754,7.901,17.655,17.655,17.655s17.655-7.901,17.655-17.655 V291.31c0-9.754,7.901-17.655,17.655-17.655h132.414v105.931c0,9.754,7.901,17.655,17.655,17.655s17.655-7.901,17.655-17.655 V273.655h132.414c9.754,0,17.655,7.901,17.655,17.655v88.276c0,9.754,7.901,17.655,17.655,17.655s17.655-7.901,17.655-17.655V291.31 C459.034,262.056,435.324,238.345,406.069,238.345z"></path> <g> <path style="fill:#000000;" d="M256,150.069c-39,0-70.621-31.62-70.621-70.621S217,8.828,256,8.828s70.621,31.62,70.621,70.621 S295,150.069,256,150.069z M256,44.138c-19.5,0-35.31,15.81-35.31,35.31s15.81,35.31,35.31,35.31s35.31-15.81,35.31-35.31 S275.5,44.138,256,44.138z"></path> <path style="fill:#000000;" d="M256,503.172c-39,0-70.621-31.62-70.621-70.621S217,361.931,256,361.931s70.621,31.62,70.621,70.621 S295,503.172,256,503.172z M256,397.241c-19.5,0-35.31,15.81-35.31,35.31c0,19.5,15.81,35.31,35.31,35.31s35.31-15.81,35.31-35.31 C291.31,413.052,275.5,397.241,256,397.241z"></path> <path style="fill:#000000;" d="M70.621,503.172c-39,0-70.621-31.62-70.621-70.621s31.62-70.621,70.621-70.621 s70.621,31.62,70.621,70.621S109.621,503.172,70.621,503.172z M70.621,397.241c-19.5,0-35.31,15.81-35.31,35.31 c0,19.5,15.81,35.31,35.31,35.31s35.31-15.81,35.31-35.31C105.931,413.052,90.121,397.241,70.621,397.241z"></path> <path style="fill:#000000;" d="M441.379,503.172c-39,0-70.621-31.62-70.621-70.621s31.62-70.621,70.621-70.621 S512,393.551,512,432.552S480.38,503.172,441.379,503.172z M441.379,397.241c-19.5,0-35.31,15.81-35.31,35.31 c0,19.5,15.81,35.31,35.31,35.31c19.5,0,35.31-15.81,35.31-35.31C476.69,413.052,460.879,397.241,441.379,397.241z"></path> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Lookup Transformation" data-placement="right">
                                                        <svg class="icon" height="512" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" style="
    width: 20px;
    height: 20px;
"><path d="M1014.2 967.8 841.8 795.4C915.4 711.1 960 600.7 960 480c0-265.1-214.9-480-480-480S0 214.9 0 480s214.9 480 480 480c121.3 0 232.1-45 316.6-119.2L969 1013.1c6.2 6.2 14.4 9.4 22.6 9.4s16.4-3.1 22.6-9.4C1026.7 1000.6 1026.7 980.3 1014.2 967.8zM480 896c-56.2 0-110.7-11-161.9-32.7-49.5-20.9-94-51-132.3-89.2s-68.2-82.7-89.2-132.3C75 590.6 64 536.2 64 480c0-56.2 11-110.7 32.7-161.9 20.9-49.5 51-94 89.2-132.3s82.7-68.2 132.3-89.2c51.2-21.7 105.7-32.7 161.9-32.7 56.2 0 110.7 11 161.9 32.7 49.5 20.9 94 51 132.3 89.2s68.2 82.7 89.2 132.3C885 369.3 896 423.8 896 480c0 56.2-11 110.7-32.7 161.9-20.9 49.5-51 94-89.2 132.3s-82.7 68.2-132.3 89.2C590.7 885 536.2 896 480 896zM558.6 206.1c-9-9.1-21.3-14.2-34.2-14.2L310 191.9c-29.8 0-54 24.1-54 53.8l0 468.4c0 29.7 24.2 53.8 54 53.8l340 0c29.8 0 54-24.1 54-53.8L704 371.6c0-12.7-5-24.8-14-33.8L558.6 206.1zM640 704 320 704l0-448 197.9 0L640 378.3 640 704zM371.7 384l91.9 0c17.7 0 32-14.3 32-32s-14.3-32-32-32l-91.9 0c-17.7 0-32 14.3-32 32S354 384 371.7 384zM339.7 480c0 17.7 14.3 32 32 32l206.9 0c17.7 0 32-14.3 32-32s-14.3-32-32-32L371.7 448C354 448 339.7 462.3 339.7 480zM578.6 576 371.7 576c-17.7 0-32 14.3-32 32s14.3 32 32 32l206.9 0c17.7 0 32-14.3 32-32S596.2 576 578.6 576z"></path></svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Union Transformation" data-placement="right">
                                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="548.756px" height="548.756px" viewBox="0 0 548.756 548.756" style="enable-background:new 0 0 548.756 548.756;" xml:space="preserve">
                                                            <g>
                                                                <g>
                                                                    <path d="M153,75.236c0-6.763-5.483-12.24-12.24-12.24H12.24C5.483,62.996,0,68.474,0,75.236v79.021h153V75.236z"/>
                                                                    <path d="M395.756,211.382c0,66.929-54.449,121.378-121.378,121.378C207.45,332.76,153,278.311,153,211.382v-32.638H0v32.638
                                                C0,362.674,123.085,485.76,274.378,485.76c151.293,0,274.378-123.086,274.378-274.378v-32.638h-153V211.382z"/>
                                                                    <path d="M536.516,62.996h-128.52c-6.757,0-12.24,5.478-12.24,12.24v79.021h153V75.236
                                                C548.756,68.479,543.278,62.996,536.516,62.996z"/>
                                                                </g>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Parser Transformation" data-placement="right">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 25.937 25.937" style="enable-background:new 0 0 25.937 25.937;" xml:space="preserve" width="512px" height="512px"><g transform="matrix(-6.12323e-17 1 -1 -6.12323e-17 25.937 0)"><g>
                                                            <path d="M15.794,25.937h-5.552c-0.092,0-0.169-0.073-0.169-0.165v-8.7c-0.76-0.727-1.74-1.695-2.594-2.74   c-1.03-1.286-2.22-3.043-2.244-5.255v-2.83H2.274c-0.069,0-0.127-0.036-0.156-0.097C2.091,6.09,2.102,6.022,2.143,5.97l4.993-5.905   C7.164,0.023,7.212,0,7.261,0l0,0c0.05,0,0.099,0.023,0.128,0.064l4.953,5.876c0.047,0.029,0.074,0.082,0.074,0.136   c0,0.099-0.082,0.179-0.172,0.17c-0.003,0-0.003,0-0.005,0H9.356v2.829c0,0.354,0.126,0.943,0.716,1.833   c0.479,0.734,1.226,1.592,2.284,2.634c0.198,0.199,0.403,0.399,0.611,0.599c0.21-0.198,0.416-0.399,0.616-0.599   c1.04-1.02,1.807-1.909,2.28-2.634c0.59-0.89,0.716-1.477,0.716-1.814V6.246h-2.882c-0.064,0-0.126-0.036-0.151-0.097   c-0.03-0.06-0.02-0.127,0.02-0.18l4.976-5.905C18.579,0.023,18.625,0,18.674,0l0,0c0.053,0,0.097,0.023,0.128,0.064l4.991,5.906   c0.043,0.053,0.054,0.12,0.027,0.18c-0.03,0.061-0.089,0.097-0.155,0.097H20.7v2.829c-0.022,2.213-1.211,3.97-2.241,5.256   c-0.65,0.794-1.421,1.612-2.497,2.644v8.797C15.962,25.863,15.887,25.937,15.794,25.937z" data-original="#030104" class="active-path" data-old_color="#030104" fill="#030104"/>
                                                        </g></g> </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding hide-preview-pane1" rel="tooltip" data-placement="left" title="Tools" id="hide_pane1">
                                </a>
                                <div class="tool-box1 hide">
                                    <div class="text-center transformation-tools-icon">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="#" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Save" data-placement="left">
                                                    <span class="ico_bor"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Validate" data-placement="left">
                                                    <span class="ico_bor"><i class="fa fa-check" aria-hidden="true"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="icon-svg">
                                                    <a href="javascript:void(0);" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Refresh" data-placement="left">
                                                    <span class="ico_bor"><i class="fa fa-refresh" aria-hidden="true"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
              </div></div></div></div>
</div>`;
var mappping_link3=``;
var mappping_link4=``;

    var mappingTabHtmlTemplate=`<div class="tab-pane findThis hgt100" id="map_capss${mapping_id}">
                                <div class='hgt100' style="position:relative">
                                    <ul class="nav nav-pills nav-pills-rose nav-pills-icons less-pad " role="tablist" >
                                        <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#link3" onclick="select_option('link3')" role="tablist"> Properties </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#link4" onclick="select_option('link4')" role="tablist"> Data Viewer </a> </li>
                                    </ul> 

                                    <div class="col-md-3 bmd-form-group" id="mapping_drop_hide1" style="position:absolute; top:-9px; right:39px">
                                                <select class="data_viewer" data-style="select-with-transition" title="Select" data-size="7" id="proper_change_n" onchange="generalPortChange(this.value,${mapping_id})">
                                                    <option value="1" selected="selected">General</option>
                                                    <option value="2">Ports</option>
                                                </select>
                                                <div class="line"></div>
                                    </div>

                                    <div class="col-md-4 show_reco bmd-form-group" id="mapping_drop_hide">
                                        <label><span class="label-pad">Show</span>
                                            <select class="data_viewer" data-style="select-with-transition" title="Select" data-size="7" id="proper_change">
                                                <option value="50">1 to 50 </option>
                                                <option value="100">51 to 100 </option>
                                                <option value="150">101 to 150 </option>
                                                <option value="200">151 to 200 </option>
                                            </select>
                                      <span class="label-pad">&nbsp;records out of 200 records</span>
                                        </label>    
                                    </div>
                                    <div class="pull-right ico-top-pos" id="data_viewer_resize"><i class="fa fa-expand" aria-hidden="true"></i>
                                    </div>

                                    <div class="tab-content  hgt86 overflowAuto">
                                        <div class="tab-pane hgt100  active show " id="link3">
                                            ${mappping_link3}
                                        </div>
                                        <div class="tab-pane  hgt100" id="link4">
                                        ${mappping_link4}
                                        </div>
                                    </div>
                              </div>
                        </div>`;
}




if(ctable_name != ''){
    if(read_or_write== 'Read'){
        var map_icons_header='<svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M314.62 227.36V196.92c0-5.3-4.462-9.596-9.966-9.596-20.588 0-39.782 1.673-57.916 5.09-10.57-9.83-23.458-16.96-37.413-20.858 15.217-12.332 24.905-30.796 24.905-51.4 0-37.4-31.294-67.81-69.76-67.81s-69.76 30.42-69.76 67.81c0 20.604 9.688 39.068 24.906 51.4-13.955 3.898-26.842 11.03-37.413 20.858-18.134-3.415-37.328-5.09-57.916-5.09-5.504 0-9.966 4.296-9.966 9.596v30.437c-11.6 3.96-19.93 14.627-19.93 27.137v19.19c0 12.5 8.332 23.175 19.93 27.137v30.437c0 5.3 4.462 9.596 9.966 9.596 52.107 0 94.895 11.888 134.657 37.41 3.332 2.138 7.72 2.143 11.056 0 39.762-25.523 82.55-37.41 134.657-37.41 5.504 0 9.966-4.296 9.966-9.596v-30.437c11.6-3.96 19.93-14.627 19.93-27.137v-19.19c-.001-12.51-8.333-23.175-19.933-27.136zM114.64 120.156c0-26.808 22.353-48.618 49.83-48.618s49.83 21.8 49.83 48.618c0 26.455-22.353 47.978-49.83 47.978s-49.83-21.523-49.83-47.978zM24.284 283.282c-5.495 0-9.966-4.305-9.966-9.596v-19.19c0-5.29 4.47-9.596 9.966-9.596s9.966 4.305 9.966 9.596v19.19c0 5.29-4.47 9.596-9.966 9.596zm130.22 69.783c-36.178-19.92-75-30-120.253-31.262v-20.98c11.6-3.96 19.93-14.627 19.93-27.137v-19.19c0-12.5-8.332-23.175-19.93-27.137v-20.706c46.826 1.294 84.466 11.91 120.253 33.85zm9.966-129.187c-18.394-11.175-37.54-19.684-57.98-25.663 11.23-7.028 24.45-10.89 38.048-10.89H184.4c13.597 0 26.817 3.86 38.048 10.89-20.44 5.98-39.586 14.488-57.98 25.663zm130.22 97.925c-45.253 1.272-84.076 11.343-120.253 31.263V240.51c35.788-21.94 73.425-32.565 120.253-33.86v20.706c-11.6 3.96-19.93 14.627-19.93 27.137v19.19c0 12.5 8.332 23.175 19.93 27.137zm19.93-48.117c0 5.29-4.47 9.596-9.966 9.596s-9.966-4.305-9.966-9.596v-19.19c0-5.29 4.47-9.596 9.966-9.596s9.966 4.305 9.966 9.596z"/></svg>';
        var minimize_image_svg='<svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M314.62 227.36V196.92c0-5.3-4.462-9.596-9.966-9.596-20.588 0-39.782 1.673-57.916 5.09-10.57-9.83-23.458-16.96-37.413-20.858 15.217-12.332 24.905-30.796 24.905-51.4 0-37.4-31.294-67.81-69.76-67.81s-69.76 30.42-69.76 67.81c0 20.604 9.688 39.068 24.906 51.4-13.955 3.898-26.842 11.03-37.413 20.858-18.134-3.415-37.328-5.09-57.916-5.09-5.504 0-9.966 4.296-9.966 9.596v30.437c-11.6 3.96-19.93 14.627-19.93 27.137v19.19c0 12.5 8.332 23.175 19.93 27.137v30.437c0 5.3 4.462 9.596 9.966 9.596 52.107 0 94.895 11.888 134.657 37.41 3.332 2.138 7.72 2.143 11.056 0 39.762-25.523 82.55-37.41 134.657-37.41 5.504 0 9.966-4.296 9.966-9.596v-30.437c11.6-3.96 19.93-14.627 19.93-27.137v-19.19c-.001-12.51-8.333-23.175-19.933-27.136zM114.64 120.156c0-26.808 22.353-48.618 49.83-48.618s49.83 21.8 49.83 48.618c0 26.455-22.353 47.978-49.83 47.978s-49.83-21.523-49.83-47.978zM24.284 283.282c-5.495 0-9.966-4.305-9.966-9.596v-19.19c0-5.29 4.47-9.596 9.966-9.596s9.966 4.305 9.966 9.596v19.19c0 5.29-4.47 9.596-9.966 9.596zm130.22 69.783c-36.178-19.92-75-30-120.253-31.262v-20.98c11.6-3.96 19.93-14.627 19.93-27.137v-19.19c0-12.5-8.332-23.175-19.93-27.137v-20.706c46.826 1.294 84.466 11.91 120.253 33.85zm9.966-129.187c-18.394-11.175-37.54-19.684-57.98-25.663 11.23-7.028 24.45-10.89 38.048-10.89H184.4c13.597 0 26.817 3.86 38.048 10.89-20.44 5.98-39.586 14.488-57.98 25.663zm130.22 97.925c-45.253 1.272-84.076 11.343-120.253 31.263V240.51c35.788-21.94 73.425-32.565 120.253-33.86v20.706c-11.6 3.96-19.93 14.627-19.93 27.137v19.19c0 12.5 8.332 23.175 19.93 27.137zm19.93-48.117c0 5.29-4.47 9.596-9.966 9.596s-9.966-4.305-9.966-9.596v-19.19c0-5.29 4.47-9.596 9.966-9.596s9.966 4.305 9.966 9.596z"/></svg>';
    }
    else if(read_or_write== 'Write'){
        var map_icons_header='<svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M207.908 121.417l56.65 56.65L121.163 321.46l-56.617-56.65zm99.635-13.662L282.28 82.492c-9.763-9.763-25.617-9.763-35.414 0l-24.2 24.2 56.65 56.65 28.228-28.228c7.573-7.573 7.573-19.785 0-27.358zM34.9 344.436c-1.03 4.64 3.158 8.797 7.798 7.67l63.126-15.306-56.617-56.65z"/></svg>'; 
        var minimize_image_svg ='<svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M207.908 121.417l56.65 56.65L121.163 321.46l-56.617-56.65zm99.635-13.662L282.28 82.492c-9.763-9.763-25.617-9.763-35.414 0l-24.2 24.2 56.65 56.65 28.228-28.228c7.573-7.573 7.573-19.785 0-27.358zM34.9 344.436c-1.03 4.64 3.158 8.797 7.798 7.67l63.126-15.306-56.617-56.65z"/></svg>';
    }
    else{
         if(property== 'express'){
            var map_icons_header='<svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M299.396 368.773H43.416c-7.094 0-13.55-3.54-16.566-9.063s-2.03-12.05 2.52-16.74l127.37-131.534L29.37 79.92c-4.55-4.705-5.52-11.233-2.52-16.755 3.017-5.523 9.47-9.063 16.566-9.063h255.98c10.094 0 18.287 7.032 18.287 15.733v47.2c0 8.7-8.193 15.736-18.287 15.736s-18.283-7.035-18.283-15.736V85.57H82.453L194.6 201.352c5.65 5.837 5.65 14.315 0 20.155L82.453 337.305h198.66v-31.468c0-8.7 8.192-15.733 18.283-15.733s18.287 7.032 18.287 15.733v47.2c0 8.7-8.193 15.736-18.287 15.736zm0 0"/></svg>'; 
            var minimize_image_svg ='<svg xmlns="http://www.w3.org/2000/svg" height="512" viewBox="-42 0 426 426.667" width="512"><path d="M299.396 368.773H43.416c-7.094 0-13.55-3.54-16.566-9.063s-2.03-12.05 2.52-16.74l127.37-131.534L29.37 79.92c-4.55-4.705-5.52-11.233-2.52-16.755 3.017-5.523 9.47-9.063 16.566-9.063h255.98c10.094 0 18.287 7.032 18.287 15.733v47.2c0 8.7-8.193 15.736-18.287 15.736s-18.283-7.035-18.283-15.736V85.57H82.453L194.6 201.352c5.65 5.837 5.65 14.315 0 20.155L82.453 337.305h198.66v-31.468c0-8.7 8.192-15.733 18.283-15.733s18.287 7.032 18.287 15.733v47.2c0 8.7-8.193 15.736-18.287 15.736zm0 0"/></svg>';
        }
    }
    if(property == 'table'){
        var selected=JSON.parse(localStorage.getItem("selected"));   
        for (var z = 0; z < selected.length; z++) {
            if(selected[z].tableAliasName == ctable_name){
               var selected_one= selected[z];
               var sindex_arr=z;
            }
        };

        var cleanerTable=`<div class="drag_table" data-id="${mapping_id}" data-type="table"
            id="drag_table_${sindex_arr}" style="top:${topPosition}px;left:${leftPosition}px;" onresize="resizable_datatable(${sindex_arr})">
            <input type="hidden" value="${read_or_write}_${ctable_name}" id="fetch_header_${sindex_arr}_${mapping_id}"/>
            <div class="tab_icon_hide">
                <a href="javascript:void(0);" onclick="add_row_table(${sindex_arr},'')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Add" data-placement="top">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="edit_table_row(${sindex_arr},'')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Edit" data-placement="top">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="TableMoveUpDown(${sindex_arr},'up','','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Move up" data-placement="top">
                    <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="TableMoveUpDown(${sindex_arr},'down','','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Move down" data-placement="top">
                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="TableCopy(${sindex_arr})" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Copy row(s)" data-placement="top">
                    <i class="fa fa-files-o" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="TablePaste(${sindex_arr})" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Paste" data-placement="top" >
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="selectAll(${sindex_arr})" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Select All"  data-placement="top">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="deleteAll(${sindex_arr},'',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="delete" data-placement="top" title="Delete row(s)">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </div>
            <div class="header_title_table">
            ${map_icons_header}
            <span class="g_sudmit_edit">${read_or_write}_${ctable_name}</span>
                <div class="pull-right">
                    <a href="javascript:void(0);"  onclick="minimizeThis(${sindex_arr});" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Minimize" data-placement="top">
                        <i class="fa fa-minus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0);" onclick="close_table(${sindex_arr})" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Delete Table" data-placement="top">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="container_min">
             <table id="dateTable_${sindex_arr}_${mapping_id}" class="display nowrap tablefont">
            <thead>
              <tr>
                <th></th>
                <th id='column-header-1'>Name<div id='column-header-1-sizer'></div></th>
                <th id='column-header-2'>Type<div id='column-header-2-sizer'></div></th>
                <th id='column-header-3'>Precision<div id='column-header-3-sizer'></div></th>
                <th id='column-header-4'>Scale<div id='column-header-4-sizer'></div></th>
                <th></th>
              </tr>
            </thead>
        <tbody source target>`;
        for (var i = 0; i < selected_one.columns.length; i++) {
        cleanerTable+=`<tr onclick="selectThis('ref_class_${sindex_arr}_${selected_one.columns[i].attribute_id}')" data-id="ref_class_${sindex_arr}_${selected_one.columns[i].attribute_id}" class="ui-widget-header">
            <td data-id="ref_class_${sindex_arr}_${selected_one.columns[i].attribute_id}_L"><img src="images/connector.png" width="10" height="10">
            </td>
            <td disabled>${selected_one.columns[i].attribute_name}</td>
            <td disabled>${selected_one.columns[i].attribute_type}</td>
            <td disabled>${selected_one.columns[i].attribute_precision}</td>
            <td disabled>${selected_one.columns[i].attribute_scale}</td>
            <td data-id="ref_class_${sindex_arr}_${selected_one.columns[i].attribute_id}_R"><img src="images/connector.png" width="10" height="10">
            </td>
          </tr>`;
        }
        cleanerTable+=`</tbody></table></div>
            <div id="container_max_${sindex_arr}" class="container_max">
            <a href="javascript:void(0);" ondblclick="maximizeThis(${sindex_arr});" id="minimizeThis" class="tooltip-trigger modal-trigger waves-effect btn-flat nopadding minimize_icon"  rel="tooltip" title="${read_or_write}_${ctable_name}"  data-placement="top"></a>
            <span data-id="ref_class_${sindex_arr}_M">${minimize_image_svg}</span>

            </div></div>`;
    }
    if(property == 'output'){
        var selected=JSON.parse(localStorage.getItem("outputs"));   
        for (var z = 0; z < selected.length; z++) {
            if(selected[z].tableAliasName == ctable_name){
               var selected_one= selected[z];
               var sindex_arr=z;
            }
        };
        sindex_arr='output'+sindex_arr;
        var cleanerTable=`<div class="drag_table" data-id="${mapping_id}" data-type="output"
            id="drag_table_${sindex_arr}" style="top:${topPosition}px;left:${leftPosition}px;" onresize="resizable_datatable(${sindex_arr})">
            <input type="hidden" value="${read_or_write}_${ctable_name}" id="fetch_header_${sindex_arr}_${mapping_id}"/>
            <div class="tab_icon_hide">
                <a href="javascript:void(0);" onclick="add_row_table('${sindex_arr}','')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Add" data-placement="top">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="edit_table_row('${sindex_arr}','')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Edit" data-placement="top">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="TableMoveUpDown('${sindex_arr}','up','','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Move up" data-placement="top">
                    <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="TableMoveUpDown('${sindex_arr}','down','','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Move down" data-placement="top">
                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="TableCopy('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Copy row(s)" data-placement="top" >
                    <i class="fa fa-files-o" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="TablePaste('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Paste" data-placement="top">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="selectAll('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Select All" data-placement="top">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="deleteAll('${sindex_arr}','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Delete row(s)" data-placement="top">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </div>
            <div class="header_title_table">
            ${map_icons_header}
            <span class="g_sudmit_edit">${read_or_write}_${ctable_name}</span>
                <div class="pull-right">
                    <a href="javascript:void(0);"  onclick="minimizeThis('${sindex_arr}');" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Minimize" data-placement="top">
                        <i class="fa fa-minus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0);" onclick="close_table('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Delete Table" data-placement="top">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="container_min"></div>
        <div id="container_max_${sindex_arr}" class="container_max">
        <a href="javascript:void(0);" ondblclick="maximizeThis('${sindex_arr}');" id="minimizeThis" class="tooltip-trigger modal-trigger waves-effect btn-flat nopadding minimize_icon"  rel="tooltip" title="${read_or_write}_${ctable_name}" data-placement="top"></a>
        <span data-id="ref_class_${sindex_arr}_M">${minimize_image_svg}</span>
        </div></div>`;
    }
    if(property == "express"){
        var selected=JSON.parse(localStorage.getItem("express"));   
        for (var z = 0; z < selected.length; z++) {
            if(selected[z].tableAliasName == ctable_name){
               var selected_one= selected[z];
               var sindex_arr=z;
            }
        };
        sindex_arr='express'+sindex_arr;
        var cleanerTable=`<div class="drag_table" data-id="${mapping_id}" data-type="express"
            id="drag_table_${sindex_arr}" style="top:${topPosition}px;left:${leftPosition}px;" onresize="resizable_datatable(${sindex_arr})">
            <input type="hidden" value="${ctable_name}" id="fetch_header_${sindex_arr}_${mapping_id}"/>
            <div class="tab_icon_hide">
                <a href="javascript:void(0);" onclick="add_row_table('${sindex_arr}','')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Add" data-placement="top">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="edit_table_row('${sindex_arr}','')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Edit" data-placement="top">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="TableMoveUpDown('${sindex_arr}','up','','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Move up" data-placement="top"">
                    <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="TableMoveUpDown('${sindex_arr}','down','','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Move down" data-placement="top">
                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="TableCopy('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Copy row(s)" data-placement="top">
                    <i class="fa fa-files-o" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="TablePaste('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Paste" data-placement="top">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </a>

                <a href="javascript:void(0);" onclick="selectAll('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Select All" data-placement="top">
                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0);" onclick="deleteAll('${sindex_arr}','',false)" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Delete row(s)" data-placement="top">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </div>
            <div class="header_title_table">
            ${map_icons_header}
            <span class="g_sudmit_edit">${ctable_name}</span>
                <div class="pull-right">
                    <a href="javascript:void(0);"  onclick="minimizeThis('${sindex_arr}');" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Minimize" data-placement="top">
                        <i class="fa fa-minus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="javascript:void(0);" onclick="close_table('${sindex_arr}')" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="Delete Table" data-placement="top">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="container_min"></div>
        <div id="container_max_${sindex_arr}" class="container_max">
        <a href="javascript:void(0);" ondblclick="maximizeThis('${sindex_arr}');" id="minimizeThis" class="tooltip-trigger modal-trigger waves-effect btn-flat nopadding minimize_icon"  rel="tooltip" title="${ctable_name}" data-placement="top"></a>
        <span data-id="ref_class_${sindex_arr}_M">${minimize_image_svg}</span>
        </div></div>`;

    }
}