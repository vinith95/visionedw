$(document).ready(function() {
    md.initDashboardPageCharts();
    $sidebar = $('.sidebar');
    $sidebar_img_container = $sidebar.find('.sidebar-background');
    $full_page = $('.full-page');
    $sidebar_responsive = $('body > .navbar-collapse');
    window_width = $(window).width();
    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
    if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
        if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
        }
    }
    $('.fixed-plugin a').click(function(event) {
        // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
        if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
                event.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
        }
    });
    $('.fixed-plugin .active-color span').click(function() {
        $full_page_background = $('.full-page-background');
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        var new_color = $(this).data('color');
        if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
        }
        if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
        }
        if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
        }
    });
    $('.fixed-plugin .background-color .badge').click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        var new_color = $(this).data('background-color');
        if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
        }
    });
    $('.fixed-plugin .img-holder').click(function() {
        $full_page_background = $('.full-page-background');
        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').addClass('active');
        var new_image = $(this).find("img").attr('src');
        if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $sidebar_img_container.fadeIn('fast');
            });
        }
        if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
            $full_page_background.fadeOut('fast', function() {
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                $full_page_background.fadeIn('fast');
            });
        }
        if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
        }
        if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
        }
    });
    $('.switch-sidebar-image input').change(function() {
        $full_page_background = $('.full-page-background');
        $input = $(this);
        if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
                $sidebar_img_container.fadeIn('fast');
                $sidebar.attr('data-image', '#');
            }
            if ($full_page_background.length != 0) {
                $full_page_background.fadeIn('fast');
                $full_page.attr('data-image', '#');
            }
            background_image = true;
        } else {
            if ($sidebar_img_container.length != 0) {
                $sidebar.removeAttr('data-image');
                $sidebar_img_container.fadeOut('fast');
            }
            if ($full_page_background.length != 0) {
                $full_page.removeAttr('data-image', '#');
                $full_page_background.fadeOut('fast');
            }
            background_image = false;
        }
    });
    $('.switch-sidebar-mini input').change(function() {
        $body = $('body');
        $input = $(this);
        if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;
            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();
        } else {
            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');
            setTimeout(function() {
                $('body').addClass('sidebar-mini');
                md.misc.sidebar_mini_active = true;
            }, 300);
        }
        // we simulate the window Resize so the charts will get updated in realtime.
        var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
        }, 180);
        // we stop the simulation of Window Resize after the animations are completed
        setTimeout(function() {
            clearInterval(simulateWindowResize);
        }, 1000);
    });
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function() {
            $(this).remove();
        });
    }, 16000);
    $('#treeview_container').simplebar();
    $('#treeview_container3').simplebar();
    $('#myElement').simplebar();
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function() {
            $(this).remove();
        });
    }, 16000);
    $(function() {

        // contact form animations
        $('#contact').click(function() {
            $('#contactForm').fadeToggle();
        })
        $(document).mouseup(function(e) {
            var container = $("#contactForm");

            if (!container.is(e.target) // if the target of the click isn't the container...
                &&
                container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.fadeOut();
            }
        });
        $(".cssLinkChanger").on('click', function() {
            var cssLink = $(this).attr('rel');
            $('.switchCss').attr('href', cssLink);
        });

    });

    $('.btn-toggle').click(function() {
        $(this).find('.btn').toggleClass('active');

        if ($(this).find('.btn-primary').length > 0) {
            $(this).find('.btn').toggleClass('btn-primary');
        }
        if ($(this).find('.btn-danger').length > 0) {
            $(this).find('.btn').toggleClass('btn-danger');
        }
        if ($(this).find('.btn-success').length > 0) {
            $(this).find('.btn').toggleClass('btn-success');
        }
        if ($(this).find('.btn-info').length > 0) {
            $(this).find('.btn').toggleClass('btn-info');
        }

        $(this).find('.btn').toggleClass('btn-default');

    });

    $('form').submit(function() {
        var radioValue = $("input[name='options']:checked").val();
        if (radioValue) {
            alert("You selected - " + radioValue);
        };
        return false;
    });
    const newRow = `<tr role="row" class="odd">
                        <td><span title="100">100</span></td>
                        <td><span title="AP-001">AP-001</span></td>
                        <td class=" my_class" title="Ajay Kumar">Ajay Kumar</td>
                        <td title="SuniodaChennai">SuniodaChennai</td>
                        <td><span title="Finance Team">Finance Team</span></td>
                        <td><span title="Sunoida Chennai">Sunoida Chennai</span></td>
                        <td><span title="Demo User">Demo User</span></td>
                        <td><span title="26-08-2019 19:17:11">26-08-2019 19:17:11</span></td>
                        <td>
                            <span class="rec-style white-text green-sbg" title="Approved">Approved</span>
                        </td>
                        <td>
                            <div class="text-ellipsis">
                                <span class="green-dot"></span><span title="Active">Active</span>
                            </div>
                            <div class="tr-edit-icons row" *ngif="columnIndex==dataColumnList.length-1">
                                <a href="javascript:void(0)" (click)="getRecordsToEdit(item,'edit')" class=" modal-trigger  waves-effect btn-flat nopadding" rel="tooltip" title="" data-original-title="Edit" data-toggle="modal" data-placement="top">
                                    <i class="material-icons ">create</i>
                                </a>
                                <a href="javascript:void(0)" *ngif="shareIconVisible(item)" class="col-3 modal-trigger waves-effect btn-flat  nopadding" rel="tooltip" title="" data-original-title="View " onclick="getRecordsToview()" data-toggle="modal" data-placement="top">
                                    <i class="material-icons">description</i>
                                </a>
                                <a href="javascript:void(0)" [ngclass]="{'col-3' : !shareIconVisible(item) }" class=" modal-trigger waves-effect btn-flat nopadding" rel="tooltip" title="" data-original-title="Remove" (click)="getRecordsToEdit(item,'delete')">
                                    <i class="material-icons">delete_outline</i>
                                </a>
                            </div>
                        </td>
                    </tr>`;
    $('#addRecords').on('click', function() {
        $('#profile-setup-table tbody').append(newRow);
    });

    let select = $("<select></select>");
    let menuLength = 10;

    for (let i = 0; i < menuLength; i++) {
        let option = $("<option></option>");
        if (i === 0) {
            $(option).val('NA');
            $(option).html('NA');
            $(select).append(option);
        } else {
            $(option).val(i);
            $(option).html(i);
            $(select).append(option);
        }
    }
    select = $(select).html();

    //Build sub menu select
    const menuGroup = ["Account Buckets", "Account Officers", "Accounts Dly", "Accounts", "Accounts Mapping", "Asset Type", "Bank Grades", "Partial GL Translation", "Countries", "Currencies", "Currency Conversion", "Currency Pairings", "Customer Attributes", "Customer Enrich Mappings", "Customer Id Remappings", "Customers", "Customers Dly", "EID Mappings", "GL Codes", "GL Enrich Ids", "GL Mappings", "General Ledger", "Core GL Translation", "Other Parameters", "Job Codes", "LE Book", "LE Book Variables", "Legal Vehicle", "GL Merge", "NAICS Codes", "Period Controls", "Pool Codes", "Remote File Transfer", "Scheme - Product", "Tenor Buckets", "Transaction Code", "Transaction Code Group", "Transaction Code Mappings", "Transaction Id Mappings", "Transactions", "Vision Details"];
    const menuProgram = ["AccountBuckets", "AccountOfficers", "AccountsDlyGL", "AccountsGL", "AccountsMapping", "AssetType", "BankGrades", "CGLTranslation", "Countries", "Currencies", "CurrencyConversion", "CurrencyPairings", "CustomerAttributes", "CustomerEnrichMappings", "CustomerIdRemappings", "Customers", "CustomersDly", "EIDMappings", "GLCodes", "GLEnrichIds", "GLMappings", "GLStatic", "GLTranslation", "GeneralStatic", "JobCodes", "LEBook", "LEBookVariables", "LegalVehicles", "MergeTable", "NAICSCodes", "PeriodControls", "PoolCodes", "RemoteFileTransfer", "SchemeCodeProductMap", "TenorBuckets", "TransactionCode", "TransactionCodeGrp", "TransactionCodeMap", "TransactionIdMappings", "Transactions", "VisionDetails"];
    let subMenuSelect = $("<select></select>");
    let menuGroupLength = menuGroup.length;

    for (let i = 0; i < menuGroupLength; i++) {
        let option = $("<option></option>");
        if (i === 0) {
            $(option).val('NA');
            $(option).html('NA');
            $(subMenuSelect).append(option);
        } else {
            $(option).val(menuGroup[i]);
            $(option).html(menuGroup[i]);
            $(subMenuSelect).append(option);
        }

    }
    subMenuSelect = $(subMenuSelect).html();
    const checkboxHtml = `<div class="form-check">
                              <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" value="" /> <span class="form-check-sign">
                                      <span class="check"></span>
                                  </span>
                              </label>
                          </div>`;



    menuProgram.forEach((data, index) => {
        $('#menuAccessList tbody').append(`<tr><td>${checkboxHtml}</td><td style='max-width: 150px;'><span>${data}</span></td></tr>`);
        //<td><input type='checkbox' class='choosenMenu' /></td>
    });
    menuGroup.forEach((data, index) => {
        //$('#menuAccessList tbody tr').eq(index).append(`<td><span>${data}</span></td><td><select style='width: 100%;' class="selectpicker" id="menuSequence" data-style="select-with-transition" title="NA" data-size="7" tabindex="-98">${select}</select></td><td><select multiple style='width: 100%;' class="selectpicker" id='subMenu' data-style="select-with-transition" title="NA" data-size="7" tabindex="-98"><option disabled="">Multi Select</option>${subMenuSelect}</td>`);
        $('#menuAccessList tbody tr').eq(index).append(`<td><span>${data}</span></td>
                                                        <td>${checkboxHtml}</td>
                                                        <td>${checkboxHtml}</td>
                                                        <td>${checkboxHtml}</td>
                                                        <td>${checkboxHtml}</td>
                                                        <td>${checkboxHtml}</td>
                                                        <td>${checkboxHtml}</td>`);
    });

    $('body').on('click', '.hasSubChild td:not(:first-child)', function() {
        $(this).parent().next('tr').find('.nestedTableScrollDiv').slideToggle('500');
    });

});