$('#minimizeSidebar1').on('click', function() {
    alert('ddd');
    if ($('body').hasClass('sidebar-mini')) {
        $('body').removeClass('sidebar-mini');
        if ($('.w-h-20').hasClass('width20')) {
            $('.hide-preview-pane').animate({ "left": "495px" });
            $(".tool-box").animate({ "left": "495px" });
        } else {
            $('.hide-preview-pane').animate({ "left": "282px" });
            $(".tool-box").animate({ "left": "282px" });
        }
    } else {
        $('body').addClass('sidebar-mini');
        if ($('.w-h-20').hasClass('width20')) {
            $('.hide-preview-pane').animate({ "left": "352px" })
            $(".tool-box").animate({ "left": "352px" });
        } else {
            $('.hide-preview-pane').animate({ "left": "103px" })
            $(".tool-box").animate({ "left": "103px" });
        }
    }
    $('.logo-mini').toggle();
});

$(document).ready(function() {
    $("body").on("mouseover", '.tooltip-trigger', function() {
        $('[role="tooltip"]').each(function() {
            if (!($(this).hasClass('show'))) {
                $(this).remove();
            }
        })

    });

    $("body").on("mouseleave", '.tooltip-trigger', function() {
        $('[role="tooltip"]').each(function() {
            if (!($(this).hasClass('show'))) {
                $(this).remove();
            }
        })

    });

    $('body').on('click', '#hide_pane', function() {
        $(".tool-box").toggle(400);
        //$(".hide-preview-pane").toggleClass('transfromation_left')
    });
    $('body').on('click', '#hide_pane1', function() {
        $(".tool-box1").toggle(400);
        //$(".hide-preview-pane1").toggleClass('tools_right');
    });

    $(".ui-tooltip-content").parents('div').remove();

    localStorage.removeItem("connection");
    localStorage.removeItem("copy");
    localStorage.removeItem("copy_table");
    localStorage.removeItem("outputs");
    localStorage.removeItem("express");

    if (localStorage.getItem("project_name")) {
        $('.pdo').removeClass('fa-folder');
        $('.pdo').addClass('fa-folder-open');
    }

    if (localStorage.getItem("selected")) {
        $('.pdo12').removeClass('fa-folder');
        $('.pdo12').addClass('fa-folder-open');
    }

    if (localStorage.getItem("mapping")) {
        $('.pdo2').removeClass('fa-folder');
        $('.pdo2').addClass('fa-folder-open');
    }

    // if ($('body').hasClass('sidebar-mini')) {
    //     $('body').removeClass('sidebar-mini');
    // } else {
    //     $('body').addClass('sidebar-mini');
    // }
    $('.logo-mini').toggle();
    $('body').on('click', '#property_resize', function() {
        $('.bg-grey-01').toggleClass('add_prop_ext_cls');
        if ($('.bg-grey-01').hasClass('add_prop_ext_cls')) {
            $('.bg-grey-01').animate({ height: 688 }, 400);
            $('#property_resize i').toggleClass('fa-expand fa-compress');
        } else {
            $('.bg-grey-01').animate({ height: 100 }, 400);
            $('#property_resize i').toggleClass('fa-compress fa-expand');
        }
    })

    $('body').on('click', '.drag_table', function() {
        var clickids = $(this).attr("id");
        $('.drag_table').each(function() {
            var ids = $(this).attr("id");
            var type = $(this).attr("data-type");
            if (clickids == ids) {
                if (type == 'table') {
                    var required_id = ids.slice(ids.length - 1);
                    var sindex_arr = required_id;
                    var selected = JSON.parse(localStorage.getItem("selected"));
                } else if (type == 'output') {
                    var required_id = ids.slice(ids.length - 1);
                    var sindex_arr = 'output' + required_id;
                    var selected = JSON.parse(localStorage.getItem("outputs"));
                } else {
                    var required_id = ids.slice(ids.length - 1);
                    var sindex_arr = 'express' + required_id;
                    var selected = JSON.parse(localStorage.getItem("express"));
                }
                $("#" + ids + ' .container_min').css({ "height": "79%" });
                var mapping_id = $("#" + ids).attr("data-id");
                $('#drag_table_' + sindex_arr + ' .header_title_table').addClass('trans_box_shadow');
                $('#drag_table_' + sindex_arr + ' .container_min').addClass('trans_box_shadow');
                $('#mapping_drop_hide1').show();
                $('#mapping_drop_hide').hide();
                var title = $('#fetch_header_' + sindex_arr + '_' + mapping_id).val();

                var selected_one = selected[required_id];
                var mappping_link3 = `<div id="read_proper">
                    <table class="table table-striped table-hover hide" id="read_pro1" style="font-size: small !important;position:absolute; border:1px solid lightgray;" cellspacing="0">
                        <thead>
                            <tr>
                            <th>Property</th>
                            <th>Value</th>
                            <th><a onclick="openModal1('general_edit','${sindex_arr}',${mapping_id})" class="tooltip-trigger pull-right" rel="tooltip" title="Edit" data-placement="left" >
                            <i class="fa fa-pencil-square" aria-hidden="true"></i></a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>Name</td>
                            <td colspan="2">${title}</td>
                            </tr>
                            <tr>
                            <td>Description</td>
                            <td colspan="2" id="fetch_table_description">${title}_description</td>
                            </tr>
                        </tbody>
                    </table>`;
                if (type == 'express') {
                    mappping_link3 += `<div id="mappping_link3_read_pro2">
                                        <table class="table table-striped table-hover" id="read_pro2"  style="width: 100%;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                        <th>Name</th>
                                                        <th>Type</th>
                                                        <th>Precision</th>
                                                        <th>Scale</th>
                                                        <th>Input</th>
                                                        <th>Output</th>
                                                        <th style="border-right: 0px solid black !important;">Expression</th>
                                                        <th style="border-left: 0px solid black !important;">
                                                        <span class="pull-right" style="margin-right:0px; opacity: 0.7;">
                                                                <a class="tooltip-trigger" onclick="add_row_table('${sindex_arr}','port_add')" rel="tooltip" title="Add" data-placement="left"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="tooltip-trigger"  onclick="edit_table_row('${sindex_arr}','port_edit')" rel="tooltip" title="Edit" data-placement="left"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                                                                <a class="tooltip-trigger"  onclick="TableMoveUpDownViewer('${sindex_arr}','up')" rel="tooltip" title="Move up" data-placement="left">
                                                                    <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="tooltip-trigger"  onclick="TableMoveUpDownViewer('${sindex_arr}','down')" rel="tooltip" title="Move down" data-placement="left">
                                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                                                                </a>
                                                                <a class="tooltip-trigger"  onclick="deleteAllViewer('${sindex_arr}')" rel="tooltip" title="Delete" data-placement="left">
                                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                                </a>
                                                        </span>
                                                        </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>`;
                    if (selected_one.columns.length > 0) {
                        for (var i = 0; i < selected_one.columns.length; i++) {
                            mappping_link3 += `<tr onclick="selectThisProp('ref_class_${i}_${selected_one.columns[i].attribute_id}')" id="ref_class_${i}_${selected_one.columns[i].attribute_id}">
                                        <td>${selected_one.columns[i].attribute_name}</td>
                                        <td>${selected_one.columns[i].attribute_type}</td>
                                        <td>${selected_one.columns[i].attribute_precision}</td>
                                        <td>${selected_one.columns[i].attribute_scale}</td>`;
                            if (!(selected_one.columns[i].express_flag)) {
                                mappping_link3 += '<td><i class="fa fa-check-circle" aria-hidden="true"></i></td>';
                            } else {
                                mappping_link3 += '<td></td>';
                            }
                            if ((selected_one.columns[i].express_flag)) {
                                mappping_link3 += '<td><i class="fa fa-check-circle" aria-hidden="true"></i></td>';
                            } else {
                                mappping_link3 += '<td></td>';
                            }
                            if ((selected_one.columns[i].express_flag)) {
                                var jso = JSON.stringify(selected_one.columns);
                                mappping_link3 += `<td colspan="2" class="stat_classes"><i class="fa fa-play stat_icon_stats" onclick="openExpModal('exp_port',${required_id},'${sindex_arr}',${i},${selected_one.columns[i].attribute_id})" aria-hidden="true"></i>
                                     <span id="stat_${sindex_arr}_${i}_${selected_one.columns[i].attribute_id}"></span>
                                     </td>
                                        </tr>`;
                            } else {
                                mappping_link3 += '<td colspan="2"></td></tr>';
                            }

                        }
                    }

                    mappping_link3 += `</tbody></table></div>`;
                } else {

                    mappping_link3 += `<div id="mappping_link3_read_pro2" style="height:100%;overflow:auto;position:relative;">
                    <table class="display nowrap tablefont table table-striped table-hover" id="read_pro2" style="width: 100%;">
                        <thead>
                            <tr>
                            <th id='column-header-1'>Name<div id='column-header-1-sizer'></div></th>
                            <th id='column-header-2'>Type<div id='column-header-2-sizer'></div></th>
                            <th id='column-header-3'>Precision<div id='column-header-3-sizer'></div></th>
                            <th id='column-header-4'>Scale
                                <span class="pull-right" style="margin-right:0px; opacity: 0.7;">
                                    <a class="tooltip-trigger"  onclick="add_row_table('${sindex_arr}','port_add')" rel="tooltip" title="Add" data-placement="left"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                    <a class="tooltip-trigger"  onclick="edit_table_row('${sindex_arr}','port_edit')" rel="tooltip" title="Edit" data-placement="left"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                                    <a class="tooltip-trigger"  onclick="TableMoveUpDownViewer('${sindex_arr}','up')" rel="tooltip" title="Move up" data-placement="left">
                                        <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                                    </a>
                                    <a class="tooltip-trigger"  onclick="TableMoveUpDownViewer('${sindex_arr}','down')" rel="tooltip" title="Move down" data-placement="left">
                                        <i class="fa fa-arrow-circle-down" aria-hidden="true"></i>
                                    </a>
                                    <a class="tooltip-trigger"  onclick="deleteAllViewer('${sindex_arr}')" rel="tooltip" title="Delete" data-placement="left">
                                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                                    </a>
                                </span>
                                <div id='column-header-4-sizer'></div>
                            </th>
                            </tr>
                        </thead>
                        <tbody>`;
                    if (selected_one.columns.length > 0) {
                        for (var i = 0; i < selected_one.columns.length; i++) {
                            mappping_link3 += `<tr onclick="selectThisProp('ref_class_${i}_${selected_one.columns[i].attribute_id}')" id="ref_class_${i}_${selected_one.columns[i].attribute_id}">
                                        <td>${selected_one.columns[i].attribute_name}</td>
                                        <td>${selected_one.columns[i].attribute_type}</td>
                                        <td>${selected_one.columns[i].attribute_precision}</td>
                                        <td>${selected_one.columns[i].attribute_scale}</td>
                                        </tr>`;
                        }
                    }
                    mappping_link3 += `</tbody></table></div>`;

                }
                mappping_link3 += `</div>`;
                var mappping_link4 = `<div id="show_record_table${required_id}" style="position:relative;height:100%"></div>`;

                $('#map_capss' + mapping_id + ' #link3').html(mappping_link3);
                $('#map_capss' + mapping_id + ' #link4').html(mappping_link4);
                $('.tooltip-trigger').tooltip();
                $('#proper_change_n').val('1');
                $('#proper_change').val('');
                if (type == 'express') {
                    $('#map_capss' + mapping_id + ' #proper_change').attr("onchange", "showRecordButton(this.value,'express','" + required_id + "')");
                }
                if (type == 'output') {
                    $('#map_capss' + mapping_id + ' #proper_change').attr("onchange", "showRecordButton(this.value,'output','" + required_id + "')");
                }
                if (type == 'table') {
                    $('#map_capss' + mapping_id + ' #proper_change').attr("onchange", "showRecordButton(this.value,'" + selected_one.tableName + "','" + required_id + "')");
                }
                $('.data_viewer').selectpicker('refresh')
                generalPortChange(1, mapping_id);

                $('#map_capss' + mapping_id + ' .hgt100 ul li a').each(function(e, s) {
                    if (e == 0) {
                        $(this).addClass('active show')
                    } else {
                        $(this).removeClass('active show')
                    }
                })
                $('#map_capss' + mapping_id + ' .hgt86 div').each(function(e, s) {
                    if (e == 0) {
                        $(this).addClass('active show')
                    } else {
                        $(this).removeClass('active show')
                    }
                })

                $('#map_capss' + mapping_id + ' #link3  #read_pro2').tableHeadFixer({
                    "head": true,
                });
                var thHeight = $('#map_capss' + mapping_id + ' #link3 #read_pro2 th:first').height();
                $('#map_capss' + mapping_id + ' #link3 #read_pro2 th').resizable({
                    handles: "e",
                    minHeight: thHeight,
                    maxHeight: thHeight,
                    minWidth: 40,
                    resize: function(event, ui) {
                        var sizerID = "#" + $(event.target).attr("id") + "-sizer";
                        $(sizerID).width(ui.size.width);
                    }
                });
                if ($("#container_max_" + sindex_arr).css('display').toLowerCase() == 'block') {
                    $('#drag_table_' + sindex_arr + ' .header_title_table').removeClass('trans_box_shadow');
                    $('#drag_table_' + sindex_arr + ' .container_min').removeClass('trans_box_shadow');
                    $("#" + ids + " .tab_icon_hide").hide();
                    window.jsp.repaintEverything();
                } else {
                    $("#" + ids + " .tab_icon_hide").show();
                    window.jsp.repaintEverything();
                }
            } else {
                $("#" + ids + ' .container_min').css({ "height": "88%" });
                $('#' + ids + ' .header_title_table').removeClass('trans_box_shadow');
                $('#' + ids + ' .container_min').removeClass('trans_box_shadow');
                $("#" + ids + " .tab_icon_hide").hide();
                window.jsp.repaintEverything();
            }
        })
    })

    $('body').on('click', '#treeview li label', function() {

        if ($(this).parent().children('i').hasClass('fa-folder')) {
            $.fn.hummingbird.expandSingle($(this).parent().children('i'), 'fa-folder', 'fa-folder-open');
        } else {
            $.fn.hummingbird.collapseSingle($(this).parent().children('i'), 'fa-folder', 'fa-folder-open');
        }

    });

    $('#g_submit').click(function() {
        var tableName = $('#general_name').val();
        var tableId = $('#general_tableId').val();
        var mapping_id = $('#general_mapping_id').val();
        $('#fetch_header_' + tableId + '_' + mapping_id).val(tableName)
        $('.drag_table').trigger('click');
        $('#drag_table_' + tableId + ' .header_title_table .g_sudmit_edit')[0].innerText = tableName;
        var selected = localStorage.getItem("selected");
    })

    $('body').on('click', '#slide_open', function() {
        if ($('.w-h-20').hasClass('width20')) {
            $('.w-h-20').animate({ 'width': '0%' }, 400);
            $('.flex-con-colum').css({ 'width': '100%' });
            $('.w-h-20').removeClass('width20');
            $('.w-h-20').addClass('width0');
            $('#slide_open i').removeClass('fa-caret-left');
            $('#slide_open i').addClass('fa-caret-right');
            if ($('body').hasClass('sidebar-mini')) {
                $('.hide-preview-pane').animate({ "left": "103px" });
                $(".tool-box").animate({ "left": "103px" });
            } else {
                $('.hide-preview-pane').animate({ "left": "282px" });
                $(".tool-box").animate({ "left": "282px" });
            }
        } else {
            $('.w-h-20').animate({ 'width': '20%' }, 400);
            $('.flex-con-colum').animate({ 'width': '80%' });
            $('.w-h-20').removeClass('width0');
            $('.w-h-20').addClass('width20');
            $('#slide_open i').removeClass('fa-caret-right');
            $('#slide_open i').addClass('fa-caret-left');
            if ($('body').hasClass('sidebar-mini')) {
                $('.hide-preview-pane').animate({ "left": "352px" });
                $(".tool-box").animate({ "left": "352px" });
            } else {
                $('.hide-preview-pane').animate({ "left": "492px" });
                $(".tool-box").animate({ "left": "492px" });
            }
        }
        $('#slide_open').toggleClass('add_right_six');
    });
    $('body').on('click', '#overview_resize', function() {
        if ($('.w-h-60').hasClass('height60')) {
            $('.w-h-60').animate({ 'height': '100%' }, 400);
            $('.w-h-40').animate({ 'height': '0%' }, 400);
            $('.w-h-40').hide();
            $('.w-h-60').removeClass('height60');
            $('.w-h-60').addClass('height100');
            $('#overview_resize i').removeClass('fa-expand');
            $('#overview_resize i').addClass('fa-compress');
            $('.preview_table').css({ 'height': '72%' });
        } else {
            $('.w-h-60').animate({ 'height': '60%' }, 400);
            $('.w-h-40').animate({ 'height': '39.6%', "display": "block" }, 400);
            $('.w-h-40').show();
            $('.w-h-60').removeClass('height100');
            $('.w-h-60').addClass('height60');
            $('#overview_resize i').removeClass('fa-compress');
            $('#overview_resize i').addClass('fa-expand');
            $('.preview_table').css({ 'height': '54%' });
        }
    });
    $('body').on('click', '#data_viewer_resize', function() {
        if ($('.w-h-40').hasClass('height40')) {
            $('.w-h-40').animate({ 'height': '100%' }, 400);
            $('.w-h-60').animate({ 'height': '0%' }, 400);
            $('.w-h-40').removeClass('height40');
            $('.w-h-40').addClass('height100');
            $('body .overview_resize').hide();
            $('#data_viewer_resize i').removeClass('fa-expand');
            $('#data_viewer_resize i').addClass('fa-compress');
            $('.height87').css({ 'height': '95%' });

            //$('#mappping_link3_read_pro2').height('100%');
        } else {
            $('.w-h-40').animate({ 'height': '40%' }, 400);
            $('.w-h-60').animate({ 'height': '60%' }, 400);
            $('.w-h-40').removeClass('height100');
            $('.w-h-40').addClass('height40');
            $('body .overview_resize').show();
            $('#data_viewer_resize i').removeClass('fa-compress');
            $('#data_viewer_resize i').addClass('fa-expand');
            $('.height87').css({ 'height': '87%' });
            // $('#mappping_link3_read_pro2').height('180px');
        }

    });
    $('body').on('click', '#ckbCheckAll', function() {
        $(".listCheckBoxClass").each(function() {
            if ($(this).parent().closest('li').css("display") != 'none') {
                $(this).prop('checked', $('#ckbCheckAll').prop('checked'));
            };
        })
        dataTableChecked();
    });
    var selected = JSON.parse(localStorage.getItem("selected"));
    var mapping = JSON.parse(localStorage.getItem("mapping"));
    var project_name = localStorage.getItem("project_name");
    if (selected) {
        $('#pdo').html(project_name);
        $('#pdo').parent().closest('ul').show();
        var len = '';
        for (var i = 0; i < selected.length; i++) {
            len += '<li class="draggable"><i class="fa fa-database tree-file"></i>&nbsp;<label><a href="javascript:void(0)" onclick=overView("' + selected[i].tableAliasName + '",' + i + ')><span class="label-text">' + selected[i].tableAliasName + '</span></a></label></li>'
        };
        //len+='<li class="draggable"><i class="fa fa-database tree-file"></i>&nbsp;<label><a href="javascript:void(0)"><span class="label-text">output</span></a></label></li>';
        $('#select_table_list').html(len);
    }
    if ($('.draggable')) {
        $('.draggable').draggable({ revert: "valid", helper: 'clone', containment: ".content-dq", scroll: false });
    }
});

function exp_func_trigger(function_name) {
    $('#live-search-boxed1').val('');
    $('#live-search-boxed1').trigger('keyup');
    if (function_name == 'DECODE') {
        var appendText = 'DECODE(TRUE,';
        $('#wmd-input').val($('#wmd-input').val() + appendText);

    } else if (function_name == 'ISNULL') {
        var appendText = 'ISNULL(';
        $('#wmd-input').val($('#wmd-input').val() + appendText);
    } else {
        var appendText = function_name + '(';
        $('#wmd-input').val($('#wmd-input').val() + appendText);
    }


}

function finishStra(tableIndex, obj1, obj2, ) {

    $('#stat_' + tableIndex + '_' + obj1 + '_' + obj2).html($('#wmd-input').val());

}

function exp_func_ports(ports) {
    $('#live-search-boxed2').val('');
    $('#live-search-boxed2').trigger('keyup');
    $('#wmd-input').val($('#wmd-input').val() + ports);
}

function openExpModal(id, index, tableIndex, obj1, obj2) {
    $('#vali_ok').prop('disabled', true);
    $('#' + id).modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });

    $("#vali_ok").attr("onclick", "finishStra('" + tableIndex + "','" + obj1 + "','" + obj2 + "')");

    var presentornot = $('#stat_' + tableIndex + '_' + obj1 + '_' + obj2).html();
    if (presentornot != '') {
        $('#wmd-input').val(presentornot)
    } else {
        $('#wmd-input').val('');
    }


    $('#vert-tabs1 ul li:first-of-type a').addClass('active show');
    $('#vert-tabs1 ul li:last-of-type a').removeClass('active show');
    $('#Exp_link1').addClass('active show');
    $('#Exp_link2').removeClass('active show');
    $('#live-search-boxed1').val('');
    $('#live-search-boxed2').val('');



    var selected = JSON.parse(localStorage.getItem('express'));
    var selected_one = selected[index];
    var ports = '';
    for (var i = 0; i < selected_one.columns.length; i++) {
        var lowCase = selected_one.columns[i].attribute_name.toLowerCase();
        ports += `<a href="javascript:void(0);" data-search-term="${lowCase}" class="list-group-item" onclick="exp_func_ports('${selected_one.columns[i].attribute_name}')">${selected_one.columns[i].attribute_name}</a>`;
    }
    $('#express_ports').html(ports);
    getScript('js/data.js', function() {
        var funcion = '';
        for (var i = 0; i < exp_fun_list.length; i++) {
            var lowCase = exp_fun_list[i].toLowerCase();
            if (exp_fun_list[i] == 'DECODE' || exp_fun_list[i] == 'ISNULL' || exp_fun_list[i] == 'OR' || exp_fun_list[i] == 'AND') {

                funcion += `<a data-search-term="${lowCase}" href="javascript:void(0);" onclick="exp_func_trigger('${exp_fun_list[i]}')" class="list-group-item">${exp_fun_list[i]}</a>`;
            } else {
                funcion += `<a data-search-term="${lowCase}" href="javascript:void(0);" onclick="exp_func_trigger('${exp_fun_list[i]}')" class="list-group-item">${exp_fun_list[i]}</a>`;
            }
        }
        $('#express_list_funn').html(funcion);
        $('#live-search-boxed1').on('keyup', function() {
            var searchTerm = $(this).val().toLowerCase();
            $('#express_list_funn a').each(function() {
                if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
        $('#live-search-boxed2').on('keyup', function() {
            var searchTerm = $(this).val().toLowerCase();
            $('#express_ports a').each(function() {
                if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        });
    });
}



function openModal(id) {
    $('#' + id).modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    })

    if (id == 'modalData1') {
        getScript('js/data.js', function() {
            var selected = JSON.parse(localStorage.getItem("selected"));
            var select_table_project = '';
            for (var i = 0; i < selected.length; i++) {
                select_table_project += '<li data-search-term="' + selected[i].tableName.toLowerCase() + '"><div class="form-check"><label class="form-check-label" for="select_' + i + '"><input class="form-check-input listCheckBoxClass" id="select_' + i + '"   type="checkbox" onclick="dataTableChecked();" value="' + selected[i].tableName + '">' + selected[i].tableName + '<span class="form-check-sign"><span class="check"></span></span></label></div></li>';
            };
            $('#multipleSelect').html(select_table_project);
        });
    }
}



function openModal1(id, tableId, mapping_id) {
    $('#' + id).modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    })
    var tableName = $('#fetch_header_' + tableId + '_' + mapping_id).val();
    $('#general_tableId').val(tableId);
    $('#general_mapping_id').val(mapping_id);
    $('#general_name').val(tableName);
    $('#general_descript').val(tableName + '_description');
}


function take_first() {
    var test = $('#take_first:checked').length;
    if (test == 1) {
        $('#preview-first').hide();
        $('#preview-second').show();
    } else {
        $('#preview-first').show();
        $('#preview-second').hide();
    }
}

$('.can-db').click(function() {
    $('#rd_tabs').addClass('active show');
    $('#relation').addClass('active show');
    $('#ff_tabs').removeClass('active show');
    $('#flat-file').removeClass('active show');

    $(".back-class").each(function(index) {
        if ($(this).attr("id") == 'db-se') {
            $(this).show();
        } else {
            $(this).hide();
        }
    });

    $(".ff-back-class").each(function(index) {
        if ($(this).attr("id") == 'ff_fileDetail') {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
    $('#connector-connect').prop('disabled', true);
    $('#list-next').prop('disabled', false);
    $('#ckbCheckAll').prop('checked', false);
    $('#rd_tabs').parent().removeClass('disabled');
    $('#ff_tabs').parent().removeClass('disabled');
    $('#filename').val('');
    $('#descript').val('');

});

function swalalert(i) {
    if (i == 'tools') {
        toastr.error("Tool doesn't support this type of relational database", 'Error');
    }
    if (i == 'toolss') {
        toastr.error("Tool doesn't support this type of Flat File", 'Error');
    }
}

function exp_func_validation() {
    toastr.success("Valid Expression", "Success");
    $('#vali_ok').prop('disabled', false);

}

function connector_validation() {
    // if($('#connector_name').val()==''){
    //     toastr.error('Enter Connector Name','Error');
    //     return false;
    // }
    // else if($('#connector_type').val()==''){
    //     toastr.error('Enter Connector Type','Error');
    //     return false;
    // }
    // else if($('#db-ip').val()==''){
    //     toastr.error('Enter IP Value','Error');
    //     return false;
    // }
    // else if($('#db-port').val()==''){
    //     toastr.error('Enter Port Value','Error');
    //     return false;
    // }
    // else if($('#db-user').val()==''){
    //     toastr.error('Enter  UserName','Error');
    //     return false;
    // }
    // else if($('#db-password').val()==''){
    //     toastr.error('Enter Password','Error');
    //     return false;
    // }
    // else{
    toastr.success("Test Connection Verified", 'Success');
    $('#connector-connect').prop('disabled', false);
    //}
}

function goBackFF(id) {
    if (id == 'ff-table-overview') {
        $('#rd_tabs').parent().addClass('disabled');
    }
    $(".ff-back-class").each(function(index) {
        if ($(this).attr("id") == id) {
            $(this).show();
        } else {
            $(this).hide();
        }
    })
}


function goBack(id) {
    if (id == 'db-id' || id == 'output_sch') {
        $('#ff_tabs').parent().addClass('disabled');
    }
    if (id == 'db-se') {
        $('#ff_tabs').parent().removeClass('disabled');
    }
    if (id != 'connnect-form') {
        $('#connector-connect').prop('disabled', true);
    }
    if (id != 'output_sch') {
        $('#list-next').prop('disabled', true);
    }
    if (id == 'output_sch') {
        $('#live-search-box').val('');
        $('.live-search-list').html('');
        getScript('js/data.js', function() {
            var appendHtml = '';
            for (var i = 0; i < db_source.length; i++) {
                appendHtml += '<li data-search-term="' + db_source[i].tableName.toLowerCase() + '"><div class="form-check"><label class="form-check-label" for="select_' + i + '"><input class="form-check-input listCheckBoxClass" id="select_' + i + '"   type="checkbox" onclick="dataTableChecked();" value="' + db_source[i].tableName + '">' + db_source[i].tableName + '<span class="form-check-sign"><span class="check"></span></span></label></div></li>';
            };
            $('.live-search-list').append(appendHtml);
        })
    }
    if (id == 'edit_table_form') {
        $('#datatable-edit').html("");
        var enable = $('.live-search-list li input').filter(':checked');
        var db_selected_source = [];
        getScript('js/data.js', function() {
            enable.each(function() {
                for (var i = 0; i < db_source.length; i++) {
                    if ($(this)[0].value == db_source[i].tableName) {
                        db_source[i].tableAliasName = db_source[i].tableName
                        db_selected_source.push(db_source[i]);
                    }
                };
            });
            var selected = JSON.parse(localStorage.getItem("selected"));
            if (selected) {
                for (var i = 0; i < db_selected_source.length; i++) {
                    selected.push(db_selected_source[i]);
                    var temp_st = selected.reduce((unique, o) => {
                        if (!unique.some(obj => obj.tableName === o.tableName && obj.tableAliasName === o.tableAliasName)) {
                            unique.push(o);
                        }
                        return unique;
                    }, []);
                }
                localStorage.setItem("selected", JSON.stringify(temp_st));
            } else {
                localStorage.setItem("selected", JSON.stringify(db_selected_source));
            }


            var sel = '<thead><tr><th>Choosen Tables</th><th>Edit Tables</th></tr></thead><tbody>';

            for (var i = 0; i < db_selected_source.length; i++) {
                sel += '<tr><td id="table_sel_' + i + '">' + db_selected_source[i].tableName + '</td><td id="table_edit_' + i + '">' + db_selected_source[i].tableAliasName + '<a onclick="editThis(' + i + ')" href="javascript:void(0);" id="editThis" class="modal-trigger waves-effect btn-flat nopadding pull-right tooltip-trigger" rel="tooltip" title="Edit" data-placement="right"><i class="fa fa-pencil ico-sm-01" aria-hidden="true"></i></a></td></tr>';
            };
            sel += '</tbody></table>';
            $('#datatable-edit').append(sel);
            $('#datatable-edit').tableHeadFixer({
                "head": true,
            });
            $('.tooltip-trigger').tooltip();
        });
    }
    $(".back-class").each(function(index) {
        if ($(this).attr("id") == id) {
            $(this).show();
        } else {
            $(this).hide();
        }
    })
    $('#ckbCheckAll').prop('checked', false);
}

function editThis(i) {
    var selected = JSON.parse(localStorage.getItem("selected"));
    selected = selected[i].tableAliasName;
    $('#table_edit_' + i).html('<input type-"text" id="changeTableId" class="form-control" value="' + selected + '"><a onclick="saveThis(' + i + ')" href="javascript:void(0);" id="saveThis" class="modal-trigger waves-effect btn-flat nopadding pull-right tooltip-trigger" rel="tooltip" title="Edit" data-placement="right" style="margin-top:-18px;"><i class="fa fa-check ico-sm-01" style="color: #00bcd4;" aria-hidden="true"></i></a>');
    $('.tooltip-trigger').tooltip();
    $('#list-finish').prop('disabled', true);
}

function saveThis(i) {
    var val = $('#changeTableId').val();
    $('#table_edit_' + i).html(val + '<a onclick="editThis(' + i + ')" href="javascript:void(0);" id="editThis" class="modal-trigger waves-effect btn-flat nopadding pull-right tooltip-trigger" data-placement="right" rel="tooltip" title="Edit"><i class="fa fa-pencil ico-sm-01" aria-hidden="true"></i></a>');
    $('.tooltip-trigger').tooltip();
    var selected = JSON.parse(localStorage.getItem("selected"));
    selected[i].tableAliasName = val;
    localStorage.setItem("selected", JSON.stringify(selected));
    $('#list-finish').prop('disabled', false);
}

function finishForm() {
    // localStorage.getItem("project_name")
    var selected = JSON.parse(localStorage.getItem("selected"));
    var len = '';
    for (var i = 0; i < selected.length; i++) {
        len += '<li class="draggable"><i class="fa fa-database tree-file"></i>&nbsp;<label><a href="javascript:void(0)" onclick=overView("' + selected[i].tableAliasName + '",' + i + ')><span class="label-text">' + selected[i].tableAliasName + '</span></a></label></li>'
    };
    $('#ff_tabs').parent().removeClass('disabled');
    $('#select_table_list').html(len);
    $('.pdo12').removeClass('fa-folder');
    $('.pdo12').addClass('fa-folder-open');
    $('.draggable').draggable({ revert: "valid", helper: 'clone', containment: ".content-dq", scroll: false });
}


function overView(i, id) {
    var checked = 0;
    var length = $("#nameTabChange .findThis").length;
    var check = $("#nameChange .chip_name").each(function(index) {
        if ($(this).text().trim() == i) {
            checked = 1;
        }
    });
    if (checked == 0) {
        MYLIBRARY.init([{ "table_id": id, "table_name": i }]);
        getScript('js/template.js', function() {
            $('#nameChange').append(lihtml);
            $('#nameContentChange').append(htmlTemplate);
            $('#nameTabChange').append(tabHtmlTemplate);
            $('#example' + id).html(tableHtml);
            $('#example' + id).tableHeadFixer({
                "head": true,
            });
            var thHeight = $('#example' + id + ' th:first').height();
            $('#example' + id + ' th').resizable({
                    handles: "e",
                    minHeight: thHeight,
                    maxHeight: thHeight,
                    minWidth: 40,
                    resize: function(event, ui) {
                        var sizerID = "#" + $(event.target).attr("id") + "-sizer";
                        $(sizerID).width(ui.size.width);
                    }
                })
                //$('#preview_table_example'+id).simplebar();
                // $('#svgappend').css({"width":"0%"});
            $('.data_viewer').selectpicker();
            $("#nameChange .chip").each(function(index) {
                if ($(this).attr("id") == "table_overview" + id) {
                    $(this).addClass('activeConnector');
                } else {
                    $(this).removeClass('activeConnector');
                    $(this).removeClass('activeConnector1');
                }
            })

            $("#nameTabChange .findThis").each(function(index) {
                if ($(this).attr("id") == "capss" + id) {
                    $(this).addClass('active show');
                } else {
                    $(this).removeClass('active show');
                }
            })

            $("#nameContentChange .findThis1").each(function(index) {
                if ($(this).attr("id") == "caps" + id) {
                    $(this).addClass('active show');
                } else {
                    $(this).removeClass('active show');
                }
            })
            if ($("#nameChange .chip").length > 0) {
                $('.w-h-5').addClass('.chip_bottom_line');
            } else {
                $('.w-h-5').removeClass('.chip_bottom_line');
            }

        });

    } else {
        showThisTab(id, 'table');

    }
}

function closeThisTab(closed, id, i) {
    $("#caps" + closed).remove();
    $("#table_overview" + closed).remove();
    $("#capss" + closed).remove();
    if (id) {
        overView(i, id);
    }
}

function closeThisMappingTab(closed) {
    $("#map_capss" + closed).remove();
    $("#mapping_overview" + closed).remove();
    $("#map_caps" + closed).remove();

}

function showThisTab(id, value) {
    if (value == 'table') {
        $("#nameChange .chip").each(function(index) {
            if ($(this).attr("id") == "table_overview" + id) {
                $(this).addClass('activeConnector');
            } else {
                $(this).removeClass('activeConnector');
                $(this).removeClass('activeConnector1');
            }
        })

        $("#nameTabChange .findThis").each(function(index) {
            if ($(this).attr("id") == "capss" + id) {
                $(this).addClass('active show');
            } else {
                $(this).removeClass('active show');
            }
        })

        $("#nameContentChange .findThis1").each(function(index) {
            if ($(this).attr("id") == "caps" + id) {
                $(this).addClass('active show');
            } else {
                $(this).removeClass('active show');
            }
        })
    } else {
        $("#nameChange .chip").each(function(index) {
            if ($(this).attr("id") == "mapping_overview" + id) {
                $(this).addClass('activeConnector1 chipss');
            } else {
                $(this).removeClass('activeConnector');
                $(this).removeClass('activeConnector1');
            }
        });
        $("#nameTabChange .findThis").each(function(index) {
            if ($(this).attr("id") == "map_capss" + id) {
                $(this).addClass('active show');
            } else {
                $(this).removeClass('active show');
            }
        });

        $("#nameContentChange .findThis1").each(function(index) {
            if ($(this).attr("id") == "map_caps" + id) {
                $(this).addClass('active show');
            } else {
                $(this).removeClass('active show');
            }
        });
    }

}


$('#live-search-box').on('keyup', function() {
    var searchTerm = $(this).val().toLowerCase();
    $('.live-search-list li').each(function() {
        if ($(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
});



function dataTableChecked() {
    var enable = $('.live-search-list li input').filter(':checked').length;
    if (enable == 0) {
        $('#list-next').prop('disabled', true);
    } else {
        $('#list-next').prop('disabled', false);
    }
}

function showRecordButton(val, data, index) {
    $('#show_record_table' + index).html('');
    $('.showRecordButton').prop('disabled', false);
    showRecord(data, index, val);
    //$('.show_record_data_viewer').show();


}

function showRecord(data, index, range) {
    if (range == 50) {
        var range1 = 0;
        var range2 = 50;
    }
    if (range == 100) {
        var range1 = 50;
        var range2 = 100;

    }
    if (range == 150) {
        var range1 = 100;
        var range2 = 150;

    }
    if (range == 200) {
        var range1 = 150;
        var range2 = 200;
    }


    if (data == 'express') {
        var selected = JSON.parse(localStorage.getItem('express'))
        selected = selected[index].columns;
        if (selected.length == 6) {
            data = 'test_file_in';
        } else {
            data = 'test_file';
        }

    } else if (data == 'output') {
        var selected = JSON.parse(localStorage.getItem('outputs'))
        selected = selected[index].columns;
        if (selected.length == 6) {
            data = 'test_file_in';
        } else {
            data = 'test_file';
        }

    } else {
        var selected = JSON.parse(localStorage.getItem("selected"));
        selected = selected[index].columns;

    }
    getScript('js/' + data + '.js', function() {
        var showTableRecord = '';
        showTableRecord += '<table id="show_record_table_index' + index + '" class="table table-striped table-hover my-custom-table viewer-table" ><thead><tr>';
        for (var i = 0; i < selected.length; i++) {
            showTableRecord += '<th>' + selected[i].attribute_name + '</th>'
        };
        showTableRecord += '</tr></thead><tbody>';

        val.forEach(function(item, s) {
            if ((s > range1) && (s < range2)) {
                showTableRecord += '<tr>';
                for (var i = 0; i < selected.length; i++) {
                    Object.keys(item).forEach(function(key) {
                        if (selected[i].attribute_name == key) {
                            showTableRecord += '<td>' + item[key] + '</td>';
                        }
                    });
                }
                showTableRecord += '</tr>';
            }
        });
        showTableRecord += '</tbody></table>';
        $('#show_record_table' + index).html(showTableRecord);
        $('#show_record_table_index' + index).tableHeadFixer({
            "head": true,
        });
    });
}


// cleaner function start

function close_table_fn(id) {
    $('.' + id).remove();
}

function table_list_creation() {
    var enable = $('.live-search-list li input').filter(':checked');
    var cleaner_table_source = [];
    getScript('js/data.js', function() {
        enable.each(function() {
            for (var i = 0; i < cleaner_source.length; i++) {
                if ($(this)[0].value == cleaner_source[i].tableName) {
                    cleaner_source[i].tableAliasName = cleaner_source[i].tableName
                    cleaner_table_source.push(cleaner_source[i]);
                }
            };
        });
        localStorage.setItem("selected", JSON.stringify(cleaner_table_source));
        var len = '';
        for (var i = 0; i < cleaner_table_source.length; i++) {
            len += '<li class="draggable"><i class="fa fa-database tree-file"></i>&nbsp;<label><a href="javascript:void(0)" onclick=overView("' + cleaner_table_source[i].tableAliasName + '",' + i + ')><span class="label-text">' + cleaner_table_source[i].tableAliasName + '</span></a></label></li>'
        };
        len += `<li data-id="1" id="change_id_ouput">
                <i class="fa fa-folder fa-minus"></i>
                <label>
                    <span class="label-text">output</span>
                </label>
                <a href="javascript:void(0);" rel="tooltip" data-placement="right" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" data-toggle="modal" data-placement="top" onclick="openModal('out_mod1');"  title="Add Error Table" >
                    <span class="material-icons tree-icon ">add</span>
                </a>
            <ul id="output_trees" style="display: none;"></ul></li>`;
        $('#select_table_list1').html(len);
        $('.tooltip-trigger').tooltip();
        $('.draggable').draggable({ revert: "valid", helper: 'clone', containment: ".content-dq", scroll: false });

    });
    $('#ckbCheckAll').prop('checked', false);
    $('.pdo1').removeClass('fa-folder');
    $('.pdo1').addClass('fa-folder-open');


}




function finish_project_creation() {
    $('#pdo').html($('#filename').val());
    localStorage.setItem("project_name", $('#filename').val());
    $('#pdo').parent().closest('ul').show();
    $('.pdo').removeClass('fa-folder');
    $('.pdo').addClass('fa-folder-open');
    $('#filename').val('');
    $('#descript').val('');

}

function finish_mapping_creation() {
    var len = $('#mapping_list li').length;
    var mapping_name = $('#mname').val();
    var patt = /^[a-zA-Z0-9_]+$/;
    var flag = patt.test(mapping_name);
    if (flag) {
        if (JSON.parse(localStorage.getItem("mapping"))) {
            var mapping = JSON.parse(localStorage.getItem("mapping"));
        } else {
            var mapping = [];
        }
        var temp = {
            "mapping_id": len,
            "mapping_name": mapping_name,
            "mapping_des": $('#mdescription').val(),
            "transformation": "",
        }
        mapping.push(temp);

        localStorage.setItem("mapping", JSON.stringify(mapping));

        $('#mapping_list').append('<li><img src="images/mapping.png" alt="mapping" class="map_ico">&nbsp;<label><a href="javascript:void(0)" onclick=mapping_overview("' + mapping_name + '",' + len + ')><span class="label-text">' + mapping_name + '</span></a></label></li>');
        $('#mapping_list').parent().closest('ul').show();
        $('#mname').val('');
        $('#mdescription').val('');
        $('.pdo2').removeClass('fa-folder');
        $('.pdo2').addClass('fa-folder-open');
    } else {
        toastr.error('Mapping name should only contain underscores(_).', 'Error')
    }


}

function outputTable() {
    // $('#mod_write').modal({
    //     show:true,
    //     backdrop: 'static',
    //     keyboard: false
    // })
    var error_table = $('#error_table_name').val();
    $('#mod_write h5').text(error_table);

    table = $('#mod_write h5').text();
    $('#change_id_ouput i').removeClass('fa-folder');
    $('#change_id_ouput i').addClass('fa-folder-open');
    $('#output_trees').append('<li class="draggable"><i class="fa fa-database tree-file"></i>&nbsp;<label><a href="javascript:void(0)"><span class="label-text">' + table + '</span></a></label></li>');
    $('#output_trees').show();
    if (localStorage.getItem('outputs')) {
        var temp = {
            "columns": [],
            "tableAliasName": table,
            "tableName": table,
            "tableNameDescription": ""
        }
        var selected = localStorage.getItem('outputs');
        selected.push(temp);
        localStorage.setItem("outputs", JSON.stringify(selected));
    } else {
        var temp = [{
            "columns": [],
            "tableAliasName": table,
            "tableName": table,
            "tableNameDescription": ""
        }];
        localStorage.setItem("outputs", JSON.stringify(temp));
    }
    $('.draggable').draggable({ revert: "valid", helper: 'clone', containment: ".content-dq", scroll: false });
    $('#error_table_name').val('');
    $('#error_table_des').val('');

}

function mapping_overview(name, id) {
    var checked = 0;
    var check = $("#nameChange .chip_name").each(function(index) {
        if ($(this).text().trim() == name) {
            checked = 1;
        }
    });
    if (checked == 0) {
        MYLIBRARY.init([{ "mapping_id": id, "mapping_name": name }]);
        getScript('js/template.js', function() {
            $('#nameChange').append(mappingTab);
            $('#nameContentChange').append(mappingHtmlTemplate);
            $('#nameTabChange').append(mappingTabHtmlTemplate);
            //$('#svgappend').append(svgtemplete);
            $('.data_viewer').selectpicker();
            $('.tooltip-trigger').tooltip();
            $("#nameChange .chip").each(function(index) {
                if ($(this).attr("id") == "mapping_overview" + id) {
                    $(this).addClass('activeConnector1 chipss');
                } else {
                    $(this).removeClass('activeConnector');
                    $(this).removeClass('activeConnector1');
                }
            });
            $("#nameTabChange .findThis").each(function(index) {
                if ($(this).attr("id") == "map_capss" + id) {
                    $(this).addClass('active show');
                } else {
                    $(this).removeClass('active show');
                }
            });

            $("#nameContentChange .findThis1").each(function(index) {
                if ($(this).attr("id") == "map_caps" + id) {
                    $(this).addClass('active show');
                } else {
                    $(this).removeClass('active show');
                }
            });
            if ($('body').hasClass('sidebar-mini')) {
                $('.hide-preview-pane').animate({ "left": "352px" });
                $(".tool-box").animate({ "left": "352px" });
            } else {
                $('.hide-preview-pane').animate({ "left": "495px" });
                $(".tool-box").animate({ "left": "495px" });
            }
            $('.jtk-demo-canvas').droppable({
                drop: function(event, ui) {
                    if (ui.draggable[0].tagName == 'LI') {
                        $('#modalData3_modal_title').text(ui.draggable[0].innerText.trim());
                        var table = ui.draggable[0].innerText.trim();
                        var leftPosition = ui.offset.left - $(this).offset().left;
                        var topPosition = ui.offset.top - $(this).offset().top;
                        if (ui.draggable.parent().attr('id') == 'output_trees') {
                            $('#mod_write').modal({
                                show: true,
                                backdrop: 'static',
                                keyboard: false
                            })
                            $("#modalData3_exampleRadios2").attr("onclick", "readorwrite('Write','" + table + "','" + leftPosition + "','" + topPosition + "','" + id + "','output')");
                        } else {
                            $('#modalData3').modal({
                                show: true,
                                backdrop: 'static',
                                keyboard: false
                            })
                            $("#modalData3_exampleRadios1").attr("onclick", "readorwrite('Read','" + table + "','" + leftPosition + "','" + topPosition + "','" + id + "','table')");
                        }
                    }
                }
            });
            var instance = jsPlumb.getInstance({
                Connector: "Straight",
                PaintStyle: { strokeWidth: 1, stroke: "#00bcd4", "dashstyle": "2 4" },
                Endpoint: ["Rectangle", { width: 5, height: 5 }],
                EndpointStyle: { fill: "#ffa500" },
                Container: "canvas"
            });
            window.jsp = instance;
            window.jsp.setContainer('canvas')
        });
    } else {
        showThisTab(id, 'mapping');
    }

}

function readorwrite(value, table, left, top, mapping_id, property) {
    var connection = [];
    create_table(table, left, top, connection, value, mapping_id, property);
}

function create_table(table, leftPosition, topPosition, connection, read_or_write, mapping_id, property) {
    if (property == 'table') {
        create_table_table(table, leftPosition, topPosition, connection, read_or_write, mapping_id, property);
    }
    if (property == 'output') {
        create_table_ouput(table, leftPosition, topPosition, connection, read_or_write, mapping_id, property);
    }
    if (property == 'express') {
        $('#hide_pane').trigger('click');

        if (localStorage.getItem('express')) {
            var temp = {
                "columns": [],
                "tableAliasName": "Exp_" + localStorage.getItem('express').length,
                "tableName": "Exp_" + localStorage.getItem('express').length,
                "tableNameDescription": ""
            }
            var selected = JSON.parse(localStorage.getItem('express'));
            selected.push(temp);
            localStorage.setItem("express", JSON.stringify(selected));
        } else {
            var temp = [{
                "columns": [],
                "tableAliasName": "Exp_",
                "tableName": "Exp_",
                "tableNameDescription": ""
            }];
            localStorage.setItem("express", JSON.stringify(temp));
        }
        create_table_ouput(table, leftPosition, topPosition, connection, read_or_write, mapping_id, property);
    }
}

function create_table_table(table, leftPosition, topPosition, connection, read_or_write, mapping_id, property) {
    MYLIBRARY.init([{
        "ctable_name": table,
        "topPosition": topPosition,
        "leftPosition": leftPosition,
        "read_or_write": read_or_write,
        "mapping_id": mapping_id,
        "property": property
    }]);
    getScript('js/template.js', function() {
        $('#map_caps' + mapping_id + ' .jtk-demo-canvas').append(cleanerTable);
        $('#dateTable_' + sindex_arr + '_' + mapping_id).tableHeadFixer({
            "head": true,
            "left": 1,
            "right": 1,
            "z-index": 4
        });
        $('#drag_table_' + sindex_arr).resizable({
            "minWidth": 260,
            "minHeight": 220
        });

        $('.tooltip-trigger').tooltip();
        var list1El = document.querySelector("#drag_table_" + sindex_arr);
        var list1Ul = list1El.querySelector("tbody");
        window.jsp.draggable(list1El, {
            containment: "#canvas",
            scrollSensitivity: 100,
            scrollSpeed: 100,
            scroll: true,
            drag: function() {
                window.jsp.repaintEverything();
            }
        });

        $("#drag_table_" + sindex_arr + " tbody tr").draggable({
            revert: "valid",
            helper: 'clone',
            cursor: 'move',
            scroll: false,
            containment: '#canvas',
        });

        $('.drag_table').trigger('click');
        var lists = jsPlumb.getSelector("#drag_table_" + sindex_arr + " tbody");

        window.jsp.batch(function() {
            for (var l = 0; l < lists.length; l++) {
                var isSource = lists[l].getAttribute("source") != null,
                    isTarget = lists[l].getAttribute("target") != null;

                var items = lists[l].querySelectorAll("tr td:first-of-type");

                for (var i = 0; i < items.length; i++) {
                    if (isSource) {
                        window.jsp.makeSource(items[i], {
                            allowLoopback: true,
                            anchor: ["Left", "Right"]
                        });
                    }

                    if (isTarget) {
                        window.jsp.makeTarget(items[i], {
                            anchor: ["Left", "Right"]
                        });
                    }
                }
                var items = lists[l].querySelectorAll("tr td:last-of-type");
                for (var i = 0; i < items.length; i++) {
                    if (isSource) {
                        window.jsp.makeSource(items[i], {
                            allowLoopback: true,
                            anchor: ["Left", "Right"]
                        });
                    }

                    if (isTarget) {
                        window.jsp.makeTarget(items[i], {
                            anchor: ["Left", "Right"]
                        });
                    }
                }
            }
            var lists1 = jsPlumb.getSelector('#drag_table_' + sindex_arr + ' .container_max span');
            window.jsp.makeSource(lists1[0], {
                anchor: ["Left", "Right"]
            });
            window.jsp.makeTarget(lists1[0], {
                anchor: ["Left", "Right"]
            });
        });

        var addListData = document.querySelector('#drag_table_' + sindex_arr + ' .container_min');
        var temp_list = window.jsp.addList(addListData);

        if (connection.length > 0) {
            for (var i = 0; i < connection.length; i++) {
                var source = $("[data-id=" + connection[i].source + "]").attr("id");
                var target = $("[data-id=" + connection[i].target + "]").attr("id");
                if (source && target) {
                    window.jsp.connect({ source: source, target: target })
                    window.jsp.repaintEverything();
                }
            }
        }

        var thHeight = $('#dateTable_' + sindex_arr + '_' + mapping_id + ' th:first').height();
        $('#dateTable_' + sindex_arr + '_' + mapping_id + ' th').resizable({
            handles: "e",
            minHeight: thHeight,
            maxHeight: thHeight,
            minWidth: 40,
            resize: function(event, ui) {
                var sizerID = "#" + $(event.target).attr("id") + "-sizer";
                $(sizerID).width(ui.size.width);
            }
        });

        $('.drag_table').each(function() {
            var temps = $(this).attr("id");
            //var need_id=temps.slice(temps.length-1);
            var list2El = document.querySelector("#" + temps);
            var list2Ul = document.querySelector('#' + temps + ' .container_min');
            var temp_list = window.jsp.addList(list2Ul);
            window.jsp.repaintEverything();
        });

        $('#drag_table_' + sindex_arr + ' .container_min').scroll(function() {
            window.jsp.repaintEverything();
        });
    });

}

function create_table_ouput(table, leftPosition, topPosition, connection, read_or_write, mapping_id, property) {
    MYLIBRARY.init([{
        "ctable_name": table,
        "topPosition": topPosition,
        "leftPosition": leftPosition,
        "read_or_write": read_or_write,
        "mapping_id": mapping_id,
        "property": property
    }]);
    getScript('js/template.js', function() {
        $('#map_caps' + mapping_id + ' .jtk-demo-canvas').append(cleanerTable);
        $('#dateTable_' + sindex_arr + '_' + mapping_id).tableHeadFixer({
            "head": true,
            "left": 1,
            "right": 1,
            "z-index": 4
        });

        $('#drag_table_' + sindex_arr).resizable({
            "minWidth": 260,
            "minHeight": 220
        });

        $('.tooltip-trigger').tooltip();
        var list1El = document.querySelector("#drag_table_" + sindex_arr);
        var list1Ul = list1El.querySelector("tbody");
        window.jsp.draggable(list1El, {
            containment: "#canvas",
            scrollSensitivity: 100,
            scrollSpeed: 100,
            scroll: true,
            drag: function() {
                window.jsp.repaintEverything();
            }
        });

        $('.drag_table').trigger('click');

        var thHeight = $('#dateTable_' + sindex_arr + '_' + mapping_id + ' th:first').height();
        $('#dateTable_' + sindex_arr + '_' + mapping_id + ' th').resizable({
            handles: "e",
            minHeight: thHeight,
            maxHeight: thHeight,
            minWidth: 40,
            resize: function(event, ui) {
                var sizerID = "#" + $(event.target).attr("id") + "-sizer";
                $(sizerID).width(ui.size.width);
            }
        });

        $('#drag_table_' + sindex_arr + ' .container_min').scroll(function() {
            window.jsp.repaintEverything();
        });
        var lists1 = jsPlumb.getSelector('#drag_table_' + sindex_arr + ' .container_max span');
        window.jsp.makeSource(lists1[0], {
            anchor: ["Left", "Right"]
        });
        window.jsp.makeTarget(lists1[0], {
            anchor: ["Left", "Right"]
        });
    });
}

function maximizeThis(tableIndex) {

    $('#drag_table_' + tableIndex + ' .ui-resizable-handle').show();

    $('.drag_table').each(function() {
        var temps = $(this).attr("id");
        var list2El = document.querySelector("#" + temps);
        var list2Ul = document.querySelector('#' + temps + ' .container_min');
        window.jsp.removeList(list2Ul);
    });

    var connected = window.jsp.getConnections();
    var connection1 = [];
    $.each(connected, function(e, s) {
        var temp_conn = {
            'source': s.sourceId,
            'target': s.targetId
        };
        connection1.push(temp_conn);
    });

    $('#drag_table_' + tableIndex + ' .container_min').show();
    $('#drag_table_' + tableIndex + ' .tab_icon_hide').show();
    $('#drag_table_' + tableIndex + ' .header_title_table').show();
    $('#drag_table_' + tableIndex + ' .container_max').hide();

    $('#drag_table_' + tableIndex).css({
        "width": "260",
        "height": "220"
    })



    var all_mini = [];
    $('.container_max').each(function() {
        if ($(this).css("display") == 'block') {
            var ids = $(this).attr("id");
            var img_ids = $('#' + ids + ' span').attr("id");
            all_mini.push({ "tableindexid": ids, "img_ids": img_ids });
        }
    })

    var connection_soruce = []
        // if($('#drag_table_'+tableIndex).attr('data-type')=='table'){
    all_mini.forEach((ele, index) => {
            var taids = ele.tableindexid.split('_');
            // if(taids[2].length == 1){
            //     var taids1=ele.tableindexid.slice(ele.tableindexid.length-1);
            //     $('#drag_table_'+taids+' .container_min tbody tr td').each(function(){
            //         connection_soruce.push($(this).attr("id"));
            //     })
            // }
            // else{
            //var taids1=ele.tableindexid.split('_');
            $('#drag_table_' + taids[2] + ' .container_min tbody tr td').each(function() {
                connection_soruce.push($(this).attr("id"));
            });
            // }
        })
        // }
        // else{
        //     all_mini.forEach((ele,index)=>{
        //         //var taids=ele.tableindexid.split('_');
        //         var taids=ele.tableindexid.slice(ele.tableindexid.length-1);
        //         $('#drag_table_'+taids+' .container_min tbody tr td').each(function(){
        //             connection_soruce.push($(this).attr("id"));
        //         });
        //     })
        // }

    var connection = JSON.parse(localStorage.getItem("connection"));

    connection.forEach((ele, index) => {
        connection_soruce.forEach((ele1, index1) => {
            if (ele.source == ele1) {
                ele1 = $('#' + ele1).attr("data-id");
                ele1 = ele1.split("_");
                var temp_value = $('#container_max_' + ele1[2] + ' span').attr("id");
                connection[index].source = temp_value;
            }
            if (ele.target == ele1) {
                ele1 = $('#' + ele1).attr("data-id");
                ele1 = ele1.split("_");
                var temp_value = $('#container_max_' + ele1[2] + ' span').attr("id");
                connection[index].target = temp_value;
            }
        });
    });

    var sourceId = $('#container_max_' + tableIndex + ' span').attr("id");
    delete_connection(sourceId);

    connection.forEach((ele, index) => {
        window.jsp.connect({ source: ele.source, target: ele.target })
        window.jsp.repaintEverything();
    })

    $('.drag_table').each(function() {
        var temps = $(this).attr("id");
        var list2El = document.querySelector("#" + temps);
        var list2Ul = document.querySelector('#' + temps + ' .container_min');
        window.jsp.addList(list2Ul);
    });
}

function delete_connection(source) {
    window.jsp.deleteConnectionsForElement($("#" + source));
    window.jsp.repaintEverything();
}

function minimizeThis(tableIndex) {
    $('#drag_table_' + tableIndex + ' .ui-resizable-handle').hide();
    var connection_soruce = []
    $('#drag_table_' + tableIndex + ' .container_min tbody tr td').each(function() {
        connection_soruce.push($(this).attr("id"));
    });

    $('.drag_table').each(function() {
        var temps = $(this).attr("id");
        var list2El = document.querySelector("#" + temps);
        var list2Ul = document.querySelector('#' + temps + ' .container_min');
        window.jsp.removeList(list2Ul);
    });

    var connected = window.jsp.getConnections();
    var connection1 = [];
    $.each(connected, function(e, s) {
        var temp_conn = {
            'source': s.sourceId,
            'target': s.targetId
        };
        connection1.push(temp_conn);
    });

    $('#drag_table_' + tableIndex + ' .container_min').hide();
    $('#drag_table_' + tableIndex + ' .tab_icon_hide').hide();
    $('#drag_table_' + tableIndex + ' .header_title_table').hide();
    $('#drag_table_' + tableIndex + ' .container_max').show();
    $('#drag_table_' + tableIndex).css({
        "width": "36",
        "height": "36"
    })

    var all_mini = [];
    $('.container_max').each(function() {
        if ($(this).css("display") == 'block') {
            var ids = $(this).attr("id");
            var img_ids = $('#' + ids + ' span').attr("id");
            all_mini.push({ "tableindexid": ids, "img_ids": img_ids });
        }
    })

    var sourceId = '';
    //if($('#drag_table_'+tableIndex).attr('data-type')=='table'){
    all_mini.forEach((ele, index) => {
        var temp = ele.tableindexid.split('_');
        if (temp[2] == tableIndex) {
            sourceId = ele.img_ids;
        }
    });
    //}
    // else{
    //     all_mini.forEach((ele,index)=>{
    //         var temp=ele.tableindexid.split('_');
    //         if(temp[2] == tableIndex){
    //             sourceId=ele.img_ids;
    //         }
    //     });
    // }
    connection1.forEach((ele, index) => {
        connection_soruce.forEach((ele1, index1) => {
            if (ele.source == ele1) {
                delete_connection(connection1[index].source);
                connection1[index].source = sourceId;
            }
            if (ele.target == ele1) {
                delete_connection(connection1[index].target);
                connection1[index].target = sourceId;
            }
        });
    });

    var connection = [];
    $.each(connected, function(e, s) {
        var temp_conn = {
            'source': s.sourceId,
            'target': s.targetId
        };
        connection.push(temp_conn);
    });

    var temp_st = localStorage.getItem("connection");

    if (temp_st == null) {
        localStorage.setItem("connection", JSON.stringify(connection));
    } else {
        temp_st = JSON.parse(temp_st);
        connection.forEach((ele, index) => {
            all_mini.forEach((ele1, index1) => {
                var sdataIds = $('#' + ele.source).attr("data-id");
                var tdataIds = $('#' + ele.target).attr("data-id");
                if (tdataIds.slice(tdataIds.length - 1) != 'M' && sdataIds.slice(sdataIds.length - 1) != 'M') {
                    temp_st.push(connection[index]);
                }
            });
        });
        var temp_st = temp_st.reduce((unique, o) => {
            if (!unique.some(obj => obj.source === o.source && obj.target === o.target)) {
                unique.push(o);
            }
            return unique;
        }, []);
        localStorage.setItem("connection", JSON.stringify(temp_st));
    }
    connection1.forEach((ele, index) => {
        window.jsp.connect({ source: ele.source, target: ele.target })
        window.jsp.repaintEverything();
    });

    $('.drag_table').each(function() {
        var temps = $(this).attr("id");
        var list2El = document.querySelector("#" + temps);
        var list2Ul = document.querySelector("#" + temps + ' .container_min');
        window.jsp.addList(list2Ul);
    });
}

function close_table(argument) {
    $('#confirmMessageID').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });

    $("#confirmation_ok").attr("onclick", "confirmCloseTable('" + argument + "')");
    $("#confirmation_cancal").attr("onclick", "confirmCloseTableDismiss()");


    if ($('#drag_table_' + argument).attr('data-type') == 'output') {
        localStorage.removeItem("outputs");

    }
    if ($('#drag_table_' + argument).attr('data-type') == 'express') {
        localStorage.removeItem("express");
    }
}

function confirmCloseTableDismiss() {
    $('#confirmMessageID').modal('hide');
}

function confirmCloseTable(argument) {
    $('#drag_table_' + argument + ' tbody tr td').each(function() {
        var del = $(this).attr("id");
        $('.drag_table').each(function() {
            var temps = $(this).attr("id");
            var list2El = document.querySelector("#" + temps);
            var list2Ul = document.querySelector('#' + temps + ' .container_min');
            window.jsp.removeList(list2Ul);
        });
        var connected = window.jsp.getConnections();
        $.each(connected, function(e, s) {
            if (s.source != null) {
                if (del == s.source.id) {
                    window.jsp.deleteConnection(s);
                }
            }
            if (s.target != null) {
                if (del == s.target.id) {
                    window.jsp.deleteConnection(s);
                }
            }
        });
        $('.drag_table').each(function() {
            var temps = $(this).attr("id");
            var list2El = document.querySelector("#" + temps);
            var list2Ul = document.querySelector('#' + temps + ' .container_min');
            window.jsp.addList(list2Ul);
        });
    });

    $('#drag_table_' + argument).remove();
    toastr.success('Your Mapping Table has been deleted.', "Success");

}

function add_row_table(id, modelData) {
    $('#new_name').val('');
    $('#new_type').val('');
    $('#new_precision').val('');
    $('#new_scale').val('');
    if (modelData == '') {
        if (!($('#drag_table_' + id + ' div').hasClass("new_row_" + id))) {
            $('#drag_table_' + id).append('<div class="container sbg-01 new_row_' + id + '"><div class="second-layer"><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="hidden" name="new_id" id="new_id" value="empty"><input type="text" class="form-control" name="new_name" id="new_name" placeholder="Name"><div class="line"></div></div></div><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="text" class="form-control" name="new_type" id="new_type"  placeholder="Type"><div class="line"></div></div></div><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="text" class="form-control" name="new_precision" id="new_precision"  placeholder="Precision"><div class="line"></div></div></div><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="text" class="form-control" name="new_scale" id="new_scale" placeholder="Scale"><div class="line"></div></div></div><div class="icon-pos-03"><a onclick="save_add_new_row(' + "'" + id + "'" + ',false)"><span style="padding-right:3px"><i class="fa fa-floppy-o circle-ico" aria-hidden="true" ></i></span></a><a onclick="del_add_new_row(' + "'" + id + "'" + ')"><span><i class="fa fa-times circle-ico" aria-hidden="true" ></i></span></a></div></div></div>');
            window.jsp.repaintEverything();
        } else {
            $('#new_name').focus();
            toastr.warning('Enter value in respective column(s)', Warning)
        }
    } else {
        $('#' + modelData).modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
        $("#" + modelData + " #port_submit").attr("onclick", "save_add_new_row('" + id + "',false)");

    }
}

function save_add_new_row(id, check) {
    var left = $('#drag_table_' + id).offset().left - $('.jtk-page-container').offset().left;
    var top = $('#drag_table_' + id).offset().top - $('.jtk-page-container').offset().top;
    if ($('#drag_table_' + id).attr('data-type') == 'table') {
        var express_flag = "";
        var index = id;
        var ouputHtml = '';
        var valueChanged = JSON.parse(localStorage.getItem("selected"));
    } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
        var express_flag = "";
        var ouputHtml = '';
        var index = id.slice(id.length - 1);
        var valueChanged = JSON.parse(localStorage.getItem("outputs"));
    } else {
        if ($('#drag_table_' + id + ' .container_min table tbody #check_output_present').length == 0) {
            var ouputHtml = `<tr id="check_output_present"  class="donotSelectThis">
                    <td></td>
                    <td>Output</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td></td>
                  </tr>
        `;

        } else {
            var ouputHtml = '';
        }
        var express_flag = 1
        var index = id.slice(id.length - 1);
        var valueChanged = JSON.parse(localStorage.getItem("express"));
    }
    if (check) {
        var name = $('#port_edit #new_name').val();
        var type = $('#port_edit #new_type').val();
        var precision = $('#port_edit #new_precision').val();
        var scale = $('#port_edit #new_scale').val();
        var new_id = $('#port_edit #new_id').val();
    } else {
        var name = $('#new_name').val();
        var type = $('#new_type').val();
        var precision = $('#new_precision').val();
        var scale = $('#new_scale').val();
        var new_id = $('#new_id').val();
    }

    if (name != '' && type != '') {

        var mapping_id = $('#drag_table_' + id).attr("data-id");
        if (new_id == 'empty') {
            var new_id1 = parseInt(valueChanged[index].columns.length + 1)
            var new_value = {
                "attribute_id": parseInt(valueChanged[index].columns.length + 1),
                "attribute_alias": name,
                "attribute_name": name,
                "attribute_type": type,
                "attribute_precision": precision,
                "attribute_scale": scale,
                "express_flag": express_flag
            }
            valueChanged[index].columns.push(new_value);
            if (valueChanged[index].columns.length == 1) {
                var appendHtmlVal = `<table id="dateTable_${id}_${mapping_id}" class="display nowrap tablefont">
                <thead>
                  <tr>
                    <th></th>
                    <th id='column-header-1'>Name<div id='column-header-1-sizer'></div></th>
                    <th id='column-header-2'>Type<div id='column-header-2-sizer'></div></th>
                    <th id='column-header-3'>Precision<div id='column-header-3-sizer'></div></th>
                    <th id='column-header-4'>Scale<div id='column-header-4-sizer'></div></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody source target>${ouputHtml}`;
                var listeleL = 'ref_class_' + id + '_' + new_id1 + '_L';
                var listeleR = 'ref_class_' + id + '_' + new_id1 + '_R';
                appendHtmlVal += `<tr onclick="selectThis('ref_class_${id}_${new_id1}')" data-id="ref_class_${id}_${new_id1}">
                                <td data-id="ref_class_${id}_${new_id1}_L"><img src="images/connector.png" width="10" height="10">
                                </td>
                                <td>${name}</td>
                                <td>${type}</td>
                                <td>${precision}</td>
                                <td>${scale}</td>
                                <td data-id="ref_class_${id}_${new_id1}_R"><img src="images/connector.png" width="10" height="10">
                                </td>
                            </tr></tbody></table>`;
                $('#drag_table_' + id + ' .container_min').html(appendHtmlVal);
                var addListData = document.querySelector('#drag_table_' + id + ' .container_min');
                var temp_list = window.jsp.addList(addListData);
            } else {
                var listeleL = 'ref_class_' + id + '_' + new_id1 + '_L';
                var listeleR = 'ref_class_' + id + '_' + new_id1 + '_R';
                var appendHtmlVal = `${ouputHtml}<tr onclick="selectThis('ref_class_${id}_${new_id1}')" data-id="ref_class_${id}_${new_id1}">
                                <td data-id="ref_class_${id}_${new_id1}_L"><img src="images/connector.png" width="10" height="10">
                                </td>
                                <td>${name}</td>
                                <td>${type}</td>
                                <td>${precision}</td>
                                <td>${scale}</td>
                                <td data-id="ref_class_${id}_${new_id1}_R"><img src="images/connector.png" width="10" height="10">
                                </td>
                            </tr>`;
                $('#dateTable_' + id + '_' + mapping_id + ' tbody tr:last').after(appendHtmlVal);
            }
            window.jsp.makeSource($("[data-id=" + listeleL + "]"), { anchor: ["Left", "Right"] });
            window.jsp.makeSource($("[data-id=" + listeleR + "]"), { anchor: ["Left", "Right"] });
            window.jsp.makeTarget($("[data-id=" + listeleL + "]"), { anchor: ["Left", "Right"] });
            window.jsp.makeTarget($("[data-id=" + listeleR + "]"), { anchor: ["Left", "Right"] });

            $('#dateTable_' + id + '_' + mapping_id).tableHeadFixer({
                "head": true,
                "left": 1,
                "right": 1,
                "z-index": 4
            });

            if ($('#drag_table_' + id).attr('data-type') == 'table') {
                localStorage.setItem("selected", JSON.stringify(valueChanged));
            } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
                localStorage.setItem("outputs", JSON.stringify(valueChanged));
            } else {
                localStorage.setItem("express", JSON.stringify(valueChanged));
            }

            toastr.success('Added!', 'Success')
        } else {
            valueChanged[index].columns.forEach(function(s, e) {
                if (s.attribute_id == new_id) {
                    var new_value = {
                        "attribute_id": new_id,
                        "attribute_alias": valueChanged[index].attribute_name,
                        "attribute_name": name,
                        "attribute_type": type,
                        "attribute_precision": precision,
                        "attribute_scale": scale,
                        "express_flag": express_flag
                    }
                    delete valueChanged[index].columns[e];
                    valueChanged[index].columns[e] = new_value
                }

            })
            var dataid = 'ref_class_' + id + '_' + new_id;
            if ($('#drag_table_' + id).attr('data-type') == 'table') {
                localStorage.setItem("selected", JSON.stringify(valueChanged));
            } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
                localStorage.setItem("outputs", JSON.stringify(valueChanged));
            } else {
                localStorage.setItem("express", JSON.stringify(valueChanged));
            }

            $("[data-id=" + dataid + "]").find("td:eq(1)").text(name);
            $("[data-id=" + dataid + "]").find("td:eq(2)").text(type);
            $("[data-id=" + dataid + "]").find("td:eq(3)").text(precision);
            $("[data-id=" + dataid + "]").find("td:eq(4)").text(scale);
            toastr.success('Updated!', 'Success')
        }


        del_add_new_row(id)
    } else {
        toastr.warning('Fill the column first!', 'Warning');
    }
    $("#mappping_link3_read_pro2").load(location.href + " #mappping_link3_read_pro2");
    $('.drag_table').trigger('click');
}

function del_add_new_row(id) {
    $('.new_row_' + id).remove();
    window.jsp.repaintEverything();
}

function edit_table_row(id, modelData) {

    var edit_dataid_source = []
    if ($('#drag_table_' + id).attr('data-type') == 'table') {
        var index = id;
        var valueChanged = JSON.parse(localStorage.getItem("selected"));
    } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
        var index = id.slice(id.length - 1);
        var valueChanged = JSON.parse(localStorage.getItem("outputs"));
    } else {
        var index = id.slice(id.length - 1);
        var valueChanged = JSON.parse(localStorage.getItem("express"));
    }
    if (modelData == '') {
        $('#drag_table_' + id + ' .container_min table tbody tr').each(function() {
            if ($(this).hasClass('highlight')) {
                var ids = $(this).attr("data-id");
                edit_dataid_source.push($(this).attr("data-id"));
            }
        });
    } else {
        $('#mappping_link3_read_pro2  table tbody tr').each(function() {
            if ($(this).hasClass('highlight')) {
                var ids = $(this).attr("data-id");
                edit_dataid_source.push($(this).attr("id"));
            }
        });
    }
    if (edit_dataid_source.length == 0) {
        toastr.error('No Row(s) Selected!', 'Error');
    } else if (edit_dataid_source.length > 1) {
        toastr.error('More than one row(s) selected', 'Error');
    } else {
        //var valueChanged=JSON.parse(localStorage.getItem("selected"));
        var rowIndex = edit_dataid_source[0].split("_");
        valueChanged = valueChanged[index].columns[parseInt(rowIndex[3] - 1)];
        if (modelData != '') {
            $('#' + modelData).modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            });
            $("#" + modelData + " #port_update").attr("onclick", "save_add_new_row('" + id + "',true)");
            $('#port_edit #new_id').val(valueChanged.attribute_id);
            $('#port_edit #new_name').val(valueChanged.attribute_name);
            $('#port_edit #new_type').val(valueChanged.attribute_type);
            $('#port_edit #new_precision').val(valueChanged.attribute_precision);
            $('#port_edit #new_scale').val(valueChanged.attribute_scale);
        } else {
            if (!($('#drag_table_' + id + ' div').hasClass("new_row_" + id))) {
                $('#drag_table_' + id).append('<div class="container sbg-01 new_row_' + id + '"><div class="second-layer"><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="hidden" name="new_id" id="new_id"  value="' + valueChanged.attribute_id + '"><input type="text" class="form-control" name="new_name" id="new_name" placeholder="Name" value="' + valueChanged.attribute_name + '"><div class="line"></div></div></div><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="text" class="form-control" name="new_type" id="new_type"  placeholder="Type" value="' + valueChanged.attribute_type + '"><div class="line"></div></div></div><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="text" class="form-control" name="new_precision" id="new_precision"  value="' + valueChanged.attribute_precision + '" placeholder="Precision"><div class="line"></div></div></div><div class="form-row"><div class="form-group remove-pad col-md-12"><input type="text" class="form-control" name="new_scale" id="new_scale" value="' + valueChanged.attribute_scale + '" placeholder="Scale"><div class="line"></div></div></div><div class="icon-pos-03"><a onclick="save_add_new_row(' + "'" + id + "'" + ',false)"><span style="padding-right:3px"><i class="fa fa-floppy-o circle-ico" aria-hidden="true" ></i></span></a><a onclick="del_add_new_row(' + "'" + id + "'" + ')"><span><i class="fa fa-times circle-ico" aria-hidden="true" ></i></span></a></div></div></div>');
                window.jsp.repaintEverything();
            } else {
                $('#new_name').focus();
                toastr.warning('Fill the column first!', 'Warning');
            }
        }
    }

}



function selectThis(dataid) {
    if ($("[data-id=" + dataid + "]").hasClass('highlight')) {
        $("[data-id=" + dataid + "]").removeClass('highlight');
    } else {
        $("[data-id=" + dataid + "]").addClass('highlight');
    }
}

function selectThisProp(id) {
    if ($("#" + id).hasClass('highlight')) {
        $("#" + id).removeClass('highlight');
    } else {
        $("#" + id).addClass('highlight');
    }
}

function selectAll(id) {
    var len = $('#drag_table_' + id + ' .container_min table tbody tr').length;
    var flag = 0;
    $('#drag_table_' + id + ' .container_min table tbody tr').each(function() {
        if (!($(this).hasClass('highlight'))) {
            if ($(this).hasClass('donotSelectThis')) {
                flag = 0;
            } else {
                flag = 1;
            }
        }
    })

    if (flag == 0) {
        $('#drag_table_' + id + ' .container_min table tbody tr').each(function() {
            $(this).removeClass('highlight')
        })
    } else {
        $('#drag_table_' + id + ' .container_min table tbody tr').each(function() {
            if (!($(this).hasClass('highlight'))) {
                $(this).addClass('highlight');
            }
        })
    }
    if ($('.donotSelectThis').hasClass('highlight')) {
        $('.donotSelectThis').removeClass('highlight');
    }

}

function deleteAllViewer(id) {
    var delete_dataid_source = []
    $('#mappping_link3_read_pro2 table tbody tr').each(function() {
        if ($(this).hasClass('highlight')) {
            delete_dataid_source.push($(this).attr("id"));
        }
    });

    deleteAll(id, delete_dataid_source, true);

}

function deleteAll(id, delete_dataid_source, comeFrom) {
    if (delete_dataid_source.length == 0) {
        var delete_dataid_source = []
        var delete_connection_source = []
        $('#drag_table_' + id + ' .container_min table tbody tr').each(function() {
            if ($(this).hasClass('highlight')) {
                var ids = $(this).attr("data-id");
                delete_dataid_source.push($(this).attr("data-id"));
                $("[data-id=" + ids + "] td").each(function(e, s) {
                    delete_connection_source.push($(this).attr("id"));
                });
            }
        });
    }

    if (delete_dataid_source.length == 0) {
        toastr.error('No Row(s) Selected!', 'Error');
    } else {
        if (!comeFrom) {
            $('.drag_table').each(function() {
                var temps = $(this).attr("id");
                var list2El = document.querySelector("#" + temps);
                var list2Ul = document.querySelector('#' + temps + ' .container_min');
                window.jsp.removeList(list2Ul);
            });
            var connected = window.jsp.getConnections();
            delete_connection_source.forEach((element1, index1) => {
                $.each(connected, function(e, s) {
                    if (s.source != null) {
                        if (element1 == s.source.id) {
                            window.jsp.deleteConnection(s);
                        }
                    }
                    if (s.target != null) {
                        if (element1 == s.target.id) {
                            window.jsp.deleteConnection(s);
                        }
                    }
                });
            });
        }

        if ($('#drag_table_' + id).attr('data-type') == 'table') {
            var index = id;
            var valueChanged = JSON.parse(localStorage.getItem("selected"));
        } else {
            var index = id.slice(id.length - 1);
            var valueChanged = JSON.parse(localStorage.getItem("outputs"));
        }
        var valueChanged1 = valueChanged[index].columns;
        if (!comeFrom) {
            delete_dataid_source.forEach((element, index) => {
                var temp_array = element.split('_')
                valueChanged1.forEach((element1, index1) => {
                    if (temp_array[3] == element1.id) {
                        valueChanged[index].columns.splice(parseInt(index1), 1);
                    }
                });
                $("[data-id=" + element + "]").remove()
            });
        } else {
            delete_dataid_source.forEach((element, index) => {
                var temp_array = element.split('_')
                valueChanged1.forEach((element1, index1) => {
                    if (temp_array[3] == element1.id) {
                        valueChanged[index].columns.splice(parseInt(index1), 1);
                    }
                });
                $("#" + element).remove()
            });

        }

        if ($('#drag_table_' + id).attr('data-type') == 'table') {
            localStorage.setItem("selected", JSON.stringify(valueChanged));
        } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
            localStorage.setItem("outputs", JSON.stringify(valueChanged));
        } else {
            localStorage.setItem("express", JSON.stringify(valueChanged));
        }

        $('.drag_table').each(function() {
            var temps = $(this).attr("id");
            var list2El = document.querySelector("#" + temps);
            var list2Ul = document.querySelector('#' + temps + ' .container_min');
            window.jsp.addList(list2Ul);
        });
        toastr.success('Row(s) Deleted Successfully', 'Success');
    }
}

function checkAdjacent(array) {
    var first_array_value = array[0];
    var flag = 0;
    array.forEach((element1, index1) => {
        if (first_array_value == element1) {
            flag++;
        }
        first_array_value++;
    })
    return flag == array.length ? true : false;
}

function moveItem(data, from, to) {
    var f = data.splice(from, 1)[0];
    data.splice(to, 0, f);
    return data;
}

function TableMoveUpDownViewer(id, direction) {
    var moved_dataid_source = [];
    var moved_indexes = []
    $('#mappping_link3_read_pro2 table tbody tr').each(function(e, s) {
        if ($(this).hasClass('highlight')) {
            var ids = $(this).attr("data-id");
            moved_indexes.push(parseInt(e - 1));
            moved_dataid_source.push($(this).attr("id"));
        }
    });
    TableMoveUpDown(id, direction, moved_dataid_source, moved_indexes, true);

}

function TableMoveUpDown(id, direction, moved_dataid_source, moved_indexes, comeFrom) {
    if (moved_dataid_source.length == 0) {
        var moved_dataid_source = [];
        var moved_indexes = []
        $('#drag_table_' + id + ' .container_min table tbody tr').each(function(e, s) {
            if ($(this).hasClass('highlight')) {
                var ids = $(this).attr("data-id");
                moved_indexes.push(e);
                moved_dataid_source.push($(this).attr("data-id"));
            }
        });
    }
    if (moved_dataid_source.length == 0) {
        toastr.error('No Row(s) Selected!', 'Error');
    } else {
        var check = checkAdjacent(moved_indexes);
        if (check) {
            if (comeFrom) {
                moved_dataid_source.forEach((element, index) => {
                    var row = $("#" + element);
                    if (direction == 'up') {
                        row.insertBefore(row.prev());
                    }
                })
                for (var i = moved_dataid_source.length - 1; i >= 0; i--) {
                    var row = $("#" + moved_dataid_source[i]);
                    if (direction == 'down') {
                        row.insertAfter(row.next());
                    }
                }
                //$('#drag_table_'+id).load(location.href+' #drag_table_'+id);
            } else {
                moved_dataid_source.forEach((element, index) => {
                    var row = $("[data-id=" + element + "]");
                    if (direction == 'up') {
                        row.insertBefore(row.prev());
                    }
                })
                for (var i = moved_dataid_source.length - 1; i >= 0; i--) {
                    var row = $("[data-id=" + moved_dataid_source[i] + "]");
                    if (direction == 'down') {
                        row.insertAfter(row.next());
                    }
                }
                $('.drag_table').each(function() {
                    var temps = $(this).attr("id");
                    var list2El = document.querySelector("#" + temps);
                    var list2Ul = document.querySelector('#' + temps + ' .container_min');
                    window.jsp.removeList(list2Ul);
                });

                var connected = window.jsp.getConnections();
                var connection = [];
                $.each(connected, function(e, s) {
                    var temp_conn = {
                        'source': s.sourceId,
                        'target': s.targetId
                    };
                    connection.push(temp_conn);
                });

                window.jsp.deleteEveryConnection();

                connection.forEach((ele, i) => {
                    window.jsp.connect({ source: ele.source, target: ele.target });
                    window.jsp.repaintEverything();
                })

                $('.drag_table').each(function() {
                    var temps = $(this).attr("id");
                    var list2El = document.querySelector("#" + temps);
                    var list2Ul = document.querySelector('#' + temps + ' .container_min');
                    window.jsp.addList(list2Ul);
                });
            }
            if ($('#drag_table_' + id).attr('data-type') == 'table') {
                var index = id;
                var valueChanged = JSON.parse(localStorage.getItem("selected"));
            } else {
                var index = id.slice(id.length - 1);
                var valueChanged = JSON.parse(localStorage.getItem("outputs"));
            }
            var valueChanged1 = valueChanged[index].columns;
            for (var i = moved_indexes.length - 1; i >= 0; i--) {
                if (direction == "down") {
                    valueChanged[index].columns = moveItem(valueChanged[index].columns, moved_indexes[i], parseInt(moved_indexes[i] + 1));
                }
            }
            for (var i = 0; i < valueChanged.length; i++) {
                if (direction == "up") {
                    valueChanged[index].columns = moveItem(valueChanged[index].columns, moved_indexes[i], parseInt(moved_indexes[i] - 1));
                }
            }
            if ($('#drag_table_' + id).attr('data-type') == 'table') {
                localStorage.setItem("selected", JSON.stringify(valueChanged));
            } else {
                localStorage.setItem("outputs", JSON.stringify(valueChanged));
            }

        } else {
            toastr.error('Move possible only in adjacent row(s),Please select adjacent row(s)', 'Error');
        }
    }
    //create_table(table,leftPosition,topPosition,connection,read_or_write,mapping_id,property)
    //$("#mappping_link3_read_pro2").load(location.href + " #mappping_link3_read_pro2");
    //$('#canvas').load(location.href + ' #canvas');

}

function TableCopy(id) {
    if (JSON.parse(localStorage.getItem("copy"))) {
        localStorage.removeItem("copy");
    }
    var copy_dataid_source = [];
    $('#drag_table_' + id + ' .container_min table tbody tr').each(function(e, s) {
        if ($(this).hasClass('highlight')) {
            var ids = $(this).attr("data-id");
            copy_dataid_source.push($(this).attr("data-id"));
        }
    });
    if (copy_dataid_source.length == 0) {
        toastr.error('No Row(s) Selected!', 'Error');
    } else {
        if ($('#drag_table_' + id).attr('data-type') == 'table') {
            var index = id;
            var valueChanged = JSON.parse(localStorage.getItem("selected"));
        } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
            var index = id;
            var valueChanged = JSON.parse(localStorage.getItem("outputs"));
        } else {
            var index = id.slice(id.length - 1);
            var valueChanged = JSON.parse(localStorage.getItem("express"));
        }
        var valueChanged1 = valueChanged[index].columns;
        var copied_value = [];
        copy_dataid_source.forEach((element, index) => {
            var temp_array = element.split('_')
            valueChanged1.forEach((element1, index1) => {
                if (element1.attribute_id == temp_array[3]) {
                    copied_value.push(element1);
                }
            });
        });
        localStorage.setItem("copy", JSON.stringify(copied_value));
        localStorage.setItem("copy_table", id);
        $('#drag_table_' + id + ' .container_min table tbody tr').each(function(e, s) {
            if ($(this).hasClass('highlight')) {
                $(this).removeClass('highlight');
            }
        });
        toastr.success('Copy Successful', 'Success');

    }
}

function TablePaste(id) {
    if (!(JSON.parse(localStorage.getItem("copy")))) {
        toastr.error('There is no copied data', 'Error');
    } else {
        var copy_table = localStorage.getItem("copy_table");
        // if(copy_table == id){
        //   Swal.fire(
        //         'Error!',
        //         "We can't paste copied data here",
        //         'error'
        //   ) ;
        // }
        // else{
        var copied_data = JSON.parse(localStorage.getItem("copy"));

        if ($('#drag_table_' + id).attr('data-type') == 'table') {
            var express_flag = "";
            var ouputHtml = '';
            var index = id;
            var valueChanged = JSON.parse(localStorage.getItem("selected"));
        } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
            var express_flag = "";
            var ouputHtml = '';
            var index = id.slice(id.length - 1);
            var valueChanged = JSON.parse(localStorage.getItem("outputs"));
        } else {
            // if($('#drag_table_'+id+ ' .container_min table tbody #check_output_present').length == 0){
            var ouputHtml = `<tr class="donotSelectThis">
                    <td></td>
                    <td>Inputs</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td></td>
                  </tr>`;
            //  }
            //else{
            //    var ouputHtml='';
            //}
            var express_flag = 0
            var index = id.slice(id.length - 1);
            var valueChanged = JSON.parse(localStorage.getItem("express"));
        }
        if (valueChanged[index].columns == 0) {
            var find_static = 0;
        } else {
            var valueChanged1 = valueChanged[index].columns;
            var find_static = [];
            valueChanged[index].columns.forEach((ele, i) => {
                find_static.push(ele.attribute_id);
            })
            find_static = Math.max(...find_static);
            find_static = parseInt(find_static + 1);
        }

        var mapping_id = $('#drag_table_' + id).attr("data-id");

        copied_data.forEach((ele, i) => {
            var new_id = parseInt(find_static + i);
            valueChanged[index].columns.push({
                "attribute_id": new_id,
                "attribute_alias": ele.attribute_name,
                "attribute_name": ele.attribute_name,
                "attribute_type": ele.attribute_type,
                "attribute_precision": ele.attribute_precision,
                "attribute_scale": ele.attribute_scale,
                "express_flag": express_flag
            })


            if (valueChanged[index].columns.length == 1) {
                var appendHtmlVal = `<table id="dateTable_${id}_${mapping_id}" class="display nowrap tablefont">
                <thead>
                  <tr>
                    <th></th>
                    <th id='column-header-1'>Name<div id='column-header-1-sizer'></div></th>
                    <th id='column-header-2'>Type<div id='column-header-2-sizer'></div></th>
                    <th id='column-header-3'>Precision<div id='column-header-3-sizer'></div></th>
                    <th id='column-header-4'>Scale<div id='column-header-4-sizer'></div></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody source target>${ouputHtml}`;
                var listeleL = 'ref_class_' + id + '_' + new_id + '_L';
                var listeleR = 'ref_class_' + id + '_' + new_id + '_R';
                appendHtmlVal += `<tr onclick="selectThis('ref_class_${id}_${new_id}')" data-id="ref_class_${id}_${new_id}">
                                <td data-id="ref_class_${id}_${new_id}_L"><img src="images/connector.png" width="10" height="10">
                                </td>
                                <td>${ele.attribute_name}</td>
                                <td>${ele.attribute_type}</td>
                                <td>${ele.attribute_precision}</td>
                                <td>${ele.attribute_scale}</td>
                                <td data-id="ref_class_${id}_${new_id}_R"><img src="images/connector.png" width="10" height="10">
                                </td>
                            </tr></tbody></table>`;
                $('#drag_table_' + id + ' .container_min').html(appendHtmlVal);
            } else {
                var listeleL = 'ref_class_' + id + '_' + new_id + '_L';
                var listeleR = 'ref_class_' + id + '_' + new_id + '_R';
                var appendHtmlVal = `<tr onclick="selectThis('ref_class_${id}_${new_id}')" data-id="ref_class_${id}_${new_id}">
                                <td data-id="ref_class_${id}_${new_id}_L"><img src="images/connector.png" width="10" height="10">
                                </td>
                                <td>${ele.attribute_name}</td>
                                <td>${ele.attribute_type}</td>
                                <td>${ele.attribute_precision}</td>
                                <td>${ele.attribute_scale}</td>
                                <td data-id="ref_class_${id}_${new_id}_R"><img src="images/connector.png" width="10" height="10">
                                </td>
                            </tr>`;
                $('#dateTable_' + id + '_' + mapping_id + ' tbody tr:last').after(appendHtmlVal);
            }

            window.jsp.makeSource($("[data-id=" + listeleL + "]"), { anchor: ["Left", "Right"] });
            window.jsp.makeSource($("[data-id=" + listeleR + "]"), { anchor: ["Left", "Right"] });
            window.jsp.makeTarget($("[data-id=" + listeleL + "]"), { anchor: ["Left", "Right"] });
            window.jsp.makeTarget($("[data-id=" + listeleR + "]"), { anchor: ["Left", "Right"] });
            $('#dateTable_' + id + '_' + mapping_id).tableHeadFixer({
                "head": true,
                "left": 1,
                "right": 1,
                "z-index": 4
            });

        });
        var addListData = document.querySelector('#drag_table_' + id + ' .container_min');
        var temp_list = window.jsp.addList(addListData);

        var connection_copy_data = JSON.parse(localStorage.getItem("copy"));
        var dpr = localStorage.getItem("copy_table");
        var connection = [];

        connection_copy_data.forEach((ele, i) => {
            valueChanged[index].columns.forEach((ele1, i1) => {
                if (i == i1) {
                    var temp = {
                        "source": $("[data-id=ref_class_" + dpr + '_' + ele.attribute_id + "_R]").attr("id"),
                        "target": $("[data-id=ref_class_" + id + '_' + ele1.attribute_id + "_L]").attr("id")
                    }
                    window.jsp.connect({
                        source: $("[data-id=ref_class_" + dpr + '_' + ele.attribute_id + "_R]").attr("id"),
                        target: $("[data-id=ref_class_" + id + '_' + ele1.attribute_id + "_L]").attr("id")
                    });
                    window.jsp.repaintEverything();
                    connection.push(temp);
                }
            });
        })



        if ($('#drag_table_' + id).attr('data-type') == 'table') {
            localStorage.setItem("selected", JSON.stringify(valueChanged));
        } else if ($('#drag_table_' + id).attr('data-type') == 'output') {
            localStorage.setItem("outputs", JSON.stringify(valueChanged));
        } else {
            localStorage.setItem("express", JSON.stringify(valueChanged));
        }

        localStorage.removeItem("copy");
        localStorage.removeItem("copy_table");


        toastr.success("Paste Successful", 'Success');

        //}
    }

    $("#mappping_link3_read_pro2").load(location.href + " #mappping_link3_read_pro2");
}


function select_option(value) {
    if (value == 'link3') {
        $('#mapping_drop_hide1').show();
        $('#mapping_drop_hide').hide();
    }
    if (value == 'link4') {
        $('#mapping_drop_hide').show();
        $('#mapping_drop_hide1').hide();

    }
}

function generalPortChange(value, mapping_id) {
    if (value == 1) {
        $('#map_capss' + mapping_id + ' #read_pro2').hide();
        $('#map_capss' + mapping_id + ' #read_pro1').show();
    }
    if (value == 2) {
        $('#map_capss' + mapping_id + ' #read_pro2').show();
        $('#map_capss' + mapping_id + ' #read_pro1').hide();

    }
}

function resizable_datatable(id) {
    $("#drag_table_" + id + '.tab_icon_hide').css({ "height": "9%" });
    $("#drag_table_" + id + '.header_title_table').css({ "height": "12%" });
    window.jsp.repaintEverything();
}

var selections = {
    items: [],
    owner: null,
    droptarget: null
};

// function dragdropcollect(id){

//     //get the collection of draggable targets and add their draggable attribute
//     for(var
//         targets = document.querySelectorAll('[data-draggable="target"]'),
//         len = targets.length,
//         i = 0; i < len; i ++)
//     {
//         targets[i].setAttribute('aria-dropeffect', 'none');
//     }

//     //get the collection of draggable items and add their draggable attributes
//     for(var
//         items = document.querySelectorAll('[data-draggable="item"]'),
//         len = items.length,
//         i = 0; i < len; i ++)
//     {
//         items[i].setAttribute('draggable', 'true');
//         items[i].setAttribute('aria-grabbed', 'false');
//         items[i].setAttribute('tabindex', '0');
//     }
// }


//     //mousedown event to implement single selection
//     document.addEventListener('mousedown', function(e){

//         //if the element is a draggable item
//         if(e.target.getAttribute('draggable'))
//         {
//             //clear dropeffect from the target containers
//             clearDropeffects();

//             //if the multiple selection modifier is not pressed
//             //and the item's grabbed state is currently false
//             if
//             (
//                 !hasModifier(e)
//                 &&
//                e.target.getAttribute('aria-grabbed') == 'false'
//             )
//             {
//                 //clear all existing selections
//                 clearSelections();

//                 //then add this new selection
//                 addSelection(e.target);
//             }
//         }

//         //else [if the element is anything else]
//         //and the selection modifier is not pressed
//         else if(!hasModifier(e))
//         {
//             //clear dropeffect from the target containers
//             clearDropeffects();

//             //clear all existing selections
//             clearSelections();
//         }

//         //else [if the element is anything else and the modifier is pressed]
//         else
//         {
//             //clear dropeffect from the target containers
//             clearDropeffects();
//         }

//     },false);


//     //mouseup event to implement multiple selection
//     document.addEventListener('mouseup', function(e){
//         // if the element is a draggable item
//         // and the multipler selection modifier is pressed
//         //var e=e.target.closest('tr');
//         if(e.target.getAttribute('draggable') && hasModifier(e))
//         {
//             //if the item's grabbed state is currently true
//             if(e.target.getAttribute('aria-grabbed') == 'true')
//             {
//                 //unselect this item
//                 removeSelection(e.target);

//                 //if that was the only selected item
//                 //then reset the owner container reference
//                 if(!selections.items.length)
//                 {
//                     selections.owner = null;
//                 }
//             }

//             //else [if the item's grabbed state is false]
//             else
//             {
//                 //add this additional selection
//                 addSelection(e.target);
//             }
//         }
//     },false);

//     //dragstart event to initiate mouse dragging
//     document.addEventListener('dragstart', function(e){
//         //if the element's parent is not the owner, then block this event
//         if(selections.owner !=e.target.parentNode)
//         {
//             e.preventDefault();
//             return;
//         }

//         //[else] if the multiple selection modifier is pressed
//         //and the item's grabbed state is currently false
//         if
//         (
//             hasModifier(e)
//             &&
//            e.target.getAttribute('aria-grabbed') == 'false'
//         )
//         {
//             //add this additional selection
//             addSelection(e.target);
//         }

//         //we don't need the transfer data, but we have to define something
//         //otherwise the drop action won't work at all in firefox
//         //most browsers support the proper mime-type syntax, eg. "text/plain"
//         //but we have to use this incorrect syntax for the benefit of IE10+
//        //e.originalEvent.dataTransfer.setData('text', '');

//         //apply dropeffect to the target containers
//         addDropeffects();

//     },false);

//         //keydown event to implement selection and abort
//     document.addEventListener('keydown', function(e){
//         //if the element is a grabbable item
//         if(e.target.getAttribute('aria-grabbed'))
//         {
//             //Space is the selection or unselection keystroke
//             if(e.keyCode == 32)
//             {
//                 //if the multiple selection modifier is pressed
//                 if(hasModifier(e))
//                 {
//                     //if the item's grabbed state is currently true
//                     if(e.target.getAttribute('aria-grabbed') == 'true')
//                     {
//                         //if this is the only selected item, clear dropeffect
//                         //from the target containers, which we must do first
//                         //in case subsequent unselection sets owner to null
//                         if(selections.items.length == 1)
//                         {
//                             clearDropeffects();
//                         }

//                         //unselect this item
//                         removeSelection(e.target);

//                         //if we have any selections
//                         //apply dropeffect to the target containers,
//                         //in case earlier selections were made by mouse
//                         if(selections.items.length)
//                         {
//                             addDropeffects();
//                         }

//                         //if that was the only selected item
//                         //then reset the owner container reference
//                         if(!selections.items.length)
//                         {
//                             selections.owner = null;
//                         }
//                     }

//                     //else [if its grabbed state is currently false]
//                     else
//                     {
//                         //add this additional selection
//                         addSelection(e.target);

//                         //apply dropeffect to the target containers
//                         addDropeffects();
//                     }
//                 }

//                 //else [if the multiple selection modifier is not pressed]
//                 //and the item's grabbed state is currently false
//                 else if(e.target.getAttribute('aria-grabbed') == 'false')
//                 {
//                     //clear dropeffect from the target containers
//                     clearDropeffects();

//                     //clear all existing selections
//                     clearSelections();

//                     //add this new selection
//                     addSelection(e.target);

//                     //apply dropeffect to the target containers
//                     addDropeffects();
//                 }

//                 //else [if modifier is not pressed and grabbed is already true]
//                 else
//                 {
//                     //apply dropeffect to the target containers
//                     addDropeffects();
//                 }

//                 //then prevent default to avoid any conflict with native actions
//                 e.preventDefault();
//             }

//             //Modifier + M is the end-of-selection keystroke
//             if(e.keyCode == 77 && hasModifier(e))
//             {
//                 //if we have any selected items
//                 if(selections.items.length)
//                 {
//                     //apply dropeffect to the target containers
//                     //in case earlier selections were made by mouse
//                     addDropeffects();

//                     //if the owner container is the last one, focus the first one
//                     if(selections.owner == targets[targets.length - 1])
//                     {
//                         targets[0].focus();
//                     }

//                     //else [if it's not the last one], find and focus the next one
//                     else
//                     {
//                         for(var len = targets.length, i = 0; i < len; i ++)
//                         {
//                             if(selections.owner == targets[i])
//                             {
//                                 targets[i + 1].focus();
//                                 break;
//                             }
//                         }
//                     }
//                 }

//                 //then prevent default to avoid any conflict with native actions
//                 e.preventDefault();
//             }
//         }

//         //Escape is the abort keystroke (for any target element)
//         if(e.keyCode == 27)
//         {
//             //if we have any selected items
//             if(selections.items.length)
//             {
//                 //clear dropeffect from the target containers
//                 clearDropeffects();

//                 //then set focus back on the last item that was selected, which is
//                 //necessary because we've removed tabindex from the current focus
//                 selections.items[selections.items.length - 1].focus();

//                 //clear all existing selections
//                 clearSelections();

//                 //but don't prevent default so that native actions can still occur
//             }
//         }

//     },false);
//     //related variable is needed to maintain a reference to the
//     //dragleave's relatedTarget, since it doesn't have e.relatedTarget
//     var related = null;

//     //dragenter event to set that variable
//     document.addEventListener('dragenter', function(e){
//         related =e.target;

//     },false);


//     //dragleave event to maintain target highlighting using that variable
//     document.addEventListener('dragleave', function(e){
//         //get a drop target reference from the relatedTarget
//         var droptarget = getContainer(related);

//         //if the target is the owner then it's not a valid drop target
//         if(droptarget == selections.owner)
//         {
//             droptarget = null;
//         }

//         //if the drop target is different from the last stored reference
//         //(or we have one of those references but not the other one)
//         if(droptarget != selections.droptarget)
//         {
//             //if we have a saved reference, clear its existing dragover class
//             if(selections.droptarget)
//             {
//                 selections.droptarget.className =
//                     selections.droptarget.className.replace(/ dragover/g, '');
//             }

//             //apply the dragover class to the new drop target reference
//             if(droptarget)
//             {
//                 droptarget.className += ' dragover';
//             }

//             //then save that reference for next time
//             selections.droptarget = droptarget;
//         }

//     },false);

//     //dragover event to allow the drag by preventing its default
//     document.addEventListener('dragover', function(e){
//         //if we have any selected items, allow them to be dragged
//         if(selections.items.length)
//         {
//             e.preventDefault();
//         }

//     },false);



//     //dragend event to implement items being validly dropped into targets,
//     //or invalidly dropped elsewhere, and to clean-up the interface either way
//     document.addEventListener('dragend', function(e)
//     {
//         //if we have a valid drop target reference
//         //(which implies that we have some selected items)
//         if(selections.droptarget)
//         {
//             //append the selected items to the end of the target container
//             for(var len = selections.items.length, i = 0; i < len; i ++)
//             {
//                 selections.droptarget.appendChild(selections.items[i]);
//             }

//             //prevent default to allow the action
//             e.preventDefault();
//         }

//         //if we have any selected items
//         if(selections.items.length)
//         {
//             //clear dropeffect from the target containers
//             clearDropeffects();

//             //if we have a valid drop target reference
//             if(selections.droptarget)
//             {
//                 //reset the selections array
//                 clearSelections();

//                 //reset the target's dragover class
//                 selections.droptarget.className =
//                     selections.droptarget.className.replace(/ dragover/g, '');

//                 //reset the target reference
//                 selections.droptarget = null;
//             }
//         }

//     },false);



//     //exclude older browsers by the features we need them to support
//     //and legacy opera explicitly so we don't waste time on a dead browser

//     function addSelection(item)
//     {
//         //if the owner reference is still null, set it to this item's parent
//         //so that further selection is only allowed within the same container
//         if(!selections.owner)
//         {
//             selections.owner = item.parentNode;
//         }

//         //or if that's already happened then compare it with this item's parent
//         //and if they're not the same container, return to prevent selection
//         else if(selections.owner != item.parentNode)
//         {
//             return;
//         }

//         //set this item's grabbed state
//         item.setAttribute('aria-grabbed', 'true');

//         //add it to the items array
//         selections.items.push(item);
//     }

//     //function for unselecting an item
//     function removeSelection(item)
//     {
//         //reset this item's grabbed state
//         item.setAttribute('aria-grabbed', 'false');

//         //then find and remove this item from the existing items array
//         for(var len = selections.items.length, i = 0; i < len; i ++)
//         {
//             if(selections.items[i] == item)
//             {
//                 selections.items.splice(i, 1);
//                 break;
//             }
//         }
//     }

//     //function for resetting all selections
//     function clearSelections()
//     {
//         //if we have any selected items
//         if(selections.items.length)
//         {
//             //reset the owner reference
//             selections.owner = null;

//             //reset the grabbed state on every selected item
//             for(var len = selections.items.length, i = 0; i < len; i ++)
//             {
//                 selections.items[i].setAttribute('aria-grabbed', 'false');
//             }

//             //then reset the items array
//             selections.items = [];
//         }
//     }

//     //shorctut function for testing whether a selection modifier is pressed
//     function hasModifier(e)
//     {
//         return (e.ctrlKey || e.metaKey || e.shiftKey);
//     }


//     //function for applying dropeffect to the target containers
//     function addDropeffects()
//     {
//         var targets = document.querySelectorAll('[data-draggable="target"]');
//         var items = document.querySelectorAll('[data-draggable="item"]');
//         //apply aria-dropeffect and tabindex to all targets apart from the owner
//         for(var len = targets.length, i = 0; i < len; i ++)
//         {
//             if
//             (
//                 targets[i] != selections.owner
//                 &&
//                 targets[i].getAttribute('aria-dropeffect') == 'none'
//             )
//             {
//                 targets[i].setAttribute('aria-dropeffect', 'move');
//                 targets[i].setAttribute('tabindex', '0');
//             }
//         }

//         //remove aria-grabbed and tabindex from all items inside those containers
//         for(var len = items.length, i = 0; i < len; i ++)
//         {
//             if
//             (
//                 items[i].parentNode != selections.owner
//                 &&
//                 items[i].getAttribute('aria-grabbed')
//             )
//             {
//                 items[i].removeAttribute('aria-grabbed');
//                 items[i].removeAttribute('tabindex');
//             }
//         }
//     }

//     //function for removing dropeffect from the target containers
//     function clearDropeffects()
//     {
//         var targets = document.querySelectorAll('[data-draggable="target"]');
//         var items = document.querySelectorAll('[data-draggable="item"]');
//         //if we have any selected items
//         if(selections.items.length)
//         {
//             //reset aria-dropeffect and remove tabindex from all targets
//             for(var len = targets.length, i = 0; i < len; i ++)
//             {
//                 if(targets[i].getAttribute('aria-dropeffect') != 'none')
//                 {
//                     targets[i].setAttribute('aria-dropeffect', 'none');
//                     targets[i].removeAttribute('tabindex');
//                 }
//             }

//             //restore aria-grabbed and tabindex to all selectable items
//             //without changing the grabbed value of any existing selected items
//             for(var len = items.length, i = 0; i < len; i ++)
//             {
//                 if(!items[i].getAttribute('aria-grabbed'))
//                 {
//                     items[i].setAttribute('aria-grabbed', 'false');
//                     items[i].setAttribute('tabindex', '0');
//                 }
//                 else if(items[i].getAttribute('aria-grabbed') == 'true')
//                 {
//                     items[i].setAttribute('tabindex', '0');
//                 }
//             }
//         }
//     }

//     //shortcut function for identifying an event element's target container
//     function getContainer(element)
//     {
//         do
//         {
//             if(element.nodeType == 1 && element.getAttribute('aria-dropeffect'))
//             {
//                 return element;
//             }
//         }
//         while(element = element.parentNode);

//         return null;
//     }
$(window).on('load', function() {
    setTimeout(function() {
        if ($('.sidebar').width() < 250) {
            $('a.logo-mini').show();
        } else {
            $('a.logo-mini').hide();
        }
    }, 500);

});
