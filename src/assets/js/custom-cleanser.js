 $(document).ready(function() {
     if (localStorage.getItem("selected")) {
         if (localStorage.getItem("project_name")) {
             $('.pdo1').removeClass('fa-folder');
             $('.pdo1').addClass('fa-folder-open');
         } else {
             $('.pdo1').addClass('fa-folder');
             $('.pdo1').removeClass('fa-folder-open');
         }
     }
     var selected = JSON.parse(localStorage.getItem("selected"));
     var mapping = JSON.parse(localStorage.getItem("mapping"));
     var project_name = localStorage.getItem("project_name");
     if (project_name) {
         $('#pdo').html(project_name);
         $('#pdo').parent().closest('ul').show();
         var len = '';
         for (var i = 0; i < selected.length; i++) {
             len += '<li class="draggable"><i class="fa fa-database tree-file"></i>&nbsp;<label><a href="javascript:void(0)" onclick=overView("' + selected[i].tableAliasName + '",' + i + ')><span class="label-text">' + selected[i].tableAliasName + '</span></a></label></li>'
         };
         // len+=`<li class="draggable" data-id="1" id="change_id_ouput">
         //         <i class="fa fa-folder fa-minus"></i>
         //         <label><a href="javascript:void(0)"><span class="label-text">output</span></a></label>
         //         <ul id="output_trees" style="display: none;"></ul></li>`;
         len += `<li data-id="1" id="change_id_ouput">
                <i class="fa fa-folder fa-minus"></i>
                <label>
                    <span class="label-text">output</span>
                </label>
                <a href="javascript:void(0);" rel="tooltip" data-placement="right" class="tooltip-trigger modal-trigger waves-effect btn-flat  nopadding" data-toggle="modal" data-placement="top" onclick="openModal('out_mod1');"  title="Add Error Table" >
                    <span class="material-icons tree-icon ">add</span>
                </a>
            <ul id="output_trees" style="display: none;"></ul></li>`;
         //len+='<li class="draggable"><i class="fa fa-folder tree-file"></i>&nbsp;<label><a href="javascript:void(0)"><span class="label-text">output</span></a></label></li>';

         $('#select_table_list1').html(len);
     } else {
         $('#lcal_hde').hide();
     }
     if (project_name) {
         var len = '';
         for (var i = 0; i < mapping.length; i++) {
             len += '<li><img src="images/mapping.png" alt="mapping" class="map_ico">&nbsp;<label><a href="javascript:void(0)" onclick=mapping_overview("' + mapping[i].mapping_name + '",' + i + ')><span class="label-text">' + mapping[i].mapping_name + '</span></a></label></li>'
         };
         $('#mapping_list').html(len);
     }
     if ($('.draggable')) {
         $('.draggable').draggable({ revert: "valid", helper: 'clone', containment: ".content-dq", scroll: false });
     }

     $('.tooltip-trigger').tooltip();
 });