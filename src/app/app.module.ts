import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { httpInterceptorProviders } from './service';
import { Interceptor } from './service/interceptors';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnvServiceProvider } from './env.service.provider';
import { DeactivateRouteComponent} from './service/deactivate-route/deactivate-route.component';
import { RenderingComponent } from './rendering/rendering.component';

// import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
// import { CacheInterceptor } from './service/cache-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    DeactivateRouteComponent,
    RenderingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
  ],
  providers: [
    // { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
    httpInterceptorProviders, EnvServiceProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
