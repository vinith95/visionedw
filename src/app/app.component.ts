import { Component, HostListener } from '@angular/core';

import { AuthService } from '../app/authentication/service/auth.service';
import { JwtService, LoaderService, GlobalService } from '../app/service';
import { TranslateService } from '@ngx-translate/core';
import { isUndefined } from 'util';
import {Router,NavigationStart} from '@angular/router'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app-component.css']
})
export class AppComponent {


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  leftContainerHeight: string;


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  constructor(
    private authService: AuthService,
    private _translate: TranslateService,
    private loaderService: LoaderService,
    private globalService:GlobalService,
    private router: Router
  ) {
    this.getPublicKey();
   router.events.subscribe((event) => {
    if (event instanceof NavigationStart) {
      if(!router.navigated){
        localStorage.setItem('Dashboard_Id',localStorage.getItem('lg_Dashboard_Id'))
      }
    }
  });
  }




  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngAfterViewInit() {
    window.onload = (ev) => {
      // if(localStorage.getItem('pageTitle') != ''){
      //   localStorage.setItem('pageTitle',localStorage.getItem('pageTitle'));
      // }
      // else{
      //   localStorage.setItem('pageTitle','Home');
      // }

      // this.globalService.pageTitle=localStorage.getItem('pageTitle');
     };
    // const getContentAreaHeight = document.getElementsByClassName('content-area')[0].clientHeight;
    // document.getElementById('card-body').style.height = getContentAreaHeight + 'px';
    // document.getElementById('card-body').children[0]['style'].height = '100%';
    document.getElementById('loader_1').style.display = "none";
  }

  loadSpinner
  // ------------------------------------------------------------
  // AFTER INITIALIZATION
  // ------------------------------------------------------------
  ngAfterViewChecked() {
    this.loaderService.showLoader.subscribe(resp => {
      this.loadSpinner = resp;
      document.getElementById('loader_1').style.display = !isUndefined(document.getElementById('loader_1')) ?
        this.loadSpinner ? 'block' : 'none'
        : 'none';
    });
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  private getPublicKey() {
    // this.authService.getToken().subscribe(
    //   (successResponse) => {
    //   },
    //   (errorResponse) => {
    //     let validationMsg = "Technical Issue, Please Try Again Later";
    //   }
    // )
  }

}
