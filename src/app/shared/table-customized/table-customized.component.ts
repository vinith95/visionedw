import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService, GlobalService } from 'src/app/service';
import { XmltoJsonService } from 'src/app/service/xmlto-json.service';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import { threadId } from 'worker_threads';
import { drillDownRequest } from '../../../app/main/reports/report.const';

@Component({
  selector: 'app-table-customized',
  templateUrl: './table-customized.component.html',
  styleUrls: ['./table-customized.component.css']
})
export class TableCustomizedComponent implements OnInit {
  altRowBgColor: string[] = [];
  dataColumnList = [];
  screentypeToolTip: any= 'maximize';
  apiLink: string = '';
  filterApi: any = '';
  pagination: any = {
    totalItems: 0,
    startIndex: 1,
    lastIndex: 7,
    actionType: "Query",
    currentPage: 1,
  };
  individualFilterList = {};
  title: any = '';
  limitedPagination: any = {
    totalItems: 0,
    startIndex: 0,
    lastIndex: 7
  };
  perPage: any = 7;
  compact_css: boolean = false;
  limitedList: any = [];
  overAllListCount: any = 0;
  previousDisp: boolean = false;
  nextDisp: boolean = false;
  screentype = 'fullscreen';
  last_toolTip: any;
  addLable: any = '';
  routingLable: any = '';
  userBasedCount: any = 4;
  viewList: any = [];
  filterList = [];
  monthList = [
    {
      name: 'January',
      id: '01'
    },
    {
      name: 'February',
      id: '02'
    },
    {
      name: 'March',
      id: '03'
    },
    {
      name: 'April',
      id: '04'
    },
    {
      name: 'May',
      id: '05'
    },
    {
      name: 'June',
      id: '06'
    },
    {
      name: 'July',
      id: '07'
    },
    {
      name: 'August',
      id: '08'
    },
    {
      name: 'September',
      id: '09'
    },
    {
      name: 'October',
      id: '10'
    },
    {
      name: 'November',
      id: '11'
    }, {
      name: 'December',
      id: '12'
    }
  ];
  data = {};
  userGroupFilterData;
  userProfileFilterData;
  homeScreenData='';
  homeScreenArry=[];
  filterTypeTwoData;
  filterTypeTwoData1 = {};
  showFilterTypeTwo = false;
  filterDataRe:any;
  postData: any;
  bulkChecked: boolean;
  showReportId: boolean = false;
  reportIdList: any = [];
  burstIdList: any = [];
  reportId;
  burstId;
  formField: any = {};

  @Input() tableValues: any;
  @Output() getFilterValue: EventEmitter<any> = new EventEmitter();
  @Output() tableEvent: EventEmitter<any> = new EventEmitter();

  drillDownList =[];
  templateDetails: any = {};
  reportDesignXml: any;
  widgetPagination: boolean = false;

  constructor( private apiService : ApiService,
    private xmlToJSonService: XmltoJsonService,
    private globalService : GlobalService) { }

  ngOnInit(): void {

    if(this.tableValues.initialRequest.reportDesignXml != '' && this.tableValues.initialRequest.reportDesignXml != null) {
      //this.tableValues.initialRequest.reportDesignXml = JSON.parse(this.tableValues.initialRequest.reportDesignXml.replaceAll("-","_"));
      let tempJson= JSON.parse(this.tableValues.initialRequest.reportDesignXml);
      this.reportDesignXml=tempJson;
      if(this.reportDesignXml.BODY["ROW-BG-COLOR"] != '') {
        this.altRowBgColor.push('#'+this.reportDesignXml.BODY["ROW-BG-COLOR"].split(",")[0]);
        this.altRowBgColor.push('#'+this.reportDesignXml.BODY["ROW-BG-COLOR"].split(",")[1]);
      }
      if(!isNullOrUndefined(this.tableValues.initialRequest.widgetPagination) && this.tableValues.initialRequest.widgetPagination != '' &&
      this.tableValues.initialRequest.widgetPagination != 0){
        this.widgetPagination = true;
        this.perPage = this.tableValues.initialRequest.widgetPagination;
        this.limitedPagination['lastIndex'] = this.tableValues.initialRequest.widgetPagination;
      }

      setTimeout(() => {

        let titleColor = '#' + this.reportDesignXml["TITLE-COLOR"]
        let event={
          type : "color",
          color:titleColor

        }
        this.tableEvent.emit(event)  });
    }
    this.viewList = this.widgetPagination?this.tableValues.data.gridDataSet.slice(0, this.perPage): this.tableValues.data.gridDataSet;
    this.overAllListCount = this.tableValues.data.gridDataSet.length;
    this.paginationDisable();
  }
  tableWidth(){
    return{
      'width': this.reportDesignXml["TABLE-WIDTH"] ? this.reportDesignXml["TABLE-WIDTH"]+ '%' : '100%',
      'border':this.reportDesignXml.HEAD["TABLE-BORDER-ENABLE"] == 'Y' ? this.reportDesignXml.HEAD["TABLE-BORDER-PIXEL"]+'px solid':'none',
      'border-color': this.reportDesignXml.HEAD["TABLE-BORDER-ENABLE"] == 'Y' ? '#'+this.reportDesignXml.HEAD["TABLE-BORDER-COLOR"]: 'none'

    }
  }
  tableStyle() {
 return {
      'border':this.reportDesignXml.HEAD["TABLE-BORDER-ENABLE"] == 'Y' ? this.reportDesignXml.HEAD["TABLE-BORDER-PIXEL"]+'px solid':'none',
      'border-color': this.reportDesignXml.HEAD["TABLE-BORDER-ENABLE"] == 'Y' ? '#'+this.reportDesignXml.HEAD["TABLE-BORDER-COLOR"]: 'none',
    }

  }
  headerStyle() {
    return {
      'background-color': '#'+this.reportDesignXml.HEAD["BG-COLOR"],
      'line-height' : this.reportDesignXml.HEAD["LINE-HEIGHT"]+'px',
      'border': this.reportDesignXml.HEAD["HEADER-BORDER-ENABLE"] == 'Y' ? '1px solid #'+this.reportDesignXml.HEAD["HEADER-BORDER-COLOR"]: '1px solid #'+this.reportDesignXml.HEAD["BG-COLOR"],
      'color': '#'+ this.reportDesignXml.HEAD["FONT-COLOR"],
      'font-weight': this.reportDesignXml.HEAD["FONT-WEIGHT"],
      'font-size': this.reportDesignXml.HEAD["FONT-SIZE"]+'px',
    }
  }
  bodyRowStyle() {
    return {
      'border-bottom': this.reportDesignXml.BODY["ROW-BORDER-ENABLE"] == 'Y' ? '1px '+this.borderStyle(this.reportDesignXml.BODY["ROW-BORDER-STYLE"])+' #'+this.reportDesignXml.BODY["ROW-BORDER-COLOR"]: 'none',
    }
  }

  borderStyle(data){
    if(!isNullOrUndefined(data)){
      return data
    }
    else{
      return 'solid';
    }

  }

  bodyColStyle(header,data) {
    let fontColor,fontWeight, background;
    if (header.colorDiff == 'F') {
      let dataValue = data[header.dbColumnName].toString().split('%').join('');
      fontWeight = 500;
      // if (this.tableValues.initialRequest.reportId == 'WIDG0001') {
      //   // if (dataValue >= 0) {
      //   //   // fontColor = '079e12'
      //   //   background = `var(--widget-background-positive)`
      //   // }
      //   // else {
      //   //   // fontColor = 'FF0000'
      //   //   background = `var(--widget-background-negative)`
      //   // }
      //   // fontColor = '3c4043';
      // } else {
        background = this.reportDesignXml.BODY["ROW-BG-COLOR"]
        if (dataValue >= 0)
          fontColor = '079e12'
        else
          fontColor = 'FF0000'
      // }
    }
    else {
      fontColor = this.reportDesignXml.BODY["FONT-COLOR"];
      fontWeight = this.reportDesignXml.BODY["FONT-WEIGHT"];
      background = this.reportDesignXml.BODY["ROW-BG-COLOR"]
    }
    return {
      'background-color': '#'+background,
      'line-height' : this.reportDesignXml.BODY["LINE-HEIGHT"]+'px',
      'border': this.reportDesignXml.BODY["COLUMN-BORDER-ENABLE"] == 'Y' ? '1px solid #'+ this.reportDesignXml.BODY["COLUMN-BORDER-COLOR"]: 'inherit',
      'color': '#'+ fontColor,
      'font-weight':fontWeight,
      'font-size': this.reportDesignXml.BODY["FONT-SIZE"]+'px'
    }
  }

  backgroundRow(header,data) {
    let dataValue = data[header.dbColumnName].toString().split('%').join('');
    if (header.colorDiff == 'C') {
      if (dataValue >= 0) {
        // fontColor = '079e12'
        return "positive"
      }
      else {
        // fontColor = 'FF0000'
        return "negative"
      }
    }
    else {
      return ''
    }
}

  totalStyle(data, header) {
    if (data.FORMAT_TYPE == 'FT' && header.colorDiff != 'Y') {
      return {
        'color': '#' + this.reportDesignXml.BODY["SUBTOTAL-FONT-COLOR"],
        'font-weight': '600'
      }
    }
  }
  drillDownReq(eve,item, rowdata, i){
  let event = {
    'type' : "drilldown",
    'event' : eve,
    'item': item,
    'rowdata' : rowdata,
    'i': i

  }
  this.tableEvent.emit(event)
  }

  listCountDisp = () => {
    this.perPage = parseInt(this.perPage);
    this.limitedPagination['startIndex'] = 0;
    this.limitedPagination['lastIndex'] = this.perPage;
    this.viewList = this.tableValues.data.gridDataSet.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);

    this.paginationDisable();
  }
  paginationDisable = () => {
    this.previousDisp = (this.limitedPagination['startIndex'] == 0) ? true : false;
    this.nextDisp = (this.viewList.length < this.perPage) ? true :
      (this.limitedPagination['lastIndex'] >= this.overAllListCount) ? true : false;
  }
  getPaginationDet = () => {
    let first = this.limitedPagination['startIndex'] + 1;
    let last = (this.limitedPagination['lastIndex'] > this.overAllListCount) ? this.overAllListCount :
      this.limitedPagination['lastIndex'];
    let aa = `${first} - ${last}  of ${this.overAllListCount}`;
    return aa;
  }
  previous = () => {

    this.limitedPagination['startIndex'] = this.limitedPagination['startIndex'] - (this.perPage);
    this.limitedPagination['lastIndex'] = this.limitedPagination['lastIndex'] - (this.perPage);
    this.viewList = this.tableValues.data.gridDataSet.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
    // this.previousDataList();
    this.paginationDisable();
  }

  // ------------------------------------------------------------
  next = () => {

    this.limitedPagination['startIndex'] = this.limitedPagination['startIndex'] + (this.perPage);
    this.limitedPagination['lastIndex'] = this.limitedPagination['lastIndex'] + (this.perPage);
    this.viewList = this.tableValues.data.gridDataSet.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
    // this.nextDataList();
    this.paginationDisable();
  }

}
