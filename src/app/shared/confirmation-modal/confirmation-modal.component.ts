import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html'
})
export class ConfirmationModalComponent implements OnInit {


  @Input() confirmationBoxTitle;
  @Input() confirmationMessage;
  @Output() result: EventEmitter<any> = new EventEmitter<any>();;


  action = (value) => {
    this.result.emit(value);
  }

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    document.getElementsByClassName('confirmModalHead')[0].parentElement.parentElement.style.cssText = 'height: 170px; width: 400px';
  }

}
