import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-popoup',
  templateUrl: './modal-popoup.component.html'
})
export class ModalPopoupComponent implements OnInit {
  

  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  @Input() modalHeader;
  @Input() modalBodyContent;
  @Output() result: EventEmitter<any> = new EventEmitter<any>();


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  constructor(private modalService: NgbModal) { }
 

  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  ngOnInit() {
  }

  closeModal(){
    this.modalService.dismissAll('Cross click');
  }

}
