import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionValiditionComponent } from './condition-validition.component';

describe('ConditionValiditionComponent', () => {
  let component: ConditionValiditionComponent;
  let fixture: ComponentFixture<ConditionValiditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConditionValiditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionValiditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
