import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
// import KothingEditor from 'kothing-editor';
// import plugins from 'kothing-editor/lib/plugins';
import { isNullOrUndefined } from "util";

@Component({
  selector: "app-condition-validition",
  templateUrl: "./condition-validition.component.html",
  styleUrls: ["./condition-validition.component.css"],
})
export class ConditionValiditionComponent implements OnInit {
  emailBody = "";
  @Input() bodyText: any;
  @Output() emitData: EventEmitter<any> = new EventEmitter<any>();
  initEditor: any;
  emptyArray: any = [];
  constructor() {}

  ngOnInit(): void {
    this.emptyArray.push(this.bodyText);
    this.emailBody = this.bodyText.whereConditions;
  }

  submit(type) {
    this.emptyArray.forEach((element) => {
      if (element) {
        element.whereConditions = this.emailBody;
      }
    });
    let data = {
      type: type,
      value: this.bodyText,
    };
    this.emitData.emit(data);
  }
}
