import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { ApiService, JwtService, GlobalService } from '../../service';
import { environment } from '../../../environments/environment';
import { isObject } from 'util';

@Component({
  selector: 'app-magnifier',
  templateUrl: './magnifier.component.html'
})
export class MagnifierComponent implements OnInit {


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  @Input() query;
  @Input() specialFilterValue;
  @Input() searchData;
  @Input() title;
  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();
  branchList1;
  selectedData;
  startIndex;
  lastIndex;
  totalRows;
  branchList;
  Flag: boolean = false;
  @ViewChild(CdkVirtualScrollViewport, { static: false }) viewport: CdkVirtualScrollViewport;


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  constructor(private globalService: GlobalService,
    private _apiService: ApiService,
    private jwtService: JwtService) { }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  ngOnInit() {
    this.loadMagnifierData()
  }

  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  loadMagnifierData(searchData?) {
    this.startIndex = 0;
    this.lastIndex = 1000;
    let data :any= {};
    if (typeof(this.query)=='object') {
       data = this.query;
       data['startIndex'] = this.startIndex;
       data['lastIndex'] = this.lastIndex;
    } else {
       data = {
        'startIndex': this.startIndex,
        'lastIndex': this.lastIndex,
        'query': this.query,
        'specialFilterValue': this.specialFilterValue
      };
    }

    this._apiService.post(environment.get_Branch_List, data).subscribe((resp) => {
      if (resp['status']) {
        this.branchList1 = resp['response'];
        this.branchList = this.branchList1;
        this.totalRows = resp['otherInfo'].totalRows
      }
    });
  }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  vpDataLength;
  dataLength;
  handler() {
    this.vpDataLength = this.viewport.getRenderedRange().end;
    this.dataLength = this.viewport.getDataLength();
    (this.Flag) ? (this.vpDataLength <= this.startIndex) ? this.Flag = false : '' : '';
    (!this.Flag) ? (this.vpDataLength >= this.dataLength - 500 && this.dataLength < this.totalRows) ? this.loadData() : '' : '';
  }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  loadData() {
    this.Flag = true;
    this.startIndex = this.startIndex + 1000;
    this.lastIndex = this.lastIndex + 1000;
    let data :any= {};
    if (typeof(this.query)=='object') {
       data = this.query;
       data['startIndex'] = this.startIndex;
       data['lastIndex'] = this.lastIndex;
    } else {
       data = {
        'startIndex': this.startIndex,
        'lastIndex': this.lastIndex,
        'query': this.query,
        'specialFilterValue': this.specialFilterValue
      };
    }
    this._apiService.post(environment.get_Branch_List, data).subscribe((resp) => {
      if (resp['status']) {
        this.branchList = [...this.branchList, ...resp['response']];
        this.branchList1 = this.branchList;
      }
    });
  }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  filter = (searchData) => {
    this.branchList1 = [];
    this.branchList = [];
    this.startIndex = 0;
    this.lastIndex = 1000;
    let data: any = {};
    if (typeof(this.query)=='object') {
      data = this.query;
      data['startIndex'] = this.startIndex;
      data['lastIndex'] = this.lastIndex;
      data['smartSearchOpt']= [{
        object: 'search',
        joinType: "AND",
        criteria: 'LIKES',
        value: searchData ? searchData : '',
      }];
   } else {
      data = {
       'startIndex': this.startIndex,
       'lastIndex': this.lastIndex,
       'query': this.query,
       'specialFilterValue': this.specialFilterValue,
       'smartSearchOpt': [{
        object: 'search',
        joinType: "AND",
        criteria: 'LIKES',
        value: searchData ? searchData : '',
      }]
     };
   }
    // const data1 = {
    //   startIndex: this.startIndex,
    //   lastIndex: this.lastIndex,
    //   query: this.query,
    //   specialFilterValue: this.specialFilterValue,
    //   smartSearchOpt: [{
    //     object: 'search',
    //     joinType: "AND",
    //     criteria: 'LIKES',
    //     value: searchData ? searchData : '',
    //   }]
    // };

    this._apiService.post(environment.get_Branch_List, data).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.branchList1 = [...this.branchList1, ...resp['response']];
        this.branchList = [...this.branchList, ...this.branchList1];
        this.totalRows = resp['otherInfo'].totalRows
      }
    });
  }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  updateSelectedData = (selectedData) => {
    this.selectedData = selectedData;
  }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  emitSelectedData(data) {
    this.filterData.emit(data);
  }


  // ------------------------------------------------------------
  //  
  // ------------------------------------------------------------
  closeModal = () => {
    this.filterData.emit('close');
  }
}
