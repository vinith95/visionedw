import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {
  @Input() response;
  @Input() header1;
  @Input() header2;
  @Input() header3;
  @Input() title;
  @Output() closePopup: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    if(this.header1 == null
    || this.header1 ==""  ){
      this.header1 = "Field Name";
    }

    if(this.header2 == null
      || this.header2 ==""  ){
        this.header2 = "Approve";
    }
    if(this.header3 == null
        || this.header3 ==""  ){
          this.header3 = "Pending";
    }
    if(this.title == null
      || this.title ==""  ){
        this.title = "Review";
    }
  }
  closeModal = () => {
    this.closePopup.emit('close');
  }

}
