import { Component, OnInit, HostListener, Input, Output, EventEmitter, ViewChild, ChangeDetectorRef, OnChanges, AfterViewInit, DoCheck, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { ApiService, GlobalService } from '../../service';
import { isNullOrUndefined } from 'util';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { DndDropEvent } from 'ngx-drag-drop';
import { VirtualScrollerComponent } from 'ngx-virtual-scroller';
import { GroupingService } from '../../service/grouping-component/grouping.service';
import { PerfectScrollbarComponent, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { environment } from '../../../environments/environment';
import { element } from 'protractor';
import { filter } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { GenericPopupComponent } from '../generic-popup/generic-popup.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.css']
})
export class DynamicTableComponent implements OnInit, OnChanges, AfterViewInit, DoCheck, OnDestroy {
  screentypeToolTip: any = "maximize";
  screentype = 'fullscreen';
  compact_css: boolean = false;
  paddingCount: number = 0;
  totalValue: any;
  commonValue: any;
  overallFilterData: any = [];
  overallFilterViewData: any;
  @Input() settings;
  @Input() exportReq;
  maintainData;
  messageStr = 'Column Grouping not allowed for this report. Allowed only for report <='
  @Output() drillDownData: EventEmitter<any> = new EventEmitter();
  @ViewChild('perfectScroll', { static: false }) perfectScroll: VirtualScrollerComponent;
  @ViewChild('highLightRow', { static: false }) highLightRow;
  overallFilterValue: any;
  viewData: any = [];
  perPage: any;
  limitedPagination: any = {
    totalItems: 0,
    startIndex: 0,
    lastIndex: 0
  };


  overAllListCount: any = 0;
  previousDisp: boolean = false;
  nextDisp: boolean = false;

  tableData: any = [];
  temptableData: any = [];
  adVsettings = 'columnView';
  sortingForm: FormGroup;
  filteringForm: FormGroup;
  toggle: any = [];
  selectedHeader: any = [];
  groupingData: any = [];
  header: any = [];
  overallVieport = true;
  droppedItem: any = [];
  pinningLeft = false;
  pinningCenter = true;
  pinningRight = false;
  showSearch = false;
  tableDetails: any;
  data: any;
  valueHeader: any = [];
  showSearchR: boolean = false;
  widthPx: string;
  adVsettingsColumn: boolean = true;
  adVsettingsSorting: boolean = false;
  adVsettingsFilter: boolean = false;
  flexHeaders: any;
  scrollPostion: any = { x: 0, y: 0 };
  gridFr: string;
  firstRow = true;
  remainRow = false;
  groupingFlag: any;
  reportOrientation: any;
  toggleNow: any = 'first';
  filterOn: boolean = false;
  headerWidthPx: string;
  response: any;
  currentPage: any;
  goToPage: any = 1;
  columnTotal = [];
  arrayObj: any = [];
  grandTotalCaption: any;
  applyGroupingFlag: boolean = false;
  groupingAvailable: boolean = true;
  @Input() set tableDatas(value: any) {
    this.maintainData = value;
    this.valueHeader = value["colArrayHeader"];
    this.tableData = value["gridDataSet"];
    this.temptableData = value["gridDataSet"];
    this.flexHeaders = value["columnHeaderslst"];
    this.response = value;
    this.columnTotal = !isNullOrUndefined(value['total']) ? value['total'] : [];
    this.grandTotalCaption = !isNullOrUndefined(value['grandTotalCaption']) ? value['grandTotalCaption']: '';
    this.reportOrientation = isNullOrUndefined(value["reportOrientation"]) ? 'P' : value["reportOrientation"];
  }
  constructor(
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private globalService: GlobalService,
    private gpService: GroupingService,
    private apiService: ApiService,
    private modalService: NgbModal
  ) {
    this.sortingList();
    this.filterList();

  }

  ngOnInit() {
    this.afterNgChanges();
    this.globalService.getsideBarChangesDectect().subscribe((params)=>{
      if(params){
        setTimeout(()=>{
        this.headerWidthinPx();
        },500)
      }
    })
    this.globalService.gettopBarChangesDectect().subscribe((params) => {
      if (params['type'] == 'reportSlide') {
        this.toggleSideFilter();
      }
    })
  }
  ngDoCheck() {

  }
  // ------------------------------------------------------------
  ngOnChanges() {

  }
  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  afterNgChanges() {
    this.perPage = this.response['maxRecords'];
    this.groupingFlag = this.maintainData['groupingFlag'];
    this.applyGroupingFlag = this.maintainData['applyGroupingFlag'];
    this.valueHeader.forEach(element => {
      element.forEach(element1 => {
        if (element1.colspan <= 1) {
          this.header.push(element1)
        }
      });
    });
    this.header.sort((a, b) => parseFloat(a.labelColNum) - parseFloat(b.labelColNum));
    this.grandTotalCaption != '' ? this.columnTotal.length ?
    this.columnTotal[0][this.header[0]['dbColumnName']] = this.grandTotalCaption : '' : '';

    if(!isNullOrUndefined(this.maintainData.displayColumn) && this.maintainData.displayColumn.length) {
      this.header.forEach(head=> {
        this.maintainData.displayColumn.forEach(disp=> {
          if(disp.dbColumnName == head.dbColumnName) {
            head.selected = disp.selected
            head.selected1 = disp.Selected1
          }
        })
      })
      this.updateWidthContent();
      setTimeout(() => {
        this.updateGridFr();
        this.headerWidthinPxColDisp();
      }, 60);
    }
    this.overallVieport = this.header.every(ele => ele.selected1);

    if (this.maintainData["pageNum"] > 1) {
      this.goToPage = this.maintainData["pageNum"]
      this.submitPage();
    }
    if (this.maintainData['resetEverything']) {
      this.groupingData = [];
      this.droppedItem = [];
      this.grouping();
      this.header.forEach(element => {
        element.grouping = false;
      });
      this.flexHeaders.forEach(element => {
        element.grouping = false;
      });
    }
    this.data = this.settings;
    //this.data.filter=this.maintainData['sortFlag']
    this.data.sorting = this.maintainData['sortFlag']
    this.data.search = this.maintainData['searchFlag']
    this.updateTable();

    if (!isNullOrUndefined(this.tableData)) {
      this.viewData = (this.data.pagination == 'Y') ? this.tableData.slice(0, this.perPage) : this.tableData;
      this.getInitialAmount();
      this.resetFilter();
      //this.overAllListCount = this.tableData.length;
      this.overAllListCount = this.response['totalRows'];
      this.paginationDisable();
      setTimeout(() => {
        //this.limitedPagination['lastIndex'] = this.maintainData.pageNum * this.perPage;
        //this.limitedPagination['startIndex'] = this.limitedPagination['lastIndex'] - this.perPage;
        this.paginationDisable();
        //this.viewData = (this.data.pagination == 'Y') ? this.tableData.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']) : this.tableData;
        this.perfectScroll.scrollToPosition(this.maintainData.scrollYx.y, 0);
        setTimeout(() => {
          if (!isNullOrUndefined(this.maintainData.rowId)) {
            this.viewData.forEach((element, index) => {
              if (element['INDEXING'] == this.maintainData.rowId) {
                this.breadCrumbText(element, 'mouseover', index, true)
              }
            });
            setTimeout(() => {
              if (!isNullOrUndefined(document.getElementById(this.maintainData.subReportId + '-highLightRow' + this.maintainData.rowId)))
                document.getElementById(this.maintainData.subReportId + '-highLightRow' + this.maintainData.rowId).classList.add('hoverClassRow');
            }, 60);
          }
          // Filter Start //
          if (this.maintainData.filterData.length) {
            for (let index = 0; index < this.maintainData.filterData.length; index++) {
              if (index != 0) {
                this.fillArr.push(this.initFilterArry());
              }
            }
            this.filteringForm.patchValue({
              filterArray: this.maintainData.filterData
            })
            // this.submitSort(false);
          }
          // Sort Start //
          if (this.maintainData.sortArray.length) {
            for (let index = 0; index < this.maintainData.sortArray.length; index++) {
              if (index != 0) {
                this.sortArr.push(this.initSortArry());
              }
            }
            this.sortingForm.patchValue({
              sortArray: this.maintainData.sortArray
            })
            // this.submitSort(false);
          }

          if(this.maintainData.filterData.length || this.maintainData.sortArray.length) {
            this.setPatchValues();
          }
          // Grouping Start //
          if (this.maintainData.droppedItem.length) {
            this.droppedItem = this.maintainData.droppedItem;
            this.header.forEach(element => {
              this.droppedItem.forEach(element1 => {
                if (element1.id == element.id) {
                  element.grouping = true;
                }
              });
            });
            this.grouping();
            this.groupingColor();
          }
          else {
            if (this.maintainData.templateId != 'MULTIPLE') {
              if (this.response.maxRecords >= this.response.totalRows) {
              this.header.forEach(element => {
                if (element.groupingFlag == "D") {
                  this.droppedItem.push(element);
                  element.grouping = true;
                }
              });
              this.grouping();
              this.groupingColor();
              if (this.droppedItem.length) {
                let data = {
                  type: 'droppedItem',
                  droppedItem: this.droppedItem,
                  val: false
                }
                this.drillDownData.emit(data)
              }
            }
            }
          }
          this.groupingAvailable = this.header.some(ele=> ele.groupingFlag == 'Y')
        }, 60);
      }, 60);
      this.updateGridFr();
      this.headerWidthinPx();
    }
    else {
      this.viewData = [];
    }
    // if (!this.maintainData['resetEverything']) {
    //   this.arrayObj = this.maintainData['sortedArray'];
    //   this.submitSortPDF();
    // }
    setTimeout(() => {
      this.sideFilterPosition();
      this.sideFilterHeight();
    }, 120)
  }

  breadCrumbText(item, val, id, uni) {
    if (this.maintainData.templateId != 'MULTIPLE') {
      let data;
      if (!uni) {
        this.viewData.forEach((element, index) => {
          if (!isNullOrUndefined(document.getElementById(this.maintainData.subReportId + '-highLightRow' + element["INDEXING"])))
            document.getElementById(this.maintainData.subReportId + '-highLightRow' + element["INDEXING"]).classList.remove('hoverClassRow');
        });
        let ele = document.getElementById(this.maintainData.subReportId + '-highLightRow' + id);
        if (ele.classList.contains('hoverClassRow')) {
          ele.classList.remove('hoverClassRow');
        }
        else {
          ele.classList.add('hoverClassRow');
        }
      }
      if (val == 'mouseover') {
        data = {
          item: item,
          intReportSeq: this.maintainData['tileSequenceId'],
          type: 'breadCrumb',
          val: 'mouseover'
        }
      }
      else {
        data = {
          item: item,
          intReportSeq: this.maintainData['tileSequenceId'],
          type: 'breadCrumb',
          val: 'mouseout'

        }
      }
      this.drillDownData.emit(data)
    }
  }

  // ------------------------------------------------------------
  // previous() {
  //   this.limitedPagination['startIndex'] = this.limitedPagination['startIndex'] - (this.perPage);
  //   this.limitedPagination['lastIndex'] = this.limitedPagination['lastIndex'] - (this.perPage);
  //   this.viewData = this.tableData.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
  //   this.perfectScroll.scrollToIndex(0);
  //   this.headerWidthinPx();
  //   this.paginationDisable();
  // }

  // // ------------------------------------------------------------
  // next() {
  //   this.limitedPagination['startIndex'] = this.limitedPagination['startIndex'] + (this.perPage);
  //   this.limitedPagination['lastIndex'] = this.limitedPagination['lastIndex'] + (this.perPage);
  //   this.viewData = this.tableData.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
  //   (this.viewData.length < this.perPage && this.tableData.length < this.overAllListCount) ? this.nextDataList() : '';
  //   this.paginationDisable();
  // }
  previous() {
    this.limitedPagination['startIndex'] = this.limitedPagination['startIndex'] - (this.perPage);
    this.limitedPagination['lastIndex'] = this.limitedPagination['lastIndex'] - (this.perPage);
    let details = this.exportReq.reportRequest;
    details['currentPage'] = this.maintainData.currentPage - 1;
    details['promptTree'] = this.maintainData.promptTree;
    details['totalRows'] = this.maintainData.totalRows;
    this.apiService.post(environment.getRMReport, details).subscribe(resp => {
      if (resp['status']) {
        let list = resp['response'] != null ? resp['response']['gridDataSet'] : [];
        // this.currentPage = resp['otherInfo']['currentPage'];
        this.maintainData.currentPage = resp['otherInfo']['currentPage'];
        this.tableData = list;
        this.viewData = this.tableData;
        this.perfectScroll.scrollToIndex(0);
        setTimeout(() => {
          this.headerWidthinPx();
        }, 60)
        this.paginationDisable();
      }
      else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }

  next() {
    this.limitedPagination['startIndex'] = this.limitedPagination['startIndex'] + (this.perPage);
    this.limitedPagination['lastIndex'] = this.limitedPagination['lastIndex'] + (this.perPage);
    let details = this.exportReq.reportRequest;
    details['promptTree'] = this.maintainData.promptTree;
    details['currentPage'] = this.maintainData.currentPage + 1;
    details['totalRows'] = this.maintainData.totalRows;
    this.apiService.post(environment.getRMReport, details).subscribe(resp => {
      if (resp['status']) {
        let list = resp['response'] != null ? resp['response']['gridDataSet'] : [];
        // this.currentPage = resp['otherInfo']['currentPage'];
        this.maintainData.currentPage = resp['otherInfo']['currentPage'];
        this.tableData = list;
        this.viewData = this.tableData;
        this.perfectScroll.scrollToIndex(0);
        setTimeout(() => {
          this.headerWidthinPx();
        }, 60)
        this.paginationDisable();
      }
      else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }

  submitPage() {
    let maxPage = Math.ceil(this.response.totalRows / this.response.maxRecords);
    if (parseInt(this.goToPage) <= maxPage) {
      let details = this.exportReq.reportRequest;
      details['promptTree'] = this.maintainData.promptTree;
      details['currentPage'] = parseInt(this.goToPage);
      details['totalRows'] = this.maintainData.totalRows;
      this.apiService.post(environment.getRMReport, details).subscribe(resp => {
        if (resp['status']) {
          let list = resp['response'] != null ? resp['response']['gridDataSet'] : [];
          // this.currentPage = resp['otherInfo']['currentPage'];
          this.maintainData.currentPage = resp['otherInfo']['currentPage'];
          this.tableData = list;
          this.limitedPagination['startIndex'] = (this.goToPage * this.response.maxRecords) - this.response.maxRecords;
          this.limitedPagination['lastIndex'] = this.goToPage * this.response.maxRecords;
          this.viewData = this.tableData;
          //this.perfectScroll.scrollToIndex(0);
          setTimeout(() => {
            this.headerWidthinPx();
          }, 60)
          this.paginationDisable();
        }
        else {
          this.globalService.showToastr.error(resp['message']);
        }
      });
    }
    else {
      this.globalService.showToastr.warning("No Records Found")
    }
  }
  // ------------------------------------------------------------
  paginationDisable() {
    this.previousDisp = (this.limitedPagination['startIndex'] == 0) ? true : false;
    this.nextDisp = (this.limitedPagination['lastIndex'] >= this.overAllListCount) ? true : false;
  }

  // ------------------------------------------------------------
  listCountDisp() {
    this.perPage = parseInt(this.perPage);
    this.limitedPagination['startIndex'] = 0;
    this.limitedPagination['lastIndex'] = this.perPage;
    this.viewData = this.tableData.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
    this.paginationDisable();
  }

  // ------------------------------------------------------------
  getPaginationDet() {
    let first = parseInt(this.limitedPagination['startIndex']) + 1;
    let last = (this.limitedPagination['lastIndex'] > this.overAllListCount) ? this.overAllListCount :
      this.limitedPagination['lastIndex'];
    let aa = `${first} - ${last}  of
      ${this.overAllListCount}`;
    return aa;
  }

  getValue(arr) {
    let totalArray = arr.map(ele => parseFloat(ele.amount));
    this.totalValue = totalArray.reduce((a, b) => a + b, 0);
  }

  getValueReturn(element) {
    let totalArray = this.tableData.map(ele => parseFloat(ele[element]));
    return totalArray.reduce((a, b) => a + b, 0)
    // if(isNullOrUndefined(element.decimalCnt) || element.decimalCnt == ''){
    //   return Number(parseFloat(totalArray)).toLocaleString('en', {minimumFractionDigits: 2,maximumFractionDigits: 2})
    // }
    // else{
    //  return Number(parseFloat(totalArray)).toLocaleString('en',
    //  {minimumFractionDigits: element.decimalCnt,maximumFractionDigits: element.decimalCnt})
    // }
  }

  getValueReturn1(arr, element) {
    let totalArray = arr.map(ele => parseFloat(ele[element]));
    return totalArray.reduce((a, b) => a + b, 0)
  }

  loopArray(arr) {
    arr.forEach(element => {
      if (element.children.length) {
        this.loopArray(element.children)
      }
      else {
        element.dataSet.forEach(element1 => {
          return element1;
        });
      }
    });
  }

  getValueReturn2(arr, name) {
    if (arr.length) {
      let sum = 0;
      arr.forEach(element => {
        if (element.children.length) {
          sum += this.getValueReturn2(element.children, name);
        }
        else {
          element.dataSet.forEach(element1 => {
            sum += parseFloat(element1[name]);
          });
        }
      });
      return sum;
      // if(isNullOrUndefined(name.decimalCnt) || name.decimalCnt == ''){
      //   return sum.toLocaleString('en', {minimumFractionDigits: 2,maximumFractionDigits: 2})
      // }
      // else{
      //  return sum.toLocaleString('en',
      //  {minimumFractionDigits: name.decimalCnt,maximumFractionDigits: name.decimalCnt})
      // }
    }
  }

  getInitialAmount() {
    this.getValue(this.tableData);
  }

  ngAfterContentInit() {
    this.updateTable();
  }

  drop(event: CdkDragDrop<string[]>, type) {
    let arr = (type == 'group') ? this.droppedItem : this.header;
    moveItemInArray(arr, event.previousIndex, event.currentIndex);
    this.grouping();
    this.settBoxFalse();
  }

  headerWidthinPx() {
    if (!isNullOrUndefined(this.highLightRow)) {
      setTimeout(() => {
        if (!isNullOrUndefined(this.highLightRow))
        this.headerWidthPx = this.highLightRow.nativeElement.offsetWidth + 'px';
      });
    }
    else {
      setTimeout(() => {
        this.headerWidthPx = "100%";
      });
    }
  }

  headerWidthinPxColDisp() {
    if (!isNullOrUndefined(this.highLightRow) && !this.droppedItem.length) {
      setTimeout(() => {
        if (!isNullOrUndefined(this.highLightRow))
        this.headerWidthPx = this.highLightRow.nativeElement.offsetWidth + 'px';
      });
    }
    else {
      setTimeout(() => {
        this.headerWidthPx = "100%";
      });
    }
  }

  toggleFn(item, allItems) {
    let index = allItems.indexOf(item);
    allItems.forEach((element, i) => {
      if (i == index) {
        element.toggle = !element.toggle;
      }
      else {
        element.toggle = false;
      }
    });
    this.updateTable();
    //this.groupingData.map(i => item.toggle =false);

    // let splitData=dataId.split(',');
    // let index =this.groupingData.indexOf(item);
    // this.groupingData.forEach((element,i) => {
    //   if(i == index){
    //    element.toggle =! element.toggle;
    //   }
    //   else{
    //    element.toggle=false;
    //   }
    // });

  }
  @HostListener('document:click', ['$event'])
  @HostListener('document:touchstart', ['$event'])
  handleOutsideClick(event) {
    if (!isNullOrUndefined(event.target)) {
      if (!event.target.classList.contains('pinningBox')) {
        this.settBoxFalse();
      }
      const sideFilterOverlay = document.getElementById('sideFilterOverlay');
      if(!isNullOrUndefined(sideFilterOverlay) && event.target.closest(".firstLevelMenuIcon")) {
        sideFilterOverlay['style']['z-index'] = '152';
      }
      // if (event.target.classList.contains('toggle-side-filter') || event.target.classList.contains('fa-caret-left')) {
      //   this.toggleSideFilter();
      // }
      // else {
      //   if (event.target.closest('.side-filter-hold') || event.target.closest('ng-dropdown-panel.sorting-move') ||
      //     event.target.classList.contains('sidebar-mini') || event.target.classList.contains('addColumnValue-02')) {
      //     return
      //   }
      //   const sideFilter = document.getElementById('side-filter-hold');
      //   // const sideArrowClass = document.getElementById('sidebarArrow');
      //   const sideFilterOverlay = document.getElementById('sideFilterOverlay');
      //   // const sideIcon = document.getElementById('side-filter-icon');
      //   if (!isNullOrUndefined(sideFilter)) {
      //     if (sideFilter.classList.contains('setRight')) {
      //       // sideIcon.style.cssText = `right: -2px; z-index: 1000;`;
      //       sideFilterOverlay.classList.remove('show', 'z-154');
      //       sideFilter.classList.remove('setRight');
      //       // sideArrowClass.classList.remove('fa-angle-double-right');
      //     }
      //   }
      // }
    }

  }

  settBoxFalse() {
    this.header.forEach(ele => { ele.settingBox = false });
  }

  updateTable() {
    this.toggle = this.groupingData.map(i => true);
  }

  onDropGrid(e: DndDropEvent) {
    if (this.maintainData.templateId != 'MULTIPLE') {
      if (e.data.colType == 'T') {
        let check = this.header.filter((item) => item.dbColumnName == e.data.dbColumnName);
        if (check.length) {
          let pos = this.droppedItem.map(ele => ele.id).indexOf(e.data.id);
          if (pos == -1) {
            this.droppedItem.push(e.data);
            this.grouping();
          }
          this.groupingColor();
        }
        else {
          this.globalService.showToastr.error("Cannot group this Column")
        }
      }
      else {
        this.globalService.showToastr.error("Cannot group this Column")
      }
      if (this.droppedItem.length) {
        let data = {
          type: 'droppedItem',
          droppedItem: this.droppedItem,
          val: false
        }
        this.drillDownData.emit(data)
      }
      document.getElementsByClassName('grouping-parent')[0].classList.remove('drag-started');
    }
  }

  groupingColor() {
    this.header.forEach(el => {
      el.grouping = false;
      let pos = this.droppedItem.map(ele => ele.id).indexOf(el.id);
      if (pos != -1) {
        el.grouping = true;
      }
    });
    let firstRow = this.header.filter(item => (item.colType == 'T' || item.colType == 'TR') && item.selected && !item.grouping)
    if (firstRow.length) {
      this.header.forEach(element1 => {
        if (element1.dbColumnName == firstRow[0].dbColumnName) {
          element1.firstRow = true;
        }
        else {
          element1.firstRow = false;
        }
      });
    }
  }

  dndDrag(type) {
    let dom = document.getElementsByClassName('grouping-parent')[0];
    if (!isNullOrUndefined) {
      (type == 'start') ?
        dom.classList.add('drag-started') : dom.classList.remove('drag-started');
    }

  }

  remove(rem) {
    if (this.maintainData.templateId != 'MULTIPLE') {
      this.droppedItem.splice(rem, 1);
      if (!this.droppedItem.length) {
        let data = {
          type: 'droppedItem',
          droppedItem: this.droppedItem,
          val: true
        }
        this.drillDownData.emit(data)
      }
      this.getInitialAmount();
      this.updateTable();
      this.grouping();
      this.groupingColor();
    }
    //this.getInitialAmount();
  }

  setpinningDiv(id, head) {
    if (document.getElementById(id)) {
      let width = document.getElementById(id).parentNode['offsetWidth'];
      let left = document.getElementById(id).parentNode['offsetLeft'];
      let current = width + left + width;
      let parentWidth = document.getElementsByClassName('table-responsive')[0]['offsetWidth'];
      if (current > parentWidth) {
        return 4;
      }
    }

    return -145;
  }

  opcloseAdSettings(type) {
    let settingId = document.getElementById('advanceSettings');
    if (type == 'open') {
      settingId.style.display = 'block';
      setTimeout(() => {
        settingId.style.width = '400px';
      }, 1);
    } else {
      settingId.style.width = '0px';
      setTimeout(() => {
        settingId.style.display = 'none';
      }, 200);
    }
  }

  overallVPCheckClk(type) {
    if (type == 'overAll') {
      this.header.forEach(ele => { ele.selected1 = this.overallVieport; });
    } else {
      this.overallVieport = this.header.every(ele => ele.selected1);
    }

  }

  hideColumn() {
    this.header.forEach(head=> {
      head.selected = head.selected1
    })
    let colspanArry = this.flexHeaders.filter(item => item.colspan > 1);
    colspanArry.forEach(element => {
      let sum = element.labelColNum + element.colspan;
      let checkFlag = 0
      this.header.forEach(element1 => {
        if (element.labelColNum <= element1.labelColNum && element1.labelColNum < sum) {
          if (element1.selected) {
            checkFlag++
          }
        }
      });
      if (checkFlag == 0) {
        element.selected = false
      }
      else {
        element.selected = true
      }
    });
    let firstRow = this.header.filter(item => (item.colType == 'T' || item.colType == 'TR') && item.selected && !item.grouping)
    if (firstRow.length) {
      this.header.forEach(element1 => {
        if (element1.dbColumnName == firstRow[0].dbColumnName) {
          element1.firstRow = true;
        }
        else {
          element1.firstRow = false;
        }
      });
    }
    if (this.maintainData.templateId != 'MULTIPLE') {
      if (!this.droppedItem.length) {
        let data = {
          type: 'droppedItem',
          val: true
        }
        this.drillDownData.emit(data)
      }
    }
    //this.removeChip();
    this.updateWidthContent();
    setTimeout(() => {
      this.updateGridFr();
      this.headerWidthinPxColDisp();
    }, 60);
    let columns = [];
    columns = this.header.filter(item=> !item.selected)
    let col = {
      type: 'displayColumn',
      columns: columns
    }
    this.drillDownData.emit(col);
  }

  removeChip() {
    this.header.forEach(element => {
      if (!element.selected) {
        let pos = this.droppedItem.map(el => el.id).indexOf(element.id);
        if (pos != -1) {
          this.droppedItem.splice(pos, 1);
          this.grouping();
        }
        this.removeSort(element);
      }
    });
  }

  grouping() {
    let filteredItem = (this.droppedItem).map(ele => ele.id);
    let groupGrid = JSON.parse(JSON.stringify(this.viewData));    //copy of orginal array
    if (filteredItem.length) {
      if (groupGrid.length) {
        let result = [],
          temp = { _: result };
        for (var i = 0; i <= groupGrid.length - 1; i++) {
          let ele = groupGrid[i];
          let curArr = [];
          filteredItem.reduce((acc, currentValue, currentIndex) => {
            curArr.push(ele[currentValue]);
            if (!acc[ele[currentValue]]) {
              acc[ele[currentValue]] = { _: [] };
              let obj = {
                id: currentValue,
                toggle: false,
                pre: currentIndex > 0 ? ele[filteredItem[currentIndex - 1]] : 'first',
                [currentValue]: ele[currentValue],
                ['children']: acc[ele[currentValue]]._,
                ['dataSet']: this.filterdataSet(groupGrid, curArr, filteredItem)
              }
              acc._.push(obj);
            }
            return acc[ele[currentValue]];
          }, temp)._;
          if (i == groupGrid.length - 1) {
            this.groupingData = result;
          }
        }

      }
      this.updateWidthContent();
    } else {
      this.refresh();
    }
    setTimeout(() => {
      this.updateGridFr();
      this.headerWidthinPx();
    }, 60);
  }

  filterdataSet(array, curArr, filteredItem) {
    let arr = [];
    if (curArr.length == filteredItem.length) {
      arr = array.filter(ele => this.grpFilter(ele, filteredItem) == curArr.join(','));
    }
    // if(this.data.pagination == 'Y'){
    //   return arr.slice(0, 10)
    // }
    // else{
    return arr;
    //}

  }

  grpFilter(ele, filteredItem) {
    let arr = [];
    for (var i = 0; i <= filteredItem.length - 1; i++) {
      arr = [...arr, ele[filteredItem[i]]]
    }
    return arr.join(',')
  }

  refresh() {
    this.groupingData = [];
  }


  //sorting
  sortingList() {
    this.sortingForm = this.fb.group({
      sortArray: this.fb.array([this.initSortArry()])
    })
  }

  get sortArr() {
    return this.sortingForm.controls.sortArray as FormArray;
  }

  initSortArry() {
    return this.fb.group({
      column: ['', Validators.required],
      type: [''],
    })
  }

  displayHeaderCol(list, index, form, detail, selectedCol, typeName) {
    let colList = this[form].value[detail].filter((ind) => { return ind != index });
    let listedCol = colList.map(ele => { return ele[selectedCol] });
    return (listedCol.length) ? list.filter(ele => { return !listedCol.includes(ele[typeName]) }) : list;
  }
  addSortDisabled: any = false;
  addSortArry() {
    if (!this.sortingForm.valid) {
      return false;
    }
    this.sortArr.push(this.initSortArry());
    this.disableSortAdd();
  }

  disableSortAdd() {
    let sortArr = (this.sortingForm.value.sortArray).length - 1;
    let headerArr = this.header.filter(ele => ele.selected).length - 1;
    this.addSortDisabled = (sortArr >= headerArr);
  }

  deleteSortArry(i) {
    this.sortArr.removeAt(i);
    this.disableSortAdd();
    //let sortArr = this.sortingForm.value.sortArray;
    //let sort = sortArr.filter(item => item.type.length);
    // if (this.response.maxRecords >= this.response.totalRows) {
    //   if (sort.length) {
    //     let reqArry = {};
    //     for (var j = 0; j <= sort.length - 1; j++) {
    //       let obj = Object.entries(sort[j]);
    //       let reqObj = { [`${obj[0][1]}`]: obj[1][1] };
    //       reqArry = { ...reqArry, ...reqObj };
    //       if (j == sort.length - 1) {
    //         this.viewData = this.gpService.orderBy(reqArry, JSON.parse(JSON.stringify(this.temptableData)), this.header);
    //         this.submitSort(false);
    //         // this.viewData = this.tableData;
    //         // this.grouping();
    //       }
    //     }
    //   }
    // }
  }

  removeSort(element) {
    if (this.sortingForm.value.sortArray.length) {
      let pos = this.sortingForm.value.sortArray.map(ele => ele.column).indexOf(element.id);
      if (pos != -1) {
        this.deleteSortArry(pos);
      }
    }
  }

  submitSortPDF() {
    if (!isNullOrUndefined(this.arrayObj) && this.arrayObj.length) {
      this.arrayObj.forEach((element, index) => {
        if (index != 0) {
          this.sortArr.push(this.initSortArry());
        }
        this.sortArr.at(index).patchValue({
          "column": element.column,
          "type": element.type
        })
      })
      let reqArry = {};
      let sortColumn = 'ORDER BY ';
      this.arrayObj.forEach((ele, ind) => {
        sortColumn += `${ele.column} ${ele.type.toUpperCase()}`;
        if (this.arrayObj.length - 1 != ind) {
          sortColumn += ',';
        }
      });
      let data = {
        sorted: true,
        type: 'sorting',
        sortData: sortColumn,
        sortArray: this.arrayObj
      }
      this.drillDownData.emit(data)
      if (this.response.maxRecords >= this.response.totalRows) {
        for (var i = 0; i <= this.arrayObj.length - 1; i++) {
          // if ((this.arrayObj[i].type).length) {
          let obj = Object.entries(this.arrayObj[i]);
          let reqObj = { [`${obj[0][1]}`]: obj[1][1] };
          reqArry = { ...reqArry, ...reqObj };
          if (i == this.arrayObj.length - 1) {
            this.viewData = this.gpService.orderBy(reqArry, this.viewData, this.header);
            // this.viewData = this.tableData;
            this.grouping();
          }
          // }
        }
      }
      else {
        if (this.arrayObj.length && this.data.sorting == 'Y') {
          let sortRequest = this.exportReq.reportRequest;
          sortRequest['currentPage'] = 1;
          sortRequest['screenSortColumn'] = sortColumn;
          this.apiService.post(environment.getRMReport, sortRequest).subscribe(resp => {
            if (resp['status']) {
              let list = resp['response'] != null ? resp['response']['gridDataSet'] : [];
              this.maintainData.currentPage = resp['otherInfo']['currentPage'];
              this.tableData = list;
              this.viewData = this.tableData;
              this.perfectScroll.scrollToIndex(0);
              setTimeout(() => {
                this.headerWidthinPx();
              }, 60)
              this.listCountDisp();
              this.paginationDisable();
            }
            else {
              this.globalService.showToastr.error(resp['message']);
            }
          });
        }
      }
    }

  }

  submitSort(flag) {
    let tempDroppedItem = [];
    if (this.droppedItem.valid) {
      tempDroppedItem = this.droppedItem;
      this.droppedItem = [];
      this.grouping();
    }
    let filter = this.filteringForm.value.filterArray;
    let filterData = filter.filter(item => !isNullOrUndefined(item.id) && item.id.length && !isNullOrUndefined(item.type) &&
    item.type.length && !isNullOrUndefined(item.filterData) && item.filterData.length)
    let sortArr = this.sortingForm.value.sortArray;
    this.arrayObj = sortArr.filter(item => !isNullOrUndefined(item.column) && item.column.length &&
    !isNullOrUndefined(item.type) && item.type.length);
    let smartSearchOpt = [];
    filterData.forEach((ele) => {
      smartSearchOpt.push({ object: ele.id, criteria: ele.type, value: ele.filterData })
    })
    let sortColumn = 'ORDER BY ';
    this.arrayObj.forEach((ele, ind) => {
      sortColumn += `${ele.column} ${ele.type.toUpperCase()}`;
      if (this.arrayObj.length - 1 != ind) {
        sortColumn += ',';
      }
    });
    // if (this.response.maxRecords >= this.response.totalRows) {
      // if (filterData.length) {
      //   this.header.forEach(ele => { ele.filterData = '' });
      //   let arrObj = [];
      //   let data = {
      //     sorted: true,
      //     type: 'filter',
      //     smartSearchOpt: smartSearchOpt,
      //     filterData: filterData
      //   }
      //   this.drillDownData.emit(data)
      //   for (var i = 0; i <= filter.length - 1; i++) {
      //     if (filter[i]['filterData'].length) {
      //       arrObj.push({ [filter[i]['id']]: filter[i]['filterData'], types: filter[i]['type'] });
      //     }
      //     if (filter.length - 1 == i) {
      //       this.viewData = this.filterMethod(arrObj);
      //       // this.getValue(this.viewData);
      //       this.overAllListCount = this.viewData.length;
      //       this.tableData = this.viewData;
      //       this.listCountDisp();
      //       this.paginationDisable();
      //       this.grouping();
      //     }
      //   }
      // }
      // if (this.arrayObj.length) {
      //   let reqArry = {};
      //   let data = {
      //     sorted: true,
      //     type: 'sorting',
      //     sortData: sortColumn,
      //     sortArray: this.arrayObj
      //   }
      //   this.drillDownData.emit(data)
      //   for (var i = 0; i <= this.arrayObj.length - 1; i++) {
      //     // if ((this.arrayObj[i].type).length) {
      //     let obj = Object.entries(this.arrayObj[i]);
      //     let reqObj = { [`${obj[0][1]}`]: obj[1][1] };
      //     reqArry = { ...reqArry, ...reqObj };
      //     if (i == this.arrayObj.length - 1) {
      //       this.viewData = this.gpService.orderBy(reqArry, this.viewData, this.header);
      //       // this.viewData = this.tableData;
      //       this.grouping();
      //     }
      //     // }
      //   }
      // }
    // }
    // else {
      // if ((this.arrayObj.length && this.data.sorting == 'Y') ||
      //   (filterData.length && this.data.search == 'Y')) {
        let sortRequest = this.exportReq.reportRequest;
        this.hideColumn();
        if (filterData.length) {
          let data = {
            sorted: true,
            type: 'filter',
            smartSearchOpt: smartSearchOpt,
            filterData: filterData
          }
          this.drillDownData.emit(data)
        }
        if (this.arrayObj.length) {
          let data = {
            sorted: true,
            type: 'sorting',
            sortData: sortColumn,
            sortArray: this.arrayObj
          }
          this.drillDownData.emit(data)
        }
        let group = {
          type: 'applyGroupingFlag',
          applyGroupingFlag: this.applyGroupingFlag,
        }
        this.drillDownData.emit(group)
        sortRequest['currentPage'] = 1;
        sortRequest['screenSortColumn'] = this.arrayObj.length ? sortColumn : "";
        sortRequest['promptTree'] = this.maintainData.promptTree;
        sortRequest['smartSearchOpt'] = filterData.length ?smartSearchOpt : [];
        let header = this.header.filter(item=> item.colspan <= 1 && item.selected);
        let colTypeT = header.filter(item=> (item.colType == 'T' || item.colType == 'TR' || item.colType == 'NR'));
        let colTypeN = header.filter(item=> (item.colType == 'I' || item.colType == 'S' || item.colType == 'N'));
        let colTypeDimension = colTypeT && colTypeT.length ? colTypeT.map(item=> item.dbColumnName).join(',') :'';
        sortRequest['showDimensions'] = colTypeDimension;
        let groupMeasure = '';
        if(colTypeN && colTypeN.length)
        colTypeN.forEach((ele, ind)=> {
          groupMeasure += `SUM(${ele.dbColumnName}) ${ele.dbColumnName}`;
          if (colTypeN.length - 1 != ind) {
            groupMeasure += ',';
          }
        })
        let colTypeMeasure = colTypeN && colTypeN.length ? colTypeN.map(item=> item.dbColumnName).join(',') : '';
        sortRequest['showMeasures'] = this.applyGroupingFlag && groupMeasure != '' ? groupMeasure :
        colTypeMeasure;
        sortRequest['applyGrouping'] = this.applyGroupingFlag ? 'Y' : 'N';
        sortRequest['runType'] = 'S';
        this.apiService.post(environment.getRMReport, sortRequest).subscribe(resp => {
          if (resp['status'] && !isNullOrUndefined(resp['response'])) {
            let send = {
              type: 'advanceSubmit',
              data: resp
            }
            this.drillDownData.emit(send);
            this.setPatchValuesSubmit(resp['response'], 'submit');
            // let list = resp['response']['gridDataSet'] != null ? resp['response']['gridDataSet'] : [];
            this.maintainData.currentPage = resp['otherInfo']['currentPage'];
            // this.response = resp['response'];
            // this.columnTotal = !isNullOrUndefined(resp['response']['total']) ? resp['response']['total'] : [];
            // this.tableData = list;
            // this.viewData = this.tableData;
            // this.overAllListCount = resp['response']['totalRows'];
            // setTimeout(() => {
            //   this.headerWidthinPx();
            // }, 60)
            // this.listCountDisp();
            // this.paginationDisable();
            // this.grouping();
            // !isNullOrUndefined(this.perfectScroll.scrollToIndex) ? this.perfectScroll.scrollToIndex(0) :'';
          }
          else {
            this.globalService.showToastr.error(resp['message']);
          }
        });
      // }
    // }
    if (tempDroppedItem.length) {
      this.droppedItem = tempDroppedItem;
      this.grouping();
    }
    if (flag)
      this.toggleSideFilter();
  }

  resetSort() {
    this.sortArr.reset();
    this.sortArr.controls = [];
    this.sortingForm.value.displayDetails = [];
    this.sortArr.push(this.initSortArry());
    this.fillArr.reset();
    this.fillArr.controls = [];
    this.sortingForm.value.displayDetails = [];
    this.fillArr.push(this.initFilterArry());
    this.inlineFilter();
    //this.tableData = this.viewData;
    this.viewData = JSON.parse(JSON.stringify(this.temptableData));
    // this.getValue(this.tableData)
    this.grouping();
    let data = {
      sorted: false,
      type: 'sorting',
      sortData: '',
      sortArray: []
    }
    this.drillDownData.emit(data);
    let dataFilter = {
      sorted: true,
      type: 'filter',
      smartSearchOpt: [],
      filterData: []
    }
    this.drillDownData.emit(dataFilter)
    // this.header.forEach(ele => { ele.selected = true });
    // this.overallVieport = this.header.every(ele => ele.selected);
    this.overallVieport = true;
    this.applyGroupingFlag = false;
    this.overallVPCheckClk('overAll');
    // this.hideColumn();
    // if (this.response.maxRecords >= this.response.totalRows) {
    //   this.inlineFilter();
    //   //this.tableData = JSON.parse(JSON.stringify(this.temptableData));
    //   // this.grouping();
    // }
    // else {
    //   if (this.data.sorting == 'Y' || this.data.search == 'Y') {
        // let sortRequest = this.exportReq.reportRequest;
        // sortRequest['screenSortColumn'] = "";
        // sortRequest['smartSearchOpt'] = [];
        // sortRequest['currentPage'] = 1;
        // let header = this.header.filter(item=> item.colspan <= 1 && item.selected);
        // let colTypeT = header.filter(item=> (item.colType == 'T' || item.colType == 'TR' || item.colType == 'NR'));
        // let colTypeN = header.filter(item=> (item.colType == 'I' || item.colType == 'S' || item.colType == 'N'));
        // let colTypeDimension = colTypeT && colTypeT.length ? colTypeT.map(item=> item.dbColumnName).join(',') :'';
        // sortRequest['showDimensions'] = colTypeDimension;
        // let groupMeasure = '';
        // if(colTypeN && colTypeN.length)
        // colTypeN.forEach((ele, ind)=> {
        //   groupMeasure += `SUM(${ele.dbColumnName}) ${ele.dbColumnName}`;
        //   if (colTypeN.length - 1 != ind) {
        //     groupMeasure += ',';
        //   }
        // })
        // let colTypeMeasure = colTypeN && colTypeN.length ? colTypeN.map(item=> item.dbColumnName).join(',') : '';
        // sortRequest['showMeasures'] = this.applyGroupingFlag && groupMeasure != '' ? groupMeasure :
        // colTypeMeasure;
        // sortRequest['applyGrouping'] = this.applyGroupingFlag ? 'Y' : 'N';
        // this.apiService.post(environment.getRMReport, sortRequest).subscribe(resp => {
        //   if (resp['status'] && !isNullOrUndefined(resp['response'])) {
        //     let send = {
        //       type: 'advanceSubmit',
        //       data: resp
        //     }
        //     this.drillDownData.emit(send);
        //     this.setPatchValuesSubmit(resp['response'], 'reset');
        //     // let list = resp['response']['gridDataSet'] != null ? resp['response']['gridDataSet'] : [];
        //     this.maintainData.currentPage = resp['otherInfo']['currentPage'];
        //     // this.response = resp['response'];
        //     // this.tableData = list;
        //     // this.viewData = this.tableData;
        //     // this.overAllListCount = resp['response']['totalRows'];
        //     // setTimeout(() => {
        //     //   this.headerWidthinPx();
        //     // }, 60)
        //     // // this.listCountDisp();
        //     // this.paginationDisable();
        //     // !isNullOrUndefined(this.perfectScroll.scrollToIndex) ? this.perfectScroll.scrollToIndex(0) : '';
        //   }
        //   else {
        //     this.globalService.showToastr.error(resp['message']);
        //   }
        // });
    //   }
    // }
    // this.toggleSideFilter();
  }

  setPatchValues() {
    let tempDroppedItem = [];
    if (this.droppedItem.valid) {
      tempDroppedItem = this.droppedItem;
      this.droppedItem = [];
      this.grouping();
    }
    let filter = this.filteringForm.value.filterArray;
    let filterData = filter.filter(item => !isNullOrUndefined(item.id) && item.id.length && !isNullOrUndefined(item.type) &&
    item.type.length && !isNullOrUndefined(item.filterData) && item.filterData.length)
    let sortArr = this.sortingForm.value.sortArray;
    this.arrayObj = sortArr.filter(item => !isNullOrUndefined(item.column) && item.column.length &&
    !isNullOrUndefined(item.type) && item.type.length);
    let smartSearchOpt = [];
    filterData.forEach((ele) => {
      smartSearchOpt.push({ object: ele.id, criteria: ele.type, value: ele.filterData })
    })
    let sortColumn = 'ORDER BY ';
    this.arrayObj.forEach((ele, ind) => {
      sortColumn += `${ele.column} ${ele.type.toUpperCase()}`;
      if (this.arrayObj.length - 1 != ind) {
        sortColumn += ',';
      }
    });
    if (filterData.length) {
      let data = {
        sorted: true,
        type: 'filter',
        smartSearchOpt: smartSearchOpt,
        filterData: filterData
      }
      this.drillDownData.emit(data)
    }
    if (this.arrayObj.length) {
      let data = {
        sorted: true,
        type: 'sorting',
        sortData: sortColumn,
        sortArray: this.arrayObj
      }
      this.drillDownData.emit(data)
    }
    let group = {
      type: 'applyGroupingFlag',
      applyGroupingFlag: this.applyGroupingFlag,
    }
    this.drillDownData.emit(group)
  }

  setPatchValuesSubmit(response, type) {
    let tempDroppedItem = [];
    if (this.droppedItem.valid) {
      tempDroppedItem = this.droppedItem;
      this.droppedItem = [];
      this.grouping();
    }
    let filter = this.filteringForm.value.filterArray;
    let filterData = filter.filter(item => !isNullOrUndefined(item.id) && item.id.length && !isNullOrUndefined(item.type) &&
    item.type.length && !isNullOrUndefined(item.filterData) && item.filterData.length)
    let sortArr = this.sortingForm.value.sortArray;
    this.arrayObj = sortArr.filter(item => !isNullOrUndefined(item.column) && item.column.length &&
    !isNullOrUndefined(item.type) && item.type.length);
    let smartSearchOpt = [];
    filterData.forEach((ele) => {
      smartSearchOpt.push({ object: ele.id, criteria: ele.type, value: ele.filterData })
    })
    let sortColumn = 'ORDER BY ';
    this.arrayObj.forEach((ele, ind) => {
      sortColumn += `${ele.column} ${ele.type.toUpperCase()}`;
      if (this.arrayObj.length - 1 != ind) {
        sortColumn += ',';
      }
    });
    // if (filterData.length) {
      let data = {
        sorted: true,
        type: 'filter',
        smartSearchOpt: smartSearchOpt,
        filterData: filterData
      }
      this.drillDownData.emit(data)
    // }
    // if (this.arrayObj.length) {
      let sortdata = {
        sorted: true,
        type: 'sorting',
        sortData: this.arrayObj.length ? sortColumn : '',
        sortArray: this.arrayObj
      }
      this.drillDownData.emit(sortdata)
    // }
    let group = {
      type: 'applyGroupingFlag',
      applyGroupingFlag: this.applyGroupingFlag,
    }
    this.drillDownData.emit(group)
    if (tempDroppedItem.length) {
      this.droppedItem = tempDroppedItem;
      // this.grouping();
    }
    if (this.droppedItem.length && response.maxRecords >= response.totalRows) {
      let data = {
        type: 'droppedItem',
        droppedItem: type == 'reset' ? [] : this.droppedItem,
        val: false
      }
      this.drillDownData.emit(data)
    }
    let columns = [];
    columns = this.header.filter(item=> !item.selected)
    let col = {
      type: 'displayColumn',
      columns: columns
    }
    this.drillDownData.emit(col);
  }
  //filter

  filterList() {
    this.filteringForm = this.fb.group({
      filterArray: this.fb.array([this.initFilterArry()])
    })
  }


  get fillArr() {
    return this.filteringForm.controls.filterArray as FormArray;
  }

  initFilterArry() {
    return this.fb.group({
      id: ['', Validators.required],
      type: ['', Validators.required],
      filterData: ['', Validators.required],
      dataType: ['']
    })
  }

  filldataType(data, index) {
    if(!isNullOrUndefined(data)){
    let dataType = this.header.filter(el => el.dbColumnName == data)[0].colType;
    this.fillArr.at(index).patchValue({ dataType: dataType });
    }
  }

  addFilterArry() {
    if (!this.filteringForm.valid) {
      return false;
    }
    this.fillArr.push(this.initFilterArry());
  }


  deleteFilterArry(i) {
    this.fillArr.removeAt(i);
  }

  resetFilter() {
    this.fillArr.reset();
    this.fillArr.controls = [];
    this.sortingForm.value.displayDetails = [];
    this.fillArr.push(this.initFilterArry());
    this.inlineFilter();
    //this.tableData = this.viewData;
    this.tableData = JSON.parse(JSON.stringify(this.temptableData));
    // this.getValue(this.tableData)
    this.grouping();
  }

  filterDisabled() {
    return this.filteringForm.value.filterArray.some(ele => ele.filterData.length)
  }

  submitFilter() {
    this.header.forEach(ele => { ele.filterData = '' });
    let arrObj = [];
    let filter = this.filteringForm.value.filterArray;
    if (this.response.maxRecords >= this.response.totalRows) {
      for (var i = 0; i <= filter.length - 1; i++) {
        if (filter[i]['filterData'].length) {
          arrObj.push({ [filter[i]['id']]: filter[i]['filterData'], types: filter[i]['type'] });
        }
        if (filter.length - 1 == i) {
          this.viewData = this.filterMethod(arrObj);
          // this.getValue(this.viewData);
          this.overAllListCount = this.viewData.length;
          this.tableData = this.viewData;
          this.listCountDisp();
          this.paginationDisable();
          this.grouping();
        }
      }
    }
  }

  // removeFilter(element) {
  //   if (this.filteringForm.value.filterArray.length) {
  //     let pos = this.filteringForm.value.filterArray.map(ele => ele.column).indexOf(element.id);
  //     if (pos != -1) {
  //       this.deleteFilterArry(pos);
  //     }
  //   }
  // }

  //pinning
  pinningBol() {
    this.header.forEach(ele => { ele.settingBox = false });
    this.pinningCenter = this.header.some(ele => ele.pinning == 'center')
    this.pinningLeft = this.header.some(ele => ele.pinning == 'left')
    this.pinningRight = this.header.some(ele => ele.pinning == 'right');
  }



  settingPopup(head) {
    this.showSearch = false;
    this.showSearchR = false;
    for (var i = 0; i <= this.header.length - 1; i++) {
      this.header[i]['settingBox'] = false;
      if (i == this.header.length - 1) {
        head.settingBox = !head.settingBox;
      }
    }
  }




  oldX = 0;
  grabber = false;
  index: any = 'No';

  resizable(event, index, type) {
    if (type == 'mousedown') {
      this.grabber = true;
      this.oldX = event.pageX;
      this.index = index;
    }
  }
  @HostListener('document:mouseup', ['$event'])
  onMoseLeave() {
    this.grabber = false;
    this.index = 'No';
  }
  @HostListener('document:mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (!this.grabber) {
      return;
    }
    this.resizer(event.pageX - this.oldX);
    this.oldX = event.pageX;
  }

  resizer(offsetX: number) {
    if (this.index != 'No') {
      let width = parseInt(this.header[this.index].width);
      let innerWidth = parseInt(this.header[this.index].innerWidth);
      width += offsetX;
      innerWidth += offsetX;
      this.header[this.index].width = String(width);
      innerWidth = (innerWidth < 145) ? 145 : innerWidth;
      this.header[this.index].innerWidth = String(innerWidth);
    }
  }

  inlineFilter() {
    let arrObj = [];
    for (var i = 0; i <= this.header.length - 1; i++) {
      if (this.header[i]['filterData'].length) {
        arrObj.push({ [this.header[i]['id']]: this.header[i]['filterData'], types: 'LIKE' });
      }
      if (this.header.length - 1 == i) {
        this.viewData = this.filterMethod(arrObj);
        this.getValue(this.viewData);
        this.tableData = this.viewData;
        this.overAllListCount = this.viewData.length;
        this.listCountDisp();
        this.paginationDisable();
        this.grouping();
      }
    }
    this.filterOn = arrObj.length ? true : false;
    if (this.maintainData.templateId != 'MULTIPLE') {
      let data = {
        type: "search",
        val: this.filterOn
      }
      this.drillDownData.emit(data)
    }
  }

  overallFilter() {
    this.overallFilterData = [];
    let arrObj = [];
    for (let i = 0; i < this.header.length; i++) {
      if (this.overallFilterValue.length) {
        arrObj.push(
          { [this.header[i]['id']]: this.overallFilterValue, types: 'LIKE' },
        );
      }
      if (this.header.length - 1 == i) {
        let field = JSON.parse(JSON.stringify(this.temptableData[0]));
        let items = JSON.parse(JSON.stringify(this.temptableData));
        let aa;
        aa = Object.keys(field);
        if (items.length && items) {
          var fill = items.filter((item) => {
            aa.forEach((elem) => {
              arrObj.forEach(ele => {
                if (ele[elem] && String(item[elem])) {
                  if (String(item[elem])
                    .toLowerCase()
                    .indexOf(String(ele[elem]).toLowerCase()) === -1) {
                    this.viewData = [];
                    return false;
                  }
                  this.overallFilterData.push(item);
                }
              });
            });
            return true;
          });
          fill = this.overallFilterData.reduce((unique, o) => {
            if (!unique.some(obj => obj.id === o.id)) {
              unique.push(o);
            }
            return unique;
          }, []);
        }
        this.viewData = !this.overallFilterValue.length ? JSON.parse(JSON.stringify(this.temptableData))
          : fill;
        this.tableData = JSON.parse(JSON.stringify(this.viewData));
        this.overAllListCount = this.viewData.length;
        this.getValue(this.viewData)
        this.listCountDisp();
        this.paginationDisable();
        this.grouping();
      }
    }
  }

  filterMethod(searching_list: any) {
    let fields = JSON.parse(JSON.stringify(this.temptableData[0])),
      items = JSON.parse(JSON.stringify(this.temptableData));
    if (items.length && items) {
      var filldata = items.filter(item => {
        for (let prop in fields) {
          for (var i = 0; i < searching_list.length; i++) {
            if (
              searching_list[i]["types"] == "LIKE" ||
              searching_list[i]["types"] == ""
            ) {
              if (
                searching_list[i][prop] &&
                String(item[prop])
                  .toLowerCase()
                  .indexOf(String(searching_list[i][prop]).toLowerCase()) === -1
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "ENDSWITH") {
              if(!isNullOrUndefined(item[prop])){
                if (
                  searching_list[i][prop] &&
                  item[prop]
                    .toLowerCase()
                    .endsWith(searching_list[i][prop].toLowerCase()) - 1
                ) {
                  return false;
                }
              }
              else {
                return false;
              }
            } else if (searching_list[i]["types"] == "STARTSWITH") {
              if(!isNullOrUndefined(item[prop])) {
                if (
                  searching_list[i][prop] &&
                  item[prop]
                    .toLowerCase()
                    .startsWith(searching_list[i][prop].toLowerCase()) - 1
                ) {
                  return false;
                }
              }
              else {
                return false;
              }
            } else if (searching_list[i]["types"] == "EQUALS") {
              if(!isNullOrUndefined(item[prop])) {
                if (
                  searching_list[i][prop] &&
                  item[prop].toLowerCase() !==
                  searching_list[i][prop].toLowerCase()
                ) {
                  return false;
                }
              }
              else {
                return false;
              }
            } else if (searching_list[i]["types"] == "LESSER") {
              if (
                searching_list[i][prop] &&
                searching_list[i][prop] - 1 < item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "GREATER") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) + 1 > item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "LESSEREQUALS") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) < item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "GREATEREQUALS") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) > item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "EQUAL") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) != item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "NOTEQUALS") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) == item[prop]
              ) {
                return false;
              }
            }
          }
        }
        return true;
      });
      return filldata;
    }
  }

  previewScreen = () => {
    let screensize = document.getElementById('maximize');
    screensize.classList.contains('fullscreen') ? this.minimize(screensize) :
      this.maximize(screensize);
  }

  // screentype = 'fullscreen';

  // ------------------------------------------------------------
  minimize = (minimize: any) => {
    this.screentypeToolTip = 'maximize';
    minimize.classList.remove('fullscreen');
    this.screentype = 'fullscreen';
    //this.last_toolTip.setAttribute('tooltip', 'maximize');
    let tooltip = document.getElementsByClassName('tool_tip');
    for (var i = 0; i < tooltip.length; i++) {
      tooltip[i].removeAttribute('flow');
    }
  }

  // ------------------------------------------------------------
  maximize = (maximize: any) => {
    this.screentypeToolTip = 'Minimize';
    maximize.classList.add('fullscreen');
    this.screentype = 'fullscreen_exit';
    //this.last_toolTip.setAttribute('tooltip', 'Minimize');
    let tooltip = document.getElementsByClassName('tool_tip');
    for (var i = 0; i < tooltip.length; i++) {
      tooltip[i].setAttribute('flow', 'left');
    }
  }

  toggleClassReturn(item, idx) {
    let retStr = '';
    item.toggle ? retStr = retStr + ' selecte-li' : retStr = retStr + ' ';
    idx != 'first' ? retStr = retStr + ' pl-3' : '';
    return retStr;
  }

  tblRowClassFn() {
    let retStr = '';
    if (this.maintainData.nextLevel > 0) {
      retStr = retStr + ' crusorPointer'
    }
    return retStr;
  }

  treeLiWidth(idx) {
    if (idx == 'first') {
      let totalArray = this.header.map(ele => parseFloat(ele.columnWidth));
      let columnWidthSum = totalArray.reduce((a, b) => a + b, 0);

      let temp = this.header.filter(item => item.selected && !item.grouping);
      let treeArray = temp.map(ele => parseFloat(ele.columnWidth));
      let treecolumnWidthSum = treeArray.reduce((a, b) => a + b, 0);
      if (columnWidthSum > 100) {
        return {
          "width": treecolumnWidthSum * (100 / columnWidthSum) + '%'
        }
      }
      else {
        return {
          "width": treecolumnWidthSum + '%'
        }
      }

    }
    else {
      return {
        "width": '100%'
      }
    }

  }
  checkMinWidthDesign(data, type, gridset) {
    let formatType = this.formatType(gridset);
    let checkMinWidth = this.checkMinWidth(data, type);
    let mergeObj = {
      ...formatType,
      ...checkMinWidth
    };
    return mergeObj;
  }

  checkMinWidth(data, type) {


    let treeColumnWidthSum;
    if (type == 'tree') {
      let temp = this.header.filter(item => item.selected && !item.grouping);
      let totalArray = temp.map(ele => parseFloat(ele.columnWidth));
      treeColumnWidthSum = totalArray.reduce((a, b) => a + b, 0);
      treeColumnWidthSum = 100 / treeColumnWidthSum;
    }
    else {
      treeColumnWidthSum = 1
    }

    if (!isNullOrUndefined(data)) {
      if (!isNullOrUndefined(data.columnWidth)) {
        let totalArray = this.header.map(ele => parseFloat(ele.columnWidth));
        let columnWidthSum =  Math.round(totalArray.reduce((a, b) => a + b, 0).toFixed(2));
        if (this.reportOrientation == 'L') {
          // if (columnWidthSum < 100) {
            // return {
            //   "width": (data.columnWidth * (100 / 180)) * treeColumnWidthSum + '%'
            // }
          // }
          // else {
            return {
              "width": data.columnWidth * treeColumnWidthSum + '%'
            }
          // }
        }
        else {
          if (columnWidthSum <= 100) {
            return {
              "width": data.columnWidth * treeColumnWidthSum + '%'
            }
          }
          else {
            return {
              "width": (data.columnWidth) * (100 / columnWidthSum) * treeColumnWidthSum + '%'
            }
          }
        }
      }
    }
  }

  checkMinWidthVal() {
    let temp = this.header.filter(item => item.selected && !item.grouping).length;
    return temp < 7 ? (100 / temp) + '%' : "174px";
  }

  checkWidth() {
    let selectedCount = 0
    this.header.forEach(element => {
      if (element.selected) {
        selectedCount++;
      }
    });
    if (selectedCount < 7) {
      return {
        "width": '100%'
      }
    }
    else {
      return {
        "width": "auto"
      }
    }
  }

  attrColSpan(head, arr) {
    let temp = this.header.filter(item => item.selected && !item.grouping).length;
    let sum = 0;
    this.flexHeaders.forEach(element1 => {
      sum += parseInt(element1.colspan);
    });
    if (temp < 7) {
      return {
        "min-width": (head.colspan * (100 / sum)) + '%'
      }
    }
    else {
      return {
        "min-width": (head.colspan * 174) + 'px'
      }
    }

  }

  groupFlexHeight(arr, colspan) {

    return {
      "height": (100 / arr.maxRowSpan) + '%',
      "width": (colspan * (100 / arr.colspan)) + '%'

    }
  }
  groupFlexWidth(count) {
    return {
      "width": (100 / count) + '%'
    }
  }
  counter(i: number) {
    return new Array(i);
  }

  showSearchRes() {
    this.showSearch = !this.showSearch;
    setTimeout(() => {
      this.showSearchR = !this.showSearchR
    }, 120);
  }

  getHeightHeader(type) {
    this.updateWidthContent();

    if (type == 'first') {
      let hight = this.maintainData['fontSize'] == 'default' ? 6 :
        this.maintainData['fontSize'] == 'medium' ? 5 :
          this.maintainData['fontSize'] == 'small' ? 4 : 3;
      return {
        "height": this.valueHeader.length * hight + '%'
      }
    }
    if (type == 'second') {
      let selectedCount = 0
      this.header.forEach(element => {
        if (element.selected) {
          selectedCount++;
        }
      });
      if (selectedCount < 7) {
        return {
          "height": (100 / this.valueHeader.length) + '%',
          "width": '100%'
        }
      }
      else {
        return {
          "height": (100 / this.valueHeader.length) + '%',
          "width": "unset"
        }
      }
    }
  }

  getHeightContent(type) {
    if (type == 'first') {
      let noNumber = this.header.filter(item => item.colType != 'T');
      let heigh = noNumber.length && this.columnTotal && this.columnTotal.length ? 93 : 98;
      let hight = this.maintainData['fontSize'] == 'default' ? 6 :
        this.maintainData['fontSize'] == 'medium' ? 5 :
          this.maintainData['fontSize'] == 'small' ? 4 : 3;
      return {
        "height": heigh - (this.valueHeader.length * hight) + '%'
      }
    }
    if (type == 'footer') {
      let noNumber = this.header.filter(item => item.colType != 'T');
      if (noNumber.length && this.columnTotal && this.columnTotal.length) {
        return {
          "height": '7%'
        }
      }
      else {
        return {
          "height": '0%'
        }
      }
    }
  }

  updateWidthContent() {
    // classChildren=document.getElementsByClassName('tr');
    //let pixel=!isNullOrUndefined(classChildren[0]) ? classChildren[0].clientWidth :174;
    let temp = this.header.filter(item => item.selected && !item.grouping).length;
    if (temp < 7) {
      this.widthPx = '100%';
    }
    else {
      this.widthPx = (temp * 174) + 'px';
    }
  }

  sorting(data) {
    // this.header.forEach(element => {
    //   if (element.name == data.name) {
    //     element.sorting = true;
    //     element.sortValue = !element.sortValue;
    //     if (element.sortValue) {
    //       let obj = {};
    //       obj[element.id] = 'asc'
    //       this.viewData = this.gpService.orderBy(obj, this.tableData);
    //       this.tableData = this.viewData;
    //       this.grouping();
    //     }
    //     else {
    //       let obj = {};
    //       obj[element.id] = 'desc'
    //       this.viewData = this.gpService.orderBy(obj, this.tableData);
    //       this.tableData = this.viewData;
    //       this.grouping();
    //     }
    //   }
    //   else {
    //     element.sorting = false;
    //     element.sortValue = false;
    //   }
    // });
  }
  drillDownRequest(item, id) {
    if (this.maintainData.templateId != 'MULTIPLE') {
      let last = (this.limitedPagination['lastIndex'] > this.overAllListCount) ? this.overAllListCount :
        this.limitedPagination['lastIndex'];
      let data = {
        type: 'drillDown',
        item: item,
        rowId: id,
        scroll: this.scrollPostion,
        intReportSeq: this.maintainData['tileSequenceId'],
        pageNum: last < this.perPage ? 1 : last / this.perPage,
        droppedItem: this.droppedItem,
        sortArray: this.sortingForm.value.sortArray.filter(item => item.type.length),
        filterData: this.filteringForm.value.filterArray.filter(item => item.id.length && item.type.length && item.filterData.length)
      }
      this.drillDownData.emit(data);
    }

  }

  adVsettingsDropdown(type) {
    this.adVsettings = type;
    if (type == 'columnView') {
      this.adVsettingsColumn = !this.adVsettingsColumn;
      this.adVsettingsSorting = false;
      this.adVsettingsFilter = false;
    }
    if (type == 'sorting') {
      this.adVsettingsSorting = !this.adVsettingsSorting;
      this.adVsettingsColumn = false;
      this.adVsettingsFilter = false;
    }
    if (type == 'Filter') {
      this.adVsettingsFilter = !this.adVsettingsFilter;
      this.adVsettingsColumn = false;
      this.adVsettingsSorting = false;
    }
  }

  onScrollEvent(data) {
    this.scrollPostion.y = data.scrollStartPosition;
  }

  gridSpan(data) {
    let temp = this.header.filter(item => item.selected && !item.grouping).length;
    //let widthVal = temp < 7 ? '100%' : "174px";
    let gridRow = data.rowspan <= 1 ? data.labelRowNum : `${data.labelRowNum} / span ${data.rowspan}`;
    let gridColumn = data.colspan <= 1 ? data.labelColNum : `${data.labelColNum} / span ${data.colspan}`;
    if (data.colspan <= 1) {
      return {
        "grid-column": gridColumn,
        "grid-row": gridRow,
        // "width": widthVal
      }
    }
    else {
      return {
        "grid-column": gridColumn,
        "grid-row": gridRow
      }
    }


  }

  updateGridFr() {
    this.gridFr = '';
    let temp = this.header.filter(item => item.selected && !item.grouping).length;
    let totalArray = this.header.map(ele => parseFloat(ele.columnWidth));
    let columnWidthSum = Math.round(totalArray.reduce((a, b) => a + b, 0).toFixed(2));
    this.header.forEach(element => {
      if (!isNullOrUndefined(element.columnWidth)) {
        if (element.selected && !element.grouping) {
          if (this.reportOrientation == 'L') {
            // if (columnWidthSum < 100) {
            //   this.gridFr = this.gridFr + ' ' + (element.columnWidth * (100 / 180)) + '%'
            // }
            // else {
              this.gridFr = this.gridFr + ' ' + element.columnWidth + '%'
            // }
          }
          else {
            if (columnWidthSum <= 100) {
              this.gridFr = this.gridFr + ' ' + element.columnWidth + '%'
            }
            else {
              this.gridFr = this.gridFr + ' ' + (element.columnWidth) * (100 / columnWidthSum) + '%'
            }
          }
        }
        else {
          this.gridFr = this.gridFr + ' 0%'
        }
      }
    });
  }

  @HostListener('window: resize', ['$event']) adjustContents() {
    this.updateWidthContent();
    this.sideFilterPosition();
    this.sideFilterHeight();
  }

  toggleSideFilter = () => {
    // let data = {
    //   type: 'closeFilterPrompt',
    // }
    // this.drillDownData.emit(data)
    const sideFilter = document.getElementById('side-filter-hold');
    // const sideArrowClass = document.getElementById('sidebarArrow');
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    // const sideIcon = document.getElementById('side-filter-icon');
    if (!isNullOrUndefined(sideFilter)) {
      if (sideFilter.classList.contains('setRight')) {
        // sideIcon.style.cssText = `right: -2px; z-index: 1000;`;
        sideFilterOverlay.classList.remove('show', 'z-154');
        sideFilter.classList.remove('setRight');
        // sideArrowClass.classList.remove('fa-angle-double-right');
      }
      else {
        setTimeout(() => {
          // if (document.getElementsByTagName('body')[0].classList.contains('sidebar-mini')) {
          //   sideIcon.style.cssText = `right: 36%; z-index: 100001;`;
          // }
          // else {
          //   sideIcon.style.cssText = `right: 42%; z-index: 100001;`;
          // }
          sideFilterOverlay.classList.add('show', 'z-154');
          sideFilter.classList.add('setRight');
          // sideArrowClass.classList.add('fa-angle-double-right');
        }, 100);
      }
    }
  }

  cancelFilter = (event) => {
    this.toggleSideFilter();
    this.header.forEach(head=> {
      head.selected1 = head.selected
    })
    this.overallVieport = this.header.every(ele => ele.selected1);
    this.applyGroupingFlag = this.maintainData['applyGroupingFlag'];
  }

  sideFilterHeight() {
    const dashboardHoldHeight = window.innerHeight;
    const filterHeaderHeight = +!isNullOrUndefined(document.getElementById('filterHeader')) ? document.getElementById('filterHeader').offsetHeight : 0;
    const filterFooterHeight = +!isNullOrUndefined(document.getElementById('filterFooter')) ? document.getElementById('filterFooter').offsetHeight : 0;
    if (!isNullOrUndefined(document.getElementById('side-filter-hold')))
      document.getElementById('side-filter-hold').style.height = `${dashboardHoldHeight - 66}px`;
    const setFilterBodyHeight = dashboardHoldHeight - (filterHeaderHeight + filterFooterHeight);
    if (!isNullOrUndefined(document.getElementById('filterBody')))
      document.getElementById('filterBody').style.cssText = `height: ${setFilterBodyHeight}px;
                                                          max-height: ${setFilterBodyHeight}px;
                                                          margin-bottom: ${filterFooterHeight}px`;
  }

  sideFilterPosition() {
    const sideFilterHold = document.getElementById('side-filter-hold');
    // const filterHoldWidth = document.getElementById('side-filter-hold').offsetWidth;
    // sideFilterHold.style.setProperty('--filterWidth', `-${filterHoldWidth}px`);
    if (!isNullOrUndefined(sideFilterHold))
      sideFilterHold.style.opacity = '1';
  }

  checkColHeader() {
    let filter = this.header.filter(item => item.colType != 'T')
    return filter.length ? true : false;
  }

  getDecimalScaling(item, head) {
    if (isNullOrUndefined(head.decimalCnt) || head.decimalCnt == '') {
      return Number(parseFloat(item)).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 })
    }
    else {
      return Number(parseFloat(item)).toLocaleString('en',
        { minimumFractionDigits: head.decimalCnt, maximumFractionDigits: head.decimalCnt })
    }
  }
  formatType(data) {
    if (data['FORMAT_TYPE'] == 'FT') {
      return { "font-weight": 600, "color": "#3c4044"}
    }
    else if (data['FORMAT_TYPE'] == 'S') {
      return { "font-weight": 600, "background-color": 'lightgray' }
    }
    else {
      return { "font-weight": "normal" }
    }
  }
  checkGroupingHeight(length) {
    if (this.maintainData.fontSize == 'medium') {
      return {
        "height": (length * 20) + 'px'
      }
    }
    else if (this.maintainData.fontSize == 'default') {
      return {
        "height": (length * 22) + 'px'
      }
    }
    else if (this.maintainData.fontSize == 'small') {
      return {
        "height": (length * 17) + 'px'
      }
    }
    else if (this.maintainData.fontSize == 'tiny') {
      return {
        "height": (length * 11) + 'px'
      }
    }
  }

  saveSettings(event) {
    // let filter = this.filteringForm.value.filterArray;
    let reportUserDeflst = [];
    let filterData = this.filteringForm.value.filterArray.filter(item => !isNullOrUndefined(item.id) && item.id.length && !isNullOrUndefined(item.type) &&
    item.type.length && !isNullOrUndefined(item.filterData) && item.filterData.length)
    let sortArr = this.sortingForm.value.sortArray;
    this.arrayObj = sortArr.filter(item => !isNullOrUndefined(item.column) && item.column.length &&
    !isNullOrUndefined(item.type) && item.type.length);
    // let smartSearchOpt = [];
    let searchColumn = '';
    if(filterData.length)
    filterData.forEach((ele, i) => {
      searchColumn += `${ele.id}!@#${ele.type}!@#${ele.filterData}`;
      if (filterData.length - 1 != i) {
        searchColumn += ',';
      }
      // smartSearchOpt.push({ object: ele.id, criteria: ele.type, value: ele.filterData })
    })
    let sortColumn = '';
    if (this.arrayObj.length) {
      sortColumn = 'ORDER BY ';
      this.arrayObj.forEach((ele, ind) => {
        sortColumn += `${ele.column} ${ele.type.toUpperCase()}`;
        if (this.arrayObj.length - 1 != ind) {
          sortColumn += ',';
        }
      });
    }
    else {
      sortColumn = '';
    }
    let groupingColumn = '';
    if (this.droppedItem.length) {
      let filteredItem = (this.droppedItem).map(ele => ele.name);
      groupingColumn = filteredItem.join('!@#')
    }
    let columns = this.header.filter(item => !item.selected1)
    let columnsToHide = '';
    if (!isNullOrUndefined(columns) && columns.length) {
      let filteredItem = columns.map(ele => ele.dbColumnName);
      columnsToHide = filteredItem.join('!@#')
    }
    let req = {
      reportId: this.maintainData.reportId,
      subReportId: this.maintainData.subReportId,
      sortColumn: sortColumn,
      searchColumn: searchColumn,
      groupingColumn: groupingColumn,
      columnsToHide: columnsToHide,
      applyGrouping: this.applyGroupingFlag ? 'Y' : 'N'
    }
    let header = this.header.filter(item => item.colspan <= 1 && item.selected1);
    let colTypeT = header.filter(item => (item.colType == 'T' || item.colType == 'TR' || item.colType == 'NR'));
    let colTypeN = header.filter(item => (item.colType == 'I' || item.colType == 'S' || item.colType == 'N'));
    let colTypeDimension = colTypeT && colTypeT.length ? colTypeT.map(item => item.dbColumnName).join(',') : '';
    req['showDimensions'] = colTypeDimension;
    let groupMeasure = '';
    if (colTypeN && colTypeN.length)
      colTypeN.forEach((ele, ind) => {
        groupMeasure += `SUM(${ele.dbColumnName}) ${ele.dbColumnName}`;
        if (colTypeN.length - 1 != ind) {
          groupMeasure += ',';
        }
      })
    let colTypeMeasure = colTypeN && colTypeN.length ? colTypeN.map(item => item.dbColumnName).join(',') : '';
    req['showMeasures'] = this.applyGroupingFlag && groupMeasure != '' ? groupMeasure :
      colTypeMeasure;
    req['applyGrouping'] = this.applyGroupingFlag ? 'Y' : 'N';
    reportUserDeflst.push(req)
    let data = {
      reportUserDeflst: reportUserDeflst
    }
    this.apiService.post(environment.saveUserSettingforReport, data).subscribe((resp) => {
      if (resp['status'] == 1) {
        const modelRef = this.modalService.open(GenericPopupComponent, {
          size: <any>'md',
          backdrop: false,
          windowClass: "genericPopup"
        });
        modelRef.componentInstance.title = "Alert";
        modelRef.componentInstance.message = "Do you want to reload the Report ?";
        modelRef.componentInstance.popupType = 'Confirm';
        modelRef.componentInstance.closeTime = 'No';
        modelRef.componentInstance.userConfirmation.subscribe(resp => {
          if (resp.type == 'Yes') {
            // this.hideColumn();
            this.submitSort(false);
          }
          else {
            this.header.forEach(head=> {
              head.selected1 = head.selected
            })
            this.overallVieport = this.header.every(ele => ele.selected1);
            this.applyGroupingFlag = this.maintainData['applyGroupingFlag'];
          }
          modelRef.close();
          this.toggleSideFilter();
        });
        this.globalService.showToastr.success(resp['message']);
      }
      else {
        this.globalService.showToastr.error(resp['message'])
      }
    });
  }

  ngOnDestroy() {
    // if(this.topSlider){
      this.globalService.topbarSlider = new BehaviorSubject({});
    // this.topSlider.unsubscribe();
    // }
  }

}

