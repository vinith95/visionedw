import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/service';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
declare var jsPlumb: any;
declare let $: any;
@Component({
  selector: 'app-userid-selections',
  templateUrl: './userid-selections.component.html',
  styleUrls: ['./userid-selections.component.css']
})
export class UseridSelectionsComponent implements OnInit {
  tableheader_columnone = [];
  tableheader_columntwo = [];

  tablecolumnvaluesarrayone = [];
  tablecolumnvaluesarraytwo = [];
  valuesPatternsLeft = [];
  valuesPatternsRight = [];
  valuesPatternsBottom = [];
  valuesPatterns = [];
  valuesPatterns1 = [];
  valuesPatterns2 = [];
  removingArrayobjects = [];
  @Input() bodyText;
  @Output() emitData: EventEmitter<any> = new EventEmitter<any>();
  @Input() inputDetailsUserIds_To: any = [];
  @Input() inputDetailsUserIds_Cc: any = [];
  @Input() ccvaluesId: any;
  @Input() TovaluesId: any;


  constructor(private modalService: NgbModal, private _apiService: ApiService,) { }
  @ViewChild(CdkVirtualScrollViewport) viewport: CdkVirtualScrollViewport;
  ngOnInit(): void {
    let leftValues = [];
    let rightValues = [];
    let bottomValues = [];

    if (!isNullOrUndefined(this.inputDetailsUserIds_To)) {
      this.removingArrayobjects = [...this.removingArrayobjects, ...this.inputDetailsUserIds_To];
    }
    if (!isNullOrUndefined(this.inputDetailsUserIds_Cc)) {
      this.removingArrayobjects = [...this.removingArrayobjects, ...this.inputDetailsUserIds_Cc];
    }


    const passlist = {
      eventCcEmailId: this.ccvaluesId,
      eventToEmailId: this.TovaluesId,
    }



   
    this._apiService.post(environment.eventApi_getMailIds, passlist).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.valuesPatterns = resp['response'][0];
        this.valuesPatterns1 = resp['response'][2];
        this.valuesPatterns2 = resp['response'][1];

        this.valuesPatterns.forEach(x => {

          if (x.userName) {
            x['isRightTouched'] = false;
            x['isLeftTouched'] = false;
            x['isMovedToRight'] = false;
            x['isMovedToLeft'] = true;
            x['isBottomTouched'] = false;
            x['isBottomChecked'] = false;
            leftValues.push(x);
          }


        });
        this.valuesPatterns1.forEach(x => {

          if (x.userName) {
            x['isRightTouched'] = true;
            x['isLeftTouched'] = false;
            x['isMovedToRight'] = true;
            x['isMovedToLeft'] = false;
            x['isBottomTouched'] = false;
            x['isBottomChecked'] = false;
            rightValues.push(x)
          }


        });
        if (!isNullOrUndefined(this.valuesPatterns)) {
          this.valuesPatterns = [...this.valuesPatterns1, ...this.valuesPatterns];
        }

        this.valuesPatterns2.forEach(x => {

          if (x.userName) {
            x['isRightTouched'] = false;
            x['isLeftTouched'] = false;
            x['isMovedToRight'] = false;
            x['isMovedToLeft'] = false;
            x['isBottomTouched'] = true;
            x['isBottomChecked'] = true;
            bottomValues.push(x)
          }


        });

        if (!isNullOrUndefined(this.valuesPatterns)) {
          this.valuesPatterns = [...this.valuesPatterns2, ...this.valuesPatterns];
        }
        this.valuesPatternsLeft = leftValues;
        this.valuesPatternsRight = rightValues;
        this.valuesPatternsBottom = bottomValues;
      }

    });






  }


  rightvalues: any = [];
  bottomvalues: any = [];
  saveSelectingColumns() {
    this.rightvalues = this.valuesPatternsRight.map((item) => { return item.visionId }).join(',')
    this.bottomvalues = this.valuesPatternsBottom.map((item) => { return item.visionId }).join(',')

    let data = {
      type: 'submit',

      eventCcEmailId: this.rightvalues,
      eventToEmailId: this.bottomvalues,
    }
    this.emitData.emit(data)
  }

  closeModal = () => {
    this.emitData.emit('close');
    this.modalService.dismissAll('Cross click')
  }

  onClickValuesPatternsItem(item, where) {

    let idx = this.valuesPatterns.findIndex(x => x.userName == item.userName);
    if (where == 'left') {
      let isTouched = !item['isLeftTouched'];
      this.valuesPatterns[idx]['isLeftTouched'] = isTouched;
      item['isLeftTouched'] = isTouched;

    } else if (where == 'right') {
      let isTouched = !item['isRightTouched'];
      this.valuesPatterns[idx]['isRightTouched'] = isTouched;
      item['isRightTouched'] = isTouched;

    }
    else if (where == 'bottom') {

      let isTouched = !item['isBottomChecked'];
      this.valuesPatterns[idx]['isBottomChecked'] = isTouched;
      item['isBottomChecked'] = isTouched;

    }

  }

  moveSelectedItems(to: string) {
    let leftSelectedArr = [];
    let rightSelectedArr = [];

    if (to == 'right') {


      this.valuesPatterns.forEach(y => {
        if (y.isMovedToRight && y.isBottomTouched == false) {
          y['isRightTouched'] = false;
          y['isLeftTouched'] = false;
          y['isBottomTouched'] = false;

          rightSelectedArr.push(y);
        } else {
          if (y.isLeftTouched && y.isBottomTouched == false) {
            y['isRightTouched'] = true;

            y['isLeftTouched'] = false;
            y['isMovedToRight'] = true;
            y['isMovedToLeft'] = false;
            rightSelectedArr.push(y);
          } else if (y.isBottomTouched == false) {
            y['isRightTouched'] = false;
            y['isLeftTouched'] = false;
            y['isMovedToLeft'] = true;
            y['isMovedToRight'] = false;
            leftSelectedArr.push(y);
          }
        }
      });
    } else {
      this.valuesPatterns.forEach(y => {
        if (y.isMovedToLeft && y.isBottomTouched == false) {
          y['isRightTouched'] = false;
          y['isLeftTouched'] = false;
          leftSelectedArr.push(y);
        } else {
          if (y.isRightTouched && y.isMovedToRight && y.isBottomTouched == false) {
            y['isLeftTouched'] = true;
            y['isRightTouched'] = false;
            y['isMovedToLeft'] = true;
            y['isMovedToRight'] = false;
            y['isBottomTouched'] = false;
            leftSelectedArr.push(y);
          } else if (y.isBottomTouched == false) {
            y['isLeftTouched'] = false;
            y['isRightTouched'] = false;
            y['isMovedToLeft'] = false;
            y['isMovedToRight'] = true;
            rightSelectedArr.push(y);
          }
        }


      });
    }
    this.valuesPatternsLeft = leftSelectedArr;
    this.valuesPatternsRight = rightSelectedArr;



  }
  moveSelectedItemsBottom(to: string) {
    let leftSelectedArr = [];
    let rightSelectedArr = [];
    if (to == 'bottom') {
      this.valuesPatterns.forEach(y => {

        if (y.isBottomTouched == true) {
          y['isBottomChecked'] = false;
          y['isLeftTouched'] = false;
          y['isMovedToRight'] = true;
          y['isMovedToLeft'] = false;
          y['isBottomTouched'] = true;
          rightSelectedArr.push(y);
        }

        if (y.isLeftTouched == true) {
          y['isBottomChecked'] = true;
          y['isLeftTouched'] = false;
          y['isMovedToRight'] = true;
          y['isMovedToLeft'] = false;
          y['isBottomTouched'] = true;
          rightSelectedArr.push(y);
        }


        if (y.isMovedToRight == false) {
          y['isRightTouched'] = false;
          y['isLeftTouched'] = false;
          y['isMovedToLeft'] = true;
          y['isMovedToRight'] = false;
          leftSelectedArr.push(y);
        }



      });
    } else {
      this.valuesPatterns.forEach(y => {
        if (y.isBottomTouched == true && !y.isBottomChecked) {
          y['isRightTouched'] = false;
          y['isLeftTouched'] = false;
          y['isMovedToLeft'] = false;
          y['isMovedToRight'] = false;
          y['isBottomTouched'] = true;
          rightSelectedArr.push(y);
        }
        else if (y.isBottomTouched == true && y.isBottomChecked) {
          y['isRightTouched'] = false;
          y['isLeftTouched'] = true;
          y['isMovedToLeft'] = true;
          y['isMovedToRight'] = false;
          y['isBottomTouched'] = false;
          leftSelectedArr.push(y);
        }

        else if (!y.isRightTouched) {
          y['isRightTouched'] = false;
          y['isLeftTouched'] = false;
          y['isMovedToLeft'] = true;
          y['isMovedToRight'] = false;
          y['isBottomTouched'] = false;
          leftSelectedArr.push(y);
        }
      });
    }
    this.valuesPatternsLeft = leftSelectedArr;
    this.valuesPatternsBottom = rightSelectedArr;

  }



}
