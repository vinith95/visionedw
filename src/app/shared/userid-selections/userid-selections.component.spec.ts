import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UseridSelectionsComponent } from './userid-selections.component';

describe('UseridSelectionsComponent', () => {
  let component: UseridSelectionsComponent;
  let fixture: ComponentFixture<UseridSelectionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UseridSelectionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UseridSelectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
