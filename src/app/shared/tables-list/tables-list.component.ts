import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tables-list',
  templateUrl: './tables-list.component.html',
  styleUrls: ['./tables-list.component.css']
})

export class TablesListComponent implements OnInit {
  @Input() tableidList;
  @Input() title;
  @Input() emailStatusList;
  @Input() selectedMail;
  @Output() filterData = new EventEmitter();
  allChecked: boolean = false;
  emailIdList1 = [];
  addEmailId = '';
  tablelistvalues: any = [];
  constructor() { }

  ngOnInit(): void {

    this.tablelistvalues = [
      { name: 'ACCOUNTS_DLY_IDX', age: 21, description: 'ACCOUNTS_DLY_IDX', checked: false },
      { name: 'ACCOUNTS_DLY_IDX1', age: 31, description: 'ACCOUNTS_DLY_IDX1', checked: false },
      { name: 'ACCOUNTS_DLY_IDX2', age: 58, description: 'ACCOUNTS_DLY_IDX2', checked: false },
      { name: 'ACCOUNTS_DLY_PK', age: 20, description: 'ACCOUNTS_DLY_PK', checked: false }
    ]
    // console.log(this.tablelistvalues);
    this.tableidList.forEach(list1 => {
      this.tablelistvalues.forEach(selected => {
        if (list1.name == selected.name) {
          //    console.log("checkingloop");
          list1.checked += true;
        }
      })
    })

  }


  emitSelectedData() {

    let value = [];
    this.tablelistvalues.map(ele => {
      if (ele.checked) {
        value.push(ele)
      }
    })
    let send = {
      type: 'emitedData',
      value: value
    }
    this.filterData.emit(send)
  }

  closeModal() {
    this.filterData.emit({ type: 'close' })
  }


}
