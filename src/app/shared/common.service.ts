import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AbstractControl, FormControl } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GenericPopupComponent } from '../shared/generic-popup/generic-popup.component';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(private toastr: ToastrService, private modalService: NgbModal,) { }
  sideNav = new BehaviorSubject(true);
  miniNav = new BehaviorSubject(false);
  profileForm = new BehaviorSubject({});
  currentProfile = new BehaviorSubject({});
  // currentProfile = new BehaviorSubject('');
  profileEdit = new BehaviorSubject<any>({});
  columnsList = new BehaviorSubject<any>({});
  cleanserNodes = new BehaviorSubject({});
  // profileEdit = new Subject<any>();

  profilerId = new BehaviorSubject('');
  profilerDetail = new BehaviorSubject({});
  //  profilerId = new Subject<any>();

  conectorColor = new BehaviorSubject('');
  // connectorColor = new BehaviorSubject(false);
  toolBarTitle = new BehaviorSubject('');
  miniView = new BehaviorSubject(false);
  data: boolean = true;
  mini: boolean = false;
  activeIcon = '';
  pageTitle = ''
  menuProgram: any = '';
  showToastr = {
    success: (msg: any) => {
      this.toastr.success(msg);
    },
    warning: (msg: any) => {
      this.toastr.warning(msg);
    },
    error: (msg: any) => {
      let isPinMsg: boolean = false;
      const modelRef = this.modalService.open(GenericPopupComponent, {
        size: <any>'md',
        backdrop: false,
        windowClass: 'errorPopup'
      });
      modelRef.componentInstance.title = 'Error';
      modelRef.componentInstance.message = msg;
      modelRef.componentInstance.popupType = 'error';
      modelRef.componentInstance.userConfirmation.subscribe((e) => {
        if (e == 'copy') {
          this.showToastr.success('Error Copied');
        }
        if (e == 'No') {
          modelRef.close();
        }
        if (e == 'pin') {
          isPinMsg = !isPinMsg;
          setTimeout(() => {
            if (!isPinMsg) {
              modelRef.close();
            }
          }, 100);
        }
      });
      setTimeout(() => {
        if (!isPinMsg) {
          modelRef.close();
        }
      }, 5000);
    },

  };
  sidenav() {
    this.data = !this.data;
    if (this.sideNav.observers.length > 1) {
      this.sideNav.observers[0].next(this.data);
    } else {
      this.sideNav.next(this.data);
    }
  }
  markAllDirty(control: AbstractControl) {
    if (control.hasOwnProperty('controls')) {
      control.markAsDirty({ onlySelf: true }) // mark group
      let ctrl = <any>control;
      for (let inner in ctrl.controls) {
        this.markAllDirty(ctrl.controls[inner] as AbstractControl);
      }
    }
    else {
      (<FormControl>(control)).updateValueAndValidity();
      (<FormControl>(control)).markAsDirty({ onlySelf: true });
    }
  }

  miniTrigger(type) {
    if (type == 'map') {
      this.mini = true
      if (this.miniNav.observers.length > 1) {
        this.miniNav.observers[0].next(this.mini);
      } else {
        this.miniNav.next(this.mini);
      }
    }
    else {
      this.mini = false;
      if (this.miniNav.observers.length > 1) {
        this.miniNav.observers[0].next(this.mini);
      } else {
        this.miniNav.next(this.mini);
      }
    }
  }
}
