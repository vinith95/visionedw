import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareOwnershipComponent } from './share-ownership.component';

describe('ShareOwnershipComponent', () => {
  let component: ShareOwnershipComponent;
  let fixture: ComponentFixture<ShareOwnershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareOwnershipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareOwnershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
