import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService, JwtService, GlobalService } from "../../service";
import { environment } from "../../../environments/environment";
import { moveItemInArray, CdkDragDrop, CdkDragStart, CdkDragMove, CdkDragEnter } from '@angular/cdk/drag-drop';
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-query-validation',
  templateUrl: './query-validation.component.html',
  styleUrls: ['./query-validation.component.css']
})

export class QueryValidationComponent implements OnInit {
  selectedColumn: any = [];
  olcheckBoxStat: any = false;
  showSubmit: boolean = false;
  emailBody = '';
  @Input() bodyText: any;
  @Input() dataPassingValue: any;
  columnList: any = [];
  @Output() emitData: EventEmitter<any> = new EventEmitter<any>();
  initEditor: any;
  temporaryArray: any = [];
  listedColumn: any;
  creteriaList: any = [];
  columnSubmitted: boolean = false;
  isFilterRemoved = false;
  showValidate: boolean = true;
  dataFormation: any = [];
  queryCond_OverAllList: any;
  constructor(private _apiService: ApiService, private globalService: GlobalService,
    private modalService: NgbModal,) { }

  ngOnInit(): void {
    this.creteriaList = creteriaList;
    this.temporaryArray.push(this.bodyText);
    let values;
    if (this.bodyText.whereConditions != '') {
      values = JSON.parse(this.bodyText.whereConditions);
    }
    let result = Array.isArray(values);
    if (result) {
      this.columnList = values;
      this.queryCond_OverAllList = this.columnList[0].otherFilter;
    }
    let data = this.dataPassingValue;
    this._apiService
      .post(environment.columnsForPolicyCondition, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.selectedColumn = resp["response"];
        }
      });
  }


  searchNode(event, parent, child) {
    const value = event.target.value;
    let li = document.getElementsByClassName(parent);
    for (var i = 0; i < li.length; i++) {
      let a = li[i].getElementsByClassName(child)[0];
      let txtValue = a.textContent || a['innerText'];
      if (txtValue.toUpperCase().indexOf((value).toUpperCase()) > -1) {
        li[i]['style'].display = "";
      } else {
        li[i]['style'].display = "none";
      }
    }
  }
  detectChange(childKey: string, value: string) {
    this.btnCondition()
  }
  btnCondition() {
    this.showValidate = true;
    this.showSubmit = false;
  }

  closePopup() {
    let data = {
      type: 'close',
    }
    this.emitData.emit(data);
  }

  dragRows(list: any) {
    this.olcheckBoxStat = false;
    this.listedColumn = list;
  }
  dropRows(event: any) {
    event.stopPropagation();
    event.preventDefault();
    this.pointedColumn(this.listedColumn);
  }
  remove(i) {
    this.columnList.splice(i, 1);
    this.btnCondition();
    this.isFilterRemoved = true;

  }

  rowsDrop(event: any) {
    event.preventDefault();
  }
  pointedColumn(list) {
    let reqList = Object.assign({}, list);
    reqList['sortType'] = 'Asc';
    reqList['joinType'] = 'And';
    reqList['creteria'] = '';
    reqList['startData'] = '';
    reqList['endData'] = '';
    this.columnList.push(reqList);
  }

  swap(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.columnList, event.previousIndex, event.currentIndex);
  }
  aggregateCondition(item) {
    const bol = ['sum', 'avg', 'min', 'max', 'count'].includes(item.creteria);
    item.startData = (bol) ? '' : item.startData;
    item.endData = (bol) ? '' : item.endData;
    if (item.creteria != 'between') {
      item.endData = '';
    }
    return bol ? false : true;
  }

  queryFormation(table) {
    table[table.length - 1]['joinType'] = '';
    let Wherequery = '';
    table.forEach((ele) => {
      Wherequery = Wherequery + `${ele["alphaSubTab"]} ${ele["creteria"]}` + " ";
      if (ele["creteria"] == "between") {
        Wherequery = Wherequery +
          `'${ele["startData"]}'` +
          " AND " +
          `'${ele["endData"]}'` + " ";
      } else if (ele["creteria"] == "in" || ele["creteria"] == "not in") {
        Wherequery = Wherequery + " (";
        Wherequery = Wherequery + this.inConditionString(ele["startData"]) + ") ";
      }
      else if (
        ele["creteria"] == "like" ||
        ele["creteria"] == "not like"
      ) {
        let aa = (!ele["startData"].startsWith("%") && !ele["startData"].endsWith("%")) ? ` '%${(ele["startData"]).toUpperCase()}%' ` : `'${ele["startData"].toUpperCase()}' `;
        Wherequery = Wherequery + aa;
      }
      else if (
        ele["creteria"] != "between" &&
        ele["creteria"] != "is not null" &&
        ele["creteria"] != "is null" &&
        ele["creteria"] != "in" &&
        ele["creteria"] != "not in" &&
        ele["creteria"] != "like" &&
        ele["creteria"] != "not like"
      ) {
        ele['startData'] = `'${ele['startData']}'`;
        Wherequery = Wherequery + " ";
        Wherequery = Wherequery + `${ele["startData"]}` + " ";
      }
      Wherequery = Wherequery + `${ele["joinType"]}`;
      Wherequery = Wherequery + " ";
    });
    return Wherequery;
  }

  inConditionString(data) {
    return `'${data.split(',').join("','")}'`
  }


  submit() {
    if (!this.columnList.length) {
      this.submitProfiler();
    }
    else {
      let checkAll = this.columnList.every(e => (e.creteria != null && e.startData != null &&
        e.creteria != '' && e.startData != ''));
      let checkBetween = [];
      checkBetween = this.columnList.filter(e => (e.creteria == 'between' && (e.endData == null || e.endData == '')));
      if (checkAll && !checkBetween.length) {
        this.submitProfiler();
      }
      else {
        this.columnSubmitted = true;
      }
    }
  }

  submitProfiler() {
    this.dataFormation =[];
    this.columnSubmitted = false;
    let values = '';
    let queryFormation = '';
    let obj = {};
    let messageToaster = '';
    let type = '';
    values = this.columnList.length ? this.queryFormation(JSON.parse(JSON.stringify(this.columnList))) : '';

    if (this.queryCond_OverAllList) {
      queryFormation = values + this.queryCond_OverAllList.toUpperCase();
    }
    else {
      queryFormation = values;
    }
    this.columnList.forEach(element => {
      if (element) {
        let data = {};
        data['alphaSubTab'] = element.alphaSubTab,
          data['joinType'] = element.joinType,
          data['creteria'] = element.creteria,
          data['startData'] = element.startData,
          data['endData'] = element.endData,
          data['sortType'] = element.sortType,
          data['otherFilter'] = this.queryCond_OverAllList
        this.dataFormation.push(data);

      }
    });
    let apiUrl = environment.edwPolicy_setupConfig_validateArchQuery;
    this.temporaryArray.forEach(element => {
      if (element) {
        element.tableName = element.columnNine;
        element.isSelected = true;
        element.whereConditions = queryFormation;
        element.jsonValue = JSON.stringify(this.dataFormation);
      }

    });
    obj = this.temporaryArray[0];
    this._apiService.post(apiUrl, obj).subscribe((resp) => {
      if (resp["status"] == 1) {
        messageToaster = "Current query affected rows :  " + resp["response"];
        type = "sucess";
        this.genericPopupOpen(messageToaster, type);
      } else {
        messageToaster = resp["message"];
        type = "error";
        this.genericPopupOpen(messageToaster, type);
      }
    });

  }



  genericPopupOpen(messageToaster, type) {
    const modRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    modRef.componentInstance.title = "Alert";
    modRef.componentInstance.popupType = "alert";
    modRef.componentInstance.message = messageToaster;
    modRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp.type == "Yes" && type == "sucess") {
        this.showSubmit = true;
        this.showValidate = false;
        this.storingArrayValues();
        modRef.close();
      }
      if (resp.type == "Yes" && type == "error") {
        this.showSubmit = false;
        this.showValidate = true;
        this.storingArrayValues();
        modRef.close();
      }
      modRef.close();
    });
  }

  storingArrayValues() {
    this.temporaryArray.forEach(element => {
      if (element) {
        element.isSelected = true;
        element.whereConditions = JSON.stringify(this.dataFormation);
      }
    });
  }





  save() {
    if (!this.columnList.length) {
      this.saveFunctionality();
    }
    else {
      let checkAll = this.columnList.every(e => (e.creteria != null && e.startData != null &&
        e.creteria != '' && e.startData != ''));
      let checkBetween = [];
      checkBetween = this.columnList.filter(e => (e.creteria == 'between' && (e.endData == null || e.endData == '')));
      if (checkAll && !checkBetween.length) {
        this.saveFunctionality();
      }
      else {
        this.columnSubmitted = true;
      }
    }
  }


  saveFunctionality() {
   // console.log("dataFormation",this.dataFormation);
    
    this.temporaryArray.forEach(element => {
      if (element) {
        element.isSelected = true;
        element.whereConditions = JSON.stringify(this.dataFormation);
      }

    });
    let data = {
      type: this.dataFormation,
      value: this.bodyText
    }
    this.emitData.emit(data);
  }


}

export const creteriaList = [{
  key: '=',
  value: '='
},
{
  key: '!=',
  value: '!='
},
{
  key: '>',
  value: '>'
},
{
  key: '>=',
  value: '>='
},
{
  key: 'between',
  value: 'between'
},
{
  key: '<',
  value: '<'
},
{
  key: '<=',
  value: '<='
},
{
  key: 'like',
  value: 'like'
},
{
  key: 'not like',
  value: 'not like'
},
{
  key: 'in',
  value: 'in'
},
{
  key: 'not in',
  value: 'not in'
},
{
  key: 'is null',
  value: 'is null'
},
{
  key: 'is not null',
  value: 'is not null'
}
];

