import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryValidationComponent } from './query-validation.component';

describe('QueryValidationComponent', () => {
  let component: QueryValidationComponent;
  let fixture: ComponentFixture<QueryValidationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueryValidationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
