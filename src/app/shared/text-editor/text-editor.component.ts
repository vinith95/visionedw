import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
// import KothingEditor from 'kothing-editor';
// import plugins from 'kothing-editor/lib/plugins';
import { isNullOrUndefined } from "util";

@Component({
  selector: "app-text-editor",
  templateUrl: "./text-editor.component.html",
  styleUrls: ["./text-editor.component.css"],
})
export class TextEditorComponent implements OnInit {
  emailBody: "";
  @Input() bodyText;
  @Input() title;
  @Output() emitData: EventEmitter<any> = new EventEmitter<any>();
  initEditor: any;

  constructor() {}

  ngOnInit(): void {
    this.emailBody = this.bodyText;
  }

  submit(type) {
    type == "clear" ? (this.emailBody = "") : "";
    let data = {
      type: type,
      value: this.emailBody,
    };
    this.emitData.emit(data);
  }
}
