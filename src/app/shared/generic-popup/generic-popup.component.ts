import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-generic-popup',
  templateUrl: './generic-popup.component.html',
  styleUrls: ['./generic-popup.component.css']
})
export class GenericPopupComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() popupType: string = 'alert';
  @Input() closeTime: string;
  @Input() orientationFlag?;
  @Input() groupingFlag?;
  @Input() groupingMessage?;
  @Input() openTemplate

  @Output() userConfirmation: EventEmitter<any> = new EventEmitter<any>();
  isPintitle: any;
  msgFlag: any;
  tempDescFlag: any

  constructor(private translate: TranslateService) {
    // this.translate.setDefaultLang(this.jwtService.get('setLanguage'));
    // this.translate.use(this.jwtService.get('setLanguage'));
    // // GET LANGUAGE
    // this.translate.currentLoader.getTranslation(this.translate.currentLang)
    //   .subscribe(data => {
    //   });
    this.isPintitle = "Pin"
  }

  ngOnInit() {
  }

  viewPdf = {
    mode: "P",
    grouping:"S",
  }

  userCofirm(e) {
    if (e == 'pin') {
      const isPin = document.getElementById('Layer_2').classList.contains('pin');
      this.isPintitle = (!isPin) ? "Unpin" : "Pin";
      (isPin) ? document.getElementById('Layer_2').classList.remove('pin') :
        document.getElementById('Layer_2').classList.add('pin');
      this.userConfirmation.emit(e);
    }
    else if (e == 'ok') {
     
      let obj = {
        type: e,
        mode: this.viewPdf.mode,
        grouping: this.viewPdf.grouping
      }
      this.userConfirmation.emit(obj);
      // this.userConfirmation.emit(this.msgFlag);
    }
    else if (e == 'Yes' && this.openTemplate) {
     
      // if(this.msgFlag && this.tempDescFlag){
      if(this.msgFlag){

        let param={
          type:e,
          name:this.msgFlag,
          description: this.tempDescFlag
        };
        this.userConfirmation.emit(param);

      }
      else{
        //this.globalService.showToastr.error('Please enter the template name and description.')

      }
    }
    else{
      let param={
        type:e,
      };
        this.userConfirmation.emit(param);

    }
  }
}
