import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecuteQueryNewComponent } from './execute-query-new.component';

describe('ExecuteQueryNewComponent', () => {
  let component: ExecuteQueryNewComponent;
  let fixture: ComponentFixture<ExecuteQueryNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecuteQueryNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecuteQueryNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
