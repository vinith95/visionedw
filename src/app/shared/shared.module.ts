import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { ToastrModule } from "ngx-toastr";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TreeModule } from "@circlon/angular-tree-component";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { NgxUsefulSwiperModule } from "ngx-useful-swiper";

// COMPONENT
import { TableComponent } from "./table/table.component";

import { ListViewComponent } from "./list-view/list-view.component";
import { SmartSearchComponent } from "./smart-search/smart-search.component";
import { ConfirmationModalComponent } from "./confirmation-modal/confirmation-modal.component";
import { ModalPopoupComponent } from "./modal-popoup/modal-popoup.component";
import { MagnifierComponent } from "./magnifier/magnifier.component";

import { alphanumeric } from "./alphaNumeric/alphanumeric";
import { SpinLoaderComponent } from "./spin-loader/spin-loader.component";
import { CloneDirective } from "./CloneDirective/clonedirective";

import {
  NgbDateParserFormatter,
  NgbDateStruct,
} from "@ng-bootstrap/ng-bootstrap";
import { NgbDateFRParserFormatter } from "./ngb-date-fr-parser-formatter";

import { DomSanPipe } from "./dom-san.pipe";
import { MagnifiernewComponent } from "./magnifiernew/magnifiernew/magnifiernew.component";

import { FusionChartsModule } from "angular-fusioncharts";
import * as FusionCharts from "fusioncharts";
import * as charts from "fusioncharts/fusioncharts.charts";
import * as FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import * as powercharts from "fusioncharts/fusioncharts.powercharts";
import * as maps from "fusioncharts/fusioncharts.maps";
import * as widgets from "fusioncharts/fusioncharts.widgets";
import * as usa from "fusioncharts/maps/fusioncharts.usa";
import * as kenya from "fusionmaps/maps/fusioncharts.kenya";
import * as nigeria from "fusionmaps/maps/fusioncharts.nigeria";
import * as africa from "fusionmaps/maps/fusioncharts.africa";
import * as ghana from "fusionmaps/maps/fusioncharts.ghana";
import * as Gantt from "fusioncharts/fusioncharts.gantt";
import * as Export from "fusioncharts/fusioncharts.excelexport";

FusionChartsModule.fcRoot(
  FusionCharts,
  charts,
  FusionTheme,
  powercharts,
  maps,
  usa,
  kenya,
  nigeria,
  africa,
  widgets,
  Gantt,
  Export
);
// FusionCharts.options['license']({
//   key:'uuA5exvF3F3I2C2A11C6C5A3F5E1A2E1mllC7E2B4dD4F3H3yxoF4E2D3isbB6C1E3ycaC1G4B1B8B4A4B3D3B4H3A33fhhD8B1SG4lhJ-7A9C11A5veE3NA1A1sslE2D6G2F3H3J3A7A5A4A2F4C2D1H4z==',
//   creditLabel: false,
// });

import { NgIdleKeepaliveModule } from "@ng-idle/keepalive";
import { ScrollDispatchModule } from "@angular/cdk/scrolling";
import { DndModule } from "ngx-drag-drop";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { CustomDatepickerModule } from "./custom-datepicker/custom-datepicker.module";
import {
  MatNativeDateModule,
  MatInputModule,
  MatDatepickerModule,
} from "@angular/material";
import {
  VirtualScrollerDefaultOptions,
  VirtualScrollerModule,
} from "ngx-virtual-scroller";
import { DynamicTableComponent } from "./dynamic-table/dynamic-table.component";
import { GenericPopupComponent } from "./generic-popup/generic-popup.component";
import { ClipboardModule } from "ngx-clipboard";
import { GenericTreeComponent } from "./generic-tree/generic-tree.component";
import { ExecuteQueryNewComponent } from "./execute-query-new/execute-query-new.component";
import { ShareOwnershipComponent } from "./share-ownership/share-ownership.component";
import { TreeViewComponent } from "./tree-view/tree-view.component";
import { UserManagementComponent } from "./share-ownership/user-management/user-management.component";
import { ExecuteQueryComponent } from "./execute-query/execute-query.component";
import { GridsterModule } from "angular-gridster2";
import { TableCustomizedComponent } from "./table-customized/table-customized.component";
import { SmartTableComponent } from "./smart-table/smart-table.component";
import { ReviewComponent } from "./review/review.component";
import { TextEditorComponent } from "./text-editor/text-editor.component";
import { FilterPromptComponent } from "./filter-prompt/filter-prompt.component";
import { AngularDraggableModule } from "angular2-draggable";
import { ConditionValiditionComponent } from "./condition-validition/condition-validition.component";
import { TablesListComponent } from "./tables-list/tables-list.component";
import { UseridSelectionsComponent } from "./userid-selections/userid-selections.component";
import { QueryValidationComponent } from "./query-validation/query-validation.component";
import {
  FontAwesomeModule,
  FaIconLibrary,
} from "@fortawesome/angular-fontawesome";
import {
  faSave,
  faPlayCircle,
  faCompress,
  faCheck,
  faPlay,
  faCompressAlt,
  faExpand,
  faExpandAlt,
  faSync,
  faRedo,
  faUndo,
  faEllipsisV,
  faSquare,
  faCircle,
  faDatabase,
  faSearchPlus,
  faSearchMinus,
  faWindowMinimize,
  faWindowMaximize,
} from "@fortawesome/free-solid-svg-icons";
import {
  faSave as farSave,
  faMinusSquare as farMinusSquare,
  faPlayCircle as farPlayCircle,
  faWindowMaximize as farWindowMaximize,
  faEye as farEye,
  faEyeSlash as farEyeSlash,
} from "@fortawesome/free-regular-svg-icons";
import { QuillModule } from "ngx-quill";

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

export function vsDefaultOptionsFactory(): VirtualScrollerDefaultOptions {
  return {
    checkResizeInterval: 1000,
    modifyOverflowStyleOfParentScroll: true,
    resizeBypassRefreshThreshold: 5,
    scrollAnimationTime: 750,
    scrollDebounceTime: 0,
    scrollThrottlingTime: 0,
    stripedTable: false,
  };
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  useBothWheelAxes: true,
};

@NgModule({
  declarations: [
    TableComponent,
    ListViewComponent,
    SmartSearchComponent,
    ConfirmationModalComponent,
    ModalPopoupComponent,
    MagnifierComponent,
    alphanumeric,
    CloneDirective,
    DynamicTableComponent,
    GenericPopupComponent,
    SpinLoaderComponent,
    DomSanPipe,
    GenericTreeComponent,
    ExecuteQueryNewComponent,
    ShareOwnershipComponent,
    TreeViewComponent,
    UserManagementComponent,
    ExecuteQueryComponent,
    TableCustomizedComponent,
    SmartTableComponent,
    ReviewComponent,
    TextEditorComponent,
    FilterPromptComponent,
    MagnifiernewComponent,
    ConditionValiditionComponent,
    TablesListComponent,
    UseridSelectionsComponent,
    QueryValidationComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    DndModule,
    DragDropModule,
    ClipboardModule,
    VirtualScrollerModule,
    NgxUsefulSwiperModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    ToastrModule.forRoot(),
    TreeModule,
    PerfectScrollbarModule,
    // TRANSLATE MODULE
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgMultiSelectDropDownModule.forRoot(),
    FusionChartsModule,
    NgIdleKeepaliveModule.forRoot(),
    ScrollDispatchModule,
    GridsterModule,
    CustomDatepickerModule,
    AngularDraggableModule,
    FontAwesomeModule,
    QuillModule.forRoot({
      modules: {
        // syntax: true,
        toolbar: [
          ["bold", "italic", "underline", "strike"], // toggled buttons
          // ['blockquote', 'code-block'],

          // [{ 'header': 1 }, { 'header': 2 }],               // custom button values
          [{ list: "ordered" }, { list: "bullet" }],
          [{ script: "sub" }, { script: "super" }], // superscript/subscript
          [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
          [{ direction: "rtl" }], // text direction

          [{ size: ["small", false, "large", "huge"] }], // custom dropdown
          [{ header: [1, 2, 3, 4, 5, 6, false] }],

          [{ color: [] }, { background: [] }], // dropdown with defaults from theme
          [{ font: [] }],
          [{ align: [] }],

          // ['clean']                                         // remove formatting button

          // ['link', 'image', 'video']                         // link and image, video]
        ],
      },
    }),
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    DomSanPipe,
    NgxUsefulSwiperModule,
    NgSelectModule,
    ClipboardModule,
    ToastrModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    DndModule,
    DragDropModule,
    VirtualScrollerModule,
    CustomDatepickerModule,
    HttpClientModule,
    TranslateModule,
    TreeModule,
    PerfectScrollbarModule,
    TableComponent,
    ListViewComponent,
    SmartSearchComponent,
    ModalPopoupComponent,
    MagnifierComponent,
    alphanumeric,
    CloneDirective,
    SpinLoaderComponent,
    DynamicTableComponent,
    GenericPopupComponent,
    NgMultiSelectDropDownModule,
    FusionChartsModule,
    NgIdleKeepaliveModule,
    GenericTreeComponent,
    ScrollDispatchModule,
    ExecuteQueryNewComponent,
    ShareOwnershipComponent,
    TreeViewComponent,
    UserManagementComponent,
    ExecuteQueryComponent,
    GridsterModule,
    TableCustomizedComponent,
    SmartTableComponent,
    ReviewComponent,
    TextEditorComponent,
    AngularDraggableModule,
    FontAwesomeModule,
    QuillModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    { provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter },
  ],
  entryComponents: [
    SmartSearchComponent,
    ConfirmationModalComponent,
    GenericPopupComponent,
    ModalPopoupComponent,
    MagnifierComponent,
    GenericTreeComponent,
    ExecuteQueryNewComponent,
    ExecuteQueryComponent,
  ],
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(
      faCompress,
      farSave,
      faCheck,
      faPlay,
      farMinusSquare,
      farPlayCircle,
      faWindowMinimize,
      faWindowMaximize,
      farWindowMaximize,
      faCompressAlt,
      faExpand,
      faExpandAlt,
      faSync,
      faRedo,
      faUndo,
      farEye,
      farEyeSlash,
      faEllipsisV,
      faSquare,
      faCircle,
      faDatabase,
      faSearchPlus,
      faSearchMinus,
      faSave,
      faPlayCircle
    );
  }
}
