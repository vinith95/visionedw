import { CdkVirtualScrollViewport } from "@angular/cdk/scrolling";
import { ApiService } from "../../../service";
import { environment } from "../../../../environments/environment";
import {
  Component,
  OnInit,
  Input,
  Output,
  ViewChild,
  EventEmitter,
} from "@angular/core";
import { log } from "console";
@Component({
  selector: "app-magnifiernew",
  templateUrl: "./magnifiernew.component.html",
  styleUrls: ["./magnifiernew.component.css"],
})
export class MagnifiernewComponent implements OnInit {
  @Input() query;
  @Input() specialFilterValue;
  @Input() searchData;
  @Input() title;

  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();
  branchList1;
  selectedData;
  startIndex;
  lastIndex;
  totalRows;
  branchList;
  nextIteration;
  maxRecordsFetch;
  @Input() inputDetails: any = [];
  selecteList: any = [];
  passvalue: any = [];
  Flag: boolean = false;
  totalRecords: any;
  @ViewChild(CdkVirtualScrollViewport, { static: false })
  viewport: CdkVirtualScrollViewport;

  constructor(
    private _apiService: ApiService
  ) { }

  ngOnInit() {
    this.inputDetails;
    this.passvalue;
    this.loadMagnifierData();
  }

  loadMagnifierData() {
    this.startIndex = 0;
    this.nextIteration = 50;
    this.maxRecordsFetch = 50;
    this.lastIndex = this.nextIteration;

    let data: any = {};
    if (typeof this.query == "object") {
      data = this.query;
      data["startIndex"] = this.startIndex;
      data["lastIndex"] = this.lastIndex;
      data["maxRecords"] = this.maxRecordsFetch;
    } else {
      data = {
        startIndex: this.startIndex,
        lastIndex: this.lastIndex,
        maxRecords: this.maxRecordsFetch,
        query: this.query,
        connectorId: this.inputDetails.connectorId,
        specialFilterValue: this.specialFilterValue,
      };
    }

    this._apiService
      .post(environment.get_Branch_List, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.branchList1 = resp["response"];
          this.totalRows = resp["otherInfo"].totalRows;
          this.branchList1.forEach((list1) => {
            this.inputDetails.forEach((element) => {
              if (
                list1.columnNine.toUpperCase() ==
                element.columnNine.toUpperCase()
              ) {
                list1.checked += true;
              }
            });
          });
          this.branchList = this.branchList1;
          this.totalRows = resp["otherInfo"].totalRows;
        }
      });
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  vpDataLength;
  dataLength;
  handler() {
    this.vpDataLength = this.viewport.getRenderedRange().end;
    this.dataLength = this.viewport.getDataLength();
    this.Flag
      ? this.vpDataLength <= this.startIndex
        ? (this.Flag = false)
        : ""
      : "";
    !this.Flag
      ? this.vpDataLength >= this.dataLength - 500 &&
        this.dataLength < this.totalRows
        ? this.loadData()
        : ""
      : "";
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  loadData() {
    this.Flag = true;
    this.startIndex = this.startIndex + this.nextIteration;
    this.lastIndex = this.lastIndex + this.nextIteration;
    if (this.startIndex >= this.totalRows && this.totalRows <= this.lastIndex) {
      this.lastIndex = this.totalRows;
    }
    let data: any = {};
    if (typeof this.query == "object") {
      data = this.query;
      data["startIndex"] = this.startIndex;
      data["lastIndex"] = this.lastIndex;
    } else {
      data = {
        startIndex: this.startIndex,
        lastIndex: this.lastIndex,
        maxRecords: this.maxRecordsFetch,
        totalRows: this.totalRows,
        query: this.query,
        specialFilterValue: this.specialFilterValue,
      };
    }

    this._apiService
      .post(environment.get_Branch_List, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          if (resp["response"] != null) {
            this.branchList = [...this.branchList, ...resp["response"]];
            this.branchList1 = this.branchList;
            this.totalRows = resp["otherInfo"].totalRows;
            this.branchList1.forEach((list1) => {
              this.inputDetails.forEach((selected) => {
                if (
                  list1.columnNine.toUpperCase() ==
                  selected.columnNine.toUpperCase()
                ) {
                  list1.checked += true;
                }
              });
            });
          }
        }
      });
    //}
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  maxRecords: any;
  filter = (searchData) => {
    this.branchList1 = [];
    this.branchList = [];
    this.startIndex = 0;
    this.lastIndex = 1000;
    this.maxRecords = 50;
    let data: any = {};
    if (typeof this.query == "object") {
      data = this.query;
      data["startIndex"] = this.startIndex;
      data["lastIndex"] = this.lastIndex;
      data["maxRecords"] = 50;
      data["smartSearchOpt"] = [
        {
          object: "search",
          joinType: "AND",
          criteria: "LIKES",
          value: searchData ? searchData : "",
        },
      ];
    } else {
      data = {
        startIndex: this.startIndex,
        lastIndex: this.lastIndex,
        query: this.query,
        maxRecords: 50,
        specialFilterValue: this.specialFilterValue,
        smartSearchOpt: [
          {
            object: "search",
            joinType: "AND",
            criteria: "LIKES",
            value: searchData ? searchData : "",
          },
        ],
      };
    }

    this._apiService
      .post(environment.get_Branch_List, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          this.branchList1 = [...this.branchList1, ...resp["response"]];
          this.branchList = [...this.branchList, ...this.branchList1];
          this.totalRows = resp["otherInfo"].totalRows;
          this.branchList1.forEach((list1) => {
            this.inputDetails.forEach((selected) => {
              if (list1.columnNine == selected.columnNine) {
                list1.checked += true;
              }
            });
          });
        }
      });
  };

  updateSelectedData = (selectedData) => {
    this.selectedData = selectedData;
  };

  saveSelectedOnes() {
    let arrayOne = this.inputDetails.filter((x) => x.checked === true);
    let arrayTwo = this.branchList1.filter((x) => x.checked === true);
    this.passvalue = arrayOne.concat(arrayTwo);
    this.filterData.emit(this.passvalue);

  }

  GetStats(value) {
    if (value != undefined) {
      value.newRecord = true;
    }
    this.selecteList.push(value);
    //console.log("this.selectedlist",this.selecteList)
    this.selecteList.forEach((list1) => {
      this.inputDetails.forEach((selected) => {
        if (list1.columnNine == selected.columnNine) {
          selected.checked += false;
        }
      });
    });
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  closeModal = () => {
    this.filterData.emit("close");
  };
}
