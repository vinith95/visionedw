import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  AfterViewInit,
  OnChanges,
  Injector,
  ViewChild,
  ElementRef,
  SimpleChanges,
} from "@angular/core";
import {
  DomSanitizer,
  SafeResourceUrl,
  SafeUrl,
} from "@angular/platform-browser";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { GlobalService, ApiService, JwtService } from "../../service";
import { SmartSearchComponent } from "../../shared/smart-search/smart-search.component";
import { LoaderService } from "../../service/loader.service";
import {
  trigger,
  state,
  style,
  transition,
  animate,
} from "@angular/animations";
import { toInteger } from "@ng-bootstrap/ng-bootstrap/util/util";
import { isUndefined, isArray, isObject, isNull } from "util";
import { typeWithParameters } from "@angular/compiler/src/render3/util";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
} from "ngx-perfect-scrollbar";
// import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from "util";
@Component({
  selector: "app-smart-table",
  templateUrl: "./smart-table.component.html",
  styleUrls: ["./smart-table.component.css"],
  animations: [
    trigger("fadeIn", [
      transition(":enter", [
        style({ opacity: "0" }),
        animate(".5s ease-out", style({ opacity: "1" })),
      ]),
    ]),
  ],
})
export class SmartTableComponent implements OnInit {
  dataColumnList = [];
  screentypeToolTip: any = "Maximize";
  apiLink: string = "";
  filterApi: any = "";
  pagination: any = {
    totalItems: 0,
    startIndex: 1,
    lastIndex: 20,
    actionType: "Query",
    currentPage: 1,
  };
  individualFilterList = {};
  title: any = "";
  limitedPagination: any = {
    totalItems: 0,
    startIndex: 0,
    lastIndex: 20,
  };
  perPage: any = 20;
  compact_css: boolean = false;
  limitedList: any = [];
  overAllListCount: any = 0;
  previousDisp: boolean = false;
  nextDisp: boolean = false;
  screentype = "fullscreen";
  last_toolTip: any;
  addLable: any = "";
  routingLable: any = "";
  userBasedCount: any = 20;
  viewList: any = [];
  filterList = [];
  monthList = [
    {
      name: "January",
      id: "01",
    },
    {
      name: "February",
      id: "02",
    },
    {
      name: "March",
      id: "03",
    },
    {
      name: "April",
      id: "04",
    },
    {
      name: "May",
      id: "05",
    },
    {
      name: "June",
      id: "06",
    },
    {
      name: "July",
      id: "07",
    },
    {
      name: "August",
      id: "08",
    },
    {
      name: "September",
      id: "09",
    },
    {
      name: "October",
      id: "10",
    },
    {
      name: "November",
      id: "11",
    },
    {
      name: "December",
      id: "12",
    },
  ];
  data = {};
  userGroupFilterData;
  userProfileFilterData;
  homeScreenData = "";
  homeScreenArry = [];
  filterTypeTwoData;
  filterTypeTwoData1 = {};
  showFilterTypeTwo = false;
  filterDataRe: any;
  postData: any;
  buttoncronStaus: Boolean = false;

  buttoncronStausAll: Boolean = true;
  showCronStatus: Boolean = false;
  otherInfo = {};
  cronStat_Tooltip: any;
  buttonCheck: boolean;
  // ------------------------------------------------------------
  @Input() tableType?;
  @Input() tableDatails;
  @Input() tableData;
  @Input() totalRows;
  @Input() filter;
  @Input() resetModel: any;
  @Input() currentRowVal: number;
  bulkChecked: boolean;
  showReportId: boolean = false;
  reportIdList: any = [];
  burstIdList: any = [];
  reportId;
  burstId;
  formField: any = {};
  checkingvalues: any;
  staticDelete: boolean = false;
  @Output() childData = new EventEmitter();
  @Input() set tableDatas(value: any) {
    //cronRunStatus
    if (
      !isNullOrUndefined(value["cronStatus_Show"]) &&
      value["cronStatus_Show"]
    ) {
      this.otherInfo = value["cronStatus_Show"].cronRunStatus;
      this.staticDelete = true;
      if (this.otherInfo == "Y") {
        this.buttoncronStaus = true;
        this.cronStat_Tooltip = "Start";
      } else {
        this.buttoncronStaus = false;
        this.cronStat_Tooltip = "Stop";
      }
    } else {
      this.staticDelete = false;
    }

    if (!isNullOrUndefined(value["cronStatus"]) && value["cronStatus"]) {
      this.otherInfo = value["cronStatus"];
      this.showCronStatus = true;
      if (this.otherInfo == "Y") {
        this.buttoncronStaus = true;
        this.cronStat_Tooltip = "Start";
      } else {
        this.buttoncronStaus = false;
        this.cronStat_Tooltip = "Stop";
      }
    }
    this.checkingvalues = value["cronStatus"];

    this.apiLink = value["api"];
    this.filterApi = value["filterApi"];
    this.filterDataRe = value["filterData"];
    this.userBasedCount = value["count"] < 20 ? 20 : value["count"];
    this.pagination["lastIndex"] = value["count"] < 20 ? 20 : value["count"];
    this.homeScreenArry = value["homeDashboard"];

    if (value["postData"]) {
      this.postData = value["postData"];
    }

    if (value["data"]) {
      this.showFilterTypeTwo = true;
      this.filterTypeTwoData = value["data"];
      this.filterTypeTwoData.forEach((element) => {
        this.filterTypeTwoData1[element.alphaSubTab] = element.children;
      });
    }
    if (value["reportId"]) {
      this.showReportId = true;
      this.reportIdList = value["reportId"];
      // this.reportIdList.forEach(element => {
      //   this.filterTypeTwoData1[element.alphaSubTab] = element.children;
      // });
    }
    if (value["formField"]) {
      this.formField = value["formField"];
    }
  }
  @Output() eventType: EventEmitter<any> = new EventEmitter();
  loaderService = this.injector.get(LoaderService);
  showData = false;
  @ViewChild("perfectScroll", { static: false })
  perfectScroll: PerfectScrollbarComponent;
  filterUserData() {
    this.data["userGroup"] = this.userGroupFilterData;
    this.data["userProfile"] = this.userProfileFilterData;
    this.initializePagination();
  }

  // ------------------------------------------------------------
  constructor(
    private _sanitizer: DomSanitizer,
    public globalService: GlobalService,
    private modalService: NgbModal,
    private injector: Injector,
    private apiService: ApiService,
    // public translate: TranslateService,
    public jwtService: JwtService
  ) {
    // this.translate.setDefaultLang(this.jwtService.get('setLanguage'));
    //   this.translate.use(this.jwtService.get('setLanguage'));
    //   // GET LANGUAGE
    //   this.translate.currentLoader.getTranslation(this.translate.currentLang)
    //     .subscribe(data => {
    //     });
  }

  pageLevelAccess;
  activedRowCls: string = "";

  // ------------------------------------------------------------
  ngOnInit() {
    if (this.showFilterTypeTwo) {
      this.userGroupFilterData = this.filterTypeTwoData[0]["alphaSubTab"];
      this.userProfileFilterData =
        this.filterTypeTwoData1["ADMIN"][0]["alphaSubTab"];
      this.filterUserData();
    }
    if (this.showReportId) {
      if (this.formField && this.formField["formPage"]) {
        this.reportIdList.forEach((report) => {
          if (report["alphaSubTab"] == this.formField["reportId"]) {
            this.reportId = report["alphaSubTab"];
          }
          report.children.forEach((burst) => {
            if (burst["alphaSubTab"] == this.formField["burstId"]) {
              this.burstId = burst["alphaSubTab"];
            }
          });
        });
        this.filterReportData();
      } else {
        this.reportId = this.reportIdList[0]["alphaSubTab"];
        this.burstId = this.reportIdList[0]["children"][0]["alphaSubTab"];
        this.filterReportData();
      }
    } else {
      this.tableType != undefined && this.apiLink == ""
        ? this.recallFullApi()
        : this.initializePagination();
    }
    this.pageLevelAccess = this.globalService.pageLevelAccess;
  }

  filterReportData() {
    this.reportIdList.forEach((element) => {
      if (element.alphaSubTab == this.reportId) {
        this.burstIdList = element.children;
      }
    });
    this.data["reportId"] = this.reportId;
    this.data["burstId"] = this.burstId;
    this.initializePagination();
  }

  reportIdChange() {
    this.burstId = "";
    this.reportIdList.forEach((element) => {
      if (element.alphaSubTab == this.reportId) {
        this.burstIdList = element.children;
      }
    });
  }
  homeDashapiCall() {
    let data = {
      userGroup: this.userGroupFilterData,
      userProfile: this.userProfileFilterData,
      homeDashboard: this.homeScreenData,
    };
    this.apiService
      .post(this.apiService.getEnvironment()["updateHomeDashboard"], data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }

  // ------------------------------------------------------------
  // gettotalRows() {

  //   !isUndefined(this.apiLink) ? this.apiService.post(this.apiService.getEnvironment()[this.apiLink], { actionType: "Query",currentPage: 1,startIndex:0,lastIndex:0}).subscribe(resp => {
  //     if (resp['status']) {
  //       this.overAllListCount = ('otherInfo' in resp) ? ('totalRows' in resp['otherInfo']) ? resp['otherInfo']['totalRows'] : '' : '';
  //       this.limitedPagination['totalItems'] = this.overAllListCount;
  //       this.pagination['totalItems'] = this.overAllListCount;
  //       this.initializePagination();

  //     } else {
  //       this.globalService.showToastr.error(resp['message']);
  //     }
  //   }) : this.viewList = this.tableData;
  // }

  getFullTable() {
    if (!isNullOrUndefined(this.tableData)) {
      this.overAllListCount = this.tableData.length;
      this.limitedPagination["totalItems"] = this.overAllListCount;
      this.pagination["totalItems"] = this.overAllListCount;
    }
    this.initializePagination();
  }

  // ------------------------------------------------------------
  ngOnChanges(changes: SimpleChanges) {
    this.tableType != undefined && this.apiLink == ""
      ? this.recallFullApi()
      : this.resetModel["key"] == "reload"
      ? this.recallApi()
      : "";
  }

  // ------------------------------------------------------------
  ngAfterViewInit() {
    // this.last_toolTip = document.getElementById('last_toolTip');
    // this.last_toolTip.setAttribute('tooltip', 'Maximize');
    if (!isUndefined(this.filter)) {
      const tempArray = [];
      this.filter.forEach((element) => {
        if (!element.hasOwnProperty("filterReferField")) {
          element.filterReferField = element.name;
        }
        tempArray.push(element);
      });
      this.dataColumnList = tempArray;
    } else {
      const tempArray = [];
      this.tableDatails.headers.forEach((element) => {
        if (element.isFilterField) {
          if (!element.hasOwnProperty("filterReferField")) {
            element.filterReferField = element.name;
          }
          tempArray.push(element);
        }
      });
      this.dataColumnList = tempArray;
    }
  }

  // ------------------------------------------------------------
  closeModal() {
    this.modalService.dismissAll();
  }

  // ------------------------------------------------------------
  resetFilter() {
    const data = {
      startIndex: 0,
      lastIndex: 1000,
      smartSearchOpt: [],
    };

    this.emitActionType("pagination", data);
    this.initializePagination();
  }

  // ------------------------------------------------------------
  secureInputHtml(html) {
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  // ------------------------------------------------------------
  bulkSelect(e) {
    this.viewList.forEach((element) => {
      element.checked = e.target.checked === true ? true : false;
      this.bulkChecked = element.checked;
    });
  }

  // ------------------------------------------------------------
  toggleCheckBoxSelect = (e, index) => {
    if (e.target.checked) {
      this.viewList[index].checked = true;
    } else {
      this.viewList[index].checked = false;
    }
    const checked = this.viewList.some((element) => element.checked == false);
    this.bulkChecked = checked ? false : true;
  };

  // ------------------------------------------------------------
  emitActionType(type, data?, index?) {
    if (
      !isNullOrUndefined(this.viewList) &&
      (data == undefined || data == "" || data == null)
    ) {
      data = this.viewList.filter((x) => {
        if (x.checked) {
          return true;
        }
      });
    }
    //let index=index1+this.limitedPagination['startIndex'];
    let currentPage = this.limitedPagination.lastIndex / 20;
    let totalItems = this.limitedPagination["totalItems"];
    let lineList = this.viewList;
    const obj = {
      type,
      data,
      index,
      currentPage,
      lineList,
      totalItems,
      formData: { reportId: this.reportId, burstId: this.burstId },
    };
    obj["filter"] = type == "refresh" ? this.filterList : undefined;
    this.eventType.next(obj);
  }

  // ------------------------------------------------------------
  showApproveOrRejectButton = () => {
    if (this.viewList) {
      return this.viewList.some((element) => element.checked === true);
    } else {
      return false;
    }
  };

  // ------------------------------------------------------------
  previewScreen = () => {
    let screensize = document.getElementById("maximize");
    screensize.classList.contains("fullscreen")
      ? this.minimize(screensize)
      : this.maximize(screensize);
  };

  // screentype = 'fullscreen';

  // ------------------------------------------------------------
  minimize = (minimize: any) => {
    this.screentypeToolTip = "Maximize";
    minimize.classList.remove("fullscreen");
    this.screentype = "fullscreen";
    //this.last_toolTip.setAttribute('tooltip', 'maximize');
    let tooltip = document.getElementsByClassName("tool_tip");
    for (var i = 0; i < tooltip.length; i++) {
      tooltip[i].removeAttribute("flow");
    }
  };

  // ------------------------------------------------------------
  maximize = (maximize: any) => {
    this.screentypeToolTip = "Minimize";
    maximize.classList.add("fullscreen");
    this.screentype = "fullscreen_exit";
    //this.last_toolTip.setAttribute('tooltip', 'Minimize');
    let tooltip = document.getElementsByClassName("tool_tip");
    for (var i = 0; i < tooltip.length; i++) {
      tooltip[i].setAttribute("flow", "left");
    }
  };

  getClass(data) {
    if (data == "Active") {
      return "text-success";
    } else if (data == "Approved") {
      return "text-warning";
    } else if (data == "Inactive") {
      return "text-danger";
    }
  }

  // ------------------------------------------------------------
  initializePagination = () => {
    if (isObject(this.data)) {
      this.pagination = { ...this.pagination, ...this.data };
    }
    if (!isNullOrUndefined(this.tableDatails.options.highLightRowIndex.index)) {
      this.pagination["lastIndex"] =
        this.tableDatails.options.highLightRowIndex.currentPage * 20;
      (this.pagination["startIndex"] = this.pagination["lastIndex"] - 19),
        (this.pagination["currentPage"] =
          this.tableDatails.options.highLightRowIndex.currentPage);
    } else {
      this.pagination["lastIndex"] = 20;
      (this.pagination["startIndex"] = 1), (this.pagination["currentPage"] = 1);
    }
    if (this.postData) {
      this.pagination["connectorId"] = this.postData.connectorId;
    }
    !isUndefined(this.apiLink) && this.apiLink != ""
      ? this.apiService
          .post(this.apiService.getEnvironment()[this.apiLink], this.pagination)
          .subscribe((resp) => {
            if (resp["status"]) {
              this.showData = true;
              this.overAllListCount =
                "otherInfo" in resp
                  ? "totalRows" in resp["otherInfo"]
                    ? resp["otherInfo"]["totalRows"]
                    : ""
                  : "";
              this.limitedPagination["totalItems"] = this.overAllListCount;
              this.pagination["totalItems"] = this.overAllListCount;
              this.showData = true;
              if (this.apiLink == "profile_get_all_values") {
                this.homeScreenData =
                  "otherInfo" in resp && !isNull(resp["otherInfo"])
                    ? resp["otherInfo"]["homeDashboard"]
                    : "";
              }
              this.limitedList =
                resp["response"] != null ? resp["response"] : [];
              this.viewList = this.limitedList.slice(0, 20);
              if (
                !isNullOrUndefined(
                  this.tableDatails.options.highLightRowIndex.index
                )
              ) {
                this.limitedPagination["startIndex"] =
                  this.pagination["startIndex"] - 1;
                this.limitedPagination["lastIndex"] =
                  this.pagination["lastIndex"];
                setTimeout(() => {
                  let idIndex =
                    this.tableDatails.options.highLightRowIndex.index;
                  document
                    .getElementById("highLightRow" + idIndex)
                    .classList.add("hoverClassRow");
                  let scrollIndex = idIndex - 2;
                  this.perfectScroll.directiveRef.scrollToTop(
                    scrollIndex *
                      document.getElementById("highLightRow" + idIndex)
                        .scrollHeight
                  );
                });
              }
            } else {
              this.globalService.showToastr.error(resp["message"]);
            }
            this.paginationDisable();
          })
      : this.fullTableSlice();
  };

  fullTableSlice() {
    this.showData = true;
    this.viewList = [];
    if (!isNullOrUndefined(this.tableData)) {
      this.limitedList = this.tableData;
      this.viewList = this.tableData.slice(0, 20);
      this.paginationDisable();
    }
  }

  // ------------------------------------------------------------
  resetPagination() {
    this.perPage = 20;
    this.limitedPagination["startIndex"] = 0;
    this.limitedPagination["lastIndex"] = 20;
  }

  // ------------------------------------------------------------
  previous = () => {
    this.limitedPagination["startIndex"] =
      this.limitedPagination["startIndex"] - this.perPage;
    this.limitedPagination["lastIndex"] =
      this.limitedPagination["lastIndex"] - this.perPage;
    //this.viewList = this.limitedList.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
    this.previousDataList();
    this.paginationDisable();
  };

  // ------------------------------------------------------------
  next = () => {
    this.limitedPagination["startIndex"] =
      this.limitedPagination["startIndex"] + this.perPage;
    this.limitedPagination["lastIndex"] =
      this.limitedPagination["lastIndex"] + this.perPage;
    this.nextDataList();
    // this.viewList = this.limitedList.slice(this.limitedPagination['startIndex'], this.limitedPagination['lastIndex']);
    // (this.viewList.length < this.perPage && this.limitedList.length < this.overAllListCount) ? this.nextDataList() : '';
    this.paginationDisable();
  };

  // ------------------------------------------------------------
  paginationDisable = () => {
    this.previousDisp =
      this.limitedPagination["startIndex"] == 0 ? true : false;
    this.nextDisp =
      this.viewList.length < this.perPage &&
      this.limitedList.length == this.overAllListCount
        ? true
        : this.limitedPagination["lastIndex"] >= this.overAllListCount
        ? true
        : false;
  };
  previousDataList() {
    let details = {
      actionType: "Query",
      totalRows: this.overAllListCount,
      startIndex: this.limitedPagination.startIndex + 1,
      lastIndex: this.limitedPagination.lastIndex,
      currentPage: this.limitedPagination.lastIndex / 20,
    };
    if (this.postData) {
      details["connectorId"] = this.postData.connectorId;
    }
    if (isObject(this.data)) {
      details = { ...details, ...this.data };
    }
    if (this.globalService.filterList.length) {
      details["smartSearchOpt"] = this.filterList;
      this.apiService
        .post(this.apiService.getEnvironment()[this.filterApi], details)
        .subscribe((resp) => {
          if (resp["status"]) {
            let list = resp["response"] != null ? resp["response"] : [];
            this.limitedList = [...this.limitedList, ...list];
            this.viewList = this.limitedList.slice(
              this.limitedPagination["startIndex"],
              this.limitedPagination["lastIndex"]
            );
            this.viewList = list;
            this.bulkChecked = false;
            this.paginationDisable();
            this.perfectScroll.directiveRef.scrollToTop();
          }
        });
    } else {
      this.apiService
        .post(this.apiService.getEnvironment()[this.apiLink], details)
        .subscribe((resp) => {
          if (resp["status"]) {
            let list = resp["response"] != null ? resp["response"] : [];
            this.limitedList = [...this.limitedList, ...list];
            this.viewList = this.limitedList.slice(
              this.limitedPagination["startIndex"],
              this.limitedPagination["lastIndex"]
            );
            this.viewList = list;
            this.bulkChecked = false;
            this.paginationDisable();
            this.perfectScroll.directiveRef.scrollToTop();
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
    }
  }
  // ------------------------------------------------------------
  nextDataList = () => {
    let details = {
      actionType: "Query",
      totalRows: this.overAllListCount,
      startIndex: this.limitedPagination.startIndex + 1,
      lastIndex: this.limitedPagination.lastIndex,
      currentPage: this.limitedPagination.lastIndex / 20,
    };
    if (isObject(this.data)) {
      details = { ...details, ...this.data };
    }
    if (this.filterList.length) {
      details["smartSearchOpt"] = this.filterList;
      if (this.postData) {
        details["connectorId"] = this.postData.connectorId;
      }
      this.apiService
        .post(this.apiService.getEnvironment()[this.filterApi], details)
        .subscribe((resp) => {
          if (resp["status"]) {
            let list = resp["response"] != null ? resp["response"] : [];
            this.limitedList = [...this.limitedList, ...list];
            this.viewList = this.limitedList.slice(
              this.limitedPagination["startIndex"],
              this.limitedPagination["lastIndex"]
            );
            this.viewList = list;
            this.bulkChecked = false;
            this.paginationDisable();
            this.perfectScroll.directiveRef.scrollToTop();
          }
        });
    } else {
      if (this.postData) {
        details["connectorId"] = this.postData.connectorId;
      }
      this.apiService
        .post(this.apiService.getEnvironment()[this.apiLink], details)
        .subscribe((resp) => {
          if (resp["status"]) {
            let list = resp["response"] != null ? resp["response"] : [];
            this.limitedList = [...this.limitedList, ...list];
            this.viewList = this.limitedList.slice(
              this.limitedPagination["startIndex"],
              this.limitedPagination["lastIndex"]
            );
            this.viewList = list;
            this.bulkChecked = false;
            this.paginationDisable();
            this.perfectScroll.directiveRef.scrollToTop();
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
    }
  };

  // ------------------------------------------------------------
  getPaginationDet = () => {
    let first = this.limitedPagination["startIndex"] + 1;
    let last =
      this.limitedPagination["lastIndex"] > this.overAllListCount
        ? this.overAllListCount
        : this.limitedPagination["lastIndex"];
    let aa = `${first} - ${last}  of
      ${this.overAllListCount}`;
    return aa;
  };

  // ------------------------------------------------------------
  listCountDisp = () => {
    this.perPage = parseInt(this.perPage);
    this.limitedPagination["startIndex"] = 0;
    this.limitedPagination["lastIndex"] = this.perPage;
    this.viewList = this.limitedList.slice(
      this.limitedPagination["startIndex"],
      this.limitedPagination["lastIndex"]
    );
    this.paginationDisable();
  };

  dateFormat(data) {
    return this.globalService.getDateFormat(data);
  }

  // ------------------------------------------------------------
  openSearchModal = (type?) => {
    const modelRef = this.modalService.open(SmartSearchComponent, {
      size: "lg",
      backdrop: "static",
    });
    modelRef.componentInstance.dataToDisplay = this.dataColumnList;
    modelRef.componentInstance.searchData = this.globalService.filterList;
    modelRef.componentInstance.filterData.subscribe((e) => {
      if (e.flag == "apply") {
        this.globalService.filterList = e.data;
        this.applyFilterData(this.globalService.filterList);
        this.individualFilterDisp();
      } else if (e.flag == "reset") {
        this.globalService.filterList = e.data;
        this.resetFilter();
        this.individualFilterDisp();
      }
      modelRef.close();
    });
  };

  individualFilterDisp = () => {
    this.dataColumnList.map((ele) => {
      ele["filter"] = false;
    });
    this.dataColumnList.forEach((ele) => {
      this.individualFilterList[ele.id] = [];
    });
    this.filterList.map((ele) => {
      let pos = this.dataColumnList
        .map((ele) => {
          return ele.id;
        })
        .indexOf(ele.object);
      if (pos != -1) {
        this.dataColumnList[pos]["filter"] = true;
        this.individualFilterList[ele.object] = [
          ...this.individualFilterList[ele.object],
          ...[ele],
        ];
      }
    });
  };

  individualFilter = (key) => {
    const modelRef = this.modalService.open(SmartSearchComponent, {
      size: "lg",
      backdrop: "static",
    });
    modelRef.componentInstance.dataToDisplay = [key];
    modelRef.componentInstance.type = "single";
    modelRef.componentInstance.searchData = this.individualFilterList[key.id];
    modelRef.componentInstance.filterData.subscribe((e) => {
      if (e.flag == "apply") {
        this.filterList = this.filterList.filter((resp, ind, arr) => {
          if (resp.object != key.id) {
            return true;
          }
        });
        this.individualFilterList[key.id] = e.data;
        this.filterList = [...this.filterList, ...e.data];
        this.applyFilterData(this.filterList);
        this.individualFilterDisp();
      } else if (e.flag == "reset") {
        this.filterList = this.filterList.filter((resp, ind, arr) => {
          if (resp.object != key.id) {
            return true;
          }
        });
        // this.filterList = e.data;
        this.individualFilterList[key.id] = [];
        this.individualFilterDisp();
        if (this.filterList.length) {
          this.applyFilterData(this.filterList);
        } else {
          this.resetFilter();
        }
      }
      modelRef.close();
    });
  };

  applyFilterData = (filterList) => {
    if (this.filterApi != "") {
      let details = Object.assign({}, this.pagination);
      if (this.data) {
        details = { ...details, ...this.data };
      }
      details["smartSearchOpt"] = filterList;
      this.limitedList = [];
      this.viewList = [];
      this.overAllListCount = 0;
      this.limitedPagination["startIndex"] = 0;
      this.limitedPagination["lastIndex"] = 20;
      this.perPage = 20;
      details["startIndex"] = 0;
      details["lastIndex"] = 20;
      details["currentPage"] = 1;
      this.apiService
        .post(this.apiService.getEnvironment()[this.filterApi], details)
        .subscribe((resp) => {
          if (resp["status"]) {
            this.overAllListCount =
              "otherInfo" in resp
                ? "totalRows" in resp["otherInfo"]
                  ? resp["otherInfo"]["totalRows"]
                  : ""
                : "";
            this.limitedPagination["totalItems"] = this.overAllListCount;
            this.pagination["totalItems"] = this.overAllListCount;
            this.limitedList = resp["response"] != null ? resp["response"] : [];
            this.viewList = this.limitedList.slice(0, 20);
            this.viewList = this.limitedList;
            this.bulkChecked = false;
            this.paginationDisable();
          }
        });
      this.paginationDisable();
    } else {
      if (!isNullOrUndefined(this.tableData)) {
        let filterData = this.filterMethod(filterList);
        this.limitedList = [];
        this.viewList = [];
        this.overAllListCount = 0;
        this.limitedPagination["startIndex"] = 0;
        this.limitedPagination["lastIndex"] = 20;
        this.perPage = 20;
        if (filterData.length) {
          this.overAllListCount = filterData.length;
          this.limitedPagination["totalItems"] = this.overAllListCount;
          this.pagination["totalItems"] = this.overAllListCount;
          this.limitedList = filterData;
          this.viewList = this.limitedList.slice(0, 20);
          this.paginationDisable();
        }
      }
      this.paginationDisable();
    }
  };

  recallFullApi() {
    if (!isNullOrUndefined(this.filterDataRe)) {
      if (this.filterDataRe.length) {
        this.globalService.filterList = this.filterDataRe;
        this.applyFilterData(this.globalService.filterList);
      } else {
        this.getFullTable();
      }
    } else {
      this.getFullTable();
    }
    this.resetPagination();
  }

  recallApi = () => {
    if (this.filterList.length) {
      this.applyFilterData(this.filterList);
    } else {
      this.initializePagination();
    }
    this.resetPagination();
  };

  iconVisible = (value: any) => {
    let return_value = true;
    return_value =
      value.hasOwnProperty("minCallReportFileName") &&
      value.minCallReportFileName != null
        ? true
        : false;
    return return_value;
  };

  iconVisible1 = (actionData, value: any) => {
    let return_value = true;
    let tempReturnValue = false;
    if (actionData.validations) {
      actionData.conditon.value.forEach((element) => {
        if (value[actionData.conditon.key] == element) {
          tempReturnValue = true;
        }
      });
      return_value = tempReturnValue;
    }
    return return_value;
  };

  iconVisible2 = (actionData, value: any) => {
    let return_value = true;
    if (!isNullOrUndefined(actionData.statusCondition)) {
      let tempReturnValue;
      actionData.statusCondition.value.forEach((element) => {
        tempReturnValue = !actionData.statusCondition.value.includes(
          value[actionData.statusCondition.key]
        );
      });
      return_value = tempReturnValue;
    }
    return return_value;
  };
  removeClass() {
    this.viewList.forEach((element, index) => {
      document
        .getElementById("highLightRow" + index)
        .classList.remove("hoverClassRow");
    });
  }

  filterMethod(dataArr: any) {
    let searching_list = [];
    dataArr.forEach((element) => {
      searching_list.push({
        [element.object]: element.value,
        types: element.criteria,
      });
    });
    let fields = this.tableData[0],
      items = this.tableData;
    if (items.length && items) {
      var filldata = items.filter((item) => {
        for (let prop in fields) {
          for (var i = 0; i < searching_list.length; i++) {
            if (
              searching_list[i]["types"] == "LIKE" ||
              searching_list[i]["types"] == ""
            ) {
              if (
                searching_list[i][prop] &&
                String(item[prop])
                  .toLowerCase()
                  .indexOf(String(searching_list[i][prop]).toLowerCase()) === -1
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "ENDSWITH") {
              if (
                searching_list[i][prop] &&
                item[prop] &&
                item[prop]
                  .toLowerCase()
                  .endsWith(searching_list[i][prop].toLowerCase()) - 1
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "STARTSWITH") {
              if (
                searching_list[i][prop] &&
                item[prop] &&
                item[prop]
                  .toLowerCase()
                  .startsWith(searching_list[i][prop].toLowerCase()) - 1
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "EQUALS") {
              if (
                searching_list[i][prop] &&
                item[prop] &&
                item[prop].toLowerCase() !==
                  searching_list[i][prop].toLowerCase()
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "<") {
              if (
                searching_list[i][prop] &&
                searching_list[i][prop] - 1 < item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == ">") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) + 1 > item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "<=") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) < item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == ">=") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) > item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "==") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) != item[prop]
              ) {
                return false;
              }
            } else if (searching_list[i]["types"] == "!=") {
              if (
                searching_list[i][prop] &&
                parseInt(searching_list[i][prop]) == item[prop]
              ) {
                return false;
              }
            }
          }
        }
        return true;
      });
      return filldata;
    }
  }

  checkAccess(data) {
    if (data.actionType == "edit") {
      if (this.globalService.pageLevelAccess.profileModify == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data.actionType == "copy") {
      if (this.globalService.pageLevelAccess.profileAdd == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (
      data.actionType == "view" ||
      data.actionType == "childrenView" ||
      data.actionType == "acountOfficerView"
    ) {
      if (this.globalService.pageLevelAccess.profileView == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data.actionType == "delete") {
      if (this.globalService.pageLevelAccess.profileDelete == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data.actionType == "download") {
      if (this.globalService.pageLevelAccess.profileDownload == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data.actionType == "upload") {
      if (this.globalService.pageLevelAccess.profileUpload == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data.actionType == "approve" || data.actionType == "reject") {
      if (this.globalService.pageLevelAccess.profileVerification == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (
      data.actionType == "update" ||
      data.actionType == "dealstage" ||
      data.actionType == "dealcalls" ||
      data.actionType == "confidentlevel" ||
      data.actionType == "statusupdate"
    ) {
      if (this.globalService.pageLevelAccess.profileModify == "Y") {
        return true;
      } else {
        return false;
      }
    }

    return true;
  }

  findMyBrowser() {
    if (navigator.userAgent.indexOf("Firefox") != -1) {
      return true;
    } else {
      return false;
    }
  }

  cornStausChange(value) {
    let cronStatus;
    if (value == "Y") {
      cronStatus = "N";
    } else {
      cronStatus = "Y";
    }
    let data = {
      cronStatus: cronStatus,
    };
    this.apiService
      .post(this.apiService.getEnvironment()["eventApi_getCornStatus"], data)
      .subscribe((resp) => {
        if (resp["status"]) {
          let response = resp["response"];
          // console.log("cronStatus",response.cronStatus);
          this.otherInfo = response.cronStatus;
          if (response.cronStatus == "Y") {
            this.buttoncronStaus = true;
            this.cronStat_Tooltip = "Start";
          } else {
            this.buttoncronStaus = false;
            this.cronStat_Tooltip = "Stop";
          }
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }

  cornStausChangeAll(value) {
    let RunStatus;
    if (value == true) {
      RunStatus = "Y";
    } else {
      RunStatus = "N";
    }
    let data = {
      cronRunStatus: RunStatus,
    };
    this.apiService
      .post(
        this.apiService.getEnvironment()[
          "prdCronControl_eventApi_getCornStatus"
        ],
        data
      )
      .subscribe((resp) => {
        if (resp["status"]) {
          let response = resp["otherInfo"];
          this.otherInfo = response.cronRunStatus;
          if (response.cronRunStatus == "Y") {
            this.buttoncronStaus = true;
            this.emitActionType("refresh");
          } else {
            this.buttoncronStaus = false;
            this.emitActionType("refresh");
          }
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }
}
