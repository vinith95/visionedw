import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ApiService, GlobalService, JwtService } from 'src/app/service';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import { GenericTreeComponent } from '../generic-tree/generic-tree.component';
import { MagnifierComponent } from '../magnifier/magnifier.component';

@Component({
  selector: 'app-filter-prompt',
  templateUrl: './filter-prompt.component.html',
  styleUrls: ['./filter-prompt.component.css']
})
export class FilterPromptComponent implements OnInit {

  @Input() filterRefCode;
  @Input() pageView;
  @Input() editData;
  @Input() filterFormValues;
  @Input() filterFormDescriptions;
  @Input() clicked;
  promptsList = [];
  filterVal: any;
  filterForm: FormGroup;
  overallPromptValues: any = {};
  treeId = {};
  magnifierId = {};
  promptValues = {};
  promptDescriptions = {};
  filterSubmitted = false;
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    noDataAvailablePlaceholderText: 'No data available',
    allowSearchFilter: true,
    maxHeight: 160
  };

  dashboardFilterSettings: IDropdownSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: true,
    maxHeight: 160
  };
  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();

  constructor(private apiService: ApiService,
    private jwtService: JwtService,
    private globalService: GlobalService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    let filterReq = {
      filterRefCode: this.filterRefCode
    }
    if (this.filterRefCode != '' && this.filterRefCode != null) {
      this.apiService.post(environment.getReportFilterList, filterReq).subscribe(filterResp => {
        if (!isNullOrUndefined(filterResp['response'])) {
          filterResp['response'].forEach(element => {
            if (element.filterType == 'DATE') {
              let dateRestrict = element.filterDateRestrict.split(',');
              let year = dateRestrict[0].split(':');
              let month = dateRestrict[1].split(':');
              let date = dateRestrict[2].split(':');
              let pastYear = year[0].slice(0, -1);
              let futureYear = year[1].slice(0, -1);
              let pastMonth = month[0].slice(0, -1);
              let futureMonth = month[1].slice(0, -1);
              let pastDate = date[0].slice(0, -1);
              let futureDate = date[1].slice(0, -1);
              let businessdates = JSON.parse(this.jwtService.get('businessDate'));
              let countrySplit = date[2] ? date[2].split("_") : [];
              if (countrySplit.length && countrySplit[1]) {
                businessdates.forEach(dateObj => {
                  if (dateObj.COUNTRY == countrySplit[1]) {
                    let isVBD = countrySplit.includes("VBD") ? true : false;
                    let isVBM = countrySplit.includes("VBM") ? true : false;
                    let isVBW = countrySplit.includes("VBW") ? true : false;
                    let isVBQ = countrySplit.includes("VBQ") ? true : false;
                    let today = new Date();
                    let todayYear = today.getFullYear();
                    let todayMonth = today.getMonth();
                    let todayDay = today.getDate();
                    if (isVBD || isVBW || isVBM || isVBQ) {
                      let maxDate: string = isVBD ? dateObj['VBD'].toString().replaceAll("-", " ") :
                        isVBW ? dateObj['VBW'].toString().replaceAll("-", " ") :
                          isVBM ? dateObj['VBM'].toString().replaceAll("-", " ") :
                            isVBQ ? dateObj['VBQ'].toString().replaceAll("-", " ") : '';
                      isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                      let vbdDate = new Date(maxDate);
                      let vbdYear = vbdDate.getFullYear();
                      let vbdMonth = vbdDate.getMonth();
                      let vbdDay = vbdDate.getDate();
                      element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                        new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                      element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                        new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                    }
                    else {
                      element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                      element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                    }
                  }
                });
              }
              else {
                let isVBD = date.includes("VBD") ? true : false;
                let isVBM = date.includes("VBM") ? true : false;
                let isVBW = date.includes("VBW") ? true : false;
                let isVBQ = date.includes("VBQ") ? true : false;
                let today = new Date();
                let todayYear = today.getFullYear();
                let todayMonth = today.getMonth();
                let todayDay = today.getDate();
                if (isVBD || isVBW || isVBM || isVBQ) {
                  // let dates: any = Object.values(businessdates);
                  // let maxDate = Math.max(...dates.map(e => new Date(e.toString().replaceAll("-", " "))));
                  let maxDate: string = isVBD ? businessdates[0]['VBD'].toString().replaceAll("-", " ") :
                    isVBW ? businessdates[0]['VBW'].toString().replaceAll("-", " ") :
                      isVBM ? businessdates[0]['VBM'].toString().replaceAll("-", " ") :
                        isVBQ ? businessdates[0]['VBQ'].toString().replaceAll("-", " ") : '';
                  isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                  let vbdDate = new Date(maxDate);
                  let vbdYear = vbdDate.getFullYear();
                  let vbdMonth = vbdDate.getMonth();
                  let vbdDay = vbdDate.getDate();
                  element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                    new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                  element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                    new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                }
                else {
                  element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                  element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                }
              }

            }
          });
          this.promptsList = filterResp['response'];
          this.filterVal = this.groupBy(filterResp['response'], 'filterRow');
          this.pageView == 'add' ? this.filterPrompt() : this.filterPromptEdit();
        }
        else {
          this.globalService.showToastr.error(filterResp['message'])
        }
      })
    }
  }

  filterPrompt() {
    let pushData = {}
    let tempDefault = [{ id: '', description: '' }];
    this.promptsList.forEach((element, objIndex) => {
      let tempArr = [];
      const isObjEmpty = Object.entries(element.filterSourceVal).length === 0 && element.filterSourceVal.constructor === Object;
      if (isObjEmpty) {
        this.overallPromptValues[element.filterSeq] = tempArr;
      } else {
        const data = element.filterSourceVal;
        Object.keys(data).forEach(ele => {
          tempArr.push({ id: ele, description: data[ele] });
        })
        this.overallPromptValues[element.filterSeq] = tempArr;
      }

      if (element.filterType == 'COMBO') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if(value == `promptvalue${element.filterSeq}`){
                tempDefault[0]['id'] = this.filterFormValues[value].replaceAll("'", "");
              }
              if(description == `promptdesc${element.filterSeq}`) {
                tempDefault[0]['description'] = this.filterFormDescriptions[description]
              }
            })
          })
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl([], Validators.required);
        }
      }
      if (element.filterType == 'DATE') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value) => {
            Object.keys(this.filterFormDescriptions).forEach((description) => {
              if (value == `promptvalue${element.filterSeq}`) {
                let dateVal = this.filterFormValues[value].replaceAll("'", "").split("-").join("/");
                if (element.filterDateFormat == "Mon-YYYY") {
                  dateVal = `01 ${dateVal}`
                }
                pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
              }
            })
          })
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }

      if (element.filterType == 'DATERANGE') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if (value == `promptvalue${element.filterSeq}`) {
                let dateVal = this.filterFormValues[value].replaceAll("'", "").split("-").join("/");
                if (element.filterDateFormat == "Mon-YYYY") {
                  dateVal = `01 ${dateVal}`
                }
                pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
              }
            })
          })
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }
      if (!['COMBO', 'DATE'].includes(element.filterType)) {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if(value == `promptvalue${element.filterSeq}`){
                pushData[`filter${element.filterSeq}Val`] = 
                new FormControl(this.filterFormValues[value].replaceAll("'", ""), Validators.required);
              }
            })
          })
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTM') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if(value == `promptvalue${element.filterSeq}`){
                this.magnifierId[`filter${element.filterSeq}Val`] = this.filterFormValues[value].replaceAll("'", "");
              }
              if(description == `promptdesc${element.filterSeq}`) {
                pushData[`filter${element.filterSeq}Val`] = 
                new FormControl(this.filterFormDescriptions[description], Validators.required);
              }
            })
          })
        }
        else {
          this.magnifierId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TREE') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if(value == `promptvalue${element.filterSeq}`){
                this.treeId[`filter${element.filterSeq}Val`] = this.filterFormValues[value].replaceAll("'", "");
              }
              if(description == `promptdesc${element.filterSeq}`) {
                pushData[`filter${element.filterSeq}Val`] = 
                new FormControl(this.filterFormDescriptions[description], Validators.required);
              }
            })
          })
        }
        else {
          this.treeId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXT') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if(value == `promptvalue${element.filterSeq}`){
                pushData[`filter${element.filterSeq}Val`] = 
                new FormControl(this.filterFormValues[value].replaceAll("'", ""), Validators.required);
              }
            })
          })
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTD') {
        if (Object.keys(this.filterFormValues).length) {
          Object.keys(this.filterFormValues).forEach((value)=>{
            Object.keys(this.filterFormDescriptions).forEach((description)=>{
              if(value == `promptvalue${element.filterSeq}`){
                pushData[`filter${element.filterSeq}Val`] = 
                new FormControl(this.filterFormValues[value].replaceAll("'", ""), Validators.required);
              }
            })
          })
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: "", disabled: true }, Validators.required);
        }
      }
    });
    this.filterForm = new FormGroup(pushData);
    setTimeout(() => {
      const mlHeight = document.querySelectorAll('.promptPopup .item2').forEach((ele) => {
        let aa = ele.closest("ng-multiselect-dropdown").nextElementSibling.getBoundingClientRect().top;
        if (aa < 310) {
          ele['style']['max-height'] = `calc(68vh - ${aa}px)`;
        }
        else {
          ele.closest("ng-multiselect-dropdown .dropdown-list")['style']['bottom'] = "30px";
        }
      });
    }, 500)
  }

  filterPromptEdit() {
    let pushData = {}
    let tempDefault = [{ id: '', description: '' }];
    this.promptsList.forEach((element, objIndex) => {
      Object.keys(this.editData).forEach((editKey) => {
        let tempArr = [];
        const isObjEmpty = Object.entries(element.filterSourceVal).length === 0 && element.filterSourceVal.constructor === Object;
        if (isObjEmpty) {
          this.overallPromptValues[element.filterSeq] = tempArr;
        } else {
          const data = element.filterSourceVal;
          Object.keys(data).forEach(ele => {
            tempArr.push({ id: ele, description: data[ele] });
          })
          this.overallPromptValues[element.filterSeq] = tempArr;
        }

        if (element.filterType == 'COMBO') {
          if (editKey == `promptdesc${element.filterSeq}`) {
            tempDefault[0]['description'] = this.editData[editKey];
          }
          if (editKey == `promptvalue${element.filterSeq}`) {
            tempDefault[0]['id'] = !isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "") : '';
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(tempDefault, Validators.required);
        }
        if (element.filterType == 'DATE') {
          if (editKey == `promptvalue${element.filterSeq}`) {
            let dateVal = !isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "").split("-").join("/") : '';
            if (element.filterDateFormat == "Mon-YYYY") {
              dateVal = `01 ${dateVal}`
            }
            pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          }
        }

        if (element.filterType == 'DATERANGE') {
          if (editKey == `promptvalue${element.filterSeq}`) {
            let dateVal = !isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "").split("-").join("/") : '';
            if (element.filterDateFormat == "Mon-YYYY") {
              dateVal = `01 ${dateVal}`
            }
            pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          }
        }
        if (!['COMBO', 'DATE'].includes(element.filterType)) {
          if (editKey == `promptvalue${element.filterSeq}`) {
            pushData[`filter${element.filterSeq}Val`] = new FormControl(!isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "") : '', Validators.required);
          }
        }
        if (element.filterType == 'TEXTM') {
          if (editKey == `promptvalue${element.filterSeq}`) {
            this.magnifierId[`filter${element.filterSeq}Val`] = !isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "") : '';
          }
          if (editKey == `promptdesc${element.filterSeq}`) {
            pushData[`filter${element.filterSeq}Val`] = new FormControl(!isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey] : '', Validators.required);
          }
        }
        if (element.filterType == 'TREE') {
          if (editKey == `promptvalue${element.filterSeq}`) {
            this.treeId[`filter${element.filterSeq}Val`] = !isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "") : '';
          }
          if (editKey == `promptdesc${element.filterSeq}`) {
            pushData[`filter${element.filterSeq}Val`] = new FormControl(!isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey] : '', Validators.required);
          }
        }
        if (element.filterType == 'TEXT') {
          if (editKey == `promptvalue${element.filterSeq}`) {
            pushData[`filter${element.filterSeq}Val`] = new FormControl(!isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "") : '', Validators.required);
          }
        }
        if (element.filterType == 'TEXTD') {
          if (editKey == `promptvalue${element.filterSeq}`) {
            pushData[`filter${element.filterSeq}Val`] = new FormControl(!isNullOrUndefined(this.editData[editKey]) ?
              this.editData[editKey].replaceAll("'", "") : '', Validators.required);
          }
        }
      })
    });
    this.filterForm = new FormGroup(pushData);
    if (this.pageView != 'edit' && this.pageView != 'copy') {
      this.filterForm.disable();
    }
    if(!this.clicked){
      let data = {
        type: 'getValue',
        promptValues: this.getPromptValues(),
        promptDescriptions: this.getpromptDescriptions()
      }
      this.filterData.emit(data);
    }
    setTimeout(() => {
      const mlHeight = document.querySelectorAll('.promptPopup .item2').forEach((ele) => {
        let aa = ele.closest("ng-multiselect-dropdown").nextElementSibling.getBoundingClientRect().top;
        if (aa < 310) {
          ele['style']['max-height'] = `calc(68vh - ${aa}px)`;
        }
        else {
          ele.closest("ng-multiselect-dropdown .dropdown-list")['style']['bottom'] = "30px";
        }
      });
    }, 500)
  }

  groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  getObjectValue(data) {
    return Object.values(data);
  }

  heightOfRow(data) {
    if (!isNullOrUndefined(data)) {
      if (data.length >= 5) {
        return {
          "height": 72 / data.length + "%"
        }
      }
      else {
        return
      }
    }
  }

  checkWidth(data) {
    let reqData = [];
    data.forEach(element => {
      reqData.push(element)
    });
    return {
      width: (100 / reqData.length) + '%'
    };
  }

  promptValuesReturn(index) {
    return `filter${index}Val`;
  }

  dropdownCheck(data, event, type?) {
    if (data.dependencyFlag == 'Y') {
      let magnifier = 0;
      let dropdownFlag = false;
      let dataValue;
      let dependentPrompt = data.dependentPrompt.split(",")
      dependentPrompt.forEach((element1, index1) => {
        this.promptsList.forEach((element, index) => {
          if (element.filterSeq == element1) {
            dataValue = this.filterForm.getRawValue()[`filter${element.filterSeq}Val`];
            if (element.filterType == 'DATE') {
              let val = this.dateReConversion(dataValue, element.filterDateFormat);
              dataValue = val;
              data[`filter${element1}Val`] = `'${val}'`;
            }
            if (dataValue.length) {
              if (Array.isArray(dataValue)) {
                if (!isNullOrUndefined(dataValue[0].id) && dataValue[0].id != '' && dataValue[0].id != 'ALL') {
                  data[`filter${element1}Val`] = this.arraySrtingCon(dataValue);
                  if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
                    dropdownFlag = true;
                  }
                }
                else {
                  magnifier++
                  this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
                }
              }
            }
            else {
              magnifier++
              Object.keys(this.filterVal).forEach(element => {
                this.filterVal[element].forEach(element1 => {
                  if (element1.filterSeq == data.filterSeq) {
                    this.overallPromptValues[data.filterSeq] = [];
                  }
                });
              });
              this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
            }
          }
        });
      });
      if (type == 'TEXTM' && magnifier == 0) {
        this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
      }
      if (type == 'TREE' && magnifier == 0) {
        this.openFilterTreePopup(data, event);
      }
      if (dropdownFlag && dataValue.length) {
        data['defaultValueId'] = '';
        this.apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
          let promptArr = []
          Object.keys(resp['response']).forEach((element, index1) => {
            promptArr.push(
              {
                id:element.split('@')[1],
                description: resp['response'][element]
              }
            )
          });
          Object.keys(this.filterVal).forEach(element => {
            this.filterVal[element].forEach(element1 => {
              if (element1.filterSeq == data.filterSeq) {
                this.overallPromptValues[data.filterSeq] = promptArr;
              }
            });
          });
        });
      }
    }
    else {
      if (data.filterType == "COMBO" || data.filterType == "TEXTD") {
        if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
          data['defaultValueId'] = '';
          this.apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
            let promptArr = []
            Object.keys(resp['response']).forEach((element, index1) => {
              promptArr.push(
                {
                  id: element.split('@')[1],
                  description: resp['response'][element]
                }
              )
            });
            Object.keys(this.filterVal).forEach(element => {
              this.filterVal[element].forEach(element1 => {
                if (element1.filterSeq == data.filterSeq) {
                  this.overallPromptValues[data.filterSeq] = promptArr;
                }
              });
            });
          });
        }
      }
      else {
        if (data.filterType == "TEXTM") {
          this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
        }
        if (data.filterType == 'TREE') {
          this.openFilterTreePopup(data, event);
        }
      }

    }
  }

  dateReConversion(date, format) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",][myDate.getMonth()];
      if (format == 'YYYY') {
        str = myDate.getFullYear();
      }
      else if (format == 'Mon-YYYY') {
        str = month + "-" + myDate.getFullYear();
      }
      else if (format == 'YYYYMM') {
        str = myDate.getFullYear() + ("0" + (myDate.getMonth() + 1)).slice(-2);
      }
      else {
        str = myDate.getDate() + "-" + month + "-" + myDate.getFullYear();
      }

      return str;
    }
    else {
      return "";
    }

  }

  arraySrtingCon(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`'${element["id"]}'`)
    });
    return arr.toString();
  }

  openCustomerMagnifierPopup(ind, event, title1) {
    event.srcElement.blur();
    event.preventDefault();
    let query;
    let tilte;
    Object.keys(this.filterVal).forEach(element => {
      this.filterVal[element].forEach(element1 => {
        if (element1.filterSeq == ind) {
          query = element1['filterSourceId'];
          tilte = element1['filterLabel'];
        }
      });
    });

    let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.promptsList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              promptM[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              promptM[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    promptM['query'] = query;
    const modelRef = this.modalService.open(MagnifierComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    modelRef.componentInstance.query = promptM;
    modelRef.componentInstance.title = title1;
    modelRef.componentInstance.filterData.subscribe((e) => {
      //let filteSeq = this.tablist[this.selectedReportId]["tabFilterValue"][ind]['filterSeq'];
      let str = `filter${ind}Val`;
      this.filterForm.patchValue({
        [str]: e.columnTen
      })
      this.magnifierId[str] = e.columnNine;
      //this.tablist[this.selectedReportId]["tabFilterSelectedValue"][ind] = e.columnNine;
      modelRef.close();
    });
  }
  openFilterTreePopup(data, event) {
    event.srcElement.blur();
    event.preventDefault();
    // let query;
    // let tilte;
    // Object.keys(this.filterVal).forEach(element => {
    //   this.filterVal[element].forEach(element1 => {
    //     if (element1.filterSeq == data.filterSeq) {
    //       query = element1['filterSourceId'];
    //       tilte = element1['filterLabel'];
    //     }
    //   });
    // });

    //let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.promptsList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              data[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              data[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    const modelRef = this.modalService.open(GenericTreeComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    document.body.classList.add('tree-popup');
    modelRef.componentInstance.title = data.filterLabel;
    modelRef.componentInstance.query = data;
    modelRef.componentInstance.selectedValue = this.treeId[`filter${data.filterSeq}Val`]
    modelRef.componentInstance.filterData.subscribe((e) => {
      let str = `filter${data.filterSeq}Val`;
      this.filterForm.patchValue({
        [str]: e.field2
      })
      this.treeId[str] = e.field1;
      document.body.classList.remove('tree-popup');
      modelRef.close();
    });
  }

  promptValueOption(seq) {
    return this.overallPromptValues[seq]
  }

  setEmptyDepent(data) {
    let filterPrompt = this.promptsList;
    filterPrompt.forEach(element => {
      if (element.dependencyFlag == 'Y') {
        let dependentPrompt = element.dependentPrompt.split(",");
        let filterSeq = data.filterSeq.toString();
        if (dependentPrompt.includes(filterSeq)) {
          let str = `filter${element.filterSeq}Val`;
          if (element.filterType == 'COMBO') {
            this.filterForm.patchValue({
              [str]: []
            })
          }
          else {
            this.filterForm.patchValue({
              [str]: ''
            })
          }

        }
      }
    });
  }

  arraySrtingConDes(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`${element["description"]}`)
    });
    return arr.toString();
  }


  getPromptValues(){
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    values.forEach(element => {
      this.promptsList.forEach(element1 => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if(element1["multiSelect"] == 'Y'){
              this.promptValues[`promptvalue${element1.filterSeq}`] =this.arraySrtingCon(rawVal[element]);
            }
            else{
              if (rawVal[element][0]) {
                this.promptValues[`promptvalue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
              }
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element],element1.filterDateFormat);
            this.promptValues[`promptvalue${element1.filterSeq}`] = `'${val}'`;
          }
          if (element1['filterType'] == 'TEXTD') {
            this.promptValues[`promptvalue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            this.promptValues[`promptvalue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TEXTM') {
            this.promptValues[`promptvalue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
            // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TREE') {
            this.promptValues[`promptvalue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
            // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TEXT') {
            this.promptValues[`promptvalue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
        }
      });
     });
    return this.promptValues;
  }
  getpromptDescriptions (){
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.promptsList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              this.promptDescriptions[`promptdesc${element1.filterSeq}`] = this.arraySrtingConDes(rawVal[element]);
            }
            else {
              if (rawVal[element][0]) {
                this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${rawVal[element][0]['description']}`;
              }
            }
          }
          if (element1['filterType'] == 'DATE') {
            if (element != '') {
              let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
              this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${val}`;
            }
            else {
              this.promptDescriptions[`promptdesc${element1.filterSeq}`] = ``;
            }

          }
          if (element1['filterType'] == 'TEXTD') {
            this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${rawVal[element]}`;
          }
          if (element1 == 'TEXT') {
            this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${rawVal[element]}`;
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${rawVal[element]}`;
          }
          if (element1['filterType'] == 'TEXTM') {
            this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${rawVal[element]}`;
          }
          if (element1['filterType'] == 'TREE') {
            this.promptDescriptions[`promptdesc${element1.filterSeq}`] = `${rawVal[element]}`;
          }
        }
      });
    });
    return this.promptDescriptions;
  }

  onSubmit(type) {
    let data = {
      type: type,
      promptValues: this.getPromptValues(),
      promptDescriptions: this.getpromptDescriptions()
    }
    this.filterData.emit(data);
  }
}
