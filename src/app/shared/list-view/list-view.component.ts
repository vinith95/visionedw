import { Component, OnInit, Input } from '@angular/core';
import { getLocaleNumberSymbol } from '@angular/common';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html'
})
export class ListViewComponent implements OnInit {


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  @Input() labels;
  @Input() values;


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  constructor() { }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngOnInit() {
  }
}
