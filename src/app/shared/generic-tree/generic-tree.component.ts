import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ApiService, GlobalService, JwtService } from 'src/app/service';
import { environment } from '../../../environments/environment';
import * as sampleData from '../../../assets/data/treeSample.json';
import { IActionMapping, ITreeOptions, TreeComponent, TreeNode } from '@circlon/angular-tree-component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-generic-tree',
  templateUrl: './generic-tree.component.html',
  styleUrls: ['./generic-tree.component.css']
})
export class GenericTreeComponent implements OnInit {

  @Input() title;
  @Input() query;
  @Input() selectedValue;
  @ViewChild('tree', { static: false }) public tree: any;
  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();
  treeSampleData:any=[];
  mode:any='expand';
  actionMapping: IActionMapping = {
    mouse: {
      click: (tree, node) => this.check(node, !node.data.checked)
    }
  };
  options:ITreeOptions = {
    displayField: 'field2',
    idField: 'field1',
    //actionMapping: this.actionMapping,
    // useCheckbox: true,
    // useVirtualScroll: true,
    // nodeHeight: 22,
    // useTriState: true,
    // nodeHeight: 10,
  }
  realParentIdVal: any;
  selectedNode: any;
  constructor(private globalService:GlobalService,private apiService:ApiService) { }

  ngOnInit() {
    this.apiService.post(environment.getTreeDataPromptList, this.query).subscribe((resp) => {
      this.treeSampleData=resp['response']
    });
  }
  onUpdateData(){
    this.tree.treeModel.expandAll();
    if(this.selectedValue !=''){
      const node = this.tree.treeModel.getNodeById(this.selectedValue);
      if (node) {
        node.toggleActivated();
      }
    }
  }
  ngAfterViewInit() {

  }
  btnSubCan=(type)=>{
    if(type == 'submit'){
      // this.fetchSelectedNode(this.tree.treeModel.nodes);
      if(!isNullOrUndefined(this.selectedNode)){
        // let idArray=this.selectedNode.map((data)=>{ return data.field1})
        // let stringArray=this.selectedNode.map((data)=>{ return data.field2})
        // let id=idArray.join(",")
        // let string=stringArray.join(",")
        // let data={id:id,string:string}
        // this.selectedNode=[];
        this.filterData.emit(this.selectedNode);
      }
      else{
        this.globalService.showToastr.error("Select atleast one value")
      }
    }
    else{
      this.closeModal()
    }
  }
  onEvent=(event)=>{
    if(event.eventName == "activate"){
      this.selectedNode=event.node.data
    }
    else{
      this.selectedNode=null
    }
  }
  fetchSelectedNode(node){
    node.forEach(element => {
     if(element.checked && !element.children){
       this.selectedNode.push(element)
     }
      if (element.children) {
        this.fetchSelectedNode(element.children);
      }
    });
  }

  closeModal = () => {
    this.filterData.emit('close');
    document.body.classList.remove('tree-popup');
  }
  expandCollapseFn=()=>{
    this.mode = this.mode == 'expand' ? 'collapse' :'expand';
    if(this.mode == 'expand'){
      this.tree.treeModel.expandAll();
    }
    else{
      this.tree.treeModel.collapseAll();
    }
  }
  check=(node, checked)=> {
    if(this.realParentIdVal==node.realParent.id){
      this.updateChildNodeCheckbox(node, checked);
      this.updateParentNodeCheckbox(node.realParent);
      this.realParentIdVal=node.realParent.id;
    }
    else{
      this.clearAllCheck();
      this.updateChildNodeCheckbox(node, checked);
      this.updateParentNodeCheckbox(node.realParent);
      this.realParentIdVal=node.realParent.id;
    }
  }
  clearAllCheck=()=>{
    this.updateChildNodeCheckboxClear(this.tree.treeModel.nodes, false);
  }
  updateChildNodeCheckboxClear=(node, checked)=> {
    node.forEach(element => {
      element.checked = checked;
      if (element.children) {
        this.updateChildNodeCheckboxClear(element.children, checked);
      }
    });
  }

  updateChildNodeCheckbox=(node, checked)=> {
    node.data.checked = checked;
    if (node.children) {
      node.children.forEach((child) => this.updateChildNodeCheckbox(child, checked));
    }
  }
  updateParentNodeCheckbox = (node)=> {
    if (!node) {
      return;
    }

    let allChildrenChecked = true;
    let noChildChecked = true;

    for (const child of node.children) {
      if (!child.data.checked) {
        allChildrenChecked = false;
      }
      if (child.data.checked) {
        noChildChecked = false;
      }
    }

    if (allChildrenChecked) {
      node.data.checked = true;
    } else if (noChildChecked) {
      node.data.checked = false;
    } else {
      node.data.checked = true;
    }
    this.updateParentNodeCheckbox(node.parent);
  }
}
