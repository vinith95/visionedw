import { Component, OnInit } from '@angular/core';
import { GlobalService, JwtService } from 'src/app/service';

@Component({
  selector: 'app-home-template',
  templateUrl: './home-template.component.html',
  styleUrls: ['./home-template.component.css']
})
export class HomeTemplateComponent implements OnInit {
  enableWidgets : boolean;
  
  constructor(private jwtService: JwtService,
    public globalService: GlobalService) { }

  ngOnInit() {
    this.enableWidgets = this.jwtService.get("enableWidgets") == 'Y' ? true : false;
    this.globalService.pageTitle = this.jwtService.get("enableWidgets") == 'Y' ? "": "Home";
    this.globalService.activeIcon = '';
  }

}
