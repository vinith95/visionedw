import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicySetupComponent } from './policy-setup.component';

describe('PolicySetupComponent', () => {
  let component: PolicySetupComponent;
  let fixture: ComponentFixture<PolicySetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolicySetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicySetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
