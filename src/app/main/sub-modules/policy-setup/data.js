export const tableDetail = {
  headers: [
    {
      name: "Policy Type Desc",
      referField: 'policyTypeDesc',
      filter: false,
      isFilterField: false,

    },
    {
      name: "Policy ID",
      referField: 'policyId',
      filter: false,
      isFilterField: true,

    },
    {
      name: "Policy Description",
      referField: 'policyDescription',
      filter: false,
      isFilterField: true
    },
    {
      name: "Policy Status",
      align: "left",
      referField: 'policyStatus',
      filter: false,
      isFilterField: true
    },
    {
      name: "Frequencey",
      referField: 'eventFrequenceyDesc',
      filter: false,
      isFilterField: true
    },
    {
      name: "Start Date",
      referField: 'startDate',
      filter: false,
      isFilterField: true
    },
    {
      name: "End Date",
      referField: 'endDate',
      filter: false,
      isFilterField: true
    },
    {
      name: "Next Run Date",
      referField: 'nextRunDate',
      filter: false,
      isFilterField: false
    },
    {
      name: "Status",
      referField: 'statusDesc',
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },
    {
      name: "Record Indicator",
      referField: 'recordIndicatorDesc',
      filter: false,
      isFilterField: true,

    },
    {
      name: 'Date Last Modified',
      referField: 'dateLastModified',
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },

  ],
  options: {
    highLightRowIndex:{
      index:null,
      currentPage:1
    },
    deleteButton: {
      enable: false,
      actionName: ''
    },
    bulkDeleteButton: {
      enable: false,
      actionName: ''
    },
    bulkApproveButton: {
      enable: false,
      actionName: ''
    },
    bulkRejectButton: {
      enable: false,
      actionName: ''
    },
    addButton: {
      enable: true,
      actionName: 'add'
    },
    filterButton: {
      enable: true,
      actionName: ''
    },
    refreshButton: {
      enable: true,
      actionName: 'refresh'
    },
    compactTableButton: {
      enable: false,
      actionName: ''
    },
    fullScreenButton: {
      enable: false,
      actionName: ''
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true
  },
  buttons: [
    // {
    //   name: 'edit',
    //   icon: 'fa fa-pencil',
    //   actionType: 'edit',
    //   validations: true,
    //   conditon: [{
    //     key: 'recordIndicator',
    //     value: [1, 2, 0]
    //   }]
    // },
    {
      name: 'edit',
      icon: 'edit',
      actionType: 'edit'
  },
    {
      name: 'view',
      icon: 'visibility',
      actionType: 'view'
    },
    {
      name: 'delete',
      icon: 'delete',
      actionType: 'delete',
      validations: true,
      conditon: {
        key: 'recordIndicator',
        value: [0]
      }
    },
    // {
    //   name: 'approve',
    //   icon: 'fa fa-check',
    //   actionType: 'approve',
    //   validations: true,
    //   conditon: {
    //     key: 'recordIndicator',
    //     value: [1, 2, 3]
    //   }
    // },
    // {
    //   name: 'reject',
    //   icon: 'fa fa-ban',
    //   actionType: 'reject',
    //   validations: true,
    //   conditon: {
    //     key: 'recordIndicator',
    //     value: [1, 2, 3]
    //   }
    // },
    // {
    //   name: 'Review',
    //   icon: 'fa fa-file',
    //   actionType: 'review',
    //   validations: true,
    //   conditon: {
    //     key: 'recordIndicator',
    //     value: [1]
    //   }
    // }
  ]
}

export const tableDetail2 =
  {
    "productName": "EDWADMIN",
    "connectionId": "DEVUSER",
    "schema": "DEVUSER",
    "makerName": "",
    "actionType": "Query",
    "description": "",
    "shortDesc": "",
    "internalStatus": 0,
    "selecteLanguage": "EN",
    "searchValue": "le_book",
    "searchKey": "ALL",
    "moduleTables": "",
    "moduleId": "",
    "moduleName": "",
    "moduleType": "",
    "screenName": "",
    "reportTitle": "",
    "applicationTheme": "",
    "maker": 0,
    "pdfGrwthPercent": 0,
    "reportOrientation": "",
    "objectName": "",
    "jsonDataForER": "",
    "jsonDataForRelations": "",
    "jsonDataForActualERData": "",
    "children": [],
    "childrenCols": [],
    "jsonColumnsDataToupdateER": ""
  }


