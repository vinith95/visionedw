import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { DatePipe } from "@angular/common";
import {
  Component, ElementRef, EventEmitter, OnInit,
  Output,
  ViewChild
} from "@angular/core";
import {
  FormControl, FormGroup,
  Validators
} from "@angular/forms";
import { NgbDateStruct, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { QueryValidationComponent } from 'src/app/shared/query-validation/query-validation.component';
import { ReviewComponent } from "src/app/shared/review/review.component";
import { TablesListComponent } from "src/app/shared/tables-list/tables-list.component";
import { isNullOrUndefined } from "util";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";
import { MagnifiernewComponent } from "./../../../shared/magnifiernew/magnifiernew/magnifiernew.component";
import { tableDetail } from "./data.js";

@Component({
  selector: "app-policy-setup",
  templateUrl: "./policy-setup.component.html",
  styleUrls: ["./policy-setup.component.css"],
})
export class PolicySetupComponent implements OnInit {
  @ViewChild("updateTemplate", { static: false })
  tableDataValues = new EventEmitter<string>();
  @Output() magnifiervalues = new EventEmitter<string>();
  profileSetupTemplate: ElementRef;
  ngxTableConfig: any = {
    api: "policy_Setup_getAllQueryResults",
    dataToDisplay: [],
    filterApi: "policy_Setup_getAllQueryResults",
    count: 10,
  };
  datePipe: DatePipe = new DatePipe("en-US");
  tableDetail;
  showFilter: boolean = false;
  policyTypeList;
  connectorTypeList;
  archievetypelist: any = [];
  optimizationtypelist: any = [];
  purgetypelist: any = [];
  eventFrequenceyList;
  recorIndicatorList;
  statusList;
  screenLangData;
  actionType = "add";
  policyformvalues: any;
  leftContainerHeight;
  resetModel: any = { key: "" };
  public policystatusvalue: boolean = true;
  public archiveVersionFlag: boolean = true;
  notshow: boolean = false;
  addBtn: boolean = false;
  editBtn: boolean = false;
  myDateValue: Date;
  // startDate: NgbDateStruct;
  min_enddate: NgbDateStruct;
  min_startdate: NgbDateStruct;
  isMasterSel: boolean;
  checkedCategoryList: any;
  maginerfiervalues: any = [];
  maginerfier_column_name: any = [];
  edwPolArchConfigList: any = [];
  edwPolOptConfigList: any = [];
  edwPolPurgeConfigList: any = [];
  addPolicy: boolean = true;
  cheknullorundefined: Boolean = false;
  pageView: any;
  editData: any = [];
  dummyArray: any = [];
  editIndex: any;
  lineList = [];
  policySetupForm: FormGroup;
  policySetupFormErrors = {
    policyType: "",
    policyId: "",
    policyDescription: "",
    policyStatus: "",
    eventFrequencey: "",
    startDate: "",
    endDate: "",
    objecttables: "",
    connectorId: "",
    //  nextRunDate: "",
  };
  startDate: any;
  endDate: any;
  startDates: any;
  endDates: any;
  err = '';
  objecttables: any;
  initPolicyManagementForm() {
    this.policySetupForm = new FormGroup({
      policyId: new FormControl("", [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/),
      ]),
      policyType: new FormControl("", Validators.required),
      connectorId: new FormControl("DEFAULT_EDW_DB", Validators.required),
      eventFrequencey: new FormControl("", Validators.required),
      policyDescription: new FormControl("", Validators.required),

      startDate: new FormControl("", [Validators.required]),
      endDate: new FormControl("err"),
      policyStatus: new FormControl(true),
      objecttables: new FormControl(""),
    });
  }
  constructor(
    private globalService: GlobalService,
    private _apiService: ApiService,
    private translate: TranslateService,
    private modalService: NgbModal
  ) {
    this.translate.setDefaultLang("en");
    this.translate.use("en");
    this.translate.currentLoader
      .getTranslation(this.translate.currentLang)
      .subscribe((data) => {
        this.screenLangData = data["policySetup"];
      });
  }

  ngOnInit(): void {
    this.archiveVersionFlag = true;
    this.policystatusvalue = true;
    this.initPolicyManagementForm();
    this.getPageLoadValues();
    this.tableDetail = tableDetail;
    this.addPolicy = false;
    this.min_startdate = {
      year: new Date().getFullYear(),
      month: new Date().getMonth(),
      day: new Date().getDate(),
    };
    this.dropdown();
    // this.changeDate();
  }

  dropdown() {
    this.optimizationtypelist = [
      { optimizeId: "ANALYZE", description: "Analyze" },
      { optimizeId: "TREBUILD", description: "Table Rebuild" },
      { optimizeId: "IREBUILD", description: "Index Rebuild" },
    ];

    this.purgetypelist = [
      { purgeId: "TRUNCATE", description: "Truncate Table" },
      { purgeId: "DROP", description: "Drop Table" },
      { purgeId: "PURGEDROP", description: "Drop Table With Purge" },
      { purgeId: "DELETE", description: "Delete Table Data" },
    ];

    this.archievetypelist = [
      { archieveId: "F", description: "FullTable" },
      { archieveId: "P", description: "Periodic" },
    ];
  }
  clearvalues() {
    this.edwPolArchConfigList = [];
    this.edwPolOptConfigList = [];
    this.edwPolPurgeConfigList = [];
    this.maginerfiervalues = [];
    this.checkedCategoryList = [];
    this.maginerfier_column_name = [];
    this.isMasterSel = false;
    this.initPolicyManagementForm();
  }

  openModal() {
    this.modalService.open(this.profileSetupTemplate, {
      size: "lg",
      windowClass: "width-popup",
      backdrop: "static",
    });
  }

  checkUncheckAll() {
    for (var i = 0; i < this.maginerfiervalues.length; i++) {
      this.maginerfiervalues[i].isSelected = this.isMasterSel;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.isMasterSel = this.maginerfiervalues.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }
  getCheckedItemList() {
    this.checkedCategoryList = [];
    for (var i = 0; i < this.maginerfiervalues.length; i++) {
      if (this.maginerfiervalues[i].isSelected)
        this.checkedCategoryList.push(this.maginerfiervalues[i]);
    }
    this.checkedCategoryList = JSON.stringify(this.checkedCategoryList);
  }
  getPageLoadValues = () => {
    this._apiService
      .get(environment.policy_Setup_PageLoadValues)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.policyTypeList = resp["response"][0];
          this.eventFrequenceyList = resp["response"][1];
          this.statusList = resp["response"][2];
          this.recorIndicatorList = resp["response"][3];
          this.policyformvalues = resp["response"][4];
          this.connectorTypeList = resp["response"][6];
          this.ngxTableConfig.cronStatus = resp["response"];
          this.showFilter = true;
        }
      });
  };

  swap(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      this.maginerfiervalues,
      event.previousIndex,
      event.currentIndex
    );
  }

  tableAction(e) {
    this.lineList = e.lineList;
    this.pageView = e.type;
    if (e.type === "edit" || e.type === "view") {
      this.editIndex = e.index;
      this.editForm(e);
      this.addBtn = false;
      this.editBtn = true;
      this.addPolicy = true;
      this.showFilter = false;
    } else if (e.type === "add") {
      this.pageView = e.type;
      this.createNewUserPolicy();
      this.addBtn = true;
      this.editBtn = false;
      this.addPolicy = true;
      this.showFilter = false;
    }
    if (e.type == "approve" || e.type == "reject" || e.type == "delete") {
      this.openGenericPopup(e);
    }
    if (e.type == "review") {
      this.reviewPopup(e.data);
    } else if (e.type == "pagination" || e.type == "refresh") {
      this.resetModel = Object.assign({}, { key: "reload" });
      this.ngxTableConfig["filterData"] = e.filter;
    }
  }

  reviewPopup(data) {
    this._apiService
      .post(environment.policy_Setup_ReviewEdwPolConfig, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          const modelRef = this.modalService.open(ReviewComponent, {
            size: "lg",
            backdrop: "static",
            windowClass: "review-popup",
          });
          modelRef.componentInstance.response = resp["response"];
          modelRef.componentInstance.closePopup.subscribe(() => {
            modelRef.close();
          });
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }

  openGenericPopup(e) {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    let prompt = "";
    if (e.type == "approve") {
      prompt = "Are you sure, you want to approve the record?";
    }
    if (e.type == "reject") {
      prompt = "Are you sure, you want to reject the record?";
    }
    if (e.type == "delete") {
      prompt = "Are you sure, you want to delete the record?";
    }
    if (e.type == "review") {
      // prompt = "Are you sure, you want to review the record?";
    }
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = prompt;
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.approveOrReject(e);
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }

  approveOrReject(e) {
    let data = {
      policyId: e.data.policyId,
      policyType: e.data.policyType,
    };
    let apiUrl =
      e.type == "approve"
        ? environment.policy_Setup_ApproveEdwPolConfig
        : e.type == "reject"
          ? environment.policy_Setup_RejectEdwPolConfig
          : e.type == "delete"
            ? environment.policy_Setup_DeleteEdwPolConfig
            : e.type == "review"
              ? environment.policy_Setup_ReviewEdwPolConfig
              : "";
    this._apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp["status"]) {
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
      this.resetModel = Object.assign({}, { key: "reload" });
      this.backToHmpage();
    });
  }

  backToHmpage() {
    this.showFilter = true;
    this.addPolicy = false;
    // this.resetForm();
    this.err = ''
  }

  targetTableChange() {
    this.isMasterSel = false;
    this.maginerfiervalues = [];
    this.edwPolArchConfigList = [];
    this.edwPolPurgeConfigList = [];
    this.edwPolOptConfigList = [];
    this.dummyArray = [];
  }

  onStartDateSelect(value: NgbDateStruct[]) {
    if (value) {
      this.min_enddate = {
        year: value["year"],
        month: value["month"],
        day: value["day"],
      };
    }
  }

  createNewUserPolicy() {
    this.screenNamePatch();
    this.resetForm();
    this.clearvalues();
  }


  resetForm() {
    this.actionType = "add";
  }
  screenNamePatch() {
    this.policySetupForm.patchValue({
      policyType: "",
    });
  }


  selectedColumn: any = [];
  whereCdt(value) {
    let data = {
      connectorId: this.policySetupForm.get('connectorId').value,
      makerName: value.columnNine,
    };
    const modelRef = this.modalService.open(QueryValidationComponent, {
      size: "xl",
      backdrop: false,
      // windowClass: "filter-popup",
    });

    modelRef.componentInstance.bodyText = value;
    modelRef.componentInstance.dataPassingValue = data;
    modelRef.componentInstance.emitData.subscribe((resp) => {
      if (resp["type"] == "submit" || resp["type"] == "clear") {
      }
      else if (resp["type"] == "close") {
        modelRef.close();
      }
      modelRef.close();
    });
  }

  tableindex(value) {
    const modelRef = this.modalService.open(TablesListComponent, {
      size: "lg",
      backdrop: "static",
    });

    modelRef.componentInstance.tableidList = value;
    modelRef.componentInstance.filterData.subscribe((e) => {
      if (e["type"] != "close") {
        // let tempArr = e.value
        //   .map((item) => {
        //     return item.name;
        //   })
        //   .join(",");
        // let patchValue = e.value.map((item) => item.name).join(";");
      }
      modelRef.close();
    });
  }

  openMagniferData(title) {
    if (
      !isNullOrUndefined(this.policySetupForm.value.eventFrequencey) &&
      this.policySetupForm.value.eventFrequencey != ""
    ) {
      let query;
      if (this.policySetupForm.get('connectorId').value
        != "DEFAULT_EDW_DB") {
        query = "ORATABLES";
      }
      else {
        query = "ERTABLES";
      }
      this.maginerfiervalues.connectorId = this.policySetupForm.get('connectorId').value
      const modelRef = this.modalService.open(MagnifiernewComponent, {
        size: "lg",
        backdrop: "static",
      });
      modelRef.componentInstance.query = query;
      modelRef.componentInstance.title = title;
      modelRef.componentInstance.inputDetails = this.maginerfiervalues;
      modelRef.componentInstance.filterData.subscribe((data) => {
        if (data != "close") {
          this.maginerfier_column_name = [];
          this.maginerfiervalues = [];
          let valuesbinding = "";
          this.maginerfiervalues = data;

          if (
            (!isNullOrUndefined(this.policySetupForm.get("policyType").value.toUpperCase() == "ARCHIEVE") ||
              (!isNullOrUndefined(this.editData.policyType.toUpperCase() == "ARCHIEVE")))
          ) {
            this.maginerfiervalues.forEach((x) => {
              if (x) {
                if (this.policySetupForm.value.eventFrequencey == "DAILY") {
                  x["archiveTableName"] = x.columnNine + "_#DDMMRRRR#";
                  // console.log( x["archiveTableName"])
                } else if (
                  this.policySetupForm.value.eventFrequencey == "WEEKLY"
                ) {
                  x["archiveTableName"] = x.columnNine + "_#DDMMRRRR#_1";
                } else if (
                  this.policySetupForm.value.eventFrequencey == "MONTHLY"
                ) {
                  x["archiveTableName"] = x.columnNine + "_#MMRRRR#";
                } else if (
                  this.policySetupForm.value.eventFrequencey == "HOURLY"
                ) {
                  x["archiveTableName"] = x.columnNine + "_#DDMMRRRRHH#";
                } else {
                  x["archiveTableName"] = x.columnNine;
                }
                if (x.archiveType == "Periodic") {
                  x["showwherecondition"] = true;
                } else {
                  x["showwherecondition"] = false;
                  x.whereConditions = "";
                }
                x.policyArchiveStatus = true;
              }

            });
          }

          if (
            (!isNullOrUndefined(this.policySetupForm.get("policyType").value.toUpperCase() == "PURGE") ||
              (!isNullOrUndefined(this.editData.policyType.toUpperCase() == "PURGE")))
          ) {
            this.maginerfiervalues.forEach((x) => {
              if (x) {
                if (this.policySetupForm.value.eventFrequencey == "DAILY") {
                  x["purgeTableName"] = x.columnNine + "_#DDMMRRRR#";
                } else if (
                  this.policySetupForm.value.eventFrequencey == "WEEKLY"
                ) {
                  x["purgeTableName"] = x.columnNine + "_#DDMMRRRR#_1";
                } else if (
                  this.policySetupForm.value.eventFrequencey == "MONTHLY"
                ) {
                  x["purgeTableName"] = x.columnNine + "_#MMRRRR#";
                } else if (
                  this.policySetupForm.value.eventFrequencey == "HOURLY"
                ) {
                  x["purgeTableName"] = x.columnNine + "_#DDMMRRRRHH#";
                } else {
                  x["purgeTableName"] = x.columnNine;
                }
                if (x.purgeType == "DELETE") {
                  x.showwherecondition = true;
                }
                else {
                  x.showwherecondition = false;
                }
                x.policyPurgeStatus = true;

              }
            });
          }


          if (
            (!isNullOrUndefined(this.policySetupForm.get("policyType").value.toUpperCase() == "OPTIMIZE") ||
              (!isNullOrUndefined(this.editData.policyType.toUpperCase() == "OPTIMIZE")))
          ) {
            this.maginerfiervalues.forEach((x) => {
              if (x) {
                x.policyOptimizeStatus = true;
              }
            });
          }

          this.policySetupForm.patchValue({
            ["objecttables"]: valuesbinding,
          });
        }
        modelRef.close();
      });
    } else {
      this.globalService.showToastr.warning("Please Select Event Frequency");
    }
  }

  backGroundFlag(recordIndicator) {
    if (!recordIndicator) {
      return {
        background: "#448847",
      };
    } else {
      return {
        background: "#ffa828",
      };
    }
  }

  editForm(e) {
    let data = {
      policyId: e.data.policyId,
      policyType: e.data.policyType,
    };
    this._apiService
      .post(environment.policy_Setup_GetQueryDetails, data)
      .subscribe((resp) => {

        if (resp["status"]) {
          this.editData = resp["otherInfo"];
          this.policySetupForm.get("policyId").disable();
          this.policySetupForm.get("policyType").disable();
          this.policySetupForm.get("connectorId").disable();
          let endDate;
          let startdate;
          if (!isNullOrUndefined(this.editData.startDate)) {
            startdate = new Date(this.editData.startDate);
          }

          if (
            !isNullOrUndefined(this.editData.endDate) &&
            this.editData.endDate != ""
          ) {
            endDate = new Date(this.editData.endDate);
          }

          let valuescheckbox;
          if (this.editData.policyStatus.toUpperCase() == "E") {
            valuescheckbox = true;
          } else {
            valuescheckbox = false;
          }
          this.policySetupForm.patchValue({
            policyId: this.editData.policyId,
            policyType: this.editData.policyType,
            eventFrequencey: this.editData.eventFrequencey,
            policyDescription: this.editData.policyDescription,
            policyStatus: valuescheckbox,
            startDate: startdate,
            endDate: endDate,
            connectorId: this.editData.connectorId,

            objecttables: this.editData.objecttables,
          });

          this.maginerfiervalues = [];
          this.edwPolArchConfigList = [];
          this.edwPolPurgeConfigList = [];
          this.edwPolOptConfigList = [];
          this.dummyArray = [];

          if (this.editData.policyType.toUpperCase() == "PURGE") {
            this.dummyArray.push(this.editData.edwPolPurgeConfigList);
            this.maginerfiervalues = this.dummyArray[0];
            if (this.magnifiervalues) {
              this.maginerfiervalues.forEach((x, index) => {
                if (x) {
                  x["policyPurgeOrder"] = index + 1;
                  x["policyPurgeSequence"] = index + 1;
                  x["policyId"] = this.policySetupForm.value.policyId;
                  x["policyType"] = this.policySetupForm.value.policyType;
                  x["columnNine"] = x.tableName;
                  x["checked"] = true;
                  x["newRecord "] = false;
                  if (x.policyPurgeStatus == "E") {
                    x["policyPurgeStatus"] = true;
                  } else {
                    x["policyPurgeStatus"] = false;
                  }
                  if (x.purgeType == "DELETE") {
                    x.showwherecondition = true;
                  }
                  else {
                    x.showwherecondition = false;
                  }
                }
              });
            }
          }
          if (this.editData.policyType.toUpperCase() == "ARCHIEVE") {
            this.dummyArray.push(this.editData.edwPolArchConfigList);
            this.maginerfiervalues = this.dummyArray[0];
            if (this.maginerfiervalues) {
              this.maginerfiervalues.forEach((x, index) => {
                if (x) {
                  x["policyArchiveSequence"] = index + 1;
                  x["policyArchiveOrder"] = index + 1;
                  x["policyId"] = this.editData.policyId;
                  x["policyType"] = this.editData.policyType;
                  x["columnNine"] = x.tableName;
                  x.archiveVersionFlag = "VY";
                  //  x.nextRunDate = "";
                  x["checked"] = true;
                  x["newRecord "] = false;
                  if (x.policyArchiveStatus == "E") {
                    x["policyArchiveStatus"] = true;
                  } else {
                    x["policyArchiveStatus"] = false;
                  }
                  if (x.archiveType == "F") {
                    x["archiveType"] = "FullTable";
                    x["showwherecondition"] = false;
                    x.whereConditions = "";
                  } else {
                    x["archiveType"] = "Periodic";
                    x["showwherecondition"] = true;
                  }
                }
              });
            }
          }
          if (this.editData.policyType.toUpperCase() == "OPTIMIZE") {
            this.dummyArray.push(this.editData.edwPolOptConfigList);
            this.maginerfiervalues = this.dummyArray[0];
            if (this.magnifiervalues) {
              this.maginerfiervalues.forEach((x, index) => {
                if (x) {
                  x["policyOptimizeOrder"] = index + 1;
                  x["policyOptimizeSequence"] = index + 1;
                  x["policyId"] = this.policySetupForm.value.policyId;
                  x["policyType"] = this.policySetupForm.value.policyType;
                  x["columnNine"] = x.tableName;
                  x["optimizeType"] = x.optimizeType;
                  x["checked"] = true;
                  x["newRecord "] = false;
                  if (x.policyOptimizeStatus == "E") {
                    x["policyOptimizeStatus"] = true;
                  } else {
                    x["policyOptimizeStatus"] = false;
                  }
                }
              });
            }

          }

          this.showFilter = false;
          this.addPolicy = true;
        }
      });
  }

  archieveTypeListOnChange(event) {
    if (event == "P") {
      this.maginerfiervalues.forEach((element) => {
        if ((element.archiveType == "P") || (element.archiveType == "Periodic")) {
          element["showwherecondition"] = true;
        } else {
          element["showwherecondition"] = false;
          element.whereConditions = "";
        }
      });
    }
  }
  purgeTypeChange(event) {
    if (event == "DELETE") {
      this.maginerfiervalues.forEach((element) => {
        if (element.purgeType == "DELETE") {
          element["showwherecondition"] = true;
        } else {
          element["showwherecondition"] = false;

        }
      });
    }
  }
  saveNewPolicySetup(type) {
    this.edwPolArchConfigList = [];
    this.edwPolPurgeConfigList = [];
    this.edwPolOptConfigList = [];
    if (this.maginerfiervalues) {
      this.maginerfiervalues.forEach((x, index) => {
        if (x) {
          x["Sequence"] = index + 1;
          x["Order"] = index + 1;
        }
      });
    }
    if (this.maginerfiervalues) {
      this.maginerfiervalues.forEach((element) => {
        if (element.isSelected) {
          if (!isNullOrUndefined(element.archiveType) && element.archiveType.toUpperCase() == "P" && element.whereConditions == "" || null) {
            this.globalService.showToastr.warning("Please Add Filter Conditions");
            return false;
          }
          else if (!isNullOrUndefined(element.purgeType) && element.purgeType.toUpperCase() == "DELETE" && element.whereConditions == "" || null) {
            this.globalService.showToastr.warning("Please Add Filter Conditions");
            return false;
          }
          else {
            this.edwPolArchConfigList.push(element);
            this.edwPolPurgeConfigList.push(element);
            this.edwPolOptConfigList.push(element);
          }
        }
      });
    }

    var rawValue = this.policySetupForm.getRawValue();
    //    this.startDates = this.globalService.dateConversion(rawValue.startDate, "dd-MM-yyyy")
    //     this.endDates= this.globalService.dateConversion(rawValue.endDate, "dd-MM-yyyy"),
    //     console.log(rawValue)
    //    console.log(this.endDates)
    // if (new Date(this.startDates) > new Date (this.endDates)) {
    //   this.err = "End Date should be greater than Start Date"      
    // }
    // else if (new Date(this.startDates) < new Date (this.endDates))
    // {
    //   this.err =''
    // }
    // else {
    //   // this.policySetupForm.controls[endDate].setErrors(null);
    // }

    // console.log(this.err)
    if (this.policySetupForm.valid && this.edwPolArchConfigList.length != 0) {
      if (this.policySetupForm.get("policyType").value.toUpperCase() == "ARCHIEVE") {
        this.edwPolArchConfigList.forEach((x) => {
          if (x.isSelected) {
            x["policyArchiveSequence"] = x.Sequence;
            x["policyArchiveOrder"] = x.Order;
            x["policyId"] = rawValue.policyId;
            x["policyType"] = rawValue.policyType;
            x.archiveVersionFlag = "VY";
            if (x.policyArchiveStatus) {
              x["policyArchiveStatus"] = "E";
            } else {
              x["policyArchiveStatus"] = "D";
            }

            if (x.archiveType == null || undefined) {
              x["archiveType"] = "FullTable";
            }
            x["tableName"] = x.columnNine;
          }
        });

        const pass_list = {
          policyId: rawValue.policyId,
          policyType: rawValue.policyType,
          eventFrequencey: rawValue.eventFrequencey,
          policyDescription: rawValue.policyDescription,
          startDate: this.datePipe.transform(rawValue.startDate, "dd-MM-yyyy"),
          endDate: this.datePipe.transform(rawValue.endDate, "dd-MM-yyyy"),
          policyStatus: rawValue.policyStatus ? "E" : "D",
          edwPolArchConfigList: this.edwPolArchConfigList,
          connectorId: rawValue.connectorId,
        };
        //console.log(pass_list.endDate)

        if (
          !isNullOrUndefined(pass_list.endDate) &&
          !isNullOrUndefined(pass_list.startDate)
        ) {
          if (pass_list.endDate < pass_list.startDate) {
            this.globalService.showToastr.warning(
              "End Date should be lesser than Start Date"
            );

          }
          else {
            this.saveapiHitting(pass_list, type);
          }
        } else {
          this.saveapiHitting(pass_list, type);
        }
      } else if (this.policySetupForm.get("policyType").value.toUpperCase() == "PURGE") {
        this.edwPolPurgeConfigList.forEach((x) => {
          if (x.isSelected) {
            x["policyPurgeOrder"] = x.Sequence;
            x["policyPurgeSequence"] = x.Order;
            x["policyId"] = rawValue.policyId;
            x["policyType"] = rawValue.policyType;
            x["tableName"] = x.columnNine;
            x.archiveVersionFlag = "VY";
            if (x.policyPurgeStatus) {
              x["policyPurgeStatus"] = "E";
            } else {
              x["policyPurgeStatus"] = "D";
            }
            if (x.purgeType == null || undefined) {
              x["purgeType"] = "TRUNCATE";
            }
            x["tableName"] = x.columnNine;
          }
        });

        const pass_list = {
          policyId: rawValue.policyId,
          policyType: rawValue.policyType,
          eventFrequencey: rawValue.eventFrequencey,
          policyDescription: rawValue.policyDescription,
          startDate: this.datePipe.transform(rawValue.startDate, "dd-MM-yyyy"),
          endDate: this.datePipe.transform(rawValue.endDate, "dd-MM-yyyy"),
          policyStatus: rawValue.policyStatus ? "E" : "D",
          edwPolPurgeConfigList: this.edwPolPurgeConfigList,
          connectorId: rawValue.connectorId,
        };
        if (
          !isNullOrUndefined(pass_list.endDate) &&
          !isNullOrUndefined(pass_list.startDate)
        ) {
          if (pass_list.endDate < pass_list.startDate) {
            this.globalService.showToastr.warning(
              "End Date should be lesser than Start Date"
            );
          }
          else {
            this.saveapiHitting(pass_list, type);
          }
        } else {
          this.saveapiHitting(pass_list, type);
        }
      } else if (this.policySetupForm.get("policyType").value.toUpperCase() == "OPTIMIZE") {
        this.edwPolOptConfigList.forEach((x) => {
          if (x.isSelected) {
            x["policyOptimizeOrder"] = x.Sequence;
            x["policyOptimizeSequence"] = x.Order;
            x["tableName"] = x.columnNine;
            x["policyId"] = rawValue.policyId;
            x["policyType"] = rawValue.policyType;
            x["tableIndexListings"] = "index";
            if (x.policyOptimizeStatus) {
              x["policyOptimizeStatus"] = "E";
            } else {
              x["policyOptimizeStatus"] = "D";
            }

            if (x.optimizeType == null || undefined) {
              x["optimizeType"] = "ANALYZE";
            }

            x["tableName"] = x.columnNine;
          }
        });

        const pass_list_optimize = {
          policyId: rawValue.policyId,
          policyType: rawValue.policyType,
          eventFrequencey: rawValue.eventFrequencey,
          policyDescription: rawValue.policyDescription,
          startDate: this.datePipe.transform(rawValue.startDate, "dd-MM-yyyy"),
          endDate: this.datePipe.transform(rawValue.endDate, "dd-MM-yyyy"),

          policyStatus: rawValue.policyStatus ? "E" : "D",
          edwPolOptConfigList: this.edwPolOptConfigList,
          connectorId: rawValue.connectorId,
        };
        if (
          !isNullOrUndefined(pass_list_optimize.endDate) &&
          !isNullOrUndefined(pass_list_optimize.startDate)
        ) {
          if (pass_list_optimize.endDate < pass_list_optimize.startDate) {
            this.globalService.showToastr.warning(
              "End Date should be lesser than Start Date"
            );
          }
          else {
            this.saveapiHitting(pass_list_optimize, type);
          }
        } else {
          this.saveapiHitting(pass_list_optimize, type);
        }
      }
    } else {
      if (this.policySetupForm.valid) {
        let isAllSelected;
        if (this.maginerfiervalues) {
          isAllSelected = this.maginerfiervalues.find(e => e.isSelected === true);
        }
        if ((this.edwPolArchConfigList.length == 0 || []) && !isAllSelected && !isNullOrUndefined(isAllSelected)) {
          this.globalService.showToastr.warning("Please select the checkbox");
        }
        else if (this.edwPolArchConfigList.length == 0 || []) {
          this.globalService.showToastr.warning("Please select the Obj/Tables");
        }
      }

      this.policySetupForm.get("policyId").markAsTouched();
      this.policySetupForm.get("policyType").markAsTouched();
      this.policySetupForm.get("policyDescription").markAsTouched();
      this.policySetupForm.get("startDate").markAsTouched();
      this.policySetupForm.get("eventFrequencey").markAsTouched();
      return;
    }
  }

  changeDate() {
    var rawValue = this.policySetupForm.getRawValue();
    this.startDates = this.globalService.dateConversion(rawValue.startDate, "dd-MM-yyyy")
    this.endDates = this.globalService.dateConversion(rawValue.endDate, "dd-MM-yyyy")
    if (new Date(this.startDates) > new Date(this.endDates)) {
      this.err = "End Date should be greater than Start Date"
    }
    else if (new Date(this.startDates) < new Date(this.endDates)) {
      this.err = ''
    }
  }

  saveapiHitting(listvaluesofpolicy, type) {
    let apiUrl =
      type == "modify"
        ? environment.policy_Setup_ModifyEdwPolConfig
        : environment.policy_Setup_AddEdwPolConfig;
    this._apiService.post(apiUrl, listvaluesofpolicy).subscribe((resp) => {
      if (resp["status"] == 1) {
        if (type == "save") {
          this.showFilter = true;
          this.addPolicy = false;
          this.clearvalues();
          this.modalService.dismissAll();
          this.resetModel = Object.assign({}, { key: "reload" });
        }
        else {
          let e = {
            type: "edit",
            data: this.editData,
            index: this.editIndex
          }
          this.tableAction(e)
        }
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.showFilter = true;
        this.addPolicy = false;
        this.globalService.showToastr.success(resp["message"]);
        this.clearvalues();
        this.modalService.dismissAll();
        this.resetModel = Object.assign({}, { key: "reload" });
      }
    });
  }

  BacktoPolicySetupmen() {
    this.clearvalues();
    this.modalService.dismissAll();
  }
  modalClose() {
    this.magnifiervalues.emit(this.maginerfiervalues);
    this.modalService.dismissAll();
  }


  disableButton(name) {
    if (name == 'first') {
      return !this.editIndex ? true : false;
    }
    if (name == 'last') {
      return this.lineList.length == 1 ? true : this.editIndex == (this.lineList.length - 1) ? true : false;
    }
  }



  navigateFn(type) {
    if (type == 'next') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'previous') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'last') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'first') {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
  }


  onSubmit(type) {
    if (type == 'next' || type == 'previous' || type == 'last' || type == 'first') {
      this.navigateFn(type);
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    }, 500);
  }
}
