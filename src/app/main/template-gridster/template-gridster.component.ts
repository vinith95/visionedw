import { Component, ElementRef, HostListener, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GridsterConfig, GridsterItem, GridsterComponent, GridsterItemComponentInterface } from 'angular-gridster2';
import { templates, GridsterConfigList, config, widgetDetails, tileDetails } from 'src/assets/js/gridster-conf'
import { BehaviorSubject,interval, Subscription } from 'rxjs';
import { CommonService } from 'src/app/service/common.service';
import { isNull, isNullOrUndefined, isString, isUndefined } from 'util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GenericPopupComponent } from 'src/app/shared/generic-popup/generic-popup.component';
import { ApiService, GlobalService, JwtService } from 'src/app/service';
import { TreeModel } from '@circlon/angular-tree-component';
import { environment } from 'src/environments/environment';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MagnifierComponent } from 'src/app/shared/magnifier/magnifier.component';
import { GenericTreeComponent } from 'src/app/shared/generic-tree/generic-tree.component';
import { XmltoJsonService } from 'src/app/service/xmlto-json.service';
import { loaderSvg } from '../home/dashboard/loader';
import { drillDownRequest } from '../reports/report.const';
import { element } from 'protractor';
import { report } from 'process';
import { widgetIcons } from 'src/assets/js/widgetIcons'
import { Router } from '@angular/router';
import FusionCharts from 'fusioncharts';
import { RootService } from 'src/app/service/root.service';
import { excelSvg } from 'src/assets/svg/xls';
import { pdfSvg } from 'src/assets/svg/pdf';
import { AngularResizableDirective } from 'angular2-draggable';
import { blue } from 'src/assets/svg/bluetheme';
import { orange } from 'src/assets/svg/orangetheme';
import { green } from 'src/assets/svg/greentheme';
import { pink } from 'src/assets/svg/pinktheme';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { GroupingService } from 'src/app/service/grouping-component/grouping.service';

@Component({
  selector: 'app-template-gridster',
  templateUrl: './template-gridster.component.html',
  styleUrls: ['./template-gridster.component.css']
})
export class TemplateGridsterComponent implements OnInit, OnDestroy {

  selectedTab: any = 'temp';
  selectedTempId: any
  tabs: any = [
    { id: 'temp', name: 'Predefined Template', select: true, class: "col-md-12 col-lg-12 col-xl-12 col-sm-12 col-xs-12" },
    // { id: 'cus', name: 'Custom Template', select: false, class: "col-md-4 col-lg-4 col-xl-4 col-sm-12 col-xs-12" },
    // { id: 'exist', name: 'Existing Template', select: false, class: "col-md-4 col-lg-4 col-xl-4 col-sm-12 col-xs-12" },

  ];
  selectedNode
  showGrider: boolean = true;
  templateView: any = false;
  widgetView: any = false
  rebindView: any = false;
  configView: any = false;
  sideBarTog: boolean = false;
  themeView: boolean = false
  templateDetails: any = {};
  dashboardObj: any = {
    name: '',
    description: '',
    tabBol: '0',
    type: '',
    tabArray: []
  };
  templates: any;
  widgetIconSvg: any
  widgetList: any;
  searchArray: any
  subscription: Subscription;
  selectedTabValue: any = {};
  selectedWidgetList: any = []
  tempWidgetList: any
  existingTempList = []
  currentTemp: any = {}
  darkTheme: false
  isCustomised: false
  rowCount = 0;
  columnCount = 0;
  gridCount = 0;
  tilesCount = 0;
  chartCount = 0;
  chartGridCount = 0;
  countCheck = 0;
  fixedChartGridCount = 4;
  tempType = 'fixed';
  // maxCountCheck = 18;
  maxCountCheck = 0
  nodes = [{
    id: 1,
    name: "Vision Dashboard",
    parent: false,
    children: [
      {
        id: 2,
        name: "Retail Dashboard",
        parent: true,

        children: [
          {
            name: "Tab1",
          },
          {
            name: "Tab2",
          },
          {
            name: "Tab3",
          },
          {
            name: "Tab4",
          },
          {
            name: "Tab5",
          },
          {
            name: "Tab6",
          },
        ]
      },
      {
        id: 3,
        name: "Revenue Dashboard",
        parent: true,
        children: [
          {
            name: 'Tab1'
          }
        ]
      },
      {
        id: 4,
        name: "Expense Summary",
        parent: true,
        children: [
          {
            name: 'Tab1'
          }
        ]
      },
    ]
  }]
  @ViewChild('dashboard', { static: false }) dashboardComp: ElementRef;
  @ViewChild('tree', { static: false }) public tree: any;
  maximizeGrid = false
  widgetFilterList = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };
  dashboardFilterSettings: IDropdownSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    closeDropDownOnSelection: true,
  };
  filterDropdown: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };
  filterDropdown1: IDropdownSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    closeDropDownOnSelection: true,
  };
  filterListSelected;
  overallPromptValues: any = [];
  magnifierId = {};
  treeId = {};
  filterForm: FormGroup;
  promptValues: any = {};
  filterVal: {};
  filterSubmitted: boolean;
  loaderSvg: any = loaderSvg;
  drillDownKeys: any = {};
  drillDownList: any = [];
  selectedTemplateId: any;
  deleteWidget: boolean = false;
  savedWidgetId: any = '';
  filterOpened: boolean = false;
  colChartLists: any = ["MSColumn2D, MSColumn3D", "bar2D", "StackedColumn3D", "Column2D"];
  pieChartLists: any = ["Pie3D", "pie2D"]
  filterBox = false
  widgetBox = false
  widgetOpened: boolean = false;
  titleColor
  searchWidget = ''
  drillDownLoader: boolean = true;
  tableValues: any = {};
  altRowBgColor: string[] = [];
  settingBox: boolean = false;
  settingOpened: boolean = false;
  templateBox: boolean = false;
  templateOpened: boolean = false;
  drillDownTitle: any = '';
  reportDesignXml: any;
  previousOpened: 'widget'
  tileList
  showDownloadDropdown: boolean = false;
  pdfIcon = pdfSvg
  excelIcon = excelSvg;
  promptDescriptions = {};
  @ViewChild('downloadZipLink', { static: false }) private downloadZipLink: ElementRef;
  listLineType = ['MSColumnLine3D', 'StackedColumn2DLine', 'StackedColumn3DLine'];
  listLineTypeDY = ['MSColumn3DLineDY', 'StackedColumn3DLineDY'];
  dragStopped: boolean = false;
  position = { x: 0, y: 0 };
  positionResize = { resized: false }
  widgetThemes = [
    {
      caption: 'Blue',
      name: "BLUE",
      svg: blue
    },
    {
      caption: 'Orange',
      name: "ORANGE",
      svg: orange
    },
    {
      caption: 'Green',
      name: "GREEN",
      svg: green
    },
    {
      caption: 'Pink',
      name: "PINK",
      svg: pink
    }
  ]
  selectedTheme: any = "BLUE";
  scalingList = [
    {
      name: "In Thousands",
      value: "1000",
      active: false
    },
    {
      name: "In Millions",
      value: "1000000",
      active: false
    },
    {
      name: "In Billions",
      value: "1000000000",
      active: false
    },
    {
      name: "No Scaling",
      value: "1",
      active: false
    }
  ]
  scalingListDD = JSON.parse(JSON.stringify(this.scalingList))
  widgetListCpy: any = [];
  filterClicked: boolean = false;
  searchFilter = '';
  topSlider;
  savedSettings: any = [];
  autoRefreshTime: any = 15;

  constructor(
    private commonService: CommonService,
    private modalService: NgbModal,
    private jwtService: JwtService,
    private globalService: GlobalService,
    private apiService: ApiService,
    private xmlToJSonService: XmltoJsonService,
    private zone: NgZone,
    private router: Router,
    private rootService: RootService,
    private gpService: GroupingService,
  ) { }

  ngOnInit() {
    this.globalService.widget.iconEnabled = true;
    this.globalService.widget.enableTheme = false;
    this.templates = templates;
    this.widgetIconSvg = widgetIcons
    this.tileList = tileDetails
    this.getWidgetList();
    this.getDasboadDetails();
    this.selectedIndex = 0
    this.maxCountCheck = this.rowCount * this.columnCount;
    this.jwtService.set('reportType', "I")
    this.globalService.getsideBarChangesDectect().subscribe((params) => {
      if (params) {
        this.showGrider = false;
        setTimeout(() => {
          this.showGrider = true;
        }, 500);
      }
    });
    this.topSlider = this.globalService.gettopBarChangesDectect().subscribe((params) => {
      if (params['type'] == 'slider') {
        // this.toggleSideFilter('widget');
        this.widgetBox = true;
        const sideFilter = document.getElementById('side-filter-hold');
        // const sideArrowClass = document.getElementById('sidebarArrow');
        const sideFilterOverlay = document.getElementById('sideFilterOverlay');

        // type == 'filter' ? this.filterOpened = true : type == 'widget' ? this.widgetOpened = true :
        //   type == 'setting' ? this.settingOpened = true : type == 'template' ? this.templateOpened = true : ''
        // type == 'filter' ? sideFilterOverlay.classList.add('show') : '';
        sideFilter.classList.add('setRight');
        !this.positionResize.resized ? sideFilter.classList.add("col-sm-8", "col-md-4", "col-lg-4", "col-xl-4") : '';
        // setTimeout(() => {
        //   const sideArrow = document.getElementById('sidebarArrow');
        //   if (!isNullOrUndefined(sideArrow))
        //     sideArrow.classList.add('fa-angle-double-right');
        // }, 100)
        // this.previousOpened = type
        sideFilter.style.transform = this.positionResize['transform'];
        sideFilter.style.width = this.positionResize['width'];
        sideFilter.style.height = this.positionResize['height'];
        sideFilter.style.left = this.positionResize['left'];
        sideFilter.style.top = this.positionResize['top'];
      }
      else if (params['type'] == 'clear') {
        this.selectConfigType('clear');
      }
      else if (params['type'] == 'save') {
        this.selectConfigType('save');
      }
      else if (params['type'] == 'delete') {
        this.deleteWidget ? this.selectConfigType('delete') : '';
      }
      else if (params['type'] == 'download') {
        this.widgetDownload();
      }
    });

  }

  @HostListener('window:resize')
  public detectResize(): void {

    this.sideFilterPosition();
    //this.dashboardContHgt();
    this.sideFilterHeight();

  }

  @HostListener('document:click', ['$event'])
  // @HostListener('document:touchstart', ['$event'])
  handleOutsideClick(event) {
    if (!event.target.classList.contains('side-filter-hold') && !event.target.closest('.side-filter-hold') &&
      !event.target.closest('.slider-top')) {
      if (!event.target.closest('.icon-box')) {
        if(event.target.closest('app-side-bar')) {
          this.filterBox = false
        }
        if (!event.target.closest('.mainTempPage') && !this.filterBox) {
          this.filterBox = false
          this.widgetBox = false
          this.settingBox = false
          this.templateBox = false
          const sideFilter = document.getElementById('side-filter-hold');
          const sideFilterOverlay = document.getElementById('sideFilterOverlay');
          const sideIcon = document.getElementById('side-filter-icon');
          // const sideArrowClass = document.getElementById('sidebarArrow');
          if (!isNullOrUndefined(sideFilter)) {
            if (sideFilter.classList.contains('setRight')) {
              sideFilterOverlay.classList.remove('show');
              sideFilter.classList.remove('setRight');
              this.position = { x: 0, y: 0 };
              // sideFilter.style.transform = 'unset'
              // sideArrowClass.classList.remove('fa-angle-double-right');
              sideFilter.classList.remove("col-sm-8", "col-md-4", "col-lg-4", "col-xl-4");
              sideFilter.classList.remove("onRezing");
              sideFilter.style.removeProperty("left");
              sideFilter.style.removeProperty("top");
              sideFilter.style.removeProperty("height");
              sideFilter.style.removeProperty("width");
            }
          }
        }
      }
    }

    // if (!event.target.classList.contains('popoverHold') && !event.target.closest('.popoverHold')) {
    //   if (!event.target.closest('.widgetItem') && !event.target.closest('.dropdown-menu')) {
    //     let drilldownPopup = document.getElementById('popoverHold')
    //     drilldownPopup.style.display = "none";
    //     this.scalingListDD.forEach(ele => ele.active = false)
    //   }
    // }
    if (!event.target.closest('.help_menu') && (event.target.closest('.mainNav11') || event.target.closest('.top-bar-menus'))) {
      let drilldownPopup = document.getElementById('popoverHold')
      drilldownPopup.style.display = "none";
      this.scalingListDD.forEach(ele => ele.active = false)
    }
    if (!event.target.closest(".popupBody") && !event.target.classList.contains('widgetAdd-icon')) {
      this.closePopoverChart();
    }

    if (!event.target.closest('.outside-click') && !event.target.closest('ngb-datepicker') && !event.target.classList.contains("checkOverAll")) {

      this.templateDetails[this.selectedIndex].dataSet.forEach((element1, index) => {
        if (element1['showWidgetMoreOption'] && element1.initialRequest && element1.initialRequest.reportFilters
          && this.filterClicked) {
          this.individualFilterReq(element1, index)
        }
        element1['showDownloadDropdown'] = false;
        element1['showWidgetMoreOption'] = false;
        element1['showWidgetSortOption'] = false;
        element1['showWidgetFilterOption'] = false;
        element1['showWidgetExportOption'] = false;
        element1['showWidgetChartOption'] = false;
        element1['showWidgetScaling'] = false;
        if (element1.initialRequest && element1.initialRequest.reportFilters &&
          element1.initialRequest.reportFilters.length)
          element1.initialRequest.reportFilters.forEach((filter) => {
            filter.filterCheckbox = false;
          })
      })
      this.drillDownList.forEach((element1) => {
        element1['showWidgetExportOption'] = false;
        element1['showWidgetScalingOption'] = false;
        element1['showWidgetSortOption'] = false;
      })
    }
  }

  individualFilterReq(item, index) {
    let sendReq = false;
    item.initialRequest.reportFilters.forEach(ele => {
      if (ele.filterList) {
        item.initialRequest[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
          ele.filterChecked ? `'ALL'` :
            ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
          ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
        sendReq = true;
      }
    })
    if (!isNullOrUndefined(item.scalingFactorValue)) {
      item.initialRequest['scalingFactor'] = item.scalingFactorValue;
      // item['scalingFactor'] = item.scalingFactorValue;
      sendReq = true;
    }
    if (sendReq) {
      this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = '';
      this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = true;
      let reportReq = Object.assign(JSON.parse(JSON.stringify(item.initialRequest)), this.getPromptValues());
      // reportReq['scalingFactor'] = "0";
      this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
        this.filterClicked = false;
        this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = false;
        this.templateDetails[this.selectedIndex].dataSet[index].otherInfo = resp['otherInfo'];
        sendReq = false;
        if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {

          this.templateDetails[this.selectedIndex].dataSet[index].data = resp['response'];
          this.templateDetails[this.selectedIndex]['dataSet'][index]['scalingFactor'] = resp['response']['scalingFactor'];
            if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'])) {
              this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'].forEach(sca => {
                if (sca.value == resp['response']['scalingFactor']) {
                  sca.active = true;
                }
                else {
                  sca.active = false;
                }
              })
            }
          if (resp['response'].objectType == 'C') {

            if (!isNull(resp['response'].chartType) && resp['response'].chartType.search("FCMap_") > -1)
              resp['response'].chartType = resp['response'].chartType.replaceAll("FCMap_", "");

            if (resp['response'].chartType == 'HLinearGauge' || resp['response'].chartType == 'AngularGauge'
              || resp['response'].chartType == 'Kenya') {
              this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'] =
                FusionCharts.transcodeData(resp['response'].chartData, "xml", "json", false)
            }
            else {
              if (this.listLineType.includes(resp['response']['chartType'])) {
                let data = resp['response']['chartData'];
                var regexp = /<dataset/g;
                let c = data.match(regexp) || [];
                resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
              }
              if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
                let data = resp['response']['chartData'];
                var regexp = /<dataset/g;
                let c = data.match(regexp) || [];
                resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
              }
              this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'] =
                this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
            }


            if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart)) {
              this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartLeftMargin = 10;
              this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartTopMargin = 15;
              this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartRightMargin = 0;
              this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartBottomMargin = 0;
            }
          }
          if (resp['response'].objectType == 'G') {
            resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
            resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
            resp['response'].columnHeaderslstCopy.forEach(element => {
              if (!isNullOrUndefined(item.columnHeaderslstCopy) && item.columnHeaderslstCopy.length){
                item.columnHeaderslstCopy.forEach(element1 => {
                  if (element['dbColumnName'] == element1['dbColumnName']) {
                    element['sortLevel'] = element1['sortLevel'];
                    element['sortReq'] = element1['sortReq'];
                  }
                })
              }
              else {
                element['sortLevel'] = 'noSort';
                element['sortReq'] = {};
              }
            })
            if (Object.keys(item['sortReq']).length) {
              resp['response'].gridDataSet =
                this.gpService.orderBy(item['sortReq'], resp['response'].gridDataSet, resp['response'].columnHeaderslstCopy);
            }
            else {
              resp['response'].gridDataSet = JSON.parse(JSON.stringify(resp['response'].gridDataSetCopy))
            }
          }
          if (resp['response'].objectType == 'T' || resp['response'].objectType == 'SC') {
            this.parsingObject(resp['response'])
          }
        }
        else {
          this.templateDetails[this.selectedIndex].dataSet[index].data = {};
          this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = resp['message']
          // this.globalService.showToastr.error(resp['message'])
        }
      })
    }
  }

  onResized(event) {
    let obj = this.templateDetails[this.selectedIndex]['templatesDetal'];
    if (!isUndefined(obj) && (!isUndefined(obj.api))) {
      this.templateDetails[this.selectedIndex]['templatesDetal'].api.resize();
    }
  }
  checkValue(event) {
    this.darkTheme = event.target.checked
    if (this.darkTheme) {
      this.templateDetails[this.selectedIndex]['theme'] = "blackTheme";
      this.templateDetails[this.selectedIndex]['dataSet'].forEach((ele, index) => {
        if (ele['data']['objectType'] == 'C') {
          ele['data']['chartDataJson']['chart']['bgAlpha'] = '0';
        }
      });
    }
    else {
      this.templateDetails[this.selectedIndex]['theme'] = "whiteTheme"
      this.templateDetails[this.selectedIndex]['dataSet'].forEach((ele, index) => {
        if (ele['data']['objectType'] == 'C') {
          if (ele['data']['chartDataJson']['chart']['bgAlpha'])
            delete ele['data']['chartDataJson']['chart']['bgAlpha'];
        }
      });
    }
  }

  checkCustomise(event) {
    this.isCustomised = event.target.checked
    if (this.isCustomised) {
      this.templateDetails[this.selectedIndex]['templatesDetal']['resizable']['enabled'] = true
      let data = this.templateDetails[this.selectedIndex]['dataSet']
      data.forEach(element => {
        element.dragEnabled = true
      });

    }
    else {
      this.templateDetails[this.selectedIndex]['templatesDetal'].resizable.enabled = false
      let data = this.templateDetails[this.selectedIndex]['dataSet']
      data.forEach(element => {
        element.dragEnabled = false
      });
    }

  }
  addCustomTemplate(tab) {
    this.tabs.forEach(ele => { ele.select = false; });
    tab.select = true;
    this.selectedTab = tab.id;
  }
  addTemplates(name, tab) {
    this.selectedTab = tab
    this.tabs.forEach(element => {
      if (this.selectedTab == element.id) {
        element.select = true
      }
      else {
        element.select = false
      }
    });
    this.sideConfigNavClose(name);
  }

  sideConfigNavClose(data) {
    this.rebindView = false;
    this.configView = false;
    this.templateView = false;
    this.widgetView = false
    this.themeView = false
    if (data != 'all') {
      setTimeout(() => {
        this[data] = true;
      }, 100);
    }
  }

  selectConfigType(type) {
    this.sideBarTog = false;
    if (type == 'template') {
      // this.selectedWidgetList = []
      // this.widgetList = this.tempWidgetList;
      // this.searchArray = [...this.widgetList]
      this.selectedTab = "temp"
      this.tabs.forEach(element => {
        if (element.id == "temp") {
          element.select = true
        }
        else {
          element.select = false
        }
      });

      // this.templateViewList = true

      this.sideConfigNavClose('templateView');
    }
    else if (type == 'widget') {
      this.sideConfigNavClose('widgetView');

    }
    else if (type == 'save') {
      // let message = ''
      // this.saveTemplate(message, '')
      if (this.isWidgetDataPresent()) {
        let widgetId = '';
        let reportUserDeflst = [];
        this.templateDetails[this.selectedIndex]['dataSet'].forEach((element, index) => {
          if (!isNullOrUndefined(element.initialRequest && element.initialRequest.objectType)) {
            let reportId = !isNullOrUndefined(element.data && element.data.reportId) ? element.data.reportId :
            !isNullOrUndefined(element.otherInfo && element.otherInfo.reportId) ? element.otherInfo.reportId : '';
            widgetId += `${element.id}-${reportId}`;
            if (this.templateDetails[this.selectedIndex]['dataSet'].length - 1 != index) {
              widgetId += '!@#';
            }
            let obj = {};
            obj['reportId'] = !isNullOrUndefined(element.data && element.data.reportId) ? element.data.reportId :
            !isNullOrUndefined(element.otherInfo && element.otherInfo.reportId) ? element.otherInfo.reportId : '';
            obj['subReportId'] = !isNullOrUndefined(element.data && element.data.subReportId) ? element.data.subReportId :
            !isNullOrUndefined(element.otherInfo && element.otherInfo.subReportId) ? element.otherInfo.subReportId : '';
            obj['groupingColumn'] = "";
            obj['searchColumn'] = "";
            obj['scalingFactor'] = element.scalingFactor;
            let list = [];
            let sendType = '';
            if(!isNullOrUndefined(element.data && element.data.objectType) && element.data.objectType == 'C')
            if(!isNullOrUndefined(element.data.chartList) && element.data.chartList.length) {
              element.data.chartList.forEach(ele=> {
                list.push({name: Object.keys(ele)[0], active: false, description: Object.values(ele)[0]})
              })
            }
            list.forEach(e=> {
              if(!isNullOrUndefined(element.data.chartType) && element.data.chartType == e.name)
              sendType = e.name
            })
            obj['chartType'] = sendType;
            let sortColumn = 'ORDER BY ';
            let sortArr = !isNullOrUndefined(element.data && element.data.columnHeaderslstCopy) ?
              element.data.columnHeaderslstCopy.filter(item => item.sortLevel != 'noSort') : [];
            sortArr.forEach((arr, ind) => {
              sortColumn += `${arr.dbColumnName} ${arr.sortLevel.toUpperCase()}`
              if (sortArr.length - 1 != ind) {
                sortColumn += ',';
              }
            })
            obj['sortColumn'] = sortArr.length ? sortColumn : '';
            let localPrompt = {}
            if (!isNullOrUndefined(element.initialRequest && element.initialRequest.reportFilters))
              element.initialRequest.reportFilters.forEach(ele => {
                if (ele.filterList) {
                  localPrompt[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
                    ele.filterChecked ? `'ALL'` :
                      ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
                    ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
                }
              })
            obj = Object.assign(obj, localPrompt);
            reportUserDeflst.push(obj)
          }
        })
        let saveReq = {
          savedWidgetId: widgetId,
          templateId: this.templateDetails[this.selectedIndex]['templateId'],
          reportUserDeflst: reportUserDeflst,
          widgetTheme: this.selectedTheme
        }
        this.apiService.post(environment.saveUserWidget, saveReq).subscribe((resp) => {
          if (resp['status'] == 1) {
            this.globalService.showToastr.success(resp['message']);
            this.selectedTemplateId = this.templateDetails[this.selectedIndex]['templateId'];
            this.savedWidgetId = widgetId;
            this.deleteWidget = true;
            this.deleteWidget ? this.globalService.widget.enableDelete = true : false
          }
          else {
            this.globalService.showToastr.error(resp['message'])
          }
        })
      }
    }
    else if (type == 'delete') {
      const modelRef = this.modalService.open(GenericPopupComponent, {
        size: <any>'md',
        backdrop: false,
        windowClass: "genericPopup"
      });
      modelRef.componentInstance.title = "Alert";
      modelRef.componentInstance.message = "Do you want to delete this widget?";
      modelRef.componentInstance.popupType = 'Confirm';
      modelRef.componentInstance.closeTime = 'No';
      // modelRef.componentInstance.openTemplate = true;
      modelRef.componentInstance.userConfirmation.subscribe(resp => {
        let reportUserDeflst = [];
        this.templateDetails[this.selectedIndex]['dataSet'].forEach((element, index) => {
          if (!isNullOrUndefined(element.data.objectType)) {
            let obj = {};
            obj['reportId'] = element.data.reportId;
            obj['subReportId'] = element.data.subReportId;
            reportUserDeflst.push(obj)
          }
        })
        if (resp.type == 'Yes') {
          let deleteReq = {
            savedWidgetId: this.savedWidgetId,
            templateId: this.selectedTemplateId,
            reportUserDeflst: reportUserDeflst
          }
          this.apiService.post(environment.deleteUserWidget, deleteReq).subscribe((resp) => {
            if (resp['status'] == 1) {
              this.globalService.showToastr.success(resp['message']);
              this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
                this.router.navigate(['/main/VisionStudio'], { skipLocationChange: true });
              });
            }
            else {
              this.globalService.showToastr.error(resp['message'])
            }
          })
        }
        modelRef.close();

      });
    }
    else if (type == 'setting') {
      this.sideConfigNavClose('themeView');


    }
    else if (type == 'saveAndPublish') {
      let message = ''
      this.saveTemplate(message, type)
      // this.openDashboardPopupTree()

    }
    else if ('clear') {
      if (this.isWidgetDataPresent())
        this.clearWidget()
    }

  }
  clearWidget() {
    if (this.templateDetails[this.selectedIndex]['dataSet'].some(item => item.data.objectType)) {
      const modelRef = this.modalService.open(GenericPopupComponent, {
        size: <any>'md',
        backdrop: false,
        windowClass: "genericPopup"
      });
      modelRef.componentInstance.title = "Alert";
      modelRef.componentInstance.message = "Do you want to clear all the widgets?";
      modelRef.componentInstance.popupType = 'Confirm';
      modelRef.componentInstance.closeTime = 'No';
      // modelRef.componentInstance.openTemplate = true;
      modelRef.componentInstance.userConfirmation.subscribe(resp => {

        if (resp.type == 'Yes') {
          this.templateDetails[this.selectedIndex]['dataSet'].forEach(element => {
            element['widgetTitle'] = '';
            element['reportId'] = '';
            element['reportPeriod'] = '';
            element['scalingFactor'] = '';
            element['data'] = {}
            if (element['apiCall']) {
              element['apiCall'].unsubscribe();
            }
          });
          //  this.selectedWidgetList = []
          this.widgetList = this.tempWidgetList
          this.searchArray = [...this.widgetList]
          this.setWidgetTrueFalse();
        }
        modelRef.close();

      });

    }
  }
  isWidgetDataPresent() {
    let data = this.templateDetails[this.selectedIndex]["dataSet"]
    let result = data.some(element => (element.data.objectType))
    return result
  }
  selectedTemp(data, name, i?) {
    // this.show = false
    let result: any = {
      type: name,
      data: data,
      index: i
    }
    this.resultData(result);
    if (name == 'template' && isNullOrUndefined(i))
      this.toggleSideFilter('widget')
  }
  resultData(data) {
    this.selectedTemplateId = data.data.templateId;
    const sideFilter = document.getElementById('side-filter-hold');
    // const sideArrowClass = document.getElementById('sidebarArrow');
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    if (sideFilter.classList.contains('setRight')) {
      this.filterOpened = false;
      this.widgetOpened = false;
      this.settingOpened = false;
      this.templateOpened = false;
      sideFilterOverlay.classList.remove('show');
      sideFilter.classList.remove('setRight');
      // sideArrowClass.classList.remove('fa-angle-double-right');
    }
    this.sideConfigNavClose('all');
    if (data.type == 'template') {
      if (this.templateDetails[this.selectedIndex]['dataSet'].some(item => item.data.objectType)) {
        const modelRef = this.modalService.open(GenericPopupComponent, {
          size: <any>'md',
          backdrop: false,
          windowClass: "genericPopup"
        });
        modelRef.componentInstance.title = "Alert";
        modelRef.componentInstance.message = "Do you want to change the Template ?";
        modelRef.componentInstance.popupType = 'Confirm';
        modelRef.componentInstance.closeTime = 'No';
        // modelRef.componentInstance.openTemplate = true;
        modelRef.componentInstance.userConfirmation.subscribe(resp => {

          if (resp.type == 'Yes') {
            let selected;
            this.templates.forEach(element => {
              if (element.templateId == data.data.templateId)
                selected = element;
            });
            this.selectedWidgetList = []
            this.widgetList = this.tempWidgetList;
            this.searchArray = [...this.widgetList]
            this.templateSettings(selected);
            this.setWidgetTrueFalse();
            this.templateDetails[this.selectedIndex]['dataSet'].forEach((ele, index) => {
              ele['data']['showLoader'] = false;
              ele['showDownloadDropdown'] = false;
              ele['scalingList'] = JSON.parse(JSON.stringify(this.scalingList))
            });
          }
          modelRef.close();

        });

      }
      else {
        let selected;
        this.templates.forEach(element => {
          if (element.templateId == data.data.templateId)
            selected = element;
        });
        this.selectedWidgetList = []
        this.widgetList = this.tempWidgetList;
        this.searchArray = [...this.widgetList]
        this.templateSettings(selected);
        this.setWidgetTrueFalse();
        //this.templateDetails[this.selectedIndex]['dataSet'] = []
        //this.templateDetails[this.selectedIndex]['dataSet'] = selected['data'];
        // this.findWidgetData()
        this.templateDetails[this.selectedIndex]['dataSet'].forEach((ele, index) => {
          ele['data']['showLoader'] = false;
          ele['scalingList'] = JSON.parse(JSON.stringify(this.scalingList))
        });
      }
    }
    if (data.type == 'widget') {
      this.closePopoverChart();
      // this.selectedTempId = data.index;
      // data.data['filterCheckbox'] = false;
      // data.data['filterChecked'] = false;
      data.data.reportFilters.forEach((filter) => {
        this.filterDataSet(filter);
      })
      let selectedWid = data.data;
      // if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[data.index]['scalingList'])) {
      //   this.templateDetails[this.selectedIndex].dataSet[data.index]['scalingList'].forEach(sca => {
      //     if (sca.value == selectedWid['scalingFactor']) {
      //       sca.active = true;
      //     }
      //   })
      // }
      selectedWid.reportFilters.forEach(ele => {
        if (ele.filterList) {
          selectedWid[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
            ele.filterChecked ? `'ALL'` :
              ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
            ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
        }
      })
      if (this.templateDetails[this.selectedIndex].dataSet[data.index]['type'] == 'tile') {
        if (data.data.objectType == 'T' || data.data.objectType == 'SC') {
          this.templateDetails[this.selectedIndex]['dataSet'][data.index]['reportId'] = selectedWid.reportId;
          this.templateDetails[this.selectedIndex].dataSet[data.index]['widgetTitle'] = selectedWid.reportTitle
          this.templateDetails[this.selectedIndex].dataSet[data.index]['initialRequest'] = selectedWid;
          let reportReq = Object.assign(JSON.parse(JSON.stringify(selectedWid)), this.getPromptValues());
          reportReq['widgetTheme'] = this.selectedTheme;
          this.templateDetails[this.selectedIndex].dataSet[data.index].data['showLoader'] = true;
          this.setWidgetTrueFalse();
          reportReq['scalingFactor'] = "0";
          this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
            this.selectedWidgetList.push(data.data)
            this.selectedTempId = data.index;
            this.searchArray = [...this.widgetList]
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['showLoader'] = false;
            this.templateDetails[this.selectedIndex].dataSet[data.index].otherInfo = resp['otherInfo'];
            if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {
              this.templateDetails[this.selectedIndex].dataSet[data.index].data = resp['response']
              // this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['endResult'] = data.data.tileData.endResult
              this.parsingObject(resp['response'])
            }
            else {
              this.templateDetails[this.selectedIndex].dataSet[data.index].data['errorMessage'] = resp['message']
            }
          })
        }
      }
      else {
        if (data.data.objectType != 'T') {
          if (data.data.reportDesignXml != '' && data.data.reportDesignXml != null) {
            //let reportDesignXml = JSON.parse(data.data.reportDesignXml.replaceAll("-","_"));
            let reportDesignXml = JSON.parse(data.data.reportDesignXml)
            let titleColor = '#' + reportDesignXml["TITLE-COLOR"];
            this.templateDetails[this.selectedIndex]['dataSet'][data.index]['titleColor'] = titleColor;
            this.titleColor = titleColor;
          }
          this.templateDetails[this.selectedIndex].dataSet[data.index]['initialRequest'] = data.data;
          this.templateDetails[this.selectedIndex].dataSet[data.index]['reportId'] = selectedWid.reportId
          this.templateDetails[this.selectedIndex].dataSet[data.index]['widgetTitle'] = selectedWid.reportTitle
          this.templateDetails[this.selectedIndex].dataSet[data.index]['reportPeriod'] = selectedWid.reportPeriod
          this.templateDetails[this.selectedIndex].dataSet[data.index]['scalingFactor'] = ""
          let reportReq = Object.assign(JSON.parse(JSON.stringify(selectedWid)), this.getPromptValues());
          this.templateDetails[this.selectedIndex].dataSet[data.index].data['showLoader'] = true;
          this.getWidgetForGridItem(data, reportReq)
        }
        // this.chartGridSettings(data);
      }
    }
    if (data.type == "exist") {
      let currentTempData = this.templateDetails[this.selectedIndex]
      if (currentTempData.dataSet.length == 0) {
        this.selectedWidgetList = []
        this.templateDetails = {}
        this.templateDetails[this.selectedIndex] = data.data
        this.templateDetails[this.selectedIndex].dataSet.forEach(element => {
          if (element.data != undefined) {
            this.selectedWidgetList.push(element.data)
          }
        });
        this.setWidgetTrueFalse();
        this.searchArray = [...this.widgetList]

      }
      else {
        let widgetData
        let layoutWidgetData
        layoutWidgetData = currentTempData.dataSet.some(function (ele) {
          return !isNullOrUndefined(ele.data)
        });
        if (layoutWidgetData) {
          widgetData = this.existingTempList.some(function (element) {
            return JSON.stringify(element.dataSet) == JSON.stringify(currentTempData.dataSet)
          });
        }
        else {
          widgetData = true
        }
        if (!widgetData && !isNullOrUndefined(widgetData)) {
          const modelRef = this.modalService.open(GenericPopupComponent, {
            size: <any>'md',
            backdrop: false,
            windowClass: "genericPopup"
          });
          modelRef.componentInstance.title = "Alert";
          modelRef.componentInstance.message = "Do you want to save template?";
          modelRef.componentInstance.popupType = 'Confirmed';
          modelRef.componentInstance.closeTime = 'No';
          modelRef.componentInstance.openTemplate = true;
          modelRef.componentInstance.userConfirmation.subscribe(resp => {

            if (resp.type == 'Yes') {
              let list = []

              // let list = this.existingTempList
              this.currentTemp = { ...currentTempData }
              this.currentTemp['cusTemplateName'] = resp.name;
              this.currentTemp['description'] = resp.description
              list.push(this.currentTemp)
              // this.jwtService.set('template', JSON.stringify(list))
              this.globalService.showToastr.success('Template saved successfully')

              this.selectedWidgetList = []
              this.templateDetails = {}
              this.templateDetails[this.selectedIndex] = data.data
              this.templateDetails[this.selectedIndex].dataSet.forEach(element => {
                if (element.data != undefined) {
                  this.selectedWidgetList.push(element.data)
                }
              });
              this.setWidgetTrueFalse();
              this.searchArray = [...this.widgetList]

            }
            else {
              this.selectedWidgetList = []
              this.templateDetails = {}
              this.templateDetails[this.selectedIndex] = data.data
              this.templateDetails[this.selectedIndex].dataSet.forEach(element => {
                if (element.data != undefined) {
                  this.selectedWidgetList.push(element.data)
                }
              });
              this.setWidgetTrueFalse();
              this.searchArray = [...this.widgetList]

            }

            modelRef.close();

          });


        }
        else {
          this.selectedWidgetList = []
          this.templateDetails = {}
          this.templateDetails[this.selectedIndex] = data.data
          this.templateDetails[this.selectedIndex].dataSet.forEach(element => {
            if (element.data != undefined) {
              this.selectedWidgetList.push(element.data)
            }
          });
          this.setWidgetTrueFalse();
          this.searchArray = [...this.widgetList]

        }
      }


    }

  }
  getWidgetForGridItem(data, reportReq) {
    this.setWidgetTrueFalse();
    reportReq['widgetTheme'] = this.selectedTheme;
    reportReq['scalingFactor'] = "0";
    this.templateDetails[this.selectedIndex].dataSet[data.index]['apiCall'] = this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
      this.selectedWidgetList.push(data.data)
      this.selectedTempId = data.index;
      this.searchArray = [...this.widgetList]
      this.templateDetails[this.selectedIndex].dataSet[data.index].data['showLoader'] = false;
      this.templateDetails[this.selectedIndex].dataSet[data.index].otherInfo = resp['otherInfo'];
      if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {
        this.templateDetails[this.selectedIndex].dataSet[data.index].data = resp['response'];
        this.templateDetails[this.selectedIndex].dataSet[data.index]['scalingFactor'] =  resp['response']['scalingFactor'];
        if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[data.index]['scalingList'])) {
          this.templateDetails[this.selectedIndex].dataSet[data.index]['scalingList'].forEach(sca => {
            if (sca.value == resp['response']['scalingFactor']) {
              sca.active = true;
            }
            else {
              sca.active = false;
            }
          })
        }
        if (resp['response'].objectType == 'C') {

          if (!isNull(resp['response'].chartType) && resp['response'].chartType.search("FCMap_") > -1)
            resp['response'].chartType = resp['response'].chartType.replaceAll("FCMap_", "");

          if (resp['response'].chartType == 'HLinearGauge' || resp['response'].chartType == 'AngularGauge'
            || resp['response'].chartType == 'Kenya') {
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'] =
              FusionCharts.transcodeData(resp['response'].chartData, "xml", "json", false)
          }
          else {
            if (this.listLineType.includes(resp['response']['chartType'])) {
              let data = resp['response']['chartData'];
              var regexp = /<dataset/g;
              let c = data.match(regexp) || [];
              resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
            }
            if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
              let data = resp['response']['chartData'];
              var regexp = /<dataset/g;
              let c = data.match(regexp) || [];
              resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
            }
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'] =
              this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
          }
          if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'].chart)) {
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'].chart.chartLeftMargin = 10;
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'].chart.chartTopMargin = 15;
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'].chart.chartRightMargin = 0;
            this.templateDetails[this.selectedIndex].dataSet[data.index].data['chartDataJson'].chart.chartBottomMargin = 0;
          }

        }
        if (resp['response'].objectType == 'G') {
          resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
          resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
          resp['response'].columnHeaderslstCopy.forEach(element => {
            element['sortLevel'] = 'noSort';
            element['sortReq'] = {};
          })
        }

      }
      else {
        this.templateDetails[this.selectedIndex].dataSet[data.index].data['errorMessage'] = resp['message']
        // this.globalService.showToastr.error(resp['message'])
      }
    })
  }
  templateSettings(resp) {
    let tempData = JSON.parse(JSON.stringify(resp['data']));
    let config = JSON.parse(JSON.stringify(resp['config']));
    config['enableEmptyCellContextMenu'] = true,
      // config['emptyCellContextMenuCallback'] = this.emptyCellClick.bind(this),
      config['draggable']['ignoreContentClass'] = 'drag-ignore';
    this.templateDetails[this.selectedIndex]['templatesDetal'] = config;
    this.templateDetails[this.selectedIndex]['singleton'] = ('singleton' in resp) ?
      JSON.parse(JSON.stringify(resp['singleton'])) : [];
    this.templateDetails[this.selectedIndex]['dataSet'] = tempData
    this.templateDetails[this.selectedIndex]['templateSelected'] = true;
    this.templateDetails[this.selectedIndex]['templateId'] = resp['templateId'];
  }
  rebindDetails: any = {};
  emptyCellClick(event: MouseEvent, item: GridsterItem) {
    this.sideConfigNavClose('all');
    let selected = this.templateDetails[this.selectedIndex];
    let type = selected['templatesDetal']['gridType'];
    if (type == 'fit') {
      let rqObj = {};
      let smArry = selected['dataSet'].filter(ele => ele.size == 'md');
      let bgArry = selected['dataSet'].filter(ele => ele.size == 'lg');
      rqObj['max'] = selected['templatesDetal']['smcount'];
      rqObj['existing'] = (smArry.length + (bgArry.length * 4));
      rqObj['dataSet'] = selected['dataSet'];
      rqObj['countType'] = (selected['templatesDetal']['smcount'] == 12) ? false : true;
      let rowCount = 0;
      let colCount = 0;
      for (var i = 0; i <= bgArry.length - 1; i++) {
        if ((bgArry[i]['rows'] > bgArry[i]['minItemRows'] || bgArry[i]['cols'] > bgArry[i]['minItemCols'])) {
          rowCount = (bgArry[i]['rows'] - bgArry[i]['minItemRows']) * 2;
          colCount = (bgArry[i]['cols'] - bgArry[i]['minItemCols']) * 2;
          rqObj['existing'] = rqObj['existing'] + rowCount + colCount;
        }
      }
      this.rebindDetails = rqObj;
      this.sideConfigNavClose('rebindView');
    }
  }
  selectedIndex: any = 0;

  tabSetObj() {
    this.templateDetails = {};
    this.templateDetails[0] = {
      'templatesDetal': {},
      'templateSelected': false,
      'dataSet': [],
      'singleton': [],
      'theme': 'whiteTheme'
    };
    this.selectedIndex = 0;
    this.previewData();
  }
  previewData() {
    this.selectedTabValue = JSON.stringify(this.templateDetails[this.selectedIndex]);
  }
  getDasboadDetails() {
    // this.subscription = this.commonService.dasTempCreate.subscribe(ele => {
    //   if (Object.keys(ele).length) {
    //     this.dashboardObj = ele;
    this.tabSetObj();
    //   }
    // })
  }

  close() {
    this.sideConfigNavClose('all');
  }
  popoverOpen(event, index, item) {
    // this.toggleSideWidget()
    this.templateDetails[this.selectedIndex]['dataSet'].forEach(ele => { ele.popoverOpened = false; });
    item.popoverOpened = true;
    const popoverHold = document.getElementsByClassName('popupBody')
    Array.from(popoverHold).forEach((pophold) => {
      pophold['style'].display = 'none';
    });
    const maintileElem = event.target.closest('.gridster-body');

    const popoverchartElem = maintileElem.querySelector('#widgetListPopup');
    popoverchartElem.style.display = 'block';
    // this.selectedTempId = index
  }
  closePopoverChart() {
    this.templateDetails[this.selectedIndex]['dataSet'].forEach(ele => { ele.popoverOpened = false; });
    const popoverHold = document.getElementsByClassName('popupBody')
    Array.from(popoverHold).forEach((pophold) => {
      pophold['style'].display = 'none';
    });
    this.searchWidget = '';
    this.searchWidgetLst('')
  }

  saveTemplate(message, type) {
    // this.existingTempList = JSON.parse(this.jwtService.get('template')) ? JSON.parse(this.jwtService.get('template')) : []
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false,
      windowClass: "genericPopup"
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = message;
    modelRef.componentInstance.popupType = 'Confirmed';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.openTemplate = true;
    modelRef.componentInstance.userConfirmation.subscribe(resp => {

      if (resp.type == 'Yes') {

        // let list = this.existingTempList
        let list = []
        this.currentTemp = { ...this.templateDetails[this.selectedIndex] }
        this.currentTemp['cusTemplateName'] = resp.name;
        this.currentTemp['description'] = resp.description
        list.push(this.currentTemp)
        // this.jwtService.set('template', JSON.stringify(list))

        if (type == "saveAndPublish") {
          document.body.classList.remove("genericPopup");
          modelRef.close();

          this.openDashboardPopupTree()

        }
        else {
          this.globalService.showToastr.success('Template saved successfully')

        }

      }
      modelRef.close();

    });

  }

  openDashboardPopupTree() {
    this.modalService.open(this.dashboardComp, { size: 'lg', backdrop: 'static' });

  }
  removeItem($event, item) {
    $event.preventDefault();
    $event.stopPropagation();
    this.templateDetails[this.selectedIndex]['dataSet'].splice(
      this.templateDetails[this.selectedIndex]['dataSet'].indexOf(item), 1);
  }
  removeWidget(item, i) {
    let selectedItem = item
    selectedItem = Object.keys(selectedItem).reduce(function (obj, key) {
      if (key != 'data') {
        obj[key] = selectedItem[key];
      }
      return obj;
    }, {});
    this.selectedWidgetList.forEach((element, index) => {
      if (element.reportId == item.initialRequest.reportId) {
        this.selectedWidgetList.splice(index, 1);
      }
    });
    this.searchArray = [...this.widgetList]

    this.templateDetails[this.selectedIndex]['dataSet'][i]['data'] = {};
    this.templateDetails[this.selectedIndex]['dataSet'][i]['otherInfo'] = {};
    delete this.templateDetails[this.selectedIndex]['dataSet'][i]['initialRequest']
    this.templateDetails[this.selectedIndex]['dataSet'][i]['widgetTitle'] = "";
    this.templateDetails[this.selectedIndex]['dataSet'][i]['reportId'] = "";
    this.templateDetails[this.selectedIndex]['dataSet'][i]['reportPeriod'] = "";
    this.templateDetails[this.selectedIndex]['dataSet'][i]['scalingFactor'] = "";
    this.templateDetails[this.selectedIndex]['dataSet'][i]['scalingFactorDd'] = "";
    if (this.templateDetails[this.selectedIndex]['dataSet'][i]['apiCall']) {
      this.templateDetails[this.selectedIndex]['dataSet'][i]['apiCall'].unsubscribe();
    }
    this.templateDetails[this.selectedIndex]['dataSet'][i]['data']['showLoader'] = false;
    this.setWidgetTrueFalse();

  }

  minMax(item, type, drop) {
    drop.close();
    item.showWidgetMoreOption = false;
    this.templateDetails[this.selectedIndex]['dataSet'].forEach(ele => { ele.maximize = false; });
    item.maximize = (type == 'Y') ? true : false;
    this.maximizeGrid = (type == 'Y') ? true : false;
    if (!isNullOrUndefined(item.data.objectType) && item.data.objectType == "C") {
      // if(!isNullOrUndefined(item.data.chartDataJson.chart) && !isNullOrUndefined(item.data.chartDataJson.chart.pieRadius) &&
      //  item.data.chartDataJson.chart.pieRadius != ''){
      //   item.data.chartDataJson.chart.pieRadius = item.maximize ? item.data.chartDataJson.chart.pieRadius * 2 :
      //   item.data.chartDataJson.chart.pieRadius / 2;
      // }
      item.data.showLoader = true;
      setTimeout(() => {
        item.data.showLoader = false;
      }, 1);
    }
  }

  onDropGrid(event, index) {
    // if (this.templateDetails[this.selectedIndex].dataSet[index]['data']['objectType'] == undefined) {
    // this.widgetList =[]
    // this.templateDetails[this.selectedIndex].dataSet[index].data = {};
    // event.data['filterCheckbox'] = false;
    // event.data['filterChecked'] = false;
    event.data.reportFilters.forEach((filter) => {
      this.filterDataSet(filter)
    })
    // if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'])) {
    //   this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'].forEach(sca => {
    //     if (sca.value == event.data['scalingFactor']) {
    //       sca.active = true;
    //     }
    //   })
    // }
    let selectedWid = event.data;
    selectedWid.reportFilters.forEach(ele => {
      if (ele.filterList) {
        selectedWid[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
          ele.filterChecked ? `'ALL'` :
            ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
          ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
      }
    })
    if (this.templateDetails[this.selectedIndex].dataSet[index]['type'] == 'tile') {
      if (event.data.objectType == 'T' || event.data.objectType == 'SC') {
        this.templateDetails[this.selectedIndex].dataSet[index].data = {};
        this.templateDetails[this.selectedIndex]['dataSet'][index]['reportId'] = event.data.reportId;
        this.templateDetails[this.selectedIndex]['dataSet'][index]['widgetTitle'] = event.data.reportTitle;
        this.templateDetails[this.selectedIndex].dataSet[index]['initialRequest'] = event.data;
        let reportReq = Object.assign(JSON.parse(JSON.stringify(selectedWid)), this.getPromptValues());
        reportReq['widgetTheme'] = this.selectedTheme;
        this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = true;
        this.setWidgetTrueFalse();
        reportReq['scalingFactor'] = "0";
        this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
          this.selectedWidgetList.push(event.data)
          this.selectedTempId = index;
          this.searchArray = [...this.widgetList]
          this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = false;
          this.templateDetails[this.selectedIndex].dataSet[index].otherInfo = resp['otherInfo'];
          if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {
            this.templateDetails[this.selectedIndex].dataSet[index].data = resp['response']
            this.parsingObject(resp['response'])
          }
          else {
            this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = resp['message']
          }
        })
      }
      else {
        this.globalService.showToastr.warning("Only Singleton widgets can be used in Singleton Tiles")
      }
    }
    else {
      if (event.data.objectType != 'T' && event.data.objectType != 'SC') {
        this.templateDetails[this.selectedIndex].dataSet[index].data = {};
        this.templateDetails[this.selectedIndex].dataSet[index]['initialRequest'] = event.data;
        this.templateDetails[this.selectedIndex]['dataSet'][index]['reportId'] = event.data.reportId;
        this.templateDetails[this.selectedIndex]['dataSet'][index]['widgetTitle'] = event.data.reportTitle;
        this.templateDetails[this.selectedIndex]['dataSet'][index]['reportPeriod'] = event.data.reportPeriod;
        this.templateDetails[this.selectedIndex]['dataSet'][index]['scalingFactor'] = "";
        if (event.data.reportDesignXml != '' && event.data.reportDesignXml != null) {
          //let reportDesignXml = JSON.parse(event.data.reportDesignXml.replaceAll("-","_"));
          let reportDesignXml = JSON.parse(event.data.reportDesignXml);
          let titleColor = '#' + reportDesignXml["TITLE-COLOR"];
          this.templateDetails[this.selectedIndex]['dataSet'][index]['titleColor'] = titleColor;
          this.titleColor = titleColor;
        }

        let reportReq = Object.assign(JSON.parse(JSON.stringify(selectedWid)), this.getPromptValues());
        reportReq['widgetTheme'] = this.selectedTheme;
        this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = true;
        this.setWidgetTrueFalse();
        reportReq['scalingFactor'] = "0";
        this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
          this.selectedWidgetList.push(event.data)
          this.selectedTempId = index;
          this.searchArray = [...this.widgetList]

          this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = false;
          this.templateDetails[this.selectedIndex].dataSet[index].otherInfo = resp['otherInfo'];
          if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {

            this.templateDetails[this.selectedIndex].dataSet[index].data = resp['response'];
            this.templateDetails[this.selectedIndex]['dataSet'][index]['scalingFactor'] = resp['response']['scalingFactor'];
            if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'])) {
              this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'].forEach(sca => {
                if (sca.value == resp['response']['scalingFactor']) {
                  sca.active = true;
                }
                else {
                  sca.active = false;
                }
              })
            }
            if (resp['response'].objectType == 'C') {

              if (!isNull(resp['response'].chartType) && resp['response'].chartType.search("FCMap_") > -1)
                resp['response'].chartType = resp['response'].chartType.replaceAll("FCMap_", "");

              if (resp['response'].chartType == 'HLinearGauge' || resp['response'].chartType == 'AngularGauge'
                || resp['response'].chartType == 'Kenya') {
                this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['chartDataJson'] =
                  FusionCharts.transcodeData(resp['response'].chartData, "xml", "json", false)
              }
              else {
                if (this.listLineType.includes(resp['response']['chartType'])) {
                  let data = resp['response']['chartData'];
                  var regexp = /<dataset/g;
                  let c = data.match(regexp) || [];
                  resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
                }
                if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
                  let data = resp['response']['chartData'];
                  var regexp = /<dataset/g;
                  let c = data.match(regexp) || [];
                  resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
                }
                this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['chartDataJson'] =
                  this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
              }


              if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['chartDataJson'].chart)) {
                this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartLeftMargin = 10;
                this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartTopMargin = 15;
                this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartRightMargin = 0;
                this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartBottomMargin = 0;
              }
            }
            if (resp['response'].objectType == 'G') {
              resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
              resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
              resp['response'].columnHeaderslstCopy.forEach(element => {
                element['sortLevel'] = 'noSort';
                element['sortReq'] = {};
              })
            }

          }
          else {
            this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = resp['message']
            // this.globalService.showToastr.error(resp['message'])
          }
        })
      }
      else {
        this.globalService.showToastr.warning("Singleton widget can be used only in Singleton Tiles")
      }
    }


    //}
  }

  plus(data) {
    if (this.tempType == 'fixed') {
      if (['chartGridCount', 'chartCount', 'gridCount'].includes(data)) {
        if (this.countCheck + this.fixedChartGridCount <= this.maxCountCheck && (this.gridCount + this.chartCount + this.chartGridCount < 3)) {
          this[data] = this[data] + 1;
          this.countCheck = this.countCheck + this.fixedChartGridCount;
        }
      }
      else if (['rowCount', 'columnCount'].includes(data)) {
        this.tilesCount = 0
        this.chartCount = 0
        this.chartGridCount = 0
        if (data == "rowCount" && this.rowCount < 3) {
          this[data] = this[data] + 1;
        }
        if (data == "columnCount" && this.columnCount < 6) {
          this[data] = this[data] + 1;
        }

        this.maxCountCheck = this.rowCount * this.columnCount
      }
      else if (['tilesCount'].includes(data)) {

        if (this.countCheck + 1 <= this.maxCountCheck) {
          this[data] = this[data] + 1;
          this.countCheck = this.countCheck + 1;
        }

      }
    }
    else {
      this[data] = this[data] + 1;
    }
  }

  minus(data) {
    if (this.tempType == 'fixed') {
      if (['chartGridCount', 'chartCount', 'gridCount'].includes(data)) {
        if (this.countCheck - this.fixedChartGridCount >= 0 && this[data] > 0) {
          this[data] = this[data] - 1;
          this.countCheck = this.countCheck - this.fixedChartGridCount;
        }
      }
      else if (['rowCount', 'columnCount'].includes(data)) {
        if (this[data] > 1) {
          this[data] = this[data] - 1;
          this.tilesCount = 0
          this.chartCount = 0
          this.chartGridCount = 0
          this.maxCountCheck = this.rowCount * this.columnCount
        }
      }
      if (data == 'tilesCount') {
        if (this.countCheck - 1 >= 0 && this[data] > 0) {
          this[data] = this[data] - 1;
          this.countCheck = this.countCheck - 1;
        }
      }
    } else {
      this[data] = this[data] - 1;
      this[data] = this[data] < 0 ? 0 : this[data];
    }
  }

  submit() {
    if (this.countCheck != 0) {
      let result = {
        type: 'template',
        data: this.addDetails()
      }
      this.resultData(result);
    }
  }

  addDetails() {
    let rsmCount = 0;
    let rbgCount = 0;
    if (this.countCheck < this.maxCountCheck && this.tempType == 'fixed') {
      let count = this.maxCountCheck - this.countCheck;
      if (count) {
        if (this.tilesCount < 6) {
          rsmCount = rsmCount + (6 - this.tilesCount);
          if ((this.gridCount + this.chartGridCount + this.chartCount) < 3) {
            rbgCount = rbgCount + (3 - (this.gridCount + this.chartGridCount + this.chartCount));
          }
        } else {
          let balTile = (this.maxCountCheck - this.tilesCount);
          let bgCount = Math.floor(balTile / 4);
          let bigTile = (bgCount - (this.gridCount + this.chartGridCount + this.chartCount));
          rbgCount = rbgCount + bigTile;
          rsmCount = (balTile - (rbgCount + this.gridCount + this.chartGridCount + this.chartCount) * 4);
        }
      }
    }
    let result = {
      config: GridsterConfigList[this.tempType],
      data: []
    }
    for (var i = 0; i < this.chartCount; i++) {
      (result.data).push(this.clone(config.chart));
    }
    for (var i = 0; i < this.gridCount; i++) {
      (result.data).push(this.clone(config.grid));
    }
    for (var i = 0; i < this.chartGridCount; i++) {
      (result.data).push(this.clone(config.chartGrid));
    }
    for (var i = 0; i < rbgCount; i++) {
      (result.data).push(this.clone(config.lg));
    }
    for (var i = 0; i < this.tilesCount; i++) {
      (result.data).push(this.clone(config.tiles));
    }
    for (var i = 0; i < rsmCount; i++) {
      (result.data).push(this.clone(config.tiles));
    }
    return result;
  }

  clone(data) {
    return JSON.parse(JSON.stringify(data))
  }

  onEvent = (event) => {
    if (event.eventName == "activate") {
      this.selectedNode = event.node.data
    }
    else {
      this.selectedNode = null
    }
  }
  replaceTabData(data, tree) {
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false,
      windowClass: "genericPopup"
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "Are you sure you want to replace the tab?";
    modelRef.componentInstance.popupType = 'Confirm';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.userConfirmation.subscribe(resp => {

      if (resp.type == 'Yes') {

        data['tabData'] = this.tempWidgetList[this.selectedIndex]
        tree.treeModel.update();
        this.globalService.showToastr.success('Dashboard tab replaced successfully')

      }
      else {

      }

      modelRef.close();

    });

  }

  addTabData(node, tree) {
    if (node.children.length <= 6) {
      const modelRef = this.modalService.open(GenericPopupComponent, {
        size: <any>'md',
        backdrop: false,
        windowClass: "genericPopup"
      });
      modelRef.componentInstance.title = "Alert";
      modelRef.componentInstance.message = "Are you sure you want to add the tab?";
      modelRef.componentInstance.popupType = 'Confirm';
      modelRef.componentInstance.closeTime = 'No';
      modelRef.componentInstance.userConfirmation.subscribe(resp => {

        if (resp.type == 'Yes') {

          let tabVal = 'Tab' + (node.children.length + 1)
          let data = {
            name: tabVal,
            tabData: this.tempWidgetList[this.selectedIndex]
          }
          this.nodes[0]['children'].forEach(element => {
            if (element) {
              if (element.id == node.id) {
                element.children.push(data)
              }
            }
          });
          tree.treeModel.update();
          this.globalService.showToastr.success('Dashboard tab added successfully')


        }
        else {

        }

        modelRef.close();

      });

    }

  }
  onUpdateData() {
    // this.tree.treeModel.expandAll();
    // if(this.selectedValue !=''){
    //   const node = this.tree.treeModel.getNodeById(this.selectedValue);
    //   if (node) {
    //     node.toggleActivated();
    //   }
    // }
  }
  // ngOnDestroy() {
  //   this.commonService.setdasTempCreate({});
  //   // this.subscription.unsubscribe();
  // }

  templateDesign() {
    //this.contentAreaHeight();
    this.sideFilterPosition();
    //this.dashboardContHgt();
    this.sideFilterHeight();
    const sideFilterHold = document.getElementById('side-filter-hold');
    if (sideFilterHold) {
      sideFilterHold.style.opacity = '1';
    }
  }
  getWidgetList() {
    this.apiService.get(environment.getWidgetMaster).subscribe(resp => {
      setTimeout(() => {
        this.templateDesign();
      }, 120)
      this.globalService.pageTitle = '';
      this.savedSettings = resp['response'][3];
      if (!isNullOrUndefined(resp['response'][0])) {
        this.widgetListCpy = JSON.parse(JSON.stringify(resp['response'][0]));
        this.templateDetails[this.selectedIndex]['templateSelected'] = true;
        this.selectedWidgetList = []
        this.widgetList = resp['response'][0];
        this.tempWidgetList = [...this.widgetList];
        this.searchArray = [...this.widgetList]
        this.widgetList.forEach((widget) => {
          widget.showWidget = true
          if (widget.objectType == 'T') {
            widget['title'] = "Singleton Widget"
          }
          else {
            widget['title'] = "Chart/Grid Widget"
          }
        });
        if (isNullOrUndefined(resp['response'][2]) || isNullOrUndefined(resp['response'][2][0]))
          this.selectedTemp(this.templates[4], 'template');
      }
      else {
        this.widgetList = [];
        this.searchArray = [...this.widgetList]
        this.tempWidgetList = []
        if (isNullOrUndefined(resp['response'][2]) || isNullOrUndefined(resp['response'][2][0]))
          this.selectedTemp(this.templates[4], 'template');
      }
      if (!isNullOrUndefined(resp['response'][1]) && resp['response'][1] != '') {
        this.templateDetails[this.selectedIndex]['templateSelected'] = true;
        let filterReq = {
          filterRefCode: resp['response'][1]
        }
        this.apiService.post(environment.getReportFilterList, filterReq).subscribe(filterResp => {
          if (!isNullOrUndefined(filterResp['response'])) {
            filterResp['response'].forEach(element => {
              if (element.filterType == 'DATE') {
                let dateRestrict = element.filterDateRestrict.split(',');
                let year = dateRestrict[0].split(':');
                let month = dateRestrict[1].split(':');
                let date = dateRestrict[2].split(':');
                let pastYear = year[0].slice(0, -1);
                let futureYear = year[1].slice(0, -1);
                let pastMonth = month[0].slice(0, -1);
                let futureMonth = month[1].slice(0, -1);
                let pastDate = date[0].slice(0, -1);
                let futureDate = date[1].slice(0, -1);
                let businessdates = JSON.parse(this.jwtService.get('businessDate'));
                let countrySplit = date[2] ? date[2].split("_") : [];
                if (countrySplit.length && countrySplit[1]) {
                  businessdates.forEach(dateObj => {
                    if (dateObj.COUNTRY == countrySplit[1]) {
                      let isVBD = countrySplit.includes("VBD") ? true : false;
                      let isVBM = countrySplit.includes("VBM") ? true : false;
                      let isVBW = countrySplit.includes("VBW") ? true : false;
                      let isVBQ = countrySplit.includes("VBQ") ? true : false;
                      let today = new Date();
                      let todayYear = today.getFullYear();
                      let todayMonth = today.getMonth();
                      let todayDay = today.getDate();
                      if (isVBD || isVBW || isVBM || isVBQ) {
                        let maxDate: string = isVBD ? dateObj['VBD'].toString().replaceAll("-", " ") :
                          isVBW ? dateObj['VBW'].toString().replaceAll("-", " ") :
                            isVBM ? dateObj['VBM'].toString().replaceAll("-", " ") :
                              isVBQ ? dateObj['VBQ'].toString().replaceAll("-", " ") : '';
                        isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                        let vbdDate = new Date(maxDate);
                        let vbdYear = vbdDate.getFullYear();
                        let vbdMonth = vbdDate.getMonth();
                        let vbdDay = vbdDate.getDate();
                        element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                          new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                        element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                          new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                      }
                      else {
                        element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                        element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                      }
                    }
                  });
                }
                else {
                  let isVBD = date.includes("VBD") ? true : false;
                  let isVBM = date.includes("VBM") ? true : false;
                  let isVBW = date.includes("VBW") ? true : false;
                  let isVBQ = date.includes("VBQ") ? true : false;
                  let today = new Date();
                  let todayYear = today.getFullYear();
                  let todayMonth = today.getMonth();
                  let todayDay = today.getDate();
                  if (isVBD || isVBW || isVBM || isVBQ) {
                    // let dates: any = Object.values(businessdates);
                    // let maxDate = Math.max(...dates.map(e => new Date(e.toString().replaceAll("-", " "))));
                    let maxDate: string = isVBD ? businessdates[0]['VBD'].toString().replaceAll("-", " ") :
                      isVBW ? businessdates[0]['VBW'].toString().replaceAll("-", " ") :
                        isVBM ? businessdates[0]['VBM'].toString().replaceAll("-", " ") :
                          isVBQ ? businessdates[0]['VBQ'].toString().replaceAll("-", " ") : '';
                    isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                    let vbdDate = new Date(maxDate);
                    let vbdYear = vbdDate.getFullYear();
                    let vbdMonth = vbdDate.getMonth();
                    let vbdDay = vbdDate.getDate();
                    element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                      new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                    element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                      new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                  }
                  else {
                    element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                    element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                  }
                }

              }
            });
            this.widgetFilterList = filterResp['response'];
            this.filterVal = this.groupBy(filterResp['response'], 'filterRow');
            this.tabBasedFilter();
            this.filterPrompt();
            setTimeout(() => {
              this.templateDesign();
            }, 120)
          }
          else {
            this.widgetFilterList = [];
          }
          if (!isNullOrUndefined(resp['response'][2])) {
            let tempIdList = [];
            let tempId = [];
            resp['response'][2].forEach((respTemp) => {
              this.templates.forEach((temp) => {
                if (!isNullOrUndefined(temp['templateId'])) {
                  if (respTemp['templateId'] == temp['templateId']) {
                    this.savedWidgetId = respTemp['savedWidgetId'];
                    this.selectedTemp(temp, 'template', 0);
                    tempIdList = respTemp['savedWidgetId'].split('!@#');
                    tempIdList.forEach((id) => {
                      tempId.push({ id: id.substring(2), index: parseInt(id.slice(0, 1)) - 1 })
                    });
                  }
                }
              })
            });
            this.widgetList.forEach((widget) => {
              tempId.forEach((obj) => {
                if (widget.reportId == obj.id) {
                  setTimeout(() => {
                    this.templateDetails[this.selectedIndex].dataSet[obj.index]['apiCall'] =
                      this.apiCall(widget, obj.index, widget, resp['response'][3]).subscribe(result => this.resultDataCall(result, widget, obj.index, 'retrieve', resp['response'][3]))
                  }, obj.index * 20)
                }
              })
            })
          }
        })
      }
      if (!isNullOrUndefined(resp['response'][2]) && !isNullOrUndefined(resp['response'][2][0])) {
        this.templateDetails[this.selectedIndex]['templateSelected'] = true;
        !isNullOrUndefined(resp['response'][2][0] && resp['response'][2][0]['widgetTheme']) &&
          resp['response'][2][0]['widgetTheme'] != '' ? this.themeSelect(resp['response'][2][0]['widgetTheme'], 'retrieve') :
          this.themeSelect("BLUE", 'retrieve');
        this.widgetBox = false;
        this.selectedWidgetList = []
        let widget = []
        let selectedWidget = resp['response'][2][0].savedWidgetId.split("!@#")
        selectedWidget.forEach((element, index) => {
          let value = index + 1 + '-'
          let obj = element.split(value)
          widget.push(obj[1])
        });
        this.widgetList.forEach(element1 => {
          widget.forEach(element2 => {
            if (element1.reportId == element2) {
              this.selectedWidgetList.push(element1)
            }
          })
        });
        this.setWidgetTrueFalse();
        this.searchArray = [...this.widgetList]

      }
    })

  }

  apiCall(item, index, widget, props?) {
    if (widget.reportDesignXml != '' && widget.reportDesignXml != null) {
      //let reportDesignXml = JSON.parse(data.data.reportDesignXml.replaceAll("-","_"));
      let reportDesignXml = JSON.parse(widget.reportDesignXml)
      let titleColor = '#' + reportDesignXml["TITLE-COLOR"];
      this.templateDetails[this.selectedIndex].dataSet[index]['titleColor'] = titleColor;
      this.titleColor = titleColor;
    }
    this.templateDetails[this.selectedIndex].dataSet[index].data = {};

    this.templateDetails[this.selectedIndex].dataSet[index]['initialRequest'] = widget;
    this.templateDetails[this.selectedIndex].dataSet[index]['showWidgetMoreOption'] = false;
    this.templateDetails[this.selectedIndex].dataSet[index]['showWidgetSortOption'] = false;
    this.templateDetails[this.selectedIndex].dataSet[index]['showWidgetFilterOption'] = false;
    this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = true;
    this.templateDetails[this.selectedIndex].dataSet[index]['reportId'] = widget.reportId
    this.templateDetails[this.selectedIndex].dataSet[index]['widgetTitle'] = widget.reportTitle
    this.templateDetails[this.selectedIndex].dataSet[index]['reportPeriod'] = widget.reportPeriod
    this.templateDetails[this.selectedIndex].dataSet[index]['scalingFactor'] = "";
    let scaleFlag = false;
    if (!isNullOrUndefined(props) && props.length) {
      props.forEach(sortCol => {
        if (sortCol.reportId == widget.reportId) {
          // this.templateDetails[this.selectedIndex].dataSet[index]['scalingFactor'] = sortCol.scalingFactor;
          item['scalingFactor'] = sortCol.scalingFactor;
          this.templateDetails[this.selectedIndex].dataSet[index]['scalingFactorDd'] = sortCol.scalingFactor;
          scaleFlag = true;
          // if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'])) {
          //   this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'].forEach(sca => {
          //     if (sca.value == sortCol.scalingFactor) {
          //       sca.active = true;
          //     }
          //     else {
          //       sca.active = false;
          //     }
          //   })
          // }
          // widget['filterCheckbox'] = false;
          if (!isNullOrUndefined(widget.reportFilters))
            widget.reportFilters.forEach((filter) => {
              if (!isNullOrUndefined(sortCol[`dataFilter${filter.filterSeq}`]) &&
                sortCol[`dataFilter${filter.filterSeq}`] != '') {
                if (filter.filterType == 'COMBO') {
                  let promptArr = []
                  Object.keys(filter.filterValueMap).forEach((element, index1) => {
                    promptArr.push(
                      {
                        id: element.split('@')[1],
                        description: filter.filterValueMap[element],
                        checked: false,
                        show: true
                      }
                    )
                  });
                  filter['filterList'] = promptArr;
                  if (filter.multiSelect == 'Y') {
                    let arr = sortCol[`dataFilter${filter.filterSeq}`].replaceAll("'", "").split(",");
                    if (arr.length && arr[0] == 'ALL') {
                      filter['filterChecked'] = true;
                      filter['filterList'].forEach((ele) => ele.checked = true)
                    }
                    else {
                      if (arr.length) {
                        arr.forEach(element => {
                          filter['filterList'].forEach(ele => {
                            if (element == ele.id) {
                              ele.checked = true
                            }
                          })
                        })
                        filter['filterChecked'] = filter['filterList'].every(item => item.checked)
                      }
                    }
                  }
                  else {
                    let arr = sortCol[`dataFilter${filter.filterSeq}`].replaceAll("'", "").split(",");
                    if (arr.length) {
                      if (arr[0] == 'ALL') {
                        filter['filterList'].forEach((ele) => ele.active = true)
                      }
                      else {
                        filter['filterList'].forEach(ele => {
                          if (arr[0] == ele.id) {
                            ele.active = true
                          }
                        })
                      }
                    }
                  }
                  // filter['filterChecked'] = false;
                }
              }
              else {
                // widget['filterCheckbox'] = false;
                if (!isNullOrUndefined(widget.reportFilters))
                  widget.reportFilters.forEach((filter) => {
                    this.filterDataSet(filter);
                  })
              }
            })
        }
      })
    }
    else {
      // widget['filterCheckbox'] = false;
      if (!isNullOrUndefined(widget.reportFilters))
        widget.reportFilters.forEach((filter) => {
          this.filterDataSet(filter);
        })
    }
    if (!isNullOrUndefined(item.reportFilters)) {
      item.reportFilters.forEach(ele => {
        if (ele.filterList) {
          item[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
            ele.filterChecked ? `'ALL'` :
              ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
            ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
        }
      })
    }
    item['widgetTheme'] = this.selectedTheme;
    let reportReq = Object.assign(JSON.parse(JSON.stringify(item)), this.getPromptValues());
    !scaleFlag ? reportReq['scalingFactor'] = "0" : '';
    return this.apiService.post(environment.getRMReport, reportReq);
  }


  resultDataCallForRefresh(resp, widget, index, type, props?) {
    if (type == "retrieve") {
      this.deleteWidget = true;
      this.deleteWidget
        ? (this.globalService.widget.enableDelete = true)
        : false;
    }
    this.selectedWidgetList.push(widget);
    this.selectedTempId = index;
    this.searchArray = [...this.widgetList];
    this.templateDetails[this.selectedIndex].dataSet[index].data["showLoader"] =
      false;
    this.templateDetails[this.selectedIndex].dataSet[index].otherInfo =
      resp["otherInfo"];
    this.setWidgetTrueFalse();
    if (resp["status"] == 1 && !isNullOrUndefined(resp["response"])) {
      this.templateDetails[this.selectedIndex].dataSet[index].data =
        resp["response"];

      this.templateDetails[this.selectedIndex].dataSet[index]["scalingFactor"] =
        resp["response"]["scalingFactor"];
      if (
        !isNullOrUndefined(
          this.templateDetails[this.selectedIndex].dataSet[index]["scalingList"]
        )
      ) {
        this.templateDetails[this.selectedIndex].dataSet[index][
          "scalingList"
        ].forEach((sca) => {
          if (sca.value == resp["response"].scalingFactor) {
            sca.active = true;
          } else {
            sca.active = false;
          }
        });
      }
      if (resp["response"].objectType == "C") {
        if (!isNullOrUndefined(props) && props.length) {
          props.forEach((prop) => {
            if (resp["response"].reportId == prop.reportId) {
              let chartType = "";
              !isNullOrUndefined(prop.chartType) && prop.chartType != ""
                ? (chartType = prop.chartType)
                : "";
              let list = [];
              if (
                !isNullOrUndefined(resp["response"].chartList) &&
                resp["response"].chartList.length
              ) {
                resp["response"].chartList.forEach((ele) => {
                  list.push({
                    name: Object.keys(ele)[0],
                    active: false,
                    description: Object.values(ele)[0],
                  });
                });
              }
              let filterdArr = list.filter((item) => item.name == chartType);
              resp["response"].chartType = filterdArr.length
                ? filterdArr[0].name
                : resp["response"].chartType;
            }
          });
        }
        if (
          !isNull(resp["response"].chartType) &&
          resp["response"].chartType.search("FCMap_") > -1
        )
          resp["response"].chartType = resp["response"].chartType.replaceAll(
            "FCMap_",
            ""
          );

        if (
          resp["response"].chartType == "HLinearGauge" ||
          resp["response"].chartType == "AngularGauge" ||
          resp["response"].chartType == "Kenya"
        ) {
          this.templateDetails[this.selectedIndex].dataSet[
            this.selectedTempId
          ].data["chartDataJson"] = FusionCharts.transcodeData(
            resp["response"].chartData,
            "xml",
            "json",
            false
          );
        } else {
          if (this.listLineType.includes(resp["response"]["chartType"])) {
            let data = resp["response"]["chartData"];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp["response"].chartData =
              this.replaceOccurrence(
                data,
                /<dataset/g,
                c.length,
                '<dataset renderas="line"'
              ) || "";
          }
          if (this.listLineTypeDY.includes(resp["response"]["chartType"])) {
            let data = resp["response"]["chartData"];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp["response"].chartData =
              this.replaceOccurrence(
                data,
                /<dataset/g,
                c.length,
                '<dataset renderas="line" parentyaxis="S" '
              ) || "";
          }
          this.templateDetails[this.selectedIndex].dataSet[
            this.selectedTempId
          ].data["chartDataJson"] = this.xmlToJSonService.conversionJson(
            resp["response"].chartData,
            resp["response"]
          );
        }
        if (
          !isNullOrUndefined(
            this.templateDetails[this.selectedIndex].dataSet[
              this.selectedTempId
            ].data["chartDataJson"].chart
          )
        ) {
          this.templateDetails[this.selectedIndex].dataSet[index].data[
            "chartDataJson"
          ].chart.chartLeftMargin = 10;
          this.templateDetails[this.selectedIndex].dataSet[index].data[
            "chartDataJson"
          ].chart.chartTopMargin = 15;
          this.templateDetails[this.selectedIndex].dataSet[index].data[
            "chartDataJson"
          ].chart.chartRightMargin = 0;
          this.templateDetails[this.selectedIndex].dataSet[index].data[
            "chartDataJson"
          ].chart.chartBottomMargin = 0;
        }
      }
      if (
        resp["response"].objectType == "T" ||
        resp["response"].objectType == "SC"
      ) {
        this.templateDetails[this.selectedIndex].dataSet[index].data =
          resp["response"];
        this.parsingObject(resp["response"]);
      }
      if (resp["response"].objectType == "G") {
        resp["response"].columnHeaderslstCopy = JSON.parse(
          JSON.stringify(resp["response"].columnHeaderslst)
        );
        resp["response"].columnHeaderslstCopy.forEach((header) => {
          header["sortLevel"] = "noSort";
          header["sortReq"] = {};
        });
        resp["response"].gridDataSetCopy = JSON.parse(
          JSON.stringify(resp["response"].gridDataSet)
        );
        if (!isNullOrUndefined(props) && props.length) {
          this.templateDetails[this.selectedIndex].dataSet.forEach((ele) => {
            props.forEach((sortCol) => {
              if (ele.data.reportId == sortCol.reportId) {
                let sortArr =
                  !isNullOrUndefined(sortCol.sortColumn) &&
                  sortCol.sortColumn != ""
                    ? sortCol.sortColumn.replace("ORDER BY ", "").split(",")
                    : [];
                sortArr.forEach((element) => {
                  resp["response"].columnHeaderslstCopy.forEach((head) => {
                    let arr = element.split(" ");
                    if (arr.length && head.dbColumnName == arr[0]) {
                      head["sortLevel"] = arr[1].toLowerCase();
                      head["sortReq"] = {};
                      head["sortReq"][`${arr[0]}`] = arr[1].toLowerCase();
                    }
                  });
                });
              }
            });
          });
        }
        let sortReq = {};
        resp["response"].columnHeaderslstCopy.forEach((sortObj) => {
          sortReq = Object.assign(sortReq, sortObj["sortReq"]);
        });
        this.templateDetails[this.selectedIndex].dataSet[index]["sortReq"] =
          sortReq;
        this.templateDetails[this.selectedIndex].dataSet[index][
          "columnHeaderslstCopy"
        ] = resp["response"].columnHeaderslstCopy;
        if (Object.keys(sortReq).length) {
          resp["response"].gridDataSet = this.gpService.orderBy(
            sortReq,
            resp["response"].gridDataSet,
            resp["response"].columnHeaderslstCopy
          );
        } else {
          resp["response"].gridDataSet = JSON.parse(
            JSON.stringify(resp["response"].gridDataSetCopy)
          );
        }
      }
    } else {
      this.templateDetails[this.selectedIndex].dataSet[index].data[
        "errorMessage"
      ] = resp["message"];
    }
  }


  resultDataCall(resp, widget, index, type, props?) {
    if (type == 'retrieve') {
      this.deleteWidget = true;
      this.deleteWidget ? this.globalService.widget.enableDelete = true : false
    }
    this.selectedWidgetList.push(widget)
    this.selectedTempId = index;
    this.searchArray = [...this.widgetList]
    this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = false;
    this.templateDetails[this.selectedIndex].dataSet[index].otherInfo = resp['otherInfo'];
    this.setWidgetTrueFalse();
    if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {


      this.templateDetails[this.selectedIndex].dataSet[index].data = resp['response'];


      //       if (resp["response"]["autoRefreshFlag"] == "Y") {
      //   this.templateDetails[this.selectedIndex].dataSet[index]["intervaelRequest"] = interval(1000 * parseInt(this.autoRefreshTime)).subscribe((x) => {

      //     this.apiCall(widget, index, widget, props).subscribe((result) =>
      //       this.resultDataCallForRefresh(
      //         result,
      //         widget,
      //         index,
      //         "",
      //         this.savedSettings
      //       )
      //     );
      //   });
      // } else {
      //   if (
      //     this.templateDetails[this.selectedIndex].dataSet[index]["intervaelRequest"]
      //   ) {
      //     this.templateDetails[this.selectedIndex].dataSet[index]["intervaelRequest"].unsubscribe();
      //   }
      // }
      this.templateDetails[this.selectedIndex].dataSet[index]['scalingFactor'] = resp['response']['scalingFactor'];
      if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'])) {
        this.templateDetails[this.selectedIndex].dataSet[index]['scalingList'].forEach(sca => {
          if (sca.value == resp['response'].scalingFactor) {
            sca.active = true;
          }
          else {
            sca.active = false;
          }
        })
      }
      if (resp['response'].objectType == 'C') {
        if (!isNullOrUndefined(props) && props.length) {
          props.forEach(prop => {
            if (resp['response'].reportId == prop.reportId) {
              let chartType = '';
              !isNullOrUndefined(prop.chartType) && prop.chartType != '' ? chartType = prop.chartType : '';
              let list = [];
              if (!isNullOrUndefined(resp['response'].chartList) && resp['response'].chartList.length) {
                resp['response'].chartList.forEach(ele => {
                  list.push({ name: Object.keys(ele)[0], active: false, description: Object.values(ele)[0] })
                })
              }
              let filterdArr = list.filter(item => item.name == chartType);
              resp['response'].chartType = filterdArr.length ? filterdArr[0].name : resp['response'].chartType;
            }
          })
        }
        if (!isNull(resp['response'].chartType) && resp['response'].chartType.search("FCMap_") > -1)
          resp['response'].chartType = resp['response'].chartType.replaceAll("FCMap_", "");

        if (resp['response'].chartType == 'HLinearGauge' || resp['response'].chartType == 'AngularGauge'
          || resp['response'].chartType == 'Kenya') {
          this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['chartDataJson'] =
            FusionCharts.transcodeData(resp['response'].chartData, "xml", "json", false)
        }
        else {
          if (this.listLineType.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
          }
          if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
          }
          this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['chartDataJson'] =
            this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
        }
        if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[this.selectedTempId].data['chartDataJson'].chart)) {
          this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartLeftMargin = 10;
          this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartTopMargin = 15;
          this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartRightMargin = 0;
          this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartBottomMargin = 0;
        }
      }
      if (resp['response'].objectType == 'T' || resp['response'].objectType == 'SC') {
        this.templateDetails[this.selectedIndex].dataSet[index].data = resp['response']
        this.parsingObject(resp['response'])
      }
      if (resp['response'].objectType == 'G') {
        resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
        resp['response'].columnHeaderslstCopy.forEach(header => {
          header['sortLevel'] = 'noSort';
          header['sortReq'] = {};
        })
        resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
        if (!isNullOrUndefined(props) && props.length) {
          this.templateDetails[this.selectedIndex].dataSet.forEach(ele => {
            props.forEach(sortCol => {
              if (ele.data.reportId == sortCol.reportId) {
                let sortArr = (!isNullOrUndefined(sortCol.sortColumn) && sortCol.sortColumn != '') ?
                  sortCol.sortColumn.replace("ORDER BY ", "").split(",") : [];
                sortArr.forEach(element => {
                  resp['response'].columnHeaderslstCopy.forEach(head => {
                    let arr = element.split(' ')
                    if (arr.length && head.dbColumnName == arr[0]) {
                      head['sortLevel'] = arr[1].toLowerCase();
                      head['sortReq'] = {}
                      head['sortReq'][`${arr[0]}`] = arr[1].toLowerCase();
                    }
                  })
                })
              }
            })
          })
        }
        let sortReq = {};
        resp['response'].columnHeaderslstCopy.forEach(sortObj => {
          sortReq = Object.assign(sortReq, sortObj['sortReq'])
        })
        this.templateDetails[this.selectedIndex].dataSet[index]['sortReq'] = sortReq;
        this.templateDetails[this.selectedIndex].dataSet[index]['columnHeaderslstCopy'] = resp['response'].columnHeaderslstCopy
        if (Object.keys(sortReq).length) {
          resp['response'].gridDataSet =
            this.gpService.orderBy(sortReq, resp['response'].gridDataSet, resp['response'].columnHeaderslstCopy);
        }
        else {
          resp['response'].gridDataSet = JSON.parse(JSON.stringify(resp['response'].gridDataSetCopy))
        }
      }
    }
    else {
      this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = resp['message']
    }
  }

  filterDataSet(filter) {
    if (filter.filterType == 'COMBO') {
      let promptArr = []
      Object.keys(filter.filterValueMap).forEach((element, index1) => {
        promptArr.push(
          {
            id: element.split('@')[1],
            description: filter.filterValueMap[element],
            checked: false,
            show: true
          }
        )
      });
      filter['filterList'] = promptArr;
      if (filter.multiSelect == 'Y') {
        let arr = Object.keys(filter.filterDefaultValue);
        if (arr.length) {
          if (arr[0] == 'ALL') {
            filter['filterChecked'] = true;
            filter['filterList'].forEach((ele) => ele.checked = true)
          }
          else {
            filter['filterList'].forEach(ele => {
              if (arr[0] == ele.id) {
                ele.checked = true
              }
            })
            filter['filterChecked'] = filter['filterList'].every(item => item.checked)
          }
        }
      }
      else {
        let arr = Object.keys(filter.filterDefaultValue);
        if (arr.length) {
          if (arr[0] == 'ALL') {
            filter['filterList'].forEach((ele) => ele.active = true)
          }
          else {
            filter['filterList'].forEach(ele => {
              if (arr[0] == ele.id) {
                ele.active = true
              }
            })
          }
        }
      }
      // filter['filterChecked'] = false;
    }
  }

  // On window resize
  @HostListener('window: resize') adjustContents() {
    this.templateDetails[this.selectedIndex]['dataSet'].forEach((ele, index) => {
      if (ele['data']['objectType'] && ele['data']['objectType'] == 'C')
        ele['data']['showLoader'] = true;
    });
    setTimeout(() => {
      this.templateDetails[this.selectedIndex]['dataSet'].forEach((ele, index) => {
        if (ele['data']['objectType'] && ele['data']['objectType'] == 'C')
          ele['data']['showLoader'] = false;
      });
      let obj = this.templateDetails[this.selectedIndex]['templatesDetal'];
      if (!isUndefined(obj) && (!isUndefined(obj.api))) {
        this.templateDetails[this.selectedIndex]['templatesDetal'].api.resize();
      }
      //this.contentAreaHeight();
      this.sideFilterPosition();
      //this.dashboardContHgt();
      this.sideFilterHeight();
    }, 300);
  }

  filterPrompt() {
    let pushData = {}
    this.widgetFilterList.forEach((element, objIndex) => {
      let tempArr = [];
      const isObjEmpty = Object.entries(element.filterSourceVal).length === 0 && element.filterSourceVal.constructor === Object;
      if (isObjEmpty) {
        this.overallPromptValues[element.filterSeq] = tempArr;
      } else {
        const data = element.filterSourceVal;
        Object.keys(data).forEach(ele => {
          tempArr.push({ id: ele, description: data[ele] });
        })
        this.overallPromptValues[element.filterSeq] = tempArr;
      }

      if (element.filterType == 'COMBO') {
        if (Object.keys(element['filterDefaultValue']).length) {
          const dataDefault = element['filterDefaultValue'];
          let tempDefault = [];
          Object.keys(dataDefault).forEach(ele => {
            tempDefault.push({ id: ele, description: dataDefault[ele] });
          })
          pushData[`filter${element.filterSeq}Val`] = new FormControl(tempDefault, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl([], Validators.required);
        }
      }
      if (element.filterType == 'DATE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          // let date = element['filterDefaultValue'].split('-');
          let dateVal = Object.keys(element['filterDefaultValue'])[0].split("-").join("/");
          if (element.filterDateFormat == "Mon-YYYY") {
            dateVal = `01 ${dateVal}`
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          //pushData[`filter${element.filterSeq}Val`] = new FormControl({ year: +date[2], month: this.numWithMonth[date[1]], day: +date[0] }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }

      if (element.filterType == 'DATERANGE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          // let date = element['filterDefaultValue'].split('-');
          let dateVal = Object.keys(element['filterDefaultValue'])[0].split("-").join("/");
          if (element.filterDateFormat == "Mon-YYYY") {
            dateVal = `01 ${dateVal}`
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          // pushData[`filter${element.filterSeq}Val`] = new FormControl({ year: +date[2], month: this.numWithMonth[date[1]], day: +date[0] }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }
      if (!['COMBO', 'DATE'].includes(element.filterType)) {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.keys(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTM') {
        if (Object.keys(element['filterDefaultValue']).length) {
          this.magnifierId[`filter${element.filterSeq}Val`] = Object.keys(element['filterDefaultValue']).join(",");
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.values(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          this.magnifierId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TREE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          this.treeId[`filter${element.filterSeq}Val`] = Object.keys(element['filterDefaultValue']).join(",");
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.values(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          this.treeId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXT') {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.keys(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTD') {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: Object.keys(element['filterDefaultValue']).join(","), disabled: true }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: "", disabled: true }, Validators.required);
        }
      }
    });
    this.filterForm = new FormGroup(pushData);
  }

  groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  getObjectValue(data) {
    return Object.values(data);
  }

  // Apply sidebar filter
  applyFilter = () => {
    this.templateDetails[this.selectedIndex].dataSet.forEach((element1, wIndex2) => {
      if (!isNullOrUndefined(element1.reportId)) {
        this.widgetList.forEach((widget) => {
          if (element1.reportId == widget.reportId) {
            setTimeout(() => {
              this.templateDetails[this.selectedIndex].dataSet[element1.id - 1]['apiCall'] =
                this.apiCall(widget, element1.id - 1, widget).subscribe(result => this.resultDataCall(result, widget, element1.id - 1, '', this.savedSettings))
            }, element1.id - 1 * 20)
          }
        })
      }
    })
    this.toggleSideFilter('filter');
  }

  getPromptValues() {
    if (this.widgetFilterList && this.widgetFilterList.length) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      values.forEach(element => {
        this.widgetFilterList.forEach(element1 => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO' && element1.filterToshow) {
              if (element1["multiSelect"] == 'Y') {
                this.promptValues[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                }
              }
            }
            if (element1['filterType'] == 'DATE' && element1.filterToshow) {
              let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
            }
            if (element1['filterType'] == 'TEXTD' && element1.filterToshow) {
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType']) && element1.filterToshow) {
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1['filterType'] == 'TEXTM' && element1.filterToshow) {
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
              // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1['filterType'] == 'TREE' && element1.filterToshow) {
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
              // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1['filterType'] == 'TEXT' && element1.filterToshow) {
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
          }
        });
      });
      return this.promptValues;
    }
    else {
      return {};
    }
  }

  cancelFilter = (event) => {
    this.toggleSideFilter('filter');
  }

  // Toggle sidefilter
  toggleSideFilter = (type) => {
    // this.toggleSideWidget()
    if (type == 'filter') {
      this.filterBox = false;
      this.widgetBox = false;
      this.settingBox = false;
      this.templateBox = false;
    }
    else if (type == 'widget') {
      this.widgetBox = true;
      this.filterBox = false;
      this.settingBox = false;
      this.templateBox = false;
    }
    else if (type == 'setting') {
      this.settingBox = true;
      this.widgetBox = false;
      this.filterBox = false;
      this.templateBox = false;
    }
    else if (type == 'template') {
      this.templateBox = true;
      this.settingBox = false;
      this.widgetBox = false;
      this.filterBox = false;
    }

    const sideFilter = document.getElementById('side-filter-hold');
    // const sideArrowClass = document.getElementById('sidebarArrow');
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    if (sideFilter.classList.contains('setRight')) {
      this.filterOpened = false;
      this.widgetOpened = false;
      this.settingOpened = false;
      this.templateOpened = false;
      type == 'filter' ? sideFilterOverlay.classList.remove('show') : '';
      sideFilter.classList.remove('setRight');
      sideFilter.classList.remove("col-sm-8", "col-md-4", "col-lg-4", "col-xl-4");
      // sideArrowClass.classList.remove('fa-angle-double-right');
      this.position = { x: 0, y: 0 };
    } else {
      type == 'filter' ? this.filterOpened = true : type == 'widget' ? this.widgetOpened = true :
        type == 'setting' ? this.settingOpened = true : type == 'template' ? this.templateOpened = true : ''
      type == 'filter' ? sideFilterOverlay.classList.add('show') : '';
      sideFilter.classList.add('setRight');
      sideFilter.classList.add("col-sm-8", "col-md-4", "col-lg-4", "col-xl-4");
      setTimeout(() => {
        const sideArrow = document.getElementById('sidebarArrow');
        if (!isNullOrUndefined(sideArrow))
          sideArrow.classList.add('fa-angle-double-right');
      }, 100)
      this.previousOpened = type;
    }
  }

  sidebarOpen(type) {
    if (type == 'filter') {
      if (this.tempWidgetList && this.tempWidgetList.length) {
        const sideFilter = document.getElementById('side-filter-hold');
        const sideArrowClass = document.getElementById('sidebarArrow');
        if (sideFilter.classList.contains('setRight')) {
          this.filterOpened = false;
          this.widgetOpened = false;
          this.settingOpened = false;
          this.templateOpened = false;
          sideFilter.classList.remove('setRight');
          sideArrowClass.classList.remove('fa-angle-double-right');
          if (this.previousOpened != type) {
            setTimeout(() => {
              this.toggleSideFilter(type);
            }, 400)
          }
          else {
            this.toggleSideFilter(type);

          }

        }
        else {
          this.toggleSideFilter(type);
        }
      }
    }
    else {
      const sideFilter = document.getElementById('side-filter-hold');
      const sideArrowClass = document.getElementById('sidebarArrow');
      if (sideFilter.classList.contains('setRight')) {
        this.filterOpened = false;
        this.widgetOpened = false;
        this.settingOpened = false;
        this.templateOpened = false;
        sideFilter.classList.remove('setRight');
        sideArrowClass.classList.remove('fa-angle-double-right');
        if (this.previousOpened != type) {
          setTimeout(() => {
            this.toggleSideFilter(type);
          }, 400)
        }
        else {
          this.toggleSideFilter(type);

        }

      }
      else {
        this.toggleSideFilter(type);
      }
    }
  }

  // Set sidefilter height
  sideFilterHeight() {
    setTimeout(() => {
      const dashboardHoldHeight = window.innerHeight;
      const filterHeaderHeight = document.getElementById('filterHeader') ? +document.getElementById('filterHeader').offsetHeight : null;
      const filterFooterHeight = document.getElementById('filterFooter') ? +document.getElementById('filterFooter').offsetHeight : null;
      if (dashboardHoldHeight && filterHeaderHeight && filterFooterHeight) {
        document.getElementById('side-filter-hold').style.height = `${dashboardHoldHeight - 80}px`;
        const setFilterBodyHeight = dashboardHoldHeight - (filterHeaderHeight + filterFooterHeight);
        document.getElementById('filterBody').style.cssText = `height: ${setFilterBodyHeight}px;
                                                              max-height: ${setFilterBodyHeight}px;
                                                              margin-bottom: ${filterFooterHeight}px`;

      }
    }, 180);
  }

  // Get sidefilter position from sidebar width using css variable and update sidebar right position
  // Update sidefilter css right position value from css variable
  sideFilterPosition() {
    const sideFilterHold = document.getElementById('side-filter-hold');
    if (sideFilterHold) {
      const filterHoldWidth = document.getElementById('side-filter-hold').offsetWidth;
      sideFilterHold.style.setProperty('--filterWidth', `-${filterHoldWidth}px`);
    }
  }

  // Filter by year || month || date in sidefilter
  fitterBy = (obj) => {
    const filterDataBy = document.getElementsByClassName('filterDataBy');
    Array.from(filterDataBy).forEach((elem) => {
      elem.classList.remove('show');
    });
    if (obj === 'Y') {
      document.getElementById('yearWise').classList.add('show');
    } else if (obj === 'M') {
      document.getElementById('monthWise').classList.add('show');
    } else {
      document.getElementById('dateWise').classList.add('show');
    }
  }

  arraySrtingCon(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`'${element["id"]}'`)
    });
    return arr.toString();
  }

  dateReConversion(date, format) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",][myDate.getMonth()];
      if (format == 'YYYY') {
        str = myDate.getFullYear();
      }
      else if (format == 'Mon-YYYY') {
        str = month + "-" + myDate.getFullYear();
      }
      else {
        str = myDate.getDate() + "-" + month + "-" + myDate.getFullYear();
      }

      return str;
    }
    else {
      return "";
    }
  }

  checkWidth(data) {
    let reqData = [];
    data.forEach(element => {
      if (element.filterToshow) {
        reqData.push(element)
      }
    });
    return {
      width: (100 / reqData.length) + '%'
    };
  }

  tabBasedFilter() {
    // const ind = this.tabIndex + 1;
    const ind = 1;
    for (let i = 0; i <= this.widgetFilterList.length - 1; i++) {
      let item = this.widgetFilterList[i];
      if (isNullOrUndefined(item['specificTab']) || (item['specificTab'] == '') || item['specificTab'] == 'ALL') {
        item['filterToshow'] = true;
      } else {
        let arr = item['specificTab'].split(',');
        if (arr.includes(`${ind}`)) {
          item['filterToshow'] = true;
        } else {
          item['filterToshow'] = false;
        }
      }
    }
  }

  promptValuesReturn(index) {
    return `filter${index}Val`;
  }

  promptValueOption(seq) {
    return this.overallPromptValues[seq]
  }

  dropdownCheck(data, event, type?) {
    if (data.dependencyFlag == 'Y') {
      let magnifier = 0;
      let dropdownFlag = false;
      let dataValue;
      let dependentPrompt = data.dependentPrompt.split(",")
      dependentPrompt.forEach((element1, index1) => {
        this.widgetFilterList.forEach((element, index) => {
          if (element.filterSeq == element1) {
            dataValue = this.filterForm.getRawValue()[`filter${element.filterSeq}Val`];
            if (element.filterType == 'DATE') {
              let val = this.dateReConversion(dataValue, element.filterDateFormat);
              dataValue = val;
              data[`filter${element1}Val`] = `'${val}'`;
            }
            if (dataValue.length) {
              if (Array.isArray(dataValue)) {
                if (!isNullOrUndefined(dataValue[0].id) && dataValue[0].id != '' && dataValue[0].id != 'ALL') {
                  data[`filter${element1}Val`] = this.arraySrtingCon(dataValue);
                  if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
                    dropdownFlag = true;
                  }
                }
                else {
                  magnifier++
                  this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
                }
              }
            }
            else {
              magnifier++
              Object.keys(this.filterVal).forEach(element => {
                this.filterVal[element].forEach(element1 => {
                  if (element1.filterSeq == data.filterSeq) {
                    this.overallPromptValues[data.filterSeq] = [];
                  }
                });
              });
              this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
            }
          }
        });
      });
      if (type == 'TEXTM' && magnifier == 0) {
        this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
      }
      if (type == 'TREE' && magnifier == 0) {
        this.openFilterTreePopup(data, event);
      }
      if (dropdownFlag && dataValue.length) {
        data['defaultValueId'] = '';
        this.apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
          let promptArr = []
          Object.keys(resp['response']).forEach((element, index1) => {
            promptArr.push(
              {
                id: element.split('@')[1],
                description: resp['response'][element]
              }
            )
          });
          Object.keys(this.filterVal).forEach(element => {
            this.filterVal[element].forEach(element1 => {
              if (element1.filterSeq == data.filterSeq) {
                this.overallPromptValues[data.filterSeq] = promptArr;
              }
            });
          });
        });
      }
    }
    else {
      if (data.filterType == "COMBO" || data.filterType == "TEXTD") {
        if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
          data['defaultValueId'] = '';
          this.apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
            let promptArr = []
            Object.keys(resp['response']).forEach((element, index1) => {
              promptArr.push(
                {
                  id: element.split('@')[1],
                  description: resp['response'][element]
                }
              )
            });
            Object.keys(this.filterVal).forEach(element => {
              this.filterVal[element].forEach(element1 => {
                if (element1.filterSeq == data.filterSeq) {
                  this.overallPromptValues[data.filterSeq] = promptArr;
                }
              });
            });
          });
        }
      }
      else {
        if (data.filterType == "TEXTM") {
          this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
        }
        if (data.filterType == 'TREE') {
          this.openFilterTreePopup(data, event);
        }
      }

    }
  }

  openCustomerMagnifierPopup(ind, event, title1) {
    event.srcElement.blur();
    event.preventDefault();
    let query;
    let tilte;
    Object.keys(this.filterVal).forEach(element => {
      this.filterVal[element].forEach(element1 => {
        if (element1.filterSeq == ind) {
          query = element1['filterSourceId'];
          tilte = element1['filterLabel'];
        }
      });
    });

    let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.widgetFilterList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              promptM[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              promptM[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    promptM['query'] = query;
    const modelRef = this.modalService.open(MagnifierComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    modelRef.componentInstance.query = promptM;
    modelRef.componentInstance.title = title1;
    modelRef.componentInstance.filterData.subscribe((e) => {
      //let filteSeq = this.tablist[this.selectedReportId]["tabFilterValue"][ind]['filterSeq'];
      let str = `filter${ind}Val`;
      this.filterForm.patchValue({
        [str]: e.columnTen
      })
      this.magnifierId[str] = e.columnNine;
      //this.tablist[this.selectedReportId]["tabFilterSelectedValue"][ind] = e.columnNine;
      modelRef.close();
    });
  }


  openFilterTreePopup(data, event) {
    event.srcElement.blur();
    event.preventDefault();
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.widgetFilterList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              data[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              data[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    const modelRef = this.modalService.open(GenericTreeComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    document.body.classList.add('tree-popup');
    modelRef.componentInstance.title = data.filterLabel;
    modelRef.componentInstance.query = data;
    modelRef.componentInstance.selectedValue = this.treeId[`filter${data.filterSeq}Val`]
    modelRef.componentInstance.filterData.subscribe((e) => {
      let str = `filter${data.filterSeq}Val`;
      this.filterForm.patchValue({
        [str]: e.field2
      })
      this.treeId[str] = e.field1;
      document.body.classList.remove('tree-popup');
      modelRef.close();
    });
  }

  setEmptyDepent(data) {
    let filterPrompt = this.widgetFilterList;
    filterPrompt.forEach(element => {
      if (element.dependencyFlag == 'Y') {
        let dependentPrompt = element.dependentPrompt.split(",");
        let filterSeq = data.filterSeq.toString();
        if (dependentPrompt.includes(filterSeq)) {
          let str = `filter${element.filterSeq}Val`;
          if (element.filterType == 'COMBO') {
            this.filterForm.patchValue({
              [str]: []
            })
          }
          else {
            this.filterForm.patchValue({
              [str]: ''
            })
          }

        }
      }
    });
  }

  filterSubmit() {
    this.widgetFilterList.forEach(element => {
      if (!element.filterToshow) {
        this.promptArray()[`filter${element.filterSeq}Val`].setErrors(null)
      }
    });
    if (this.filterForm.invalid) {
      this.filterSubmitted = true
    }
    else {
      let tabFilterValue = this.widgetFilterList;
      let dateArr = tabFilterValue.filter((item) => item.filterType == 'DATE');
      if (dateArr.length) {
        dateArr.forEach(element => {
          if (element.dependencyFlag == 'Y') {
            this.compareTwoDates(element.dependentPrompt, element.filterSeq);
          }
        });
        if (this.filterForm.valid) {
          this.applyFilter();
        }
      }
      else {
        this.applyFilter();
      }
    }
  }

  compareTwoDates(startIndex, endIndex) {
    if (new Date(this.filterForm.controls[`filter${endIndex}Val`].value) < new Date(this.filterForm.controls[`filter${startIndex}Val`].value)) {
      this.filterForm.controls[`filter${endIndex}Val`].setErrors({ 'endDate': true });
      return false
    }
  }

  promptArray() { return this.filterForm.controls; }

  drillDownReq(event, item, rowData, i) {
    if (item.data.ddFlag == 'Y') {
      this.drillDownList = [];
      this.drillDownKeys = {};
      let tmpStr = `drillDownKey${1}`
      let ddkey;
      this.drillDownLoader = true;
      let ddData;
      this.tableValues = item.initialRequest;
      if (this.tableValues.reportDesignXml != '' && this.tableValues.reportDesignXml != null) {
        //this.reportDesignXml = JSON.parse(this.reportDesignXml);
        let tempJson = JSON.parse(this.tableValues.reportDesignXml);
        this.reportDesignXml = tempJson;
        if ((item['data']['objectType'] != 'T' && item['data']['objectType'] != 'SC') && this.reportDesignXml.BODY["ROW-BG-COLOR"] != '') {
          this.altRowBgColor.push('#' + this.reportDesignXml.BODY["ROW-BG-COLOR"].split(",")[0]);
          this.altRowBgColor.push('#' + this.reportDesignXml.BODY["ROW-BG-COLOR"].split(",")[1]);
        }
      }
      // if (isNullOrUndefined(this.drillDownKeys[tmpStr])) {
      // }
      // else {
      //   this.drillDownKeys[tmpStr] = this.drillDownKeys[tmpStr].slice(0, -1)
      //   this.drillDownKeys[tmpStr] += `!@#${ddkey}'`;
      // }
      if (item['data']['objectType'] == 'G') {
        ddkey = rowData['DDKEYID'];
        this.popoverOpenDrillDown(event, 'G');
        let breadCrumb, breadCrumbArr;
        breadCrumbArr = item.data['columnHeaderslst'].filter(items => items.drillDownLabel);
        if (breadCrumbArr.length) {
          // breadCrumbLabel = !isNullOrUndefined(breadCrumbArr[0].caption) && breadCrumbArr[0].caption != '' ? breadCrumbArr[0].caption : 'DDKEYID';
          breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
        }
        else {
          breadCrumb = 'DDKEYID';
        }
        ddData = {
          tabId: rowData[breadCrumb],
          tabLabel: breadCrumbArr[0].caption,
          tabName: rowData[breadCrumb],
          index: i,
          currentLevelFlag: true,
          titleColor: this.titleColor,
          ddObjectType: item['data']['objectType']
        }
        this.drillDownHit(item, ddkey, ddData, i)
      }
      else if (item['data']['objectType'] == 'T' || item['data']['objectType'] == 'SC') {
        ddkey = item['data']['endResult']['DRILLDOWN_KEY']['value'];
        this.popoverOpenDrillDown(event, 'G');
        ddData = {
          tabId: ddkey,
          tabLabel: ddkey,
          tabName: ddkey,
          index: i,
          currentLevelFlag: true,
          titleColor: this.titleColor,
          ddObjectType: item['data']['objectType']
        }
        this.drillDownHit(item, ddkey, ddData, i)
      }
      else {
        this.popoverOpenDrillDown(document.getElementById('widgetPopId' + item.id), 'C')
        let dataIndex = event.dataObj.dataIndex;
        this.zone.run(() => {
          if (isNullOrUndefined(item['data']['chartDataJson']['dataset'])) {
            if (!isNullOrUndefined(item['data']['chartDataJson']['category'])) {
              item['data']['chartDataJson']['category'].forEach(element => {
                element['category'].forEach((ele) => {
                  ele['category'].forEach(e => {
                    if (e.label == event.dataObj.label) {
                      ddkey = e.ddkey;
                    }
                  })
                })
              })
            }
            else {
              ddkey = item['data']['chartDataJson']['data'][dataIndex].ddkey
            }
          }
          else {
            let datasetIndex = event.dataObj.datasetIndex;
            ddkey = item['data']['chartDataJson']['dataset'][datasetIndex]["data"][dataIndex].ddkey
          }
          ddData = {
            tabId: '',
            tabLabel: item['data']['ddKeyPromptLabel'] != undefined && item['data']['ddKeyPromptLabel'] != null ?
            item['data']['ddKeyPromptLabel'] : '',
            tabName: ddkey,
            index: i,
            currentLevelFlag: true,
            titleColor: this.titleColor,
            ddObjectType: item['data']['objectType']
          }
          let bubbleTitle = '';
          if (!isNullOrUndefined(event)) {
            if (item.data.chartType == 'Bubble') {
              if (!isNullOrUndefined(item.data.chartDataJson['categories'])) {
                item.data.chartDataJson['categories'][0]['category'].forEach(element => {
                  if (element.x == event.dataObj.x.toString()) {
                    bubbleTitle = element.label;
                  }
                })
              }
              ddData['tabId'] = bubbleTitle != '' ? bubbleTitle : '';
            }
            else {
              ddData['tabId'] = event.dataObj.datasetName != '' ?
                `${event.dataObj.datasetName}, ${event.dataObj.categoryLabel}` : '';
              ddData['tabId'] = ddData['tabId'].replaceAll("undefined,", "");
              ddData['tabName'] = ddData['tabId']
            }
          }
          this.drillDownHit(item, ddkey, ddData, i)
        });
      }
      this.drillDownKeys[tmpStr] = `'${ddkey}'`;
    }
  }


  drillDownHit(item, ddkey, ddData, i) {
    let drillDownReq = JSON.parse(JSON.stringify(drillDownRequest));
    Object.keys(drillDownReq).forEach((element) => {
      Object.keys(this.templateDetails[this.selectedIndex].dataSet[i].initialRequest).forEach((data) => {
        if (element == data) {
          drillDownReq[element] = this.templateDetails[this.selectedIndex].dataSet[i].initialRequest[element]
        }
      })
    })
    drillDownReq['currentLevel'] = item.data.currentLevel;
    drillDownReq['nextLevel'] = item.data.nextLevel;
    drillDownReq['reportTitle'] = item.data.reportTitle;
    drillDownReq['reportId'] = item.data.reportId;
    drillDownReq['drillDownKey1'] = `'${ddkey}'`;
    drillDownReq['filterPosition'] = this.tableValues['filterPosition'];
    drillDownReq['scalingFactor'] = !isNullOrUndefined(item.scalingFactorDd) && item.scalingFactorDd != '' ?
     item.scalingFactorDd : '0';
    drillDownReq['reportType'] = "W";
    item.initialRequest.reportFilters.forEach(ele => {
      if (ele.filterList) {
        drillDownReq[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
          ele.filterChecked ? `'ALL'` :
            ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
          ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
      }
    })
    drillDownReq = Object.assign(drillDownReq, this.getPromptValues());
    ddData['ddReq'] = drillDownReq;
    this.apiService.post(environment.getRMReport, drillDownReq).subscribe(resp => {
      this.drillDownLoader = false;
      if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {

        if (resp['response'].objectType == 'C') {
          if (this.listLineType.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
          }
          if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
          }
          resp['response']['chartDataJson'] =
            this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
        }
        if (resp['response'].objectType == 'G') {
          resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
          resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
          resp['response'].columnHeaderslstCopy.forEach(element => {
            element['sortLevel'] = 'noSort';
            element['sortReq'] = {};
          })
        }
        !isNullOrUndefined(resp['response'].grandTotalCaption) && resp['response'].grandTotalCaption != '' ?
        resp['response'].total.length ?
        resp['response'].total[0][resp['response'].columnHeaderslst[0]['dbColumnName']] = resp['response'].grandTotalCaption : '' : '';
        this.tableValues.scalingFactor = resp['response'].scalingFactor;
        this.scalingListDD.forEach((ele) => {
          if(ele.value == resp['response'].scalingFactor)
          ele.active = true;
          else
          ele.active = false;
        })
        ddData['widgetDetail'] = resp['response']
      }
      else {
        ddData['errorMessage'] = resp['message'];
        // this.globalService.showToastr.error(resp['message'])
      }
      this.drillDownTitle = (!isNullOrUndefined(item.initialRequest.reportTitle) && item.initialRequest.reportTitle != '') ?
        item.initialRequest.reportTitle : !isNullOrUndefined(resp['response']) && !isNullOrUndefined(resp['response'].reportTitle) ?
          resp['response'].reportTitle : '';
      this.drillDownList.push(ddData);
    })
  }

  findMyBrowser() {
    if (navigator.userAgent.indexOf("Firefox") != -1) {
      return true;
    }
    else {
      return false;
    }
  }

  popoverOpenDrillDown(event, type) {

    const popoverHold = document.getElementsByClassName('popoverHold');
    const navbarheight = document.getElementsByClassName('navbar')[0]['offsetHeight'];
    const dashboardMenuHeight = document.getElementById('dashboard-hold').offsetHeight;
    Array.from(popoverHold).forEach((pophold) => {
      // pophold['style'].height = '100%';
      // pophold['style'].width = '100%';
      pophold['style'].display = 'none';
    });
    let maintileElem;
    if (type == 'G')
      maintileElem = event.target.closest('.maintile');
    else
      maintileElem = event.closest('.maintile');
    // Set profit level indicator class
    // if (maintileElem.querySelector('.profitValue')) {
    //   this.popProfitSuccessClass = maintileElem.querySelector('.profitValue').classList.contains('text-success');
    //   this.popProfitDangerClass = maintileElem.querySelector('.profitValue').classList.contains('text-danger');
    //   this.popProfitGreyClass = maintileElem.querySelector('.profitValue').classList.contains('text-muted');
    // }
    // Check if the tile type is chart or data
    const tileType = maintileElem.classList.contains('chartTile');
    // Get top and left position of the parent maintile element
    const targetTop = +maintileElem.getBoundingClientRect().top - 50;
    const targetLeft = +maintileElem.getBoundingClientRect().left - 100;
    // Get window height
    const windowWidth = +window.innerWidth;
    const windowHeight = +window.innerHeight;
    // Calculate height and width for popover div
    let popoverDivHeight = +(windowHeight / 4 * 2);
    if (tileType) {
      popoverDivHeight = windowHeight - (navbarheight + dashboardMenuHeight);
    }
    const popoverDivWidth = +windowWidth / 2;
    // Get left position and width of the target parent element
    const maintileLeft = maintileElem.getBoundingClientRect().left;
    const popDivLeftandWidth = maintileLeft + popoverDivWidth;
    // Get top position and height of the target parent element
    const mailtileTop = maintileElem.getBoundingClientRect().top;
    const popDivTopandHeight = mailtileTop + popoverDivHeight;
    const popoverchartElem = document.getElementById('popoverHold');
    // Set popover div left/right based on viewport
    if ((windowWidth - 100) < popDivLeftandWidth) {
      popoverchartElem.style.left = 'unset';
      popoverchartElem.style.right = '0px';
    } else {
      popoverchartElem.style.left = '0px';
      popoverchartElem.style.right = 'unset';
    }
    // Set popover div top/bottom based on viewport
    if ((windowHeight - 100) < popDivTopandHeight) {
      popoverchartElem.style.top = 'unset';
      popoverchartElem.style.bottom = '1px';
    } else {
      popoverchartElem.style.top = '0px';
      popoverchartElem.style.bottom = 'unset';
    }

    popoverchartElem.style.display = 'block';

    // const objectIndex = maintileElem.getAttribute('data-index');
    // const objectChildIndex = maintileElem.getAttribute('data-child-index');
    // Set height and width for the popover element
    setTimeout(() => {
      popoverchartElem.style.opacity = '1';
      if (windowWidth > 425) {
        // popoverchartElem.style.height = `${popoverDivHeight * 1.5}px`;
        // popoverchartElem.style.width = `${popoverDivWidth * 1.2}px`;
        popoverchartElem.style.height = '100%';
        popoverchartElem.style.width = '100%';
      } else if (windowWidth <= 425) {
        popoverchartElem.style.left = '50%';
        popoverchartElem.style.top = '50%';
        popoverchartElem.style.transform = 'translate(-50%, -50%)';
        // popoverchartElem.style.height = `${popoverDivHeight}px`;
        // popoverchartElem.style.width = `${popoverDivWidth}px`;
        popoverchartElem.style.height = '100%';
        popoverchartElem.style.width = '100%';
      } else {
        // popoverchartElem.style.height = `${(windowHeight - 200)}px`;
        // popoverchartElem.style.width = `${windowWidth - 50}px`;
        popoverchartElem.style.height = '100%';
        popoverchartElem.style.width = '100%';
      }
    }, 50);
  }

  closePopoverChartDrillDown(event, data) {
    // data.openDrill = false;
    // this.startThis('swiperSeq' + data.tileSequence, data);
    ///this.startThis("all",data);
    const mainTile = event.target.closest('.maintile');
    // this.popoverTarget = event.target.closest('.footer-seperator');
    const popoverchartElem = document.getElementById('popoverHold');
    // const parentHeight = mainTile.offsetHeight;
    // const parentWidth = mainTile.offsetWidth;
    // popoverchartElem.style.height = '100%'; // `${parentHeight}px`;
    // popoverchartElem.style.width = '100%'; // `${parentWidth}px`;
    setTimeout(() => {
      // event.target.closest('.popoverHold').style.opacity = 0;
      event.target.closest('.popoverHold').style.display = 'none';
    }, 200);
  }

  clickTableEvent(event, index) {
    if (event.type == 'color') {
      // this.templateDetails[this.selectedIndex]['dataSet'][index]['titleColor'] = event.color
      // this.titleColor = event.color;

    }
    else if (event.type == 'drilldown') {
      this.drillDownReq(event.event, event.item, event.rowdata, index)
    }
  }
  searchWidgetLst(name) {
    // this.searchArray = [...this.widgetList]
    // this.widgetList =  this.searchArray.filter(el =>{
    //   let element = el.reportTitle.toLowerCase()
    //   return element.includes(name.toLowerCase())
    // })
    if (!isNullOrUndefined(this.widgetList)) {
      this.widgetList.forEach((widget) => { widget.showWidget = true });
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1, wIndex2) => {
        if (!isNullOrUndefined(element1.reportId)) {
          this.widgetList.forEach((element, wIndex) => {
            if (element.reportId == element1.reportId)
              element.showWidget = false;
          });
        }
      });
      this.widgetList.forEach((widget) => {
        let element = widget.reportTitle.toLowerCase()
        if (!element.includes(name.toLowerCase()) && widget.showWidget)
          widget.showWidget = false
      })
    }

  }

  searchFilterLst(filter) {
    filter.filterList.forEach((ele) => {
      let element = ele.description.toLowerCase()
      if (!element.includes(this.searchFilter.toLowerCase()))
        ele.show = false
      else
        ele.show = true
    })
  }

  updateforEvent(event, item) {
    // if (item['data'].ddFlag == 'Y') {
    // this.popoverOpenDrillDown(event);
    // }
  }

  drillDownDataRequest(event, item, rowData, i) {
    if (item.widgetDetail.ddFlag == 'Y') {
      // this.drillDownKeys = {};
      this.drillDownList = [];
      let tmpStr = `drillDownKey${1}`
      let ddkey;
      this.drillDownLoader = true;
      let ddData;
      if (item['widgetDetail']['objectType'] == 'G') {
        ddkey = rowData['DDKEYID'];
        let breadCrumb, breadCrumbArr;
        breadCrumbArr = item.data['columnHeaderslst'].filter(items => items.drillDownLabel);
        if (breadCrumbArr.length) {
          // breadCrumbLabel = !isNullOrUndefined(breadCrumbArr[0].caption) && breadCrumbArr[0].caption != '' ? breadCrumbArr[0].caption : 'DDKEYID';
          breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
        }
        else {
          breadCrumb = 'DDKEYID';
        }
        ddData = {
          tabId: ddkey,
          tabLabel: breadCrumbArr[0].caption,
          tabName: rowData[breadCrumb],
          index: i,
          currentLevelFlag: true
        }
      }
      else {
        let dataIndex = event.dataObj.dataIndex;
        this.zone.run(() => {
          if (isNullOrUndefined(item['widgetDetail']['chartDataJson']['dataset'])) {
            ddkey = item['widgetDetail']['chartDataJson']['data'][dataIndex].ddkey
          }
          else {
            let datasetIndex = event.dataObj.datasetIndex;
            ddkey = item['widgetDetail']['chartDataJson']['dataset'][datasetIndex]["data"][dataIndex].ddkey
          }
        });
        ddData = {
          tabId: ddkey,
          tabLabel: ddkey,
          tabName: ddkey,
          index: i,
          currentLevelFlag: true
        }
      }
      if (isNullOrUndefined(this.drillDownKeys[tmpStr])) {
        this.drillDownKeys[tmpStr] = `'${ddkey}'`;
      }
      else {
        this.drillDownKeys[tmpStr] = this.drillDownKeys[tmpStr].slice(0, -1)
        this.drillDownKeys[tmpStr] += `!@#${ddkey}'`;
      }
      let drillDownReq = JSON.parse(JSON.stringify(drillDownRequest));
      Object.keys(drillDownReq).forEach((element) => {
        Object.keys(this.templateDetails[this.selectedIndex].dataSet[i].initialRequest).forEach((data) => {
          if (element == data) {
            drillDownReq[element] = this.templateDetails[this.selectedIndex].dataSet[i].initialRequest[element]
          }
        })
        Object.keys(this.drillDownKeys).forEach((keys) => {
          if (element == keys) {
            drillDownReq[element] = this.drillDownKeys[element];
          }
        })
      })
      drillDownReq['currentLevel'] = item.widgetDetail.currentLevel;
      drillDownReq['nextLevel'] = item.widgetDetail.nextLevel;
      drillDownReq['reportTitle'] = item.widgetDetail.reportTitle;
      drillDownReq['reportId'] = item.widgetDetail.reportId;
      // drillDownReq['drillDownKey1'] = ddkey;
      drillDownReq = Object.assign(drillDownReq, this.getPromptValues());
      this.drillDownList.forEach((element) => {
        element.currentLevelFlag = false;
      });
      ddData['ddReq'] = drillDownReq;
      drillDownReq['scalingFactor'] = "0";
      this.apiService.post(environment.getRMReport, drillDownReq).subscribe(resp => {
        this.drillDownLoader = false;
        if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {
          if (resp['response'].objectType == 'C') {
            if (this.listLineType.includes(resp['response']['chartType'])) {
              let data = resp['response']['chartData'];
              var regexp = /<dataset/g;
              let c = data.match(regexp) || [];
              resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
            }
            if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
              let data = resp['response']['chartData'];
              var regexp = /<dataset/g;
              let c = data.match(regexp) || [];
              resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
            }
            resp['response']['chartDataJson'] =
              this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
          }
          if (resp['response'].objectType == 'G') {
            resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
            resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
            resp['response'].columnHeaderslstCopy.forEach(element => {
              element['sortLevel'] = 'noSort';
              element['sortReq'] = {};
            })
          }
          ddData['widgetDetail'] = resp['response']
        }
        else {
          ddData['errorMessage'] = resp['message'];
          this.globalService.showToastr.error(resp['message'])
        }
        this.drillDownList.push(ddData);
      })
    }
  }

  tableStyle() {
    return {
      'border': this.reportDesignXml.HEAD["TABLE-BORDER-ENABLE"] == 'Y' ? this.reportDesignXml.HEAD["TABLE-BORDER-PIXEL"] + 'px solid' : 'none',
      'border-color': this.reportDesignXml.HEAD["TABLE-BORDER-ENABLE"] == 'Y' ? '#' + this.reportDesignXml.HEAD["TABLE-BORDER-COLOR"] : 'none',
    }

  }
  headerStyle(drillData) {
    //  if(drillData['ddObjectType'] == 'T'){
    //   return {
    //     'background-color': '#00bcd4',
    //   }
    //  }
    //  else {
    //  return {
    //    'background-color': '#'+this.reportDesignXml.HEAD["BG-COLOR"],
    //   //  'line-height' : this.reportDesignXml.HEAD["LINE-HEIGHT"]+'px',
    //   //  'border': this.reportDesignXml.HEAD["HEADER-BORDER-ENABLE"] == 'Y' ? '1px solid #'+this.reportDesignXml.HEAD["HEADER-BORDER-COLOR"]: '1px solid #'+this.reportDesignXml.HEAD["BG-COLOR"],
    //   //  'color': '#'+ this.reportDesignXml.HEAD["FONT-COLOR"],
    //   //  'font-weight': this.reportDesignXml.HEAD["FONT-WEIGHT"],
    //   //  'font-size': this.reportDesignXml.HEAD["FONT-SIZE"]+'px',
    //  }
    // }
    return {
      'background-color': '#' + this.reportDesignXml.HEAD["BG-COLOR"],
      'line-height': this.reportDesignXml.HEAD["LINE-HEIGHT"] + 'px',
      'border': 'unset',
      // 'border': this.reportDesignXml.HEAD["HEADER-BORDER-ENABLE"] == 'Y' ? '1px solid #' + this.reportDesignXml.HEAD["HEADER-BORDER-COLOR"] : '1px solid #' + this.reportDesignXml.HEAD["BG-COLOR"],
      'color': '#' + this.reportDesignXml.HEAD["FONT-COLOR"],
      'font-weight': this.reportDesignXml.HEAD["FONT-WEIGHT"],
      'font-size': this.reportDesignXml.HEAD["FONT-SIZE"] + 'px',
    }
  }
  bodyRowStyle() {
    return {
      'border-bottom': this.reportDesignXml.BODY["ROW-BORDER-ENABLE"] == 'Y' ? '1px ' + this.borderStyle(this.reportDesignXml.BODY["ROW-BORDER-STYLE"]) + ' #' + this.reportDesignXml.BODY["ROW-BORDER-COLOR"] : 'none',
    }
  }

  borderStyle(data) {
    if (!isNullOrUndefined(data)) {
      return data
    }
    else {
      return 'solid';
    }

  }

  //  bodyColStyle(drillData) {
  //   if(drillData['ddObjectType'] == 'T'){
  //     return {
  //       // 'background-color': '#00bcd4',
  //       "border": '1px solid #00bcd4',
  //       'line-height' : '21px'
  //     }
  //    }
  //    else {
  //    return {
  //      'background-color': '#'+this.reportDesignXml.BODY["ROW-BG-COLOR"],
  //     //  'line-height' : this.reportDesignXml.BODY["LINE-HEIGHT"]+'px',
  //      'border': this.reportDesignXml.BODY["COLUMN-BORDER-ENABLE"] == 'Y' ? '1px solid #'+ this.reportDesignXml.BODY["COLUMN-BORDER-COLOR"]: 'inherit',
  //     //  'color': '#'+ this.reportDesignXml.BODY["FONT-COLOR"],
  //     //  'font-weight': this.reportDesignXml.BODY["FONT-WEIGHT"],
  //     //  'font-size': this.reportDesignXml.BODY["FONT-SIZE"]+'px'
  //    }
  //   }
  //  }

  bodyColStyle(header, data) {
    let fontColor, fontWeight, background;
    if (header.colorDiff == 'Y') {
      let dataValue = data[header.dbColumnName].split('%').join('');
      fontWeight = 500;
      // if (this.tableValues.initialRequest.reportId == 'WIDG0002') {
      //   if (dataValue >= 0) {
      //     // fontColor = '079e12'
      //     background = 'a8ffab'
      //   }
      //   else {
      //     // fontColor = 'FF0000'
      //     background = 'ffa8a8'
      //   }
      //   fontColor = '3c4043';
      // } else {
        background = this.reportDesignXml.BODY["ROW-BG-COLOR"]
        if (dataValue >= 0)
          fontColor = '079e12'
        else
          fontColor = 'FF0000'
      // }
    }
    else {
      fontColor = this.reportDesignXml.BODY["FONT-COLOR"];
      fontWeight = this.reportDesignXml.BODY["FONT-WEIGHT"];
      background = this.reportDesignXml.BODY["ROW-BG-COLOR"]
    }

    return {
      'background-color': '#' + background,
      'line-height': this.reportDesignXml.BODY["LINE-HEIGHT"] + 'px',
      'border': this.reportDesignXml.BODY["COLUMN-BORDER-ENABLE"] == 'Y' ? '1px solid #' + this.reportDesignXml.BODY["COLUMN-BORDER-COLOR"] : 'inherit',
      'color': '#' + fontColor,
      'font-weight': fontWeight,
      'font-size': this.reportDesignXml.BODY["FONT-SIZE"] + 'px'
    }
  }

  totalStyle(data) {
    if (data.FORMAT_TYPE == 'FT') {
      return {
        'color': '#' + this.reportDesignXml.BODY["SUBTOTAL-FONT-COLOR"],
        'font-weight': '600'
      }
    }
  }
  setWidgetTrueFalse() {
    this.widgetList.forEach((widget) => { widget.showWidget = true });
    this.templateDetails[this.selectedIndex].dataSet.forEach((element1, wIndex2) => {
      //if(!isNullOrUndefined(element1.data.reportId)){
      this.widgetList.forEach((element, wIndex) => {
        if (element.reportId == element1.reportId)
          element.showWidget = false;
      });
      //}
    });
  }


  scalingFactorText(data) {
    let scale = data == '1' ? "" : data == '1000' ? "(In Thousands)" :
      data == '1000000' ? "(In Millions)" : data == '1000000000' ? "(In Billions)" : "";
    return scale
  }
  parsingObject(objectOG) {
    if ((objectOG.objectType == 'T' ||objectOG.objectType == 'SC') && !isNullOrUndefined(objectOG['tileData']) && objectOG['tileData'] != '') {
      objectOG['endResult'] = this.reqFormat(JSON.parse(objectOG['tileData']), 0, 0);
      objectOG['designValues'] = JSON.parse(objectOG['tileData']);
    }
    return objectOG

  }

  reqFormat(list, colourCss?, ink?) {
    let obj = list['PLACEHOLDERS'];
    let arrList = Object.keys(obj);
    let endList = {};
    (arrList).forEach((el, ind, arr) => {
      let set = obj[el];
      endList[el] = {
        type: String(set['SOURCECOL']).startsWith('-'),
        value: this.stringForming(set, el, colourCss, ink),
        toolTip: !isNullOrUndefined(set['TOOLTIP']) ? set['TOOLTIP'].split('_').join(' ') : '',
      };
    })
    return endList;
  }
  stringForming(data, el, colourCss?, ink?) {
    let string = '';
    string = `${(data['PREFIX'] && data['PREFIX'].length) ? `${data['PREFIX']}` : ''}${string}`;
    string = `${(data['PRECAPTION'] && data['PRECAPTION'].length) ? `${this.underscoreChange(data['PRECAPTION'])} ` : ''}${string}`;
    // string = `${string}${this.valueFormat(data)}`;
    // if (el != 'PLACEHOLDER4') {
    string = `${string}${this.underscoreChange(data['SOURCECOL'])}`;
    // } else {
    //   string = `${string}${this.underscoreChange(this.colorCss(data['SOURCECOL'], colourCss, ink))}`;
    // }
    string = `${string}${(data['SUFFIX'] && data['SUFFIX'].length) ? `${data['SUFFIX']}` : ''}`;
    string = `${string}${(data['PRECAPTION'] && data['POSTCAPTION'].length) ? ` ${this.underscoreChange(data['POSTCAPTION'])}` : ''}`;
    if (el == 'DRILLDOWN_KEY') {
      string = data;
    }
    return string;
  }
  underscoreChange(data) {
    if (isString(data)) {
      data = data.split('_').join(' ');
    }
    return data;
  }

  colorCss(val, colourCss?, ink?) {
    if (!isNull(val)) {
      const negindex = String(val).indexOf('-');
      // const color = this.cardCenterText1(colourCss, ink);
      return (negindex != -1) ? `<span style="color:#63e068" class="yoyText">${val}</span>` : `<span style="color:#63e068" class="yoyText">${val}</span>`;
    }
    return '';
  }

  tileStyle(item) {
    if (item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      let bgType = tempJson.HEAD["BG-COLOR"].split(",");
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] == 'Y') {
        return {
          'border': `2px solid #${tempJson.HEAD["HEADER-BORDER-COLOR"]}`,
          'color': '#' + tempJson.HEAD["HEADER-BORDER-COLOR"],
        }
      }
      if (bgType && bgType.length == 1) {
        return {
          'background-color': '#' + bgType[0],
          'color': '#' + tempJson.HEAD["FONT-COLOR"],
        }
      }
      if (bgType && bgType.length == 2) {
        return {
          'background-image': `linear-gradient(to right, #${bgType[0]}, #${bgType[1]}`,
          'color': '#' + tempJson.HEAD["FONT-COLOR"],
        }
      }
    }
  }

  titleColorStyle(item) {
    if (item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] != 'Y') {
        return {
          'color': '#' + tempJson["TITLE-COLOR"]
        }
      }
    }
  }

  centerFontStyle(item) {
    if (item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] != 'Y') {
        return {
          'color': '#' + tempJson.HEAD["MID-FONT-COLOR"]
        }
      }
    }
  }

  borderTopStyle(item) {
    if (item.initialRequest && item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] != 'Y') {
        return {
          'border-top': `solid 2px #${tempJson.HEAD["MID-FONT-COLOR"]}`
        }
      }
    }
  }

  widgetDownload() {
    if (this.tempWidgetList && this.tempWidgetList.length) {
      let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
      let model = {
        type: "widget",
        dataSource: JSON.stringify(this.templateDetails[this.selectedIndex]),
        fileName: `${this.templateDetails[this.selectedIndex].templateId}_${visionId}`
      };
      this.rootService.post('getPath', model).subscribe(response => {
        if (response['status'] == 1) {
          this.getpromptDescriptions();
          const keys = Object.keys(this.promptDescriptions);
          let promptArr = [];
          let promptLabel;
          this.widgetFilterList.forEach((label) => {
            keys.forEach((prompt) => {
              if (`promptDescription${label.filterSeq}` == prompt)
                promptArr.push(`${label.filterLabel} : ${this.promptDescriptions[prompt]}`)
            })
          })
          // let scale = item.scalingFactor == '1' ? "No Scaling" : item.scalingFactor == '1000' ? "In Thousand's" :
          //   item.scalingFactor == '1000000' ? "In Million's" : item.scalingFactor == '1000000000' ? "In Billion's" : "";
          // promptArr.push(`Scaling Factor : ${scale}`);
          promptLabel = promptArr.join('!@#');
          let gridReportArr = [];
          let gridReportIds;
          this.templateDetails[this.selectedIndex].dataSet.forEach((report) => {
            if (report.data && report.data.objectType == 'G') {
              // gridReportArr.push(`${report.data.reportId} : ${report.data.nextLevel}`)
              let obj = {};
              obj['reportId'] = report.initialRequest.reportId;
              obj['nextLevel'] = report.initialRequest.nextLevel;
              obj['objectType'] = report.initialRequest.objectType;
              obj['columnHeaderslst'] = report.data.columnHeaderslst;
              obj['gridDataSet'] = report.data.gridDataSet;
              obj['reportTitle'] = report.data.reportTitle;
              gridReportArr.push(Object.assign(obj, this.getPromptValues()))
            }
          });
          // gridReportIds = gridReportArr.join('!@#');
          // let downloadData = {
          //   reportTitle: model.fileName,
          //   promptLabel: promptLabel,
          //   gridReportIds: gridReportIds,
          //   fileName: model.fileName
          // }
          let formData = new FormData();
          formData.append('reportTitle', model.fileName);
          formData.append('promptLabel', promptLabel);
          formData.append('gridReportIds', JSON.stringify(gridReportArr));
          formData.append('fileName', model.fileName);
          formData.append('appTheme', this.globalService.selectedTheme['color'].replaceAll("#",""))
          this.apiService.fileDownloads(environment.widgetExport, formData).subscribe((resp) => {
            const blob = new Blob([resp], { type: 'application/pdf' });
            const url = window.URL.createObjectURL(blob);
            const link = this.downloadZipLink.nativeElement;
            link.href = url;
            link.download = `${model.fileName}.pdf`;
            link.click();
            window.URL.revokeObjectURL(url);

          }, (error: any) => {
            if (error.substr(12, 3) == '204') {
              this.globalService.showToastr.error('No Records to Export');
            }
            if (error.substr(12, 3) == '420') {
              this.globalService.showToastr.error('Error Generating Report');
            }
            if (error.substr(12, 3) == '417') {
              this.globalService.showToastr.error('Error while exporting the report');
            }
          });
        }
      },(error)=> {
        this.globalService.showToastr.error("Unable to download. Contact system admin")
      });
    }
  }


  toggleShow(event, dropId, item) {
    if (dropId == 'showDownloadDropdown') {
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
        element1['showDownloadDropdown'] = false;
      })
      item['showDownloadDropdown'] = true;
    } else if (dropId == 'showWidgetMoreOption') {
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1, index) => {
        if (element1['showWidgetMoreOption'] && element1.initialRequest && element1.initialRequest.reportFilters
          && this.filterClicked) {
          this.individualFilterReq(element1, index)
        }
        if (element1['reportId'] != item['reportId']) {
          element1['showWidgetMoreOption'] = false;
        }
      })
      item['showWidgetMoreOption'] = !item['showWidgetMoreOption'];
    } else if (dropId == 'showWidgetSortOption') {
      if (item.initialRequest && item.initialRequest.sortFlag == 'Y') {
        this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
          element1['showWidgetFilterOption'] = false;
          element1['showWidgetExportOption'] = false;
          element1['showWidgetScaling'] = false;
          element1['showWidgetChartOption'] = false;
          if (element1['reportId'] != item['reportId']) {
            element1['showWidgetSortOption'] = false;
          }
        });
        item['showWidgetSortOption'] = !item['showWidgetSortOption'];
      }
    } else if (dropId == 'showWidgetFilterOption') {
      if (item.initialRequest && item.initialRequest.filterFlag == 'Y'
        && item.initialRequest.reportFilters && item.initialRequest.reportFilters.length) {
        this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
          element1['showWidgetSortOption'] = false;
          element1['showWidgetExportOption'] = false;
          element1['showWidgetScaling'] = false;
          element1['showWidgetChartOption'] = false;
          if (element1['reportId'] != item['reportId']) {
            element1['showWidgetFilterOption'] = false;
          }
        });
        if (!isNullOrUndefined(item.initialRequest.reportFilters)
          && item.initialRequest.reportFilters.length) {
          item.initialRequest.reportFilters.forEach(filter => {
            if (filter.filterType == 'COMBO' && !isNullOrUndefined(filter.filterList)
              && filter.filterList.length) {
              this.searchFilter = '';
              this.searchFilterLst(filter);
            }
          })
        }
        item['showWidgetFilterOption'] = !item['showWidgetFilterOption'];
      }
    } else if (dropId == 'showWidgetExportOption') {
      if (item.initialRequest && item.initialRequest.objectType != 'C') {
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
        element1['showWidgetFilterOption'] = false;
        element1['showWidgetSortOption'] = false;
        element1['showWidgetScaling'] = false;
        element1['showWidgetChartOption'] = false;
        if (element1['reportId'] != item['reportId']) {
          element1['showWidgetExportOption'] = false;
        }
      });
      item['showWidgetExportOption'] = !item['showWidgetExportOption'];
    }
    } else if (dropId == 'showWidgetScaling') {
      if (item.initialRequest && item.initialRequest.scalingFactor != '' && item.initialRequest.scalingFactor != '0') {
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
        element1['showWidgetFilterOption'] = false;
        element1['showWidgetSortOption'] = false;
        element1['showWidgetExportOption'] = false;
        element1['showWidgetChartOption'] = false;
        if (element1['reportId'] != item['reportId']) {
          element1['showWidgetScaling'] = false;
        }
      });
      item['showWidgetScaling'] = !item['showWidgetScaling'];
    }
    }
    else if (dropId == 'showWidgetChartOption') {
      if (item.initialRequest && item.initialRequest.objectType == 'C') {
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
        element1['showWidgetFilterOption'] = false;
        element1['showWidgetSortOption'] = false;
        element1['showWidgetScaling'] = false;
        if (element1['reportId'] != item['reportId']) {
          element1['showWidgetChartOption'] = false;
        }
      });
      item['showWidgetChartOption'] = !item['showWidgetChartOption'];
    }
  }
    this.templateDetails[this.selectedIndex].dataSet.forEach((element1) => {
      if (element1.initialRequest && element1.initialRequest.reportFilters &&
        element1.initialRequest.reportFilters.length)
        element1.initialRequest.reportFilters.forEach((filter) => {
          filter.filterCheckbox = false;
        })
    })
  }

  toggleShowDrillDown(event, dropId, item) {
    if (dropId == 'showWidgetSortOption') {
      item['showWidgetExportOption'] = false;
      item['showWidgetScalingOption'] = false;
      item['showWidgetSortOption'] = !item['showWidgetSortOption'];
    }
    if (dropId == 'showWidgetExportOption' && item['widgetDetail'] && item['widgetDetail'].objectType == 'G') {
      item['showWidgetSortOption'] = false;
      item['showWidgetScalingOption'] = false;
      item['showWidgetExportOption'] = !item['showWidgetExportOption'];
    }
    if (dropId == 'showWidgetScalingOption') {
      item['showWidgetSortOption'] = false;
      item['showWidgetExportOption'] = false;
      item['showWidgetScalingOption'] = !item['showWidgetScalingOption'];
    }
  }

  arraySrtingConDes(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`${element["description"]}`)
    });
    return arr.toString();
  }

  getpromptDescriptions() {
    if (this.widgetFilterList && this.widgetFilterList.length) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      this.widgetFilterList.forEach(element1 => {
        values.forEach((element, promptIndex) => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO') {
              if (element1["multiSelect"] == 'Y') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = this.arraySrtingConDes(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element][0]['description']}`;
                }
              }
            }
            if (element1['filterType'] == 'DATE') {
              if (element != '') {
                let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${val}`;
              }
              else {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = ``;
              }

            }
            if (element1['filterType'] == 'TEXTD') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (element1 == 'TEXT') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (element1['filterType'] == 'TEXTM') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (element1['filterType'] == 'TREE') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
          }
        });
      });
    }
  }

  exportGrid(item, type, viewList, mode, drop) {
    drop.close();
    item['showWidgetExportOption'] = false;
    let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
    this.getpromptDescriptions();
    let localPrompt = [];
    item.initialRequest.reportFilters.forEach(ele => {
      if (ele.filterList) {
        item.initialRequest[`dataFilter${ele.filterSeq}`] = ele.multiSelect == 'Y' ?
          ele.filterChecked ? `'ALL'` :
            ele.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
          ele.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
        let desc = ele.multiSelect == 'Y' ?
          ele.filterChecked ? 'ALL' :
            ele.filterList.filter(item => item.checked).map(el => `${el.description}`).join(",") :
          ele.filterList.filter(item => item.active).map(el => `${el.description}`).join(",");
        let str = '';
        str += `${ele.filterLabel} : ${desc}`;
        localPrompt.push(str)
      }
    })
    let req = Object.assign(JSON.parse(JSON.stringify(item.initialRequest)), this.getPromptValues());
    const keys = Object.keys(this.promptDescriptions);
    let promptArr = [];
    this.widgetFilterList.forEach((label) => {
      keys.forEach((prompt) => {
        if (`promptDescription${label.filterSeq}` == prompt)
          promptArr.push(`${label.filterLabel} : ${this.promptDescriptions[prompt]}`)
      })
    })
    promptArr = promptArr.concat(localPrompt)
    let scale = item.scalingFactor == '1' ? "No Scaling" : item.scalingFactor == '1000' ? "In Thousands" :
      item.scalingFactor == '1000000' ? "In Millions" : item.scalingFactor == '1000000000' ? "In Billions" : "No Scaling";
    promptArr.push(`Scaling Factor : ${scale}`);
    req['promptLabel'] = promptArr.join('!@#');
    req['reportOrientation'] = "P";
    let sortColumn = 'ORDER BY ';
    let sortArr = !isNullOrUndefined(item.data.columnHeaderslstCopy) ?
      item.data.columnHeaderslstCopy.filter(el => el.sortLevel != 'noSort') : [];
    sortArr.forEach((arr, ind) => {
      sortColumn += `${arr.dbColumnName} ${arr.sortLevel.toUpperCase()}`
      if (sortArr.length - 1 != ind) {
        sortColumn += ',';
      }
    })
    req['screenSortColumn'] = sortArr.length ? sortColumn : '';
    req['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
    let api = viewList == 'xlsx' ? environment.getRMreportExcelExport : viewList == 'csv' ? environment.reportExportToCsv : environment.getRMreportPdfExport;
    this.apiService.fileDownloads(api, req).subscribe((resp) => {
      const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
      const url = window.URL.createObjectURL(blob);
      if (type == 'download') {
        const link = this.downloadZipLink.nativeElement;
        link.href = url;
        link.download = viewList == 'xlsx' ? `${req.reportTitle}_${visionId}.xlsx` :
          viewList == 'csv' ? `${req.reportTitle}_${visionId}.csv` :
            `${req.reportTitle}_${visionId}.pdf`;
        link.click();
      }
      window.URL.revokeObjectURL(url);

    }, (error: any) => {
      if (error.substr(12, 3) == '204') {
        this.globalService.showToastr.error('No Records to Export');
      }
      if (error.substr(12, 3) == '420') {
        this.globalService.showToastr.error('Error Generating Report');
      }
      if (error.substr(12, 3) == '417') {
        this.globalService.showToastr.error('Error while exporting the report');
      }
    });
  }

  exportGridDrillDown(item, type, viewList, mode?) {
    item['showDownloadDropdown'] = false;
    let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
    this.getpromptDescriptions();
    let req = Object.assign(JSON.parse(JSON.stringify(item.ddReq)), this.getPromptValues())
    const keys = Object.keys(this.promptDescriptions);
    let promptArr = [];
    this.widgetFilterList.forEach((label) => {
      keys.forEach((prompt) => {
        if (`promptDescription${label.filterSeq}` == prompt)
          promptArr.push(`${label.filterLabel} : ${this.promptDescriptions[prompt]}`)
      })
    })
    let drillLabel = '';
    this.drillDownList.forEach((element, ind) => {
      drillLabel += `${element.tabLabel} : ${element.tabName}`
      if (this.drillDownList.length - 1 != ind) {
        drillLabel += ' | ';
      }
    });
    // req['drillDownLabel'] = drillLabel;
    promptArr.push(drillLabel)
    let scale = this.tableValues.scalingFactor == '1' ? "No Scaling" : this.tableValues.scalingFactor == '1000' ? "In Thousands" :
      this.tableValues.scalingFactor == '1000000' ? "In Millions" : this.tableValues.scalingFactor == '1000000000' ? "In Billions" : "No Scaling";
    promptArr.push(`Scaling Factor : ${scale}`);
    req['promptLabel'] = promptArr.join('!@#');
    req['reportOrientation'] = mode;
    req['filterPosition'] = this.tableValues['filterPosition']
    req['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
    let api = viewList == 'xlsx' ? environment.getRMreportExcelExport : viewList == 'csv' ? environment.reportExportToCsv : environment.getRMreportPdfExport;
    this.apiService.fileDownloads(api, req).subscribe((resp) => {
      const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
      const url = window.URL.createObjectURL(blob);
      if (type == 'download') {
        const link = this.downloadZipLink.nativeElement;
        link.href = url;
        link.download = viewList == 'xlsx' ? `${req.reportTitle}_${visionId}.xlsx` :
          viewList == 'csv' ? `${req.reportTitle}_${visionId}.csv` :
            `${req.reportTitle}_${visionId}.pdf`;
        link.click();
      }
      window.URL.revokeObjectURL(url);

    }, (error: any) => {
      if (error.substr(12, 3) == '204') {
        this.globalService.showToastr.error('No Records to Export');
      }
      if (error.substr(12, 3) == '420') {
        this.globalService.showToastr.error('Error Generating Report');
      }
      if (error.substr(12, 3) == '417') {
        this.globalService.showToastr.error('Error while exporting the report');
      }
    });
  }

  replaceOccurrence(string, regex, n, replace) {
    var i = 0;
    return string.replace(regex, function (match) {
      i += 1;
      if (i === n) return replace;
      return match;
    });
  }

  tabClick(type) {
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    if (type == 'template') {
      sideFilterOverlay.classList.remove('show')
      this.templateBox = true;
      this.widgetBox = false;
      this.filterBox = false;
      this.settingBox = false;
    }
    if (type == 'widget') {
      sideFilterOverlay.classList.remove('show')
      this.templateBox = false;
      this.widgetBox = true;
      this.filterBox = false;
      this.settingBox = false;
    }
    if (type == 'filter') {
      sideFilterOverlay.classList.add('show')
      this.templateBox = false;
      this.widgetBox = false;
      this.filterBox = true;
      this.settingBox = false;
    }
    if (type == 'setting') {
      sideFilterOverlay.classList.remove('show')
      this.templateBox = false;
      this.widgetBox = false;
      this.filterBox = false;
      this.settingBox = true;
    }
  }

  onStart(event) {
    this.dragStopped = true;
  }

  onStop(event) {
    this.dragStopped = false;
    this.positionResize['transform'] = document.getElementById('side-filter-hold').style.transform;
  }

  onReset(block: AngularResizableDirective) {
    // block.resetSize();
    // let { size, position } = block.getStatus();
    // // this.size = size;
    // this.positionResize = position;
    this.filterBox = false
    this.widgetBox = false
    this.settingBox = false
    this.templateBox = false
    const sideFilter = document.getElementById('side-filter-hold');
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    if (sideFilter.classList.contains('setRight')) {
      sideFilterOverlay.classList.remove('show');
      sideFilter.classList.remove('setRight');
      this.position = { x: 0, y: 0 };
    }
    sideFilter.classList.remove("col-sm-8", "col-md-4", "col-lg-4", "col-xl-4");
    sideFilter.classList.remove("onRezing");
    sideFilter.style.removeProperty("left");
    sideFilter.style.removeProperty("top");
    sideFilter.style.removeProperty("height");
    sideFilter.style.removeProperty("width");

  }

  onResizeStart(event) {
    const sideFilter = document.getElementById("side-filter-hold");
    sideFilter.classList.remove("col-sm-8", "col-md-4", "col-lg-4", "col-xl-4");
    sideFilter.classList.add("onRezing");
  }

  onResizeStop(event) {
    this.positionResize.resized = true;
    const sideFilter = document.getElementById("side-filter-hold");
    this.positionResize['transform'] = sideFilter.style.transform;
    this.positionResize['width'] = sideFilter.style.width;
    this.positionResize['height'] = sideFilter.style.height;
    this.positionResize['left'] = sideFilter.style.left;
    this.positionResize['top'] = sideFilter.style.top;
  }

  themeSelect(type, prop) {
    this.selectedTheme = type;
    let color = type == 'BLUE' ? "#00b0e0" : type == 'ORANGE' ? "#d67243" : type == 'GREEN' ? "#029d98" : "#e54bb2";
    let positive = type == 'BLUE' ? "#61cafb" : type == 'ORANGE' ? "#F99C54" : type == 'GREEN' ? "#7bcdc6" : "#eda4d5";
    let negative = type == 'BLUE' ? "#92D5EC" : type == 'ORANGE' ? "#FAB47C" : type == 'GREEN' ? "#97f1e9" : "#fbc3e8";
    document.documentElement.style
      .setProperty('--widget-border', color);
    document.documentElement.style
      .setProperty('--widget-background-positive', positive);
    document.documentElement.style
      .setProperty('--widget-background-negative', negative);
    if (prop == 'click') {
      this.templateDetails[this.selectedIndex].dataSet.forEach((element1, wIndex2) => {
        if (!isNullOrUndefined(element1.reportId)) {
          this.widgetList.forEach((widget) => {
            if (element1.reportId == widget.reportId && element1.initialRequest &&
              element1.initialRequest.objectType == 'C') {
              setTimeout(() => {
                this.templateDetails[this.selectedIndex].dataSet[element1.id - 1]['apiCall'] =
                  this.apiCall(widget, element1.id - 1, widget).subscribe(result => this.resultDataCall(result, widget, element1.id - 1, ''))
              }, element1.id - 1 * 20)
            }
          })
        }
      })
    }
  }

  overAllFilterCheck(type, filter, item) {
    if (type == 'overall') {
      filter.filterList.forEach((item) => {
        item.checked = filter.filterChecked ? true : false;
      })
    }
    else {
      filter.filterChecked = filter.filterList.every(item => item.checked) ? true : false;
    }
    this.filterClicked = true
  }

  onlyClick(data, filter) {
    filter.filterList.forEach(element => element.checked = false);
    data.checked = true;
    filter.filterChecked = false;
    this.filterClicked = true;
  }

  dropdownClick(item, filter) {
    if (!isNullOrUndefined(item.initialRequest && item.initialRequest.reportFilters)) {
      item.initialRequest.reportFilters.forEach((ele) => {
        if (ele.filterLabel == filter.filterLabel) {
          ele.filterCheckbox = !ele.filterCheckbox
        }
        else {
          ele.filterCheckbox = false;
        }
      })
    }
  }

  sortTable(item, sort, index) {
    sort['sortLevel'] == 'noSort' ? sort['sortLevel'] = 'asc' : sort['sortLevel'] == 'asc' ? sort['sortLevel'] = 'desc' :
      sort['sortLevel'] == 'desc' ? sort['sortLevel'] = 'noSort' : '';
    sort['sortReq'][sort.dbColumnName] = sort['sortLevel']
    sort['sortLevel'] == 'noSort' ? delete sort['sortReq'][sort.dbColumnName] : '';
    let sortReq = {};
    item.data.columnHeaderslstCopy.forEach(sortObj => {
      sortReq = Object.assign(sortReq, sortObj['sortReq'])
    })
    item['sortReq'] = sortReq
    item['columnHeaderslstCopy'] = JSON.parse(JSON.stringify(item.data.columnHeaderslstCopy))
    item.data['showLoader'] = true;
    if (Object.keys(sortReq).length) {
      item.data.gridDataSet =
        this.gpService.orderBy(sortReq, item.data.gridDataSet, item.data.columnHeaderslstCopy);
    }
    else {
      item.data.gridDataSet = JSON.parse(JSON.stringify(item.data.gridDataSetCopy))
    }
    setTimeout(() => {
      item.data['showLoader'] = false
    }, 1)
  }

  sortTableDrillDown(item, sort, index) {
    sort['sortLevel'] == 'noSort' ? sort['sortLevel'] = 'asc' : sort['sortLevel'] == 'asc' ? sort['sortLevel'] = 'desc' :
      sort['sortLevel'] == 'desc' ? sort['sortLevel'] = 'noSort' : '';
    sort['sortReq'][sort.dbColumnName] = sort['sortLevel']
    sort['sortLevel'] == 'noSort' ? delete sort['sortReq'][sort.dbColumnName] : '';
    let sortReq = {};
    item.widgetDetail.columnHeaderslstCopy.forEach(sortObj => {
      sortReq = Object.assign(sortReq, sortObj['sortReq'])
    })
    item['sortReq'] = sortReq
    item['columnHeaderslstCopy'] = JSON.parse(JSON.stringify(item.widgetDetail.columnHeaderslstCopy))
    if (Object.keys(sortReq).length) {
      item.widgetDetail.gridDataSet =
        this.gpService.orderBy(sortReq, item.widgetDetail.gridDataSet, item.widgetDetail.columnHeaderslstCopy);
    }
    else {
      item.widgetDetail.gridDataSet = JSON.parse(JSON.stringify(item.widgetDetail.gridDataSetCopy))
    }
  }

  scalingSelect(item, scale, drop, index) {
    item['showWidgetMoreOption'] = false;
    item.showWidgetFilterOption = false;
    item.showWidgetSortOption = false;
    item.showWidgetScaling = false;
    item.showWidgetExportOption = false;
    item['showWidgetChartOption'] = false;
    drop.close();
    item.scalingFactorValue = scale.value;
    item['scalingFactorDd'] = scale.value;
    // item.scalingList.forEach((ele) => {
    //   ele.active = false
    // })
    // scale.active = true;
    this.individualFilterReq(item, index)
  }

  selectChart(item, chart, drop, index) {
    item['showWidgetMoreOption'] = false;
    item.showWidgetFilterOption = false;
    item.showWidgetSortOption = false;
    item.showWidgetScaling = false;
    item.showWidgetExportOption = false;
    item['showWidgetChartOption'] = false;
    drop.close();
    item.data.chartType = chart.name;
    this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = true;
    setTimeout(()=> {
      this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = false;
    },500)
  }

  scalingSelectDrillDown(item, scale) {
    item.showWidgetScalingOption = false;
    // this.tableValues.scalingFactor = scale.value;
    // this.scalingListDD.forEach((ele) => {
    //   ele.active = false
    // })
    // scale.active = true;
    let req = Object.assign(JSON.parse(JSON.stringify(item.ddReq)), this.getPromptValues());
    req['scalingFactor'] = scale.value;
    this.drillDownLoader = true;
    this.apiService.post(environment.getRMReport, req).subscribe(resp => {
      this.drillDownLoader = false;
      this.drillDownScalingHit(item,resp);
    })
  }

  drillDownScalingHit(item,resp) {
    if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {

      if (resp['response'].objectType == 'C') {
        if (this.listLineType.includes(resp['response']['chartType'])) {
          let data = resp['response']['chartData'];
          var regexp = /<dataset/g;
          let c = data.match(regexp) || [];
          resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
        }
        if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
          let data = resp['response']['chartData'];
          var regexp = /<dataset/g;
          let c = data.match(regexp) || [];
          resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
        }
        resp['response']['chartDataJson'] =
          this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
      }
      if (resp['response'].objectType == 'G') {
        resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
        resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
        resp['response'].columnHeaderslstCopy.forEach(element => {
          if (!isNullOrUndefined(item.columnHeaderslstCopy) && item.columnHeaderslstCopy.length){
            item.columnHeaderslstCopy.forEach(element1 => {
              if (element['dbColumnName'] == element1['dbColumnName']) {
                element['sortLevel'] = element1['sortLevel'];
                element['sortReq'] = element1['sortReq'];
              }
            })
          }
          else {
            element['sortLevel'] = 'noSort';
            element['sortReq'] = {};
          }
        })
        // resp['response'].columnHeaderslstCopy.forEach(element => {
        //   element['sortLevel'] = 'noSort';
        //   element['sortReq'] = {};
        // })
      }
      this.tableValues.scalingFactor = resp['response'].scalingFactor;
      this.scalingListDD.forEach((ele) => {
        if(ele.value == resp['response'].scalingFactor)
        ele.active = true;
        else
        ele.active = false;
      })
      item['widgetDetail'] = resp['response']
    }
    else {
      item['errorMessage'] = resp['message'];
    }
  }

  singleSelect(filters, data, filter) {
    if (filter.multiSelect != 'Y') {
      filters.forEach(ele => ele.active = false)
      data.active = true;
      this.filterClicked = true;
    }
  }

  resetWidget(item, drop, index) {
    item['showWidgetMoreOption'] = false;
    item.showWidgetFilterOption = false;
    item.showWidgetSortOption = false;
    item.showWidgetScaling = false;
    item.showWidgetExportOption = false;
    item.showWidgetChartOption = false;
    if(!isNullOrUndefined(item.data.columnHeaderslstCopy))
    item.data.columnHeaderslstCopy.forEach(elem=> {
      elem['sortLevel'] = 'noSort';
      elem['sortReq'] = {};
    })
    item.columnHeaderslstCopy = [];
    item['sortReq'] = {};
    drop.close();
    !isNullOrUndefined(item.scalingList) ? item.scalingList.forEach(ele => { ele.active = false }) : ''
    let scale = JSON.parse(JSON.stringify(this.widgetListCpy)).filter(ele => ele.reportId == item.reportId)[0]['scalingFactor'];
    if (!isNullOrUndefined(scale)) {
      item['initialRequest']['scalingFactor'] = scale;
      item['scalingFactor'] = scale;
      item.scalingFactorValue = scale;
      item['scalingFactorDd'] = scale;
      !isNullOrUndefined(item.scalingList) ? item.scalingList.forEach(ele => {
        if (scale == ele.value) {
          ele.active = true
        }
      }) : ''
    }
    if (!isNullOrUndefined(item['initialRequest'].reportFilters))
      item['initialRequest'].reportFilters.forEach((filter) => {
        this.filterDataSet(filter)
        item.initialRequest[`dataFilter${filter.filterSeq}`] = filter.multiSelect == 'Y' ?
          filter.filterChecked ? `'ALL'` :
            filter.filterList.filter(item => item.checked).map(el => `'${el.id}'`).join(",") :
          filter.filterList.filter(item => item.active).map(el => `'${el.id}'`).join(",");
      })
    let reportReq = Object.assign(JSON.parse(JSON.stringify(item.initialRequest)), this.getPromptValues());
    this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = '';
    this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = true;
    reportReq['scalingFactor'] = "0";
    this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
      this.templateDetails[this.selectedIndex].dataSet[index].data['showLoader'] = false;
      this.templateDetails[this.selectedIndex].dataSet[index].otherInfo = resp['otherInfo'];
      if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {
        this.templateDetails[this.selectedIndex].dataSet[index].data = resp['response'];
        if (resp['response'].objectType == 'C') {
          if (!isNull(resp['response'].chartType) && resp['response'].chartType.search("FCMap_") > -1)
            resp['response'].chartType = resp['response'].chartType.replaceAll("FCMap_", "");

          if (resp['response'].chartType == 'HLinearGauge' || resp['response'].chartType == 'AngularGauge'
            || resp['response'].chartType == 'Kenya') {
            this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'] =
              FusionCharts.transcodeData(resp['response'].chartData, "xml", "json", false)
          }
          else {
            if (this.listLineType.includes(resp['response']['chartType'])) {
              let data = resp['response']['chartData'];
              var regexp = /<dataset/g;
              let c = data.match(regexp) || [];
              resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
            }
            if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
              let data = resp['response']['chartData'];
              var regexp = /<dataset/g;
              let c = data.match(regexp) || [];
              resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
            }
            this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'] =
              this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
          }


          if (!isNullOrUndefined(this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart)) {
            this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartLeftMargin = 10;
            this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartTopMargin = 15;
            this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartRightMargin = 0;
            this.templateDetails[this.selectedIndex].dataSet[index].data['chartDataJson'].chart.chartBottomMargin = 0;
          }
        }
        if (resp['response'].objectType == 'G') {
          resp['response'].columnHeaderslstCopy = JSON.parse(JSON.stringify(resp['response'].columnHeaderslst));
          resp['response'].gridDataSetCopy = JSON.parse(JSON.stringify(resp['response'].gridDataSet));
          resp['response'].columnHeaderslstCopy.forEach(element => {
            element['sortLevel'] = 'noSort';
            element['sortReq'] = {};
          })
        }
        if (resp['response'].objectType == 'T' || resp['response'].objectType == 'SC') {
          this.parsingObject(resp['response'])
        }
      }
      else {
        this.templateDetails[this.selectedIndex].dataSet[index].data = {};
        this.templateDetails[this.selectedIndex].dataSet[index].data['errorMessage'] = resp['message']
      }
    })
  }

  resetDrillDown(item, i) {
    item.showWidgetScalingOption = false;
    item.showWidgetSortOption = false;
    item.showWidgetExportOption = false;
    let scale = '';
    this.widgetListCpy.forEach(wid=>{
      if(wid.reportId == item.ddReq.reportId){
        scale = wid.scalingFactor
      }
    })
    this.scalingListDD.forEach((ele) => {
      if(ele.value == scale)
      ele.active = true;
      else
      ele.active = false;
    })
    this.tableValues.scalingFactor = scale;
    let reportReq = Object.assign(JSON.parse(JSON.stringify(item.ddReq)), this.getPromptValues());
    // reportReq['scalingFactor'] = scale;
    item['sortReq'] = {}
    item['columnHeaderslstCopy'] = [];
    this.drillDownLoader = true;
    reportReq['scalingFactor'] = "0";
    this.apiService.post(environment.getRMReport, reportReq).subscribe(resp => {
      this.drillDownLoader = false;
      this.drillDownScalingHit(item,resp)
    })
  }

  getChartList(item) {
    let list = [];
    if(!isNullOrUndefined(item.data) && !isNullOrUndefined(item.data.chartList) && item.data.chartList.length) {
      item.data.chartList.forEach(ele=> {
        list.push({name: Object.keys(ele)[0], active: false, description: Object.values(ele)[0]})
      })
    }
    list.forEach(e=> {
      if(!isNullOrUndefined(item.data.chartType) && item.data.chartType == e.name)
      e.active = true
    })
    return list;
  }

  removePercentage(data) {
    if (!isNullOrUndefined(data) && data != '')
      return data.toString().split('%').join('').split('-').join('');
    else
      return ''
  }

  scBorderStyle(item) {
    return {
      'border-color': this.getBorderColor(item)
    }
  }

  getBorderColor(item) {
    let borderColor = '';
    let designVal = !isNullOrUndefined(item.data.designValues && item.data.designValues['PLACEHOLDERS']) ?
    item.data.designValues['PLACEHOLDERS'] : {};
    if (!isNullOrUndefined(designVal)) {
      let dataValue = item.data.endResult?.PLACEHOLDER1.value.toString().split('%').join('');
      if(!isNullOrUndefined(designVal['COLOR_DIFF']) && designVal['COLOR_DIFF'] == 'Y') {
        dataValue >= 0 ? borderColor = '#079e12' : borderColor = '#FF0000';
      }
      else {
        if(!isNullOrUndefined(designVal['ColorCode']) && designVal['ColorCode'] != '') {
          borderColor = designVal['ColorCode'].replace("#", "");
          borderColor = `#${borderColor}`;
        }
        else {
          borderColor = '#2a2a2a'
        }
      }
    }
    return borderColor
  }

  circleText(item) {
    let color = ''
    let designVal = !isNullOrUndefined(item.data.designValues && item.data.designValues['PLACEHOLDERS']) ?
    item.data.designValues['PLACEHOLDERS'] : {};
    if (!isNullOrUndefined(designVal)) {
      if(!isNullOrUndefined(designVal['APPLY_TEXT_COLOR']) && designVal['APPLY_TEXT_COLOR'] == 'Y') {
        color = this.getBorderColor(item)
      }
    }
    return {
      'color': color
    }
  }

  checkDefaultChart(chartType) {
    let flag = false;
    if(!isNullOrUndefined(this.widgetIconSvg &&this.widgetList) && this.widgetIconSvg.length && chartType != null) {
      let arr = [];
      arr = this.widgetIconSvg.filter(e=> e.chartType == chartType);
      flag = arr.length ? false : true;
    }
    return flag
  }

  colHeaderXmltoJson(object) {
    if(!isNullOrUndefined(object['colHeaderXml']) && object['colHeaderXml'] != '') {
      object['endResult'] = this.reqFormat(JSON.parse(object['colHeaderXml']), 0, 0);
    }
  }

  ngOnDestroy() {
    this.globalService.widget.iconEnabled = false;
    this.globalService.widget.enableTheme = true;
    this.globalService.topbarSlider = new BehaviorSubject({});
    //  this.templateDetails[this.selectedIndex].dataSet.forEach(element => {
    //   if(element.intervaelRequest){
    //     element.intervaelRequest.unsubscribe();
    //   }

    // });
    if(this.topSlider)
    this.topSlider.unsubscribe()
  }
}
