import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateGridsterComponent } from './template-gridster.component';

describe('TemplateGridsterComponent', () => {
  let component: TemplateGridsterComponent;
  let fixture: ComponentFixture<TemplateGridsterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateGridsterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateGridsterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
