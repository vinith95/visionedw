import { Component, OnInit, HostListener, Output, EventEmitter, Input } from '@angular/core';
import {defaultSvg} from '../../../../../assets/data/dashGroup';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-dashboard-tree-slide',
  templateUrl: './dashboard-tree-slide.component.html',
  styleUrls: ['./dashboard-tree-slide.component.css']
})
export class DashboardTreeSlideComponent implements OnInit {

  defaultSvg:any =defaultSvg;
  @Input() dashGroup;
  accordinValue: any;
  constructor(private router:Router,private globalService:GlobalService) {
    if(!isNullOrUndefined(this.dashGroup)){
      this.dashGroup.forEach(element => {
        element.isOpen=false;
      });
    }
   }

@Output() closeClick = new EventEmitter();
  ngOnInit() {
  }
  accordinFn(data){
    this.dashGroup.forEach(element => {
      if(data.alphaSubTab == element.alphaSubTab){
        element.isOpen=!element.isOpen;
      }
      else{
        element.isOpen=false;
      }
      if(!isNullOrUndefined(element.children)){
        element.children.forEach(element1 => {
          element1.isOpen=false;
        });
      }
    });  
  }
  closeSlide(){
    this.closeClick.emit(true)
  }

  accordinFnSub(data){
    this.dashGroup.forEach(element1 => {
      element1.children.forEach(element=> {
        if(data.alphaSubTab == element.alphaSubTab){
          element.isOpen=!element.isOpen;
        }
        else{
          element.isOpen=false;
        }
      })
    });
  }

  heightReturn(children){
    if(children.length > 8){
      return {
        "height":"208px"
      }
    }
    else{
      return {
        "height":"100%"
      }
    }

  }

  menuclick(dash1, dash, data){
    this.globalService.accordinValueDash=data.alphaSubTab;
    this.globalService.accordinSubValueDash=dash.alphaSubTab;
    this.globalService.selectedDashboard=dash1;
    localStorage.setItem('Dashboard_Id',dash1.alphaSubTab);
    this.closeSlide();
    
    this.globalService.activeIcon = ''
    this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/main/VisionReportSuitedash'], { skipLocationChange: true });
    }); 
    let body = document.body;
    const menumain = document.querySelectorAll(".menu-main");
      menumain.forEach((item)=>{
        item.classList.remove("m-0");
      });
    body.classList.add("sidebar-mini");
    document.getElementById("logo-mini").style.display = "inline";
    const subMenuList = document.querySelectorAll('.subMenu');
    subMenuList.forEach((ele) => {
      ele['style'].display = 'none';
    });
    const subMenuNormal = document.querySelectorAll('.submenu-normal');
    subMenuNormal.forEach((elem) => {
      elem['style'].display = 'none';
    });
    // this.globalService.accordinValue=data.applicationId;
    // this.globalService.accordinSubValue=dash.reportCategory;
   
    // this.closeSlide()
    // this.globalService.selectedReport=dash1;
    // this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['/main/reports'], { skipLocationChange: true });
    // }); 
  }

}
