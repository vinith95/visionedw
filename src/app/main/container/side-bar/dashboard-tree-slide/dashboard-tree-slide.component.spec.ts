import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DashboardTreeSlideComponent } from './dashboard-tree-slide.component';

describe('DashboardTreeSlideComponent', () => {
  let component: DashboardTreeSlideComponent;
  let fixture: ComponentFixture<DashboardTreeSlideComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTreeSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTreeSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
