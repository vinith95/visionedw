import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { ApiService, GlobalService } from '../../../service';
import { $ } from 'protractor';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { isNullOrUndefined } from 'util';
import { ER_BROWSER } from 'src/assets/svg/dataExtractor';
import { EVENT_POLICY } from 'src/assets/svg/dataProfiler';
import { POLICY_SETUP } from 'src/assets/svg/rulesLibrary';
import { PROFILE_SETUP } from 'src/assets/svg/dataCleanser';
import { TABLE_METADATA } from 'src/assets/svg/dataMonitor';
import { supervisor_account } from 'src/assets/svg/dataReports';
import { BUSINESS_GLOSSARY } from 'src/assets/svg/dqScheduler';
import { CRON_STATUS } from 'src/assets/svg/CORN_STATUS';
import { CommonService } from '../../../shared/common.service';







@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html'
})
export class SideBarComponent implements OnInit {
  reportGroup: any;
  reportSlideTrue: boolean;
  dashGroup: any;
  // menuProgram: any = '';
  dashboardSliderTitle: any = '';
  reportSliderTitle: any = '';
  svgIconsList = {
    ER_BROWSER: ER_BROWSER,
    EVENT_POLICY: EVENT_POLICY,
    POLICY_SETUP: POLICY_SETUP,
    PROFILE_SETUP: PROFILE_SETUP,
    TABLE_METADATA: TABLE_METADATA,
    supervisor_account:supervisor_account,
    BUSINESS_GLOSSARY:BUSINESS_GLOSSARY,
    CRON_STATUS:CRON_STATUS
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  constructor(public globalService: GlobalService,
    private apiService: ApiService, public common: CommonService,
    private router: Router) {
    this.menuList = JSON.parse(localStorage.getItem('menu_hierarchy'));

    if (isNullOrUndefined(this.globalService.pageLevelAccess)) {
      // this.common.pageTitle = this.menuList[4].children[1].menuName;
      // this.globalService.pageLevelAccess = this.menuList[4].children[1];
    }
  }

  slideTrue: boolean = false;
  menuList;
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  username = localStorage.getItem('User_Name');
  showMenu = false;

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngOnInit() {
    // $('.menu-main').addClass('m-0');
    if (!this.globalService.pageTitle) {
      this.globalService.pageTitle = localStorage.getItem('pageTitle') ? localStorage.getItem('pageTitle') : 'Home';
    }
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  setPageName(menuName) {
    localStorage.setItem('pageTitle', menuName);
    this.globalService.pageTitle = menuName;
    const menuLists = document.getElementsByClassName('subMenu');
    for (let i = 0; i <= menuLists.length; i++) {
      if (document.getElementsByClassName('subMenu')[i]) {
        document.getElementsByClassName('subMenu')[i]['style'].display = 'none';

      }
    }
    let body = document.body;
    const menumain = document.querySelectorAll(".menu-main");
    menumain.forEach((item) => {
      item.classList.remove("m-0");
    });
    body.classList.add("sidebar-mini");
    document.getElementById("logo-mini").style.display = "inline";
    const subMenuList = document.querySelectorAll('.subMenu');
    subMenuList.forEach((ele) => {
      ele['style'].display = 'none';
    });
    const subMenuNormal = document.querySelectorAll('.submenu-normal');
    subMenuNormal.forEach((elem) => {
      elem['style'].display = 'none';
    });
  }

  setAccess(data) {
    // this.globalService.pageLevelAccess = '';       localStorage.setItem('Dashboard_Id', 'MDM00002');
    // if(data['menuName']=="Branch Dashboard"){
    // localStorage.setItem('Dashboard_Id', localStorage.getItem('lg_Dashboard_Id'));
    // }
    this.globalService.pageLevelAccess = data;
  }

  /**************************************/
  /********** Toggle main menu **********/
  /*********************************** */
  // showHide(ev) {
  //   const getSidebarWidth = document.getElementsByClassName('sidebar')[0]['offsetWidth'];
  //   const clickedOnMainNav = ev.target.classList.contains('firstLevelMenuIcon');
  //   const firstLevelMenuIndex = ev.target.getAttribute('data-index');
  //   let ismenuVisible;
  //   ev = ev;
  //   let elem;
  //   Array.from(document.getElementsByClassName('thirdLevel')).forEach((thirdLevelelem, index) => {
  //     if (thirdLevelelem['offsetHeight'] > 0) {
  //       thirdLevelelem['style'].display = 'none';
  //     }
  //   });
  //   if (getSidebarWidth < 260) {
  //     elem = ev.srcElement.closest('li').children[1];
  //     ismenuVisible = document.getElementById('mainNav').children[0].children[1].getClientRects().length;
  //   } else {
  //     const childLength = ev.srcElement.closest('li').children.length;
  //     if (childLength > 2) {
  //       elem = ev.srcElement.closest('li').children[2];
  //     } else {
  //       elem = ev.srcElement.closest('li').children[1];
  //     }
  //   }
  //   if (!elem) {
  //     return;
  //   }
  //   elem = elem.style;
  //   elem.display = (elem.display !== 'block') ? 'block' : 'none';
  //   setTimeout(() => {
  //     elem.transform = (elem.display === 'block') ? 'scale(1)' : 'scale(0)';
  //   }, 100);

  //   if (clickedOnMainNav) {
  //     const secondLevelMenu = document.getElementsByClassName('secondLevelMenu');
  //     Array.from(secondLevelMenu).forEach((secondelem, index) => {
  //       if (secondelem.closest('.firstLevel').getAttribute('data-index') != firstLevelMenuIndex) {
  //         // if (secondelem['offsetHeight'] > 0 && index != firstLevelMenuIndex) {
  //         secondelem['style'].cssText = 'display: none; transform: scale(0);';
  //         // }
  //       }
  //     });
  //   }
  //   if (ev.target.closest('.secondLevelMenuLi') != null) {
  //     const secondLevelLiLength = +ev.target.closest('.secondLevelMenuLi').parentElement.children.length;
  //     const secondLevelLiIndex = +ev.target.closest('.secondLevelMenuLi').getAttribute('data-sli');
  //     let nextSibling;
  //     let previousSibling;
  //     let nextSiblingHgt;
  //     let previousSiblingHgt;
  //     if (secondLevelLiLength != (secondLevelLiIndex + 1)) {
  //       nextSibling = ev.target.closest('.secondLevelMenuLi').nextSibling;
  //       nextSiblingHgt = +ev.target.closest('.secondLevelMenuLi').nextSibling.offsetHeight;
  //       if (nextSiblingHgt > 40) {
  //         nextSibling.children[1].style.cssText = 'display: none; transform: scale(0)';
  //       }
  //     } else {
  //       previousSibling = ev.target.closest('.secondLevelMenuLi').previousSibling;
  //       previousSiblingHgt = +ev.target.closest('.secondLevelMenuLi').previousSibling.offsetHeight;
  //       if (previousSiblingHgt > 40) {
  //         previousSibling.children[1].style.cssText = 'display: none; transform: scale(0)';
  //       }
  //     }
  //   }
  // }
  showHide(ev) {
    const getSidebarWidth = document.getElementsByClassName('sidebar')[0]['offsetWidth'];
    const clickedOnMainNav = ev.target.closest(".firstLevelMenuIcon");
    const firstLevelMenuIndex = ev.target.closest(".firstLevelMenuIcon").getAttribute('data-index');
    let ismenuVisible;
    ev = ev;
    let elem;
    if (getSidebarWidth < 260) {
      if (!isNullOrUndefined(ev.srcElement.closest('li'))) {
        elem = ev.srcElement.closest('li').children[1];
        if (!isNullOrUndefined(document.getElementById('mainNav').children[0].children[1])) {
          ismenuVisible = document.getElementById('mainNav').children[0].children[1].getClientRects().length;
        }
      }


    } else {
      const childLength = ev.srcElement.closest('li').children.length;
      if (childLength > 2) {
        elem = ev.srcElement.closest('li').children[2];
      } else {
        elem = ev.srcElement.closest('li').children[1];
      }
    }
    if (!elem) {
      return;
    }
    elem = elem.style;
    elem.display = (elem.display !== 'block') ? 'block' : 'none';
    setTimeout(() => {
      elem.transform = (elem.display === 'block') ? 'scale(1)' : 'scale(0)';
    }, 100);

    if (clickedOnMainNav) {
      const secondLevelMenu = document.getElementsByClassName('secondLevelMenu');
      Array.from(secondLevelMenu).forEach((secondelem, index) => {
        if (secondelem.closest('.firstLevel').getAttribute('data-index') != firstLevelMenuIndex) {
          // if (secondelem['offsetHeight'] > 0 && index != firstLevelMenuIndex) {
          secondelem['style'].cssText = 'display: none; transform: scale(0);';
          // }
        }
      });
    }
    if (ev.target.closest('.secondLevelMenuLi') != null) {
      const secondLevelLiLength = +ev.target.closest('.secondLevelMenuLi').parentElement.children.length;
      const secondLevelLiIndex = +ev.target.closest('.secondLevelMenuLi').getAttribute('data-sli');
      let nextSibling;
      let previousSibling;
      let nextSiblingHgt;
      let previousSiblingHgt;
      if (secondLevelLiLength != (secondLevelLiIndex + 1)) {
        nextSibling = ev.target.closest('.secondLevelMenuLi').nextSibling;
        nextSiblingHgt = +ev.target.closest('.secondLevelMenuLi').nextSibling.offsetHeight;
        if (nextSiblingHgt > 40) {
          nextSibling.children[1].style.cssText = 'display: none; transform: scale(0)';
        }
      } else {
        previousSibling = ev.target.closest('.secondLevelMenuLi').previousSibling;
        if (!isNullOrUndefined(previousSibling)) {
          previousSiblingHgt = +ev.target.closest('.secondLevelMenuLi').previousSibling.offsetHeight;
          if (previousSiblingHgt > 40) {
            previousSibling.children[1].style.cssText = 'display: none; transform: scale(0)';
          }
        }
      }
      Array.from(document.getElementsByClassName('thirdLevel')).forEach((thirdLevelelem) => {
        if (thirdLevelelem['offsetHeight'] > 40) {
          const windowHeight = window.innerHeight;
          const topPosition = +thirdLevelelem.getBoundingClientRect().top;
          const bottomPosition = +thirdLevelelem.getBoundingClientRect().bottom;
          const thirdLevelelemHgt = thirdLevelelem['offsetHeight'];
          // const setHeight = windowHeight - (topPosition + (40 * secondLevelLiLength));
          const setHeight = windowHeight / 2.7;

          /***********************************************/
          /*For setting dynamic height for the menu items*/
          /*Code commented for future use, don't remove */
          /***********************************************/

          // if (bottomPosition > topPosition) {
          //   setHeight = bottomPosition - topPosition;
          // } else {
          //   setHeight = topPosition - bottomPosition;
          // }
          // thirdLevelelem['style'].cssText = `min-height: 150px; height: ${setHeight}px;
          // overflow: auto; display: block; transform: scale(1)`;
        }
      });
    }
  }

  @HostListener('document:click', ['$event'])
  @HostListener('document:touchstart', ['$event'])
  handleOutsideClick(event) {
    if (!event.target.closest('.nav-item')) {
      const selectAllMenu = document.getElementById('mainNav').getElementsByClassName('subMenu');
      // const selectAllMenuUl = document.getElementById('mainNav').querySelectorAll('.active-firstLevel ul');
      // Array.from(selectAllMenuUl).forEach((ulElem) => {
      //   // ulElem.style.transform = 'scale(0)';
      //   ulElem['style'].display = 'none';
      // });
      Array.from(selectAllMenu).forEach((ulElem) => {
        // ulElem.style.transform = 'scale(0)';
        ulElem['style'].display = 'none';
      });
      if (!event.target.closest('.dashSlide') && !event.target.classList.contains("expand_icon")) {
        this.slideTrue = false;
        this.reportSlideTrue = false;
      }
    }
    if (event.target.classList.contains('firstLevelMenuIcon')) {
      // const thirdLevelUl = document.getElementsByClassName('thirdLevel');
      // Array.from(thirdLevelUl).forEach((thirdUlElem) => {
      //   thirdUlElem['style'].transform = 'scale(0)';
      //   thirdUlElem['style'].display = 'none';
      // });
      if (this.slideTrue) {
        this.slideTrue = false;
      }
      if (this.reportSlideTrue) {
        this.reportSlideTrue = false;
      }
    }
    if (event.target.closest('.dashboardIconSta')) {
      const selectAllMenuUl = document.getElementById('mainNav').querySelectorAll('ul');
      Array.from(selectAllMenuUl).forEach((ulElem) => {
        ulElem.style.transform = 'scale(0)';
        ulElem.style.display = 'none';
      });
    }
    if (event.target.closest('.mainNav11')) {
      const element = document.querySelectorAll("ngx-dashboard #popoverHold");
      if (!isNullOrUndefined(element) && element.length) {
        element.forEach((ele) => {
          ele['style'].height = '100%';
          ele['style'].width = '100%';
          ele['style'].display = 'none';
          ele['style']['z-index'] = '24';
        })
      }
    }
  }
  dashboardReportSlide(menu) {
    if (menu.menuProgram == "reports") {
      this.reportSliderTitle = menu.menuName;
      this.apiService.get(environment.getReportList).subscribe((resp) => {
        if (!isNullOrUndefined(resp)) {
          this.globalService.pageLevelAccess = menu;
          resp['response'].forEach(element => {
            element.reportslst.forEach(element1 => {
              element1.isMenuActive = false;
            });
          });
          this.reportGroup = resp.response;
          this.reportGroup.forEach(element => {
            if (this.globalService.accordinValue == element.applicationId) {
              element.isOpen = true;
            }
            else {
              element.isOpen = false;
            }
            if (!isNullOrUndefined(element.reportslst)) {
              element.reportslst.forEach(element1 => {
                if (this.globalService.accordinSubValue == element1.reportCategory) {
                  element1.isOpen = true;
                }
                else {
                  element1.isOpen = false;
                }
                if (!isNullOrUndefined(element1.reportslst)) {
                  element1.reportslst.forEach(element2 => {
                    if (!isNullOrUndefined(this.globalService.selectedReport)) {
                      if (this.globalService.selectedReport.reportTitle == element2.reportTitle) {
                        element2.highlight = true;
                      }
                      else {
                        element2.highlight = false;
                      }
                    }
                  });
                }
              });
            }
          });
        }
        else {
          this.globalService.showLoader.error('ReportList is Empty')
        }
        this.reportSlideTrue = !this.reportSlideTrue;
        this.slideTrue = false;
      })
    } else {
      this.dashboardSliderTitle = menu.menuName;
      this.apiService.get(environment.getDashboardGroupList).subscribe((resp) => {
        this.slideTrue = !this.slideTrue;
        this.reportSlideTrue = false;
        this.dashGroup = resp.response;
        this.dashGroup.forEach(element => {
          if (this.globalService.accordinValueDash == element.alphaSubTab) {
            element.isOpen = true;
          }
          else {
            element.isOpen = false;
          }
          if (!isNullOrUndefined(element.children)) {
            element.children.forEach(element1 => {
              if (this.globalService.accordinSubValueDash == element1.alphaSubTab) {
                element1.isOpen = true;
              }
              else {
                element1.isOpen = false;
              }
              if (!isNullOrUndefined(element1.children)) {
                element1.children.forEach(element2 => {
                  if (!isNullOrUndefined(this.globalService.selectedDashboard)) {
                    if (this.globalService.selectedDashboard.alphaSubTab == element2.alphaSubTab) {
                      element2.highlight = true;
                    }
                    else {
                      element2.highlight = false;
                    }
                  }
                });
                if (element1.children.length) {
                  element['reportsAvailable'] = true;
                }
                else {
                  element['reportsAvailable'] = false;
                }
              }
            });
          }
        });
      })
    }
  }
  dashboardSlide() {
    this.apiService.get(environment.getDashboardGroupList).subscribe((resp) => {
      this.slideTrue = !this.slideTrue;
      this.reportSlideTrue = false;
      this.dashGroup = resp.response;
    })
  }

  reportSlide(menu) {
    this.apiService.get(environment.getReportList).subscribe((resp) => {
      if (!isNullOrUndefined(resp)) {
        resp['response'].forEach(element => {
          element.reportslst.forEach(element1 => {
            element1.isMenuActive = false;
          });
        });
        this.reportGroup = resp.response;
      }
      else {
        this.globalService.showLoader.error('ReportList is Empty')
      }
      this.reportSlideTrue = !this.reportSlideTrue;
      this.slideTrue = false;
    })
  }

  setActiveBg(menu1) {
    if (this.router.url == '/main/reports' && menu1.menuProgram == 'reports') {
      return 'reportActiveBg'
    }
    else if (this.router.url == '/main/VisionStudiodash' && menu1.menuProgram == 'dashboards') {
      return 'reportActiveBg'
    }
    else {
      return ''
    }
  }

  findActiveIcon(menu) {
    this.common.activeIcon = menu.menuName;
    setTimeout(() => {
      const thirdLevel = document.querySelectorAll('.active-firstLevel .subMenu .thirdLevel');
      thirdLevel.forEach((elem) => {
        elem['style'].display = 'block';
        elem['style'].transform = 'scale(1)';
      });
      const selectAllMenuUl = document.getElementById('mainNav').querySelectorAll('.non-active-firstLevel ul');
      Array.from(selectAllMenuUl).forEach((ulElem) => {
        ulElem['style'].transform = 'scale(0)';
        ulElem['style'].display = 'none';
      });
    }, 200)
  }


  disableTooltip() {
    if (document.body.classList.contains('sidebar-mini')) {
      return false;
    }
    else {
      return true
    }
  }

  secondLevel(submenuLevel1, menu) {
    if (isNullOrUndefined(submenuLevel1.children) || !submenuLevel1.children.length) {
      this.globalService.menuProgram = submenuLevel1.menuProgram;
      this.setAccess(submenuLevel1);
      this.setPageName(submenuLevel1.menuName);
      this.findActiveIcon(menu);
      //console.log("submenuLevel1.menuProgram",submenuLevel1.menuProgram);
      
      this.router.navigate(['/main/menu-maintenance/' + submenuLevel1.menuProgram], { skipLocationChange: true });
    }
  }

  getRoutingSecondLevel(submenuLevel1) {
    if (isNullOrUndefined(submenuLevel1.children) || !submenuLevel1.children.length) {
      return 'menu-maintenance' + '/' + submenuLevel1.menuProgram;
    }
  }

  // getRouting(submenuLevel1, submenuLevel2) {
  //   if (submenuLevel1.menuProgram == 'Dashboards') {
  //     return 'dashboard1';
  //   } else {
  //     if (submenuLevel1.menuProgram == 'Deal-Mgmt' || submenuLevel1.menuProgram == 'CustomerMDM') {
  //       return 'deal-management' + '/' + submenuLevel2.menuProgram;
  //     } else if (submenuLevel1.menuProgram == "deal-Reports" || submenuLevel1.menuProgram == "lead-Reports" || submenuLevel1.menuProgram == "MPRreport") {
  //       return 'report' + '/' + submenuLevel2.menuProgram;
  //     }
  //     else if (submenuLevel1.menuProgram == 'ReportDashboards') {
  //       return 'reports';
  //     }
  //     else {
  //       return 'menu-maintenance' + '/' + submenuLevel2.menuProgram;
  //     }
  //   }

  // }

  getRouting(submenuLevel1, submenuLevel2) {
    if (submenuLevel1.menuProgram == 'Dashboards') {
      return 'dashboard1';
    } else {
      if (submenuLevel2.menuProgram == 'ReportDashboards') {
        return 'reports';
      }
      else {
        return 'menu-maintenance' + '/' + submenuLevel2.menuProgram;
      }

    }

  }







}
