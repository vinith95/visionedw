import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DashboardSlideComponent } from './dashboard-slide.component';

describe('DashboardSlideComponent', () => {
  let component: DashboardSlideComponent;
  let fixture: ComponentFixture<DashboardSlideComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
