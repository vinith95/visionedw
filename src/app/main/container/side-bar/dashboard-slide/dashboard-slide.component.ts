import { Component, OnInit, HostListener, Output, EventEmitter, Input } from '@angular/core';
import {defaultSvg} from '../../../../../assets/data/dashGroup';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-slide',
  templateUrl: './dashboard-slide.component.html',
  styleUrls: ['./dashboard-slide.component.css']
})
export class DashboardSlideComponent implements OnInit {
  defaultSvg:any =defaultSvg;
  //dashGroup: any;
  @Input() dashGroup;
  constructor(private router:Router) { }

@Output() closeClick = new EventEmitter();
  ngOnInit() {
    //this.dashGroup=this.dashGroupList;
  }

  spanReturn(span:any){
    return {
      "grid-row":span,
    }
  }
  closeSlide(){
    this.closeClick.emit(true)
  }

  openDashboard(dash){
    localStorage.setItem('Dashboard_Id',dash.alphaSubTab);
    this.closeSlide();
    this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/main/VisionEdwAdmin'], { skipLocationChange: true });
    });
  }



}
