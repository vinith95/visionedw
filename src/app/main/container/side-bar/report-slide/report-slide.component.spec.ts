import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReportSlideComponent } from './report-slide.component';

describe('ReportSlideComponent', () => {
  let component: ReportSlideComponent;
  let fixture: ComponentFixture<ReportSlideComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportSlideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
