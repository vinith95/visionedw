import { Component, OnInit, HostListener, Output, EventEmitter, Input } from '@angular/core';
import { defaultSvg } from '../../../../../assets/data/dashGroup';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-report-slide',
  templateUrl: './report-slide.component.html',
  styleUrls: ['./report-slide.component.css']
})
export class ReportSlideComponent implements OnInit {
  reportS: any = {
    "reportId": "AMN001",
    "reportTitle": "Customers",
    "filterFlag": "Y",
  }
  defaultSvg: any = defaultSvg;
  @Input() reportGroup;
  accordinValue: any;
  constructor(private router: Router, private globalService: GlobalService) {
    if (!isNullOrUndefined(this.reportGroup)) {
      this.reportGroup.forEach(element => {
        element.isOpen = false;
      });
    }
  }

  @Output() closeClick = new EventEmitter();
  searchReport = '';

  ngOnInit() {
  }

  // searchReportLst() {
  //   this.reportGroup.forEach(first => {
  //     first.reportslst.forEach(second => {
  //       second.reportslst.forEach(third => {
  //       if (this.searchReport != '' &&
  //       third.reportTitle.toLowerCase().includes(this.searchReport.toLowerCase())) {
  //         console.log('aa', first,second,third, third.reportTitle.toLowerCase())
  //         first.isOpen = true;
  //         second.isOpen = true;
  //       }
  //       else {
  //         console.log('bb', first,second,third, third.reportTitle.toLowerCase())
  //         first.isOpen = false;
  //         second.isOpen = false;
  //       }
  //     })
  //   });
  // })
  // }

  accordinFn(data) {
    this.reportGroup.forEach(element => {
      if (data.applicationId == element.applicationId) {
        element.isOpen = !element.isOpen;
      }
      else {
        element.isOpen = false;
      }
      if (!isNullOrUndefined(element.reportslst)) {
        element.reportslst.forEach(element1 => {
          element1.isOpen = false;
        });
      }
    });
  }
  closeSlide() {
    this.closeClick.emit(true)
  }

  accordinFnSub(data) {
    this.reportGroup.forEach(element1 => {
      element1.reportslst.forEach(element => {
        if (data.reportCategory == element.reportCategory) {
          element.isOpen = !element.isOpen;
        }
        else {
          element.isOpen = false;
        }
      })
    });
  }

  heightReturn(reportslst) {
    if (reportslst.length > 8) {
      return {
        "height": "208px"
      }
    }
    else {
      return {
        "height": "100%"
      }
    }

  }

  menuclick(dash1, dash, data) {
    this.globalService.accordinValue = data.applicationId;
    this.globalService.accordinSubValue = dash.reportCategory;
    // this.reportGroup.forEach(element => {
    //   if (!isNullOrUndefined(element.reportslst))
    //     element.reportslst.forEach(element1 => {
    //       if (!isNullOrUndefined(element1.reportslst)) {
    //         element1.reportslst.forEach(element2 => {
    //           element2.highlight = false;
    //         })
    //       }
    //     })
    // });
    // dash.highlight = true;
    this.closeSlide()
    this.globalService.selectedReport = dash1;

    this.globalService.activeIcon = ''
    this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/main/reports'], { skipLocationChange: true });
    });
    let body = document.body;
    const menumain = document.querySelectorAll(".menu-main");
    menumain.forEach((item) => {
      item.classList.remove("m-0");
    });
    body.classList.add("sidebar-mini");
    document.getElementById("logo-mini").style.display = "inline";
    const subMenuList = document.querySelectorAll('.subMenu');
    subMenuList.forEach((ele) => {
      ele['style'].display = 'none';
    });
    const subMenuNormal = document.querySelectorAll('.submenu-normal');
    subMenuNormal.forEach((elem) => {
      elem['style'].display = 'none';
    });
  }
}
