import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { isNullOrUndefined } from 'util';
import { environment } from '../../../../environments/environment';
import { ApiService, GlobalService, JwtService } from '../../../service';
import { colorData, dummyData, sideBarColor, themeColor } from './colorList';
// import { customerDetailsPagelabels } from '../../deal-management/deal-entry/data.js';

declare var $: any;
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  // ------------------------------------------------------------
  showThemeDropdown: boolean;
  showNotifDropdown: boolean;
  showProfileDropdown: boolean;
  profile = false;
  settings = false;
  colorList: any = colorData;
  sideBarList: any = sideBarColor;
  themeColorList: any = themeColor;
  dummyDataList: any = dummyData;
  notificationList:any =[];
  getReminderDate = false;
  reminderDate: NgbDate;
  activeReminder;
  notificationCount: number = 0;
  birthdayNotif = true;
  dealNotif = false;
  customerDetailsPagelabels;
  eventSetupForm: FormGroup;
  @ViewChild('customerDetails', { static: false }) private customerDetails: ElementRef;
  @ViewChild("notificationPopupAction") private notificationPopupAction: ElementRef;
  @ViewChild("notificationPopupView") private notificationPopupView: ElementRef;
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  localTheme = JSON.parse(localStorage.getItem('themeAndLanguage'))
  colorRequest = {
    "visionId": 1,
    // "appTheme": this.localTheme['appTheme'],
    // "reportSliderTheme": this.localTheme['reportSliderTheme'],
    "language": "EN"
  }
  selectedColor: any;
  iconDropdown: boolean = false;
  settingDropdown: boolean = false;
  userName = localStorage.getItem('User_Name');
  eventValues: any = [];
  modalReference: any;
  editdata = [];
  notificationData: any = [];
  countValues: any = [];
  readButtonColorChange: boolean = false;
  IsmodelShow: boolean = false;
  shownList = null;
  actionType: any;
  actionButtonColorChange: boolean = false;
  oldnotificationvalues: any = [];

  // ------------------------------------------------------------
  constructor(
    private jwtService: JwtService,
    private router: Router,
    private translate: TranslateService,
    public globalService: GlobalService,
    private _apiService: ApiService,
    @Inject(DOCUMENT) public document: any,
    private modalService: NgbModal
  ) {
    this.translate.setDefaultLang('en');
    this.translate.use('en');
    // GET LANGUAGE
    this.translate.currentLoader.getTranslation(this.translate.currentLang)
      .subscribe(data => {
      });
  }
  options = ["All", "5", "10", "15"];
  selectedQuantity = "All";
  txtNotificationValue: string;

  NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];
  // ------------------------------------------------------------
  ngOnInit() {
    $('#minimizeSidebar1').on('click', function () {
      if ($('body').hasClass('sidebar-mini')) {
        $('.menu-main').addClass('m-0');
        $('body').removeClass('sidebar-mini');
        if ($('.w-h-20').hasClass('width20')) {
          $('.hide-preview-pane').animate({ left: '495px' });
          $('.tool-box').animate({ left: '495px' });
        } else {
          $('.hide-preview-pane').animate({ left: '282px' });
          $('.tool-box').animate({ left: '282px' });
        }
      } else {
        $('.menu-main').removeClass('m-0');
        $('body').addClass('sidebar-mini');
        if ($('.w-h-20').hasClass('width20')) {
          $('.hide-preview-pane').animate({ left: '352px' })
          $('.tool-box').animate({ left: '352px' });
        } else {
          $('.hide-preview-pane').animate({ left: '103px' })
          $('.tool-box').animate({ left: '103px' });
        }
      }
      $('.logo-mini').toggle();
    });
    
    // this.customerDetailsPagelabels = customerDetailsPagelabels;
    this.themeAndColor();
    this.getNotifications();
    this.selectedQuantity = "ALL";

  }

  themeAndColor() {
    if (!isNullOrUndefined(this.localTheme['appTheme'])) {
      this.colorList.forEach((element) => {
        if (this.localTheme['appTheme'] == element.name) {
          this.getCssThemeUrl(element, 'initial');
        }
      })
    }
    this.themeColorList.forEach((element) => {
      if (this.localTheme['reportSliderTheme'] == element.name) {
        this.getCssSliderTheme(element);
      }
    })
  }

  apply() {
    this._apiService.post(environment.saveAppTheme, this.colorRequest).subscribe((resp) => {
      if (resp['status'] == 1) {
        // this.globalService.showToastr.success(resp['message'])
      }
      else {
        this.globalService.showToastr.error(resp['message']);
      }
    })
    this.showThemeDropdown = false;
  }

  // Reset page title to home while navigation from other page
  setPageTitle() {
    localStorage.setItem('Dashboard_Id', localStorage.getItem('lg_Dashboard_Id'))
    this.globalService.pageTitle = localStorage.getItem('pageTitle');
  }

  // ------------------------------------------------------------
  client_name: any;
  showNoticationBar:boolean =false;
  getNotifications() {
    this.notificationList =[];
    this._apiService.get(environment.getNotification).subscribe((resp) => {
      this.notificationList = resp['response'] ? resp['response'] :[];
      if(this.notificationList.length){
        this.showNoticationBar =true;
      }
     
      this.notificationList.forEach(element => {
        if(element.internalStatus){
          element.internalOrginalStatus =element.internalStatus;
        }
      });
      if (resp["response"]) {
        this.notificationCount = resp['request'];
      }
    });
  }


  customerDetailsData;
  viewCustomerDetails = (data) => {
    event.stopPropagation();
    let reqData = {
      "actionType": "Query",
      "cifId": data.cifID,
      "country": 'KE',
      "leBook": '01'
    };
    const promise = this._apiService.post(environment.get_global_cif_customer_details, reqData).toPromise();
    promise.then((resp) => {
      if (resp['status']) {
        this.customerDetailsData = resp['response'];
        this.modalService.open(this.customerDetails, { size: 'lg', backdrop: 'static' });
      }
      else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }

  // Toggle notification tabs
  toggleNotifContent(data) {
    if (data == 'dealNotif') {
      this.dealNotif = true;
      this.birthdayNotif = false;
    } else if (data == 'birthdayNotif') {
      this.dealNotif = false;
      this.birthdayNotif = true;
    }
  }




  notificationAccordion(data) {
    if (data == 'dealNotif') {
      this.dealNotif = true;
      this.birthdayNotif = false;
    } else if (data == 'birthdayNotif') {
      this.dealNotif = false;
      this.birthdayNotif = true;
    }
    const notifAccorCont = document.querySelectorAll('.notifAccorCont');
    notifAccorCont.forEach((elem) => {
      elem['style'].height = '0px';
    });
    const accorElem = document.getElementById(data);
    accorElem.style.height = '300px';
  }

  showReminder(i) {
    if (this.activeReminder && this.activeReminder == i) {
      this.activeReminder = '';
    } else {
      this.activeReminder = i;
    }
  }

  // ------------------------------------------------------------
  hideVisibleSubMenu() {
    // const subMenuList = document.querySelectorAll('.subMenu');
    // subMenuList.forEach((ele) => {
    //   ele['style'].display = 'none';
    // });
    const subMenuNormal = document.querySelectorAll('.active-firstLevel .submenu-normal');
    const thirdLevelNormal = document.querySelectorAll('.active-firstLevel .submenu-normal .thirdLevelNormal');
    const thirdLevel = document.querySelectorAll('.active-firstLevel .submenu-normal .thirdLevel');
    const secondLevelNormal = document.querySelectorAll('.non-active-firstLevel .submenu-normal.secondLevelNormal');
    if (document.body.classList.contains('sidebar-mini')) {
      subMenuNormal.forEach((elem) => {
        elem['style'].display = 'none';
        elem['style'].transform = 'scale(0)';
      });
      thirdLevel.forEach((elem) => {
        elem['style'].display = 'block';
        elem['style'].transform = 'scale(1)';
      });
    }
    else {
      subMenuNormal.forEach((elem) => {
        elem['style'].display = 'block';
        elem['style'].transform = 'scale(1)';
      });
      thirdLevelNormal.forEach((elem) => {
        elem['style'].display = 'block';
        elem['style'].transform = 'scale(1)';
      });
      secondLevelNormal.forEach((elem) => {
        elem['style'].display = 'none';
        elem['style'].transform = 'scale(1)';
      });
    }
    //this.globalService.sideBarChangesDectect("1");
  }
  // ------------------------------------------------------------
  @HostListener('click', ['$event']) onSidebarResize() {
    this.hideVisibleSubMenu();
  }

  showiconbar: boolean = false;
  opendropdownMenu() {
    this.showiconbar = true;
  }


  // ------------------------------------------------------------
  toggleShow(event, dropId) {
    if (dropId === 'showProfileDropdown') {
      this.showThemeDropdown = false;
      this.showNotifDropdown = false;
      this.showProfileDropdown = !this.showProfileDropdown;
      this.iconDropdown = false;
      this.settingDropdown = false;
      this.showiconbar = false;
      this.showNoticationBar =false;
    }
    if (dropId === 'showNotifDropdown') {
      this.showiconbar = true;
      this.showProfileDropdown = false;
      this.showThemeDropdown = false;
      this.showNotifDropdown = !this.showNotifDropdown;
      this.iconDropdown = false;
      this.settingDropdown = false;
       this.showNoticationBar =true;
    }
    if (dropId === 'showThemeDropdown') {
      this.showProfileDropdown = false;
      this.showNotifDropdown = false;
      this.showThemeDropdown = !this.showThemeDropdown;
      this.iconDropdown = false;
      this.settingDropdown = false;
      this.showiconbar = false;
      this.showNoticationBar =false;
    }
    if (dropId === 'iconDropdown') {
      this.showProfileDropdown = false;
      this.showThemeDropdown = false;
      this.showNotifDropdown = false;
      this.iconDropdown = !this.iconDropdown;
      this.settingDropdown = false;
      this.showiconbar = false;
      this.showNoticationBar =false;
    }
    if (dropId === 'settingDropdown') {
      this.showProfileDropdown = false;
      this.showNotifDropdown = false;
      this.showThemeDropdown = false;
      this.iconDropdown = false;
      this.settingDropdown = !this.settingDropdown;
      this.showiconbar = false;
      this.showNoticationBar =false;
    }
  }

  templateSliderOpen(type, drop?) {

    drop != undefined ? drop.close() : '';
    this.iconDropdown = false;
    let data = {
      type: type
    }
    this.globalService.topBarChangesDectect(data);
  }

  // ------------------------------------------------------------
  @HostListener('document:click', ['$event'])
  @HostListener('document:touchstart', ['$event'])
  handleOutsideClick(event) {
    if (!event.target.closest('.nav-item') && !event.target.closest('ngb-datepicker') && !event.target.closest('.dropdown-menu')) {
      this.showProfileDropdown = false;
      this.showNotifDropdown = false;
      this.showThemeDropdown = false;
      this.iconDropdown = false;
      this.settingDropdown = false;
      if (event.target.closest('.sample')) {
        this.showiconbar = true;
      }
      else {
        this.showiconbar = false;
      }
    }
    if (event.target.closest(".mainNav11")) {
      this.showiconbar = false;
    }

  }
  setsidBar() {
    this.globalService.sideBarChangesDectect("1");
  }
  // ------------------------------------------------------------
  getCssThemeUrl(data: any, type) {
    this.colorList.forEach((ele) => {
      ele.select = false;
    })
    data.select = true;
    this.globalService.selectedTheme = data;
    this.colorRequest['appTheme'] = data.name;
    if (this.selectedColor == 'DARK') {
      document.documentElement.style
        .setProperty('--content-color', data.sliderdFontColor);
      document.documentElement.style
        .setProperty('--border-btn-color', data['border_btn']);
      document.documentElement.style
        .setProperty('--table-border', data.tableBorder);
    }
    else {
      document.documentElement.style
        .setProperty('--selected-color', data.selected_color);
      document.documentElement.style
        .setProperty('--content-color', data.color);
      document.documentElement.style
        .setProperty('--slider-border', data.color);
      document.documentElement.style
        .setProperty('--slider-font-color', data.sliderFontColor);
      document.documentElement.style
        .setProperty('--slider-background', data.slider_background);
      document.documentElement.style
        .setProperty('--box-shadow-color', data.boxShadow);
      document.documentElement.style
        .setProperty('--table-border', data.tableBorder);
      document.documentElement.style
        .setProperty('--autocomplete', data.autocomplete);
      document.documentElement.style
        .setProperty('--tree-icon-shadow', data['tree-icon-shadow']);
      document.documentElement.style
        .setProperty(' --tool-tip-shadow', data['tool-tip-shadow']);
      document.documentElement.style
        .setProperty('--form-image', data['form-image']);
      document.documentElement.style
        .setProperty('--blacktheme-border-2px', data.border);
      document.documentElement.style
        .setProperty('--border-btn-color', data['border_btn']);
      document.documentElement.style
        .setProperty('--open-folder-svg', data['open_folder']);
      document.documentElement.style
        .setProperty('--folder-closed-svg', data['closed_folder']);
    }
    type == 'change' ? this.apply() : '';
  }
  onThemeChange(templateData) {
    this.globalService.silderChanges(templateData);
  }

  getCssSliderTheme(data: any) {
    this.themeColorList.forEach((ele) => {
      ele.select = false;
    })
    data.select = true;
    this.colorRequest['reportSliderTheme'] = data.name;
    this.selectedColor = data.name;
    if (data.name == 'DARK') {
      this.onThemeChange('dark')
      document.documentElement.style
        .setProperty('--slider-border', data.slider_border);
      document.documentElement.style
        .setProperty('--slider-font-color', data.font_color);
      document.documentElement.style
        .setProperty('--slider-background', data.color);
      document.documentElement.style
        .setProperty('--selected-color', data.selected_color);
    }
    else {
      this.onThemeChange('light')
      this.colorList.forEach((element) => {
        if (this.colorRequest['appTheme'] == element.name) {
          this.getCssThemeUrl(element, 'change');
        }
      })
    }

  }

  sideNavColor(data) {
    document.documentElement.style
      .setProperty('--sidebar-background', data.background);
    document.documentElement.style
      .setProperty('--sidebar-color', data.color);
    document.documentElement.style
      .setProperty('--sidebar-icon-color', data.iconColor);
  }
  // ------------------------------------------------------------
  logout() {

    this.jwtService.unset('VisionAuthenticate');
    this.jwtService.unset('User_Name');
    this.jwtService.unset('Dashboard_Id');
    this.jwtService.unset('lg_Dashboard_Id');
    this.jwtService.unset('menu_hierarchy');
    this.jwtService.unset('businessDate');
    this.jwtService.unset('reportTemplate');
    this.jwtService.unset('reportType');
    this.jwtService.unset('enableWidgets');
    localStorage.setItem('pageTitle', '');
    this.router.navigate(['/']);
    this.globalService.pageLevelAccess = '';
    this.globalService.accordinValue = '';
    this.globalService.accordinSubValue = '';
    this.globalService.selectedReport = {};
    this.globalService.accordinValueDash = '';
    this.globalService.accordinSubValueDash = '';
    this.globalService.selectedDashboard = {};
    this.globalService.themeChanges("")
    this.globalService.silderChanges("")
    this.closeFullscreen();
    document.body.classList.add('sidebar-mini');
  }



  goHome() {
    if (localStorage.getItem('Dashboard_Id') != localStorage.getItem('lg_Dashboard_Id')) {
      this.globalService.pageTitle = localStorage.getItem('pageTitle');
      this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
        this.router.navigate(['/main/VisionEdwAdmin'], { skipLocationChange: true });
      });
      localStorage.setItem('Dashboard_Id', localStorage.getItem('lg_Dashboard_Id'));
    }
    else {
      this.globalService.pageTitle = localStorage.getItem('pageTitle');
      this.router.navigateByUrl('/main/empty', { skipLocationChange: true }).then(() => {
        this.router.navigate(['/main/VisionEdwAdmin'], { skipLocationChange: true });
      });
    }
  }
  closeFullscreen() {
    if (window.innerHeight == screen.height) {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        this.document.msExitFullscreen();
      }
    }
  }

  initilizeEventSetupForm() {
    this.eventSetupForm = new FormGroup({
      eventType: new FormControl(""),
      eventSubType: new FormControl(""),
      eventId: new FormControl(""),
      eventDescription: new FormControl(""),
      eventConditionType: new FormControl(""),
      eventCondition: new FormControl(""),
      eventStatus: new FormControl(""),
      event_sequence: new FormControl(''),
      notificationActionComment: new FormControl('', Validators.required),
    });
  }

  openPopupAddcolumn(data) {
    this.editdata = [];
    this.editdata.push(data);
    this.IsmodelShow = true;
    this.initilizeEventSetupForm();
    this.modalReference = this.modalService.open(this.notificationPopupAction, {
      size: "md",
      backdrop: "static",
      windowClass: 'eventObjectsPopUp'
    });
    this.eventSetupForm.patchValue({
      eventType: data.eventType + " - " + data.eventTypeDesc,
      eventSubType: data.eventSubType + " - " + data.eventSubTypeDesc,
      eventId: data.eventId + " - " + data.eventDescription,
      eventCondition: data.notificationMessage,
    });
    this.eventSetupForm.get("eventType").disable();
    this.eventSetupForm.get("eventSubType").disable();
    this.eventSetupForm.get("eventId").disable();
    this.eventSetupForm.get("eventCondition").disable();

  }


  updateNotification(data) {
    this.eventValues = [];
    this._apiService.post(environment.getNotificationDetails, data).subscribe((resp) => {
      if (resp['status'] == 0) {
        this.eventValues = resp['response'];
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }
  readApiHitting(data) {
    if (this.txtNotificationValue == "" || this.txtNotificationValue == undefined) {
      this.globalService.showToastr.warning("Please Enter Comments");
    }
    else {
      data.notificationActionComment = this.txtNotificationValue;
      data.notificationActionType = this.actionType;
      data.notificationStatus = this.actionType;
      this._apiService.post(environment.edwApiConfig_updateNotification, data).subscribe((resp) => {
        if (resp['status'] == 1) {
          this.globalService.showToastr.success(resp['message']);
          this.getNotificationsRelaod();
        } else {
          this.globalService.showToastr.error(resp['message']);
        }
      });
    }


  }

  actionApiHitting(data) {
    this.readButtonColorChange = true;
    this.actionButtonColorChange = false;
    data.notificationActionComment = '';
    data.notificationActionType = "READ";
    data.notificationStatus = "READ";
    this._apiService.post(environment.edwApiConfig_updateNotification, data).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.globalService.showToastr.success(resp['message']);
        this.getNotificationsRelaod();
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });



  }

  getNotificationsRelaod() {
    this.notificationList =[];
    this._apiService.get(environment.getNotification).subscribe((resp) => {
      this.notificationList = resp['response'] ? resp['response'] :[];
      if(!this.notificationList.length){
        this.showNoticationBar =false;
      }
      this.notificationList.forEach(element => {
        if(element.internalStatus){
          element.internalOrginalStatus =element.internalStatus;
        }
      });
      if (resp["response"]) {
        this.notificationCount = resp['request'];
      }
    });
  }


  submitEventSetupForm() {
    let rawValue = this.eventSetupForm.getRawValue();
    this.editdata.forEach(element => {
      if (element) {
        element.notificationActionComment = rawValue.notificationActionComment;
        element.notificationActionType = "ACTION";
        element.notificationStatus = "ACTION";
      }
    });

    this._apiService.post(environment.edwApiConfig_updateNotification, this.editdata[0]).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.globalService.showToastr.success(resp['message']);
        this.updateNotification(this.editdata[0]);
        this.modalClose();
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }




  modalClose() {
    this.modalService.dismissAll()
  }


  toggleList(list, value) {
    this.actionButtonColorChange = true;
    this.readButtonColorChange = false;

    this.actionType = '';
    this.actionType = value;
    if (this.isListExpanded(list)) {
      this.shownList = null;
    } else {
      this.shownList = list;
    }
  };

  isListExpanded(list) {
    return this.shownList === list;
  };


  filterNotificationList(value) {
    this.notificationData = this.oldnotificationvalues.filter(t => t.notificationStatus === value);
  }

  notificationDropDown() {
    if (this.selectedQuantity == "ALL") {
      this.saveParentValues.internalStatus = "0";
    }
    else {
      this.saveParentValues.internalStatus = this.selectedQuantity;
    }
    this._apiService.post(environment.getNotificationDetails, this.saveParentValues).subscribe((resp) => {
      if (resp['status'] == 0) {
        this.notificationData = resp['response'];
        this.oldnotificationvalues = resp['response'];
        this.countValues = resp['request'].split("|");
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }


  saveParentValues: any = [];
  openNotificationPopupView(data) {
    this.saveParentValues = data;
    data.internalStatus = "0";
    this.notificationData = [];
    this.IsmodelShow = true;
    this._apiService.post(environment.getNotificationDetails, data).subscribe((resp) => {
      if (resp['status'] == 0) {
        this.notificationData = resp['response'];
        this.oldnotificationvalues = resp['response'];
        this.countValues = resp['request'].split("|");


      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
    this.modalReference = this.modalService.open(this.notificationPopupView, {
      size: "lg",
      backdrop: "static",
      windowClass: "notification-popup",

    });
    this.initilizeEventSetupForm();
    this.eventSetupForm.patchValue({
      eventType: data.eventType + " - " + data.eventTypeDesc,
      eventSubType: data.eventSubType + " - " + data.eventSubTypeDesc,
      eventId: data.eventId + " - " + data.eventDescription,
      event_sequence: data.event_sequence,
      eventCondition: data.notificationMessage,
    });
    this.eventSetupForm.get("eventType").disable();
    this.eventSetupForm.get("eventSubType").disable();
    this.eventSetupForm.get("eventId").disable();
    this.eventSetupForm.get("eventCondition").disable();
  }

}

