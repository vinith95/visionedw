import { NgModule, ChangeDetectorRef, ElementRef ,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { MainRoutingModule } from './main-routing.module';
import { TopBarComponent } from './container/top-bar/top-bar.component';
import { SideBarComponent } from './container/side-bar/side-bar.component';
import { MainComponent } from './main.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { TilesComponent } from './home/dashboard/tiles/tiles.component';
import { ChartComponent } from './home/dashboard/chart/chart.component';
import { GridComponent } from './home/dashboard/grid/grid.component';
import { SwiperLocalComponent } from './home/dashboard/swiper/swiper.component';
import { DashboardSlideComponent } from './container/side-bar/dashboard-slide/dashboard-slide.component';
import { EmptyComponent } from './home/dashboard/empty/empty.component'
import { ReportsComponent } from './reports/reports.component';
import { ReportSlideComponent } from './container/side-bar/report-slide/report-slide.component';
import { DashboardTreeSlideComponent } from './container/side-bar/dashboard-tree-slide/dashboard-tree-slide.component';
import { HomeTemplateComponent } from './home-template/home-template.component';
import { TemplateGridsterComponent } from './template-gridster/template-gridster.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListviewgridlistComponent } from './listviewgridlist/listviewgridlist.component';
import { TablecolumnlistingComponent } from './ermodule/tablecolumnlisting/tablecolumnlisting.component';
import { TablecolumnmappingComponent } from './ermodule/tablecolumnmapping/tablecolumnmapping.component';


@NgModule({
  declarations: [
    TopBarComponent,
    SideBarComponent,
    MainComponent,
    HomeComponent,
    DashboardComponent,
    TilesComponent,
    ReportSlideComponent,
    ChartComponent,
    GridComponent,
    DashboardSlideComponent,
    SwiperLocalComponent,
    EmptyComponent,
    ReportsComponent,
    DashboardTreeSlideComponent,
    HomeTemplateComponent,
    TemplateGridsterComponent,
    ListviewgridlistComponent,
    TablecolumnlistingComponent,
    TablecolumnmappingComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    NgbModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class MainModule { }
