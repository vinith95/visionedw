import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService, JwtService, GlobalService } from '../../service';
import { environment } from '../../../environments/environment';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isUndefined, isObject, isNull } from 'util';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit, AfterViewInit {


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  leftContainerHeight: string;
  reportId;
  reportTypes = [
    {
      alphaSubTab: 1,
      description: 'Summary Report'
    },
    {
      alphaSubTab: 2,
      description: 'Detailed Report'
    }
  ];

  @ViewChild('downloadZipLink', { static: false }) private downloadZipLink: ElementRef;

  countryDropdownSettings = {
    singleSelection: true,
    idField: 'columnOne',
    textField: 'columnThree',
    selectAllText: 'Select All',
    filterSelectAllText: true,
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };

  leBookDropdownSettings = {
    singleSelection: true,
    idField: 'columnTwo',
    textField: 'columnTwo',
    selectAllText: 'Select All',
    filterSelectAllText: true,
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };

  dropdownSettings = {
    singleSelection: false,
    idField: 'alphaSubTab',
    textField: 'description',
    selectAllText: 'Select All',
    filterSelectAllText: true,
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };

  branchDropdownSettings = {
    singleSelection: false,
    idField: 'columnNine',
    textField: 'columnTen',
    selectAllText: 'Select All',
    filterSelectAllText: true,
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  }

  dropdownSettings1: IDropdownSettings = {
    singleSelection: false,
    idField: 'alphaSubTab',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };

  dropdownSettings2: IDropdownSettings = {
    singleSelection: false,
    idField: 'alphaSubTab',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };

  sbuDropdownSettings = {
    singleSelection: false,
    idField: 'columnOne',
    textField: 'columnTwo',
    selectAllText: 'Select All',
    filterSelectAllText: true,
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };

  selectedItems = [];
  tableData1;
  colArray1;
  modalHeader;
  ddkeyField;
  accountOfficer;
  branchDropdownList;
  dealType;
  dealStage;
  confidentLevel;
  dealStatus;
  leadType;
  promptValueAll = false;
  promptValue1All = false;
  promptValue2All = false;
  promptValue3All = false;
  promptValue4All = false;
  promptValue5All = false;
  promptValue6All = false;
  promptValueBranchAll = false;
  promptValue: any;
  promptValueBranch: any;
  promptValue1: any;
  promptValue2: NgbDate;
  promptValue3: NgbDate;
  promptValue4;
  promptValue5;
  promptValue6;
  reportTypeId;
  subReportFlag = true;
  exportFileData;
  months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  groupingVisible: any = true;
  tableData;
  colArray = [];
  colArrayObj = {};
  style = { width: '100%', margin: '0 auto' };
  showReport = false;
  mprReportIdObject: any;
  mprReport: boolean;
  tableDataArray: any = [];
  mprReportDataArray: any = [];
  mprReportDataArray1: any = {};
  headersLengthArray: any = [];
  branchList: any;
  promptValueDate: any;
  countrySelect: any;
  LeBookSelect: any;
  tdCount: any = 0;
  sbulist: any;
  mprExportRequestData: any[];
  pageTitle: any;

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  constructor(
    public globalService: GlobalService,
    private _apiService: ApiService,
    private jwtService: JwtService,
    private modalService: NgbModal,
    private route: ActivatedRoute) {
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngOnInit() {
    this.getPageLoadValues();
    this.getBranchList();
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    this.showReport = false;
    this.route
      .data
      .subscribe(v => {

        this.reportId = v.reportId;
        if (this.reportId == 'MPR001') {
          this.getPageLoadValues1();
        }
        this.showReport = true;
      });
    const promise = this._apiService.getCountryList().toPromise();
    promise.then((data) => {
      this.LeBookSelect = data['response'];
      this.countrySelect = data['response'];
    });
    const sbuPromise = this._apiService.getSBUList().toPromise();
    sbuPromise.then((data) => {
      this.sbulist = data['response'];
    });
    this.pageTitle = localStorage.getItem('pageTitle');
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + 'px';
      document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
      document.getElementById('card-body').children[0]['style'].height = '100%';
    }, 1);
  }
  getPageLoadValues = () => {

    this._apiService.get(environment.getReportPrompts).subscribe((resp) => {
      if (resp['status']) {
        this.accountOfficer = resp['response'][0];
        if (this.accountOfficer.length === 1) {
          this.promptValue1 = this.accountOfficer;
        }
        this.dealType = resp['response'][1];
        this.dealStage = resp['response'][2];
        this.confidentLevel = resp['response'][3];
        this.dealStatus = resp['response'][4];
        this.leadType = resp['response'][5];
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }

  getBranchList = () => {
    const data = {
      lastIndex: 1000,
      query: 'M0001',
      startIndex: 1
    };

    this._apiService.post(environment.get_Branch_List, data).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.branchDropdownList = resp['response'];
        if (this.branchDropdownList.length === 1) {
          this.promptValue2 = this.branchDropdownList;
        }
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }

  getPageLoadValues1 = () => {
    this._apiService.post(environment.getReportPrompts1, { groupReportId: 'MPR001' }).subscribe((resp) => {
      if (resp['status']) {
        this.mprReportIdObject = resp['response'];
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }


  onSelectAll(e, promt) {
    if (promt == 'promptValue1') {
      this.promptValue1All = true;
    } else if (promt == 'promptValue4') {
      this.promptValue4All = true;
    } else if (promt == 'promptValue5') {
      this.promptValue5All = true;
    } else if (promt == 'promptValueBranch') {
      this.promptValueBranchAll = true;
    } else if (promt == 'promptValue6') {
      this.promptValue6All = true;
    }
  }

  onDeSelectAll(e, promt) {
    if (promt == 'promptValue1') {
      this.promptValue1All = false;
    } else if (promt == 'promptValue4') {
      this.promptValue4All = false;
    } else if (promt == 'promptValue5') {
      this.promptValue5All = false;
    } else if (promt == 'promptValueBranch') {
      this.promptValueBranchAll = false;
    } else if (promt == 'promptValue6') {
      this.promptValue6All = false;
    }
  }

  // ------------------------------------------------------------
  getReport = (type) => {
    let data;
    this.subReportFlag = true;
    if (this.reportId !== 'MPR001' && this.reportId !== 'BMRM001') {
      const fromDate = this.promptValue2.day + this.promptValue2.month + this.promptValue2.year;
      const toDate = this.promptValue3.day + this.promptValue3.month + this.promptValue3.year;
    }
    if (this.reportId == 'DE00008') {
      this.subReportFlag = this.reportTypeId == 1 ? false : true;
      if (this.reportTypeId == 1) {
        data = {
          subReportFlag: false,
          promptValue3: this.promptValue4All ? this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString(),
          promptValue1: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
          promptValue2: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
          promptValue4: this.promptValue5All ? this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString(),
          promptValue5: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : "'ALL'"
        };
      } else {
        data = {
          subReportFlag: true,
          promptValue1: '',
          promptValue2: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
          promptValue3: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
          promptValue4: this.promptValue4All ? this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString(),
          promptValue5: this.promptValue5All ? this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString(),
          promptValue6: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString()
        };
      }

    } else if (this.reportId == 'DE00006') {
      this.subReportFlag = false;
      data = {
        subReportFlag: true,
        promptValue3: this.promptValue4All ? this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString(),
        promptValue1: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
        promptValue2: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
        promptValue4: this.promptValue5All ? this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString(),
        promptValue5: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString()
      };
    } else if (this.reportId == 'DE00011') {
      this.subReportFlag = false;
      data = {
        subReportFlag: false,
        promptValue3: this.promptValue4All ? this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString(),
        promptValue1: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
        promptValue2: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
        promptValue4: this.promptValue5All ? this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString(),
        promptValue5: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString()
      };
    } else if (this.reportId == 'BMRM001') {
      this.subReportFlag = false;
      data = {
        subReportFlag: false,
        promptValue1: !this.promptValueBranchAll ? this.promptValueBranch.map(x => '\'' + x.columnNine + '\'').toString() : this.promptValueBranch.map(x => '\'' + x.columnNine + '\'').toString(),
        promptValue2: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString(),
        promptValue3: this.promptValue1All ? this.promptValue1.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue1.map(x => '\'' + x.alphaSubTab + '\'').toString(),
      };
    } else if (this.reportId == 'MPR001') {
      this.subReportFlag = false;
    } else {
      data = {
        subReportFlag: true,
        promptValue1: this.promptValue1All ? this.promptValue1.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue1.map(x => '\'' + x.alphaSubTab + '\'').toString(),
        promptValue2: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
        promptValue3: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
        promptValue4: this.promptValue4All ? this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue4.map(x => '\'' + x.alphaSubTab + '\'').toString(),
        promptValue5: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString()
      };
    }
    if (this.reportId == 'DE00001') {
      data['reportId'] = this.reportId;
      data['promptValue5'] = this.promptValue5All ? this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString();
      data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
    } else if (this.reportId == 'DE00002') {
      if (this.reportTypeId == 1) {
        data['reportId'] = 'DE00002';
        data.promptValue5 = `'${this.reportTypeId}'`;
        data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
      } else {
        data['reportId'] = 'DE00003';
        data.promptValue5 = `'${this.reportTypeId}'`;
        data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
      }
    } else if (this.reportId == 'DE00004') {
      if (this.reportTypeId == 1) {
        data['reportId'] = 'DE00004';
        data.promptValue5 = `'${this.reportTypeId}'`;
        data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
      } else {
        data['reportId'] = 'DE00005';
        data.promptValue5 = `'${this.reportTypeId}'`;
        data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
      }
    } else if (this.reportId == 'DE00006') {
      data['reportId'] = 'DE00006';
      data['subReportFlag'] = false;
      data['promptValue5'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : "'ALL'";
    } else if (this.reportId == 'BMRM001') {
      data['reportId'] = 'BMRM001';
      data['subReportFlag'] = false;
      //data['promptValue5'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : "'ALL'";
    } else if (this.reportId == 'DE00008') {
      if (this.reportTypeId == 1) {
        data['reportId'] = 'DE00008';
        data.promptValue5 = `'${this.reportTypeId}'`;
        data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
      } else {
        data['reportId'] = 'DE00009';
        data['subReportFlag'] = true;
        // data.promptValue5 = `'${this.reportTypeId}'`;
        data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
      }

    } else if (this.reportId == 'DE00010') {
      data['reportId'] = this.reportId;
      data['promptValue5'] = this.promptValue5All ? this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString() : this.promptValue5.map(x => '\'' + x.alphaSubTab + '\'').toString();
      data['promptValue6'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
    } else if (this.reportId == 'DE00011') {
      data['reportId'] = this.reportId;
      data['promptValue5'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
    } else if (this.reportId == 'DE00011') {
      data['reportId'] = this.reportId;
      data['promptValue5'] = !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString();
    }
    if (type == 'report') {
      if (this.reportId !== 'MPR001') {
                  let tempArr = [];
        this._apiService.post(environment.getReport, data).subscribe((resp) => {
          if (resp['status']) {
            this.colArray = [];
            /* if(resp['response'].length > 100){
              resp['response'].slice(0, 100);
            } */
            this.tableData = resp;
            if(this.tableData.response.length > 1000){
              this.tableData['response'] = this.tableData.response.slice(0, 500);
            }
            let rowSpanCount = 0;
            let colSpanCount = 0;
            let maxLabelRowNum = 0;
            let colspanOverAll = 0;
            this.tableData.otherInfo.columnHeaderslst.forEach(element => {
              rowSpanCount = +element.colspan + rowSpanCount;
              colSpanCount = +element.rowspan + colSpanCount;
              maxLabelRowNum = +element.labelRowNum >= maxLabelRowNum ? element.labelRowNum : maxLabelRowNum;
              if (element.colspan >= 1) {
                colspanOverAll++;
              }
            });
            const dataColumnCount = (colSpanCount == 0 && rowSpanCount == 0) ? this.tableData.otherInfo.columnHeaderslst.length : this.tableData.otherInfo.columnHeaderslst.length - colspanOverAll;
            for (let z = 1; z <= dataColumnCount; z++) {
              this.colArray.push({ column: 'dataColumn' + z, referField: '' });
            }
            let lastColSpanCount = 0;
            let aa = 0;
            this.tableData.otherInfo.columnHeaderslst.forEach((element, i) => {
              if (colspanOverAll == 0) {
                this.colArray[i].referField = element.colType;
              } else {
                if (element.labelRowNum == 1) {
                  if (element.colspan > 0) {
                    lastColSpanCount = lastColSpanCount + element.colspan;
                    aa++;
                  } else {
                    if (lastColSpanCount == 0) {
                      this.colArray[i].referField = element.colType;
                    } else {
                      this.colArray[i - aa].referField = element.colType;
                    }
                  }
                }
              }
            });
            setTimeout(() => {
              if ((this.tableData.response.length && isUndefined(this.reportTypeId)) ||
                (this.tableData.response.length && !isUndefined(this.reportTypeId) && this.reportTypeId == 1)) {
                if (this.tableData.response[0].detailReportlst) {
                  this.groupBy(this.tableData.response[0].detailReportlst[0]);
                } else {
                  this.groupBy(this.tableData.response[0]);
                }
              }
            }, 500);
          } else {
            this.tableData = '';
            this.globalService.showToastr.error(resp['message']);
          }
        });
      }
      if (this.reportId === 'MPR001') {
        this.mprReportDataArray = [];
        this.mprExportRequestData = [];
        let currentReportId;
        this.mprReportIdObject.forEach((mprObj, objIndex) => {
          const mprData = {
            reportId: mprObj.reportId,
            subReportFlag: false,
            promptValue1: !this.promptValueBranchAll ? this.promptValueBranch.map(x => '\'' + x.columnNine + '\'').toString() : "'ALL'",
            promptValue2: '\'' + this.promptValueDate + '\'',
            promptValue3: !this.promptValue1All ? this.promptValue1.map(x => '\'' + x.alphaSubTab + '\'').toString() : "'ALL'",
            promptValue4: !this.promptValue4All ? this.promptValue4.map(x => '\'' + x.columnOne + '\'').toString() : "'ALL'",
            promptValue5: !this.promptValue5All ? this.promptValue5.map(x => '\'' + x.columnTwo + '\'').toString() : "'ALL'",
            promptValue6: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString()
          };
          this.mprExportRequestData.push(mprData);
          const promise = this._apiService.post(environment.getReport, mprData).toPromise();
          promise.then((resp) => {
            let rowSpanCount = 0;
            let colSpanCount = 0;
            let maxLabelRowNum = 0;
            let colspanOverAll = 0;
            let noOfTds = 0;
            this.tdCount = 0;
            resp['otherInfo'].columnHeaderslst.forEach((element, dataIndex) => {
              rowSpanCount = +element.colspan + rowSpanCount;
              colSpanCount = +element.rowspan + colSpanCount;
              maxLabelRowNum = +element.labelRowNum >= maxLabelRowNum ? element.labelRowNum : maxLabelRowNum;
              if (element.colspan >= 1) {
                colspanOverAll++;
              }
              noOfTds = +element.labelColNum;
              if (noOfTds > this.tdCount) {
                this.tdCount = noOfTds;
              }
              resp['tdCount'] = this.tdCount;
            });
            currentReportId = resp['otherInfo'].reportId;
            this.mprReportDataArray.push(resp);
            this.mprReportDataArray1[currentReportId] = resp;

          }).then(() => {
            this.mprReport = true;
          });

        });

      }
      this.getTableStyle();
    }
    if (type == 'excel') {
      if (this.reportId === 'MPR001') {
        let exceldata = this.mprExportRequestData;
        this._apiService.fileDownloadwithParam(environment.groupReportExport, exceldata).subscribe((data) => {
          const blob = new Blob([data], { type: 'application/vnd.ms-excel' });
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, this.tableData.otherInfo.reportTitle);
          } else {
            var a = document.createElement('a');
            a.href = URL.createObjectURL(blob);
            a.download = `${this.pageTitle}.xlsx`;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
          }

        }, (error: any) => {
          if (error.substr(12, 3) == '204') {
            this.globalService.showToastr.error('No Records to Export');
          }
          if (error.substr(12, 3) == '420') {
            this.globalService.showToastr.error('Error Generating Report');
          }
          if (error.substr(12, 3) == '417') {
            this.globalService.showToastr.error('Error while exporting the report');
          }
        });
      } else {
        this._apiService.fileDownloads(environment.excelExport, data).subscribe((success) => {
          const blob = new Blob([success], { type: 'text/xls' });
          const url = window.URL.createObjectURL(blob);
          const link = this.downloadZipLink.nativeElement;
          link.href = url;
          link.download = `${this.tableData.otherInfo.reportTitle}.xlsx`;
          link.click();
          window.URL.revokeObjectURL(url);

        }, (error: any) => {
          if (error.substr(12, 3) == '204') {
            this.globalService.showToastr.error('No Records to Export');
          }
          if (error.substr(12, 3) == '420') {
            this.globalService.showToastr.error('Error Generating Report');
          }
          if (error.substr(12, 3) == '417') {
            this.globalService.showToastr.error('Error while exporting the report');
          }
        });
      }

    }
  }

  openDrillDownModal(content, data) {
    let tempData;
    if (this.reportId == 'DE00006') {
      this.modalHeader = 'BM / RM Sales Deal Funnel Report-DrillDown';
      this.ddkeyField = data['ddKeyFieldValue'].split('-');
      tempData = {
        subReportFlag: false,
        promptValue1: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
        promptValue2: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
        promptValue3: '\'' + this.ddkeyField[0] + '\'',
        promptValue4: '\'' + this.ddkeyField[1] + '\'',
        promptValue5: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : "'ALL'",
        promptValue6: '\'' + this.ddkeyField[2] + '\'',
      };
      tempData['reportId'] = 'DE00007';
    } else if (this.reportId == 'DE00011') {
      this.modalHeader = 'BM / RM Sales Lead Funnel Report-DrillDown';
      this.ddkeyField = data['ddKeyFieldValue'].split('-');
      tempData = {
        subReportFlag: false,
        promptValue1: '\'' + `${this.promptValue2.day}-${this.months[this.promptValue2.month - 1]}-${this.promptValue2.year}` + '\'',
        promptValue2: '\'' + `${this.promptValue3.day}-${this.months[this.promptValue3.month - 1]}-${this.promptValue3.year}` + '\'',
        promptValue3: '\'' + this.ddkeyField[0] + '\'',
        promptValue4: '\'' + this.ddkeyField[1] + '\'',
        promptValue5: !this.promptValue6All ? this.promptValue6.map(x => '\'' + x.columnOne + '\'').toString() : "'ALL'",
        promptValue6: '\'' + this.ddkeyField[2] + '\'',
      };
      tempData['reportId'] = 'DE00012';
    }
    this.exportFileData = tempData;
    this._apiService.post(environment.getReport, tempData).subscribe((resp) => {
      if (resp['status']) {
        this.tableData1 = '';
        this.colArray1 = [];
        this.tableData1 = resp;

        let rowSpanCount = 0;
        let colSpanCount = 0;
        let maxLabelRowNum = 0;
        let colspanOverAll = 0;
        this.tableData1.otherInfo.columnHeaderslst.forEach(element => {
          rowSpanCount = +element.colspan + rowSpanCount;
          colSpanCount = +element.rowspan + colSpanCount;
          maxLabelRowNum = +element.labelRowNum >= maxLabelRowNum ? element.labelRowNum : maxLabelRowNum;
          if (element.colspan >= 1) {
            colspanOverAll++;
          }
        });
        const dataColumnCount1 = (colSpanCount == 0 && rowSpanCount == 0) ? this.tableData1.otherInfo.columnHeaderslst.length : this.tableData1.otherInfo.columnHeaderslst.length - colspanOverAll;
        for (let z = 1; z <= dataColumnCount1; z++) {
          this.colArray1.push({ column: 'dataColumn' + z, referField: '' });
        }
        let lastColSpanCount = 0;
        let aa = 0;
        this.tableData1.otherInfo.columnHeaderslst.forEach((element, i) => {
          if (colspanOverAll == 0) {
            this.colArray1[i].referField = element.colType;
          } else {
            if (element.labelRowNum == 1) {
              if (element.colspan > 0) {
                lastColSpanCount = lastColSpanCount + element.colspan;
                aa++;
              } else {
                if (lastColSpanCount == 0) {
                  this.colArray1[i].referField = element.colType;
                } else {
                  this.colArray1[i - aa].referField = element.colType;
                }
              }
            }
          }
        });
        setTimeout(() => {
          this.groupBy(this.tableData1.response[0]);
        }, 100);
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
    this.modalService.open(content, { size: 'lg', windowClass: 'reportModal', backdrop: 'static' });
  }

  exportFile() {
    if (this.reportId === 'MPR001') {
    } else {
      this._apiService.fileDownloads(environment.excelExport, this.exportFileData).subscribe((success) => {
        const blob = new Blob([success], { type: 'text/xls' });
        const url = window.URL.createObjectURL(blob);
        const link = this.downloadZipLink.nativeElement;
        link.href = url;
        link.download = `${this.tableData.otherInfo.reportTitle}.xlsx`;
        link.click();
        window.URL.revokeObjectURL(url);

      }, (error: any) => {
        if (error.substr(12, 3) == '204') {
          this.globalService.showToastr.error('No Records to Export');
        }
        if (error.substr(12, 3) == '420') {
          this.globalService.showToastr.error('Error Generating Report');
        }
        if (error.substr(12, 3) == '417') {
          this.globalService.showToastr.error('Error while exporting the report');
        }
      });
    }
  }

  groupBy(dashBoardObj) {
    // this.groupingVisible = !this.groupingVisible;
    this.groupingVisible = false;

    const obj = Object.keys(dashBoardObj);
    function groupTable(rows, startIndex, total) {

      if (total === 0) {
        return;
      }
      var i, tds = [],
        currentIndex = startIndex,
        count = 1,
        lst = [];
      for (var ind = 0; ind < rows.length; ind++) {
        for (var index = 0; index < rows[ind].getElementsByTagName('td').length; index++) {
          index == currentIndex ? tds.push(rows[ind].getElementsByTagName('td')[index]) : '';
        }
      }
      var ctrl = tds[0];
      lst.push(rows[0]);
      for (i = 1; i <= tds.length; i++) {
        var tdsInnerText;
        if (tds[i]) { tdsInnerText = tds[i].innerText; } else { tdsInnerText = ''; }
        if (ctrl.innerText == tdsInnerText) {
          count++;
          !isUndefined(tds[i]) ? tds[i].classList.add('deleted') : '';
          lst.push(rows[i]);
        } else {
          if (count > 1) {
            ctrl.setAttribute('rowspan', count);
            groupTable(lst, startIndex + 1, total - 1);
          }
          count = 1;
          lst = [];
          ctrl = tds[i];
          lst.push(rows[i]);
        }
      }
    }
    var tableId = document.getElementById('groupTable');
    if (!this.groupingVisible) {
      var tdGroup = [];
      for (var i = 0; i < tableId.getElementsByTagName('tr').length; i++) {
        if (tableId.getElementsByTagName('tr')[i].children.length != 0) {
          tableId.getElementsByTagName('tr')[i].children[0].tagName == 'TD' ? tdGroup.push(tableId.getElementsByTagName('tr')[i]) : '';
        }
      }
      groupTable(tdGroup, 1, obj.length);
      for (var it = 0; it < tableId.getElementsByClassName('deleted').length; it++) {
        tableId.getElementsByClassName('deleted')[it].classList.add('none');
      }
    } else {
      for (var it = 0; it < tableId.getElementsByClassName('deleted').length; it++) {
        tableId.getElementsByClassName('deleted')[it].classList.remove('none');
      }
      for (var removeAtt = 0; removeAtt < tableId.getElementsByTagName('td').length; removeAtt++) {
        tableId.getElementsByTagName('td')[removeAtt].hasAttribute('rowspan') ? tableId.getElementsByTagName('td')[removeAtt].removeAttribute('rowspan') : '';
      }
    }
  }

  groupTable(tableId, rows, startIndex, total) {
    if (total === 0) {
      return;
    }
    var i, tds = [],
      currentIndex = startIndex,
      count = 1,
      lst = [];

    for (var ind = 0; ind < rows.length; ind++) {
      for (var index = 0; index < rows[ind].getElementsByTagName('td').length; index++) {
        index == currentIndex ? tds.push(rows[ind].getElementsByTagName('td')[index]) : '';
      }
    }

    var ctrl = tds[0];
    lst.push(rows[0]);
    for (i = 1; i <= tds.length; i++) {
      var tdsInnerText;
      if (tds[i]) {
        tdsInnerText = tds[i].innerText;
      } else {
        tdsInnerText = '';
      }
      if (ctrl.innerText == tdsInnerText) {
        count++;
        tds[i].classList.add('deleted');
        lst.push(rows[i]);
      } else {
        if (count > 1) {
          ctrl.setAttribute('rowspan', count);
          this.groupTable(tableId, lst, startIndex + 1, total - 1);
        }
        count = 1;
        lst = [];
        ctrl = tds[i];
        lst.push(rows[i]);
      }
    }
  }

  getIndex(index: any) {
    var mod = index % 26,
      pow = (index / 26) | 0,
      out = mod ? String.fromCharCode(64 + mod) : (--pow, 'Z');
    return pow ? this.getIndex(pow) + out : out;
  }

  getStyle = (colType) => {
    if (colType == 'N') {
      return {
        'text-align': 'right'
      };
    } else if (colType == 'G') {
      return {
        'font-weight': 'bold'
      };
    }
  }
  getTableStyle() {
    if (this.reportId == 'DE00002' || this.reportId == 'DE00004' || this.reportId == 'DE00011' || this.reportId == 'DE00008' || this.reportId == 'DE00006') {
      if (this.reportTypeId == 1 || isUndefined(this.reportTypeId)) {
        this.style = {
          width: '60%',
          margin: '0 auto'
        };
      } else {
        this.style = {
          width: '100%',
          margin: '0 auto'
        };
      }
    }
  }
  disablerptButton() {
    if (this.reportId != 'MPR001')
      return !isUndefined(this.tableData) ? (this.tableData.response.length != 0) ? false : true : true;
    else if (this.reportId == 'MPR001')
      return !isNull(this.mprReportDataArray) ? (this.mprReportDataArray.length != 0) ? false : true : false;
  }

  disableButton() {
    let data;
    if (this.reportId == 'DE00006') {
      data = {
        promptValue3: this.promptValue4,
        promptValue1: this.promptValue2,
        promptValue2: this.promptValue3,
        promptValue4: this.promptValue5,
        promptValue6: this.promptValue6
      };
    } else if (this.reportId == 'DE00011') {
      data = {
        promptValue3: this.promptValue4,
        promptValue1: this.promptValue2,
        promptValue2: this.promptValue3,
        promptValue4: this.promptValue5,
        promptValue5: this.promptValue6
      };
    } else if (this.reportId == 'DE00004') {
      data = {
        promptValue1: this.promptValue1,
        promptValue2: this.promptValue2,
        promptValue3: this.promptValue3,
        promptValue4: this.promptValue4,
        promptValue5: this.promptValue6
      };
    } else if (this.reportId == 'DE00002') {
      data = {
        promptValue1: this.promptValue1,
        promptValue2: this.promptValue2,
        promptValue3: this.promptValue3,
        promptValue4: this.promptValue4,
        promptValue5: this.promptValue6
      };
    } else if (this.reportId == 'DE00008') {
      data = {
        promptValue2: this.promptValue2,
        promptValue3: this.promptValue3,
        promptValue4: this.promptValue4,
        promptValue5: this.promptValue5,
        promptValue6: this.promptValue6
      };
    } else if (this.reportId == 'MPR001') {
      data = {
        promptValue1: this.promptValue1,
        promptValue2: this.promptValueDate,
        promptValue3: this.promptValueBranch,
        promptValue4: this.promptValue4,
        promptValue5: this.promptValue5,
        promptValue6: this.promptValue6
      };
    } else if (this.reportId == 'BMRM001') {
      data = {
        promptValue1: this.promptValueBranch,
        promptValue2: this.promptValue1,
        promptValue3: this.promptValue6
      };
    } else {
      data = {
        promptValue1: this.promptValue1,
        promptValue2: this.promptValue2,
        promptValue3: this.promptValue3,
        promptValue4: this.promptValue4,
        promptValue5: this.promptValue5,
        promptValue6: this.promptValue6
      };
    }
    if (this.reportId != 'MPR001') {
      for (var key in data) {
        if (data[key] == null || data[key] == '' || data[key].length == 0 || !isObject(data[key])) {
          return true;
        }
      }
    }
    else {
      for (var key in data) {
        if (data[key] == null || data[key] == '' || data[key].length == 0 || (!isUndefined(this.promptValueDate) && this.promptValueDate.length !== 6)) {
          return true;
        }
      }
    }
    return false;
  }
}


