import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// COMPONENTS
import { ReportComponent } from './report.component';

const routes: Routes = [
  {
    path: 'deal-performance-report',
    component: ReportComponent,
    data: {
      reportId: 'DE00001'
    }
  },
  {
    path: 'deal-confidence-report',
    component: ReportComponent,
    data: {
      reportId: 'DE00002'
    }
  },
  {
    path: 'deal-stage-report',
    component: ReportComponent,
    data: {
      reportId: 'DE00004'
    }
  },
  {
    path: 'deal-sales-funnel',
    component: ReportComponent,
    data: {
      reportId: 'DE00006'
    }
  },
  {
    path: 'deal-call-report',
    component: ReportComponent,
    data: {
      reportId: 'DE00008'
    }
  },
  {
    path: 'lead-performance-report',
    component: ReportComponent,
    data: {
      reportId: 'DE00010'
    }
  },
  {
    path: 'lead-sales-funnel',
    component: ReportComponent,
    data: {
      reportId: 'DE00011'
    }
  },
  {
    path: 'mpr-report',
    component: ReportComponent,
    data: {
      reportId: 'MPR001'
    }
  },
  {
    path: 'bmrm-port',
    component: ReportComponent,
    data: {
      reportId: 'BMRM001'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
