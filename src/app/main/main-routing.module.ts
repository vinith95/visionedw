import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// COMPONENTS
import { MainComponent } from './main.component';
import { HomeComponent } from './home/home.component';

// import { ReportComponent } from './reports/report.component';
import { EmptyComponent } from './home/dashboard/empty/empty.component';
import { ReportsComponent } from './reports/reports.component';
import { HomeTemplateComponent } from './home-template/home-template.component';
import { ErmoduleComponent } from './ermodule/ermodule.component';
import { ListviewgridlistComponent } from './listviewgridlist/listviewgridlist.component';

const routes: Routes = [{
  path: '',
  component: MainComponent,
  children: [
    {
      path: '',
      redirectTo: 'VisionEdwAdmin',
      pathMatch: 'full'
    },

    {
      path: 'VisionEdwAdmin',
      component: HomeTemplateComponent,
    },
    {
       path:"listviewjsplumb",
       component:ListviewgridlistComponent,
    },
    {
      path: 'VisionEdwAdmindash',
      component: HomeComponent,
      //pathMatch: 'full'
    },
    {
      path: 'ErSchemaBrowser',
      component: HomeTemplateComponent,
      pathMatch: 'full'
    },
    {
      path: 'empty',
      component: EmptyComponent,
      pathMatch: 'full'
    },
    {
      path: 'menu-maintenance',
      loadChildren: () => import('../main/menu-maintenance/menu-maintenance.module').then(m => m.MenuMaintenanceModule)
    },
    {
      path: 'reports',
      component: ReportsComponent,
      pathMatch: 'full'
    },
    {
      path: 'report',
      loadChildren: () => import('../main/reportsOld/reports.module').then(m => m.ReportsModule)
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
