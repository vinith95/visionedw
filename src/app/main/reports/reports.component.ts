import { Component, OnInit, HostListener, NgZone, ChangeDetectorRef, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { JwtService, GlobalService, ApiService, LoaderService } from 'src/app/service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MagnifierComponent } from 'src/app/shared/magnifier/magnifier.component';
import { reportRequest, drillDownRequest } from './report.const';
import { HttpClient } from "@angular/common/http";
import { FormControl, FormGroup, Validators } from '@angular/forms';
//import { ExcelService } from 'src/app/service/excel.service';
import { XmltoJsonService } from 'src/app/service/xmlto-json.service';
import { Router } from '@angular/router';
import * as column from '../../../assets/data/interActivereReport.json'
import { EnvService } from 'src/app/env.service';
import { loaderSvg } from '../home/dashboard/loader';
import { defaultSvg } from 'src/assets/svg/default';
import { mediumSvg } from 'src/assets/svg/medium';
import { smallSvg } from 'src/assets/svg/small';
import { tinySvg } from 'src/assets/svg/tiny';
import { excelSvg } from 'src/assets/svg/xls';
import { pdfSvg } from 'src/assets/svg/pdf';
import { htmlSvg } from 'src/assets/svg/html';
import { csvSvg} from 'src/assets/svg/csv'
import { GenericPopupComponent } from '../../shared/generic-popup/generic-popup.component';
import { distinctUntilChanged, pairwise } from 'rxjs/operators';
import { GenericTreeComponent } from 'src/app/shared/generic-tree/generic-tree.component';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})

export class ReportsComponent implements OnInit, AfterViewChecked {
  leftContainerHeight: string;
  overallPromptValues: any = {};
  tableData = [];
  column: any;
  numWithMonth = { 'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12 };
  tableSettingJson: any = {
    pagination: "Y",
    pinning: "N",
    filter: "Y",
    grouping: "Y",
    sorting: "Y",
    columnDragandDrop: "N",
    columnResize: "N",
    advanceSettings: "Y",
    scrollbarX: "Y",
    excelExport: "Y"
  }
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    noDataAvailablePlaceholderText: 'No data available',
    allowSearchFilter: true,
    maxHeight: 160,
    searchPlaceholderText: 'Type to search'
  };

  dashboardFilterSettings: IDropdownSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    noDataAvailablePlaceholderText: 'No data available',
    closeDropDownOnSelection: true,
    maxHeight: 160,
    searchPlaceholderText: 'Type to search'
  };
  selectedReport: any;
  tablist: any = {};
  selectedReportId: any;
  genForm: any = 'xlsx';
  repertGenSuc: boolean = false;
  slideTrueTop: boolean = false;
  drillDownKeys = {};
  promptValues = {};
  promptDescriptions = {};
  crumbText: any;
  filterForm: FormGroup;
  filterSubmitted: boolean = false;
  filterVal: any;
  interActiveObj: any = {}
  mismatch: boolean = false;
  interActiveRespone: any;
  serverUrl: string;
  pdfView: boolean = false;
  orientation: boolean;
  viewList: any = 'xlsx';
  showDownloadDropdown: boolean;
  showViewDropdown: boolean;
  showZoomDropdown: boolean;
  showInfoDropdown: boolean;
  loaderSvg: any = loaderSvg;
  defaultIcon = defaultSvg;
  mediumIcon = mediumSvg;
  smallIcon = smallSvg;
  tinyIcon = tinySvg;
  pdfIcon = pdfSvg
  excelIcon = excelSvg;
  htmlIcon = htmlSvg;
  csvIcon = csvSvg;

  fontSizeList = [{
    caption: "Default",
    name: "default",
    active: false,
    svg: this.defaultIcon
  },
  {
    caption: "Medium",
    name: "medium",
    active: true,
    svg: this.mediumIcon
  },
  {
    caption: "Small",
    name: "small",
    active: false,
    svg: this.smallIcon
  },
  {
    caption: "Tiny",
    name: "tiny",
    active: false,
    svg: this.tinyIcon
  }
  ]
  currentViewIcon = this.mediumIcon;
  filterOn: boolean = false;
  viwerGrid: boolean = true;
  fetchElementList: any = [];
  submittedOnce: boolean = false;
  @ViewChild('downloadZipLink', { static: false }) private downloadZipLink: ElementRef;
  magnifierId = {};
  treeId = {};
  selectedReportType: any;
  hoverSeq: any;
  maintainData: any = {};
  currentFontSize: any = 'medium';
  exportRequest: any;
  reportRequestData: any;
  drillDownRequestData: any;
  reportOrientation: boolean = true;
  scalingList: any = [
    {
      alphaSubTab: '1',
      description: 'No Scaling'
    },
    {
      alphaSubTab: '1000',
      description: 'In Thousands'
    },
    {
      alphaSubTab: '1000000',
      description: 'In Millions'
    },
    {
      alphaSubTab: '1000000000',
      description: 'In Billions'
    }
  ];
  scalingFactor: any;
  showBeforeDownload: boolean;
  reportOrientationPrompt: boolean = true;
  listLineType = ['MSColumnLine3D', 'StackedColumn2DLine', 'StackedColumn3DLine'];
  listLineTypeDY = ['MSColumn3DLineDY', 'StackedColumn3DLineDY'];
  ddKeysChart = {
    count: 1
  };
  screentypeToolTip: any = "Expand";
  screentype = 'fa-expand';
  downloaddropDownObj: any = {};
  breadCrumbShow: boolean = true;
  reportInfo: any;
  showOtherFilters: boolean = false;
  dropdownSetting: any = {
    showScalingOption: false,
    showExportOption: false,
    showViewOption: false,
    showZoomOption: false,
    showPdfOption: false
  };
  disableScaling: boolean = false;
  applyGroupingFlag: boolean = false;


  constructor(private jwtService: JwtService,
    private translate: TranslateService,
    public globalService: GlobalService,
    private modalService: NgbModal,
    // private excelService: ExcelService,
    private zone: NgZone,
    private router: Router,
    private env: EnvService,
    private loaderService: LoaderService,
    private cdRef: ChangeDetectorRef,
    private _apiService: ApiService,
    private xmlToJSonService: XmltoJsonService, private http: HttpClient) {
    // this.translate.setDefaultLang(this.jwtService.get('setLanguage'));
    // this.translate.use(this.jwtService.get('setLanguage'));
    // // GET LANGUAGE
    // this.translate.currentLoader.getTranslation(this.translate.currentLang)
    //   .subscribe(data => {
    //   });
    //this.serverUrl = this.env.serverUrl;
  }

  ngOnInit() {
    this.globalService.getsideBarChangesDectect().subscribe((params)=>{
      if(params){
        if(this.tablist[this.selectedReportId] && this.tablist[this.selectedReportId].tabDetail.length){
          this.tablist[this.selectedReportId].tabDetail.forEach(element => {
            element['group'].forEach(element1 => {
              if(element1.tileSequenceId == "group3"){
                element1['group'].forEach(element2 => {
                  if(element2['objectType'] == 'C' && element2.reportDetail != undefined)
                    element2.reportDetail[0].response.show=false;
                });
              }
              else{
                if(element1['objectType'] == 'C' && element1.reportDetail != undefined)
                  element1.reportDetail[0].response.show=false;
              }
            });
          });
          setTimeout(() => {
            this.tablist[this.selectedReportId].tabDetail.forEach(element => {
              element['group'].forEach(element1 => {
                if(element1.tileSequenceId == "group3"){
                  element1['group'].forEach(element2 => {
                    if(element2['objectType'] == 'C' && element2.reportDetail != undefined)
                      element2.reportDetail[0].response.show=true;
                  });
                }
                else{
                  if(element1['objectType'] == 'C' && element1.reportDetail != undefined)
                    element1.reportDetail[0].response.show=true;
                }
              });
            });
          },500);
        }
      }
    });
    this.column = JSON.parse(JSON.stringify(column['default']))
    this.reportRequestData = JSON.parse(JSON.stringify(reportRequest))
    this.drillDownRequestData = JSON.parse(JSON.stringify(drillDownRequest))
    this.selectedReport = this.globalService.selectedReport;
    this.selectedReportId = this.globalService.selectedReport.reportId;
    this.selectedReportType = this.globalService.selectedReport.reportType;
    this.scalingFactor = !isNullOrUndefined(this.globalService.selectedReport.scalingFactor) ?
      this.globalService.selectedReport.scalingFactor : '';
      this.scalingList.forEach(ele=> {
        if(this.scalingFactor == ele.alphaSubTab)
        ele.active = true;
        else
        ele.active = false
      });
    this.reportOrientationPrompt = this.globalService.selectedReport.reportOrientation == 'L' ?
      true : false;
    this.menuclick(this.selectedReport);
    // setTimeout(() => {
    //   const mlHeight = document.querySelectorAll('.report-slider-top .item2').forEach((ele) => {
    //     let aa = ele.closest("ng-multiselect-dropdown").nextElementSibling.getBoundingClientRect().top;
    //     if (aa < 310) {
    //       ele['style']['max-height'] = `calc(68vh - ${aa}px)`;
    //     }
    //     else {
    //       ele.closest("ng-multiselect-dropdown .dropdown-list")['style']['bottom'] = "30px";
    //     }
    //   });
    // }, 1200)
  }

  heightOfRow(data) {
    if (!isNullOrUndefined(data)) {
      if (data.length >= 5) {
        return {
          "height": 72 / data.length + "%"
        }
      }
      else {
        return
      }
    }
  }

  /* Component Funtion */

  dateChange() {
    let tabFilterValue = this.tablist[this.selectedReportId]["tabFilterValue"];
    let dateArr = tabFilterValue.filter((item) => item.filterType == 'DATE');
    if (dateArr.length) {
      dateArr.forEach(element => {
        this.filterForm.get(`filter${element.filterSeq}Val`).valueChanges.pipe(
          distinctUntilChanged(),
          pairwise() // gets a pair of old and new value
        ).subscribe(([oldValue, newValue]) => {
          if (element.dependencyFlag == 'Y') {
            if (dateArr.length >= 2 && this.submittedOnce) {
              this.compareTwoDates(dateArr[0].filterSeq, dateArr[1].filterSeq);
            }
          }
          else {
            if (element.filterLabel == 'From Date') {
              this.compareDates(element.filterSeq, element.filterSeq + 1);
            }
          }
        })
      });
    }
  }

  compareTwoDates(startIndex, endIndex) {
    if (new Date(this.filterForm.getRawValue()[`filter${endIndex}Val`]) < new Date(this.filterForm.getRawValue()[`filter${startIndex}Val`])) {
      this.filterForm.controls[`filter${endIndex}Val`].setErrors({ 'endDate': true });
      return false
    }
    else {
      this.filterForm.controls[`filter${endIndex}Val`].setErrors(null);
    }
  }

  compareDates(startIndex, endIndex) {
    if (new Date(this.filterForm.getRawValue()[`filter${endIndex}Val`]) < new Date(this.filterForm.getRawValue()[`filter${startIndex}Val`])) {
      this.filterForm.controls[`filter${endIndex}Val`].setErrors({ 'notDependency': true });
      return false
    }
    else {
      this.filterForm.controls[`filter${endIndex}Val`].setErrors(null);
    }
  }

  fetchObjData(datas) {
    let objData;
    if (this.selectedReportType == 'I') {
      if (this.selectedReport.templateId == 'MULTIPLE') {
        this.tablist[this.selectedReportId].tabDetail.forEach(element => {
          element['group'].forEach((element1, index1) => {
            if (element1.tileSequenceId == datas) {
              objData = element1.reportDetail;
            }
          });
        });
      }
      else {
        this.tablist[this.selectedReportId].tabDetail.forEach(element => {
          element['group'].forEach((element1, index1) => {
            if (element1.tileSequenceId == 'group3') {
              element1.group.forEach(ele => {
                if (ele.tileSequenceId == datas) {
                  objData = ele.reportDetail;
                }
              })
            }
            else {
              if (element1.tileSequenceId == datas) {
                objData = element1.reportDetail;
              }
            }
          });
        });
      }

    }
    else {
      objData = this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"];
    }
    return !isNullOrUndefined(objData) ? objData : [];
  }

  fetchClass(datas) {
    let objData;
    if (this.selectedReportType == 'I') {
      this.tablist[this.selectedReportId].tabDetail.forEach(element => {
        element['group'].forEach((element1, index1) => {
          if (element1.tileSequenceId == 'group3') {
            element1.group.forEach(ele => {
              if (ele.tileSequenceId == datas) {
                objData = ele;
              }
            })
          }
          else {
            if (element1.tileSequenceId == datas) {
              objData = element1;
            }
          }
        });
      });
    }
    else {
      objData = this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"];
    }
    return !isNullOrUndefined(objData) ? objData : [];
  }
  /* Component Funtion */

  get promptArray() { return this.filterForm.controls; }

  @HostListener('window: resize') adjustContents() {
    //this.contentAreaHeight();
    this.sideFilterPosition();
    //this.dashboardContHgt();
    this.sideFilterHeight();
    // this.profitValuePosInTile();
  }

  sideFilterHeight() {
    setTimeout(() => {
      const dashboardHoldHeight = window.innerHeight;
      const filterHeaderHeight = document.getElementById('filterHeader')? +document.getElementById('filterHeader').offsetHeight: null;
      const filterFooterHeight = document.getElementById('filterFooter')? +document.getElementById('filterFooter').offsetHeight: null;
      if(dashboardHoldHeight && filterHeaderHeight && filterFooterHeight){
        document.getElementById('side-filter-hold').style.height = `${dashboardHoldHeight - 80}px`;
        const setFilterBodyHeight = dashboardHoldHeight - (filterHeaderHeight + filterFooterHeight);
        document.getElementById('filterBody').style.cssText = `height: ${setFilterBodyHeight}px;
                                                              max-height: ${setFilterBodyHeight}px;
                                                              margin-bottom: ${filterFooterHeight}px`;

      }
     }, 180);
  }
  sideFilterPosition() {
    const sideFilterHold = document.getElementById('side-filter-hold');
    if(sideFilterHold){
      const filterHoldWidth = document.getElementById('side-filter-hold').offsetWidth;
      sideFilterHold.style.setProperty('--filterWidth', `-${filterHoldWidth}px`);
    }
}
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  getStyle(colType) {
    if (colType == 'N') {
      return {
        'text-align': 'right'
      };
    } else if (colType == 'G') {
      return {
        'font-weight': 'bold'
      };
    }
  }

  promptValuesReturn(index) {
    return `filter${index}Val`;
  }

  promptValueOption(seq) {
    //this.overallPromptValues[seq].sort((a,b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    return this.overallPromptValues[seq]
  }

  dropdownCheck(data, event, type?) {
    if (data.dependencyFlag == 'Y') {
      let magnifier = 0;
      let dropdownFlag = false;
      let dataValue;
      let dependentPrompt = data.dependentPrompt.split(",")
      dependentPrompt.forEach((element1, index1) => {
        this.tablist[this.selectedReportId].tabFilterValue.forEach((element, index) => {
          if (element.filterSeq == element1) {
            dataValue = this.filterForm.getRawValue()[`filter${element.filterSeq}Val`];
            if (element.filterType == 'DATE') {
              let val = this.dateReConversion(dataValue, element.filterDateFormat);
              dataValue = val;
              data[`filter${element1}Val`] = `'${val}'`;
            }
            if (dataValue.length) {
              if (Array.isArray(dataValue)) {
                if (!isNullOrUndefined(dataValue[0].id) && dataValue[0].id != '' && dataValue[0].id != 'ALL') {
                  data[`filter${element1}Val`] = this.arraySrtingCon(dataValue);
                  if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
                  // if (event.target.closest('.report-filter')) {
                  dropdownFlag = true;
                  }
                }
                else {
                  magnifier++
                  this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
                }
              }
            }
            else {
              magnifier++
              Object.keys(this.filterVal).forEach(element => {
                this.filterVal[element].forEach(element1 => {
                  if (element1.filterSeq == data.filterSeq) {
                    this.overallPromptValues[data.filterSeq] = [];
                  }
                });
              });
              this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
            }
          }
        });
      });
      if (type == 'TEXTM' && magnifier == 0) {
        this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
      }
      if (type == 'TREE' && magnifier == 0) {
        this.openFilterTreePopup(data, event);
      }
      if (dropdownFlag && dataValue.length) {
        data['defaultValueId'] = '';
        this._apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
          let promptArr = []
          Object.keys(resp['response']).forEach((element, index1) => {
            promptArr.push(
              {
                id:element.split('@')[1],
                description: resp['response'][element]
              }
            )
          });
          Object.keys(this.filterVal).forEach(element => {
            this.filterVal[element].forEach(element1 => {
              if (element1.filterSeq == data.filterSeq) {
                this.overallPromptValues[data.filterSeq] = promptArr;
              }
            });
          });
        });
      }
    }
    else {
      if (data.filterType == "COMBO" || data.filterType == "TEXTD") {
        if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
      // if (event.target.closest('.report-filter')) {
        data['defaultValueId'] = '';
          this._apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
            let promptArr = []
            Object.keys(resp['response']).forEach((element, index1) => {
              promptArr.push(
                {
                  id: element.split('@')[1],
                  description: resp['response'][element]
                }
              )
            });
            Object.keys(this.filterVal).forEach(element => {
              this.filterVal[element].forEach(element1 => {
                if (element1.filterSeq == data.filterSeq) {
                  this.overallPromptValues[data.filterSeq] = promptArr;
                }
              });
            });
          });
        }
      }
      else {
        if (data.filterType == "TEXTM") {
          this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
        }
        if (data.filterType == 'TREE') {
          this.openFilterTreePopup(data, event);
        }
      }

    }
  }

  getObjectValue(data) {
    return Object.values(data);
  }

  checkWidth(data) {
    let reqData = [];
    data.forEach(element => {
      reqData.push(element)
    });
    return {
      width: (100 / reqData.length) + '%'
    };
  }

  arraySrtingCon(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`'${element["id"]}'`)
    });
    return arr.toString();
  }

  arraySrtingConDes(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`${element["description"]}`)
    });
    return arr.toString();
  }

  dateReConversion(date, format) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",][myDate.getMonth()];
      if (format == 'YYYY') {
        str = myDate.getFullYear();
      }
      else if (format == 'Mon-YYYY') {
        str = month + "-" + myDate.getFullYear();
      }
      else if (format == 'YYYYMM') {
        str = myDate.getFullYear() + ("0" + (myDate.getMonth() + 1)).slice(-2);
      }
      else {
        str = myDate.getDate() + "-" + month + "-" + myDate.getFullYear();
      }

      return str;
    }
    else {
      return "";
    }

  }




  groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  rowHeight(res) {
    return {
      "height": res.rowHeight
    }
  }

  update(event, data, type) {
    if(data.ddFlag == 'Y'){
      let parentTitle = data.breadCrumbTilte != '' ? data.breadCrumbTilte + ' - ' : '';
      let ddkey;
        let dataIndex = event.dataObj.dataIndex;
        this.zone.run(() => {
          if (isNullOrUndefined(data['chartData']['dataset'])) {
            ddkey = data['chartData']['data'][dataIndex].ddkey
          }
          else {
            let datasetIndex = event.dataObj.datasetIndex;
            ddkey = data['chartData']['dataset'][datasetIndex]["data"][dataIndex].ddkey
          }
          this.drillDownHitChart(ddkey, data, parentTitle + event.dataObj.categoryLabel);
        });
    }
    else {
      let parentTitle = data.breadCrumbTilte != '' ? data.breadCrumbTilte + ' - ' : '';
      let ddkey;
      this.drillDownKeys = {};
      if (type == 'C') {
        let dataIndex = event.dataObj.dataIndex;
        this.zone.run(() => {
          if (isNullOrUndefined(data['chartData']['dataset'])) {
            ddkey = data['chartData']['data'][dataIndex].ddkey
          }
          else {
            let datasetIndex = event.dataObj.datasetIndex;
            ddkey = data['chartData']['dataset'][datasetIndex]["data"][dataIndex].ddkey
          }
          this.initApiHitAfterDDKEY(ddkey, data, parentTitle + event.dataObj.categoryLabel);
        });
      }
      else {
        ddkey = event['DDKEYID'];
        let arrFilter = data['columnHeaderslst'].filter(item => item.drillDownLabel);
        let ddKeyLabel = arrFilter.length ? arrFilter[0].dbColumnName : 'DDKEYID';
        this.initApiHitAfterDDKEY(ddkey, data, parentTitle + event[ddKeyLabel]);
      }
    }
  }

  initApiHitAfterDDKEY(ddkey, data, ddKeyLabel) {
    this.interActiveRespone.forEach((element, index) => {
      if (element.subReportId != data.subReportId) {
        if (element.parentSubReportID == data.subReportId) {
          this.interActiveHit(element, ddkey, ddKeyLabel)
        }
      }
    });
  }

  // drillDownHit(ddkey, data, ddKeyLabel) {
  //   this.interActiveRespone.forEach((element, index) => {
  //     if (element.subReportId != data.subReportId) {
  //       // if (element.parentSubReportID == data.subReportId) {
  //         this.drillDownHitChart(element, ddkey, ddKeyLabel)
  //       // }
  //     }
  //   });
  // }

  drillDownHitChart(ddkey, data, ddKeyLabel) {
    let objData = this.fetchObjData(data.intReportSeq)
    if (objData.length) {
      objData[0]['response']['show'] = false;
    }
    let reportRequest = JSON.parse(JSON.stringify(this.reportRequestData))
    const sendReq = Object.keys(reportRequest);
    sendReq.forEach((element, index) => {
      reportRequest[element] = data[element];
    });
    if (this.tablist[this.selectedReportId]["tabFilterValue"].length) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
        values.forEach((element, promptIndex) => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO') {
              if (element1["multiSelect"] == 'Y') {
                reportRequest[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                this.promptValues[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                }
              }
            }
            if (element1['filterType'] == 'DATE') {
              if (element != '') {
                let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                reportRequest[`promptValue${element1.filterSeq}`] = `'${val}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
              }
              else {
                reportRequest[`promptValue${element1.filterSeq}`] = `''`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `''`;
              }

            }
            if (element1['filterType'] == 'TEXTD') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1 == 'TEXT') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1['filterType'] == 'TEXTM') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
            }
            if (element1['filterType'] == 'TREE') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
            }

          }
        });
      });
    }
    let tmpStr = `drillDownKey${this.ddKeysChart.count}`;
    this.ddKeysChart[tmpStr] = ddkey;
    Object.keys(this.ddKeysChart).forEach((key)=>{
      if(key != "count"){
        reportRequest[key] = this.ddKeysChart[key];
      }
    })
    let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
    this.tablist[this.selectedReportId]["tabFilterValue"].map(item=> `G${item.filterSeq}`).join("-"): '';
    reportRequest['filterPosition'] = filterPosition;
    this._apiService.post(environment.getRMReport, reportRequest).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.ddKeysChart.count++;
        if (objData.length) {
          objData[0]['response']['show'] = true;
        }
        resp['response']['show'] = true;
        if (resp['response'].objectType == 'C') {
          //resp['response']['responsedel'] = true

          // resp['response'].intReportSeq == 4 ? resp['response'].chartData =
          // `<chart plotFillAlpha="70" plotFillHoverColor="#6baa01" showPlotBorder="0" numDivlines="2" showValues="1" valueFontSize="10" showTrendlineLabels="0"  xAxisMinValue="0" xAxisMaxValue="100" xAxisName="Avg Bal (Bn)" yAxisName="EOP Bal (Bn)" theme="fusion"> <categories> <category label="Oct-2015" x="0"></category><category label="Nov-2015" x="64209.52"></category><category label="Dec-2015" x="62306.89"></category><category label="Jan-2016" x="65227.2"></category><category label="Feb-2016" x="65003.72"></category><category label="Mar-2016" x="64925.28"></category> </categories> <dataset color="426FC0"><set x="0" y="0"></set><set x="64209.52" y="64117.78"></set><set x="62306.89" y="65314.56"></set><set x="65227.2" y="67393.77"></set><set x="65003.72" y="65583.71"></set><set x="64925.28" y="66323.13"></set></dataset><trendlines><line startvalue="20000" endvalue="30000" istrendzone="1" color="#aaaaaa" alpha="14" /><line startvalue="10000" endvalue="20000" istrendzone="1" color="#aaaaaa" alpha="7" /></trendlines></chart>`
          // :''

          if (this.listLineType.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
          }
          else if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
          }
          resp['response'].chartData = this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
          if(!isNullOrUndefined(resp['response'].chartData['chart'])) {
            resp['response'].chartData['chart']['chartLeftMargin'] = 10;
            resp['response'].chartData['chart']['chartTopMargin'] = 15;
            resp['response'].chartData['chart']['chartRightMargin'] = 0;
            resp['response'].chartData['chart']['chartBottomMargin']=0;
          }

        }
        else {
          let objData = this.fetchObjData(data.intReportSeq);
          let classData = this.fetchClass(data.intReportSeq);
          objData[0].drillDownFlag = true;
          if (resp['status'] == 1) {
            let checkFlag = this.colWidthCheck(resp['response']["columnHeaderslst"]);
            if (checkFlag) {
              objData[0].drillDownList.push({
                id: parseInt(objData[0].drillDownList.length) + 1,
                tabName: ddkey,
                tabLabel: ddKeyLabel,
                tableDetail: { currentLevelFlag: true, intReportSeq: data.intReportSeq },
                message: 'Report Column Header Mismatch - Please Contact Admin'
              })
              objData[0].drillDownList.forEach((element, index) => {
                if (index == objData[0].drillDownList.length - 1) {
                  element.tableDetail.currentLevelFlag = true;
                }
                else {
                  element.tableDetail.currentLevelFlag = false;
                }
              });
              this.mismatch = true;
              this.globalService.showToastr.error('Report Column Header Mismatch - Please Contact Admin')
            }
            else {
              resp['response']['tileSequenceId'] = this.selectedReportType == 'I' ? parseInt(resp['response']['intReportSeq']) : 1;
              resp['response'].currentLevelFlag = true;
              resp['response'].currentLevel = resp['response']['currentLevel'],
                resp['response'].nextLevel = resp['response']['nextLevel'],
                resp['response']['title'] = this.selectedReport['reportTitle'];
              resp['response']['reportType'] = this.selectedReportType;
              resp['response']['templateId'] = this.selectedReport['templateId']
              resp['response']['showSearch'] = false;
              resp['response']['showSearchR'] = false;
              resp['response']['fontSize'] = this.selectedReportType == 'I' ?
                (classData.gridClass == 'classGridh50' || classData.gridClass == 'classGridh60' ||
                  classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30' ||
                  classData.gridClass == 'classGridh66' || classData.gridClass == 'classGridh25' ||
                  classData.gridClass == 'classGridh20') ? 'small' :
                  //(classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30') ? 'tiny' :
                  classData.gridClass == 'classGridh100' ? 'medium' : this.currentFontSize : this.currentFontSize;
              resp['response'].scrollYx = { x: 0, y: 0 };
              resp['response'].pageNum = 1;
              resp['response'].rowId = undefined;
              resp['response'].droppedItem = [];
              resp['response'].sortArray = [];
              resp['response'].filterData = [];
              resp['response']['groupingFlag'] = this.selectedReport.groupingFlag;
              resp['response']['maxRecords'] = this.selectedReport.maxRecords;
              resp['response']['resetEverything'] = false
              resp['response']['colArrayHeader'] = [];
              if (!isNullOrUndefined(resp['response']["gridDataSet"])) {
                resp['response']["columnHeaderslst"].forEach(element => {
                  element.id = element.dbColumnName != '' ? element.dbColumnName : false;
                  element.name = element.caption;
                  element.selected = true;
                  element.selected1 = true;
                  element.settingBox = false;
                  element.sorting = false;
                  element.sortValue = false;
                  element.pinning = 'center';
                  element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
                  element.grouping = false;
                  element.filterData = '';
                  element.dataType = element.colType;
                  element.colspan = element.colspan == 0 ? 1 : element.colspan;
                  element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
                });
                let headObj = this.groupBy(resp['response']["columnHeaderslst"], 'labelRowNum');
                Object.keys(headObj).forEach((item) => {
                  resp['response']['colArrayHeader'].push(headObj[item]);
                });
                // if (objData[0].drillDownList.length) {
                //   let list = objData[0].drillDownList;
                //   let index = list.findIndex(x => x.currentLevel == resp['response']['currentLevel']);
                //   if (index == -1) {
                //     list.push({
                //       id: parseInt(objData[0].drillDownList.length) + 1,
                //       tabName: '',
                //       tabLabel: '',
                //       tableDetail: resp['response'],
                //     })
                //   }
                //   else {
                //     list[index] = {
                //       id: parseInt(objData[0].drillDownList.length) + 1,
                //       tabName: '',
                //       tabLabel: '',
                //       tableDetail: resp['response'],
                //     };
                //     list = list.splice(parseInt(index) + 1, list.length - index)
                //   }
                //   list.forEach(element => {
                //     if (element.tableDetail.currentLevel == resp['response']['currentLevel']) {
                //       element.tableDetail.currentLevelFlag = true;
                //     }
                //     else {
                //       element.tableDetail.currentLevelFlag = false;
                //     }
                //   });
                // }
                // else {
                //   objData[0].drillDownList.push(
                //     {
                //       id: parseInt(objData[0].drillDownList.length) + 1,
                //       tabName: '',
                //       tabLabel: '',
                //       tableDetail: resp['response'],
                //     }
                //   )
                // }
              }
              // if (this.selectedReportType == 'I') {
              //   if (!isNullOrUndefined(resp['response']["gridDataSet"][0]['DDKEYID']) || !isNullOrUndefined(resp['response']["gridDataSet"][0]['DDKEY'])) {
              //     let ddkey = !isNullOrUndefined(resp['response']["gridDataSet"][0]['DDKEYID']) ? resp['response']["gridDataSet"][0]['DDKEYID'] : resp['response']["gridDataSet"][0]['DDKEY'];
              //     this.interActiveRespone.forEach((element, index) => {
              //       if (element.parentSubReportID == resp['response'].subReportId) {
              //         element.parentSeq = resp['response'].intReportSeq;
              //         this.interActiveHit(element, ddkey, "", true, resp['response']['tileSequenceId'])
              //       }
              //     });
              //   }
              // }
            }
          }
          else {
            objData[0].drillDownList.push({
              id: parseInt(objData[0].drillDownList.length) + 1,
              tabName: '',
              tabLabel: '',
              tableDetail: { currentLevelFlag: true, intReportSeq: data.intReportSeq },
              message: resp['message']
            })
            objData[0].drillDownList.forEach((element, index) => {
              if (index == objData[0].drillDownList.length - 1) {
                element.tableDetail.currentLevelFlag = true;
              }
              else {
                element.tableDetail.currentLevelFlag = false;
              }
            });
            this.globalService.showToastr.error(resp['message'])
          }
        }
        objData[0].drillDownList.push(
          {
            id: parseInt(objData[0].drillDownList.length) + 1,
            tabName: ddkey,
            tabLabel: ddKeyLabel,
            tableDetail: resp['response'],
          }
        )
        let list = objData[0].drillDownList;
        list.forEach(element => {
          if (element.tableDetail.currentLevel == resp['response']['currentLevel']) {
            element.tableDetail.currentLevelFlag = true;
          }
          else {
            element.tableDetail.currentLevelFlag = false;
          }
        });
        // if (ddKeyLabel) {
        //   resp['response'].breadCrumbTilte = ddKeyLabel
        // }
        // else {
        //   resp['response'].breadCrumbTilte = ddkey
        // }

        this.tablist[this.selectedReportId]['tabDetail'].forEach((element, index) => {
          element['group'].forEach((element1, index1) => {
            if (element1.tileSequenceId == parseInt(resp['response'].intReportSeq)) {
              // if (element1.subReportId == resp['response'].subReportId) {
              // let detailed = {
              //   response: resp['response'],
              //   drillDownFlag: false,
              //   drillDownList: [],
              //   message: resp['message']
              // }
              // element1["reportDetail"] = [detailed];
              // element1["parentSeq"] = data.parentSeq;
              element1['objectType'] = resp['response'].objectType
              // }
            }
            if (element1.tileSequenceId == 'group3') {
              element1.group.forEach(ele => {
                if (ele.tileSequenceId == parseInt(resp['response'].intReportSeq)) {
                  // if (ele.subReportId == resp['response'].subReportId) {
                  // let detailed = {
                  //   response: resp['response'],
                  //   drillDownFlag: false,
                  //   drillDownList: [],
                  //   message: resp['message']
                  // }
                  // ele["reportDetail"] = [detailed]
                  // ele["parentSeq"] = data.parentSeq;
                  ele['objectType'] = resp['response'].objectType
                  // }
                }
              })
            }
          });
        });
        // this.interActiveRespone.forEach(element => {
        //   if (element.subReportId != resp['response'].subReportId) {
        //     if (element.parentSubReportID == resp['response'].subReportId) {
        //       element.parentSeq = resp['response'].intReportSeq;
        //       element.show = false
        //       let ddkey1, ddKeyLabel = '';
        //       resp['response'].breadCrumbTilte != '' ? ddKeyLabel = ddKeyLabel + resp['response'].breadCrumbTilte + ' - ' : ddKeyLabel;
        //       if (resp['response'].objectType == 'C') {
        //         if (isNullOrUndefined(resp['response']['chartData']['dataset'])) {
        //           if (!isNullOrUndefined(resp['response']['chartData']['data'])) {
        //             ddkey1 = resp['response']['chartData']['data'][0].ddkey
        //             ddKeyLabel = ddKeyLabel + resp['response']['chartData']['data'][0].label
        //           }
        //         }
        //         else {
        //           ddkey1 = resp['response']['chartData']['dataset'][0]["data"][0].ddkey
        //           ddKeyLabel = ddKeyLabel + resp['response']['chartData']['categories'][0]["category"][0].label;
        //         }
        //       }
        //       else {
        //         let arrFilter = resp['response']['columnHeaderslst'].filter(items => items.drillDownLabel);
        //         let ddKeyLabel1 = arrFilter.length ? arrFilter[0].dbColumnName : 'DDKEYID';
        //         ddkey1 = resp['response']['gridDataSet'][0]["DDKEYID"];
        //         ddKeyLabel = ddKeyLabel + resp['response']['gridDataSet'][0][ddKeyLabel1];
        //       }
        //       this.interActiveHit(element, ddkey1, ddKeyLabel)
        //     }
        //   }
        // });
      }
      else {
        this.tablist[this.selectedReportId]['tabDetail'].forEach(element => {
          element['group'].forEach(element1 => {
            if (element1.tileSequenceId == data.intReportSeq) {
              element1['message'] = resp['message']
              element1["reportDetail"] = undefined;
            }
            if (element1.tileSequenceId == 'group3') {
              element1.group.forEach(ele => {
                if (ele.tileSequenceId == data.intReportSeq) {
                  ele['message'] = resp['message']
                  ele["reportDetail"] = undefined;
                }
              })
            }
          });
        });
      }
      this.tablist[this.selectedReportId].tabDetail.forEach(element => {
        element['group'].forEach((element1, index1) => {
          if (element1.tileSequenceId == 'group3') {
            element1.group.forEach(ele => {
              if (ele.parentSeq == data.intReportSeq) {
                ele["reportDetail"] = undefined;
              }
            })
          }
          else {
            if (element1.parentSeq == data.intReportSeq) {
              element1["reportDetail"] = undefined;
            }
          }
        });
      });
    })
  }
  pointerCheck(data) {
    let retStr = '';
    if (!isNullOrUndefined(data["DDKEYID"])) {
      retStr = retStr + ' crusorPointer'
    }
    return retStr;
  }
  colWidthCheck(data) {
    let checkFlag = 0
    data.forEach(element => {
      if (isNullOrUndefined(element.columnWidth) && element.colspan <= 1) {
        checkFlag++
      }
    });
    let temp = data.filter(item => item.colspan <= 1);
    let totalArray = temp.map(ele => parseFloat(ele.columnWidth));
    let treeColumnWidthSum = (totalArray.reduce((a, b) => a + b, 0)).toFixed(2);
    return checkFlag > 0 || Math.round(treeColumnWidthSum) > 100 ? true : false;
  }

  fontSizeReturn() {
    // if (this.tablist[this.selectedReportId].tabDetail.length) {
    //   let totalChar = 0
    //   if (this.tablist[this.selectedReportId].tabDetail[0].drillDownList.length) {
    //     this.tablist[this.selectedReportId].tabDetail[0].drillDownList.forEach(element => {
    //       if (isNullOrUndefined(element.tabName))
    //         totalChar += element.tabName.length
    //     });
    //   }
    //   if (totalChar > 100) {
    //     return {
    //       "font-size": (80 - (this.tablist[this.selectedReportId].tabDetail[0].drillDownList.length * 2)) + '%'
    //     }
    //   }
    // }
  }


  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight = !isNullOrUndefined(document.getElementsByClassName('navbar')) ?
        document.getElementsByClassName('navbar')[0].clientHeight : 0;
      this.leftContainerHeight = getContentAreaHeight + 'px';
      document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight + 8) + 'px';
      document.getElementById('card-body').children[0]['style'].height = '100%';
    }, 1);
  }


  @HostListener('window: resize', ['$event']) onResize(event) {
    if(this.tablist[this.selectedReportId].tabDetail.length){
      this.tablist[this.selectedReportId].tabDetail.forEach(element => {
        element['group'].forEach(element1 => {
          if(element1.tileSequenceId == "group3"){
            element1['group'].forEach(element2 => {
              if(element2['objectType'] == 'C' && element2.reportDetail != undefined)
                element2.reportDetail[0].response.show=false;
            });
          }
          else{
            if(element1['objectType'] == 'C' && element1.reportDetail != undefined)
              element1.reportDetail[0].response.show=false;
          }
        });
      });
      setTimeout(() => {
        this.tablist[this.selectedReportId].tabDetail.forEach(element => {
          element['group'].forEach(element1 => {
            if(element1.tileSequenceId == "group3"){
              element1['group'].forEach(element2 => {
                if(element2['objectType'] == 'C' && element2.reportDetail != undefined)
                  element2.reportDetail[0].response.show=true;
              });
            }
            else{
              if(element1['objectType'] == 'C' && element1.reportDetail != undefined)
                element1.reportDetail[0].response.show=true;
            }
          });
        });
        setTimeout(() => {
          const getContentAreaHeight = window.innerHeight;
          const navbarHeight = !isNullOrUndefined(document.getElementsByClassName('navbar')) ?
            document.getElementsByClassName('navbar')[0].clientHeight : 0;
          this.leftContainerHeight = getContentAreaHeight + 'px';
          document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight + 8) + 'px';
          document.getElementById('card-body').children[0]['style'].height = '100%';
        })
      },500);
    }
    const getContentAreaHeight = window.innerHeight;
    const navbarHeight = !isNullOrUndefined(document.getElementsByClassName('navbar')) ?
      document.getElementsByClassName('navbar')[0].clientHeight : 0;
    this.leftContainerHeight = getContentAreaHeight + 'px';
    document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight + 8) + 'px';
    document.getElementById('card-body').children[0]['style'].height = '100%';
  }

  @HostListener('document:click', ['$event']) closeSerach(event) {
    if (!event.target.closest('.repprtScreenCss .nav-item') && !event.target.closest('ngb-datepicker')) {
      this.showDownloadDropdown = false;
      this.showViewDropdown = false;
      this.showZoomDropdown = false;
      this.showInfoDropdown = false;
      this.showBeforeDownload = false;
      for(let i=0; i<10; i++){
        this.downloaddropDownObj[`dropdown${i}`] = false;
      }
      for(let i=0; i<10; i++){
        this.downloaddropDownObj[`info${i}`] = false;
      }
    }
    if (!event.target.closest('.outside-click')) {
      Object.keys(this.dropdownSetting).forEach(drop=>{
          this.dropdownSetting[drop] = false;
      })
    }
    if (event.target.closest('main_id') || event.target.closest('.nav-serach')
      //event.target.classList.contains('showSearch') || event.target.classList.contains('fa-search')
      || event.target.tagName == 'INPUT') {
      return
    }
    else {
      if (this.tablist[this.selectedReportId]["tabDetail"][0]) {
        if (this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]) {
          if (this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"]) {
            let report = this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"][0];
            if (report['drillDownFlag']) {
              report['drillDownList'].forEach((ele) => {
                ele['tableDetail']['showSearch'] = false;
                ele['tableDetail']['showSearchR'] = false;
              })
            }
            report['response']['showSearch'] = false;
            report['response']['showSearchR'] = false;
          }
        }
      }
    }
  }

  filterSlide() {
    this.slideTrueTop = !this.slideTrueTop;
  }

  filterClose() {
    this.slideTrueTop = false;
    if (!this.repertGenSuc) {
      this.globalService.pageTitle = "Home";
      this.router.navigate(['/main/VisionEdwAdmin']);
    }
  }

  openCustomerMagnifierPopup(ind, event, title1) {
    event.srcElement.blur();
    event.preventDefault();
    let query;
    let tilte;
    Object.keys(this.filterVal).forEach(element => {
      this.filterVal[element].forEach(element1 => {
        if (element1.filterSeq == ind) {
          query = element1['filterSourceId'];
          tilte = element1['filterLabel'];
        }
      });
    });

    let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              promptM[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              promptM[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    promptM['query'] = query;
    const modelRef = this.modalService.open(MagnifierComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    modelRef.componentInstance.query = promptM;
    modelRef.componentInstance.title = title1;
    modelRef.componentInstance.filterData.subscribe((e) => {
      //let filteSeq = this.tablist[this.selectedReportId]["tabFilterValue"][ind]['filterSeq'];
      let str = `filter${ind}Val`;
      this.filterForm.patchValue({
        [str]: e.columnTen
      })
      this.magnifierId[str] = e.columnNine;
      //this.tablist[this.selectedReportId]["tabFilterSelectedValue"][ind] = e.columnNine;
      modelRef.close();
    });
  }
  openFilterTreePopup(data, event) {
    event.srcElement.blur();
    event.preventDefault();
    // let query;
    // let tilte;
    // Object.keys(this.filterVal).forEach(element => {
    //   this.filterVal[element].forEach(element1 => {
    //     if (element1.filterSeq == data.filterSeq) {
    //       query = element1['filterSourceId'];
    //       tilte = element1['filterLabel'];
    //     }
    //   });
    // });

    //let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              data[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              data[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    const modelRef = this.modalService.open(GenericTreeComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    document.body.classList.add('tree-popup');
    modelRef.componentInstance.title = data.filterLabel;
    modelRef.componentInstance.query = data;
    modelRef.componentInstance.selectedValue = this.treeId[`filter${data.filterSeq}Val`]
    modelRef.componentInstance.filterData.subscribe((e) => {
      if (e != 'close') {
        let str = `filter${data.filterSeq}Val`;
        this.filterForm.patchValue({
          [str]: e.field2
        })
        this.treeId[str] = e.field1;
      }
      document.body.classList.remove('tree-popup');
      modelRef.close();
    });
  }

  classReturn() {
    let retClass = ''
    if (this.slideTrueTop) {
      retClass += ' slideDashRightTransform';
    }
    else {
      retClass += '';
    }
    return retClass;
  }


  dataRangeLabel(data) {
    return data.filterLabel
  }

  export(type, viewList, mode?) {
    if (this.conditionForDowload) {
      let objData;
      let response;
      response = this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"];
      if (!response[0].drillDownList.length) {
        objData = response[0].response;
      }
      else {
        response[0].drillDownList.forEach(element => {
          if (element.tableDetail.currentLevelFlag) {
            objData = element.tableDetail;
          }
        });
      }

      let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
      //if (type == 'download') {
      type == 'download' ? this.showDownloadDropdown = false : this.showViewDropdown = false;
      let sendReq: boolean = type == 'view' ? viewList == 'xlsx' ? false : true : true;
      //let orientation = objData.reportOrientation == 'L' ? true : false;
      if (sendReq) {
        let drillDownReq;
        if (objData.currentLevel > 1) {
          let drillDownRequest = JSON.parse(JSON.stringify(this.drillDownRequestData))
          const sendReq = Object.keys(drillDownRequest);
          const drillDownKeys = Object.keys(this.drillDownKeys);
          const promptValues = Object.keys(this.promptValues);
          sendReq.forEach((element, index) => {
            drillDownRequest[element] = this.selectedReport[element];
            drillDownKeys.forEach((element1, index1) => {
              if (element1 == element) {
                drillDownRequest[element] = this.drillDownKeys[element]
              }
            });
            promptValues.forEach((element1, index1) => {
              if (element1 == element) {
                drillDownRequest[element] = this.promptValues[element]
              }
            });
          });
          drillDownReq = drillDownRequest;
        }
        let req = objData.currentLevel > 1 ? drillDownReq : this.exportRequest;
        // let rawVal = this.filterForm.getRawValue();
        // let dateArr = this.tablist[this.selectedReportId]["tabFilterValue"].filter(item => item.filterType == 'DATE');
        // if (viewList == 'pdf') {
        //   if (dateArr.length) {
        //     if (dateArr.length == 1) {
        //       let val = this.dateReConversion(rawVal[`filter${dateArr[0].filterSeq}Val`], dateArr[0].filterDateFormat);
        //       req['promptLabel'] = `Report Date : ${val}`;

        //     }
        //     else {
        //       let valFrom = this.dateReConversion(rawVal[`filter${dateArr[0].filterSeq}Val`], dateArr[0].filterDateFormat);
        //       let valTo = this.dateReConversion(rawVal[`filter${dateArr[1].filterSeq}Val`], dateArr[1].filterDateFormat);
        //       req['promptLabel'] = `From Date : ${valFrom} | To Date : ${valTo}`;
        //     }
        //   }
        // }
          const rawVal = this.filterForm.getRawValue();
          const values = Object.keys(rawVal);
          this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
            values.forEach((element, promptIndex) => {
              if (element == `filter${element1.filterSeq}Val`) {
                if (element1['filterType'] == 'COMBO') {
                  if (element1["multiSelect"] == 'Y') {
                    this.promptDescriptions[`promptDescription${element1.filterSeq}`] = this.arraySrtingConDes(rawVal[element]);
                  }
                  else {
                    if (rawVal[element][0]) {
                      this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element][0]['description']}`;
                    }
                  }
                }
                if (element1['filterType'] == 'DATE') {
                  if (element != '') {
                    let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                    this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${val}`;
                  }
                  else {
                    this.promptDescriptions[`promptDescription${element1.filterSeq}`] = ``;
                  }

                }
                if (element1['filterType'] == 'TEXTD') {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
                }
                if (element1 == 'TEXT') {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
                }
                if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
                }
                if (element1['filterType'] == 'TEXTM') {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
                }
                if (element1['filterType'] == 'TREE') {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
                }
              }
            });
          });
          const keys = Object.keys(this.promptDescriptions);
          let promptArr = [];
          this.tablist[this.selectedReportId]["tabFilterValue"].forEach((label)=>{
            keys.forEach((prompt)=>{
            if(`promptDescription${label.filterSeq}` == prompt)
            promptArr.push(`${label.filterLabel} : ${this.promptDescriptions[prompt]}`)
            })
          })

          if (objData.currentLevel > 1) {
          req['currentPage'] = 1;
          let drillLabel = '';
          response[0].drillDownList.forEach((element, ind) => {
            drillLabel += `${element.tabLabel} : ${element.tabName.trim()}`
            if (response[0].drillDownList.length - 1 != ind) {
              drillLabel += ' | ';
            }
          })
          // req['drillDownLabel'] = drillLabel;
          promptArr.push(drillLabel);
          req['currentLevel'] = objData.currentLevel - 1;
          req['nextLevel'] = (isNullOrUndefined(objData.nextLevel) || objData.nextLevel == "" || parseInt(objData.nextLevel) == 0) ?
            parseInt(objData.currentLevel) : objData.nextLevel - 1;
        }
        let scale = this.scalingFactor == '1' ? "No Scaling" : this.scalingFactor == '1000' ? "In Thousand's" :
          this.scalingFactor == '1000000' ? "In Million's" : this.scalingFactor == '1000000000' ? "In Billion's" : "";
          promptArr.push(`Scaling Factor : ${scale}`);
          req['promptLabel'] = promptArr.join('!@#');
        req['reportTitle'] = this.selectedReport["reportTitle"];
        if (!isNullOrUndefined(objData['droppedItem'])) {
          if(objData['droppedItem'].length) {
            let filteredItem = (objData['droppedItem']).map(ele => ele.name);
            req['screenGroupColumn'] = filteredItem.join('!@#')
          }
          else {
            req['screenGroupColumn'] = "";
          }
        }
        req['screenSortColumn'] = '';
        req['smartSearchOpt'] = [];
        if(objData['sortArray'].length){
          req['screenSortColumn'] = 'ORDER BY ';
          objData['sortArray'].forEach((ele, ind) => {
            req['screenSortColumn'] += `${ele.column} ${ele.type.toUpperCase()}`;
            if (objData['sortArray'].length - 1 != ind) {
              req['screenSortColumn'] += ',';
            }
          });
        }
        if(objData['filterData'].length){
          objData['filterData'].forEach((ele) => {
            req['smartSearchOpt'].push({ object: ele.id, criteria: ele.type, value: ele.filterData })
          })
        }
        if(!isNullOrUndefined(objData['displayColumn']) && objData['displayColumn'].length) {
          let filteredItem = (objData['displayColumn']).map(ele => ele.dbColumnName);
          req['columnsToHide'] = filteredItem.join('!@#')
        }
        else {
          let arr = [];
          objData.columnHeaderslst.forEach(element => {
            if((element.colspan <= 1 && !element.selected)){
              arr.push(element)
            }
          });
          let filteredItem = arr.map(ele => ele.dbColumnName);
          req['columnsToHide'] = filteredItem.join('!@#')
        }
        req['scalingFactor'] = this.scalingFactor;
        //req['smartSearchOpt'] = this.smartSearchOpt;
        req['reportOrientation'] = mode;
        let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
          this.tablist[this.selectedReportId]["tabFilterValue"].map(item => `G${item.filterSeq}`).join("-") : '';
          req['filterPosition'] = filterPosition;
        let header = objData.columnHeaderslst.filter(item=> item.colspan <= 1 && item.selected);
        let colTypeT = header.filter(item=> (item.colType == 'T' || item.colType == 'TR' || item.colType == 'NR'));
        let colTypeN = header.filter(item=> (item.colType == 'I' || item.colType == 'S' || item.colType == 'N'));
        let colTypeDimension = colTypeT && colTypeT.length ? colTypeT.map(item=> item.dbColumnName).join(',') :'';
        req['showDimensions'] = colTypeDimension;
        let groupMeasure = '';
        if(colTypeN && colTypeN.length)
        colTypeN.forEach((ele, ind)=> {
          groupMeasure += `SUM(${ele.dbColumnName}) ${ele.dbColumnName}`;
          if (colTypeN.length - 1 != ind) {
            groupMeasure += ',';
          }
        })
        let colTypeMeasure = colTypeN && colTypeN.length ? colTypeN.map(item=> item.dbColumnName).join(',') : '';
        req['showMeasures'] = this.applyGroupingFlag && groupMeasure != '' ? groupMeasure :
        colTypeMeasure;
        req['applyGrouping'] = this.applyGroupingFlag ? 'Y' : 'N';
        req['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
        type == 'view' ? this.viewList = viewList : '';
        type == 'view' ? viewList == 'xlsx' ? this.viwerGrid = true : this.viwerGrid = false : '';
        let api = this.selectedReport.templateId == 'MULTIPLE' ? viewList == 'xlsx' ? environment.multipleExcelExport : environment.multiplePdfExport
          : viewList == 'xlsx' ? environment.getRMreportExcelExport : viewList == 'csv' ? environment.reportExportToCsv :environment.getRMreportPdfExport;




        this._apiService.fileDownloads(api, req).subscribe((resp) => {
          if (resp['type'] != 'application/json') {
            const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
            const url = window.URL.createObjectURL(blob);
            if (type == 'download') {
              const link = this.downloadZipLink.nativeElement;
              link.href = url;
              link.download = viewList == 'xlsx' ? `${req.reportTitle}_${visionId}.xlsx` :
                viewList == 'csv' ? `${req.reportTitle}_${visionId}.csv` :
                  `${req.reportTitle}_${visionId}.pdf`;
              link.click();
            }
            if (type == 'view' && viewList == 'pdf') {
              if (this.selectedReport.templateId != 'MULTIPLE') {
                document.querySelector("iframe").src = url;
              }
              else {
                let placeholder = document.getElementById('reportPlaceholder1');
                placeholder['src'] = url;
              }
              //objData.resetEverything = false;
              //objData['droppedItem'] = this.droppedItem;
              //objData['sorting'] = this.sortedArray;
              //objData['filterData'] = this.filterData;
              // let placeholder = document.getElementById('reportPlaceholder')
              // let iframe = document.createElement('iframe');
              // document.querySelector("iframe").src = url;
              // document.getElementsByTagName("title")[0].innerHTML = 'some title';
              // iframe.style.width = '100%'
              // iframe.style.height = '100%'
              // if (placeholder.firstChild) {
              //   placeholder.removeChild(placeholder.firstChild);
              // }
              //placeholder.appendChild(iframe);
            }
            window.URL.revokeObjectURL(url);
          }
          else {
            this.globalService.showToastr.error("No Records Found")
          }
        }, (error: any) => {
          if (error.substr(12, 3) == '204') {
            this.globalService.showToastr.error('No Records to Export');
          }
          if (error.substr(12, 3) == '420') {
            this.globalService.showToastr.error('Error Generating Report');
          }
          if (error.substr(12, 3) == '417') {
            this.globalService.showToastr.error('Error while exporting the report');
          }
        });
      }
      else {
        type == 'view' ? this.viewList = viewList : '';
        type == 'view' ? viewList == 'xlsx' ? this.viwerGrid = true : this.viwerGrid = false : '';
      }
      //}
    }
    else {
      this.globalService.showToastr.warning('This Feature Work-in-Progress')
    }
  }

  exportBefore(type, viewList, mode?) {
    if (this.filterForm.invalid) {
      this.filterSubmitted = true
    }
    else {
      this.submittedOnce = true;
      let tabFilterValue = this.tablist[this.selectedReportId]["tabFilterValue"];
      let dateArr = tabFilterValue.filter((item) => item.filterType == 'DATE');
      if (dateArr.length) {
        dateArr.forEach(element => {
          if (element.dependencyFlag == 'Y') {
            this.compareTwoDates(element.dependentPrompt, element.filterSeq);
          }
          else {
            if (element.filterLabel == 'From Date') {
              this.compareDates(element.filterSeq, element.filterSeq + 1);
            }
          }
        });
        if (this.filterForm.valid) {
          this.getReportInPrompt('report', type, viewList, mode)
        }
      }
      else {
        this.getReportInPrompt('report', type, viewList, mode)
      }
    }
  }

  getReportInPrompt(value, type, viewList, mode?) {
    if (value == 'report') {
      this.viewList = 'xlsx';
      this.viwerGrid = true;
      let reportRequest = JSON.parse(JSON.stringify(this.reportRequestData))
      const sendReq = Object.keys(reportRequest)
      sendReq.forEach((element, index) => {
        element == 'nextLevel' ? reportRequest[element] = 1 : reportRequest[element] = this.selectedReport[element];
      });
      if (this.tablist[this.selectedReportId]["tabFilterValue"].length) {
        const rawVal = this.filterForm.getRawValue();
        const values = Object.keys(rawVal);
        this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
          values.forEach((element, promptIndex) => {
            if (element == `filter${element1.filterSeq}Val`) {
              if (element1['filterType'] == 'COMBO') {
                if (element1["multiSelect"] == 'Y') {
                  reportRequest[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                  this.promptValues[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                }
                else {
                  if (rawVal[element][0]) {
                    reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                    this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                  }
                }
              }
              if (element1['filterType'] == 'DATE') {
                if (element != '') {
                  let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                  reportRequest[`promptValue${element1.filterSeq}`] = `'${val}'`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
                }
                else {
                  reportRequest[`promptValue${element1.filterSeq}`] = `''`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `''`;
                }

              }
              if (element1['filterType'] == 'TEXTD') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              }
              if (element1 == 'TEXT') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              }
              if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              }
              if (element1['filterType'] == 'TEXTM') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
              }
              if (element1['filterType'] == 'TREE') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
              }
            }
          });
        });
      }
      reportRequest['scalingFactor'] = this.scalingFactor;
      reportRequest['reportOrientation'] = mode;
        const rawVal = this.filterForm.getRawValue();
        const values = Object.keys(rawVal);
        this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
          values.forEach((element, promptIndex) => {
            if (element == `filter${element1.filterSeq}Val`) {
              if (element1['filterType'] == 'COMBO') {
                if (element1["multiSelect"] == 'Y') {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = this.arraySrtingConDes(rawVal[element]);
                }
                else {
                  if (rawVal[element][0]) {
                    this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element][0]['description']}`;
                  }
                }
              }
              if (element1['filterType'] == 'DATE') {
                if (element != '') {
                  let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${val}`;
                }
                else {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = ``;
                }

              }
              if (element1['filterType'] == 'TEXTD') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              }
              if (element1 == 'TEXT') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              }
              if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              }
              if (element1['filterType'] == 'TEXTM') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              }
              if (element1['filterType'] == 'TREE') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              }
            }
          });
        });
        const keys = Object.keys(this.promptDescriptions);
        let promptArr = [];
        this.tablist[this.selectedReportId]["tabFilterValue"].forEach((label)=>{
          keys.forEach((prompt)=>{
          if(`promptDescription${label.filterSeq}` == prompt)
          promptArr.push(`${label.filterLabel} : ${this.promptDescriptions[prompt]}`)
          })
        })
        let scale = this.scalingFactor == '1' ? "No Scaling" : this.scalingFactor == '1000' ? "In Thousand's" :
        this.scalingFactor == '1000000' ? "In Million's" : this.scalingFactor == '1000000000' ? "In Billion's" : "";
        promptArr.push(`Scaling Factor : ${scale}`);
        reportRequest['promptLabel'] = promptArr.join('!@#');
      let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
      this.showBeforeDownload = false;
      let api;
      if (this.selectedReportType == 'CB') {
        api = environment.getCBReportExport;
        reportRequest['templateId'] = this.selectedReport.templateId;
        reportRequest['reportType'] = this.selectedReportType;
      }
      else {
        api = this.selectedReport.templateId == 'MULTIPLE' ? viewList == 'xlsx' ? environment.multipleExcelExport : environment.multiplePdfExport
          : viewList == 'xlsx' ? environment.getRMreportExcelExport : viewList == 'csv' ? environment.reportExportToCsv: environment.getRMreportPdfExport;
      }
      let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
          this.tablist[this.selectedReportId]["tabFilterValue"].map(item => `G${item.filterSeq}`).join("-") : '';
      reportRequest['filterPosition'] = filterPosition;
      reportRequest['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
      this._apiService.fileDownloads(api, reportRequest).subscribe((resp) => {
        if (resp['type'] != 'application/json') {
          const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
          const url = window.URL.createObjectURL(blob);
          if (type == 'download') {
            const link = this.downloadZipLink.nativeElement;
            link.href = url;
            link.download = viewList == 'xlsx' ? `${reportRequest.reportTitle}_${visionId}.xlsx` :
             viewList == 'csv' ? `${reportRequest.reportTitle}_${visionId}.csv`:
              `${reportRequest.reportTitle}_${visionId}.pdf`;
            link.click();
          }
          window.URL.revokeObjectURL(url);
        }
        else {
          this.globalService.showToastr.error("No Records Found")
        }
      }, (error: any) => {
        if (error.substr(12, 3) == '204') {
          this.globalService.showToastr.error('No Records to Export');
        }
        if (error.substr(12, 3) == '420') {
          this.globalService.showToastr.error('Error Generating Report');
        }
        if (error.substr(12, 3) == '417') {
          this.globalService.showToastr.error('Error while exporting the report');
        }
      });
      // this.apiCall(this.selectedReport).subscribe(result => this.resultDataCall(result, 0))
    }
  }

  getFilterLabel(seq) {
    let fill = this.tablist[this.selectedReportId]["tabFilterValue"].filter((item) => item.filterSeq == seq)
    return fill[0].filterLabel
  }

  showSearchRes() {
    let report = this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"][0];
    if (report['drillDownFlag']) {
      report['drillDownList'].forEach((ele) => {
        ele['tableDetail']['showSearch'] = !ele['tableDetail']['showSearch'];
        ele['tableDetail']['showSearchR'] = !ele['tableDetail']['showSearchR'];
      })
    }
    report['response']['showSearch'] = !report['response']['showSearch'];
    setTimeout(() => {
      report['response']['showSearchR'] = !report['response']['showSearchR'];
    }, 120);
    this.showDownloadDropdown = false;
    this.showViewDropdown = false;
    this.showZoomDropdown = false;
    this.showInfoDropdown = false;
    this.showBeforeDownload = false;
  }

  setEmptyDepent(data) {
    let filterPrompt = this.tablist[this.selectedReportId]["tabFilterValue"];
    filterPrompt.forEach(element => {
      if (element.dependencyFlag == 'Y') {
        let dependentPrompt = element.dependentPrompt.split(",");
        let filterSeq = data.filterSeq.toString();
        if (dependentPrompt.includes(filterSeq)) {
          let str = `filter${element.filterSeq}Val`;
          if (element.filterType == 'COMBO') {
            this.filterForm.patchValue({
              [str]: []
            })
          }
          else {
            this.filterForm.patchValue({
              [str]: ''
            })
          }

        }
      }
    });
  }

  toggleShow(event, dropId, data?) {
    if (dropId == 'showDownloadDropdown') {
      this.showDownloadDropdown = !this.showDownloadDropdown;
      this.showViewDropdown = false;
      this.showZoomDropdown = false;
      this.showBeforeDownload = false;
      this.showInfoDropdown = false;
    }
    if (dropId === 'showViewDropdown') {
      this.showViewDropdown = !this.showViewDropdown;
      this.showDownloadDropdown = false;
      this.showZoomDropdown = false;
      this.showBeforeDownload = false;
      this.showInfoDropdown = false;
    }
    if (dropId === 'showZoomDropdown') {
      this.showZoomDropdown = !this.showZoomDropdown;
      this.showDownloadDropdown = false;
      this.showViewDropdown = false;
      this.showBeforeDownload = false;
      this.showInfoDropdown = false;
    }
    if (dropId === 'showInfoDropdown') {
      let objData;
      let response;
      response = this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"];
      if (!response[0].drillDownList.length) {
        objData = response[0].response;
      }
      else {
        response[0].drillDownList.forEach(element => {
          if (element.tableDetail.currentLevelFlag) {
            objData = element.tableDetail;
          }
        });
      }
      this.showInfoDropdown = !this.showInfoDropdown;
      this.showDownloadDropdown = false;
      this.showViewDropdown = false;
      this.showBeforeDownload = false;
      this.showZoomDropdown = false;
    }
    if (dropId === 'showBeforeDownload') {
      if (this.filterForm.invalid) {
        this.filterSubmitted = true;
        this.showBeforeDownload = false;
      }
      else {
        this.submittedOnce = true;
        let tabFilterValue = this.tablist[this.selectedReportId]["tabFilterValue"];
        let dateArr = tabFilterValue.filter((item) => item.filterType == 'DATE');
        if (dateArr.length) {
          dateArr.forEach(element => {
            if (element.dependencyFlag == 'Y') {
              this.compareTwoDates(element.dependentPrompt, element.filterSeq);
            }
            else {
              if (element.filterLabel == 'From Date') {
                this.compareDates(element.filterSeq, element.filterSeq + 1);
              }
            }
          });
        }
        if (this.filterForm.valid) {
          this.showBeforeDownload = !this.showBeforeDownload;
          this.showDownloadDropdown = false;
          this.showViewDropdown = false;
          this.showZoomDropdown = false;
          this.showInfoDropdown = false;
        }
      }
    }
  }

  toggleShowDrop(event, dropId) {
    Object.keys(this.dropdownSetting).forEach(drop=>{
      if(dropId == drop)
        this.dropdownSetting[drop] = true;
      else
        this.dropdownSetting[drop] = false;
    })
    if(dropId == 'showScalingOption'){
      if(this.scalingFactor != '0')
        this.dropdownSetting['showScalingOption'] = true;
      else
        this.dropdownSetting['showScalingOption'] = false;
    }
    if(dropId == 'showViewOption'){
      if(this.tablist[this.selectedReportId].tabDetail && this.tablist[this.selectedReportId].tabDetail.length
         && this.selectedReportType != 'CB')
        this.dropdownSetting['showViewOption'] = true;
      else
        this.dropdownSetting['showViewOption'] = false;
    }
    if(dropId == 'showExportOption' || dropId == 'showZoomOption'){
      if(this.selectedReportType != 'CB')
        this.dropdownSetting[dropId] = true;
      else
        this.dropdownSetting[dropId] = false;
    }
  }

  toggleShowDropPDF(event, dropId) {
    this.dropdownSetting.showPdfOption = !this.dropdownSetting.showPdfOption;
  }

  checkInfo(){
    let objData;
    let response;
    response = this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"];
    if (!response[0].drillDownList.length) {
      objData = response[0].response;
    }
    else {
      response[0].drillDownList.forEach(element => {
        if (element.tableDetail.currentLevelFlag) {
          objData = element.tableDetail;
        }
      });
    }
    if(!isNullOrUndefined(objData)){
      if(!isNullOrUndefined(objData['reportInfo']) && objData['reportInfo'] != ''){
        // const blob = new Blob([objData['reportInfo']], { type: 'text/xls' });
        // const url = objData['reportInfo']
        // let placeholder = document.getElementById('iframe-info');
        //     placeholder['src'] = url;
        //           var iframe = document.createElement('iframe');
        // var html = '<body>Foo</body>';
        // placeholder['src'] = 'data:text/html;charset=utf-8,' + encodeURI(objData['reportInfo']);
// document.body.appendChild(iframe);
        this.reportInfo = objData['reportInfo']
        return true
      }
      else {
        return false
      }
    }
    else {
      return false
    }
  }

  changeFont(type) {
    this.currentViewIcon = type.svg;
    this.fontSizeList.forEach((ele) => {
      ele.active = false;
    });
    type.active = true;
    let report = this.tablist[this.selectedReportId]["tabDetail"][0]["group"][0]["reportDetail"][0];
    if (report['drillDownFlag']) {
      report['drillDownList'].forEach((ele) => {
        ele['tableDetail']['fontSize'] = type.name;
      })
    }
    report['response']['fontSize'] = type.name;
    this.currentFontSize = type.name;
    this.showZoomDropdown = false;
  }

  interActiveFont(columns, title, breadCrumb) {
    // let check = false;
    // let checkFont = false;
    // let checkSmallFont = false;
    // let checkTinyFont = false;
    // this.interActiceReportDetail.forEach((element) => {
    //   element['group'].forEach((elem) => {
    //     let titleLength;
    //     if (elem.reportTitle) {
    //       titleLength = elem.reportTitle.length;
    //     }
    //     let breadCrumbLength;
    //     if (typeof this.interActiveObj[elem.subReportId] == "object") {
    //       breadCrumbLength = this.interActiveObj[elem.subReportId].breadCrumbTilte.length;
    //     }
    //     let totalChar = titleLength + breadCrumbLength;
    //     if (totalChar > 35 && totalChar <= 45) {
    //       checkFont = true;
    //     }
    //     if (totalChar > 45 && totalChar <= 53) {
    //       check = true;
    //     }
    //     if (totalChar > 53) {
    //       checkTinyFont = true;
    //     }
    //   })
    // })
    // if (!check && checkFont) {
    //   return {
    //     "font-size": "12px"
    //   }
    // }
    // if (check && !checkTinyFont) {
    //   return {
    //     "font-size": "11px"
    //   }
    // }
    // if (checkTinyFont) {
    //   return {
    //     "font-size": "10px"
    //   }
    // }
  }

  findMyBrowser() {
    if (navigator.userAgent.indexOf("Firefox") != -1) {
      return true;
    }
    else {
      return false;
    }
  }


  /* Selected Report Function */

  menuclick(list) {
    this.tablist[list.reportId] = {
      reportId: list.reportId,
      reportTitle: list.reportTitle,
      tabFilterValue: "",
      tabDetail: [],
      reportType: list.reportType,
    }
    this.selectedReportType == 'I' ? this.jwtService.set('reportTemplate', this.selectedReport.templateId)
      : this.jwtService.set('reportTemplate', 'DEFAULT')
      this.jwtService.set('reportType', this.selectedReport.reportType)
    if (this.selectedReportType == 'I') {
      this.tableSettingJson.advanceSettings = 'N';
    }
    if (this.selectedReport.templateId == 'MULTIPLE') {
      this.tableSettingJson.pagination = 'N';
    }
    this.getFilterList(list);
    this.slideTrueTop = true;
    this.globalService.pageTitle = 'Report/' + list.reportTitle;
  }

  /* Selected Report Function */

  /* Fetch Report Filter List Function */

  getFilterList(list) {
    var data = {
      reportId: list.reportId,
      reportTitle: list.reportTitle,
      reportDescription: list.reportDescription,
      filterFlag: list.filterFlag,
      filterRefCode: list.filterRefCode,
      subSequence: list.subSequence,
      reportQuery: list.reportQuery
    };
    if (list.filterFlag == 'Y') {
      this._apiService.post(environment.getReportFilterList, data).subscribe((resp) => {
        if (resp['status'] == 1) {
          resp['response'].forEach(element => {
            if (element.filterType == 'DATE') {
              let dateRestrict = element.filterDateRestrict.split(',');
              let year = dateRestrict[0].split(':');
              let month = dateRestrict[1].split(':');
              let date = dateRestrict[2].split(':');
              let pastYear = year[0].slice(0, -1);
              let futureYear = year[1].slice(0, -1);
              let pastMonth = month[0].slice(0, -1);
              let futureMonth = month[1].slice(0, -1);
              let pastDate = date[0].slice(0, -1);
              let futureDate = date[1].slice(0, -1);
              let businessdates = JSON.parse(this.jwtService.get('businessDate'));
              let countrySplit = date[2] ? date[2].split("_") : [];
              if (countrySplit.length && countrySplit[1]) {
                businessdates.forEach(dateObj => {
                  if (dateObj.COUNTRY == countrySplit[1]) {
                    let isVBD = countrySplit.includes("VBD") ? true : false;
                    let isVBM = countrySplit.includes("VBM") ? true : false;
                    let isVBW = countrySplit.includes("VBW") ? true : false;
                    let isVBQ = countrySplit.includes("VBQ") ? true : false;
                    let today = new Date();
                    let todayYear = today.getFullYear();
                    let todayMonth = today.getMonth();
                    let todayDay = today.getDate();
                    if (isVBD || isVBW || isVBM || isVBQ) {
                      let maxDate: string = isVBD ? dateObj['VBD'].toString().replaceAll("-", " ") :
                        isVBW ? dateObj['VBW'].toString().replaceAll("-", " ") :
                          isVBM ? dateObj['VBM'].toString().replaceAll("-", " ") :
                            isVBQ ? dateObj['VBQ'].toString().replaceAll("-", " ") : '';
                      isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                      let vbdDate = new Date(maxDate);
                      let vbdYear = vbdDate.getFullYear();
                      let vbdMonth = vbdDate.getMonth();
                      let vbdDay = vbdDate.getDate();
                      element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                        new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                      element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                        new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                    }
                    else {
                      element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                      element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                    }
                  }
                });
              }
              else {
                let isVBD = date.includes("VBD") ? true : false;
                let isVBM = date.includes("VBM") ? true : false;
                let isVBW = date.includes("VBW") ? true : false;
                let isVBQ = date.includes("VBQ") ? true : false;
                let today = new Date();
                let todayYear = today.getFullYear();
                let todayMonth = today.getMonth();
                let todayDay = today.getDate();
                if (isVBD || isVBW || isVBM || isVBQ) {
                  // let dates: any = Object.values(businessdates);
                  // let maxDate = Math.max(...dates.map(e => new Date(e.toString().replaceAll("-", " "))));
                  let maxDate: string = isVBD ? businessdates[0]['VBD'].toString().replaceAll("-", " ") :
                    isVBW ? businessdates[0]['VBW'].toString().replaceAll("-", " ") :
                      isVBM ? businessdates[0]['VBM'].toString().replaceAll("-", " ") :
                        isVBQ ? businessdates[0]['VBQ'].toString().replaceAll("-", " ") : '';
                  isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                  let vbdDate = new Date(maxDate);
                  let vbdYear = vbdDate.getFullYear();
                  let vbdMonth = vbdDate.getMonth();
                  let vbdDay = vbdDate.getDate();
                  element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                    new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                  element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                    new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                }
                else {
                  element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                  element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                }
              }

            }
          });
          this.filterVal = this.groupBy(resp['response'], 'filterRow');
          this.tablist[list.reportId]["tabFilterValue"] = resp['response'];
          this.filterPrompt();
        }
      })
    }
    else {
      this.tablist[list.reportId]["tabFilterValue"] = []
      this.getReport('report');
    }
  }

  promptMore() {
    this.showOtherFilters = !this.showOtherFilters;
  }

  /* Fetch Report Filter List Function */

  /* Forming Report Filter Form Funtion */

  filterPrompt() {
    let pushData = {}
    this.tablist[this.selectedReportId]["tabFilterSelectedValue"] = {}
    this.tablist[this.selectedReportId]["tabFilterValue"].forEach((element, objIndex) => {
      let tempArr = [];
      const isObjEmpty = Object.entries(element.filterSourceVal).length === 0 && element.filterSourceVal.constructor === Object;
      if (isObjEmpty) {
        this.overallPromptValues[element.filterSeq] = tempArr;
      } else {
        const data = element.filterSourceVal;
        Object.keys(data).forEach(ele => {
          tempArr.push({ id: ele, description: data[ele] });
        })
        this.overallPromptValues[element.filterSeq] = tempArr;
      }

      if (element.filterType == 'COMBO') {
        if (Object.keys(element['filterDefaultValue']).length) {
          const dataDefault = element['filterDefaultValue'];
          let tempDefault = [];
          Object.keys(dataDefault).forEach(ele => {
            tempDefault.push({ id: ele, description: dataDefault[ele] });
          })
          pushData[`filter${element.filterSeq}Val`] = new FormControl(tempDefault, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl([], Validators.required);
        }
      }
      if (element.filterType == 'DATE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          // let date = element['filterDefaultValue'].split('-');
          let dateVal = Object.keys(element['filterDefaultValue'])[0].split("-").join("/");
          if (element.filterDateFormat == "Mon-YYYY") {
            dateVal = `01 ${dateVal}`
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          //pushData[`filter${element.filterSeq}Val`] = new FormControl({ year: +date[2], month: this.numWithMonth[date[1]], day: +date[0] }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }

      if (element.filterType == 'DATERANGE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          // let date = element['filterDefaultValue'].split('-');
          let dateVal = Object.keys(element['filterDefaultValue'])[0].split("-").join("/");
          if (element.filterDateFormat == "Mon-YYYY") {
            dateVal = `01 ${dateVal}`
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          // pushData[`filter${element.filterSeq}Val`] = new FormControl({ year: +date[2], month: this.numWithMonth[date[1]], day: +date[0] }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }
      if (!['COMBO', 'DATE'].includes(element.filterType)) {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.keys(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTM') {
        if (Object.keys(element['filterDefaultValue']).length) {
          this.magnifierId[`filter${element.filterSeq}Val`] = Object.keys(element['filterDefaultValue']).join(",");
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.values(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          this.magnifierId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TREE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          this.treeId[`filter${element.filterSeq}Val`] = Object.keys(element['filterDefaultValue']).join(",");
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.values(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          this.treeId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXT') {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.keys(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTD') {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: Object.keys(element['filterDefaultValue']).join(","), disabled: true }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: "", disabled: true }, Validators.required);
        }
      }
    });
    this.filterForm = new FormGroup(pushData);
    this.dateChange();
    if(this.filterForm.valid){
      // setTimeout(() => {
      //   const mlHeight = document.querySelectorAll('.report-slider-top .item2').forEach((ele) => {
      //     let aa = ele.closest("ng-multiselect-dropdown").nextElementSibling.getBoundingClientRect().top;
      //     if (aa < 70) {
      //       ele['style']['max-height'] = `calc(41vh - ${aa}px)`;
      //     }
      //     else {
      //       ele.closest("ng-multiselect-dropdown .dropdown-list")['style']['bottom'] = "30px";
      //     }
      //   });
      // }, 1200)
      this.filterSubmit('report');
    }
  }

  /* Forming Report Filter Form Function */

  /* Report Filter Form Submit Function */

  filterSubmit(type) {
    this.ddKeysChart = { count: 1}
    if (this.filterForm.invalid) {
      this.filterSubmitted = true
      this.showOtherFilters = true;
    }
    else {
      this.submittedOnce = true;
      let tabFilterValue = this.tablist[this.selectedReportId]["tabFilterValue"];
      let dateArr = tabFilterValue.filter((item) => item.filterType == 'DATE');
      if (dateArr.length) {
        dateArr.forEach(element => {
          if (element.dependencyFlag == 'Y') {
            this.compareTwoDates(element.dependentPrompt, element.filterSeq);
          }
          else {
            if (element.filterLabel == 'From Date') {
              this.compareDates(element.filterSeq, element.filterSeq + 1);
            }
          }
        });
        if (this.filterForm.valid) {
          this.getReport(type)
        }
      }
      else {
        this.getReport(type)
      }
    }
  }

  /* Report Filter Form Submit Function */

  /* Get Both Interactive and Normal Report */

  getReport(type) {
    this.slideTrueTop = false;
    this.repertGenSuc = true;
    this.tableSettingJson.filter = 'Y';
    this.globalService.pageTitle = 'Reports/' + this.selectedReport.reportTitle;
    let columnsss = JSON.parse(JSON.stringify(this.column))
    if (type == 'report') {
      if (this.selectedReportType == 'I') {
        this.drillDownKeys = {};
        let sendData = {
          reportId: this.selectedReport['reportId'],
          reportTitle: this.selectedReport['reportTitle'],
          filterFlag: this.selectedReport['filterFlag'],
          filterRefCode: this.selectedReport['filterRefCode'],
          reportOrientation: this.selectedReport['reportOrientation'],
          reportType: this.selectedReportType,
          templateId: this.selectedReport['templateId'],
        }
        this._apiService.post(environment.getInterActive, sendData).subscribe((resp) => {
          if (this.selectedReport['templateId'] == 'MULTIPLE') {
            let createdMultipleTemp = [];
            for (let index = 1; index <= resp['response'].length; index++) {
              createdMultipleTemp.push({
                id: "group1",
                groupColumn: "col-sm-12 col-md-12 col-lg-12 col-xl-12",
                rowHeight: resp['response'].length == 1 ? "100%" : resp['response'].length == 2 ? "50%" : "40%",
                subGroup: "N",
                group: [
                  {
                    tileSequenceId: index,
                    columns: "col-sm-12 col-md-12 col-lg-12 col-xl-12",
                    gridClass: resp['response'].length != 1 ? "classGridh50" : "classGridh100",
                    reportDetail: [{ response: { show: false } }]

                  }
                ]
              })
            }
            this.tablist[this.selectedReportId]['tabDetail'] = createdMultipleTemp;
          }
          else {
            this.tablist[this.selectedReportId]['tabDetail'] = columnsss[this.selectedReport['templateId']];
          }
          this.interActiveRespone = resp['response'];
          this.tablist[this.selectedReportId]['tabDetail'].forEach(element => {
            element['group'].forEach(element1 => {
              resp['response'].forEach(element2 => {
                if (element1.tileSequenceId == 'group3') {
                  element1.group.forEach(ele => {
                    if (ele.tileSequenceId == element2.intReportSeq) {
                      ele.reportTitle = element2.reportTitle;
                      ele.objectType = element2.objectType;
                      ele.subReportId = element2.subReportId;
                      ele.reportDetail = [{ response: { show: false } }]
                    }
                  })
                }
                else {
                  if (element1.tileSequenceId == element2.intReportSeq) {
                    element1.reportTitle = element2.reportTitle;
                    element1.objectType = element2.objectType;
                    element1.subReportId = element2.subReportId;
                    element1.reportDetail = [{ response: { show: false } }]
                  }
                }
              })
            })
          });
          if (this.selectedReport['templateId'] == 'MULTIPLE') {
            this.initMultipleRequest(resp['response']);
          }
          else {
            this.initApiHit(resp['response']);
          }
        })
      }
      else {
        this.tablist[this.selectedReportId]['tabDetail'] = columnsss['DEFAULT'];
        this.tableData = [];
        this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["objectType"] = 'G';
        this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportTitle"] = this.selectedReport["reportTitle"];
        this.viewList = 'xlsx';
        this.viwerGrid = true;
        this.apiCall(this.selectedReport).subscribe(result => this.resultDataCall(result, 0))
      }
    }
  }

  /* Get Both Interactive and Normal Report */

  /* Normal Report Funtion */

  apiCall(item) {
    let reportRequest = JSON.parse(JSON.stringify(this.reportRequestData))
    const sendReq = Object.keys(reportRequest)
    sendReq.forEach((element, index) => {
      element == 'nextLevel' ? reportRequest[element] = 1 : reportRequest[element] = item[element];
    });
    if (this.tablist[this.selectedReportId]["tabFilterValue"].length) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
        values.forEach((element, promptIndex) => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO') {
              if (element1["multiSelect"] == 'Y') {
                reportRequest[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                this.promptValues[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                }
              }
            }
            if (element1['filterType'] == 'DATE') {
              if (element != '') {
                let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                reportRequest[`promptValue${element1.filterSeq}`] = `'${val}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
              }
              else {
                reportRequest[`promptValue${element1.filterSeq}`] = `''`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `''`;
              }

            }
            if (element1['filterType'] == 'TEXTD') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1 == 'TEXT') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1['filterType'] == 'TEXTM') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
            }
            if (element1['filterType'] == 'TREE') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
            }
          }
        });
      });
    }
    reportRequest['scalingFactor'] = this.scalingFactor;
    let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
    this.tablist[this.selectedReportId]["tabFilterValue"].map(item=> `G${item.filterSeq}`).join("-"): '';
    reportRequest['filterPosition'] = filterPosition;
    this.maintainData['reportRequest'] = reportRequest;
    this.exportRequest = reportRequest;
    return this._apiService.post(environment.getRMReport, reportRequest);
  }

  resultDataCall(data, ind) {
    this.showOtherFilters = false;
    this.disableScaling = false;
    if (data['status'] == 1) {
      data['response']['show'] = true;
      this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["objectType"] = 'G';
      this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["subReportId"] = data['response']['subReportId'];
      let checkFlag = this.colWidthCheck(data['response'].columnHeaderslst);
      if (!checkFlag) {
        data['response']['tileSequenceId'] = 1;
        data['response']['colArrayHeader'] = [];
        let headObj = this.groupBy(data['response'].columnHeaderslst, 'labelRowNum');
        Object.keys(headObj).forEach((item) => {
          data['response']['colArrayHeader'].push(headObj[item]);
        });
        if (!isNullOrUndefined(data.response["gridDataSet"])) {
          data['response'].columnHeaderslst.forEach(element => {
            element.id = element.dbColumnName != '' ? element.dbColumnName : false;
            element.name = element.caption;
            element.selected = true;
            element.selected1 = true;
            element.settingBox = false;
            element.sorting = false;
            element.sortValue = false;
            element.pinning = 'center';
            element.colspan = element.colspan == 0 ? 1 : element.colspan;
            element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
            element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
            element.grouping = false;
            element.filterData = '';
            element.dataType = element.colType;
          });
          data['response']['title'] = this.selectedReport['reportTitle'];
          data['response']['reportType'] = this.selectedReportType;
          data['response']['templateId'] = this.selectedReport['templateId']
          data['response']['showSearch'] = false;
          data['response']['showSearchR'] = false;
          data['response']['fontSize'] = 'medium';
          data['response']['groupingFlag'] = this.selectedReport.groupingFlag;
          data['response']['maxRecords'] = this.selectedReport.maxRecords;
          data['response']['scrollYx'] = { x: 0, y: 0 };
          data['response']['pageNum'] = 1;
          data['response']['rowId'] = undefined;
          if (!isNullOrUndefined(data['response']['reportUserDeflst']) &&
            data['response']['reportUserDeflst'].length) {
            data['response']['sortArray'] = [];
            data['response']['filterData'] = [];
            data['response']['droppedItem'] = [];
            let sortColumn = !isNullOrUndefined(data['response']['reportUserDeflst'][0].sortColumn) ?
              data['response']['reportUserDeflst'][0].sortColumn.replaceAll("ORDER BY ", "") : '';
            sortColumn.split(",").forEach(element => {
              if (element && element.length) {
                let subElem = element.split(" ");
                if(subElem.length && subElem[0] && subElem[1])
                data['response']['sortArray'].push({ column: subElem[0], type: subElem[1].toLowerCase() });
              }
            })
            let searchColumn = !isNullOrUndefined(data['response']['reportUserDeflst'][0].searchColumn) ?
              data['response']['reportUserDeflst'][0].searchColumn : '';
              searchColumn.split(",").forEach(element => {
              if (element && element.length) {
                let subElem = element.split("!@#");
                let dataType = data['response'].columnHeaderslst.filter(item => item.dbColumnName == subElem[0]);
                data['response']['filterData'].push({ id: subElem[0], type: subElem[1],
                   filterData: subElem[2], dataType:  dataType[0].colType});
              }
            })
            let columnsHide = !isNullOrUndefined(data['response']['reportUserDeflst'][0].columnsToHide) ?
            data['response']['reportUserDeflst'][0].columnsToHide : '';
            let groupingColumn = !isNullOrUndefined(data['response']['reportUserDeflst'][0].groupingColumn) ?
            data['response']['reportUserDeflst'][0].groupingColumn : '';
            data['response'].columnHeaderslst.forEach(head=>{
              columnsHide.split("!@#").forEach(ele=>{
                if(ele != '' && head.dbColumnName == ele){
                  head.selected = false;
                  head.selected1 = false;
                }
              })
              groupingColumn.split("!@#").forEach(ele=>{
                if(ele != '' && head.caption == ele && (data['response'].maxRecords >= data['response'].totalRows)){
                  data['response']['droppedItem'].push(head)
                }
              })
            })
            let applyGrouping = !isNullOrUndefined(data['response']['reportUserDeflst'][0].applyGrouping) ?
              data['response']['reportUserDeflst'][0].applyGrouping : '';
              this.applyGroupingFlag = applyGrouping == 'Y' ? true: false;
              data['response'].applyGroupingFlag = applyGrouping == 'Y' ? true: false;
          }
          else {
            data['response']['sortArray'] = [];
            data['response']['filterData'] = [];
            data['response']['droppedItem'] = [];
            data['response'].applyGroupingFlag = false;
          }
          // data['response']['droppedItem'] = [];
          // data['response']['sortArray'] = [];
          // data['response']['filterData'] = [];
          data['response']['resetEverything'] = false
          this.reportOrientation = data['response']['reportOrientation'] == 'L' ? true : false;
          let detailed = {
            response: data['response'],
            drillDownFlag: false,
            drillDownList: [],
            message: data['message']
          }
          this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"] = [detailed];
        }
      }
      else {
        this.mismatch = true;
        let detailed = {
          response: null,
          drillDownFlag: false,
          drillDownList: [],
          message: "Column width Configuration mismatch - Please Contact Admin"
        }
        this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["message"] = data['message']
        this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"] = [detailed];
        this.globalService.showToastr.error('Column width Configuration mismatch - Please Contact Admin')
      }
    }
    else {
      this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["message"] = data['message']
      this.globalService.showToastr.error(data['message'])
    }
    // else if (data['status'] == 41) {
    //   let detailed={
    //     response:data['response'],
    //     drillDownFlag:false,
    //     drillDownList:[],
    //     message: data['message']
    //   }
    //   this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"]=[detailed];
    // }

    // else {
    //   this.globalService.showToastr.error(data['message'])
    // }
  }

  /* Normal Report Funtion */

  /* Interactive Report Funtion */

  initApiHit(arrData) {
    arrData.forEach(element => {
      if (element.parentSubReportID == element.subReportId) {
        element.parentSeq = 0;
        element.show = false
        this.interActiveHit(element, '');
      }
    });
  }

  interActiveRequest(letdata, ddkey, ddKeyLabel?, flag?, tileSequenceId?, lastddFlag?) {
    let reportRequest = JSON.parse(JSON.stringify(this.reportRequestData))
    const sendReq = Object.keys(reportRequest)
    sendReq.forEach((element, index) => {
      reportRequest[element] = element == 'nextLevel' ? parseInt(letdata[element]) : letdata[element];
    });
    if (this.tablist[this.selectedReportId]["tabFilterValue"].length) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
        values.forEach((element, promptIndex) => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO') {
              if (element1["multiSelect"] == 'Y') {
                reportRequest[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                this.promptValues[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                }
              }
            }
            if (element1['filterType'] == 'DATE') {
              if (element != '') {
                let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                reportRequest[`promptValue${element1.filterSeq}`] = `'${val}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
              }
              else {
                reportRequest[`promptValue${element1.filterSeq}`] = `''`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `''`;
              }

            }
            if (element1['filterType'] == 'TEXTD') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1 == 'TEXT') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
            }
            if (element1['filterType'] == 'TEXTM') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
            }
            if (element1['filterType'] == 'TREE') {
              reportRequest[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
              this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
            }

          }
        });
      });
    }
    let tmpStr = `drillDownKey${parseInt(tileSequenceId)}`
    if (ddkey) {
      if (flag) {
        this.drillDownKeys[`drillDownKey0`] = `${this.drillDownKeys[`drillDownKey0`]}`;
        if (isNullOrUndefined(this.drillDownKeys[tmpStr])) {
          this.drillDownKeys[tmpStr] = `'${ddkey}'`;
        }
        else {
          this.drillDownKeys[tmpStr] = this.drillDownKeys[tmpStr].slice(0, -1)
          this.drillDownKeys[tmpStr] += `!@#${ddkey}'`;
        }
      }
      else if (lastddFlag) {
        this.drillDownKeys[`drillDownKey0`] = `${this.drillDownKeys[`drillDownKey0`]}`;
        let drillDownKeys = this.drillDownKeys[tmpStr];
        drillDownKeys = drillDownKeys.substring(1);
        drillDownKeys = drillDownKeys.slice(0, -1);
        drillDownKeys = drillDownKeys.split('!@#');
        drillDownKeys.pop();
        drillDownKeys.push(ddkey);
        drillDownKeys = drillDownKeys.join("!@#")
        this.drillDownKeys[tmpStr] = `'${drillDownKeys}'`;
      }
      else {
        this.drillDownKeys[`drillDownKey0`] = `'${ddkey}'`;
      }
      const drillDownKeys = Object.keys(this.drillDownKeys);
      drillDownKeys.forEach(element => {
        sendReq.forEach((element1, index) => {
          if (element1 == element) {
            reportRequest[element] = this.drillDownKeys[element];
          }
        });
      });
    }
    reportRequest["intReportSeq"] = letdata.intReportSeq;
    this.maintainData['reportRequest'] = reportRequest;
    return reportRequest;
  }

  replaceOccurrence(string, regex, n, replace) {
    var i = 0;
    return string.replace(regex, function (match) {
      i += 1;
      if (i === n) return replace;
      return match;
    });
  }

  interActiveHit(letdata, ddkey, ddKeyLabel?, flag?, tileSequenceId?, lastddFlag?) {
    let objData = this.fetchObjData(letdata.intReportSeq)
    if (objData.length) {
      objData[0]['response']['show'] = false;
    }
    let sendRequest = this.interActiveRequest(letdata, ddkey, ddKeyLabel, flag, tileSequenceId, lastddFlag);
    let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
    this.tablist[this.selectedReportId]["tabFilterValue"].map(item=> `G${item.filterSeq}`).join("-"): '';
    sendRequest['filterPosition'] = filterPosition;
    this._apiService.post(environment.getRMReport, sendRequest).subscribe((resp) => {
      if (resp['status'] == 1) {
        resp['response']['show'] = true;
        if (resp['response'].objectType == 'C') {
          //resp['response']['responsedel'] = true
          // resp['response'].intReportSeq == 4 ? resp['response'].chartData =
          // `<chart plotFillAlpha="70" plotFillHoverColor="#6baa01" showPlotBorder="0" numDivlines="2" showValues="1" valueFontSize="10" showTrendlineLabels="0"  xAxisMinValue="0" xAxisMaxValue="100" xAxisName="Avg Bal (Bn)" yAxisName="EOP Bal (Bn)" theme="fusion"> <categories> <category label="Oct-2015" x="0"></category><category label="Nov-2015" x="64209.52"></category><category label="Dec-2015" x="62306.89"></category><category label="Jan-2016" x="65227.2"></category><category label="Feb-2016" x="65003.72"></category><category label="Mar-2016" x="64925.28"></category> </categories> <dataset color="426FC0"><set x="0" y="0"></set><set x="64209.52" y="64117.78"></set><set x="62306.89" y="65314.56"></set><set x="65227.2" y="67393.77"></set><set x="65003.72" y="65583.71"></set><set x="64925.28" y="66323.13"></set></dataset><trendlines><line startvalue="20000" endvalue="30000" istrendzone="1" color="#aaaaaa" alpha="14" /><line startvalue="10000" endvalue="20000" istrendzone="1" color="#aaaaaa" alpha="7" /></trendlines></chart>`
          // :''

          if (this.listLineType.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line"') || '';
          }
          else if (this.listLineTypeDY.includes(resp['response']['chartType'])) {
            let data = resp['response']['chartData'];
            var regexp = /<dataset/g;
            let c = data.match(regexp) || [];
            resp['response'].chartData = this.replaceOccurrence(data, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
          }
          resp['response'].chartData = this.xmlToJSonService.conversionJson(resp['response'].chartData, resp['response'])
          if(!isNullOrUndefined(resp['response'].chartData['chart'])) {
            resp['response'].chartData['chart']['chartLeftMargin'] = 10;
            resp['response'].chartData['chart']['chartTopMargin'] = 15;
            resp['response'].chartData['chart']['chartRightMargin'] = 0;
            resp['response'].chartData['chart']['chartBottomMargin']=0;
          }


        }
        else {
          resp['response']['tileSequenceId'] = parseInt(resp['response'].intReportSeq);
          let classData = this.fetchClass(parseInt(resp['response'].intReportSeq));
          resp['response']['colArrayHeader'] = [];
          resp['response'].currentLevel = resp['response']['currentLevel'],
            resp['response'].nextLevel = resp['response']['nextLevel'],
            resp['response'].scrollYx = { x: 0, y: 0 };
          resp['response'].pageNum = 1;
          resp['response'].rowId = undefined;
          resp['response'].droppedItem = [];
          resp['response'].sortArray = [];
          resp['response'].filterData = [];
          resp['response']['title'] = this.selectedReport['reportTitle'];
          resp['response']['reportType'] = this.selectedReportType;
          resp['response']['templateId'] = this.selectedReport['templateId']
          resp['response']['showSearch'] = false;
          resp['response']['showSearchR'] = false;
          resp['response']['fontSize'] = (classData.gridClass == 'classGridh50' || classData.gridClass == 'classGridh60' ||
            classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30' || classData.gridClass == 'classGridh66' ||
             classData.gridClass == 'classGridh25' || classData.gridClass == 'classGridh20') ? 'small' :
            //(classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30') ? 'tiny' :
            classData.gridClass == 'classGridh100' ? 'medium' : 'small';
          resp['response']['groupingFlag'] = this.selectedReport.groupingFlag;
          resp['response']['maxRecords'] = this.selectedReport.maxRecords;
          resp['response']['resetEverything'] = false;
          let headObj = this.groupBy(resp['response']["columnHeaderslst"], 'labelRowNum');
          Object.keys(headObj).forEach((item) => {
            resp['response']['colArrayHeader'].push(headObj[item]);
          });
          resp['response']["columnHeaderslst"].forEach(element => {
            element.id = element.dbColumnName != '' ? element.dbColumnName : false;
            element.name = element.caption;
            element.selected = true;
            element.selected1 = true;
            element.settingBox = false;
            element.sorting = false;
            element.sortValue = false;
            element.pinning = 'center';
            element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
            element.grouping = false;
            element.filterData = '';
            element.dataType = element.colType;
            element.colspan = element.colspan == 0 ? 1 : element.colspan;
            element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
          });
        }
        if (ddKeyLabel) {
          resp['response'].breadCrumbTilte = ddKeyLabel
        }
        else {
          resp['response'].breadCrumbTilte = ddkey
        }
        this.tablist[this.selectedReportId]['tabDetail'].forEach((element, index) => {
          element['group'].forEach((element1, index1) => {
            if (element1.tileSequenceId == parseInt(resp['response'].intReportSeq)) {
              if (element1.subReportId == resp['response'].subReportId) {
                let detailed = {
                  response: resp['response'],
                  drillDownFlag: false,
                  drillDownList: [],
                  message: resp['message']
                }
                element1["reportDetail"] = [detailed];
                element1["parentSeq"] = letdata.parentSeq;
                element1["reportOrientation"] =  resp['response']['reportOrientation'] == 'L' ? true: false;
              }
            }
            if (element1.tileSequenceId == 'group3') {
              element1.group.forEach(ele => {
                if (ele.tileSequenceId == parseInt(resp['response'].intReportSeq)) {
                  if (ele.subReportId == resp['response'].subReportId) {
                    let detailed = {
                      response: resp['response'],
                      drillDownFlag: false,
                      drillDownList: [],
                      message: resp['message']
                    }
                    ele["reportDetail"] = [detailed]
                    ele["parentSeq"] = letdata.parentSeq;
                    ele["reportOrientation"] =  resp['response']['reportOrientation'] == 'L' ? true: false;
                  }
                }
              })
            }
          });
        });
        this.interActiveRespone.forEach(element => {
          if (element.subReportId != resp['response'].subReportId) {
            if (element.parentSubReportID == resp['response'].subReportId) {
              element.parentSeq = resp['response'].intReportSeq;
              element.show = false
              let ddkey1, ddKeyLabel = '';
              resp['response'].breadCrumbTilte != '' ? ddKeyLabel = ddKeyLabel + resp['response'].breadCrumbTilte + ' - ' : ddKeyLabel;
              if (resp['response'].objectType == 'C') {
                if (isNullOrUndefined(resp['response']['chartData']['dataset'])) {
                  if (!isNullOrUndefined(resp['response']['chartData']['data'])) {

                    ddkey1 = resp['response']['chartData']['data'][0].ddkey
                    ddKeyLabel = ddKeyLabel + resp['response']['chartData']['data'][0].label
                  }
                }
                else {
                  ddkey1 = resp['response']['chartData']['dataset'][0]["data"][0].ddkey
                  ddKeyLabel = ddKeyLabel + resp['response']['chartData']['categories'][0]["category"][0].label;
                }
              }
              else {
                let arrFilter = resp['response']['columnHeaderslst'].filter(items => items.drillDownLabel);
                let ddKeyLabel1 = arrFilter.length ? arrFilter[0].dbColumnName : 'DDKEYID';
                ddkey1 = resp['response']['gridDataSet'][0]["DDKEYID"];
                ddKeyLabel = ddKeyLabel + resp['response']['gridDataSet'][0][ddKeyLabel1];
              }
              this.interActiveHit(element, ddkey1, ddKeyLabel)
            }
          }
        });
      }
      else {
        this.tablist[this.selectedReportId]['tabDetail'].forEach(element => {
          element['group'].forEach(element1 => {
            if (element1.tileSequenceId == letdata.intReportSeq) {
              element1['message'] = resp['message']
              element1["reportDetail"] = undefined;
            }
            if (element1.tileSequenceId == 'group3') {
              element1.group.forEach(ele => {
                if (ele.tileSequenceId == letdata.intReportSeq) {
                  ele['message'] = resp['message']
                  ele["reportDetail"] = undefined;
                }
              })
            }
          });
        });
      }
      this.tablist[this.selectedReportId].tabDetail.forEach(element => {
        element['group'].forEach((element1, index1) => {
          if (element1.tileSequenceId == 'group3') {
            element1.group.forEach(ele => {
              if (ele.parentSeq == letdata.intReportSeq) {
                ele["reportDetail"] = undefined;
              }
            })
          }
          else {
            if (element1.parentSeq == letdata.intReportSeq) {
              element1["reportDetail"] = undefined;
            }
          }
        });
      });
    })
  }

  /* Interactive Report Funtion */

  /* Multiple Grid Function */

  initMultipleRequest(data) {
    data.forEach((mainelement, index1) => {
      let reportRequest = JSON.parse(JSON.stringify(this.reportRequestData))
      const sendReq = Object.keys(reportRequest)
      sendReq.forEach((element, index) => {
        reportRequest[element] = element == 'nextLevel' ? parseInt(index1) + 1 : mainelement[element];
      });
      if (this.tablist[this.selectedReportId]["tabFilterValue"].length) {
        const rawVal = this.filterForm.getRawValue();
        const values = Object.keys(rawVal);
        this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
          values.forEach((element, promptIndex) => {
            if (element == `filter${element1.filterSeq}Val`) {
              if (element1['filterType'] == 'COMBO') {
                if (element1["multiSelect"] == 'Y') {
                  reportRequest[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                  this.promptValues[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
                }
                else {
                  if (rawVal[element][0]) {
                    reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                    this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
                  }
                }
              }
              if (element1['filterType'] == 'DATE') {
                if (element != '') {
                  let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                  reportRequest[`promptValue${element1.filterSeq}`] = `'${val}'`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
                }
                else {
                  reportRequest[`promptValue${element1.filterSeq}`] = `''`;
                  this.promptValues[`promptValue${element1.filterSeq}`] = `''`;
                }

              }
              if (element1['filterType'] == 'TEXTD') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              }
              if (element1 == 'TEXT') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              }
              if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
              }
              if (element1['filterType'] == 'TEXTM') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
              }
              if (element1['filterType'] == 'TREE') {
                reportRequest[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
              }

            }
          });
        });
      }
      reportRequest["intReportSeq"] = index1;
      reportRequest['scalingFactor'] = this.scalingFactor;
      let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
      this.tablist[this.selectedReportId]["tabFilterValue"].map(item=> `G${item.filterSeq}`).join("-"): '';
      reportRequest['filterPosition'] = filterPosition;
      this.maintainData['reportRequest'] = reportRequest;
      this.exportRequest = reportRequest;
      this._apiService.post(environment.getRMReport, reportRequest).subscribe((resp) => {
        if (resp['status'] == 1 || (resp['status'] == 41 && !isNullOrUndefined(resp['response']))) {
          resp['response']['show'] = true;
          resp['response']['tileSequenceId'] = parseInt(resp['response'].intReportSeq);
          let classData = this.fetchClass(parseInt(resp['response'].intReportSeq));
          resp['response']['colArrayHeader'] = [];
          resp['response'].currentLevel = resp['response']['currentLevel'],
            resp['response'].nextLevel = resp['response']['nextLevel'],
            resp['response'].scrollYx = { x: 0, y: 0 };
          resp['response'].pageNum = 1;
          resp['response'].reportOrientation = mainelement.reportOrientation;
          resp['response'].rowId = undefined;
          resp['response'].droppedItem = [];
          resp['response'].sortArray = [];
          resp['response'].filterData = [];
          resp['response']['title'] = this.selectedReport['reportTitle'];
          resp['response']['reportType'] = this.selectedReportType;
          resp['response']['templateId'] = this.selectedReport['templateId']
          resp['response']['showSearch'] = false;
          resp['response']['showSearchR'] = false;
          resp['response']['fontSize'] = (classData.gridClass == 'classGridh50' || classData.gridClass == 'classGridh60' ||
            classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30' ||
            classData.gridClass == 'classGridh66' || classData.gridClass == 'classGridh25' ||
            classData.gridClass == 'classGridh20') ? 'small' :
            //(classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30') ? 'tiny' :
            classData.gridClass == 'classGridh100' ? 'medium' : 'small';
          resp['response']['groupingFlag'] = this.selectedReport.groupingFlag;
          resp['response']['maxRecords'] = this.selectedReport.maxRecords;
          resp['response']['resetEverything'] = false;
          this.reportOrientation = resp['response']['reportOrientation'] == 'L' ? true : false;
          let headObj = this.groupBy(resp['response']["columnHeaderslst"], 'labelRowNum');
          Object.keys(headObj).forEach((item) => {
            resp['response']['colArrayHeader'].push(headObj[item]);
          });
          resp['response']["columnHeaderslst"].forEach(element => {
            element.id = element.dbColumnName != '' ? element.dbColumnName : false;
            element.name = element.caption;
            element.selected = true;
            element.selected1 = true;
            element.settingBox = false;
            element.sorting = false;
            element.sortValue = false;
            element.pinning = 'center';
            element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
            element.grouping = false;
            element.filterData = '';
            element.dataType = element.colType;
            element.colspan = element.colspan == 0 ? 1 : element.colspan;
            element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
          });
          this.tablist[this.selectedReportId]['tabDetail'].forEach((element, index) => {
            element['group'].forEach((element1, index1) => {
              if (element1.tileSequenceId == parseInt(resp['otherInfo'].nextLevel)) {
                let detailed = {
                  response: resp['response'],
                  drillDownFlag: false,
                  drillDownList: [],
                  message: resp['message']
                }
                element1["reportDetail"] = [detailed];
                //element['rowHeight']=(resp['response']['gridDataSet'].length*22)+(resp['response']['colArrayHeader'].length*22)+'px';
                element1["parentSeq"] = mainelement.parentSeq;
              }
            });
          });
        }
        else {
          this.tablist[this.selectedReportId]['tabDetail'].forEach(element => {
            element['group'].forEach(element1 => {
              if (element1.tileSequenceId == resp['otherInfo'].nextLevel) {
                element1['message'] = resp['message']
                element1["reportDetail"] = undefined;
                // element['rowHeight']='50px';
              }
            });
          });
        }
      })
    });
  }

  resultDataCallSubmit(data) {
    if (data['status'] == 1 && !isNullOrUndefined(data['response'])) {
      if (parseInt(data['response']['currentLevel']) == 1) {
        data['response']['show'] = true;
        this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["objectType"] = 'G';
        this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["subReportId"] = data['response']['subReportId'];
        let checkFlag = this.colWidthCheck(data['response'].columnHeaderslst);
        if (!checkFlag) {
          data['response']['tileSequenceId'] = 1;
          data['response']['colArrayHeader'] = [];
          let headObj = this.groupBy(data['response'].columnHeaderslst, 'labelRowNum');
          Object.keys(headObj).forEach((item) => {
            data['response']['colArrayHeader'].push(headObj[item]);
          });
          if (!isNullOrUndefined(data.response["gridDataSet"])) {
            data['response'].columnHeaderslst.forEach(element => {
              element.id = element.dbColumnName != '' ? element.dbColumnName : false;
              element.name = element.caption;
              element.selected = true;
              element.selected1 = true;
              element.settingBox = false;
              element.sorting = false;
              element.sortValue = false;
              element.pinning = 'center';
              element.colspan = element.colspan == 0 ? 1 : element.colspan;
              element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
              element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
              element.grouping = false;
              element.filterData = '';
              element.dataType = element.colType;
            });
            data['response']['title'] = this.selectedReport['reportTitle'];
            data['response']['reportType'] = this.selectedReportType;
            data['response']['templateId'] = this.selectedReport['templateId']
            data['response']['showSearch'] = false;
            data['response']['showSearchR'] = false;
            data['response']['fontSize'] = 'medium';
            data['response']['groupingFlag'] = this.selectedReport.groupingFlag;
            data['response']['maxRecords'] = this.selectedReport.maxRecords;
            data['response']['scrollYx'] = { x: 0, y: 0 };
            data['response']['pageNum'] = 1;
            data['response']['rowId'] = undefined;
            data['response']['sortArray'] = [];
            data['response']['filterData'] = [];
            data['response']['droppedItem'] = [];
            data['response']['applyGroupingFlag'] = this.applyGroupingFlag;

            data['response']['resetEverything'] = false
            this.reportOrientation = data['response']['reportOrientation'] == 'L' ? true : false;
            let detailed = {
              response: data['response'],
              drillDownFlag: false,
              drillDownList: [],
              message: data['message']
            }
            this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"] = [detailed];
          }
        }
        else {
          this.mismatch = true;
          let detailed = {
            response: null,
            drillDownFlag: false,
            drillDownList: [],
            message: "Column width Configuration mismatch - Please Contact Admin"
          }
          this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["message"] = data['message']
          this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["reportDetail"] = [detailed];
          this.globalService.showToastr.error('Column width Configuration mismatch - Please Contact Admin')
        }
      }
      if (parseInt(data['response']['currentLevel']) > 1) {
        let objData = this.fetchObjData(data['response'].intReportSeq);
      let classData = this.fetchClass(data['response'].intReportSeq);
      objData[0].drillDownFlag = true;
      this.breadCrumbShow = true;
      this.crumbText = '';
      if (data['status'] == 1) {
        // let checkFlag = this.colWidthCheck(data['response']["columnHeaderslst"]);
        // if (checkFlag) {
        //   objData[0].drillDownList.push({
        //     id: parseInt(objData[0].drillDownList.length) + 1,
        //     tabName: data[breadCrumb],
        //     tabLabel: breadCrumbLabel,
        //     tableDetail: { currentLevelFlag: true, intReportSeq: datas.intReportSeq },
        //     message: 'Report Column Header Mismatch - Please Contact Admin'
        //   })
        //   objData[0].drillDownList.forEach((element, index) => {
        //     if (index == objData[0].drillDownList.length - 1) {
        //       element.tableDetail.currentLevelFlag = true;
        //     }
        //     else {
        //       element.tableDetail.currentLevelFlag = false;
        //     }
        //   });
        //   this.mismatch = true;
        //   this.globalService.showToastr.error('Report Column Header Mismatch - Please Contact Admin')
        // }
        // else {
          data['response']['tileSequenceId'] = this.selectedReportType == 'I' ? parseInt(data['response']['intReportSeq']) : 1;
          data['response'].currentLevelFlag = true;
          data['response'].currentLevel = data['response']['currentLevel'],
          data['response'].nextLevel = data['response']['nextLevel'],
          data['response']['title'] = this.selectedReport['reportTitle'];
          data['response']['reportType'] = this.selectedReportType;
          data['response']['templateId'] = this.selectedReport['templateId']
          data['response']['showSearch'] = false;
          data['response']['showSearchR'] = false;
          data['response']['fontSize'] = this.selectedReportType == 'I' ?
            (classData.gridClass == 'classGridh50' || classData.gridClass == 'classGridh60' ||
              classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30' ||
              classData.gridClass == 'classGridh66' || classData.gridClass == 'classGridh25' ||
              classData.gridClass == 'classGridh20') ? 'small' :
              //(classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30') ? 'tiny' :
              classData.gridClass == 'classGridh100' ? 'medium' : this.currentFontSize : this.currentFontSize;
          data['response'].scrollYx = { x: 0, y: 0 };
          data['response'].pageNum = 1;
          data['response'].rowId = undefined;
          data['response'].droppedItem = [];
          data['response'].sortArray = [];
          data['response'].filterData = [];
          data['response']['applyGroupingFlag'] = this.applyGroupingFlag;
          data['response']['groupingFlag'] = this.selectedReport.groupingFlag;
          data['response']['maxRecords'] = this.selectedReport.maxRecords;
          data['response']['resetEverything'] = false
          data['response']['colArrayHeader'] = [];
          if (!isNullOrUndefined(data['response']["gridDataSet"])) {
            data['response']["columnHeaderslst"].forEach(element => {
              element.id = element.dbColumnName != '' ? element.dbColumnName : false;
              element.name = element.caption;
              element.selected = true;
              element.selected1 = true;
              element.settingBox = false;
              element.sorting = false;
              element.sortValue = false;
              element.pinning = 'center';
              element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
              element.grouping = false;
              element.filterData = '';
              element.dataType = element.colType;
              element.colspan = element.colspan == 0 ? 1 : element.colspan;
              element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
            });
            let headObj = this.groupBy(data['response']["columnHeaderslst"], 'labelRowNum');
            Object.keys(headObj).forEach((item) => {
              data['response']['colArrayHeader'].push(headObj[item]);
            });

          objData[0].drillDownList[parseInt(data['response'].intReportSeq) - 2]['tableDetail'] = data['response'];

          }
        // }
      }
      else {
        // objData[0].drillDownList.push({
        //   id: parseInt(objData[0].drillDownList.length) + 1,
        //   tabName: data[breadCrumb],
        //   tabLabel: breadCrumbLabel,
        //   tableDetail: { currentLevelFlag: true, intReportSeq: datas.intReportSeq },
        //   message: data['message']
        // })
        let obj =  {
          currentLevelFlag: true,
          intReportSeq: data['response'].intReportSeq
        }
        objData[0].drillDownList[parseInt(data['response'].intReportSeq) - 2]['tableDetail'] = obj;
        objData[0].drillDownList[parseInt(data['response'].intReportSeq) - 2]['message'] = data['response']['message']
        objData[0].drillDownList.forEach((element, index) => {
          if (index == objData[0].drillDownList.length - 1) {
            element.tableDetail.currentLevelFlag = true;
          }
          else {
            element.tableDetail.currentLevelFlag = false;
          }
        });
        this.globalService.showToastr.error(data['message'])
      }
      }
    }
    else {
      this.tablist[this.selectedReportId].tabDetail[0]['group'][0]["message"] = data['message']
      // this.globalService.showToastr.error(data['message'])
    }
  }
  /* Multiple Grid Function */

  /* DrillDown request For Both Normal and Interactive Report */

  drillDownDataRequest(datas) {
    let response;
    let objData;
    if (datas.type != 'breadCrumb' && datas.type != 'search' &&
    datas.type != 'closeFilterPrompt' && datas.type != 'applyGroupingFlag' && datas.type != 'advanceSubmit') {
      objData = this.fetchObjData(datas.intReportSeq);
      if (!objData[0]["drillDownList"].length) {
        response = objData[0]["response"];
      }
      else {
        response = objData[0]["drillDownList"][objData[0]["drillDownList"].length - 1]['tableDetail']
      }
    }
    if (datas.type == 'closeFilterPrompt') {
      this.slideTrueTop = false
    }
    else if (datas.type == 'breadCrumb') {
      this.breadCrumbText(datas.item, datas.val, datas.intReportSeq);
    }
    else if (datas.type == 'droppedItem') {
      response.droppedItem =!isNullOrUndefined(datas.droppedItem) ? datas.droppedItem : [];
      if (datas.val) {
        this.tableSettingJson.filter = 'Y';
      }
      else {
        this.tableSettingJson.filter = 'N';
      }
    }
    else if (datas.type == 'search') {
      this.filterOn = datas.val;
    }
    else if (datas.type == 'sorting') {
      response.sortArray =!isNullOrUndefined(datas.sortArray) ? datas.sortArray : [];
    }
    else if(datas.type == 'filter'){
      response.filterData =!isNullOrUndefined(datas.filterData) ? datas.filterData : [];
    }
    else if(datas.type == 'displayColumn'){
      response.displayColumn =!isNullOrUndefined(datas.columns) ? datas.columns : [];
    }
    else if(datas.type == 'applyGroupingFlag') {
      this.applyGroupingFlag = datas.applyGroupingFlag
    }
    else if(datas.type == 'advanceSubmit') {
      this.viwerGrid = false;
      setTimeout(()=>{ this.viwerGrid = true})
      this.resultDataCallSubmit(datas.data)
    }
    else {
      let data = datas.item;
      if (response.nextLevel > 0) {
        if (!isNullOrUndefined(data['DDKEYID']) || !isNullOrUndefined(data['DDKEY'])) {
          let sendData;
          let ddkey = !isNullOrUndefined(data['DDKEYID']) ? data['DDKEYID'] : data['DDKEY'];
          if (this.selectedReportType == 'I') {
            let tmpStr = `drillDownKey${parseInt(response.currentLevel)}`
            if (!isNullOrUndefined(this.drillDownKeys[tmpStr])) {
              this.drillDownKeys[tmpStr] = this.drillDownKeys[tmpStr].substring(1);
              this.drillDownKeys[tmpStr] = this.drillDownKeys[tmpStr].slice(0, -1)
              this.drillDownKeys[tmpStr] = `'${this.drillDownKeys[tmpStr]}!@#${ddkey}'`;
            }
            else {
              this.drillDownKeys[tmpStr] = `'${ddkey}'`;
            }
          }
          else {
            this.drillDownKeys[`drillDownKey${parseInt(response.currentLevel)}`] = `'${ddkey}'`;
          }
          let drillDownRequest = JSON.parse(JSON.stringify(this.drillDownRequestData))
          const sendReq = Object.keys(drillDownRequest);
          const drillDownKeys = Object.keys(this.drillDownKeys);
          const promptValues = Object.keys(this.promptValues);
          sendReq.forEach((element, index) => {
            drillDownRequest[element] = element == 'nextLevel' ? response.nextLevel :
              element == 'currentLevel' ? response.currentLevel : this.selectedReport[element];
            drillDownKeys.forEach((element1, index1) => {
              if (element1 == element) {
                drillDownRequest[element] = this.drillDownKeys[element]
              }
            });
            promptValues.forEach((element1, index1) => {
              if (element1 == element) {
                drillDownRequest[element] = this.promptValues[element]
              }
            });
          });
          sendData = drillDownRequest;
          if (!isNullOrUndefined(sendData)) {
            response.scrollYx = datas.scroll;
            response.pageNum = datas.pageNum;
            response.rowId = datas.rowId;
            response.sortArray = datas.sortArray;
            response.droppedItem = datas.droppedItem;
            response.filterData = datas.filterData;
            let breadCrumb, breadCrumbLabel, breadCrumbArr;
            if (!objData[0]["drillDownList"].length) {
              breadCrumbArr = response['columnHeaderslst'].filter(items => items.drillDownLabel);
            }
            else {
              breadCrumbArr = response['columnHeaderslst'].filter(items => items.drillDownLabel);
            }
            if (breadCrumbArr.length) {
              breadCrumbLabel = !isNullOrUndefined(breadCrumbArr[0].caption) && breadCrumbArr[0].caption != '' ? breadCrumbArr[0].caption : 'DDKEYID';
              breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
            }
            else {
              breadCrumb = 'DDKEYID';
            }
            this.drillDownApiHit(sendData, datas, breadCrumb, breadCrumbLabel, response)
          }
          else {
            this.globalService.showToastr.error("")
          }
        }
      }
      else {
        if (this.selectedReportType == 'I') {
          if (!isNullOrUndefined(data['DDKEYID']) || !isNullOrUndefined(data['DDKEY'])) {
            let ddkey = !isNullOrUndefined(data['DDKEYID']) ? data['DDKEYID'] : data['DDKEY'];
            this.interActiveRespone.forEach((element, index) => {
              if (element.subReportId != response.subReportId) {
                if (element.parentSubReportID == response.subReportId) {
                  element.parentSeq = response.intReportSeq;
                  this.interActiveHit(element, ddkey, "", false, response['tileSequenceId'], true)
                }
              }
            });
          }
        }
      }
    }
  }

  drillDownApiHit(sendData, datas, breadCrumb, breadCrumbLabel, response?) {

    this.breadCrumbShow = false;
    let data = datas.item;
    sendData['scalingFactor'] = this.scalingFactor;
    let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
    this.tablist[this.selectedReportId]["tabFilterValue"].map(item=> `G${item.filterSeq}`).join("-"): '';
    sendData['filterPosition'] = filterPosition;
    this.maintainData['reportRequest'] = sendData;
    let obj = this.fetchObjData(datas.intReportSeq);
    if (this.selectedReportType == 'I' && obj.length) {
        obj[0]['response']['show'] = false;
    }
    this._apiService.post(environment.getRMReport, sendData).subscribe((resp) => {
      if(this.selectedReportType == 'I' && obj.length){
          obj[0]['response']['show'] = true;
      }
      this.disableScaling = true;
      let objData = this.fetchObjData(datas.intReportSeq);
      let classData = this.fetchClass(datas.intReportSeq);
      objData[0].drillDownFlag = true;
      this.breadCrumbShow = true;
      this.crumbText = '';
      if (resp['status'] == 1) {
        let checkFlag = this.colWidthCheck(resp['response']["columnHeaderslst"]);
        if (checkFlag) {
          objData[0].drillDownList.push({
            id: parseInt(objData[0].drillDownList.length) + 1,
            tabName: data[breadCrumb],
            tabLabel: breadCrumbLabel,
            tableDetail: { currentLevelFlag: true, intReportSeq: datas.intReportSeq },
            message: 'Report Column Header Mismatch - Please Contact Admin'
          })
          objData[0].drillDownList.forEach((element, index) => {
            if (index == objData[0].drillDownList.length - 1) {
              element.tableDetail.currentLevelFlag = true;
            }
            else {
              element.tableDetail.currentLevelFlag = false;
            }
          });
          this.mismatch = true;
          this.globalService.showToastr.error('Report Column Header Mismatch - Please Contact Admin')
        }
        else {
          resp['response']['tileSequenceId'] = this.selectedReportType == 'I' ? parseInt(resp['response']['intReportSeq']) : 1;
          resp['response'].currentLevelFlag = true;
          resp['response'].currentLevel = resp['response']['currentLevel'],
            resp['response'].nextLevel = resp['response']['nextLevel'],
            resp['response']['title'] = this.selectedReport['reportTitle'];
          resp['response']['reportType'] = this.selectedReportType;
          resp['response']['templateId'] = this.selectedReport['templateId']
          resp['response']['showSearch'] = false;
          resp['response']['showSearchR'] = false;
          resp['response']['fontSize'] = this.selectedReportType == 'I' ?
            (classData.gridClass == 'classGridh50' || classData.gridClass == 'classGridh60' ||
              classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30' ||
              classData.gridClass == 'classGridh66' || classData.gridClass == 'classGridh25' ||
              classData.gridClass == 'classGridh20') ? 'small' :
              //(classData.gridClass == 'classGridh40' || classData.gridClass == 'classGridh30') ? 'tiny' :
              classData.gridClass == 'classGridh100' ? 'medium' : this.currentFontSize : this.currentFontSize;
          resp['response'].scrollYx = { x: 0, y: 0 };
          resp['response'].pageNum = 1;
          resp['response'].rowId = undefined;
          resp['response'].droppedItem = [];
          resp['response'].sortArray = [];
          resp['response'].filterData = [];
          resp['response'].applyGroupingFlag = false;
          resp['response']['groupingFlag'] = this.selectedReport.groupingFlag;
          resp['response']['maxRecords'] = this.selectedReport.maxRecords;
          resp['response']['resetEverything'] = false
          resp['response']['colArrayHeader'] = [];
          if (!isNullOrUndefined(resp['response']["gridDataSet"])) {
            resp['response']["columnHeaderslst"].forEach(element => {
              element.id = element.dbColumnName != '' ? element.dbColumnName : false;
              element.name = element.caption;
              element.selected = true;
              element.selected1 = true;
              element.settingBox = false;
              element.sorting = false;
              element.sortValue = false;
              element.pinning = 'center';
              element.class = (element.colType == 'T' || element.colType == 'D') ? 'text-left' : 'text-right';
              element.grouping = false;
              element.filterData = '';
              element.dataType = element.colType;
              element.colspan = element.colspan == 0 ? 1 : element.colspan;
              element.rowspan = element.rowspan == 0 ? 1 : element.rowspan;
            });
            let headObj = this.groupBy(resp['response']["columnHeaderslst"], 'labelRowNum');
            Object.keys(headObj).forEach((item) => {
              resp['response']['colArrayHeader'].push(headObj[item]);
            });
            if (!isNullOrUndefined(resp['response']['reportUserDeflst']) &&
            resp['response']['reportUserDeflst'].length) {
            resp['response']['sortArray'] = [];
            resp['response']['filterData'] = [];
            resp['response']['droppedItem'] = [];
            let sortColumn = !isNullOrUndefined(resp['response']['reportUserDeflst'][0].sortColumn) ?
            resp['response']['reportUserDeflst'][0].sortColumn.replaceAll("ORDER BY ", "") : '';
            sortColumn.split(",").forEach(element => {
              if (element && element.length) {
                let subElem = element.split(" ");
                if(subElem.length && subElem[0] && subElem[1])
                resp['response']['sortArray'].push({ column: subElem[0], type: subElem[1].toLowerCase() });
              }
            })
            let searchColumn = !isNullOrUndefined(resp['response']['reportUserDeflst'][0].searchColumn) ?
            resp['response']['reportUserDeflst'][0].searchColumn : '';
              searchColumn.split(",").forEach(element => {
              if (element && element.length) {
                let subElem = element.split("!@#");
                let dataType = resp['response'].columnHeaderslst.filter(item => item.dbColumnName == subElem[0]);
                resp['response']['filterData'].push({ id: subElem[0], type: subElem[1],
                   filterData: subElem[2], dataType:  dataType[0].colType});
              }
            })
            let columnsHide = !isNullOrUndefined(resp['response']['reportUserDeflst'][0].columnsToHide) ?
            resp['response']['reportUserDeflst'][0].columnsToHide : '';
            let groupingColumn = !isNullOrUndefined(resp['response']['reportUserDeflst'][0].groupingColumn) ?
            resp['response']['reportUserDeflst'][0].groupingColumn : '';
            resp['response'].columnHeaderslst.forEach(head=>{
              columnsHide.split("!@#").forEach(ele=>{
                if(ele != '' && head.dbColumnName == ele){
                  head.selected = false;
                  head.selected1 = false;
                }
              })
              groupingColumn.split("!@#").forEach(ele=>{
                if(ele != '' && head.caption == ele && (resp['response'].maxRecords >= resp['response'].totalRows)){
                  resp['response']['droppedItem'].push(head)
                }
              })
            })
            let applyGrouping = !isNullOrUndefined(resp['response']['reportUserDeflst'][0].applyGrouping) ?
            resp['response']['reportUserDeflst'][0].applyGrouping : '';
              this.applyGroupingFlag = applyGrouping == 'Y' ? true: false;
              resp['response'].applyGroupingFlag = applyGrouping == 'Y' ? true: false;
          }
          // else {
          //   data['response']['sortArray'] = [];
          //   data['response']['filterData'] = [];
          //   data['response']['droppedItem'] = [];
          // }
            if (objData[0].drillDownList.length) {
              let list = objData[0].drillDownList;
              let index = list.findIndex(x => x.currentLevel == resp['response']['currentLevel']);
              if (index == -1) {
                list.push({
                  id: parseInt(objData[0].drillDownList.length) + 1,
                  tabName: data[breadCrumb],
                  tabLabel: breadCrumbLabel,
                  tableDetail: resp['response'],
                })
              }
              else {
                list[index] = {
                  id: parseInt(objData[0].drillDownList.length) + 1,
                  tabName: data[breadCrumb],
                  tabLabel: breadCrumbLabel,
                  tableDetail: resp['response'],
                };
                list = list.splice(parseInt(index) + 1, list.length - index)
              }
              list.forEach(element => {
                if (element.tableDetail.currentLevel == resp['response']['currentLevel']) {
                  if (this.selectedReportType == 'I') {
                    this.tablist[this.selectedReportId].tabDetail.forEach(main => {
                      main['group'].forEach((element1, index1) => {
                        if (element1.tileSequenceId == 'group3') {
                          element1.group.forEach(ele => {
                            if (ele.tileSequenceId == datas.intReportSeq) {
                              ele["reportOrientation"] = element.tableDetail["reportOrientation"] == 'L' ? true : false;
                            }
                          })
                        }
                        else {
                          if (element1.tileSequenceId == datas.intReportSeq) {
                            element1["reportOrientation"] = element.tableDetail["reportOrientation"] == 'L' ? true : false;
                          }
                        }
                      });
                    });
                  }
                  element.tableDetail.currentLevelFlag = true;
                }
                else {
                  element.tableDetail.currentLevelFlag = false;
                }
              });
            }
            else {
              objData[0].drillDownList.push(
                {
                  id: parseInt(objData[0].drillDownList.length) + 1,
                  tabName: data[breadCrumb],
                  tabLabel: breadCrumbLabel,
                  tableDetail: resp['response'],
                }
              )
            }
          }
          if (this.selectedReportType == 'I') {
            if (!isNullOrUndefined(resp['response']["gridDataSet"][0]['DDKEYID']) || !isNullOrUndefined(resp['response']["gridDataSet"][0]['DDKEY'])) {
              let ddkey = !isNullOrUndefined(resp['response']["gridDataSet"][0]['DDKEYID']) ? resp['response']["gridDataSet"][0]['DDKEYID'] : resp['response']["gridDataSet"][0]['DDKEY'];
              this.interActiveRespone.forEach((element, index) => {
                if (element.parentSubReportID == resp['response'].subReportId) {
                  element.parentSeq = resp['response'].intReportSeq;
                  this.interActiveHit(element, ddkey, "", true, resp['response']['tileSequenceId'])
                }
              });
            }
          }
        }
      }
      // else if (resp['status'] == 41) {
      //   if (objData[0].drillDownList.length) {
      //     objData[0].drillDownList.push({
      //         id: parseInt(objData[0].drillDownList.length) + 1,
      //         tabName: data[breadCrumb],
      //         tabLabel: breadCrumbLabel,
      //       })
      //   }
      //   else {
      //     objData[0].drillDownList.push({
      //         id: parseInt(objData[0].drillDownList.length) + 1,
      //         tabName: data[breadCrumb],
      //         tabLabel: breadCrumbLabel,
      //       })
      //   }

      // }
      else {
        objData[0].drillDownList.push({
          id: parseInt(objData[0].drillDownList.length) + 1,
          tabName: data[breadCrumb],
          tabLabel: breadCrumbLabel,
          tableDetail: { currentLevelFlag: true, intReportSeq: datas.intReportSeq },
          message: resp['message']
        })
        objData[0].drillDownList.forEach((element, index) => {
          if (index == objData[0].drillDownList.length - 1) {
            element.tableDetail.currentLevelFlag = true;
          }
          else {
            element.tableDetail.currentLevelFlag = false;
          }
        });
        this.globalService.showToastr.error(resp['message'])
      }
    })
  }

  backDrillDown(opt) {
    let objData
    let seq = this.selectedReport.reportType == 'I' ? opt.tableDetail.intReportSeq : opt.intReportSeq;
    objData = this.fetchObjData(seq);
    let drill = objData[0].drillDownList;
    let index = drill.findIndex(x => x.tableDetail.currentLevel == opt.tableDetail.currentLevel);
    if (this.selectedReportType == "I") {
      this.tablist[this.selectedReportId].tabDetail.forEach(element => {
        element['group'].forEach((element1, index1) => {
          if (element1.tileSequenceId == 'group3') {
            element1.group.forEach(ele => {
              if (ele.parentSeq == seq) {
                ele["reportDetail"] = undefined;
              }
            })
          }
          else {
            if (element1.parentSeq == seq) {
              element1["reportDetail"] = undefined;
            }
          }
        });
      });
      if (Object.keys(this.drillDownKeys).length) {
        let tmpStr = `drillDownKey${parseInt(seq)}`
        let drillDownKeys = this.drillDownKeys[tmpStr];
        drillDownKeys = drillDownKeys.split('!@#');
        drillDownKeys.splice(index, drillDownKeys.length);
        if (drillDownKeys.length) {
          if (drillDownKeys.length == 1) {
            this.drillDownKeys[tmpStr] = `${drillDownKeys[0]}'`
          }
          else {
            drillDownKeys.forEach((element, keyIndex) => {
              if (keyIndex == drillDownKeys.length - 1) {
                this.drillDownKeys[tmpStr] = `${element}'`
              }
              else if (keyIndex == 0) {
                this.drillDownKeys[tmpStr] = `${element}`
              }
              else {
                this.drillDownKeys[tmpStr] = `!@#${element}`
              }
            });
          }
        }
        else {
          this.drillDownKeys[tmpStr] = undefined;
        }
      }
      else {
        let tempddKeys = Object.keys(this.ddKeysChart).filter(item => item != "count");
        if(tempddKeys.length){
          for (let i = index; i < tempddKeys.length; i++) {
            let tmp = `drillDownKey${i + 1}`;
            this.ddKeysChart[tmp] = "";
          }
          this.ddKeysChart.count = index + 1;
          this.tablist[this.selectedReportId].tabDetail.forEach(element => {
            element['group'].forEach((element1, index1) => {
              if (index == 0) {
                if (element1.tileSequenceId == 'group3') {
                  element1.group.forEach(ele => {
                    if (ele.tileSequenceId == seq) {
                      ele["objectType"] = ele.reportDetail[0].response.objectType;
                    }
                  })
                }
                else {
                  if (element1.tileSequenceId == seq) {
                    element1["objectType"] = element1.reportDetail[0].response.objectType;
                  }
                }
              }
              else {
                if (element1.tileSequenceId == 'group3') {
                  element1.group.forEach(ele => {
                    if (ele.tileSequenceId == seq) {
                      ele["objectType"] = ele.reportDetail[0].drillDownList[index + 1].tableDetail.objectType;
                    }
                  })
                }
                else {
                  if (element1.tileSequenceId == seq) {
                    element1["objectType"] = element1.reportDetail[0].drillDownList[index + 1].tableDetail.objectType;
                  }
                }
              }
            });
          });
        }
      }
    }
    if (this.selectedReportType != "I") {
      if (index == 0) {
        this.maintainData.reportRequest['currentLevel'] = 1;
        this.maintainData.reportRequest['nextLevel'] = 1;
        this.disableScaling = false;
      }
      else {
        this.maintainData.reportRequest['currentLevel'] = index;
        this.maintainData.reportRequest['nextLevel'] = index + 1;
      }
      if(!isNullOrUndefined(objData[0].response) &&
      !isNullOrUndefined(objData[0].response.droppedItem) && objData[0].response.droppedItem.length){
        this.tableSettingJson.filter = 'N';
      }
      else{
        this.tableSettingJson.filter = 'Y';
      }
    }
    drill.splice(index, drill.length);
    if (drill.length == 0) {
      objData[0].drillDownFlag = false;
      if (this.viewList == 'pdf') {
        this.export('view', 'pdf')
      }
    }
    else {
      objData[0].drillDownFlag = true;
      if (this.viewList == 'pdf') {
        this.export('view', 'pdf')
      }
      drill.forEach(element => {
        if (parseInt(element.tableDetail.nextLevel) == parseInt(opt.tableDetail.currentLevel)) {
          if (this.selectedReportType == 'I') {
            this.tablist[this.selectedReportId].tabDetail.forEach(main => {
              main['group'].forEach((element1, index1) => {
                if (element1.tileSequenceId == 'group3') {
                  element1.group.forEach(ele => {
                    if (ele.tileSequenceId == seq) {
                      ele["reportOrientation"] = element.tableDetail["reportOrientation"] == 'L' ? true : false;
                    }
                  })
                }
                else {
                  if (element1.tileSequenceId == seq) {
                    element1["reportOrientation"] = element.tableDetail["reportOrientation"] == 'L' ? true : false;
                  }
                }
              });
            });
          }
          element.tableDetail.currentLevelFlag = true;
          if(!isNullOrUndefined(element.tableDetail.droppedItem) && element.tableDetail.droppedItem.length){
            this.tableSettingJson.filter = 'N';
          }
          else {
            this.tableSettingJson.filter = 'Y';
          }
        }
        else {
          element.tableDetail.currentLevelFlag = false;
        }
      });
    }
  }

  /* Breadcrumbs */

  breadCrumbText(datas, type, intReportSeq) {
    this.hoverSeq = intReportSeq;
    let objData = this.fetchObjData(intReportSeq)
    let response;
    if (!objData[0]["drillDownList"].length) {
      response = objData[0]["response"];
    }
    else {
      response = objData[0]["drillDownList"][objData[0]["drillDownList"].length - 1]['tableDetail']
    }
    if (response.nextLevel > 0) {
      if (type == "mouseover") {
        let breadCrumb, breadCrumbLabel, breadCrumbArr;
        if (!objData[0]["drillDownList"].length) {
          breadCrumbArr = response['columnHeaderslst'].filter(items => items.drillDownLabel);
        }
        else {
          breadCrumbArr = response['columnHeaderslst'].filter(items => items.drillDownLabel);
        }
        if (breadCrumbArr.length) {
          breadCrumbLabel = !isNullOrUndefined(breadCrumbArr[0].caption) && breadCrumbArr[0].caption != '' ? breadCrumbArr[0].caption : 'DDKEYID';
          breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
        }
        else {
          breadCrumb = 'DDKEYID';
        }
        this.crumbText = datas[breadCrumb];
      }
      else {
        if(this.breadCrumbShow)
        this.crumbText = '';
      }
    }
  }

  /* Breadcrumbs */

  /* DrillDown request For Both Normal and Interactive Report */
  heightRetrn(templateId, length) {
    if (templateId == 'MULTIPLE') {

      let temp = length < 3 ? "100%" : (40 * length) + "%"
      return {
        height: temp
      }

    } else {
      return {
        height: "100%"
      }
    }
  }
  conditionForViewer() {
    if (this.selectedReport.templateId == 'MULTIPLE') {
      if (this.viewList == 'xlsx' && this.viwerGrid) {
        return true
      }
      else {
        return false
      }
    }
    else {
      return true
    }

  }
  conditionForDowload() {
    if (this.selectedReport.reportType != 'I') {
      return true;
    }
    else {
      if (this.selectedReport.templateId == 'MULTIPLE') {
        return true;
      }
      else {
        return false;
      }
    }
  }


  // ------------------------------------------------------------
  previewScreen (id, data) {
    // data.reportDetail[0].response.chartData.chart.caption = "";
    // data.reportDetail[0].response.chartData.chart.subcaption = "";
    // data.reportDetail[0].response.chartData.chart.showPercentValues = "0";
    // data.reportDetail[0].response.chartData.chart.maxLabelHeight = "40";
    // data.reportDetail[0].response.chartData.chart.useEllipsesWhenOverflow = "1";
    let screensize = document.getElementById('mini' + id);
    screensize.classList.contains('fullscreen-report') ? this.minimize(screensize, data) :
      this.maximize(screensize, data);
  }


  // ------------------------------------------------------------
  minimize(minimize: any, data) {
    if (!isNullOrUndefined(data.reportDetail) && data.reportDetail[0]['response']['show']) {
      // delete data.reportDetail[0].response.chartData.chart.showBorder
      if (!isNullOrUndefined(data.reportDetail) && data.reportDetail[0].drillDownList.length) {
        data.reportDetail[0].drillDownList.forEach(element => {
          element.tableDetail['fontSize'] = 'small';
        });
      }
      if (!isNullOrUndefined(data.reportDetail)) {
        data.reportDetail[0].response['fontSize'] = 'small';
        data.reportDetail[0]['response']['show'] = false;
        setTimeout(() => {
          data.reportDetail[0]['response']['show'] = true;
        }, 1);
      }
      data.gridClass = data.tempGridClass;
      this.screentypeToolTip = 'Expand';
      minimize.classList.remove('fullscreen-report');
      this.screentype = 'fa-expand';
      // let tooltip = document.getElementsByClassName('tool_tip');
      // for (var i = 0; i < tooltip.length; i++) {
      //   tooltip[i].removeAttribute('flow');
      // }
    }
  }

  // ------------------------------------------------------------
  maximize(maximize: any, data) {
    if (!isNullOrUndefined(data.reportDetail) && data.reportDetail[0]['response']['show']) {
      // let objData = this.fetchObjData(data.tileSequenceId)
      if (!isNullOrUndefined(data.reportDetail) && data.reportDetail[0].drillDownList.length) {
        data.reportDetail[0].drillDownList.forEach(element => {
          // element.tableDetail['tempfontSize'] = element.tableDetail['fontSize'];
          element.tableDetail['fontSize'] = 'medium';
        });
      }
      // data.reportDetail[0].response['tempfontSize'] = data.reportDetail[0].response['fontSize'];
      if (!isNullOrUndefined(data.reportDetail)) {
        data.reportDetail[0].response['fontSize'] = 'medium';
        data.reportDetail[0]['response']['show'] = false;
        setTimeout(() => {
          data.reportDetail[0]['response']['show'] = true;
        }, 1);
      }
      data['tempGridClass'] = data.gridClass;
      data.gridClass = 'classGridh100';
      // data.reportDetail[0].response.chartData.chart.showBorder = "0";
      this.screentypeToolTip = 'Compress';
      maximize.classList.add('fullscreen-report');
      this.screentype = 'fa-compress';
      // let tooltip = document.getElementsByClassName('tool_tip');
      // for (var i = 0; i < tooltip.length; i++) {
      //   tooltip[i].setAttribute('flow', 'left');
      // }
    }
  }

  interActiveToggleShow(event, dropId, data) {
    if (!isNullOrUndefined(data.reportDetail) && data.reportDetail[0]['response']['show']) {
      if (dropId == 'showDownloadDropdown') {
        for (let i = 0; i < 10; i++) {
          this.downloaddropDownObj[`dropdown${i}`] = false;
        }
        for (let i = 0; i < 10; i++) {
          this.downloaddropDownObj[`info${i}`] = false;
        }
        this.downloaddropDownObj[`dropdown${data.tileSequenceId}`] = true;
        // this.showDownloadDropdown = !this.showDownloadDropdown;
        // this.showViewDropdown = false;
        // this.showZoomDropdown = false;
        // this.showBeforeDownload = false;
      }
      if (dropId == 'showInfoDropdown') {
        for (let i = 0; i < 10; i++) {
          this.downloaddropDownObj[`info${i}`] = false;
        }
        for (let i = 0; i < 10; i++) {
          this.downloaddropDownObj[`dropdown${i}`] = false;
        }
        this.downloaddropDownObj[`info${data.tileSequenceId}`] = true;
      }
    }
  }

  checkInfoInteractive(data){
    if (!isNullOrUndefined(data.reportDetail) && data.reportDetail[0]['response']['show']) {
    let objData;
    let response;
    response = data["reportDetail"];
    if (!response[0].drillDownList.length) {
      objData = response[0].response;
    }
    else {
      response[0].drillDownList.forEach(element => {
        if (element.tableDetail.currentLevelFlag) {
          objData = element.tableDetail;
        }
      });
    }
    if(!isNullOrUndefined(objData)){
      if(!isNullOrUndefined(objData['reportInfo']) && objData['reportInfo'] != ''){
        data['reportInfo'] = objData['reportInfo']
        return true
      }
      else {
        return false
      }
    }
    else {
      return false
    }
  }
  }

  interActiveExport(type, filetype, mode, data) {
    this.downloaddropDownObj[`dropdown${data.tileSequenceId}`] = false;
    let sendRequest;
    this.interActiveRespone.forEach(element => {
      if (element.intReportSeq == data.tileSequenceId) {
        sendRequest = this.interActiveRequest(element, '');
      }
    });
    if (sendRequest != undefined) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      this.tablist[this.selectedReportId]["tabFilterValue"].forEach(element1 => {
        values.forEach((element, promptIndex) => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO') {
              if (element1["multiSelect"] == 'Y') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = this.arraySrtingConDes(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element][0]['description']}`;
                }
              }
            }
            if (element1['filterType'] == 'DATE') {
              if (element != '') {
                let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${val}`;
              }
              else {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = ``;
              }

            }
            if (element1['filterType'] == 'TEXTD') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (element1 == 'TEXT') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (element1['filterType'] == 'TEXTM') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
            if (element1['filterType'] == 'TREE') {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
            }
          }
        });
      });
      const keys = Object.keys(this.promptDescriptions);
      let promptArr = [];
      this.tablist[this.selectedReportId]["tabFilterValue"].forEach((label) => {
        keys.forEach((prompt) => {
          if (`promptDescription${label.filterSeq}` == prompt)
            promptArr.push(`${label.filterLabel} : ${this.promptDescriptions[prompt]}`)
        })
      })

      sendRequest['scalingFactor'] = this.scalingFactor;
      sendRequest['reportOrientation'] = mode;
      let objData;
      let response;
      response = data["reportDetail"];
      if (!response[0].drillDownList.length) {
        objData = response[0].response;
      }
      else {
        const sendReq = Object.keys(sendRequest);
        const drillDownKeys = Object.keys(this.drillDownKeys);
        const chartddKeys = Object.keys(this.ddKeysChart).filter(item => item != 'count');
        sendReq.forEach((element, index) => {
          if(drillDownKeys.length){
            drillDownKeys.forEach((element1, index1) => {
              if (element1 == element) {
                sendRequest[element] = this.drillDownKeys[element]
              }
            });
          }
          else{
            chartddKeys.forEach((element1, index1) => {
              if (element1 == element) {
                sendRequest[element] = this.ddKeysChart[element]
              }
            });
          }
        });
        let drillLabel = '';
        response[0].drillDownList.forEach((element, ind) => {
          drillLabel += `${element.tabLabel} : ${element.tabName}`
          if (response[0].drillDownList.length - 1 != ind) {
            drillLabel += ' | ';
          }
          if (element.tableDetail.currentLevelFlag) {
            objData = element.tableDetail;
            sendRequest['currentLevel'] = (parseFloat(objData.currentLevel) - 0.1).toFixed(1);
            sendRequest['nextLevel'] = objData.ddFlag == 'Y' ? !isNullOrUndefined(objData.nextLevel) && objData.nextLevel != "" ?
              (parseFloat(objData.nextLevel) - 0.1).toFixed(1) : objData.currentLevel : objData.currentLevel;
          }
        });
        // sendRequest['drillDownLabel'] = drillLabel;
        promptArr.push(drillLabel)
      }
      let scale = this.scalingFactor == '1' ? "No Scaling" : this.scalingFactor == '1000' ? "In Thousand's" :
        this.scalingFactor == '1000000' ? "In Million's" : this.scalingFactor == '1000000000' ? "In Billion's" : "";
      promptArr.push(`Scaling Factor : ${scale}`);
      sendRequest['promptLabel'] = promptArr.join('!@#');
      let filterPosition = !isNullOrUndefined(this.tablist[this.selectedReportId]["tabFilterValue"]) ?
          this.tablist[this.selectedReportId]["tabFilterValue"].map(item => `G${item.filterSeq}`).join("-") : '';
      sendRequest['filterPosition'] = filterPosition;
      sendRequest['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
      let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
      let api = filetype == 'xlsx' ? environment.getRMreportExcelExport : filetype == 'csv' ? environment.reportExportToCsv : environment.getRMreportPdfExport;
      this._apiService.fileDownloads(api, sendRequest).subscribe((resp) => {
        const blob = new Blob([resp], { type: filetype == 'xlsx' ? 'text/xls': filetype == 'csv' ? 'text/csv' : 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        if (type == 'download') {
          const link = this.downloadZipLink.nativeElement;
          link.href = url;
          link.download = filetype == 'xlsx' ? `${sendRequest.reportTitle}_${visionId}.xlsx`:
           filetype == 'csv' ?  `${sendRequest.reportTitle}_${visionId}.csv`:
            `${sendRequest.reportTitle}_${visionId}.pdf`;
          link.click();
        }
        if (type == 'view' && filetype == 'pdf') {
          if (this.selectedReport.templateId != 'MULTIPLE') {
            document.querySelector("iframe").src = url;
          }
          else {
            let placeholder = document.getElementById('reportPlaceholder1');
            placeholder['src'] = url;
          }
        }
        window.URL.revokeObjectURL(url);

      }, (error: any) => {
        if (error.substr(12, 3) == '204') {
          this.globalService.showToastr.error('No Records to Export');
        }
        if (error.substr(12, 3) == '420') {
          this.globalService.showToastr.error('Error Generating Report');
        }
        if (error.substr(12, 3) == '417') {
          this.globalService.showToastr.error('Error while exporting the report');
        }
      });
    }
  }

  scalingSelect(scale, drop) {
    drop.close();
    if(this.disableScaling){
      const modelRef = this.modalService.open(GenericPopupComponent, {
        size: <any>'md',
        backdrop: false,
        windowClass: "genericPopup"
      });
      modelRef.componentInstance.title = "Alert";
      modelRef.componentInstance.message = "Change of scaling factor will reload from Main Report. Click Yes to proceed";
      modelRef.componentInstance.popupType = 'Confirm';
      modelRef.componentInstance.closeTime = 'No';
      modelRef.componentInstance.userConfirmation.subscribe(resp => {
        if (resp.type == 'Yes') {
          this.dropdownSetting['showScalingOption'] = false;
          this.scalingFactor = scale.alphaSubTab;
          this.scalingList.forEach(ele => ele.active = false);
          scale.active = true;
          this.selectedReportType != 'CB' ? this.filterSubmit('report'): '';
        }
        modelRef.close();
      });
    }
    else {
    this.dropdownSetting['showScalingOption'] = false;
    this.scalingFactor = scale.alphaSubTab;
    this.scalingList.forEach(ele=> ele.active = false);
    scale.active = true;
    this.selectedReportType != 'CB' ? this.filterSubmit('report'): '';
    }
  }

  exportDropdown(type, viewList, drop, mode?) {
    drop.close();
    this.dropdownSetting['showExportOption'] = false;
    this.dropdownSetting['showViewOption'] = false;
    this.dropdownSetting['showPdfOption'] = false;
    if(this.tablist[this.selectedReportId].tabDetail && this.tablist[this.selectedReportId].tabDetail.length) {
      this.export(type, viewList, mode)
    }
    else {
      this.exportBefore(type, viewList, mode)
    }
  }

  promptWidth () {
    let length = this.tablist[this.selectedReportId].tabFilterValue.length;
    if(length > 6){
      return {
        width: '16.6667%'
      }
    }
    else {
      return {
        width: (100/length)+'%'
      }
    }
  }

  reportSliderOpen() {
    if(this.tablist[this.selectedReportId].tabDetail && this.tablist[this.selectedReportId].tabDetail.length) {
      this.globalService.topBarChangesDectect({type: 'reportSlide'})
    }
  }
}
