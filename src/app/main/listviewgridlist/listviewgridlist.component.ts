import { Component, CUSTOM_ELEMENTS_SCHEMA, Renderer2, ChangeDetectorRef, OnInit, ViewChild, ElementRef } from '@angular/core';
declare var d3: any;
declare var jsPlumb: any;
declare let $: any;
@Component({
  selector: 'app-listviewgridlist',
  templateUrl: './listviewgridlist.component.html',
  styleUrls: ['./listviewgridlist.component.css']
})
export class ListviewgridlistComponent implements OnInit {

  constructor(private changeDetector: ChangeDetectorRef) { }
  leftContainerHeight: string;
  ngOnInit(): void {
    // this.zoomin();;
   // this.jsplumbnew();
  }

  zoomin() {
    var zoom = d3.zoom()
      .scaleExtent([0.1, 1])
      .on('zoom', zoomed);

    var root = d3.select('.root').call(zoom);
    var canvas = d3.select('.canvas');

    function zoomed() {
      var transform = d3.event.transform;
      canvas.style("transform", "translate(0px,0px) scale(" + transform.k + ")");
    }
  }

  jsplumbstart() {
    jsPlumb.ready(function () {

      $('.dd').draggable(
        {


          drag: function () {
            jsPlumb.repaintEverything();
            //jsPlumb.repaint($(this));

          }
        });


      var endpointOptions = {
        isSource: true,
        isTarget: true,

        endpoint: ["Dot", {
          radius: 10
        }],
        style: {
          fillStyle: 'blue'

        },
        maxConnections: -1,
        connector: "Straight",
        connectorStyle: {
          lineWidth: 3,
          strokeStyle: 'black'

        },
        scope: "blackline",
        dropOptions: {
          drop: function (e, ui) {
            alert('drop!');
          }
        }
      };


      jsPlumb.connect({
        source: "window3",
        target: "window4",

      });
      jsPlumb.connect({
        source: "window5",
        target: "window6",

      });


    });

  }
  jsplumbnew() {
    jsPlumb.ready(function () {

      /*Second Instance*/
      var instance = jsPlumb.getInstance();
      instance.importDefaults({
        Connector: ["Bezier", {
          curviness: 150
        }],
        Anchors: ["BottomCenter", "TopCenter"]
      });

      instance.connect({
        source: "item_input",
        target: "downstream_1",
        scope: "someScope"
      });
      instance.connect({
        source: "item_input",
        target: "downstream_2",
        scope: "someScope"
      });
      instance.connect({
        source: "item_input",
        target: "downstream_3",
        scope: "someScope"
      });
    });

  }
  ngAfterViewInit() {


  }

}
