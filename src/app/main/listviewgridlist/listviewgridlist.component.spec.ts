import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListviewgridlistComponent } from './listviewgridlist.component';

describe('ListviewgridlistComponent', () => {
  let component: ListviewgridlistComponent;
  let fixture: ComponentFixture<ListviewgridlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListviewgridlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListviewgridlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
