import { Component, OnInit, ChangeDetectorRef, OnDestroy, HostListener, AfterViewInit } from '@angular/core';
import { JwtService, GlobalService } from '../service';
import { AuthService } from '../authentication/service/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { isUndefined, isNull, isNullOrUndefined } from 'util';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import * as FusionCharts from 'fusioncharts';

declare var $: any;
@Component({
  selector: 'app-main-container',
  templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {
  themeNow: string;
  sliderNow: string;
  

  constructor(private modalService: NgbModal,
    private authService: AuthService,
    private jwtService: JwtService,
    private router: Router,
    private cdRef: ChangeDetectorRef,
    private globalService:GlobalService,
    private http: HttpClient,
    public translateService: TranslateService) {
    setTimeout(() => {
      // this.checkIdleStatus(1740, 30, 1800);
    }, 1000);
  }

  leftContainerHeight;
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  // isUserLogin = true;
  countDown = 0;
  showCountDown = false;
  // loadSpinner: boolean = false;
  // idleOnTimeOut: any;
  // idleOnIdleStart: any;
  // idleOnTimeoutWarning: any;
  // idleOnPing: any;
  // idleState = 'Not started.';
  // timedOut = false;
  // lastPing?: Date = null;
  public loader: boolean = false;

  ngOnInit() {
    //this.changeThemeNow();
    //this.isUserLogin = !!localStorage.getItem('VisionAuthenticate');

    this.http.get('assets/data/license.json').subscribe(data => {
      FusionCharts.options['license'](data);
    }, error => {
    });
  }


  @HostListener('window: resize', ['$event']) onResize(event) {
    if(this.router.url != '/main/VisionEdwAdmin' && this.router.url != '/main/reports' &&
    this.router.url != '/main/VisionReportSuitedash'){
      setTimeout(() => {
        const getContentAreaHeight = window.innerHeight;
        const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
        this.leftContainerHeight = getContentAreaHeight + 'px';
        document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
        document.getElementById('card-body').children[0]['style'].height = '100%';
      });
    }
  }


  changeOfRoutes = () => {

    if(this.router.url == '/main/VisionEdwAdmin'){
      this.changeThemeNow();
    }
    else if(this.router.url == '/main/reports'){
      this.globalService.getReportSliderTheme().subscribe((params)=>{
        this.sliderNow=params;
        if(this.sliderNow == 'dark'){
          document.body.classList.add("dark");
          setTimeout(()=>{
            if(!isNullOrUndefined(document.getElementById("report-form")))
          document.getElementById("report-form").classList.add("dark-validation");
          },1500)
        }
        if(this.sliderNow == 'light'){
          document.body.classList.add("light");
          setTimeout(() => {
            if(!isNullOrUndefined(document.getElementById("report-form")))
            document.getElementById("report-form").classList.remove("dark-validation");
          }, 1500)
        }
        //this.globalService.silderChanges(this.sliderNow);
      })
    }
    else{
      this.themeNow="";
      this.sliderNow='';
      this.globalService.silderChanges(this.sliderNow);
      this.globalService.themeChanges(this.themeNow);
    }
    this.modalService.dismissAll();
  }

  changeThemeNow(){
    this.globalService.getThemeChanges().subscribe((params)=>{
      this.themeNow=params;
     // this.globalService.themeChanges(this.themeNow);
    })
  }

  /**
   *
   * @param idelSeconds - Allowed User Idel time Ex. 29 Min (1740 Seconds)
   * @param timeOutSeconds - Session logout After this thime Ex. 1 Min (60 Seconds)
   * @param tokenUpdateInterval - Updated Authenticate token after this time Ex. 30 Min (1800 Seconds)
   */
  // checkIdleStatus(idelSeconds, timeOutSeconds, tokenUpdateInterval) {

  //   this.idle.setIdle(idelSeconds);
  //   this.idle.setTimeout(timeOutSeconds);
  //   this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

  //   this.idle.onIdleEnd.subscribe(() => {
  //     this.showCountDown = false;
  //     this.idleState = 'No longer idle.';
  //     this.getPublicKey();
  //   });

  //   this.idleOnTimeOut = this.idle.onTimeout.subscribe(() => {
  //     this.idleState = 'Timed out!';
  //     this.timedOut = true;
  //     this.showCountDown = false;
  //     this.logout();
  //   });

  //   this.idleOnIdleStart = this.idle.onIdleStart.subscribe(() => {
  //     this.showCountDown = true;
  //     this.idleState = 'You\'ve gone idle!';
  //   });

  //   this.idleOnTimeoutWarning = this.idle.onTimeoutWarning.subscribe((countdown) => {
  //     this.idleState = 'You will time out in ' + countdown + ' seconds!';
  //     this.countDown = countdown;
  //   });

  //    this.keepalive.interval(tokenUpdateInterval);

  //   this.idleOnPing = this.keepalive.onPing.subscribe(() => {
  //     this.getPublicKey();
  //   });

  //   this.reset();
  // }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  // reset() {
  //   this.idle.watch();
  //   this.idleState = 'Started.';
  //   this.timedOut = false;
  // }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  // private getPublicKey() {
  //   this.authService.getToken().subscribe(
  //     (successResponse) => {
   //     },
  //     (errorResponse) => {
  //       let validationMsg = "Technical Issue, Please Try Again Later";
  //     }
  //   )
  // }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  logout() {
    this.jwtService.unset('VisionAuthenticate');
    this.router.navigate(['/']);
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngOnDestroy(): void {
    // this.idleOnTimeOut.unsubscribe()
    // this.idleOnIdleStart.unsubscribe()
    // this.idleOnTimeoutWarning.unsubscribe()
    // this.idleOnPing.unsubscribe()
  }

}
