import { Component, OnInit, Input, AfterViewInit, HostListener, ChangeDetectorRef, OnChanges, ElementRef, ChangeDetectionStrategy, ViewChild, ViewChildren, QueryList, DoCheck } from '@angular/core';
// import * as data from '../../../../assets/data/dashboarddata.json';
import { isNull, isString, isNullOrUndefined, isObject, isArray } from 'util';
import { ApiService, GlobalService } from 'src/app/service';
import { environment } from 'src/environments/environment';
import { of } from 'rxjs';
import { concatMap, delay, map, isEmpty } from 'rxjs/operators';
//import { NgxXml2jsonService } from 'ngx-xml2json';

import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'ngx-useful-swiper';
import * as theme from '../../../../assets/data/theme.json';
import * as columns from '../../../../assets/data/columns.json';

// import { dashboardDetail } from 'src/assets/data/GetDasboardDetail.json';
import { loaderSvg } from './loader';
import { DomSanPipe } from 'src/app/shared/dom-san.pipe';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DomSanPipe]
})
export class DashboardComponent implements OnInit, AfterViewInit, OnChanges, DoCheck {

  dashboardData: any = {};
  dashboardFilterFlag: boolean;
  dashboardFilterList: any = [];
  theme: any = theme['default'];
  columns: any = columns['default'];
  leftContainerHeight: string;
  popoverData: any[];
  popProfitSuccessClass: any;
  popProfitDangerClass: any;
  popProfitGreyClass: any;
  popoverTarget: any;
  relationshipData: any;
  tabId: any;
  templateId: any;
  prompt: any;
  promptValue: any;
  loaderSvg: any = loaderSvg;
  @ViewChildren(SwiperComponent) swiperEle: QueryList<SwiperComponent>
  selectedIdex: any = 0;
  dashboardDataDetail: any;
  tileDetail: any = [];
  themeNow: any;
  months = { '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec' };
  monthWithNum = { '01': '1', '02': '2', '03': '3', '04': '4', '05': '5', '06': '6', '07': '7', '08': '8', '09': '9', '10': '10', '11': '11', '12': '12' }
  numWithMonth = { 'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12 }
  gradient: any;
  promptDescriptions: any = {};
  childTiledData: any = [];
  drillDownKeys: any = {};
  parentTitle: any = {};
  @Input() set inputData(value: any) {
    this.dashboardData = value['dashboardTabData'];
    this.tabId = value['tabId'];
    this.templateId = value['templateId'];
    this.promptValue = value['promptValues'];
    this.themeNow = value['themeNow'];
    this.gradient = value['gradient']
    this.promptDescriptions = value['promptDescriptions']
  };
  dropDownGroup = ['BANK GROUP', 'DIVISION', 'PARENT SBU', 'SBU'];
  listLineType = ['MSColumnLine3D', 'StackedColumn2DLine', 'StackedColumn3DLine'];
  listLineTypeDY = ['MSColumn3DLineDY', 'StackedColumn3DLineDY'];


  constructor(private globalService: GlobalService, private cdr: ChangeDetectorRef,
     private apiService: ApiService, private elementRef: ElementRef) { }

  ngOnInit() {
    this.globalService.getsideBarChangesDectect().subscribe((params)=>{
      if(params){
        this.dashboardDataDetail.forEach(element => {
          element['group'].forEach(element1 => {
            if(element1.columnId == "group3"){
              element1['group'].forEach(element2 => {
                if(element2['dataSource']['tileType'] == 'C' && element2.dataSource.childTileslst.length)
                  element2['dataSource']['responsedel']=false;
              });
            }
            else{
              if(element1['dataSource']['tileType'] == 'C' && element1.dataSource.childTileslst.length)
                element1['dataSource']['responsedel']=false;
            }
          });
        });
        setTimeout(() => {
          this.dashboardDataDetail.forEach(element => {
            element['group'].forEach(element1 => {
              if(element1.columnId == "group3"){
                element1['group'].forEach(element2 => {
                  if(element2['dataSource']['tileType'] == 'C' && element2.dataSource.childTileslst.length)
                    element2['dataSource']['responsedel']=true;
                });
              }
              else{
                if(element1['dataSource']['tileType'] == 'C' && element1.dataSource.childTileslst.length)
                  element1['dataSource']['responsedel']=true;
              }
            });
          });
        },500);
      }
    });

  }
  ngOnChanges() {
    this.childTiledData = [];
    this.drillDownKeys = {};
    this.parentTitle = {};
    this.dashboardData.map(ele => { return ele.responsedel = false })
    if (this.tabId != undefined && this.promptValue != undefined) {
      this.tileDetail['tabId'] = this.tabId;
      this.tileDetail['templateId'] = this.templateId;
      this.tileDetail['promptValue'] = this.promptValue;
      this.tileDetail['themeNow'] = this.themeNow;
      this.tileDetail['gradient'] = this.gradient;
      this.tileDetail['promptDescriptions'] = this.promptDescriptions;
      this.tileDetail['childTiledData'] = this.childTiledData;

    }
    this.columnSet();
    this.bindingDetails();
  }

  ngDoCheck() {
  }

  columnSet() {
    this.dashboardDataDetail = this.columns[this.templateId];
    this.dashboardDataDetail.forEach((element1, index1) => {
      if (element1.subGroup == 'N') {
        element1.group.forEach((element2, index2) => {
          this.dashboardData.forEach((element, index) => {
            if (element.tileSequence == element2.columnId) {
              element2.dataSource = element;
            }
          });
        });
      }
      else {
        element1.group.forEach((element2, index2) => {
          this.dashboardData.forEach((element, index) => {
            if (element2.columnId == 'group3') {
              element2.group.forEach((element3, index3) => {
                if (element.tileSequence == element3.columnId) {
                  element3.dataSource = element;
                }
              });
            }
            else {
              if (element.tileSequence == element2.columnId) {
                element2.dataSource = element;
              }
            }
          });
        });
      }
    });
  }

  bindingDetails() {
    for (let i = 0; i <= this.dashboardData.length - 1; i++) {
      if(this.dashboardData[i]['parentSequence'] == 0 || this.dashboardData[i]['parentSequence'] == null ||
      this.dashboardData[i]['parentSequence'] == '' || this.dashboardData[i]['parentSequence'] == this.dashboardData[i]['tileSequence'])
      this.apiCall(this.dashboardData[i]).subscribe(result => this.resultDataCall(result, this.dashboardData[i]))
      else
      this.childTiledData.push(this.dashboardData[i])
    }
  }

  arraySrtingCon(data){
    let arr=[];
    data.forEach(element => {
      arr.push(`'${element["id"]}'`)
    });
    return arr.toString();
  }
  apiCall(item) {
    item.childTileslst = [];
    let prompKeys= Object.keys(this.promptValue)
    let itemKeys= Object.keys(item)
    prompKeys.forEach(element => {
      itemKeys.forEach(element1 => {
        if(element1 == element){
          item[element1] = this.promptValue[element];
        }
      });
    });
    return this.apiService.post(environment.getTileData, item);
  }

  parentChildApiCall(item, parentResponse) {
    item.childTileslst = [];
    let ddKey = '';
    if(parentResponse.tileType == 'C'){
      let chartDataSet = parentResponse.childTileslst[0].chartDataSet;
      if (isNullOrUndefined(chartDataSet['dataset'])) {
        if (!isNullOrUndefined(chartDataSet['category'])) {
          chartDataSet['category'].forEach(element => {
            element['category'].forEach((ele) => {
              ddKey = ele['category'][0]['ddkey']
            })
          })
        }
        else {
          ddKey = !isNullOrUndefined(chartDataSet['data'][0].ddkey) ? chartDataSet['data'][0].ddkey : '';
        }
      }
      else {
        ddKey = !isNullOrUndefined(chartDataSet['dataset'][0]["data"][0].ddkey) ?
         chartDataSet['dataset'][0]["data"][0].ddkey : '';
      }
    }
    if (parentResponse.tileType == 'G') {
      let gridDataSet = parentResponse.childTileslst[0].gridDataSet;
      ddKey = !isNullOrUndefined(gridDataSet[0]['DDKEYID']) ? gridDataSet[0]['DDKEYID'] : '';
    }
    this.dashboardData.map(ele => {
      if (ele.tileSequence == item.tileSequence)
        return ele['responsedel'] = false
    })
    this.drillDownKeys[`drillDownKey${parentResponse.tileSequence}`] = ddKey;
    let sendKeys = Object.values(this.drillDownKeys).filter(e=> !isNullOrUndefined(e) && e != '').join("!@#");
    item['drillDownKey0'] = `'${sendKeys}'`;
    let prompKeys= Object.keys(this.promptValue)
    let itemKeys= Object.keys(item)
    prompKeys.forEach(element => {
      itemKeys.forEach(element1 => {
        if(element1 == element){
          item[element1] = this.promptValue[element];
        }
      });
    });
    return this.apiService.post(environment.getTileData, item);
  }

  getChildData(event) {
    if(event.type == 'parentChild' && !isNullOrUndefined(event.ddKey) && event.ddKey != ''){
      let parentTile = [];
      this.dashboardData.forEach(elem=> {
        if(event.data.parentSequence != 0 && event.data.parentSequence != null &&
          event.data.parentSequence != '' && elem.tileSequence != event.data.tileSequence &&
          elem.tileSequence == event.data.parentSequence) {
          parentTile.push(elem);
        }
      })
      !parentTile.length ? this.drillDownKeys = {} : '';
      this.childTiledData.forEach((tile)=> {
        if (tile.parentSequence == event.data.tileSequence) {
          this.dashboardData.map(ele=> {
            if(ele.tileSequence == tile.tileSequence)
            return ele['responsedel'] = false
          })
          this.parentClickApiCall(tile, event.data, event.ddKey).subscribe(result => {
            !parentTile.length ? this.parentTitle = {} : '';
            this.parentTitle[`parentTitle${event.data.tileSequence}`] = event.title;
            this.resultDataCall(result, tile)
          })
        }
      })
    }
  }

  parentClickApiCall(item, parentResponse, ddKey) {
    item.childTileslst = [];
    // this.dashboardData.map(ele => {
    //   if (ele.tileSequence == item.tileSequence)
    //     return ele['responsedel'] = false
    // })
    this.drillDownKeys[`drillDownKey${parentResponse.tileSequence}`] = ddKey;
    let sendKeys = Object.values(this.drillDownKeys).filter(e=> !isNullOrUndefined(e) && e != '').join("!@#");
    item['drillDownKey0'] = `'${sendKeys}'`;
    let prompKeys= Object.keys(this.promptValue)
    let itemKeys= Object.keys(item)
    prompKeys.forEach(element => {
      itemKeys.forEach(element1 => {
        if(element1 == element){
          item[element1] = this.promptValue[element];
        }
      });
    });
    return this.apiService.post(environment.getTileData, item);
  }

  parentCaption(parentResponse) {
    let title:any = '';
    if(parentResponse.tileType == 'C'){
      let chartDataSet = parentResponse.childTileslst[0].chartDataSet;
      if (parentResponse.chartType == 'Bubble') {
        if (!isNullOrUndefined(chartDataSet['categories'])) {
          title = chartDataSet['categories'][0]['category'][0].label;
        }
      }
      if (parentResponse.chartType == "Pie3D" || parentResponse.chartType == "Pie2D" ||
      parentResponse.chartType == "Pie") {
        if (!isNullOrUndefined(chartDataSet['categories'])) {
          title = chartDataSet['dataset'][0]['label'];
        }
      }
      else {
        let seriesName = chartDataSet['dataset'][0]['seriesname']
        if (!isNullOrUndefined(chartDataSet['categories'])) {
          title = `${seriesName}, ${chartDataSet['categories'][0]['category'][0].label}`;
        }
      }
      title = title.replaceAll("undefined,", "")
    }
    if (parentResponse.tileType == 'G') {
      let gridDataSet = parentResponse.childTileslst[0].gridDataSet;
      let breadCrumb = '';
      let breadCrumbArr = [];
      breadCrumbArr = parentResponse.childTileslst[0].gridColumnSet.filter(items => items.drillDownLabel);
      if (breadCrumbArr.length) {
        breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
      }
      title = !isNullOrUndefined(breadCrumb && gridDataSet[0][breadCrumb]) ? gridDataSet[0][breadCrumb] : '';
    }
    this.parentTitle[`parentTitle${parentResponse.tileSequence}`] = title;
  }

  themeChecked(data) {

    if (data.subTiles == "Y") {
      data.childTileslst.forEach(element => {
        if (element.theme != null) {
          element.themeSetting = this.theme[this.themeNow].multiTile[element.theme];
        }
        else {
          if (data.theme != null) {
            element.themeSetting = this.theme[this.themeNow].multiTile[data.theme];
          }
          else {
            element.themeSetting = this.theme[this.themeNow].multiTile['defaultTheme']
          }
        }
      });
    }
  }
  xmlToJson(xml) {
    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) {
      // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj[attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) {
      // text
      obj = xml.nodeValue;
    }

    // do children
    // If all text nodes inside, get concatenated text from them.
    var textNodes = [].slice.call(xml.childNodes).filter(function (node) {
      return node.nodeType === 3;
    });
    if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
      obj = [].slice.call(xml.childNodes).reduce(function (text, node) {
        return text + node.nodeValue;
      }, "");
    } else if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof obj[nodeName] == "undefined") {
          obj[nodeName] = this.xmlToJson(item);
        } else {
          if (typeof obj[nodeName].push == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(this.xmlToJson(item));
        }
      }
    }
    return obj;
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  chartBarColor(data, xml2, type) {
    // let chartData = type == 'single' ? data.childTileslst[0].chartDataSet : data.chartDataSet;

    // let colorCode = this.theme[this.themeNow].colorCode
    // let xml = xml2.xml;
    // if ((xml.ColorCode == undefined || xml.ColorCode == null || this.isEmpty(xml.ColorCode)) && (xml.EnableColorPalette == undefined || xml.EnableColorPalette == null || this.isEmpty(xml.EnableColorPalette)) && (xml.ColorPalette == undefined || xml.ColorPalette == null || this.isEmpty(xml.ColorPalette))) {
    //   if (chartData['chart'] && chartData['chart']['set']) {
    //     chartData['data'].forEach((element, ind) => {
    //       element['color'] = colorCode[0];
    //     });
    //   }
    //   if (chartData['chart'] && chartData['chart']['dataset']) {
    //     chartData['dataset'].forEach((element, ind) => {
    //       element['color'] = colorCode[0]
    //     });
    //   }
    // }
    // else {
    //   if (xml.EnableColorPalette != undefined || xml.EnableColorPalette != null || !this.isEmpty(xml.EnableColorPalette)) {
    //     if (xml.EnableColorPalette == 'Y') {
    //       if (xml.ColorPalette != undefined || xml.ColorPalette != null || !this.isEmpty(xml.ColorPalette)) {

    //       }
    //       else {
    //         if (chartData['chart'] && chartData['chart']['set']) {
    //           chartData['data'].forEach((element, ind) => {
    //             element['color'] = colorCode[ind]
    //           });
    //         }
    //         if (chartData['chart'] && chartData['chart']['dataset']) {
    //           chartData['dataset'].forEach((element, ind) => {
    //             element['color'] = colorCode[ind]
    //           });
    //         }
    //       }
    //     }
    //     if (xml.EnableColorPalette == 'N') {
    //       if (xml.ColorCode != undefined || xml.ColorCode != null || !this.isEmpty(xml.ColorCode)) {
    //         if (chartData['chart'] && chartData['chart']['set']) {
    //           chartData['data'].forEach((element, ind) => {
    //             element['color'] = '#' + xml.ColorCode;
    //           });
    //         }
    //         if (chartData['chart'] && chartData['chart']['dataset']) {
    //           chartData['dataset'].forEach((element, ind) => {
    //             element['color'] = '#' + xml.ColorCode;
    //           });
    //         }
    //       }
    //       else {
    //         if (chartData['chart'] && chartData['chart']['set']) {
    //           chartData['data'].forEach((element, ind) => {
    //             element['color'] = colorCode[0];
    //           });
    //         }
    //         if (chartData['chart'] && chartData['chart']['dataset']) {
    //           chartData['dataset'].forEach((element, ind) => {
    //             element['color'] = colorCode[0]
    //           });
    //         }
    //       }
    //     }
    //   }
    //   else {
    //     if (xml.ColorCode != undefined || xml.ColorCode != null || !this.isEmpty(xml.ColorCode)) {
    //       if (chartData['chart'] && chartData['chart']['set']) {
    //         chartData['data'].forEach((element, ind) => {
    //           element['color'] = '#' + xml.ColorCode;
    //         });
    //       }
    //       if (chartData['chart'] && chartData['chart']['dataset']) {
    //         chartData['dataset'].forEach((element, ind) => {
    //           element['color'] = '#' + xml.ColorCode;
    //         });
    //       }
    //     }
    //     if (xml.ColorPalette != undefined || xml.ColorPalette != null || !this.isEmpty(xml.ColorPalette)) {
    //     }

    //   }
    // }
  }

  chartBarxmlCoversion(data) {
    if (data.subTiles == 'Y') {
      data.childTileslst.forEach((element, index) => {
        if (element.tileType == 'C') {
          const parser = new DOMParser();
          const xml = parser.parseFromString('<xml>' + element.propertyAttr + '</xml>', 'text/xml');
          let xml2 = this.xmlToJson(xml);
          this.chartBarColor(element, xml2, 'multiple');
        }
      });
    }
    else {
      if (data.tileType == 'C') {
        const parser = new DOMParser();
        const xml = parser.parseFromString('<xml>' + data.propertyAttr + '</xml>', 'text/xml');
        let xml2 = this.xmlToJson(xml);
        this.chartBarColor(data, xml2, 'single');
      }
    }

  }

  conversionJson(xml, data, type, index?) {
    var xml2 = xml.replace(/&/g, '&amp;');
    const parser = new DOMParser();
    const xml1 = parser.parseFromString(xml2, 'text/xml');
    const json = this.xmlToJson(xml1);

    if (type == 'multiple')
      if (isNullOrUndefined(data.childTileslst[index]["theme"]))
        data.childTileslst[index]["theme"] = "defaultTheme";

    let themeNow = type == 'multiple' ? data.childTileslst[index]["theme"] : '';
    json['chart'] ? delete json['chart']['#text'] : "";


    if (json['chart'] && json['chart']['useRoundEdges'] && this.themeNow != 'DS_DEFAULT') {
      json['chart']['useRoundEdges'] = 0;
    }

    //let themeNow = type == 'multiple' ? (!isNullOrUndefined(data.childTileslst[index]["theme"])) ? data.childTileslst[index]["theme"] : 'defaultTheme' : '';
    if (json['chart'] && json['chart']['usePlotGradientColor'] && this.gradient) {
      json['chart']['usePlotGradientColor'] = 1;
      json['chart']['plotGradientColor'] = this.theme[this.themeNow].bgColorChart;
    }
    else {
      if (json['chart'] && json['chart']['use3DLighting'] && this.themeNow != 'DS_DEFAULT') {
        json['chart']['use3DLighting'] = 0;
      }
    }

    if (json['chart'] && json['chart']['set']) {
      if (isArray(json['chart']['set'])) {
        json['data'] = json['chart']['set'];
      }
      else {
        json['data'] = []
        json['data'].push(json['chart']['set']);
      }

      !isNullOrUndefined(json['data']) && json['data'].forEach((element, ind) => {
        element['labelFontColor'] = type == 'single' ? this.theme[this.themeNow].labelColor : this.theme[this.themeNow]['multiTile'][themeNow].labelColor;
        element['valueFontColor'] = type == 'single' ? this.theme[this.themeNow].labelColor : this.theme[this.themeNow]['multiTile'][themeNow].labelColor;
      })
      json['chart']['bgColor'] = type == 'single' ? this.theme[this.themeNow].bgColorChart : this.theme[this.themeNow]['multiTile'][themeNow].bgColorChart;
      json['chart']['baseFontColor'] = type == 'single' ? this.theme[this.themeNow].labelColor : this.theme[this.themeNow]['multiTile'][themeNow].labelColor;
    }
    if (json['chart'] && json['chart']['categories']) {
      delete json['chart']['categories']['#text'];
      json['categories'] = []
      json['categories'].push(json['chart']['categories']);
      json['dataset'] = json['chart']['dataset'];
      json['dataset'] = json['chart']['dataset'];
      !isNullOrUndefined(json['dataset']) && isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
        if(isArray(json['categories'][0]['category'])){
          json['categories'][0]['category']=json['categories'][0]['category'];
        }
        else{
          let tempCat=json['categories'][0]['category'];
          json['categories'][0]['category']=[];
          json['categories'][0]['category'].push(tempCat);
        }
        if (isArray(element['set'])) {
          element['data'] = element['set'];
        }
        else {
          element['data'] = []
          element['data'].push(element['set']);
        }
      });

      if (!isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isObject(json['dataset'])) {
        json['dataset']['data'] = [];
        json['dataset']['data'] = json['dataset']['set'];
        delete json['dataset']['set'];
        delete json['chart']['categories'];
        delete json['chart']['dataset'];
        delete json['chart']['styles'];
        if(isArray(json['categories'][0]['category'])){
          json['categories'][0]['category']=json['categories'][0]['category'];
        }
        else{
          let tempCat=json['categories'][0]['category'];
          json['categories'][0]['category']=[];
          json['categories'][0]['category'].push(tempCat);
        }
        var dataset1 = JSON.parse(JSON.stringify(json));
        json['dataset'] = [];
        if(isArray(dataset1['dataset']['data'])){
          json['dataset'].push({"seriesName":dataset1['dataset']['seriesName'],"color":dataset1['dataset']['color'],"data":dataset1['dataset']['data']});
        }
        else{
          json['dataset'].push({"seriesName":dataset1['dataset']['seriesName'],"color":dataset1['dataset']['color'],"data":[dataset1['dataset']['data']]});
        }
      } else {
        !isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
          element['data'] = element['set']
        });
      }
      json['chart']['bgColor'] = type == 'single' ? this.theme[this.themeNow].bgColorChart : this.theme[this.themeNow]['multiTile'][themeNow].bgColorChart;
      json['chart']['baseFontColor'] = type == 'single' ? this.theme[this.themeNow].labelColor : this.theme[this.themeNow]['multiTile'][themeNow].labelColor;
    }
    return json
  }

  chartChecked(data) {
    if (data.tileType == 'C') {
      if (data.subTiles == "Y") {
        !isNullOrUndefined(data.childTileslst) && data.childTileslst.forEach((element, index) => {
          let xml = element.chartDataSet;
          if (!isNullOrUndefined(element.chartDataSet))
            element.chartDataSet = element.chartDataSet != "" ? this.conversionJson(xml, data, 'multiple', index) : ""
          else
            element.chartDataSet = "";
        });
      }
      else {
        let xml = data.childTileslst[0].chartDataSet;
        if (!isNullOrUndefined(data.childTileslst[0].chartDataSet))
          data.childTileslst[0].chartDataSet = data.childTileslst[0].chartDataSet != "" ? this.conversionJson(xml, data, 'single') : "";
        else
          data.childTileslst[0].chartDataSet = ""

      }
    }
  }

  resultDataCall(data, item) {
    if (data['response'].tabId == this.tabId) {
      data['response']['message'] = data['message'];
      data['response']['status'] = data['status'];
      data['response']['tileRequest'] = item;
      data['response'].responsedel = true;
      //data['response'].swiperIndex=0;
      //data['response'].drillDownHide=true;
      if (data['response'].tileType == 'T') {
        //const parser = new DOMParser();
        ///const xml = parser.parseFromString( data['response'].propertyAttr, 'text/xml');
        ///data['response'].propertyAttr = this.ngxXml2jsonService.xmlToJson(xml);
      }

      // else{
      //   const parser = new DOMParser();
      //   const xml = parser.parseFromString(data['response'].childTileslst[0].chartDataSet, 'text/xml');
      //   data['response'].childTileslst[0].chartDataSet= this.ngxXml2jsonService.xmlToJson(xml);
      // }

      // if (data['response'].subTiles == 'Y') {
      //   !isNullOrUndefined(data['response'].childTileslst) && data['response'].childTileslst.forEach((element1, ind1, arr1) => {
      //     if (element1.tileType == 'T') {
      //       const parser = new DOMParser();
      //       const xml = parser.parseFromString(element1.propertyAttr, 'text/xml');
      //       element1.propertyAttr = this.ngxXml2jsonService.xmlToJson(xml);
      //     }
      //   });
      // }

      this.themeChecked(data['response']);
      if (data['response']['tileType'] == 'C' && !isNullOrUndefined(data['response'].childTileslst)) {
        data['response'].childTileslst.forEach((element) => {
          if (!isNullOrUndefined(element.chartDataSet) && element.chartDataSet != "" &&
            this.listLineType.includes(data['response']['chartType'])) {
            let datas = element['chartDataSet'];
            var regexp = /<dataset/g;
            let c = datas.match(regexp) || [];
            element['chartDataSet'] = this.replaceOccurrence(datas, /<dataset/g, c.length, '<dataset renderas="line"') || '';
          }
          if (!isNullOrUndefined(element.chartDataSet) && element.chartDataSet != "" &&
            this.listLineTypeDY.includes(data['response']['chartType'])) {
            let datas = element['chartDataSet'];
            var regexp = /<dataset/g;
            let c = datas.match(regexp) || [];
            element['chartDataSet'] = this.replaceOccurrence(datas, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
          }
        })
      }
      this.chartChecked(data['response']);
      if (this.themeNow != 'DS_DEFAULT') {
        if (data['response'].tileType == 'C') {
          if (data['response'].subTiles == "Y") {
            !isNullOrUndefined(data['response'].childTileslst) && data['response'].childTileslst.forEach((element, index) => {
              if (!isNullOrUndefined(element.chartDataSet) && element.chartDataSet != "")
                this.chartBarxmlCoversion(data['response']);
            });
          }
          else {
            if (!isNullOrUndefined(data['response'].childTileslst[0].chartDataSet) && data['response'].childTileslst[0].chartDataSet != "")
              this.chartBarxmlCoversion(data['response']);
          }
        }
      }

      data['response'] = this.parsingObject(data['response']);
      !isNullOrUndefined(this.dashboardData) && this.dashboardData.forEach((element, index) => {
        if (element.tileSequence == data['response'].tileSequence) {
            let keys = Object.keys(this.parentTitle).filter(e=> !isNullOrUndefined(e) && e != '');
          keys.forEach(key=> {
            if(key == `parentTitle${element.parentSequence}`) {
              data['response']['parentCaption'] = ` - ${Object.values(this.parentTitle).join(" - ")}`;
            }
          })
          this.dashboardData[index] = data['response'];
        }
      });
      if(!isNullOrUndefined(data['response']))
      this.childTiledData.forEach((tile)=> {
        if(tile.parentSequence == data['response'].tileSequence)
        this.parentChildApiCall(tile, data['response']).subscribe(result => {
          this.parentCaption(data['response']);
          this.resultDataCall(result, tile)
        })
      })
      this.columnSet();
      this.cdr.detectChanges();
      setTimeout(() => {
        this.sideFilterPosition();
        //this.dashboardContHgt();
        this.sideFilterHeight();
        this.profitValuePosInTile();

      }, 120);

    }
  }

  replaceOccurrence(string, regex, n, replace) {
    var i = 0;
    return string.replace(regex, function (match) {
      i += 1;
      if (i === n) return replace;
      return match;
    });
  }

  dateConversion(element) {
    let ele = element.split('-');
    return { "year": +ele[2], "month": +this.monthWithNum[ele[1]], "day": +ele[0] };
  }
  dateReConversion(element) {
    let month = String(element.month).length == 1 ? `0${element.month}` : element.month;
    let day = String(element.day).length == 1 ? `0${element.day}` : element.day;
    return `${day}-${this.months[month]}-${element.year}`
  }




  parsingObject(objectOG) {
    if (objectOG.childTileslst.length) {
      if (objectOG.subTiles == 'Y') {
        !isNullOrUndefined(objectOG.childTileslst) && objectOG.childTileslst.forEach((element, ind1, arr1) => {
          if (element.tileType == 'T' && !isNull(element['tileDataSet'])) {

            arr1[ind1]['endResult'] = this.reqFormat(JSON.parse(element['tileDataSet']), objectOG, ind1);


          }
        });
      }
      else {
        if (objectOG.tileType == 'T' && !isNull(objectOG.childTileslst[0]['tileDataSet']) && objectOG.childTileslst[0]['tileDataSet'] != '') {
          objectOG['endResult'] = this.reqFormat(JSON.parse(objectOG.childTileslst[0]['tileDataSet']), 0, 0);
        }
      }
    }



    if (objectOG['tileType'] == 'G') {
      objectOG['gridColumnSet'] = (isNull(objectOG['gridColumnSet']) || (objectOG['gridColumnSet'] == "")) ? [] : objectOG['gridColumnSet'];
      objectOG['gridDataSet'] = (isNull(objectOG['gridDataSet']) || (objectOG['gridDataSet'] == "")) ? [] : objectOG['gridDataSet'];
      objectOG['gridColumnFormats'] = (isNull(objectOG['gridColumnFormats']) || (objectOG['gridColumnFormats'] == "")) ? [] : objectOG['gridColumnFormats'];
    }

    return objectOG

  }

  reqFormat(list, colourCss?, ink?) {
    let obj = list['PLACEHOLDERS'];
    let arrList = Object.keys(obj);
    let endList = {};
    (arrList).forEach((el, ind, arr) => {
      let set = obj[el];
      endList[el] = {
        type: String(set['SOURCECOL']).startsWith('-'),
        value: this.stringForming(set, el, colourCss, ink),
        toolTip: !isNullOrUndefined(set['TOOLTIP']) ? set['TOOLTIP'].split('_').join(' '):'',
      };
    })
    return endList;
  }

  colorCss(val, colourCss?, ink?) {
    if (!isNull(val)) {
      const negindex = String(val).indexOf('-');
      const color = this.cardCenterText1(colourCss, ink);
      return (negindex != -1) ? `<span style="color:'${color}'" class="yoyText-red">${val}</span>` : `<span style="color:${color}" class="yoyText">${val}</span>`;
    }
    return '';
  }

  stringForming(data, el, colourCss?, ink?) {
    let string = '';
    string = `${(data['PREFIX'].length) ? `${data['PREFIX']}` : ''}${string}`;
    string = `${(data['PRECAPTION'].length) ? `${this.underscoreChange(data['PRECAPTION'])} ` : ''}${string}`;
    // string = `${string}${this.valueFormat(data)}`;
    if (el != 'PLACEHOLDER4') {
      string = `${string}${this.underscoreChange(data['SOURCECOL'])}`;
    } else {
      string = `${string}${this.underscoreChange(this.colorCss(data['SOURCECOL'], colourCss, ink))}`;
    }
    string = `${string}${(data['SUFFIX'].length) ? `${data['SUFFIX']}` : ''}`;
    string = `${string}${(data['POSTCAPTION'].length) ? ` ${this.underscoreChange(data['POSTCAPTION'])}` : ''}`;
    return string;
  }
  underscoreChange(data) {
    if (isString(data)) {
      data = data.split('_').join(' ');
    }
    return data;
  }

  rowHeight(res) {
    return{
      "height": res.rowHeight
    }
    // if (res.subGroup == 'Y') {
    //   return {
    //     "height": "65%"
    //   }
    // }
    // else {
    //   return {
    //     "height": "35%"
    //   }
    // }
  }

  valueFormat(data) {
    let string = '';
    if (data['NUMBERFORMAT'] == 'Y') {
      let val: any = '';
      data['SOURCECOL'] = String(data['SOURCECOL']);
      if (!isNaN(data['SOURCECOL'].replace(',', ''))) {
        val = (data['SOURCECOL'].includes('.')) ?
          parseFloat(data['SOURCECOL']) : parseInt(data['SOURCECOL']);
      } else {
        val = data['SOURCECOL'];
      }
      if (val != '' && data['SCALING'].length) {
        let count = (data['SCALING'] == 'T') ? 1000 : 1000000;
        string = `${data['SOURCECOL'] / count}`;
      }
    } else {
      string = data['SOURCECOL'];
    }
    return string;
  }


  ngAfterViewInit() {
    this.templateDesign();
    this.cdr.detectChanges();
    // if (!isNullOrUndefined(this.promptValue))  {
    //   setTimeout(() => {
    //     if(!isNullOrUndefined(document.querySelector('[data-label="Vision SBU"]'))){
    //       const sbuSelect = document.querySelector('[data-label="Vision SBU"]').querySelectorAll('.multiselect-item-checkbox');
    //       sbuSelect.forEach((el, ind) => {
    //         const nodeValue = el.querySelector('div').textContent.trim();
    //         if (this.dropDownGroup.includes(nodeValue)) {
    //           el.classList.add('pointer-none');
    //           el.querySelector('div').classList.add('font-weight-bold', 'dropdownGroup');
    //         }
    //       })
    //     }
    //   }, 10);
    // }


  }


  cardCenterText1(data, index?) {
    if (data) {
      if (data.childTileslst[index].theme != null) {
        return data.childTileslst[index].themeSetting.centerTextColor;
      }
      else {
        if (data.theme != null) {
          return data.themeSetting.centerTextColor;
        }
      }
    }
  }




  templateDesign() {
    //this.contentAreaHeight();
    this.sideFilterPosition();
    //this.dashboardContHgt();
    this.sideFilterHeight();
    this.profitValuePosInTile();
    document.getElementById('side-filter-hold').style.opacity = '1';
  }
  // On window resize
  @HostListener('window: resize') adjustContents() {
    this.dashboardDataDetail.forEach(element => {
      element['group'].forEach(element1 => {
        if(element1.columnId == "group3"){
          element1['group'].forEach(element2 => {
            if(element2['dataSource']['tileType'] == 'C' && element2.dataSource.childTileslst.length)
              element2['dataSource']['responsedel']=false;
          });
        }
        else{
          if(element1['dataSource']['tileType'] == 'C' && element1.dataSource.childTileslst.length)
            element1['dataSource']['responsedel']=false;
        }
      });
    });
    setTimeout(() => {
      this.dashboardDataDetail.forEach(element => {
        element['group'].forEach(element1 => {
          if(element1.columnId == "group3"){
            element1['group'].forEach(element2 => {
              if(element2['dataSource']['tileType'] == 'C' && element2.dataSource.childTileslst.length)
                element2['dataSource']['responsedel']=true;
            });
          }
          else{
            if(element1['dataSource']['tileType'] == 'C' && element1.dataSource.childTileslst.length)
              element1['dataSource']['responsedel']=true;
          }
        });
      });
    //this.contentAreaHeight();
    this.sideFilterPosition();
    //this.dashboardContHgt();
    this.sideFilterHeight();
    this.profitValuePosInTile();
    },500);
  }
  // Apply sidebar filter
  applyFilter = (event) => {
    this.toggleSideFilter();
  }
  cancelFilter = (event) => {
    this.toggleSideFilter();
  }
  // Set content area to 100% of the viewport
  contentAreaHeight() {
    const getContentAreaHeight = window.innerHeight;
    const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
    this.leftContainerHeight = getContentAreaHeight + 'px';
    document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
    document.getElementById('card-body').children[0]['style'].height = '100%';
  }

  // Set dashboard container height to 100%
  dashboardContHgt() {
    let dashboardRowHeight;
    let dashboardFirstRowHgt;
    const dashboardRow = document.getElementsByClassName('dashboard-row');
    Array.from(dashboardRow).forEach((elem, index) => {
      if (elem['offsetHeight']) {
        dashboardRowHeight = document.getElementById('branchArea').clientHeight;
        if (elem.children[0].children.length > 1) {
          if (this.templateId == 'T00003' || this.templateId == 'T00006') {
            dashboardFirstRowHgt = elem.children[0].children[1]['offsetHeight'];
            const dashboardSecondRowHgt = dashboardRowHeight - dashboardFirstRowHgt;
            elem.children[0].children[0]['style'].height = `${dashboardSecondRowHgt}px`;
          } else {
            dashboardFirstRowHgt = elem.children[0].children[0]['offsetHeight'];
            const dashboardSecondRowHgt = dashboardRowHeight - dashboardFirstRowHgt;
            elem.children[0].children[1]['style'].height = `${dashboardSecondRowHgt}px`;
          }

        } else {
          if (this.templateId == 'T00005' || this.templateId == 'T00006') {
            document.querySelector('.row.dashboard-row-pl-0').classList.add('h-100');
          }
        }
      }
    });
  }

  // Position profit value inside tile in dashboard
  profitValuePosInTile() {
    let cardFooterValue;
    let profitValueHeight;
    const profitValueElem = document.getElementsByClassName('profitValue');
    if (profitValueElem.length) {
      cardFooterValue = document.getElementsByClassName('footer-seperator')[0]['offsetHeight'];
      profitValueHeight = document.getElementsByClassName('profitValue')[0]['offsetHeight'];
      const bottomVal = (cardFooterValue + profitValueHeight - 5);
      Array.from(profitValueElem).forEach((elem => {
        // elem['style'].bottom = `${bottomVal - 25}px`;
      }));
    }
  }

  // Toggle sidefilter
  toggleSideFilter = () => {
    const sideFilter = document.getElementById('side-filter-hold');
    const sideArrowClass = document.getElementById('sidebarArrow');
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    if (sideFilter.classList.contains('setRight')) {
      sideFilterOverlay.classList.remove('show');
      sideFilter.classList.remove('setRight');
      sideArrowClass.classList.remove('fa-angle-double-right');
    } else {
      sideFilterOverlay.classList.add('show');
      sideFilter.classList.add('setRight');
      sideArrowClass.classList.add('fa-angle-double-right');
    }
  }

  // Set sidefilter height
  sideFilterHeight() {
    setTimeout(() => {
      const dashboardHoldHeight = window.innerHeight;
      const filterHeaderHeight = +document.getElementById('filterHeader').offsetHeight;
      const filterFooterHeight = +document.getElementById('filterFooter').offsetHeight;
      document.getElementById('side-filter-hold').style.height = `${dashboardHoldHeight - 80}px`;
      const setFilterBodyHeight = dashboardHoldHeight - (filterHeaderHeight + filterFooterHeight);
      document.getElementById('filterBody').style.cssText = `height: ${setFilterBodyHeight}px;
                                                            max-height: ${setFilterBodyHeight}px;
                                                            margin-bottom: ${filterFooterHeight}px`;
    }, 180);
  }

  // Get sidefilter position from sidebar width using css variable and update sidebar right position
  // Update sidefilter css right position value from css variable
  sideFilterPosition() {
    const sideFilterHold = document.getElementById('side-filter-hold');
    const filterHoldWidth = document.getElementById('side-filter-hold').offsetWidth;
    sideFilterHold.style.setProperty('--filterWidth', `-${filterHoldWidth}px`);
  }

  // Filter by year || month || date in sidefilter
  fitterBy = (obj) => {
    const filterDataBy = document.getElementsByClassName('filterDataBy');
    Array.from(filterDataBy).forEach((elem) => {
      elem.classList.remove('show');
    });
    if (obj === 'Y') {
      document.getElementById('yearWise').classList.add('show');
    } else if (obj === 'M') {
      document.getElementById('monthWise').classList.add('show');
    } else {
      document.getElementById('dateWise').classList.add('show');
    }
  }


}
