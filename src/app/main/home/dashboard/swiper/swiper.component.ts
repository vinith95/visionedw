import { Component, OnInit, ViewChildren, QueryList, Input, HostListener, AfterViewChecked, ɵCodegenComponentFactoryResolver, ViewChild, ElementRef } from '@angular/core';
import { SwiperComponent } from 'ngx-useful-swiper';
import Swiper, { SwiperOptions } from 'swiper';
import { isNull, isNullOrUndefined, isObject, isArray } from 'util';
import { ApiService, GlobalService } from 'src/app/service';
import { environment } from 'src/environments/environment';
import { loaderSvg } from '../loader';
import * as theme from '../../../../../assets/data/theme.json';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.css']
})
export class SwiperLocalComponent implements OnInit,AfterViewChecked {
  data: any;
  loaderSvg: any = loaderSvg;
  filpConfig: SwiperOptions = {
    updateOnWindowResize:true,
    autoplay: { delay: 3000, disableOnInteraction: true },
    speed: 1200,
    centeredSlides: true,
    spaceBetween:0,
    loop: true,
    effect: 'flip',
    grabCursor: false,
    flipEffect: {
      slideShadows: true, limitRotation: true
    },
    pagination: { el: '.swiper-pagination', clickable: true },
  };

  fadeConfig: SwiperOptions = {
    updateOnWindowResize:true,
    centeredSlides: true,
    spaceBetween:0,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    },
    speed: 1000,
    loop: true,
    effect: 'fade',
    grabCursor: false,
    fadeEffect: {
      crossFade: true
    },
    pagination: { el: '.swiper-pagination', clickable: true },

  };

  slideConfig: SwiperOptions = {
    updateOnWindowResize:true,
    centeredSlides: true,
    spaceBetween:0,
    effect:'slide',
    pagination: { el: '.swiper-pagination', clickable: true },
    autoHeight: false,
    allowTouchMove: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: true
    },
    speed: 800,
    loop: true
  };


  slideChartConfig: SwiperOptions = {
    updateOnWindowResize:true,
    centeredSlides: true,
    spaceBetween:0,
    effect:'slide',
    pagination: { el: '.swiper-pagination', clickable: true },
    autoHeight: false,
    allowTouchMove: true,
    speed: 800,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    loop: true
  };

  coverConfig: SwiperOptions = {
    updateOnWindowResize:true,
    centeredSlides: true,
    spaceBetween:0,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    },
    speed: 800,
    loop: true,
    effect: 'coverflow',
    grabCursor: false,
    coverflowEffect: {
      slideShadows: true,
      rotate: 15,
      stretch: 15,
      depth: 5,
      modifier: 5
    },
    pagination: { el: '.swiper-pagination', clickable: true },
  };
  cubeConfig: SwiperOptions = {
    updateOnWindowResize:true,
    centeredSlides: true,
    spaceBetween:0,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    speed: 1500,
    loop: true,
    slidesPerView: 1,
    slidesPerColumn: 1,
    effect: 'cube',
    grabCursor: false,
    cubeEffect: {
      shadow: false,
      slideShadows: false,
      shadowOffset: 40,
      shadowScale: 0.94,
    },
    pagination: { el: '.swiper-pagination', clickable: true },
  };

  @ViewChildren(SwiperComponent) swiperEle: QueryList<SwiperComponent>
  tileDetails: any;
  popoverTarget: any;
  selectedIndex: any = 0;
  popProfitSuccessClass: any;
  popProfitGreyClass: any;
  popProfitDangerClass: any;
  popoverData: any[];
  tabId: any;
  prompt: any;
  promptValue: any;
  theme: any=theme["default"];
  drillDownlstFlag: boolean = false;
  themeNow: any;
  gradient: any;
  promptDescriptions: any = {};
  @Input() set inputData(value: any) {
    this.data = value;
  }
  @Input() set tileDetail(value: any) {
    this.tileDetails = value;
    this.tabId = value['tabId'];
    this.prompt = value['prompt'];
    this.promptValue = value['promptValue'];
    this.themeNow = value['themeNow'];
    this.gradient = value['gradient']
    this.promptDescriptions = value['promptDescriptions']


  }
  dashboardId: any;
  dropdownSetting= {
    showExportOption: false
  }
  @ViewChild('downloadZipLink', { static: false }) private downloadZipLink: ElementRef;
  constructor(private apiService: ApiService, private globalService: GlobalService) { }

  ngOnInit() {
    this.dashboardId = String(localStorage.getItem('Dashboard_Id'));
    this.globalService.getsideBarChangesDectect().subscribe((params)=>{
      if(params){
          if(!isNullOrUndefined(this.swiperEle)){
            this.swiperEle['_results'].forEach(element => {
              let effect= element.elementRef.nativeElement.id;
              element.swiper.destroy();
              setTimeout(() => {
                if(element.config.effect=='cube')
                  element.swiper = new Swiper('#'+effect+' .swiper-container', this.cubeConfig);
                if(element.config.effect=='flip')
                  element.swiper = new Swiper('#'+effect+' .swiper-container', this.filpConfig);
                if(element.config.effect=='fade')
                  element.swiper = new Swiper('#'+effect+' .swiper-container', this.fadeConfig);
                if(element.config.effect=='cover')
                  element.swiper = new Swiper('#'+effect+' .swiper-container', this.coverConfig);
                if(element.config.effect=='slide')
                  element.swiper = new Swiper('#'+effect+' .swiper-container', this.slideConfig);
                window.dispatchEvent(new Event('resize'));
              }, 600);
            });
          }
       }
    })
  }
  ngAfterViewInit(){


  }

  ngAfterViewChecked(){


  }
  @HostListener('window: resize', ['$event']) onResize(event) {

  }


  startThis(id, data) {
    if (data.openDrill) {
      this.stopThis(id, data)
    }
    else {
      this.swiperEle['_results'].forEach(element => {
        if (element.elementRef.nativeElement.id == id) {
          element.swiper.autoplay.start();
        }
      });
    }
  }

  stopThis(id, data) {

    this.swiperEle['_results'].forEach(element => {
      if (element.elementRef.nativeElement.id == id) {
        element.swiper.autoplay.stop();
      }
    });
  }



  @HostListener('click', ['$event']) drillDownLoop($event) {
    if ($event.target.classList.contains('multiple-fa-line-chart')) {
      let sringId = $event.target.id;
      let id = sringId.split('_');
      this.popoverchart($event, this.data, id[2])
    }
    if (!$event.target.closest('.outside-click')) {
      Object.keys(this.dropdownSetting).forEach(drop=>{
          this.dropdownSetting[drop] = false;
      })
    }
  }

  xmlToJson(xml) {
    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) {
      // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj[attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) {
      // text
      obj = xml.nodeValue;
    }

    // do children
    // If all text nodes inside, get concatenated text from them.
    var textNodes = [].slice.call(xml.childNodes).filter(function (node) {
      return node.nodeType === 3;
    });
    if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
      obj = [].slice.call(xml.childNodes).reduce(function (text, node) {
        return text + node.nodeValue;
      }, "");
    } else if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof obj[nodeName] == "undefined") {
          obj[nodeName] = this.xmlToJson(item);
        } else {
          if (typeof obj[nodeName].push == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(this.xmlToJson(item));
        }
      }
    }
    return obj;
  }

  conversionJson(xml) {
    var xml2 = xml.replace(/&/g, '&amp;');
    const parser = new DOMParser();
    const xml1 = parser.parseFromString(xml2, 'text/xml');
    const json = this.xmlToJson(xml1);

    json['chart'] ? delete json['chart']['#text'] : "";

    if (json['chart'] && json['chart']['useRoundEdges'] && this.themeNow != 'DS_DEFAULT') {
      json['chart']['useRoundEdges'] = 0;
    }

    if (json['chart'] && json['chart']['usePlotGradientColor'] && this.gradient) {
      json['chart']['usePlotGradientColor'] = 1;
      json['chart']['plotGradientColor'] = this.theme[this.themeNow].bgColorChart;
    }
    else {
      if (json['chart'] && json['chart']['use3DLighting'] && this.themeNow != 'DS_DEFAULT') {
        json['chart']['use3DLighting'] = 0;
      }
    }

    if (json['chart'] && json['chart']['set']) {
      if(isArray(json['chart']['set'])){
        json['data'] = json['chart']['set'];
      }
      else{
        json['data']=[]
        json['data'].push(json['chart']['set']);
      }
    }

    if (json['chart'] && json['chart']['categories']) {
      delete json['chart']['categories']['#text'];
      json['categories'] = []
      json['categories'].push(json['chart']['categories']);
      // if (json['chart']['dataset']) {
      //   json['dataset'] = json['chart']['dataset'];
      //   json['dataset'].forEach((element, ind) => {
      //     element['data'] = element['set'];
      //   });
      // }
      json['dataset'] = json['chart']['dataset'];
      !isNullOrUndefined(json['dataset']) && isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
        if(isArray(json['categories'][0]['category'])){
          json['categories'][0]['category']=json['categories'][0]['category'];
        }
        else{
          let tempCat=json['categories'][0]['category'];
          json['categories'][0]['category']=[];
          json['categories'][0]['category'].push(tempCat);
        }
        if (isArray(element['set'])) {
          element['data'] = element['set'];
        }
        else {
          element['data'] = []
          element['data'].push(element['set']);
        }
      });

      if (!isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isObject(json['dataset'])) {
        json['dataset']['data'] = [];
        json['dataset']['data'] = json['dataset']['set'];
        delete json['dataset']['set'];
        delete json['chart']['categories'];
        delete json['chart']['dataset'];
        delete json['chart']['styles'];
        if(isArray(json['categories'][0]['category'])){
          json['categories'][0]['category']=json['categories'][0]['category'];
        }
        else{
          let tempCat=json['categories'][0]['category'];
          json['categories'][0]['category']=[];
          json['categories'][0]['category'].push(tempCat);
        }
        var dataset1 = JSON.parse(JSON.stringify(json));
        json['dataset']=[];
        if(isArray(dataset1['dataset']['data'])){
          json['dataset'].push({"seriesName":dataset1['dataset']['seriesName'],"color":dataset1['dataset']['color'],"data":dataset1['dataset']['data']});
        }
        else{
          json['dataset'].push({"seriesName":dataset1['dataset']['seriesName'],"color":dataset1['dataset']['color'],"data":[dataset1['dataset']['data']]});
        }
      } else {
        !isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
          element['data'] = element['set']
        });
      }
    }
    if (json['chart']){
      json['chart']['bgColor'] = this.theme[this.themeNow].drillDownChartColor;
      json['chart']['baseFontColor'] = this.theme[this.themeNow].drillDownChartLabelColor;
    }

    return json
  }

  // Open popover chart
  popoverchart(event, data, index) {
    data.openDrill = true;
    this.selectedIndex = index;
    let sendData = data.childTileslst[index].drillDownlst;
    let dashboardFilterRequest = {};
    if (sendData.length) {
      if(!isNullOrUndefined(this.promptValue)){
        let prompKeys= Object.keys(this.promptValue)
        let itemKeys= Object.keys(sendData[0])
        prompKeys.forEach(element => {
          itemKeys.forEach(element1 => {
            if(element1 == element){
              sendData[0][element1] = this.promptValue[element];
            }
          });
        });
      // const values = Object.values(this.prompt);

      // values.forEach((element, promptIndex) => {
      //   if (this.promptValue[promptIndex]['filterType'] == 'COMBO') {
      //     this.promptValue[promptIndex]['filterDefaultValue'] = element[0]['id'];
      //     sendData[0][`promptValue${this.promptValue[promptIndex]['promptValueId']}`] = `'${element[0]['id']}'`;
      //   }
      //   if (this.promptValue[promptIndex]['filterType'] == 'DATE') {
      //     let val = this.dateReConversion(element);
      //     this.promptValue[promptIndex]['filterDefaultValue'] = val;
      //     sendData[0][`promptValue${this.promptValue[promptIndex]['promptValueId']}`] = `'${val}'`;
      //   }
      //   if (!['COMBO', 'DATE'].includes(this.promptValue[promptIndex]['filterType'])) {
      //     this.promptValue[promptIndex]['filterDefaultValue'] = element;
      //     sendData[0][`promptValue${this.promptValue[promptIndex]['promptValueId']}`] = `'${element}'`;
      //   }
      // });
    }
      let obj = [{
        tabId: this.tabId,
        dashboardId: this.dashboardId,
        tileSeq: data.tileSequence,
        ...dashboardFilterRequest
      }];
      this.popoverOpen(event);
      this.drillDownlstFlag = false;
      this.apiService.post(environment.getDrillDownData, sendData).subscribe(ele => {
        this.drillDownlstFlag = true;
        data.childTileslst[index]['drillDownlst'] = (('response' in ele) && !isNull(ele['response'])) ?
          (('drillDownlst' in ele['response']) && !isNull(ele['drillDownlst'])) ? ele['response']['drillDownlst'] : [] : [];
        if (data.childTileslst[index]['drillDownlst'][0]["tileType"] == 'C') {
          if (!isNullOrUndefined(data.childTileslst[index]['drillDownlst'][0]['chartDataSet1'])) {
            data.childTileslst[index]['drillDownlst'][0]['chartDataSet1'] = (data.childTileslst[index]['drillDownlst'][0]['chartDataSet1'] != "") ? this.conversionJson(data.childTileslst[index]['drillDownlst'][0]['chartDataSet']) : "";
          }
          else {
            data.childTileslst[index]['drillDownlst'][0]['chartDataSet1'] = ""
          }
        }

      })
    }
    else {
      this.globalService.showToastr.warning("DrillDown is not configured properly.")
    }


  }
  months = { '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec' };
  dateReConversion(element) {
    let month = String(element.month).length == 1 ? `0${element.month}` : element.month;
    let day = String(element.day).length == 1 ? `0${element.day}` : element.day;
    return `${day}-${this.months[month]}-${element.year}`
  }


  popoverOpen(event) {
    // 'dashboardId': this.dashboardId

    // Reset popover array
    this.popoverData = [];
    // Reset all visible popover element
    const popoverHold = document.getElementsByClassName('popoverHold');
    const navbarheight = document.getElementsByClassName('navbar')[0]['offsetHeight'];
    const dashboardMenuHeight = document.getElementById('dashboard-hold').offsetHeight;
    Array.from(popoverHold).forEach((pophold) => {
      pophold['style'].height = '100%';
      pophold['style'].width = '100%';
      pophold['style'].display = 'none';
    });

    const maintileElem = event.target.closest('.maintile');
    // Set profit level indicator class
    if (maintileElem.querySelector('.profitValue')) {
      this.popProfitSuccessClass = maintileElem.querySelector('.profitValue').classList.contains('text-success');
      this.popProfitDangerClass = maintileElem.querySelector('.profitValue').classList.contains('text-danger');
      this.popProfitGreyClass = maintileElem.querySelector('.profitValue').classList.contains('text-muted');
    }
    // Check if the tile type is chart or data
    const tileType = maintileElem.classList.contains('chartTile');
    // Get top and left position of the parent maintile element
    const targetTop = +maintileElem.getBoundingClientRect().top - 50;
    const targetLeft = +maintileElem.getBoundingClientRect().left - 100;
    // Get window height
    const windowWidth = +window.innerWidth;
    const windowHeight = +window.innerHeight;
    // Calculate height and width for popover div
    let popoverDivHeight = +(windowHeight / 4 * 2);
    if (tileType) {
      popoverDivHeight = windowHeight - (navbarheight + dashboardMenuHeight);
    }
    const popoverDivWidth = +windowWidth / 2;
    // Get left position and width of the target parent element
    const maintileLeft = maintileElem.getBoundingClientRect().left;
    const popDivLeftandWidth = maintileLeft + popoverDivWidth;
    // Get top position and height of the target parent element
    const mailtileTop = maintileElem.getBoundingClientRect().top;
    const popDivTopandHeight = mailtileTop + popoverDivHeight;
    // Set popover div left/right based on viewport
    if ((windowWidth - 100) < popDivLeftandWidth) {
      maintileElem.querySelector('#popoverHold').style.left = 'unset';
      maintileElem.querySelector('#popoverHold').style.right = '-2px';
    } else {
      maintileElem.querySelector('#popoverHold').style.left = '-2px';
      maintileElem.querySelector('#popoverHold').style.right = 'unset';
    }
    // Set popover div top/bottom based on viewport
    if ((windowHeight - 100) < popDivTopandHeight) {
      maintileElem.querySelector('#popoverHold').style.top = 'unset';
      maintileElem.querySelector('#popoverHold').style.bottom = '-2px';
    } else {
      maintileElem.querySelector('#popoverHold').style.top = '-2px';
      maintileElem.querySelector('#popoverHold').style.bottom = 'unset';
    }

    const popoverchartElem = maintileElem.querySelector('#popoverHold');
    popoverchartElem.style.display = 'block';
    popoverchartElem.style['z-index'] = '1000';

    const objectIndex = maintileElem.getAttribute('data-index');
    const objectChildIndex = maintileElem.getAttribute('data-child-index');
    // Set height and width for the popover element
    setTimeout(() => {
      popoverchartElem.style.opacity = 1;
      if (windowWidth > 425) {
        popoverchartElem.style.height = `${popoverDivHeight * 1.5}px`;
        popoverchartElem.style.width = `${popoverDivWidth * 1.2}px`;
      } else if (windowWidth <= 425) {
        popoverchartElem.style.left = '50%';
        popoverchartElem.style.top = '50%';
        popoverchartElem.style.transform = 'translate(-50%, -50%)';
        popoverchartElem.style.height = `${popoverDivHeight}px`;
        popoverchartElem.style.width = `${popoverDivWidth}px`;
      } else {
        popoverchartElem.style.height = `${(windowHeight - 200)}px`;
        popoverchartElem.style.width = `${windowWidth - 50}px`;
      }
    }, 50);
  }


  // Open popover chart
  closePopoverChart(event, data) {
    data.openDrill = false;
    this.startThis('swiperSeq' + data.tileSequence, data);
    ///this.startThis("all",data);
    const mainTile = event.target.closest('.maintile');
    this.popoverTarget = event.target.closest('.footer-seperator');
    const popoverchartElem = mainTile.querySelector('#popoverHold');
    const parentHeight = mainTile.offsetHeight;
    const parentWidth = mainTile.offsetWidth;
    popoverchartElem.style.height = '100%'; // `${parentHeight}px`;
    popoverchartElem.style.width = '100%'; // `${parentWidth}px`;
    popoverchartElem.style['z-index'] = '24';
    setTimeout(() => {
      // event.target.closest('.popoverHold').style.opacity = 0;
      event.target.closest('.popoverHold').style.display = 'none';
    }, 200);
  }

  toggleShowDrop(event,dropId) {
    Object.keys(this.dropdownSetting).forEach(drop=>{
      if(dropId == drop)
        this.dropdownSetting[drop] = true;
      else
        this.dropdownSetting[drop] = false;
    })
  }

  exportGridDrillDown(type, viewList, drop, mode?) {
    drop.close();
    this.dropdownSetting.showExportOption = false;
    let exportreq = JSON.parse(JSON.stringify(this.data.childTileslst[0].drillDownlst[0]));
    const keys = Object.keys(this.promptDescriptions['promptDescriptions']);
    let promptDesc = [];
    if(this.promptDescriptions['promptArr'] && this.promptDescriptions['promptArr'].length)
    this.promptDescriptions['promptArr'].forEach((label) => {
      keys.forEach((prompt) => {
        if (`promptDescription${label.filterSeq}` == prompt)
        promptDesc.push(`${label.filterLabel} : ${this.promptDescriptions['promptDescriptions'][prompt]}`)
      })
    })
    exportreq['promptLabel'] = promptDesc.join('!@#');
    exportreq['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
    exportreq['isDrillDown'] = true;
    let api = viewList == 'xlsx' ? environment.dashboardExcelDataforGrid : viewList == 'pdf' ?
     environment.dashboardPdfExportforGrid : '';
    let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
    this.apiService.fileDownloads(api, exportreq).subscribe((resp) => {
      if (resp['type'] != 'application/json') {
        const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        if (type == 'download') {
          const link = this.downloadZipLink.nativeElement;
          link.href = url;
          link.download = viewList == 'xlsx' ? `${exportreq.tileCaption}_${visionId}.xlsx` :
            viewList == 'csv' ? `${exportreq.tileCaption}_${visionId}.csv` :
              `${exportreq.tileCaption}_${visionId}.pdf`;
          link.click();
        }
        window.URL.revokeObjectURL(url);
      }
      else {
        this.globalService.showToastr.error("No Records Found")
      }
    }, (error: any) => {
      if (error.substr(12, 3) == '204') {
        this.globalService.showToastr.error('No Records to Export');
      }
      if (error.substr(12, 3) == '420') {
        this.globalService.showToastr.error('Error Generating Report');
      }
      if (error.substr(12, 3) == '417') {
        this.globalService.showToastr.error('Error while exporting the report');
      }
    });
  }
}
