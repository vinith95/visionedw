import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SwiperLocalComponent } from './swiper.component';

describe('SwiperLocalComponent', () => {
  let component: SwiperLocalComponent;
  let fixture: ComponentFixture<SwiperLocalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SwiperLocalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwiperLocalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
