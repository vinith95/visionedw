import { Component, OnInit, Input, NgZone, ViewChild, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';
import { ApiService, GlobalService } from 'src/app/service';
import { environment } from 'src/environments/environment';
import { isArray, isNullOrUndefined, isObject } from 'util';
import * as theme from '../../../../../assets/data/theme.json';
import { loaderSvg } from '../loader';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  data: any;
  drillDownKeys: {};
  promptValue: any;
  themeNow: any;
  drillDownlstFlag: boolean = false;
  listLineType = ['MSColumnLine3D', 'StackedColumn2DLine', 'StackedColumn3DLine'];
  listLineTypeDY = ['MSColumn3DLineDY', 'StackedColumn3DLineDY'];
  theme: any = theme['default'];
  loaderSvg: any = loaderSvg;
  promptDescriptions: any = {};
  totalChildTiledData: any = [];

  @Input() set inputData(value: any) {
    this.data=value;
  }

  @Input() set tileDetail(value: any) {
    // this.tabId = value['tabId'];
    // this.prompt = value['prompt'];
    this.promptValue = value['promptValue'];
    this.themeNow = value['themeNow'];
    this.promptDescriptions = value['promptDescriptions']
    // this.gradient = value['gradient']
    this.totalChildTiledData = value['childTiledData'];

  }

  @Output() childData = new EventEmitter();
  dropdownSetting= {
    showExportOption: false
  }
  @ViewChild('downloadZipLink', { static: false }) private downloadZipLink: ElementRef;

  constructor(private zone: NgZone,
    private apiService: ApiService,
    private globalService: GlobalService) { }

  ngOnInit() {
  }
  cardBackgroudColor(data,index?){
    if(data.subTiles == "N"){
      if(data.theme !=null){
        return {
          "background-color":data.themeSetting.backgroundColor,
          "border":data.themeSetting.border
        }
      }
      else{
        return{}
      }
    }
    else{
      if(data.childTileslst[index].theme !=null){
        return {
          "background-color":data.childTileslst[index].themeSetting.backgroundColor,
          "border":data.childTileslst[index].themeSetting.border
        }
      }
      else{
        if(data.theme !=null){
          return {
            "background-color":data.themeSetting.backgroundColor,
            "border":data.themeSetting.border
        }
        }
        else{
          return{}
        }
      }
    }
  }
  cardColor(data,index?){
    if(data.tileType == "T"){
      if(data.subTiles == "N"){
        if(data.theme !=null){
          return {"color":data.themeSetting.color}
        }
        else{
          return{}
        }
      }
      else{
        if(data.childTileslst[index].theme !=null){
          return {"color":data.childTileslst[index].themeSetting.color}
        }
        else{
          if(data.theme !=null){
            return {"color":data.themeSetting.color}
          }
          else{
            return{}
          }
        }
      }
    }
    else{
      return{}
    }

  }


  drillDownReq(event, data, rowData) {
    let childTile = [];
    if (!isNullOrUndefined(this.totalChildTiledData))
      this.totalChildTiledData.forEach(ele => {
        if (ele.parentSequence == data.tileSequence) {
          childTile.push(ele);
        }
      })
    if (data.drillDownFlag && !childTile.length) {
      if (!isNullOrUndefined(data.childTileslst) && data.childTileslst.length && !isNullOrUndefined(data.childTileslst[0].drillDownlst) &&
        data.childTileslst[0].drillDownlst.length) {
        // this.drillDownList = [];
        this.drillDownKeys = {};
        let tmpStr = `drillDownKey${1}`
        let ddkey;
        // let ddData;
        this.popoverOpen(event)
        // let dataIndex = event.dataObj.dataIndex;
        // this.zone.run(() => {
        ddkey = !isNullOrUndefined(rowData['DDKEYID']) ? rowData['DDKEYID'] : '';
        // this.popoverOpenDrillDown(event, 'G');
        let breadCrumb, breadCrumbArr;
        breadCrumbArr = data.childTileslst[0].gridColumnSet.filter(items => items.drillDownLabel);
        if (breadCrumbArr.length) {
          // breadCrumbLabel = !isNullOrUndefined(breadCrumbArr[0].caption) && breadCrumbArr[0].caption != '' ? breadCrumbArr[0].caption : 'DDKEYID';
          breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
        }
        // else {
        //   breadCrumb = 'DDKEYID';
        // }
        // ddData = {
        //   tabId: ddkey,
        //   tabLabel: breadCrumbArr[0].caption,
        //   tabName: rowData[breadCrumb],
        //   index: i,
        //   currentLevelFlag: true,
        //   titleColor: this.titleColor,
        //   ddObjectType: item['data']['objectType']
        // }
        this.drillDownKeys[tmpStr] = `'${ddkey}'`;
        // this.drillDownHit(item, ddkey, ddData, i)
        data['drillDownlst'] = [];
        // data.openDrill = true;
        let sendData;
        // if (this.swiperIndex == 'single') {
        sendData = data.childTileslst[0].drillDownlst;
        if (!isNullOrUndefined(this.promptValue)) {
          let prompKeys = Object.keys(this.promptValue)
          let itemKeys = Object.keys(sendData[0])
          prompKeys.forEach(element => {
            itemKeys.forEach(element1 => {
              if (element1 == element) {
                sendData[0][element1] = this.promptValue[element];
              }
            });
          });
        }
        sendData[0][tmpStr] = `'${ddkey}'`;
        // this.popoverOpen(event);
        this.drillDownlstFlag = false;
        this.apiService.post(environment.getDrillDownData, sendData).subscribe(resp => {
          data['drillDownlst'] = (resp['status'] == 1 && !isNullOrUndefined(resp['response']) &&
            !isNullOrUndefined(resp['response'].drillDownlst)) ? resp['response'].drillDownlst : [];
          if (resp['status'] == 1 && !isNullOrUndefined(resp['response'])) {
            if (!isNullOrUndefined(data['drillDownlst'][0]) && data['drillDownlst'][0]["tileType"] == 'C') {
              if (!isNullOrUndefined(data['drillDownlst'][0]['chartDataSet']) && data['drillDownlst'][0]['chartDataSet'] != '') {
                if (this.listLineType.includes(data['drillDownlst'][0]['chartType'])) {
                  let datas = data['drillDownlst'][0]['chartDataSet'];
                  var regexp = /<dataset/g;
                  let c = datas.match(regexp) || [];
                  data['drillDownlst'][0]['chartDataSet'] = this.replaceOccurrence(datas, /<dataset/g, c.length, '<dataset renderas="line"') || '';
                }
                if (this.listLineTypeDY.includes(data['drillDownlst'][0]['chartType'])) {
                  let datas = data['drillDownlst'][0]['chartDataSet'];
                  var regexp = /<dataset/g;
                  let c = datas.match(regexp) || [];
                  data['drillDownlst'][0]['chartDataSet'] = this.replaceOccurrence(datas, /<dataset/g, c.length, '<dataset renderas="line" parentyaxis="S" ') || '';
                }
                data['drillDownlst'][0]['chartDataSet1'] = this.conversionJson(data['drillDownlst'][0]['chartDataSet']);
                data['drillDownlst'][0]['chartDataSet1'].chart.chartLeftMargin = 10;
                data['drillDownlst'][0]['chartDataSet1'].chart.chartTopMargin = 20;
                data['drillDownlst'][0]['chartDataSet1'].chartRightMargin = 0;
                data['drillDownlst'][0]['chartDataSet1'].chart.chartBottomMargin = 0;
                // data['drillDownlst'][0]['chartDataSet1'] = FusionCharts.transcodeData(data['drillDownlst'][0]['chartDataSet'], "xml", "json",false)
              }
              else {
                data['drillDownlst'][0]['chartDataSet1'] = "";
              }
            }
          }
          else {
            data['drillDownlst'].push({ errorMessage: resp['message'] })
          }
          if (!isNullOrUndefined(data['drillDownlst'][0])) {
            data['drillDownlst'][0]['tileCaption'] += !isNullOrUndefined(breadCrumb && rowData[breadCrumb]) ? ` : ${rowData[breadCrumb]}` : '';
          }
          setTimeout(() => { this.drillDownlstFlag = true }, 500)
        })
        // }
        // });
      }
    }
    if (childTile.length) {
      let ddkey = !isNullOrUndefined(rowData['DDKEYID']) ? rowData['DDKEYID'] : '';
      let title = '';
      let breadCrumb = '';
      let breadCrumbArr = [];
        breadCrumbArr = data.childTileslst[0].gridColumnSet.filter(items => items.drillDownLabel);
        if (breadCrumbArr.length) {
          // breadCrumbLabel = !isNullOrUndefined(breadCrumbArr[0].caption) && breadCrumbArr[0].caption != '' ? breadCrumbArr[0].caption : 'DDKEYID';
          breadCrumb = !isNullOrUndefined(breadCrumbArr[0].dbColumnName) && breadCrumbArr[0].dbColumnName != '' ? breadCrumbArr[0].dbColumnName : 'DDKEYID';
        }
        title = !isNullOrUndefined(breadCrumb && rowData[breadCrumb]) ? rowData[breadCrumb] : '';
      let emitData = {
        type: 'parentChild',
        ddKey: ddkey,
        data: data,
        title: title
      }
      this.childData.emit(emitData)
    }
  }

  replaceOccurrence(string, regex, n, replace) {
    var i = 0;
    return string.replace(regex, function (match) {
      i += 1;
      if (i === n) return replace;
      return match;
    });
  }

  conversionJson(xml) {
    var xml2 = xml.replace(/&/g, '&amp;');
    const parser = new DOMParser();
    const xml1 = parser.parseFromString(xml2, 'text/xml');
    const json = this.xmlToJson(xml1);
    delete json['chart']['#text'];
    if (json['chart'] && json['chart']['set']) {
      if(isArray(json['chart']['set'])){
        json['data'] = json['chart']['set'];
      }
      else{
        json['data']=[]
        json['data'].push(json['chart']['set']);
      }
      json['chart']['bgColor'] = this.theme.bgColorChart;
      json['chart']['baseFontColor'] = this.theme.labelColor;
    }
    if (json['chart'] && json['chart']['categories']) {
      delete json['chart']['categories']['#text'];
      json['categories'] = []
      json['categories'].push(json['chart']['categories']);
      json['dataset'] = json['chart']['dataset'];
      !isNullOrUndefined(json['dataset']) && isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
        if(isArray(json['categories'][0]['category'])){
          json['categories'][0]['category']=json['categories'][0]['category'];
        }
        else{
          let tempCat=json['categories'][0]['category'];
          json['categories'][0]['category']=[];
          json['categories'][0]['category'].push(tempCat);
        }
        if (isArray(element['set'])) {
          element['data'] = element['set'];
        }
        else {
          element['data'] = []
          element['data'].push(element['set']);
        }
      });

      if (!isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isObject(json['dataset'])) {
        json['dataset']['data'] = [];
        json['dataset']['data'] = json['dataset']['set'];
        delete json['dataset']['set'];
        delete json['chart']['categories'];
        delete json['chart']['dataset'];
        delete json['chart']['styles'];
        if(isArray(json['categories'][0]['category'])){
          json['categories'][0]['category']=json['categories'][0]['category'];
        }
        else{
          let tempCat=json['categories'][0]['category'];
          json['categories'][0]['category']=[];
          json['categories'][0]['category'].push(tempCat);
        }
        var dataset1 = JSON.parse(JSON.stringify(json));
        json['dataset']=[];
        if(isArray(dataset1['dataset']['data'])){
          json['dataset'].push({"seriesName":dataset1['dataset']['seriesName'],"color":dataset1['dataset']['color'],"data":dataset1['dataset']['data']});
        }
        else{
          json['dataset'].push({"seriesName":dataset1['dataset']['seriesName'],"color":dataset1['dataset']['color'],"data":[dataset1['dataset']['data']]});
        }

      }
      else {
        !isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
          element['data'] = element['set']
        });
      }
    }
    if (!isNullOrUndefined(json['chart'])) {
      json['chart']['bgColor'] = this.theme[this.themeNow].drillDownChartColor;
      json['chart']['baseFontColor'] = this.theme[this.themeNow].drillDownChartLabelColor;
    }
    json['chart']['bgColor'] = this.theme[this.themeNow].drillDownChartColor;
    json['chart']['baseFontColor'] = this.theme[this.themeNow].drillDownChartLabelColor;

    return json
  }

  xmlToJson(xml) {
    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) {
      // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj[attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) {
      // text
      obj = xml.nodeValue;
    }

    // do children
    // If all text nodes inside, get concatenated text from them.
    var textNodes = [].slice.call(xml.childNodes).filter(function (node) {
      return node.nodeType === 3;
    });
    if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
      obj = [].slice.call(xml.childNodes).reduce(function (text, node) {
        return text + node.nodeValue;
      }, "");
    } else if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof obj[nodeName] == "undefined") {
          obj[nodeName] = this.xmlToJson(item);
        } else {
          if (typeof obj[nodeName].push == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(this.xmlToJson(item));
        }
      }
    }
    return obj;
  }

   // Open popover chart
  popoverOpen(event) {

    // Reset popover array
    // this.popoverData = [];
    // Reset all visible popover element
    const popoverHold = document.getElementsByClassName('popoverHold');
    const navbarheight = document.getElementsByClassName('navbar')[0]['offsetHeight'];
    const dashboardMenuHeight = document.getElementById('dashboard-hold').offsetHeight;
    Array.from(popoverHold).forEach((pophold) => {
      pophold['style'].height = '100%';
      pophold['style'].width = '100%';
      pophold['style'].display = 'none';
    });

    const maintileElem = event.target.closest('.maintile');
    // const maintileElem = event.closest('.maintile');
    // Set profit level indicator class
    // if (maintileElem.querySelector('.profitValue')) {
    //   this.popProfitSuccessClass = maintileElem.querySelector('.profitValue').classList.contains('text-success');
    //   this.popProfitDangerClass = maintileElem.querySelector('.profitValue').classList.contains('text-danger');
    //   this.popProfitGreyClass = maintileElem.querySelector('.profitValue').classList.contains('text-muted');
    // }
    // Check if the tile type is chart or data
    const tileType = maintileElem.classList.contains('chartTile');
    // Get top and left position of the parent maintile element
    const targetTop = +maintileElem.getBoundingClientRect().top - 50;
    const targetLeft = +maintileElem.getBoundingClientRect().left - 100;
    // Get window height
    const windowWidth = +window.innerWidth;
    const windowHeight = +window.innerHeight;
    // Calculate height and width for popover div
    let popoverDivHeight = +(windowHeight / 4 * 2);
    // if (tileType) {
    //   popoverDivHeight = windowHeight - (navbarheight + dashboardMenuHeight);
    // }
    const popoverDivWidth = +windowWidth / 2;
    // Get left position and width of the target parent element
    const maintileLeft = maintileElem.getBoundingClientRect().left;
    const popDivLeftandWidth = maintileLeft + popoverDivWidth;
    // Get top position and height of the target parent element
    const mailtileTop = maintileElem.getBoundingClientRect().top;
    const popDivTopandHeight = mailtileTop + popoverDivHeight;
    // Set popover div left/right based on viewport
    if ((windowWidth - 100) < popDivLeftandWidth) {
      maintileElem.querySelector('#popoverHold').style.left = 'unset';
      maintileElem.querySelector('#popoverHold').style.right = '-2px';
    } else {
      maintileElem.querySelector('#popoverHold').style.left = '-2px';
      maintileElem.querySelector('#popoverHold').style.right = 'unset';
    }
    // Set popover div top/bottom based on viewport
    if ((windowHeight - 100) < popDivTopandHeight) {
      maintileElem.querySelector('#popoverHold').style.top = 'unset';
      maintileElem.querySelector('#popoverHold').style.bottom = '-2px';
    } else {
      maintileElem.querySelector('#popoverHold').style.top = '-2px';
      maintileElem.querySelector('#popoverHold').style.bottom = 'unset';
    }

    const popoverchartElem = maintileElem.querySelector('#popoverHold');
    popoverchartElem.style.display = 'block';
    popoverchartElem.style['z-index'] = '1000';

    const objectIndex = maintileElem.getAttribute('data-index');
    const objectChildIndex = maintileElem.getAttribute('data-child-index');
    // Set height and width for the popover element
    setTimeout(() => {
      popoverchartElem.style.opacity = 1;
      if (windowWidth > 425) {
        popoverchartElem.style.height = `${popoverDivHeight * 1.5}px`;
        popoverchartElem.style.width = `${popoverDivWidth * 1.2}px`;
      } else if (windowWidth <= 425) {
        popoverchartElem.style.left = '50%';
        popoverchartElem.style.top = '50%';
        popoverchartElem.style.transform = 'translate(-50%, -50%)';
        popoverchartElem.style.height = `${popoverDivHeight}px`;
        popoverchartElem.style.width = `${popoverDivWidth}px`;
      } else {
        popoverchartElem.style.height = `${(windowHeight - 200)}px`;
        popoverchartElem.style.width = `${windowWidth - 50}px`;
      }
    }, 50);
  }

  // close popover chart
  closePopoverChart(event, data) {
    data.openDrill = false;
    // this.startThis('swiperSeq' + data.tileSequence, data);
    ///this.startThis("all",data);
    const mainTile = event.target.closest('.maintile');
    // this.popoverTarget = event.target.closest('.footer-seperator');
    const popoverchartElem = mainTile.querySelector('#popoverHold');
    const parentHeight = mainTile.offsetHeight;
    const parentWidth = mainTile.offsetWidth;
    popoverchartElem.style.height = '100%'; // `${parentHeight}px`;
    popoverchartElem.style.width = '100%'; // `${parentWidth}px`;
    popoverchartElem.style['z-index'] = '24';
    setTimeout(() => {
      // event.target.closest('.popoverHold').style.opacity = 0;
      event.target.closest('.popoverHold').style.display = 'none';
    }, 200);
  }


  removeComma(data) {
    return data.replaceAll(",","")
  }

  @HostListener('document:click', ['$event']) close(event) {
    if (!event.target.closest('.outside-click')) {
      Object.keys(this.dropdownSetting).forEach(drop=>{
          this.dropdownSetting[drop] = false;
      })
    }
  }

  toggleShowDrop(event,dropId) {
    Object.keys(this.dropdownSetting).forEach(drop=>{
      if(dropId == drop)
        this.dropdownSetting[drop] = true;
      else
        this.dropdownSetting[drop] = false;
    })
  }

  exportGrid(type, viewList, drop, mode?) {
    drop.close();
    this.dropdownSetting.showExportOption = false;
    let exportreq = JSON.parse(JSON.stringify(this.data['tileRequest']));
    const keys = Object.keys(this.promptDescriptions['promptDescriptions']);
    let promptDesc = [];
    if(this.promptDescriptions['promptArr'] && this.promptDescriptions['promptArr'].length)
    this.promptDescriptions['promptArr'].forEach((label) => {
      keys.forEach((prompt) => {
        if (`promptDescription${label.filterSeq}` == prompt)
        promptDesc.push(`${label.filterLabel} : ${this.promptDescriptions['promptDescriptions'][prompt]}`)
      })
    })
    exportreq['promptLabel'] = promptDesc.join('!@#');
    exportreq['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
    let api = viewList == 'xlsx' ? environment.dashboardExcelDataforGrid : viewList == 'pdf' ?
     environment.dashboardPdfExportforGrid : '';
    let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
    this.apiService.fileDownloads(api, exportreq).subscribe((resp) => {
      if (resp['type'] != 'application/json') {
        const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        if (type == 'download') {
          const link = this.downloadZipLink.nativeElement;
          link.href = url;
          link.download = viewList == 'xlsx' ? `${exportreq.tileCaption}_${visionId}.xlsx` :
            viewList == 'csv' ? `${exportreq.tileCaption}_${visionId}.csv` :
              `${exportreq.tileCaption}_${visionId}.pdf`;
          link.click();
        }
        window.URL.revokeObjectURL(url);
      }
      else {
        this.globalService.showToastr.error("No Records Found")
      }
    }, (error: any) => {
      if (error.substr(12, 3) == '204') {
        this.globalService.showToastr.error('No Records to Export');
      }
      if (error.substr(12, 3) == '420') {
        this.globalService.showToastr.error('Error Generating Report');
      }
      if (error.substr(12, 3) == '417') {
        this.globalService.showToastr.error('Error while exporting the report');
      }
    });
  }

  exportGridDrillDown(type, viewList, drop, mode?) {
    drop.close();
    this.dropdownSetting.showExportOption = false;
    let exportreq = JSON.parse(JSON.stringify(this.data.childTileslst[0].drillDownlst[0]));
    const keys = Object.keys(this.promptDescriptions['promptDescriptions']);
    let promptDesc = [];
    if(this.promptDescriptions['promptArr'] && this.promptDescriptions['promptArr'].length)
    this.promptDescriptions['promptArr'].forEach((label) => {
      keys.forEach((prompt) => {
        if (`promptDescription${label.filterSeq}` == prompt)
        promptDesc.push(`${label.filterLabel} : ${this.promptDescriptions['promptDescriptions'][prompt]}`)
      })
    })
    exportreq['promptLabel'] = promptDesc.join('!@#');
    exportreq['applicationTheme'] = this.globalService.selectedTheme['color'].replaceAll("#","");
    exportreq['isDrillDown'] = true;
    let api = viewList == 'xlsx' ? environment.dashboardExcelDataforGrid : viewList == 'pdf' ?
     environment.dashboardPdfExportforGrid : '';
    let visionId = JSON.parse(localStorage.getItem('themeAndLanguage')).visionId;
    this.apiService.fileDownloads(api, exportreq).subscribe((resp) => {
      if (resp['type'] != 'application/json') {
        const blob = new Blob([resp], { type: viewList == 'xlsx' ? 'text/xls' : viewList == 'csv' ? 'text/csv' : 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        if (type == 'download') {
          const link = this.downloadZipLink.nativeElement;
          link.href = url;
          link.download = viewList == 'xlsx' ? `${exportreq.tileCaption}_${visionId}.xlsx` :
            viewList == 'csv' ? `${exportreq.tileCaption}_${visionId}.csv` :
              `${exportreq.tileCaption}_${visionId}.pdf`;
          link.click();
        }
        window.URL.revokeObjectURL(url);
      }
      else {
        this.globalService.showToastr.error("No Records Found")
      }
    }, (error: any) => {
      if (error.substr(12, 3) == '204') {
        this.globalService.showToastr.error('No Records to Export');
      }
      if (error.substr(12, 3) == '420') {
        this.globalService.showToastr.error('Error Generating Report');
      }
      if (error.substr(12, 3) == '417') {
        this.globalService.showToastr.error('Error while exporting the report');
      }
    });
  }

  setTitle(data) {
    return !isNullOrUndefined(data) ? data.replace(" - ", "") : "";
 }

}
