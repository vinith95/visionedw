import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as dashboardTheme from '../../../assets/data/dashboardTheme.json';
import { environment } from '../../../environments/environment';
import { ApiService, GlobalService, JwtService } from '../../service';
import { isNull, isNullOrUndefined } from 'util';
import { MagnifierComponent } from 'src/app/shared/magnifier/magnifier.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GenericTreeComponent } from 'src/app/shared/generic-tree/generic-tree.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  template2: boolean;
  branchValue;
  relManagerValue;
  customerValue;
  productsValue;
  industriesValue;
  pipelineValue;
  tabId: any;
  currentTabIndex: any = 0;
  customerId: boolean = false;
  tabIndex: number = 0;
  themeNow: string;
  dashboardId: string;
  filterForm: FormGroup;
  filterSubmitted: boolean;
  filterVal: {};
  magnifierId = {};
  treeId = {};
  promptDescriptions: any = {};
  constructor(
    private globalService: GlobalService,
    private apiService: ApiService,
    private modalService: NgbModal,
    private jwtService: JwtService
  ) {
  }
  leftContainerHeight: string;
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true
  };
  dashboardFilterSettings: IDropdownSettings = {
    singleSelection: true,
    idField: 'id',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    closeDropDownOnSelection: true,
  };

  promptValues: any={};
  dashboardDetails;
  dashboardFilterList;
  dashboardFilterFlag: boolean;
  template1: boolean;
  overallPromptValues: any = [];
  dashboardFilterPrompts: any = [];

  templateData = {
    dashboardTabData: [],
    dashboardFilterFlag: false,
    dashboardFilterList: [],
    templateId: ''
  };
  dashboardTheme: any = dashboardTheme['default'];
  hidShow = false;
  magnifierPrompt: any;

  ngOnInit() {
    this.getDashboardDetails();
  }

  promptArray() { return this.filterForm.controls; }

  groupBy(xs, key) {
    return xs.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  ngAfterViewInit() { }

  // On window resize
  @HostListener('window: resize') adjustContents() {

  }


  // Get dashboard details
  getDashboardDetails = () => {
    this.dashboardId = String(localStorage.getItem('Dashboard_Id'));
    if (!['na', 'null', ''].includes(this.dashboardId.toLowerCase())) {
      const dashboardRequest = {
        'dashboardId': this.dashboardId
      };
      this.apiService.post(environment.getDashboardOnLoad, dashboardRequest).subscribe((resp) => {
        if (resp['status']) {
          this.globalService.pageTitle = resp['response'].dashboardName;
          if (!isNull(resp['response'].dashboardTabs) && resp['response'].dashboardTabs.length) {
            this.dashboardDetails = resp['response'].dashboardTabs;
            this.templateData['themeNow'] = resp['response']['dashboard_Theme'];

            if (isNullOrUndefined(this.templateData['themeNow']) || (this.templateData['themeNow'] == ""))
              this.templateData['themeNow'] = 'DS_DEFAULT';
            let themeTemp = this.templateData['themeNow'].split('_');
            this.templateData['themeNow'] = themeTemp[0] + '_' + themeTemp[1];
            this.themeNow = themeTemp[0] + '_' + themeTemp[1];
            this.onThemeChange(themeTemp[0] + '_' + themeTemp[1]);
            if (themeTemp[2] != undefined) {
              this.templateData['gradient'] = true;
            }
            else {
              this.templateData['gradient'] = false;
            }
            dashboardRequest['tabId'] = resp['response'].dashboardTabs[0]['tabId'];
            dashboardRequest['templateId'] = resp['response'].dashboardTabs[0]['templateId'];
            this.dashboardFilterFlag = resp['response']['filterFlag'] == 'Y' ? true : false;
            if (this.dashboardFilterFlag) {
              let sendFilterData={
                filterFlag:  resp['response'].filterFlag,
                filterRefCode:  resp['response'].filterRefCode,
                dashboardId:  resp['response'].dashboardId,
                dashboardName: resp['response'].dashboardName
              }
              this.apiService.post(environment.getReportFilterList, sendFilterData).subscribe((filterResp) => {
                if (filterResp['status'] == 1) {
                  filterResp['response'].forEach(element => {
                    if (element.filterType == 'DATE') {
                      let dateRestrict = element.filterDateRestrict.split(',');
                      let year = dateRestrict[0].split(':');
                      let month = dateRestrict[1].split(':');
                      let date = dateRestrict[2].split(':');
                      let pastYear = year[0].slice(0, -1);
                      let futureYear = year[1].slice(0, -1);
                      let pastMonth = month[0].slice(0, -1);
                      let futureMonth = month[1].slice(0, -1);
                      let pastDate = date[0].slice(0, -1);
                      let futureDate = date[1].slice(0, -1);
                      let businessdates = JSON.parse(this.jwtService.get('businessDate'));
                      let countrySplit = date[2] ? date[2].split("_") : [];
                      if (countrySplit.length && countrySplit[1]) {
                        businessdates.forEach(dateObj => {
                          if (dateObj.COUNTRY == countrySplit[1]) {
                            let isVBD = countrySplit.includes("VBD") ? true : false;
                            let isVBM = countrySplit.includes("VBM") ? true : false;
                            let isVBW = countrySplit.includes("VBW") ? true : false;
                            let isVBQ = countrySplit.includes("VBQ") ? true : false;
                            let today = new Date();
                            let todayYear = today.getFullYear();
                            let todayMonth = today.getMonth();
                            let todayDay = today.getDate();
                            if (isVBD || isVBW || isVBM || isVBQ) {
                              let maxDate: string = isVBD ? dateObj['VBD'].toString().replaceAll("-", " ") :
                                isVBW ? dateObj['VBW'].toString().replaceAll("-", " ") :
                                  isVBM ? dateObj['VBM'].toString().replaceAll("-", " ") :
                                    isVBQ ? dateObj['VBQ'].toString().replaceAll("-", " ") : '';
                              isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                              let vbdDate = new Date(maxDate);
                              let vbdYear = vbdDate.getFullYear();
                              let vbdMonth = vbdDate.getMonth();
                              let vbdDay = vbdDate.getDate();
                              element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                                new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                              element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                                new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                            }
                            else {
                              element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                              element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                            }
                          }
                        });
                      }
                      else {
                        let isVBD = date.includes("VBD") ? true : false;
                        let isVBM = date.includes("VBM") ? true : false;
                        let isVBW = date.includes("VBW") ? true : false;
                        let isVBQ = date.includes("VBQ") ? true : false;
                        let today = new Date();
                        let todayYear = today.getFullYear();
                        let todayMonth = today.getMonth();
                        let todayDay = today.getDate();
                        if (isVBD || isVBW || isVBM || isVBQ) {
                          // let dates: any = Object.values(businessdates);
                          // let maxDate = Math.max(...dates.map(e => new Date(e.toString().replaceAll("-", " "))));
                          let maxDate: string = isVBD ? businessdates[0]['VBD'].toString().replaceAll("-", " ") :
                            isVBW ? businessdates[0]['VBW'].toString().replaceAll("-", " ") :
                              isVBM ? businessdates[0]['VBM'].toString().replaceAll("-", " ") :
                                isVBQ ? businessdates[0]['VBQ'].toString().replaceAll("-", " ") : '';
                          isVBM || isVBQ ? maxDate = `01 ${maxDate}` : '';
                          let vbdDate = new Date(maxDate);
                          let vbdYear = vbdDate.getFullYear();
                          let vbdMonth = vbdDate.getMonth();
                          let vbdDay = vbdDate.getDate();
                          element.pastDate = (date[0] == "VBD" || date[0] == "VBW" || month[0] == "VBM" || month[0] == "VBQ") ?
                            new Date(maxDate) : new Date(vbdYear - pastYear, vbdMonth - pastMonth, vbdDay - pastDate);
                          element.futureDate = (date[1] == "VBD" || date[1] == "VBW" || month[1] == "VBM" || month[1] == "VBQ") ?
                            new Date(maxDate) : new Date(vbdYear + parseInt(futureYear), vbdMonth + parseInt(futureMonth), vbdDay + parseInt(futureDate));
                        }
                        else {
                          element.pastDate = new Date(todayYear - pastYear, todayMonth - pastMonth, todayDay - pastDate);
                          element.futureDate = new Date(todayYear + parseInt(futureYear), todayMonth + parseInt(futureMonth), todayDay + parseInt(futureDate));
                        }
                      }

                    }
                  });
                  this.dashboardFilterList = filterResp['response'];
                  this.filterVal = this.groupBy(filterResp['response'], 'filterRow');
                  this.tabBasedFilter();
                  this.filterPrompt();
                  this.dashboardTabRequest(dashboardRequest, 1, false);
                }
                else {
                  this.dashboardFilterList = [];
                }
              })
            }
          }
          else if (isNull(resp['response'].dashboardTabs) || !resp['response'].dashboardTabs.length) {
            document.getElementById('dashboard-hold').style.border = 'none';
          }
        } else {
          this.globalService.showToastr.error(resp['message']);
        }
      })
    }
  }

  onThemeChange(templateData) {
    this.globalService.themeChanges(templateData);
  }

  dashboardTabRequest(dashboardRequest, type, filterType) {
    this.tabId = dashboardRequest.tabId;
    if (!filterType) {
      if (!isNull(this.dashboardDetails) && this.dashboardDetails.length) {
        this.templateData['dashboardTabData'] = this.dashboardDetails[this.tabIndex].tileDetails;
        this.templateData['tabId'] = this.dashboardDetails[this.tabIndex]['tabId'];
        this.templateData['templateId'] = this.dashboardDetails[this.tabIndex]['templateId'];
        this.templateData['promptValues'] =this.getPromptValues();
        this.templateData['promptDescriptions'] =this.getpromptDescriptions();
        this.hidShow = false;
        if (type) {
          setTimeout(() => {
            document.querySelectorAll('.tabss')[this.currentTabIndex].classList.add('activeTab');
            if (document.querySelectorAll('.tabss')[this.currentTabIndex]['innerText'] == 'Customers') {
              this.customerId = true;
            }
          }, 10);
        }
        this.hidShow = true;
        this.templateData = JSON.parse(JSON.stringify(this.templateData));
        document.getElementById('loader_1').style.display = 'none';;
      }
    }
    else {
      this.templateData['promptValues'] = this.promptValues;
      this.templateData['themeNow'] = this.themeNow;
      this.templateData['promptDescriptions'] =this.getpromptDescriptions();
      this.templateData = JSON.parse(JSON.stringify(this.templateData));
      this.hidShow = true;
    }
  }

  arraySrtingCon(data){
    let arr=[];
    data.forEach(element => {
      arr.push(`'${element["id"]}'`)
    });
    return arr.toString();
  }

  getPromptValues(){
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    let dashboardFilterRequest = {};
    values.forEach(element => {
      this.dashboardFilterList.forEach(element1 => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO' && element1.filterToshow) {
            if(element1["multiSelect"] == 'Y'){
              this.promptValues[`promptValue${element1.filterSeq}`] =this.arraySrtingCon(rawVal[element]);
            }
            else{
              if (rawVal[element][0]) {
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
              }
            }
          }
          if (element1['filterType'] == 'DATE' && element1.filterToshow) {
            let val = this.dateReConversion(rawVal[element],element1.filterDateFormat);
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
          }
          if (element1['filterType'] == 'TEXTD' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType']) && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TEXTM' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
            // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TREE' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
            // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TEXT' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
        }
      });
     });
    return this.promptValues;
  }
  // Apply sidebar filter
  applyFilter = () => {
    this.hidShow = false;
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    let dashboardFilterRequest = {};
    values.forEach(element => {
      this.dashboardFilterList.forEach(element1 => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO' && element1.filterToshow) {
            if(element1["multiSelect"] == 'Y'){
              this.promptValues[`promptValue${element1.filterSeq}`] =this.arraySrtingCon(rawVal[element]);
            }
            else{
              if (rawVal[element][0]) {
                this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element][0]['id']}'`;
              }
            }
          }
          if (element1['filterType'] == 'DATE' && element1.filterToshow) {
            let val = this.dateReConversion(rawVal[element],element1.filterDateFormat);
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${val}'`;
          }
          if (element1['filterType'] == 'TEXTD' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType']) && element1.filterToshow ) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TEXTM' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.magnifierId[`filter${element1.filterSeq}Val`]}'`;
            // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TREE' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${this.treeId[`filter${element1.filterSeq}Val`]}'`;
            // this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
          if (element1['filterType'] == 'TEXT' && element1.filterToshow) {
            this.promptValues[`promptValue${element1.filterSeq}`] = `'${rawVal[element]}'`;
          }
        }
      });
     });
    const dashboardRequest = {
      'dashboardId': this.dashboardId,
      'tabId': this.dashboardDetails[this.currentTabIndex]['tabId'],
      'templateId': this.dashboardDetails[this.currentTabIndex]['templateId'],
      ...dashboardFilterRequest
    };
    this.dashboardTabRequest(dashboardRequest, this.tabId, true);
    this.toggleSideFilter();
  }

  cancelFilter = (event) => {
    this.toggleSideFilter();
  }

  // Toggle sidefilter
  toggleSideFilter = () => {
    const sideFilter = document.getElementById('side-filter-hold');
    const sideArrowClass = document.getElementById('sidebarArrow');
    const sideFilterOverlay = document.getElementById('sideFilterOverlay');
    if (sideFilter.classList.contains('setRight')) {
      sideFilterOverlay.classList.remove('show');
      sideFilter.classList.remove('setRight');
      sideArrowClass.classList.remove('fa-angle-double-right');
    } else {
      sideFilterOverlay.classList.add('show');
      sideFilter.classList.add('setRight');
      sideArrowClass.classList.add('fa-angle-double-right');
    }
  }

  // Set sidefilter height
  sideFilterHeight() {
    const dashboardHoldHeight = window.innerHeight;
    const filterHeaderHeight = +document.getElementById('filterHeader').offsetHeight;
    const filterFooterHeight = +document.getElementById('filterFooter').offsetHeight;
    document.getElementById('side-filter-hold').style.height = `${dashboardHoldHeight - 80}px`;
    const setFilterBodyHeight = dashboardHoldHeight - (filterHeaderHeight + filterFooterHeight);
    document.getElementById('filterBody').style.cssText = `height: ${setFilterBodyHeight}px;
                                                          max-height: ${setFilterBodyHeight}px;
                                                          margin-bottom: ${filterFooterHeight}px`;
  }

  // Get sidefilter position from sidebar width using css variable and update sidebar right position
  // Update sidefilter css right position value from css variable
  sideFilterPosition() {
    const sideFilterHold = document.getElementById('side-filter-hold');
    const filterHoldWidth = document.getElementById('side-filter-hold').offsetWidth;
    sideFilterHold.style.setProperty('--filterWidth', `-${filterHoldWidth}px`);
  }

  // Filter by year || month || date in sidefilter
  fitterBy = (obj) => {
    const filterDataBy = document.getElementsByClassName('filterDataBy');
    Array.from(filterDataBy).forEach((elem) => {
      elem.classList.remove('show');
    });
    if (obj === 'Y') {
      document.getElementById('yearWise').classList.add('show');
    } else if (obj === 'M') {
      document.getElementById('monthWise').classList.add('show');
    } else {
      document.getElementById('dateWise').classList.add('show');
    }
  }

  switchTabs(event, tab, ind) {
    this.tabIndex = ind;
    const tabeName = tab.tabName;
    this.tabBasedFilter()
    this.currentTabIndex = ind;
    const tabs = document.querySelectorAll('.tabss');
    tabs.forEach((tabsElm) => {
      tabsElm.classList.remove('activeTab');
    });
    event.target.closest('.card-body').classList.add('activeTab');
    const dashboardRequest = {
      'dashboardId': this.dashboardId,
      'tabId': tab.tabId,
      'templateId': tab.templateId
    };
    this.hidShow = false;
    this.dashboardTabRequest(dashboardRequest, 0, false);
  }

  months = { '01': 'Jan', '02': 'Feb', '03': 'Mar', '04': 'Apr', '05': 'May', '06': 'Jun', '07': 'Jul', '08': 'Aug', '09': 'Sep', '10': 'Oct', '11': 'Nov', '12': 'Dec' };
  monthWithNum = { '01': '1', '02': '2', '03': '3', '04': '4', '05': '5', '06': '6', '07': '7', '08': '8', '09': '9', '10': '10', '11': '11', '12': '12' }
  numWithMonth = { 'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12 }


  dateConversion(element) {
    let ele = element.split('-');
    return { "year": +ele[2], "month": +this.monthWithNum[ele[1]], "day": +ele[0] };
  }
  dateReConversion(date,format) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",][myDate.getMonth()];
      if (format == 'YYYY') {
        str = myDate.getFullYear();
      }
      else if (format == 'Mon-YYYY') {
        str = month + "-" + myDate.getFullYear();
      }
      else {
        str = myDate.getDate() + "-" + month + "-" + myDate.getFullYear();
      }

      return str;
    }
    else{
      return "";
    }
  }

  // ------------------------------------------------------------

  openCustomerMagnifierPopup(ind, event, title1) {
    event.srcElement.blur();
    event.preventDefault();
    let query;
    let tilte;
    Object.keys(this.filterVal).forEach(element => {
      this.filterVal[element].forEach(element1 => {
        if (element1.filterSeq == ind) {
          query = element1['filterSourceId'];
          tilte = element1['filterLabel'];
        }
      });
    });

    let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.dashboardFilterList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              promptM[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              promptM[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            promptM[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    promptM['query'] = query;
    const modelRef = this.modalService.open(MagnifierComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    modelRef.componentInstance.query = promptM;
    modelRef.componentInstance.title = title1;
    modelRef.componentInstance.filterData.subscribe((e) => {
      //let filteSeq = this.tablist[this.selectedReportId]["tabFilterValue"][ind]['filterSeq'];
      let str = `filter${ind}Val`;
      this.filterForm.patchValue({
        [str]: e.columnTen
      })
      this.magnifierId[str] = e.columnNine;
      //this.tablist[this.selectedReportId]["tabFilterSelectedValue"][ind] = e.columnNine;
      modelRef.close();
    });
  }

  tabBasedFilter() {
    const ind = this.tabIndex + 1;
    for (let i = 0; i <= this.dashboardFilterList.length - 1; i++) {
      let item = this.dashboardFilterList[i];
      if (isNullOrUndefined(item['specificTab']) || (item['specificTab'] == '') || item['specificTab'] == 'ALL') {
        item['filterToshow'] = true;
      } else {
        let arr = item['specificTab'].split(',');
        if (arr.includes(`${ind}`)) {
          item['filterToshow'] = true;
        } else {
          item['filterToshow'] = false;
        }
      }
    }
  }

  getFilterLabel(seq) {
    let fill = this.dashboardFilterList.filter((item) => item.filterSeq == seq)
    return fill[0].filterLabel
  }
  promptValuesReturn(index) {
    return `filter${index}Val`;
  }

  compareTwoDates(startIndex, endIndex) {
    if (new Date(this.filterForm.controls[`filter${endIndex}Val`].value) < new Date(this.filterForm.controls[`filter${startIndex}Val`].value)) {
      this.filterForm.controls[`filter${endIndex}Val`].setErrors({ 'endDate': true });
      return false
    }
  }

  filterSubmit() {
    this.dashboardFilterList.forEach(element => {
      if(!element.filterToshow){
        this.promptArray()[`filter${element.filterSeq}Val`].setErrors(null)
      }
    });
    if (this.filterForm.invalid) {
      this.filterSubmitted = true
    }
    else {
      let tabFilterValue = this.dashboardFilterList;
      let dateArr = tabFilterValue.filter((item) => item.filterType == 'DATE');
      if (dateArr.length) {
        dateArr.forEach(element => {
          if (element.dependencyFlag == 'Y') {
            this.compareTwoDates(element.dependentPrompt, element.filterSeq);
          }
        });
        if (this.filterForm.valid) {
          this.applyFilter();
        }
      }
      else {
        this.applyFilter();
      }
    }
  }
  promptValueOption(seq) {
    return this.overallPromptValues[seq]
  }
  filterPrompt() {
    let pushData = {}
    this.dashboardFilterList.forEach((element, objIndex) => {
      let tempArr = [];
      const isObjEmpty = Object.entries(element.filterSourceVal).length === 0 && element.filterSourceVal.constructor === Object;
      if (isObjEmpty) {
        this.overallPromptValues[element.filterSeq] = tempArr;
      } else {
        const data = element.filterSourceVal;
        Object.keys(data).forEach(ele => {
          tempArr.push({ id: ele, description: data[ele] });
        })
        this.overallPromptValues[element.filterSeq] = tempArr;
      }

      if (element.filterType == 'COMBO') {
        if (Object.keys(element['filterDefaultValue']).length) {
          const dataDefault = element['filterDefaultValue'];
          let tempDefault = [];
          Object.keys(dataDefault).forEach(ele => {
            tempDefault.push({ id: ele, description: dataDefault[ele] });
          })
          pushData[`filter${element.filterSeq}Val`] = new FormControl(tempDefault, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl([], Validators.required);
        }
      }
      if (element.filterType == 'DATE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          // let date = element['filterDefaultValue'].split('-');
          let dateVal = Object.keys(element['filterDefaultValue'])[0].split("-").join("/");
          if (element.filterDateFormat == "Mon-YYYY") {
            dateVal = `01 ${dateVal}`
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          //pushData[`filter${element.filterSeq}Val`] = new FormControl({ year: +date[2], month: this.numWithMonth[date[1]], day: +date[0] }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }

      if (element.filterType == 'DATERANGE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          // let date = element['filterDefaultValue'].split('-');
          let dateVal = Object.keys(element['filterDefaultValue'])[0].split("-").join("/");
          if (element.filterDateFormat == "Mon-YYYY") {
            dateVal = `01 ${dateVal}`
          }
          pushData[`filter${element.filterSeq}Val`] = new FormControl(new Date(dateVal), Validators.required);
          // pushData[`filter${element.filterSeq}Val`] = new FormControl({ year: +date[2], month: this.numWithMonth[date[1]], day: +date[0] }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl('', Validators.required);
        }
      }
      if (!['COMBO', 'DATE'].includes(element.filterType)) {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.keys(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTM') {
        if (Object.keys(element['filterDefaultValue']).length) {
          this.magnifierId[`filter${element.filterSeq}Val`] = Object.keys(element['filterDefaultValue']).join(",");
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.values(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          this.magnifierId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TREE') {
        if (Object.keys(element['filterDefaultValue']).length) {
          this.treeId[`filter${element.filterSeq}Val`] = Object.keys(element['filterDefaultValue']).join(",");
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.values(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          this.treeId[`filter${element.filterSeq}Val`] = "";
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXT') {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl(Object.keys(element['filterDefaultValue']).join(","), Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl("", Validators.required);
        }
      }
      if (element.filterType == 'TEXTD') {
        if (Object.keys(element['filterDefaultValue']).length) {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: Object.keys(element['filterDefaultValue']).join(","), disabled: true }, Validators.required);
        }
        else {
          pushData[`filter${element.filterSeq}Val`] = new FormControl({ value: "", disabled: true }, Validators.required);
        }
      }
    });
    this.filterForm = new FormGroup(pushData);
  }

  dropdownCheck(data, event, type?) {
    if (data.dependencyFlag == 'Y') {
      let magnifier = 0;
      let dropdownFlag = false;
      let dataValue;
      let dependentPrompt = data.dependentPrompt.split(",")
      dependentPrompt.forEach((element1, index1) => {
        this.dashboardFilterList.forEach((element, index) => {
          if (element.filterSeq == element1) {
            dataValue = this.filterForm.getRawValue()[`filter${element.filterSeq}Val`];
            if (element.filterType == 'DATE') {
              let val = this.dateReConversion(dataValue, element.filterDateFormat);
              dataValue = val;
              data[`filter${element1}Val`] = `'${val}'`;
            }
            if (dataValue.length) {
              if (Array.isArray(dataValue)) {
                if (!isNullOrUndefined(dataValue[0].id) && dataValue[0].id != '' && dataValue[0].id != 'ALL') {
                  data[`filter${element1}Val`] = this.arraySrtingCon(dataValue);
                  if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
                    dropdownFlag = true;
                  }
                }
                else {
                  magnifier++
                  this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
                }
              }
            }
            else {
              magnifier++
              Object.keys(this.filterVal).forEach(element => {
                this.filterVal[element].forEach(element1 => {
                  if (element1.filterSeq == data.filterSeq) {
                    this.overallPromptValues[data.filterSeq] = [];
                  }
                });
              });
              this.filterForm.controls[`filter${index + 1}Val`].setErrors({ 'incorrect': true });
            }
          }
        });
      });
      if (type == 'TEXTM' && magnifier == 0) {
        this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
      }
      if (type == 'TREE' && magnifier == 0) {
        this.openFilterTreePopup(data, event);
      }
      if (dropdownFlag && dataValue.length) {
        data['defaultValueId'] = '';
        this.apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
          let promptArr = []
          Object.keys(resp['response']).forEach((element, index1) => {
            promptArr.push(
              {
                id:element.split('@')[1],
                description: resp['response'][element]
              }
            )
          });
          Object.keys(this.filterVal).forEach(element => {
            this.filterVal[element].forEach(element1 => {
              if (element1.filterSeq == data.filterSeq) {
                this.overallPromptValues[data.filterSeq] = promptArr;
              }
            });
          });
        });
      }
    }
    else {
      if (data.filterType == "COMBO" || data.filterType == "TEXTD") {
        if (event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn') {
          data['defaultValueId'] = '';
          this.apiService.post(environment.getReportChildFilter, data).subscribe((resp) => {
            let promptArr = []
            Object.keys(resp['response']).forEach((element, index1) => {
              promptArr.push(
                {
                  id: element.split('@')[1],
                  description: resp['response'][element]
                }
              )
            });
            Object.keys(this.filterVal).forEach(element => {
              this.filterVal[element].forEach(element1 => {
                if (element1.filterSeq == data.filterSeq) {
                  this.overallPromptValues[data.filterSeq] = promptArr;
                }
              });
            });
          });
        }
      }
      else {
        if (data.filterType == "TEXTM") {
          this.openCustomerMagnifierPopup(data.filterSeq, event, data.filterLabel);
        }
        if (data.filterType == 'TREE') {
          this.openFilterTreePopup(data, event);
        }
      }

    }
  }

  openFilterTreePopup(data, event) {
    event.srcElement.blur();
    event.preventDefault();
    // let query;
    // let tilte;
    // Object.keys(this.filterVal).forEach(element => {
    //   this.filterVal[element].forEach(element1 => {
    //     if (element1.filterSeq == data.filterSeq) {
    //       query = element1['filterSourceId'];
    //       tilte = element1['filterLabel'];
    //     }
    //   });
    // });

    //let promptM: any = {};
    const rawVal = this.filterForm.getRawValue();
    const values = Object.keys(rawVal);
    this.dashboardFilterList.forEach(element1 => {
      values.forEach((element, promptIndex) => {
        if (element == `filter${element1.filterSeq}Val`) {
          if (element1['filterType'] == 'COMBO') {
            if (element1["multiSelect"] == 'Y') {
              data[`promptValue${element1.filterSeq}`] = this.arraySrtingCon(rawVal[element]);
            }
            else {
              data[`promptValue${element1.filterSeq}`] = (rawVal[element][0] && !isNullOrUndefined(rawVal[element][0]['id'])) ? `'${rawVal[element][0]['id']}'` : `'ALL'`;
            }
          }
          if (element1['filterType'] == 'DATE') {
            let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(val) ? `'${val}'` : '';
          }
          if (!['COMBO', 'DATE'].includes(element1['filterType'])) {
            data[`promptValue${element1.filterSeq}`] = !isNullOrUndefined(rawVal[element]) ? `'${rawVal[element]}'` : `'ALL'`;
          }
        }
      });
    });
    const modelRef = this.modalService.open(GenericTreeComponent, {
      size: 'lg',
      backdrop: 'static'
    });
    document.body.classList.add('tree-popup');
    modelRef.componentInstance.title = data.filterLabel;
    modelRef.componentInstance.query = data;
    modelRef.componentInstance.selectedValue = this.treeId[`filter${data.filterSeq}Val`]
    modelRef.componentInstance.filterData.subscribe((e) => {
      let str = `filter${data.filterSeq}Val`;
      this.filterForm.patchValue({
        [str]: e.field2
      })
      this.treeId[str] = e.field1;
      document.body.classList.remove('tree-popup');
      modelRef.close();
    });
  }

  setEmptyDepent(data) {
    let filterPrompt =this.dashboardFilterList;
    filterPrompt.forEach(element => {
      if (element.dependencyFlag == 'Y') {
        let dependentPrompt = element.dependentPrompt.split(",");
        let filterSeq = data.filterSeq.toString();
        if (dependentPrompt.includes(filterSeq)) {
          let str = `filter${element.filterSeq}Val`;
          if (element.filterType == 'COMBO') {
            this.filterForm.patchValue({
              [str]: []
            })
          }
          else {
            this.filterForm.patchValue({
              [str]: ''
            })
          }

        }
      }
    });
  }
  getObjectValue(data) {
    return Object.values(data);
  }
  checkWidth(data) {
    let reqData = [];
    data.forEach(element => {
      if(element.filterToshow){
        reqData.push(element)
      }
    });
    return {
      width: (100 / reqData.length) + '%'
    };
  }

  getpromptDescriptions() {
    if (this.dashboardFilterList && this.dashboardFilterList.length) {
      const rawVal = this.filterForm.getRawValue();
      const values = Object.keys(rawVal);
      let promptArray = [];
      this.dashboardFilterList.forEach(element1 => {
        values.forEach((element, promptIndex) => {
          if (element == `filter${element1.filterSeq}Val`) {
            if (element1['filterType'] == 'COMBO' && element1.filterToshow) {
              if (element1["multiSelect"] == 'Y') {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = this.arraySrtingConDes(rawVal[element]);
              }
              else {
                if (rawVal[element][0]) {
                  this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element][0]['description']}`;
                }
              }
              promptArray.push(element1);
            }
            if (element1['filterType'] == 'DATE' && element1.filterToshow) {
              if (element != '') {
                let val = this.dateReConversion(rawVal[element], element1.filterDateFormat);
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${val}`;
              }
              else {
                this.promptDescriptions[`promptDescription${element1.filterSeq}`] = ``;
              }
              promptArray.push(element1);
            }
            if (element1['filterType'] == 'TEXTD' && element1.filterToshow) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              promptArray.push(element1);
            }
            if (element1 == 'TEXT' && element1.filterToshow) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              promptArray.push(element1);
            }
            if (!['COMBO', 'DATE'].includes(element1['filterType']) && element1.filterToshow) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              promptArray.push(element1);
            }
            if (element1['filterType'] == 'TEXTM' && element1.filterToshow) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              promptArray.push(element1);
            }
            if (element1['filterType'] == 'TREE' && element1.filterToshow) {
              this.promptDescriptions[`promptDescription${element1.filterSeq}`] = `${rawVal[element]}`;
              promptArray.push(element1);
            }
          }
        });
      });
      return { promptDescriptions: this.promptDescriptions, promptArr: promptArray}
    }
  }

  arraySrtingConDes(data) {
    let arr = [];
    data.forEach(element => {
      arr.push(`${element["description"]}`)
    });
    return arr.toString();
  }
}
