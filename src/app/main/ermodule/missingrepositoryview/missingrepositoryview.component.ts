import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild
} from "@angular/core";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";


@Component({
  selector: 'app-missingrepositoryview',
  templateUrl: './missingrepositoryview.component.html',
  styleUrls: ['./missingrepositoryview.component.css']
})


export class MissingrepositoryviewComponent implements OnInit, OnChanges {
  @ViewChild("viewcontainer") viewcontainer: ElementRef;
  leftContainerHeight: string;
  table_headervalues: any = [];
  tablelist_columns: any = [];
  checkedTableList: any = [];
  @Input() nodetreevalues: any;
  @Output() backToSummaryClick = new EventEmitter();
  pageShow: boolean = false;
  pageview: any;
  relationByIndexList: any = [];
  relationByNameList: any = [];
  isOpen_relation = false;
  isOpen_relation_sub = false;
  checkedCategoryList: any = [];
  isMasterSel: boolean;
  isMasterSel_byname: boolean;
  checkedCategoryListByName: any = [];
  results: any = [];
  constructor(
    private changeDetector: ChangeDetectorRef,
    private _apiService: ApiService,
    public globalService: GlobalService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.tabletreevalues();
  }

  tabletreevalues() {
    this.table_headervalues = [];
    this.relationByIndexList = [];
    this.relationByNameList = [];

    this.table_headervalues = this.nodetreevalues.otherInfo;
    this.relationByIndexList = this.nodetreevalues.relationByIndexList;
    this.relationByNameList = this.nodetreevalues.relationByNameList;
    this.isOpen_relation_sub = false;
    this.isOpen_relation = false;
    // if(this.relationByIndexList == "" && this.relationByNameList ==""){
    //   this.isOpen_relation_sub = false;
    //   this.isOpen_relation = false;
    // }
  }

  toggleShowDiv(value: string) {
    if (value == "relation") {
      this.isOpen_relation = !this.isOpen_relation;
      this.isOpen_relation_sub = false;
    }
    if (value == "relation_sub") {
      this.isOpen_relation_sub = !this.isOpen_relation_sub;
      this.isOpen_relation = false;
    }
  }


  checkUncheckAll() {
    for (var i = 0; i < this.relationByIndexList.length; i++) {
      this.relationByIndexList[i].isSelected = this.isMasterSel;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.isMasterSel = this.relationByIndexList.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedCategoryList = [];
    for (var i = 0; i < this.relationByIndexList.length; i++) {
      if (this.relationByIndexList[i].isSelected)
        this.checkedCategoryList.push(this.relationByIndexList[i]);
    }
  }

 

  checkUncheckAllByName() {
    for (var i = 0; i < this.relationByNameList.length; i++) {
      this.relationByNameList[i].isSelected = this.isMasterSel_byname;
    }
    this.getCheckedItemListByName();
  }
 

  isAllSelectedByName() {
    this.isMasterSel_byname = this.relationByNameList.every(function (
      item: any
    ) {
      return item.isSelected == true;
    });
    this.getCheckedItemListByName();
  }

  getCheckedItemListByName() {
    this.checkedCategoryListByName = [];
    for (var i = 0; i < this.relationByNameList.length; i++) {
      if (this.relationByNameList[i].isSelected)
        this.checkedCategoryListByName.push(this.relationByNameList[i]);
    }
  }

  addMissingRelationValues() {
    
    this.results =[];
    this.results.push(
      ...this.checkedCategoryList,
      ...this.checkedCategoryListByName
    );

    const valueContains = this.results.filter(value => value.isSelected === true).length;
    if (valueContains < 1) {
      this.globalService.showToastr.warning("Please Select the Tables");
    }
    else{
      const api_Modify = environment.edwObjinsertRelationsData;
      this._apiService.post(api_Modify, this.results).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.relationByIndexList = this.relationByIndexList.filter((el) => !this.checkedCategoryList.includes(el));
          this.relationByNameList = this.relationByNameList.filter((el) => !this.checkedCategoryListByName.includes(el));
          this.globalService.showToastr.success(resp["message"]);
          this.cancelBtnAll();
        } else {
          this.globalService.showToastr.error(resp["message"]);
          this.cancelBtnAll();
        }
      });
    }

  
  }

  cancelBtnAll() {
    this.isMasterSel= false;
    this.isMasterSel_byname = false;
    this.checkedCategoryList =[];
    this.checkedCategoryListByName =[];
    this.relationByNameList.forEach(element => {
      if (element) {
        element["is_open"] = false;
        element["isSelected"] = false;
      }

    });
    this.checkedCategoryListByName.forEach(element1 => {
      if (element1) {
        element1["is_open"] = false;
        element1["isSelected"] = false;
      }

    });
    this.relationByIndexList.forEach(element => {
      if (element) {
        element["is_open"] = false;
        element["isSelected"] = false;
      }

    });
    this.checkedCategoryList.forEach(element1 => {
      if (element1) {
        element1["is_open"] = false;
        element1["isSelected"] = false;
      }

    });
    this.isOpen_relation_sub = false;
    this.isOpen_relation = false;
  }


  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.changeDetector.detectChanges();
    });
  }
}

