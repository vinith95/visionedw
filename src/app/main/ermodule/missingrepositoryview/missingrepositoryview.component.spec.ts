import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingrepositoryviewComponent } from './missingrepositoryview.component';

describe('MissingrepositoryviewComponent', () => {
  let component: MissingrepositoryviewComponent;
  let fixture: ComponentFixture<MissingrepositoryviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingrepositoryviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingrepositoryviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
