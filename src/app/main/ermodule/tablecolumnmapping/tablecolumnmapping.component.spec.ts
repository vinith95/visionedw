import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablecolumnmappingComponent } from './tablecolumnmapping.component';

describe('TablecolumnmappingComponent', () => {
  let component: TablecolumnmappingComponent;
  let fixture: ComponentFixture<TablecolumnmappingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablecolumnmappingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablecolumnmappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
