import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService, GlobalService } from 'src/app/service';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
declare var jsPlumb: any;
declare let $: any;
@Component({
  selector: 'app-tablecolumnmapping',
  templateUrl: './tablecolumnmapping.component.html',
  styleUrls: ['./tablecolumnmapping.component.css']
})


export class TablecolumnmappingComponent implements OnInit {

  tableheader_columnone = [];
  tableheader_columntwo = [];

  tablecolumnvaluesarrayone = [];
  tablecolumnvaluesarraytwo = [];

  tablecolumnvalues: any = [];
  tablecolumnvaluesobj: any = [];
  existingcolumnvalues: any = [];
  checkedtablecolumnsvalues: any = [];
  checkedtablecolumnsvaluesobj: any = [];
  isMasterSel: boolean;
  checkedCategoryList: any;

  @Input() objectsids;
  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();
  @Output() closemodal: EventEmitter<any> = new EventEmitter<any>();

  constructor(private apiService: ApiService, private globalService: GlobalService) { }

  ngOnInit(): void {

    this.objectsids;
    this.tableheader_columnone = this.objectsids[0].objectName;
    this.tableheader_columntwo = this.objectsids[1].objectName;
    this.tablecolumnvaluesarrayone = this.objectsids[0].tablevalues;
    this.tablecolumnvaluesarraytwo = this.objectsids[1].tablevalues;
    this.jqPlumbStart();
  }





  jqPlumbStart() {
    setTimeout(() => {

      const instance = jsPlumb.getInstance({
        Connector: "Straight",
        PaintStyle: {
          strokeWidth: 2,
          stroke: '#000000',
          fill: 'none'
        },
        Endpoint: ['Rectangle', {
          width: 5,
          height: 5,
        }],
        EndpointStyle: {
          fill: '#ffa500'
        },
        Container: 'canvas',
        ConnectionOverlays: [
          ['Arrow', {
            location: 1,
            width: 11,
            length: 11,
          }],
        ],
      });
      (<any>window).jsp = instance;
      (<any>window).jsp.setContainer('canvas');
      console.log((<any>window).jsp);



      this.makeSourceTarget();
      setTimeout(() => {
        this.connectionNode('jsPlumb_5_1', 'jsPlumb_5_2', 'window_4');
      }, 10);
    }, 500);
  }

  makeSourceTarget() {

    let moduleId = 'tablecolumn';
    const lists = jsPlumb.getSelector('#' + moduleId + ' .testing');

    if (lists.length) {
      for (let l = 0; l < lists.length; l++) {
        const isSource = lists[l].getAttribute('source') != null;
        if (isSource) {
          (<any>window).jsp.makeTarget(lists[l], {
            allowLoopback: true,
            maxConnections: 1,
            anchor: ['Left', 'Right'],
          });
        }
      }
    }
    const outList = jsPlumb.getSelector('#' + moduleId + ' .testing');
    if (outList.length) {
      for (let l = 0; l < outList.length; l++) {
        const isTarget = outList[l].getAttribute('target') != null;
        if (isTarget) {
          (<any>window).jsp.makeSource(outList[l], {
            anchor: ['Left', 'Right'],
          });
        }
      }
    }
  }

  connectionNode(source, target, label) {

    let src = document.getElementById($('[data-id=' + "jsPlumb_5_1" + ']').attr('id'));
    let tgt = document.getElementById($('[data-id=' + "jsPlumb_5_2" + ']').attr('id'));

    if (!isNullOrUndefined(src) && !isNullOrUndefined(tgt) && !isNullOrUndefined(label))
      this.jsPlumbConnectionFn(src, tgt, label);

  }

  jsPlumbConnectionFn(source, target, label) {

    (<any>window).jsp.connect({
      source: source,
      target: target,
      scope: "someScope",
      detachable: false,
      reattach: false,
      hoverPaintStyle: { stroke: "#000" },
      overlays: [
        ["Label",
          { label: label, id: "label_" + source + target, cssClass: "edgeLabel" }

        ],
      ],

    });
  }

  saveSelectingColumns() {
    this.checkedtablecolumnsvalues = this.tablecolumnvalues.filter(x => x.isSelected === true
      || x.alreadyexitingtable === "Y");
    this.checkedtablecolumnsvaluesobj = this.tablecolumnvalues.filter(x => x.displayCoumn === "Y");

    if (this.checkedtablecolumnsvalues.length > 0) {
      const api_update = environment.updateDisplayColumn;
      this.apiService.post(api_update, this.checkedtablecolumnsvalues).subscribe((resp) => {
        if (resp['status'] == 1) {
          this.globalService.showToastr.success(resp['message']);
          this.closeModal();

        } else {

          this.globalService.showToastr.error(resp['message']);
          this.closeModal();
        }
      });
    }
    else {
      this.globalService.showToastr.success('Please Select Table Columns');
    }
  }

  closeModal() {
    let passingvalues
    if (this.checkedtablecolumnsvaluesobj.length > 0) {
      passingvalues = this.checkedtablecolumnsvaluesobj
    }
    let data = {
      flag: 'close',
      data: passingvalues
    }
    this.filterData.emit(data);
  }
}
