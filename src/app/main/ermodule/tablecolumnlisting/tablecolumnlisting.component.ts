import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApiService, GlobalService } from 'src/app/service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tablecolumnlisting',
  templateUrl: './tablecolumnlisting.component.html',
  styleUrls: ['./tablecolumnlisting.component.css']
})
export class TablecolumnlistingComponent implements OnInit {
  tablecolumnvalues: any = [];
  tablecolumnvaluesobj: any = [];
  existingcolumnvalues: any = [];
  checkedtablecolumnsvalues: any = [];
  checkedtablecolumnsvaluesobj: any = [];
  isMasterSel: boolean;
  checkedCategoryList: any;
  @Input() dataToDisplay;
  @Input() searchData;
  @Input() objectsids;
  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();
  @Output() closemodal: EventEmitter<any> = new EventEmitter<any>();
  constructor(private apiService: ApiService, private globalService: GlobalService) { }

  ngOnInit(): void {
    this.tablecolumnvalues = [];
    this.tablecolumnvaluesobj.push(this.dataToDisplay);
    this.tablecolumnvalues = this.tablecolumnvaluesobj[0];
    this.existingcolumnvalues = this.searchData;
    this.tablecolumnvalues.forEach(element => {
      if (element != null) {
        element.verifierName = this.objectsids.verifierName;
        element.makerName = this.objectsids.makerName
      }
    });

    for (let i = 0; i < this.tablecolumnvalues.length; i++) {
      for (let j = 0; j < this.existingcolumnvalues.length; j++) {
        if (this.tablecolumnvalues[i].columnName === this.existingcolumnvalues[j].columnName) {
          this.tablecolumnvalues[i].alreadyexitingtable = "Y";
          this.tablecolumnvalues[i].isSelected = true;
          this.tablecolumnvalues[i].displayCoumn = "Y";
          this.isAllSelected();
        }
      }
    }
  }

  checkUncheckAll() {
    for (var i = 0; i < this.tablecolumnvalues.length; i++) {
      this.tablecolumnvalues[i].isSelected = this.isMasterSel;
      this.tablecolumnvalues[i].displayCoumn = 'N';
    }
    this.getCheckedItemList();
  }
  isAllSelected() {

    this.tablecolumnvalues.filter(x => x.displayCoumn).map(x => {
      if (x.isSelected == false) {
        x.displayCoumn = "N";
      }
    })
    this.isMasterSel = this.tablecolumnvalues.every(function (item: any) {
      return item.isSelected == true;
    })
    this.getCheckedItemList();
  }
  getCheckedItemList() {
    this.checkedCategoryList = [];
    for (var i = 0; i < this.tablecolumnvalues.length; i++) {
      if (this.tablecolumnvalues[i].isSelected)
        this.tablecolumnvalues[i].displayCoumn = "Y";
      this.checkedCategoryList.push(this.tablecolumnvalues[i]);
    }
    this.checkedCategoryList = JSON.stringify(this.checkedCategoryList);
  }

  saveSelectingColumns() {
    this.checkedtablecolumnsvalues
     = this.tablecolumnvalues.filter(x => x.isSelected === true
      || x.alreadyexitingtable === "Y");
    this.checkedtablecolumnsvaluesobj = this.tablecolumnvalues.filter(x => x.displayCoumn === "Y");

    if (this.checkedtablecolumnsvalues.length > 0) {
      const api_update = environment.updateDisplayColumn;
      this.apiService.post(api_update, this.checkedtablecolumnsvalues).subscribe((resp) => {
        if (resp['status'] == 1) {
          this.globalService.showToastr.success(resp['message']);
          this.closeModal();

        } else {

          this.globalService.showToastr.error(resp['message']);
          this.closeModal();
        }
      });
    }
    else {
      this.globalService.showToastr.success('Please Select Table Columns');
    }


  }

  closeModal() {
    let passingvalues;

    if (this.checkedtablecolumnsvaluesobj.length > 0) {
      passingvalues = this.checkedtablecolumnsvaluesobj
    }
    let data = {
      flag: 'close',
      data: passingvalues
    }

    this.filterData.emit(data);
  }



}
