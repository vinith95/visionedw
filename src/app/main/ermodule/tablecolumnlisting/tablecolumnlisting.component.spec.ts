import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablecolumnlistingComponent } from './tablecolumnlisting.component';

describe('TablecolumnlistingComponent', () => {
  let component: TablecolumnlistingComponent;
  let fixture: ComponentFixture<TablecolumnlistingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablecolumnlistingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablecolumnlistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
