import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErmoduleComponent } from './ermodule.component';

describe('ErmoduleComponent', () => {
  let component: ErmoduleComponent;
  let fixture: ComponentFixture<ErmoduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErmoduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErmoduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
