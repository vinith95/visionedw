import { DOCUMENT } from "@angular/common";
import {
  Component, ElementRef, EventEmitter, HostListener, Inject, Input, OnInit, Output, ViewChild
} from "@angular/core";
import { GridsterConfig } from "angular-gridster2";
import { ApiService, GlobalService } from "src/app/service";
import { maximiseSvg } from "src/assets/svg/maximise";
import { minimiseSvg } from "src/assets/svg/minimize";
import { saveSvg } from "src/assets/svg/save";
import { summarySvg } from "src/assets/svg/Summary";
import { windowMaxSvg } from "src/assets/svg/window-max";
import { windowMinimiseSvg } from "src/assets/svg/window-min";
import { zoomDefault } from "src/assets/svg/zoom-default";
import { zoomInSvg } from "src/assets/svg/zoom-in";
import { zoomOutSvg } from "src/assets/svg/zoom-out";
import { isNullOrUndefined } from "util";
import { environment } from "../../../../environments/environment";

declare var jsPlumb: any;
declare let d3: any;

@Component({
  selector: "app-er-diagramoverall",
  templateUrl: "./er-diagramoverall.component.html",
  styleUrls: ["./er-diagramoverall.component.css"],
})
export class ErDiagramoverallComponent implements OnInit {
  @Input() erAllvalues;
  @Input() erDisplayType;
  @Input() treenode;
  @Output() childData = new EventEmitter();

  @ViewChild("downloadZipLink", { static: false })
  private downloadZipLink: ElementRef;
  options: GridsterConfig;
  dashboard: any = [];
  curentPosition: any = { x: 0, y: 0 };
  maximizeData: any;
  hidetree: any;
  fullScreenDisp: boolean = false;
  elem: HTMLElement;
  //d3 Function //

  d3Links = [];
  d3ToolTipDiv: any;
  d3ToolTipDiv2: any;
  d3Data: any;
  d3Width: any;
  d3Height: any;
  d3ForceLayout: any;
  svgMain: any;
  svg: any;
  d3Zoom: any;
  d3LinkConnection: any;
  d3NodeConnection: any;
  node_drag: any;
  d3SelectedNode: any;
  d3ForeignObject: any;
  showMinimize: boolean;
  showMaximize: boolean = true;
  svgIconsList = {
    maximiseSvg: maximiseSvg,
    minimiseSvg: minimiseSvg,
    windowMinimiseSvg: windowMinimiseSvg,
    windowMaxSvg: windowMaxSvg,
    saveSvg: saveSvg,
    zoomInSvg: zoomInSvg,
    zoomOutSvg: zoomOutSvg,
    summarySvg: summarySvg,
    zoomDefault: zoomDefault,
  };
  showiconbar: boolean = true;
  configurationSchema: any;
  menuList;
  menuList_obj_one;
  menuList_obj_two: any = [];
  tabledrag_IsChanged_X: any;
  tabledrag_IsChanged_Y: any;
  showingfunction: any;
  showingfunction2: any;
  showPositonsConditions: boolean = false;


  constructor(@Inject(DOCUMENT) public document1: any, private globalService: GlobalService, private _apiService: ApiService) {
    this.menuList = JSON.parse(localStorage.getItem("menu_hierarchy"));
    this.menuList_obj_one = this.menuList[1].children;
    this.menuList_obj_two.push(this.menuList[0]);
  }
  iconTooltipValue:any;
  ngOnInit(): void {
    localStorage.removeItem('dataSource');
    this.elem = document.documentElement;
    this.menuList_obj_two.forEach((element) => {
      if (element.menuProgram == "erSchemaBrowser") {
        this.configurationSchema = element;
      }
    });
    //this.options = gridsterConfigVar;
    this.dashboard = this.erAllvalues;
    this.dashboard.forEach((element) => {
      // if(element.reportOrientation == "GRID"){
      //   element.listflag = true;
      // }else{
      //   element.listflag = false;
      // }
      if(this.erDisplayType.toUpperCase() == "GRID"){
        element.listflag = true; 
       // this.iconTooltipValue = "Table View";

      }else{
        element.listflag = false;
       // this.iconTooltipValue = "List View";
   
      }
      element.maximum = true;
      element.selectedOne = false;
      element.offSetY = element.y;
      element.offSetX = element.x;
    });
    setTimeout(() => {
      this.d3SvgCreation();
    }, 100);
  }

  @HostListener("document:click", ["$event"])
  @HostListener("document:touchstart", ["$event"])
  handleOutsideClick(event) {
    if (event.target.closest(".insideBoxScroll")) {
      this.showiconbar = false;
    }
  }

  opendropdownMenu() {
    this.showiconbar = true;
  }
  moduleType: any;
  api_passing;
  arraypassingValues: any = [];
  saveExportOptions(type) {
    if (!isNullOrUndefined(this.dashboard.moduleTables)) {
      this.moduleType = "BG";
      this.dashboard.moduleId = "";
    }

    this.dashboard.forEach(element => {
      if (element) {
        this.arraypassingValues.push({
          connectionId: element.connectionId,
          schema: element.schema,
          productName: element.productName,
          reportTitle: "Summary"
        });
      }

    });

    let obj = {
      applicationTheme: this.globalService.selectedTheme["color"].replaceAll(
        "#",
        ""
      ),
      reportTitle: "Summary",
    };

    if (type == "xlsx") {
      this.api_passing = environment.exportDataToExcel;
    }
    if (type == "Html") {
      this.api_passing = environment.exportDataToHtml;
    }
    if (type == "Pdf") {
      this.api_passing = environment.exportDataToPdf;
    }
    if (type == "Script") {
      this.api_passing = environment.exportTableScript;
    }

    let visionId = JSON.parse(
      localStorage.getItem("themeAndLanguage")
    ).visionId;

    this._apiService.fileDownloads(this.api_passing, this.arraypassingValues[0]).subscribe(
      (resp) => {
        if (resp["type"] != "application/json") {
          const blob = new Blob([resp], {
            type:
              type == "Html" || "Script"
                ? "application/zip"
                : type == "xlsx"
                  ? "text/xls"
                  : type == "pdf"
                    ? "text/pdf"
                    : "application/pdf",
          });
          const url = window.URL.createObjectURL(blob);
          const link = this.downloadZipLink.nativeElement;
          link.href = url;
          link.download =
            type == "xlsx" ? `${obj.reportTitle}.xlsx` :
              type == "Html" ? `${obj.reportTitle}.zip` :
                type == "Script" ? `${obj.reportTitle}.zip` :
                  type == "Pdf" ? `${obj.reportTitle}.pdf` : '';
          link.click();
          window.URL.revokeObjectURL(url);
        } else {
          this.globalService.showToastr.error("No Records Found");
        }
      },
      (error: any) => {
        if (error.substr(12, 3) == "204") {
          this.globalService.showToastr.error("No Records to Export");
        }
        if (error.substr(12, 3) == "420") {
          this.globalService.showToastr.error("Error Generating Report");
        }
        if (error.substr(12, 3) == "417") {
          this.globalService.showToastr.error(
            "Error while exporting the report"
          );
        }
      }
    );
  }


  obj: any = [];
  saveGeography() {
    let returnArr = [];
    this.erAllvalues.forEach(element => {
      if (element) {

        returnArr.push({
          schema: element.schema,
          borderColor: element.borderColor,
          pulsButton: true,
          data: {},
          moduleId: element.moduleId,
          productName: element.productName,
          connectionId: element.connectionId,
          description: Math.round(element.offSetX),
          shortDesc: Math.round(element.offSetY),
        });


      }
    });
    let apiUrl = environment.edwERSummary_updateErSummaryXYAxis;
    this._apiService.post(apiUrl, returnArr).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  dragOptionAdd() {
    this.options["draggable"]["start"] = (event, $element, widget) => {
      let obj = JSON.parse(JSON.stringify(event));
      this.curentPosition["x"] = obj["x"];
      this.curentPosition["y"] = obj["y"];
    };
    this.options["draggable"]["stop"] = (event, $element, widget) => {
      setTimeout(() => {
        if (
          event.x != this.curentPosition.x ||
          event.y != this.curentPosition.y
        ) {
          setTimeout(() => {
            (<any>window).jsp.repaintEverything();
          }, 1);
        }
      }, 1);
    };
    this.options["resizable"]["stop"] = (event, $element, widget) => {
      setTimeout(() => {
        (<any>window).jsp.repaintEverything();
      }, 1);
    };
    this.options["resizable"]["start"] = (event, $element, widget) => { };
  }

  d3SvgCreation() {
    this.d3ToolTipDiv = d3
      .select("body")
      .append("div")
      .attr("class", "d3_tooltip")
      .style("opacity", 0)
      .style("left", 0)
      .style("top", 0);
    this.d3ToolTipDiv2 = d3
      .select("body")
      .append("div")
      .attr("class", "d3_tooltip")
      .style("opacity", 0)
      .style("left", 0)
      .style("top", 0);


    this.d3Data = {
      nodes: this.dashboard,
      links: [],
    };

    let mainDivEle = document.getElementById("d3RenderId");
    if (mainDivEle) {
      let mainDiv = mainDivEle.getBoundingClientRect();
      this.d3Width = mainDiv.width;
      this.d3Height = mainDiv.height;

      this.d3ForceLayout = d3.forceSimulation().force("link", d3.forceLink());

      this.svgMain = d3
        .select("#d3RenderId")
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("class", "overallModulesSVG")
        .attr("viewBox", `0 0 ${this.d3Width} ${this.d3Height}`);

      this.svg = this.svgMain.append("g");
      this.d3Zoom = d3
        .zoom()
        .scaleExtent([0.25, 10])
        .on("zoom", (e) => {
          this.svg.attr("transform", d3.event.transform);
        });

      this.svgMain.call(this.d3Zoom).on("dblclick.zoom", null);

      this.svg
        .append("defs")
        .append("marker")
        .attr("id", "dominating")
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 10)
        .attr("refY", 0)
        .attr("orient", "auto-start-reverse")
        .attr("markerWidth", 5)
        .attr("markerHeight", 10)
        .append("path")
        .attr("d", "M0,-5L10,0L0,5")
        .attr("fill", "#4285f4");

      this.svg
        .append("defs")
        .append("marker")
        .attr("id", "dot")
        .attr("viewBox", [0, 0, 20, 20])
        .attr("refX", 10)
        .attr("refY", 10)
        .attr("markerWidth", 5)
        .attr("markerHeight", 5)
        .append("circle")
        .attr("cx", 10)
        .attr("cy", 10)
        .attr("r", 10)
        .style("fill", "#4285f4");

      this.d3LinkConnection = this.svg
        .append("g")
        .selectAll(".link")
        .attr("class", "edwD3links");
      this.d3NodeConnection = this.svg
        .append("g")
        .selectAll(".node")
        .attr("class", "edwD3nodes");
      setTimeout(() => {
        this.d3OuterFunction();
        this.d3Data.nodes.forEach((element) => {
          this.d3UpdateFunction(element.moduleId);
        });
      }, 100);
    }
  }

  maximizeItem(item1) {
    let filtervalues = this.treenode[0].children[1].children;
    let jsonDataForRelations;
    filtervalues.forEach((element) => {
      if (element.moduleName == item1.moduleName) {
        jsonDataForRelations = element.jsonDataForRelations;
      }
    });

    item1["jsondatarelation"] = jsonDataForRelations != "" ? jsonDataForRelations : "";
    

    this.maximizeData = item1;
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    // this.options.api.optionsChanged(); // MIKEY NOTE: this makes things going in Gridster2!
    this.childData.emit({
      type: "maximize",
      data: this.maximizeData,
      value: false,
    });
    localStorage.setItem('dataSource', JSON.stringify(item1));
  }

  menushowlist(item1) {
    item1.showMiniview = !item1.showMiniview;
    item1.listflag = !item1.listflag;
  }



  d3OuterFunction() {
    this.node_drag = d3
      .drag()
      .on("start", (d, i) => {
        this.dragstart(d, i);
      })
      .on("drag", (d, i) => {
        this.dragmove(d, i);
      })
      .on("end", (d, i) => {
        this.dragend(d, i);
      });
  }
  dragstart(d, i) {
    document
      .querySelector(`#drag_table_highLight_${d.moduleId}`)
      .classList.add("d3active");
    d3.select(`#node${d.moduleId} svgTransBox`).classed("d3active", true);
  }

  dragmove(d, i) {
    d.offSetX += d3.event.dx;
    d.offSetY += d3.event.dy;
    this.tabledrag_IsChanged_X += d.offSetX;
    this.tabledrag_IsChanged_Y += d.offSetY;
    this.showingfunction = + Math.round(d.offSetX);
    this.showingfunction2 = + Math.round(d.offSetY);
    this.showPositonsConditions = true;
    let obj = {
      table_X: d.offSetX,
      table_Y: d.offSetY,
    };

    this.tick(d.moduleId);
    // let width =0;
    // let id = document.getElementById("w-h-20");
    // if (id) {
    //   width = id.offsetWidth;
    // }
    // console.log("d3.event",d3.event);
    // console.log(d.offSetX < (112 + width))
    // let XPos = d.offSetX.toString().includes("-") ? (112 + width) : d.offSetX > 824 ? 
    // (112 + width) + 824 : (112 + width) + d.offSetX;
    // let yPos = d.offSetY < 61  ? 61 : d.offSetY;

    // this.d3ToolTipDiv
    //   .html(`X : ${d.offSetX.toFixed(2)} Y : ${d.offSetY.toFixed(2)}`)
    //   .style("opacity", 0.9)
    //   .style("left", (XPos) + "px")
    //   .style("top", (yPos) + "px");

  }

  dragend(d, i) {
    document
      .querySelector(`#drag_table_highLight_${d.moduleId}`)
      .classList.remove("d3active");
    d3.select(`#node${d.moduleId} svgTransBox`).classed("d3active", false);
    setTimeout(() => {
      this.showPositonsConditions = false;
    }, 3000);
    // this.d3ToolTipDiv
    // .style("opacity", 0)
    // .style("left", 0)
    // .style("top", 0);
  }

  tick(moduleId) {
    if (!isNullOrUndefined(this.d3LinkConnection)) {
      this.d3LinkConnection
        .attr("x1", function (d: any) {
          if (d.source.maximum) return d.source.offSetX + 170;
          else return d.source.offSetX + 38;
        })
        .attr("y1", function (d: any) {
          if (d.source.maximum) return d.source.offSetY + 95;
          else return d.source.offSetY + 23;
        })
        .attr("x2", function (d: any) {
          return d.target.offSetX;
        })
        .attr("y2", function (d: any) {
          if (d.target.maximum) return d.target.offSetY + 95;
          else return d.target.offSetY + 23;
        });
    }
    if (!isNullOrUndefined(this.d3NodeConnection)) {
      let gNode = this.svg.select(`#node${moduleId}`);
      gNode.attr("transform", function (d: any) {
        return "translate(" + d.offSetX + "," + d.offSetY + ")";
      });
    }
  }

  d3UpdateFunction(moduleId) {
    this.d3LinkConnection = this.d3LinkConnection.data(this.d3Data.links);
    this.d3LinkConnection.exit().remove();
    let linkEnter = this.d3LinkConnection
      .enter()
      .append("line")
      .attr("class", "link")
      .attr("marker-end", "url(#dominating)")
      .attr("marker-start", "url(#dot)");
    //.on('click', (d)=>{this.clickLink(d)});

    this.d3LinkConnection = linkEnter.merge(this.d3LinkConnection);
    this.d3NodeConnection = this.d3NodeConnection.data(this.d3Data.nodes);
    let nodeEnter = this.d3NodeConnection
      .enter()
      .append("g")
      .attr("class", "node")
      .attr("id", (d) => {
        return `node${d.moduleId}`;
      })
      .call(this.node_drag);

    this.d3NodeConnection = nodeEnter.merge(this.d3NodeConnection);
    this.d3NodeConnection.exit().remove();
    this.d3ForceLayout.nodes(this.d3Data.nodes).on("tick", this.tick(moduleId));
    this.d3ForceLayout.force("link").links(this.d3Data.links);
    this.d3DrawFunction(moduleId);
  }

  d3DrawFunction(moduleId) {
    this.d3SelectedNode = this.svg.select(`g#node${moduleId}`);
    let selectedObj;
    this.dashboard.forEach((element) => {
      if (element.moduleId == moduleId) selectedObj = element;
    });
    this.d3ForeignObject = this.d3SelectedNode
      .append("foreignObject")
      .on("click", () => this.dropTableSelected(moduleId))
      .attr("x", 0)
      .attr("y", 0)
      .attr("id", `drag_table_${moduleId}`)
      .attr(
        "style",
        `border-color:${selectedObj.backgroundcolur}` + "!important"
      )
      .attr("class", () => {
        if (selectedObj.maximum) return "textClass drag_table modulePageWH";
        else return "miniTextClass drag_table modulePageWH1";
      });

    if (selectedObj.maximum) {
      let svgTransBox = this.d3ForeignObject
        .append("xhtml:div")
        .attr("id", `drag_table_highLight_${moduleId}`)
        .attr("class", () => {
          if (selectedObj.selectedOne)
            return "svgTransBox svgTransBox1 w-100 d3boxShadowEffect";
          else return "svgTransBox svgTransBox1 w-100";
        });
      this.d3TransFormHeaderGenerate(selectedObj, svgTransBox);
      let svgTransbody = svgTransBox.append("xhtml:div").attr("class", () => {
        if (selectedObj.tableClicked)
          return "svgTransbody container_min positionRelative";
        else return "svgTransbody container_min positionRelative";
      });
      if (selectedObj.listflag) {
        selectedObj.moduleData.forEach((element) => {
          let height = 150;
          let divContainer = svgTransbody
            .append("div")
            .attr("class", "widthgridster gridster-box-child  m-1")
            .style("height", `${height}px`)
            .style("width", `${element.width}px`)
            .style("top", `${element.offSetY}px`)
            .style("left", `${element.offSetX}px`)
            .attr("data-id", element.tableId);
          divContainer
            .append("div")
            .attr("class", "header_label")
            .html(element.tableName);

          let table = divContainer
            .append("div")
            .attr("class", "overflow_auto")
            .append("table")
            .attr("id", "statictable mt-1")
            .append("tbody");
          element.erTableData.forEach((element2) => {
            table
              .append("tr")
              .append("td")
              .attr("title", element2.columnName)
              .html(element2.columnName);
          });
        });
      } else {
        let ul = svgTransbody.append("ul").attr("class", "underline");
        selectedObj.moduleData.forEach((element) => {
          ul.append("li").html(element.tableName);
        });
      }
    } else {
      this.minimizeForeignObject(selectedObj);
    }
  }

  d3TransFormHeaderGenerate(selectedObj, svgTransBox) {
    let svgTransHeader = svgTransBox.append("xhtml:div").attr("class", () => {
      if (selectedObj.selectedOne) return "svgTransHeader svgTransHeader1";
      else return "svgTransHeader svgTransHeader1";
    });

    let svgTransHeaderDiv = svgTransHeader
      .append("div")
      .attr(
        "class",
        "header_title_table header_title_table1 drag-accept d-flex"
      );
    svgTransHeaderDiv
      .append("span")
      .attr("class", "boxIconSvgTrans")
      .append("span")
      .attr("class", "material-icons font18pxSvg")
      .attr("aria-hidden", "true")
      .html("table_chart");

    let svgTransHeaderDivTemp = svgTransHeaderDiv
      .append("span")
      .attr("class", "g_sudmit_edit1 g_sudmit_edit1")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html(selectedObj.moduleName)
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });

    svgTransHeaderDivTemp.append("span").html(() => {
      if (selectedObj.moduleName.length > 20)
        return selectedObj.moduleName.slice(0, 19) + "..";
      else return selectedObj.moduleName;
    });

    let svgTransHeaderDivTemp2 = svgTransHeaderDiv
      .append("span")
      .attr("class", "pull-right pos_minCan  pos_minCan1 d-flex");

    let listIconA = svgTransHeaderDivTemp2
      .append("a")
      .attr("href", "javascript:void(0);")
      .on("click", () => this.menushowlist(selectedObj))
      .attr("class", "")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html(this.iconTooltipValue)
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });
    if (selectedObj.listflag) {
      this.iconTooltipValue = "List View";
      listIconA
        .append("span")
        .attr("class", "material-icons font18pxSvg")
        .html("format_list_bulleted");
    } else {
      this.iconTooltipValue = "Table View";
      
      listIconA
        .append("span")
        .attr("class", "material-icons font18pxSvg")
        .html("apps");
    }

    svgTransHeaderDivTemp2
      .append("a")
      .attr("href", "javascript:void(0);")
      .on("click", () => this.minimize(selectedObj))
      .attr("class", "")
      .append("span")
      .attr("class", "material-icons font18pxSvg")
      .html("remove")
      //.attr("aria-hidden","true")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html("Minimize")
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });

    svgTransHeaderDivTemp2
      .append("a")
      .attr("href", "javascript:void(0);")
      .on("click", () => this.maximizeItem(selectedObj))
      .attr("class", "")
      .append("span")
      .attr("class", "material-icons font18pxSvg")
      .html("open_in_full")
      //.attr("aria-hidden","true")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html("Expand")
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });
  }

  dropTableSelected(moduleId) {
    this.dashboard.forEach((element) => {
      element.selectedOne = false;
    });
    this.dashboard.forEach((element) => {
      if (element.moduleId == moduleId) element.selectedOne = true;
    });

    this.dashboard.forEach((element) => {
      this.d3G_Recreate(element.moduleId);
    });
  }

  d3G_Recreate(opt) {
    let deleteNode = this.svg.select(`g#node${opt} #drag_table_${opt}`);
    if (deleteNode) {
      deleteNode.remove();
      this.d3UpdateFunction(opt);
    }
  }
  maxmize(selectedObj) {
    selectedObj.maximum = true;
    this.d3G_Recreate(selectedObj.moduleId);
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
  }

  minimize(selectedObj) {
    selectedObj.maximum = false;
    this.d3G_Recreate(selectedObj.moduleId);
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
  }

  minimizeForeignObject(selectedObj) {
    let svgMinimize = this.d3ForeignObject
      .append("xhtml:div")
      .attr("class", "svgMinimize")
      .on("dblclick", () => this.maxmize(selectedObj));

    let svgMinimizeDiv = svgMinimize
      .append("xhtml:div")
      .attr("class", "container_max header_title_table header_title_table1")
    // .on("mouseover", (d) => {
    //   this.d3ToolTipDiv
    //     .html(selectedObj.moduleName)
    //     .style("opacity", 0.9)
    //     .style("left", d3.event.pageX - 34 + "px")
    //     .style("top", d3.event.pageY - 40 + "px");
    // })
    // .on("mouseout", (d) => {
    //   this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    // });

    svgMinimizeDiv
      .append("span")
      .append("span")
      .attr("class", "material-icons font18pxSvg")
      .html("table_chart");
    svgMinimizeDiv = svgMinimize
      .append("xhtml:div")
      .attr("class", "container_max header_title_table");
    let span = svgMinimizeDiv
      .append("span");
    svgMinimizeDiv.html(`<span class="miniTableName-maxi">${selectedObj.moduleName}</span>`)
  }





  //d3 Function //

  zoomIn() {
    this.svgMain.select("g").transition().call(this.d3Zoom.scaleBy, 1.2);
  }
  zoomOut() {
    this.svgMain.select("g").transition().call(this.d3Zoom.scaleBy, 0.8);
  }
  zoomReset() {
    this.svgMain.select("g").transition().call(this.d3Zoom.scaleTo, 1);
  }

  minimizeAll() {
    this.showMinimize = true;
    this.showMaximize = false;
    this.dashboard.forEach((ele) => {
      if (ele.maximum == 1) this.minimize(ele);
    });
  }
  maximizeAll() {

    this.showMinimize = false;
    this.showMaximize = true;
    this.dashboard.forEach((ele) => {
      if (ele.maximum == 0) this.maxmize(ele);
    });
  }

  fullScreen() {
    this.fullScreenDisp
      ? this.goOutFullscreen()
      : this.goFulScreen(document.getElementById("d3FullScreen"));
    // this.miniViewBol()
  }

  goOutFullscreen() {
    let sidebar = document.getElementById("max_sidebar");
    let navbar = document.getElementById("max_nav");
    let mainDiv = document.getElementById("max_parent");
    let mainPanel = document.getElementById("mainPanel");
    let wh20 = document.getElementById("w-h-20");
    let d3FullScreen = document.getElementById("d3FullScreen");
    let slide = document.getElementById("slide_open");

    sidebar.classList.remove("max_sidebar_full");
    navbar.classList.remove("max_nav_full");
    mainDiv.classList.remove("max_parent_full", "maximize-view");
    wh20.classList.remove("w-h-20_full");
    mainPanel.classList.remove("mainPanel_full", "maximize-pannel");
    d3FullScreen.classList.remove("d3FullScreen_full");
    slide.classList.remove("d-none");
    // mainDiv.classList.remove('maximize-view');
    // mainPanel.classList.remove('maximize-pannel');
    if (window.innerHeight == screen.height) {
      if (this.document1.exitFullscreen) {
        this.document1.exitFullscreen();
      } else if (this.document1.mozCancelFullScreen) {
        this.document1.mozCancelFullScreen();
      } else if (this.document1.webkitExitFullscreen) {
        this.document1.webkitExitFullscreen();
      } else if (this.document1.msExitFullscreen) {
        this.document1.msExitFullscreen();
      }
    }
    this.fullScreenDisp = false;
    this.ngAfterViewInit();
  }

  goFulScreen = (element) => {
    let sidebar = document.getElementById("max_sidebar");
    let navbar = document.getElementById("max_nav");
    let mainDiv = document.getElementById("max_parent");
    let mainPanel = document.getElementById("mainPanel");
    let wh20 = document.getElementById("w-h-20");
    let d3FullScreen = document.getElementById("d3FullScreen");
    let slide = document.getElementById("slide_open");

    sidebar.classList.add("max_sidebar_full");
    navbar.classList.add("max_nav_full");
    mainDiv.classList.add("max_parent_full", "maximize-view");
    wh20.classList.add("w-h-20_full");
    mainPanel.classList.add("mainPanel_full", "maximize-pannel");
    d3FullScreen.classList.add("d3FullScreen_full");
    slide.classList.add("d-none");
    // mainDiv.classList.add('maximize-view');
    // mainPanel.classList.add('maximize-pannel');
    this.fullScreenDisp = true;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    }
    this.ngAfterViewInit();
  };

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }
}




// jqPlumbStart() {
//   setTimeout(() => {
//     const instance = jsPlumb.getInstance({
//       Connector: "Straight",
//       PaintStyle: {
//         strokeWidth: 1,
//         stroke: "#ccc",
//         fill: "none",
//       },
//       Endpoint: [
//         "Rectangle",
//         {
//           width: 5,
//           height: 5,
//         },
//       ],
//       EndpointStyle: {
//         fill: "#ffa500",
//       },
//       Container: "canvas",
//       ConnectionOverlays: [
//         [
//           "Arrow",
//           {
//             location: 1,
//             width: 11,
//             length: 11,
//           },
//         ],
//       ],
//     });
//     (<any>window).jsp = instance;
//     (<any>window).jsp.setContainer("canvas");
//     setTimeout(() => {
//       this.startConnection();
//     }, 10);
//   }, 500);
// }

// startConnection() {
//   this.connectionNode("Admin", "Demography", "Top", "Bottom");
//   this.connectionNode("Admin", "Operations", "Right", "Left");
//   this.connectionNode("Demography", "Products", "Right", "Left");
//   this.connectionNode("Operations", "Products", "Top", "Bottom");
// }

// connectionNode(source, target, anchor1, anchor2) {
//   let src = document.getElementById(source);
//   let tgt = document.getElementById(target);
//   this.jsPlumbConnectionFn(src, tgt, anchor1, anchor2);
// }

// jsPlumbConnectionFn(source, target, anchor1, anchor2) {
//   (<any>window).jsp.connect({
//     source: source,
//     target: target,
//     anchor: [anchor1, anchor2],
//     hoverPaintStyle: { stroke: "#000" },
//     overlays: [["Label", { label: "foo", id: "myLabel" }]],
//   });
// }
