import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErDiagramoverallComponent } from './er-diagramoverall.component';

describe('ErDiagramoverallComponent', () => {
  let component: ErDiagramoverallComponent;
  let fixture: ComponentFixture<ErDiagramoverallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErDiagramoverallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErDiagramoverallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
