import {
  Component,
  Input,
  OnInit,
  HostListener,
  OnChanges,
  Output,
  ViewChild,
  ElementRef,
  EventEmitter,
  Inject,
} from "@angular/core";
import { TablecolumnlistingComponent } from "./../tablecolumnlisting/tablecolumnlisting.component";
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { ApiService, GlobalService } from "src/app/service";
import { environment } from "src/environments/environment";
import { isNullOrUndefined } from "util";
import { gridsterConfigVar } from "../data.js";
import { GridsterConfig } from "angular-gridster2";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CommonService } from "src/app/service/common.service";
import { LoaderService } from "src/app/service/loader.service";
import { DOCUMENT } from "@angular/common";
import { ITreeOptions } from "@circlon/angular-tree-component";

import { maximiseSvg } from "src/assets/svg/maximise";
import { minimiseSvg } from "src/assets/svg/minimize";
import { windowMaxSvg } from "src/assets/svg/window-max";
import { windowMinimiseSvg } from "src/assets/svg/window-min";
import { zoomInSvg } from "src/assets/svg/zoom-in";
import { zoomOutSvg } from "src/assets/svg/zoom-out";
import { zoomDefault } from "src/assets/svg/zoom-default";
import { saveSvg } from "src/assets/svg/save";
import { summarySvg } from "src/assets/svg/Summary";

declare let d3: any;
declare let $: any;

@Component({
  selector: "app-maximize",
  templateUrl: "./maximize.component.html",
  styleUrls: ["./maximize.component.css"],
})
export class MaximizeComponent implements OnInit, OnChanges {
  @Input() maximizeData;
  @Input() dragOptiontrigger;
  @Input() triggerOption;
  @Input() fileExplorer;
  @Output() childData = new EventEmitter();
  @Output() childDataForTree = new EventEmitter();
  @Output() dragOptionIsChanged = new EventEmitter<any>();
  curentPosition: any = { x: 0, y: 0 };
  tablelistcolumnselectedvales: any = [];
  tablecolumns: any = [];
  tablecolumnsvalues: any = [];
  maximizedatacolumnvalues: any = [];
  configurationSchema: any;
  temp_array_maximize_values: any = [];
  menuList;
  menuList_obj_one;
  menuList_obj_two: any = [];
  type: any;
  moduleType: any;
  showMinimize: boolean;
  showMaximize: boolean;
  options: GridsterConfig = {
    draggable: {
      enabled: false,
    },
    pushItems: true,
    resizable: {
      enabled: false,
    },
  };
  api_passing;
  viewList: any = "xlsx";
  firstInstance: any;
  connections: any = [];
  tablesname = [];
  tablecolumnsarrayone: any = [];
  tablecolumnsarraytwo: any = [];
  checkingdropcondition: boolean = true;
  sourcetable: any;
  labelvaluesForConnection: any;
  lablevalues: any = [];
  tabN: any;
  realtionString: any = [];
  arrayTemporary = [];
  valuesForLabel: any;

  /* d3 Function Start Here */
  margin: any = { top: 20, right: 20, bottom: 20, left: 20 };
  svg: any;
  d3Width: any;
  d3Height: any;
  d3ForceLayout: any;
  d3LinkConnection: any;
  d3NodeConnection: any;
  d3ForeignObject: any;
  d3SelectedNode: any;
  d3RelationSvg: any;
  d3RelationForceLayout: any;
  relationsLink: any = [];
  d3RelationConnection: any;
  node_drag: any;
  d3ToolTipDiv: any;
  d3PopupDiv: any;
  d3sourceId: any;
  d3targetId: any;
  d3Zoom: any;
  svgMain: any;
  @ViewChild("relationsContent", { static: false })
  relationsContent: ElementRef;
  loopingTableData: any;
  loopBindingTableDta: any;

  d3Data: any = {
    nodes: [],
    links: [],
  };
  d3DataObj: any = {
    nodes: [],
    links: [],
  };

  sourceTableObj: any = {};
  targetTableList: any[];
  targetTable: any[];
  sourceTable: any[];
  popupd3ForeignObject: any;
  popupsvg: any;
  popupd3RelationSvg: any = [];
  popupd3LinkConnectionData: any = [];
  popupd3Links = [];
  showConnector: boolean;
  tempArrayVariable: any = [];
  showingfunction: any;
  showingfunction2: any;
  showPositonsConditions: boolean = false;
  connection = [
    {
      type: "Connect by position",
      active: "",
    },
    {
      type: "Connect by name",
      active: "",
    },
  ];
  tabledrag_IsChanged_X: any;
  tabledrag_IsChanged_Y: any;
  popupconnectedSources: any[];
  popupconnectedTargets: any[];
  targetPopup = [];
  targetFlag: boolean = false;
  currentTran: any = {};
  selectedTargetTable: any;
  fullScreenDisp: boolean = false;
  elem: HTMLElement;
  dragDisable: boolean = false;
  showiconbar: boolean = true;
  public options1: ITreeOptions = {
    allowDrag: (node) => node.data.moduleType == "T",
    allowDrop: false,
    useCheckbox: false,
    allowDragoverStyling: true,
    useVirtualScroll: false,
    displayField: "nodeName",
    isExpandedField: "expanded",
    actionMapping: {
      mouse: {
        dragStart: (tree, node) => {
          if (!node["hasChildren"] && node.data.moduleType == "T")
            node["dragging"] = true;
        },
        dragEnd: (tree, node) => {
          node["dragging"] = true;
        },
      },
    },
  };
  @ViewChild("connectionPopup") private connectionPopup: ElementRef;
  @ViewChild("downloadZipLink", { static: false })
  private downloadZipLink: ElementRef;

  svgIconsList = {
    maximiseSvg: maximiseSvg,
    minimiseSvg: minimiseSvg,
    windowMinimiseSvg: windowMinimiseSvg,
    windowMaxSvg: windowMaxSvg,
    saveSvg: saveSvg,
    zoomInSvg: zoomInSvg,
    zoomOutSvg: zoomOutSvg,
    summarySvg: summarySvg,
    zoomDefault: zoomDefault,
  };

  constructor(
    private modalService: NgbModal,
    private commonService: CommonService,
    private apiService: ApiService,
    private globalService: GlobalService,
    private loaderService: LoaderService,
    @Inject(DOCUMENT) public document1: any
  ) {
    this.menuList = JSON.parse(localStorage.getItem("menu_hierarchy"));
    this.menuList_obj_one = this.menuList[1].children;
    this.menuList_obj_two.push(this.menuList[0]);
  }
  erDisplay: any;
  ngOnInit(): void {
    this.elem = document.documentElement;
    this.menuList_obj_two.forEach((element) => {
      if (element.menuProgram == "erSchemaBrowser") {
        this.configurationSchema = element;
      }
    });

    this.temp_array_maximize_values = this.maximizeData;
    this.options = gridsterConfigVar;
    if (this.maximizeData.moduleData.length)
      this.erDisplay = this.maximizeData.reportOrientation;
    this.maximizeData.moduleData.forEach((element) => {
      if (this.maximizeData.erDisplayType.toUpperCase() == "GRID") {
        element.maximum = true;
        this.showMinimize = false;
        this.showMaximize = true;
      } else {
        element.maximum = false;
        this.showMaximize = false;
        this.showMinimize = true;
      }

    });
    setTimeout(() => {
      this.d3SvgCreation();
    }, 100);
  }

  maxiBtnSet: boolean = false;
  bckButtonShow: boolean = true;
  ngOnChanges() {

    //-->for drag option--> trigger--> In  ermodule component
    if (this.dragOptiontrigger.length != 0) {
      this.zoomFitpantree();
    }
    if (this.triggerOption.vlaues) {

      let mainDivEle = document.getElementById("d3RenderId");
      if (mainDivEle) {

        let mainDiv = mainDivEle.getBoundingClientRect();
        this.d3Width = mainDiv.width;
        this.d3Height = mainDiv.height;
        this.d3ForceLayout = d3.forceSimulation().force("link", d3.forceLink());
        this.svgMain = d3
          .select("#d3RenderId")
          .select("svg")
          .attr("preserveAspectRatio", "xMinYMin meet")
          .attr("class", "overallModulesSVG")
          .attr("viewBox", `0 0 ${this.d3Width} ${this.d3Height}`);
      }
    }
  }

  ngOnDestroy() {
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
  }


  mainFunctions(color?) {
    this.loaderService.hide();
    setTimeout(() => {
      this.d3SvgCreation();
    });
  }

  //--->svg functionalities for all main page table loaded //
  d3SvgCreation() {
    this.d3ToolTipDiv = [];
    this.d3PopupDiv = [];

    this.d3ToolTipDiv = d3
      .select("body")
      .append("div")
      .attr("class", "d3_tooltip")
      .style("opacity", 0)
      .style("left", 0)
      .style("top", 0);
    this.d3PopupDiv = d3
      .select("#d3PopupDiv1")

      .style("opacity", 0)
      .style("left", "0px")
      .style("top", "0px");

    let linkerList = this.linedraw();
    this.d3Data = {
      nodes: this.maximizeData.moduleData,
      links: linkerList,
    };
    let mainDivEle = document.getElementById("d3RenderId");
    if (mainDivEle) {
      let mainDiv = mainDivEle.getBoundingClientRect();
      this.d3Width = mainDiv.width;
      this.d3Height = mainDiv.height;


      this.d3ForceLayout = d3.forceSimulation().force("link", d3.forceLink());
      this.svgMain = d3
        .select("#d3RenderId")
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("class", "overallModulesSVG")
        .attr("viewBox", `0 0 ${this.d3Width} ${this.d3Height}`);
      // .attr("viewBox", `0 0 ${this.d3Width} 565`);

      this.svg = this.svgMain.append("g");
      this.d3Zoom = d3
        .zoom()
        .scaleExtent([0.25, 10])
        .on("zoom", (e) => {
          this.svg.attr("transform", d3.event.transform);
        });

      this.svgMain.call(this.d3Zoom).on("dblclick.zoom", null);

      this.svg
        .append("defs")
        .append("marker")
        .attr("id", "dominating")
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 10)
        .attr("refY", 0)
        .attr("orient", "auto-start-reverse")
        .attr("markerWidth", 5)
        .attr("markerHeight", 5)
        .append("path")
        .attr("d", "M0,-5L10,0L0,5")
        .attr("fill", "#4285f4");

      this.svg
        .append("defs")
        .append("marker")
        .attr("id", "dot")
        .attr("viewBox", [0, 0, 20, 20])
        .attr("refX", 10)
        .attr("refY", 10)
        .attr("markerWidth", 5)
        .attr("markerHeight", 5)
        .append("circle")
        .attr("cx", 10)
        .attr("cy", 10)
        .attr("r", 10)
        .style("fill", "#4285f4");

      this.d3LinkConnection = this.svg
        .append("g")
        .selectAll(".link")
        .attr("class", "edwD3links");

      this.d3NodeConnection = this.svg
        .append("g")
        .selectAll(".node")
        .attr("class", "edwD3nodes");
      setTimeout(() => {
        this.d3OuterFunction();
        this.d3Data.nodes.forEach((element) => {
          this.d3UpdateFunction(element.tableId);
        });
      }, 100);
    }
  }

  d3OuterFunction() {
    this.node_drag = d3
      .drag()
      .on("start", (d, i) => {
        this.dragstart(d, i);
      })

      .on("drag", (d, i) => {
        this.dragmove(d, i);
      })
      .on("end", (d, i) => {
        this.dragend(d, i);
      });
  }
  dragstart(d, i) {
    let selectedObj;
    this.maximizeData.moduleData.forEach((element) => {
      if (element.tableId == d.tableId) selectedObj = element;
    });
    if (!isNullOrUndefined(document
      .querySelector(`#drag_table_highLight_${d.tableId}`))) {
      document
        .querySelector(`#drag_table_highLight_${d.tableId}`)
        .classList.add("d3active");
      d3.select(`#node${d.tableId} svgTransBox`).classed("d3active", true);
    }
  }

  dragmove(d, i) {
    d.offSetX += d3.event.dx;
    d.offSetY += d3.event.dy;
    this.tabledrag_IsChanged_X += d.offSetX;
    this.tabledrag_IsChanged_Y += d.offSetY;
    this.showingfunction = + Math.round(d.offSetX);
    this.showingfunction2 = + Math.round(d.offSetY);
    this.showPositonsConditions = true;
    let obj = {
      table_X: d.offSetX,
      table_Y: d.offSetY,
    };
    this.dragOptionIsChanged.emit(obj);
    this.tick(d.tableId);
    // let XPos = d3.event.sourceEvent.layerX < 104 ? 104 : d3.event.sourceEvent.layerX > (window.innerWidth - 118) ? window.innerWidth - 118 : d3.event.sourceEvent.layerX;
    // let yPos = d3.event.sourceEvent.layerY < 61 ? 61 : d3.event.sourceEvent.layerY > (window.innerHeight - 70) ? window.innerHeight - 70 : d3.event.sourceEvent.layerY;

    // this.d3ToolTipDiv
    //   .html(`X : ${d.offSetX.toFixed(2)} Y : ${d.offSetY.toFixed(2)}`)
    //   .style("opacity", 0.9)
    //   .style("left", (XPos) + "px")
    //   .style("top", (yPos) + "px");

  }


  dragend(d, i) {
    if (!isNullOrUndefined(document
      .querySelector(`#drag_table_highLight_${d.tableId}`)))
      document
        .querySelector(`#drag_table_highLight_${d.tableId}`).classList.remove("d3active");
    d3.select(`#node${d.tableId} svgTransBox`).classed("d3active", false);
    setTimeout(() => {
      this.showPositonsConditions = false;
    }, 3000);
    // this.d3ToolTipDiv
    //   .style("opacity", 0)
    //   .style("left", 0)
    //   .style("top", 0);
  }

  tick(tableId) {
    if (!isNullOrUndefined(this.d3LinkConnection)) {
      this.d3LinkConnection

        .attr("x1", function (d: any) {
          if (d.source.maximum) return d.source.offSetX + 170;
          else return d.source.offSetX + 38;
        })
        .attr("y1", function (d: any) {
          if (d.source.maximum) return d.source.offSetY + 95;
          else return d.source.offSetY + 23;
        })
        .attr("x2", function (d: any) {
          return d.target.offSetX;
        })
        .attr("y2", function (d: any) {
          if (d.target.maximum) return d.target.offSetY + 95;
          else return d.target.offSetY + 23;
        });
    }
    if (!isNullOrUndefined(this.d3NodeConnection)) {
      let gNode = this.svg.select(`#node${tableId}`);
      gNode.attr("transform", function (d: any) {
        return "translate(" + d.offSetX + "," + d.offSetY + ")";
      });
    }
  }

  d3UpdateFunction(tableId) {
    this.d3LinkConnection = this.d3LinkConnection.data(this.d3Data.links);
    this.d3LinkConnection.exit().remove();
    let linkEnter = this.d3LinkConnection
      .enter()
      .append("line")
      .attr("class", "link")
      .attr("marker-end", "url(#dominating)")
      .attr("marker-start", "url(#dot)")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html(d.connectionname)
          .style("opacity", 0.9)
          .style("width", "auto")
          .style("height", "auto")
          .style("left", d3.event.pageX - 95 + "px")
          .style("top", d3.event.pageY - 85 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });

    this.d3LinkConnection = linkEnter.merge(this.d3LinkConnection);
    this.d3NodeConnection = this.d3NodeConnection.data(this.d3Data.nodes);
    let nodeEnter = this.d3NodeConnection
      .enter()
      .append("g")
      .attr("class", "node")
      .attr("id", (d) => {
        return `node${d.tableId}`;
      });
    if (this.configurationSchema.profileModify == "Y") {
      this.d3NodeConnection.call(this.node_drag);
    }

    this.d3NodeConnection = nodeEnter.merge(this.d3NodeConnection);
    this.d3NodeConnection.exit().remove();
    this.d3ForceLayout.nodes(this.d3Data.nodes).on("tick", this.tick(tableId));
    this.d3ForceLayout.force("link").links(this.d3Data.links);
    this.d3DrawFunction(tableId);
  }

  d3DrawFunction(tableId) {
    this.d3SelectedNode = this.svg.select(`g#node${tableId}`);
    let selectedObj;
    this.maximizeData.moduleData.forEach((element) => {
      if (element.tableId == tableId) {
        selectedObj = element;
      }
    });
    this.d3ForeignObject = this.d3SelectedNode
      .append("foreignObject")
      .on("click", () => this.dropTableSelected(tableId))
      .attr("x", 0)
      .attr("y", 0)
      .attr("id", `drag_table_${tableId}`)
      .attr("class", () => {
        if (selectedObj.maximum) {
          return "textClass drag_table";
        }

        else {
          return "miniTextClass drag_table";
        }
      });

    if (selectedObj.maximum) {
      this.d3TransFormIconGenerate(selectedObj);
      let svgTransBox = this.d3ForeignObject
        .append("xhtml:div")
        .attr("id", `drag_table_highLight_${tableId}`)
        .attr("class", () => {
          if (selectedObj.tableClicked)
            return "svgTransBox w-100 d3boxShadowEffect";
          else return "svgTransBox w-100";
        });
      this.d3TransFormHeaderGenerate(selectedObj, svgTransBox);
      let svgTransbody = svgTransBox.append("xhtml:div").attr("class", () => {
        if (selectedObj.tableClicked) return "svgTransbody container_min";
        else return "svgTransbody container_min";
      });

      let ul = svgTransbody
        .append("ul")
        .attr("class", "display nowrap tablefont")
        .attr("id", "excelDataTable");

      selectedObj.erTableData.forEach((element: any, index) => {
        let li = ul
          .append("li")
          .attr("id", () => {
            return `row_${selectedObj.tableId}_${element.columnId}`;
          })
          .attr("class", "d-flex align-items-center justify-content-between");

        let liDiv = li
          .append("div")
          .attr("class", "ml-4")
          .html(element.columnName);

        if (!isNullOrUndefined(element.dataIndex) && element.dataIndex) {
          li.append("i")
            .attr("class", "fa fa-key boxInfoPos")
            .attr("aria-hidden", "true");
        }
      });
    } else {
      this.minimizeForeignObject(selectedObj);
    }
  }

  d3TransFormIconGenerate(selectedObj) {
    let svgTransEventIcons = this.d3ForeignObject
      .append("xhtml:div")
      .attr("class", "svgTransEventIcons");

    if (this.configurationSchema.profileModify == "Y") {
      if (selectedObj.tableClicked) {
        svgTransEventIcons
          .append("a")
          .attr("href", "javascript:void(0);")
          .on("click", () => this.autoconnectColumns(selectedObj))
          .append("i")
          .attr("class", "fa fa-link")
          .attr("aria-hidden", "true")
          .on("mouseover", (d) => {
            this.d3ToolTipDiv
              .html("Select All")
              .html("Connect")
              .style("opacity", 0.9)
              .style("left", d3.event.pageX - 34 + "px")
              .style("top", d3.event.pageY - 40 + "px");
          })
          .on("mouseout", (d) => {
            this.d3ToolTipDiv
              .style("opacity", 0)
              .style("left", 0)
              .style("top", 0);
          });

        svgTransEventIcons
          .append("a")
          .attr("href", "javascript:void(0);")
          .on("click", () => this.openTablecolumns(selectedObj))
          .append("i")
          .attr("class", "fa fa-list")
          .attr("aria-hidden", "true")
          .on("mouseover", (d) => {
            this.d3ToolTipDiv
              .html("Columns List")
              .style("opacity", 0.9)
              .style("left", d3.event.pageX - 34 + "px")
              .style("top", d3.event.pageY - 40 + "px");
          })
          .on("mouseout", (d) => {
            this.d3ToolTipDiv
              .style("opacity", 0)
              .style("left", 0)
              .style("top", 0);
          });
      }
    }
  }

  checkClass() {
    if (this.configurationSchema.profileModify == "Y") {
      return "pos_minCan d-flex";
    } else return "pos_minCan d-flex visibility_none";
  }

  d3TransFormHeaderGenerate(selectedObj, svgTransBox) {
    let svgTransHeader = svgTransBox.append("xhtml:div").attr("class", () => {
      if (selectedObj.tableClicked) return "svgTransHeader";
      else return "svgTransHeader";
    });

    let svgTransHeaderDiv = svgTransHeader
      .append("div")
      .attr("class", "header_title_table-maxi drag-accept d-flex");
    svgTransHeaderDiv
      .append("span")
      .attr("class", "boxIconSvgTrans")
      .append("i")
      .attr("class", "fa fa-table")
      .attr("aria-hidden", "true");

    let svgTransHeaderDivTemp = svgTransHeaderDiv
      .append("span")
      .attr("class", "g_sudmit_edit")
      .on("click", () => this.tableClicked(selectedObj))
    // .on("mouseover", (d) => {
    //   this.d3ToolTipDiv
    //     .html(selectedObj.tableName)
    //     .style("opacity", 0.9)
    //     .style("left", d3.event.pageX - 34 + "px")
    //     .style("top", d3.event.pageY - 40 + "px");
    // })
    // .on("mouseout", (d) => {
    //   this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    // });

    svgTransHeaderDivTemp.append("span").html(() => {
      if (selectedObj.tableName.length > 17)
        return selectedObj.tableName.slice(0, 17) + "..";
      else return selectedObj.tableName;
    });

    let svgTransHeaderDivTemp2 = svgTransHeaderDiv
      .append("span")
      .attr("class", () => this.checkClass());
    let svgTransHeaderDivTemp3 = svgTransHeaderDiv
      .append("span")
      .attr("class", () => this.checkClass());

    svgTransHeaderDivTemp2
      .append("a")
      .attr("href", "javascript:void(0);")
      .on("click", () => this.minimize(selectedObj))
      .attr("class", "")
      .append("i")
      .attr("class", "fa fa-minus-circle")
      .attr("aria-hidden", "true")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html("Minimize")
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });

    svgTransHeaderDivTemp3
      .append("a")
      .attr("href", "javascript:void(0);")
      .on("click", () => this.removeTable(selectedObj))
      .attr("class", "")
      .append("i")
      .attr("class", "fa fa-times-circle")
      .attr("aria-hidden", "true")
      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html("Remove")
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      });
    // }
  }

  dropTableSelected(tableId) {
    this.maximizeData.moduleData.forEach((element) => {
      element.tableClicked = false;
    });
    this.maximizeData.moduleData.forEach((element) => {
      if (element.tableId == tableId) element.tableClicked = true;
    });

    this.maximizeData.moduleData.forEach((element) => {
      this.d3G_Recreate(element.tableId);
    });
  }

  d3G_Recreate(opt) {
    let deleteNode = this.svg.select(`g#node${opt} #drag_table_${opt}`);
    if (deleteNode) {
      deleteNode.remove();
      this.d3UpdateFunction(opt);
    }
  }
  maxmize(selectedObj) {
    selectedObj.maximum = true;
    this.d3G_Recreate(selectedObj.tableId);

    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
  }

  minimize(selectedObj) {
    selectedObj.maximum = false;
    this.d3G_Recreate(selectedObj.tableId);

    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
  }

  removeTable(selectedObj) {
    let deleteNode = this.svg.select(`g#node${selectedObj.tableId}`);
    if (deleteNode) deleteNode.remove();

    this.d3UpdateFunction(selectedObj.tableId);
    this.d3ToolTipDiv.style("opacity", 0);
    this.removeD3Links(selectedObj.tableId);


  }

  removeD3Links(tableId) {

    for (var i = this.d3Data.links.length - 1; i >= 0; i--) {
      if (
        this.d3Data.links[i].source.tableId == tableId ||
        this.d3Data.links[i].target.tableId == tableId
      ) {
        this.d3Data.links.splice(i, 1);
      }
    }
    this.maximizeData.moduleData = this.maximizeData.moduleData.filter(obj => {
      return obj.tableId !== tableId
    });


  }




  tableClicked(item) {

    this.d3G_Recreate(item.tableId);
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);

    this.childData.emit({ type: "table", data: item });
  }


  minimizeForeignObject(selectedObj) {
    let svgMinimize = this.d3ForeignObject
      .append("xhtml:div")
      .attr("class", "svgMinimize")
      .on("dblclick", () => this.maxmize(selectedObj));
    let svgMinimizeDiv = svgMinimize
      .append("xhtml:div")
      .attr("class", "container_max header_title_table");
    // let span = svgMinimizeDiv
    //   .append("span");
    // svgMinimizeDiv.html(`<i class="fa fa-table hide-cursor"  aria-hidden=true></i>
    //   <span class="miniTableName">${selectedObj.tableName}</span>`)

    svgMinimizeDiv
      .append("span")
      .attr("class", "miniTableName");
    svgMinimizeDiv
      .append("i")
      .attr("class", "fa fa-table hide-cursor")
      .attr("aria-hidden", "true");

    svgMinimizeDiv.append("span").html(() => {
      if (selectedObj.tableName)
        return selectedObj.tableName
    }).on("click", () => this.tableClicked(selectedObj));
    // .on("mouseover", (d) => {
    //   this.d3ToolTipDiv
    //     .html(selectedObj.tableName)
    //     .style("opacity", 0.9)
    //     .style("left", d3.event.pageX - 34 + "px")
    //     .style("top", d3.event.pageY - 40 + "px");
    // })
    // .on("mouseout", (d) => {
    //   this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    // });
    //let svgSpanFlex=svgMinimizeDiv.append("div").attr("class", "d-flex flex-column justify-content-center align-items-center");

    // svgSpanFlex
    // .append("span")
    // .html(selectedObj.tableName)
    // span.append("i")
    // .attr("class", "fa fa-table")
    // .attr("aria-hidden", "true");

  }
  showboolean: boolean = false;
  autoconnectColumns(selectedObj) {
    this.showboolean = true;
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    this.sourceTableObj = selectedObj;
    this.sourceTable = [];
    this.targetTableList = [];

    this.sourceTable = this.sourceTableObj["erTableData"];
    this.sourceTable.forEach((source) => {
      source["tranColumnId"] = source["columnId"];
    });
    let holdingarrayvlaues = this.d3Data.links;
    let labelconnectionvalues = holdingarrayvlaues.filter(value => value["source"].tableName === this.sourceTableObj.tableName);
    let tableNames: any = [];
    for (let i = 0; i < labelconnectionvalues.length; i++) {
      tableNames.push({ "tableName": labelconnectionvalues[i].target.tableName });
    }
    this.maximizeData["moduleData"].forEach((target) => {
      if (target.tableId != this.sourceTableObj["tableId"])
        this.targetTableList.push({
          tableId: target.tableId,
          tableName: target.tableName,
          checked: false,
        });
    });

    this.targetTableList.forEach((element) => {
      tableNames.forEach((element1) => {
        if (element.tableName == element1.tableName) {
          element.checked = true;
        }
      });
    });
    this.modalService.open(this.connectionPopup, {
      size: "lg",
      backdrop: "static",
      windowClass: "addLinkpopup",
    });
  }

  targetTableChange(table) {
    this.clearLink();
    this.sourcetable = table.tableName;
    this.selectedTargetTable = table.tableId;
    this.targetTable = [];
    this.maximizeData["moduleData"].forEach((target) => {
      if (target.tableId == table.tableId)
        this.targetTable = target["erTableData"];
    });
    this.targetTable.forEach((ele) => {
      ele["targetColumnId"] = ele["columnId"];
    });
    this.targetPopup = JSON.parse(JSON.stringify(this.targetTable));
    let data = {
      objectName: table.tableName,
    };
    this.loadPopConnectionvalues(data)
  }
  separateArrayvalues: any = [];
  mainArr = [];
  loadPopConnectionvalues(data) {
    this.apiService
      .post(environment.edwObj_getColumnByTable, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          setTimeout(() => {
            this.targetTable = resp["response"];
            this.targetPopup = JSON.parse(JSON.stringify(this.targetTable));
            let arr = [];
            this.popupd3Links = [];
            this.separateArrayvalues = [];
            this.mainArr = [];
            let holdingarrayvlaues = this.d3Data.links;
            var labelconnectionvalues = holdingarrayvlaues.filter(value => value["source"].tableName === this.sourceTableObj.tableName && value['target'].tableName === data.objectName);
            if (labelconnectionvalues.length) {
              this.separateArrayvalues.push(labelconnectionvalues[0].connectionname);
             
              let values = labelconnectionvalues[0].connectionname;
              arr = values.split("<br>");
              //console.log("values", values);

              arr.forEach((element) => {
                if (element) {
                  let values = element.split("=");
                  let src = values[0];
                  let trg = values[1];
                  // console.log("src", src);
                  // console.log("trg", trg);

                  this.mainArr.push({
                    source: src.split(".")[1].replace(/\s*\,\s*/g, ""),
                    target: trg.split(".")[1].replaceAll(" ", ""),
                  });
                }
              });
            }


            if (this.mainArr.length != 0) {
              this.popupd3LinkConnectionData = [];
              this.sourceTable.forEach((element) => {
                this.mainArr.forEach((element1) => {
                  if (
                    element.columnName.replace(/\s/g, "") ==
                    element1.source.replace(/\s/g, "")
                  ) {
                    element.tranColumnId = element.columnId;
                    element1.tranColumnId = element.columnId;
                  }
                });
              });

              this.targetTable.forEach((checkingelem) => {
                this.mainArr.forEach((element2) => {
                  if (
                    element2.target.replace(/\s/g, "") ==
                    checkingelem.columnName.replace(/\s/g, "")
                  ) {
                    checkingelem.targetColumnId = checkingelem.columnId;
                    element2.targetColumnId = checkingelem.columnId;
                  }
                });
              });

              this.popupd3LinkConnectionData = this.mainArr;
            }

            this.d3DataFormSource();
            this.d3DataFormTarget(true);
          }, 1000);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }

  replaceAll(str, find, replace) {
    let escapedFind = find.replace(/([.*#+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    return str.replace(new RegExp(escapedFind, "g"), replace);
  }
  linedraw() {
    let returnArr = [];
    if (!isNullOrUndefined(this.maximizeData.jsondatarelation)) {
      let relations = this.maximizeData.jsondatarelation.split("|");
      let sourceDistRealtion = [];
      relations.forEach((element, index1) => {
        if (element != "") {
          element = element.replaceAll("'", "");
          let hashremovedvalues = element.replace("#", ",");
          sourceDistRealtion = hashremovedvalues.split(",");

          let sourceRealationString = sourceDistRealtion[2].replaceAll(
            "And",
            "<br>"
          );

          sourceRealationString = sourceRealationString.replaceAll(
            "AND",
            "<br>"
          );
          sourceRealationString = sourceRealationString.replaceAll(
            "and",
            "<br>"
          );
          sourceRealationString = sourceRealationString.replaceAll(
            "OR",
            "<br>"
          );
          sourceRealationString = sourceRealationString.replaceAll(
            "or",
            "<br>"
          );
          sourceRealationString = sourceRealationString.replaceAll(
            "Or",
            "<br>"
          );
          sourceDistRealtion[1] = sourceDistRealtion[1].replaceAll(" ", "");
          this.labelvaluesForConnection = sourceRealationString;

          if (sourceDistRealtion.length) {
            let source = this.maximizeData.moduleData.filter((item) => {
              if (
                item.tableId.toUpperCase() ==
                sourceDistRealtion[0].toUpperCase()
              )
                return item;
            });
            let target = this.maximizeData.moduleData.filter((item) => {
              if (
                item.tableId.toUpperCase() ==
                sourceDistRealtion[1].toUpperCase()
              )
                return item;
            });
            if (!isNullOrUndefined(source[0]) && !isNullOrUndefined(target[0]))
              returnArr.push({
                source: source[0],
                target: target[0],
                connectionname: this.labelvaluesForConnection,
              });
          }
        }
      });
    }
    return returnArr;
  }

  d3LinksDataUpdate(source, target, labelValues) {
    this.d3Data.links.push({
      source: source,
      target: target,
      connectionname: labelValues,
    });
    this.d3UpdateFunction(source);
  }



  saveErDigramChangesForTree(item) {
    let tempArr = this.maximizeData.moduleData
      .map((item) => {
        return item.tableName;
      })
      .join(",");
    let obj = {
      schema: this.maximizeData.schema.toUpperCase(),
      productName: this.maximizeData.productName.toUpperCase(),
      connectionId: this.maximizeData.connectionId.toUpperCase(),
      selecteLanguage: this.maximizeData.selecteLanguage.toUpperCase(),
      moduleId: this.maximizeData.moduleId,
      moduleTables: tempArr,
      screenName: this.maximizeData.screenName,
      jsonDataForActualERData: JSON.stringify(this.maximizeData),
    };

    const api_Modify = environment.addModifyERDiagram;
    this.apiService.post(api_Modify, obj).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.maximizedatapassingFortree();
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  maximizedatapassingFortree() {
    this.childDataForTree.emit({ type: "table", data: this.dragOptiontrigger });
    this.dragOptiontrigger = [];
  }

  zoomFitpan(item?) {
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    if (
      (!isNullOrUndefined(this.tabledrag_IsChanged_X) ||
        !isNullOrUndefined(this.tabledrag_IsChanged_Y)) && (this.tabledrag_IsChanged_X != "" || this.tabledrag_IsChanged_Y != "")
    ) {
      const modRef = this.modalService.open(GenericPopupComponent, {
        size: <any>"md",
        backdrop: false,
        windowClass: "genericPopup",
      });
      modRef.componentInstance.title = "Alert";
      modRef.componentInstance.popupType = "Confirm";
      modRef.componentInstance.message =
        "Do you want to save changes you made?";
      modRef.componentInstance.userConfirmation.subscribe((resp) => {
        if (resp.type == "Yes") {
          setTimeout(() => {
            modRef.close();
            this.saveErDigramChanges(item);
            let obj = {
              table_X: "",
              table_Y: "",
            };

            this.dragOptionIsChanged.emit(obj);
          }, 10);
        }
        if (resp.type == "No") {
          this.maximizedatapassing(item);

          let obj = {
            table_X: "",
            table_Y: "",
          };
          this.dragOptionIsChanged.emit(obj);
          modRef.close();
        }
        modRef.close();
      });
    } else {
      this.maximizedatapassing(item);
    }
  }

  zoomFitpantree(item?) {
    if (
      (!isNullOrUndefined(this.tabledrag_IsChanged_X) ||
        !isNullOrUndefined(this.tabledrag_IsChanged_Y)) && (this.tabledrag_IsChanged_X != "" || this.tabledrag_IsChanged_Y != "")
    ) {
      const modRef = this.modalService.open(GenericPopupComponent, {
        size: <any>"md",
        backdrop: false,
        windowClass: "genericPopup",
      });
      modRef.componentInstance.title = "Alert";
      modRef.componentInstance.popupType = "Confirm";
      modRef.componentInstance.message =
        "Do you want to save changes you made?";
      modRef.componentInstance.userConfirmation.subscribe((resp) => {
        if (resp.type == "Yes") {
          setTimeout(() => {
            modRef.close();
            this.saveErDigramChangesForTree(item);
          }, 10);
        }
        if (resp.type == "No") {
          modRef.close();
          this.maximizedatapassingFortree();
        }
        modRef.close();
      });
    }
    else {
      //this.maximizedatapassingFortree();
      let obj = {
        table_X: "",
        table_Y: "",
      };
      this.dragOptionIsChanged.emit(obj);
    }

  }

  maximizedatapassing(item) {
    if (item) item.maximize = false;

    //this.childDataForTree.emit({ type: "table", data: this.dragOptiontrigger });
    this.dragOptiontrigger = [];

    this.childData.emit({ type: "minimize", data: this.maximizeData });
  }

  saveExportOptions(type) {
    if (!isNullOrUndefined(this.maximizeData.moduleTables)) {
      this.moduleType = "BG";
      this.maximizeData.moduleId = "";
    }

    let obj = {
      applicationTheme: this.globalService.selectedTheme["color"].replaceAll(
        "#",
        ""
      ),
      reportTitle: this.maximizeData.moduleName,
      productName: this.maximizeData.productName,
      moduleId: this.maximizeData.moduleId,
      moduleName: this.maximizeData.moduleName,
      schema: this.maximizeData.schema.toUpperCase(),
      connectionId: this.maximizeData.connectionId.toUpperCase(),
      selecteLanguage: this.maximizeData.selecteLanguage.toUpperCase(),
      statusDesc: this.maximizeData.moduleTables,
      // moduleType: this.moduleType,
      moduleType: this.maximizeData.screenName,
      screenName: this.maximizeData.screenName,
    };

    if (type == "xlsx") {
      this.api_passing = environment.exportDataToExcel;
    }
    if (type == "Html") {
      this.api_passing = environment.exportDataToHtml;
    }
    if (type == "Pdf") {
      this.api_passing = environment.exportDataToPdf;
    }
    if (type == "Script") {
      this.api_passing = environment.exportTableScript;
    }

    let visionId = JSON.parse(
      localStorage.getItem("themeAndLanguage")
    ).visionId;

    this.apiService.fileDownloads(this.api_passing, obj).subscribe(
      (resp) => {
        if (resp["type"] != "application/json") {
          const blob = new Blob([resp], {
            type:
              type == "Html" || "Script"
                ? "application/zip"
                : type == "xlsx"
                  ? "text/xls"
                  : type == "pdf"
                    ? "application/pdf"
                    : "text/pdf",
          });
          const url = window.URL.createObjectURL(blob);
          const link = this.downloadZipLink.nativeElement;

          link.href = url;


          link.download =
            type == "xlsx" ? `${obj.reportTitle}.xlsx` :
              type == "Html" ? `${obj.reportTitle}.zip` :
                type == "Script" ? `${obj.reportTitle}.zip` :
                  type == "Pdf" ? `${obj.reportTitle}.pdf` : '';

          link.click();
          window.URL.revokeObjectURL(url);
        } else {
          this.globalService.showToastr.error("No Records Found");
        }
      },
      (error: any) => {
        if (error.substr(12, 3) == "204") {
          this.globalService.showToastr.error("No Records to Export");
        }
        if (error.substr(12, 3) == "420") {
          this.globalService.showToastr.error("Error Generating Report");
        }
        if (error.substr(12, 3) == "417") {
          this.globalService.showToastr.error(
            "Error while exporting the report"
          );
        }
      }
    );
  }

  saveErDigramChanges(item) {
    let tempArr = this.maximizeData.moduleData
      .map((item) => {
        return item.tableName;
      })
      .join(",");
    let obj = {
      schema: this.maximizeData.schema.toUpperCase(),
      productName: this.maximizeData.productName.toUpperCase(),
      connectionId: this.maximizeData.connectionId.toUpperCase(),
      selecteLanguage: this.maximizeData.selecteLanguage.toUpperCase(),
      moduleId: this.maximizeData.moduleId,
      moduleTables: tempArr,
      screenName: this.maximizeData.screenName,
      jsonDataForActualERData: JSON.stringify(this.maximizeData),
    };
    const api_Modify = environment.addModifyERDiagram;
    this.apiService.post(api_Modify, obj).subscribe((resp) => {
      if (resp["status"] == 1) {
        //  this.maximizedatapassing(item);
        this.dragOptiontrigger = [];
        this.tabledrag_IsChanged_X = "";
        this.tabledrag_IsChanged_Y = "";
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  //svg for all main table loaded <----------------//

  //---------->drag and drop options//

  dropArea(event) {

    this.checkingdropcondition = true;
    let values = event["element"]["data"];
    let moduleName = this.maximizeData.moduleName;
    let moduleId = this.maximizeData.moduleId;
    if (event["element"] != null && values["moduleType"] != "M") {
      let tablename = values.moduleName.replace(/\s*\,\s*/g, "");
      this.maximizeData.moduleData.forEach((element) => {
        if (tablename == element.tableName) {
          this.checkingdropcondition = false;
          this.globalService.showToastr.warning("Table Already Exits");
        }
      });

      if (this.checkingdropcondition) {
        let offSetX = event["event"]["offsetX"];
        let offSetY = event["event"]["offsetY"];
        let X = event["event"]["X"];
        let Y = event["event"]["Y"];
        const size = 5;
        let columnlist;
        if (values.moduleType != "MO") {
          columnlist = values.edwObjColumnsList.slice(0, size);
        } else {
          let columnlist2 = values.children.slice(0, size);
          columnlist2.forEach((element) => {
            element.columnId = element.maker;
            element.columnOrder = element.maker;
            element.columnName = element.moduleName;
          });
          columnlist = columnlist2;
        }

        const pass_list = {
          schema: values.schema,
          cols: 16,
          connectionId: values.connectionId,
          height: 190,
          offSetX: offSetX,
          offSetY: offSetY,
          productName: values.productName,
          rows: 12,
          selecteLanguage: "EN",
          erTableData: columnlist,
          tableId: moduleId + "_" + values.moduleName,
          tableName: values.moduleName,
          width: 175,
          x: X,
          y: Y,
          tableClicked: false,
          maximum: true

        };
        if (pass_list) {
          this.maximizeData.moduleData.push(pass_list);
        }
        let index = this.maximizeData.moduleData.findIndex(
          (x) => x.tableId === pass_list.tableId
        );
        this.d3UpdateFunction(pass_list.tableId);
        this.d3G_Recreate(pass_list.tableId);
        // this.d3DrawFunction(pass_list.tableId);
        this.tick(pass_list.tableId);
        this.dragstart(pass_list, index);
      }
    } else {
      this.globalService.showToastr.warning(
        "Please drag values from another modules."
      );
    }
  }
  //drag and drop options<----------------//

  //---------->adding new column to the tables//

  openTablecolumns(item1) {
    this.maximizedatacolumnvalues = item1.erTableData;
    this.tablecolumns = [];
    let objpassing = {
      verifierName: this.maximizeData.moduleId,
      makerName: item1.tableId,
    };
    let obj = {
      schema: item1.schema.toUpperCase(),
      productName: item1.productName.toUpperCase(),
      connectionId: item1.connectionId.toUpperCase(),
      selecteLanguage: item1.selecteLanguage.toUpperCase(),
      objectName: item1.tableName.toUpperCase(),
      columnName: item1.tableName.toUpperCase(),
    };
    if (obj.schema != null || "") {
      this.apiService
        .post(environment.getTablecolumns, obj)
        .subscribe((resp) => {
          if (resp["status"] == 1) {
            this.tablecolumns = resp["response"];
          }
          if (this.tablecolumns != "") {
            this.openngbModalTable(objpassing);
          }
        });
    }
  }

  openngbModalTable(objpassing) {
    const modelRef = this.modalService.open(TablecolumnlistingComponent, {
      size: "sm",
      windowClass: "modal-table",
      backdrop: "static",
    });
    modelRef.componentInstance.dataToDisplay = this.tablecolumns;
    modelRef.componentInstance.searchData = this.maximizedatacolumnvalues;
    modelRef.componentInstance.objectsids = objpassing;
    modelRef.componentInstance.filterData.subscribe((e) => {
      if (e.flag == "close") {
        if (e.data == undefined) {
          modelRef.close();
          this.d3ToolTipDiv
            .style("opacity", 0)
            .style("left", 0)
            .style("top", 0);
        }
        if (!isNullOrUndefined(e.data)) {
          this.tablelistcolumnselectedvales = e.data;
        }
        if ((!isNullOrUndefined(this.tablelistcolumnselectedvales)) &&
          !this.tablelistcolumnselectedvales) {
          this.maximizeData.moduleData.forEach((element) => {
            if (
              element.tableName.toLowerCase() ==
              this.tablelistcolumnselectedvales[0].tableName.toLowerCase()
            ) {
              element.erTableData = this.tablelistcolumnselectedvales;
              this.d3G_Recreate(element.tableId);
            }
          });
        }
        modelRef.close();
      }
    });
  }
  //---------->adding new column to the tables//

  // ---->start popup connection for relation svg//
  d3DataFormSource() {
    let lengthA = this.sourceTable.length;
    let lengthB = this.targetTable.length;
    let relationSvgHeight = lengthA > lengthB ? lengthA : lengthB;
    // svg height //
    setTimeout(() => {
      let mainDiv = document
        .getElementById("d3addlinkpopup")
        .getBoundingClientRect();
      this.popupsvg = d3
        .select("#d3addlinkpopup")
        .append("svg")
        .attr("width", mainDiv.width)
        .attr("height", relationSvgHeight * 20 + 25);
      this.popupd3RelationSvg = this.popupsvg.append("g");
      d3.selection.prototype.moveUp = function () {
        return this.each(function () {
          this.parentNode.appendChild(this);
        });
      };

      this.popupd3RelationSvg
        .append("defs")
        .append("marker")
        .attr("id", "dominating")
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 10)
        .attr("refY", 0)
        .attr("orient", "auto-start-reverse")
        .attr("markerWidth", 5)
        .attr("markerHeight", 10)
        .attr("class", "test2")
        .append("path")
        .attr("d", "M0,-5L10,0L0,5")
        .attr("fill", "#4285f4")
        .on("click", (d) => this.onClick(d));

      this.popupd3RelationSvg
        .append("defs")
        .append("marker")
        .attr("id", "dot")
        .attr("viewBox", [0, 0, 20, 20])
        .attr("refX", 10)
        .attr("refY", 10)
        .attr("markerWidth", 5)
        .attr("markerHeight", 5)
        .append("circle")
        .attr("cx", 10)
        .attr("cy", 10)
        .attr("r", 10)
        .style("fill", "#4285f4");

      this.popupd3ForeignObject = this.popupd3RelationSvg
        .append("foreignObject")
        .attr("x", 0)
        .attr("y", -5)
        .attr("class", "icon_block");

      this.popupd3ForeignObject
        .append("xhtml:div")
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("class", "connection_icon-top")
        .on("click", () => this.autoConnect())
        .append("i")
        .attr("class", "fa fa-link")
        .attr("aria-hidden", "true")
        .on("mouseover", (d) => {
          this.d3ToolTipDiv
            .html("Auto Connect")
            .style("opacity", 0.9)
            .style("left", d3.event.pageX - 44 + "px")
            .style("top", d3.event.pageY - 40 + "px");
        })
        .on("mouseout", (d) => {
          this.d3ToolTipDiv
            .style("opacity", 0)
            .style("left", 0)
            .style("top", 0);
        });
      //------------------888----------------
      this.popupd3ForeignObject = this.popupd3RelationSvg
        .append("foreignObject")
        .attr("x", 20)
        .attr("y", -5)
        .attr("class", "icon_block");

      this.popupd3ForeignObject
        .append("xhtml:div")
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("class", "connection_icon-top")
        .on("click", () => this.deleteLink())
        .append("i")
        .attr("class", "fa fa-trash-o")
        .attr("aria-hidden", "true")
        .on("mouseover", (d) => {
          this.d3ToolTipDiv
            .html("Delete All")
            .style("opacity", 0.9)
            .style("left", d3.event.pageX - 34 + "px")
            .style("top", d3.event.pageY - 40 + "px");
        })
        .on("mouseout", (d) => {
          this.d3ToolTipDiv
            .style("opacity", 0)
            .style("left", 0)
            .style("top", 0);
        });

      this.popupd3RelationSvg
        .append("text")
        .attr("class", "relationHeader")
        .attr("x", 40)
        .attr("y", 10)
        .attr("dx", 45)
        .text("Source Column(s):");

      this.popupd3RelationSvg
        .append("rect")
        .attr("x", 0)
        .attr("y", 20)
        .attr("width", 300)
        .attr("height", lengthA * 20)
        .attr("stroke", "#ccc")
        .attr("fill", "#fff");

      if (this.sourceTable.length) {
        this.sourceTable.forEach((element, i) => {
          element.lineX = 0;
          element.lineY = i * 20 + 40;
        });
      }

      this.popupd3RelationSvg
        .selectAll(`.textSource`)
        .data(this.sourceTable)
        .enter()
        .append("text")
        .attr("id", (d) => {
          return `subnode${d.columnId}`;
        })
        .attr("x", (d, i) => {
          return d.lineX;
        })
        .attr("y", (d, i) => {
          return d.lineY - 5;
        })
        .attr("dx", 30)
        .text((d: any) => {
          return d.columnName;
        })
        .attr("class", "subnode");

      this.popupd3RelationSvg
        .selectAll(`.textSource`)
        .data(this.sourceTable)
        .enter()
        .append("foreignObject")
        .attr("x", (d, i) => {
          return d.lineX + 270;
        })
        .attr("y", (d, i) => {
          return d.lineY - 22;
        })
        .attr("class", "icon_block")
        .append("xhtml:div")
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("class", "connection_icon")
        .on("click", (d, i) => this.popupDivCall(d, i))
        .append("i")
        .attr("class", "fa fa-link")
        .attr("aria-hidden", "true")
        .on("mouseover", (d) => {
          this.d3ToolTipDiv
            .html("Link")
            .style("opacity", 0.9)
            .style("left", d3.event.pageX - 17 + "px")
            .style("top", d3.event.pageY - 40 + "px");
        })
        .on("mouseout", (d) => {
          this.d3ToolTipDiv
            .style("opacity", 0)
            .style("left", 0)
            .style("top", 0);
        });

      this.popupd3RelationSvg
        .selectAll(`.bottomLine`)
        .data(this.sourceTable)
        .enter()
        .append("line")

        .attr("class", (d, i) => {
          if (i != this.sourceTable.length - 1) return "bottomLine";
          else return "";
        })
        .attr("x1", (d: any) => {
          return d.lineX;
        })
        .attr("y1", (d: any) => {
          return d.lineY;
        })
        .attr("x2", (d: any) => {
          return d.lineX + 300;
        })
        .attr("y2", (d: any) => {
          return d.lineY;
        });
    });
  }

  d3DataFormTarget(flag) {

    let lengthA = this.sourceTable.length;
    let lengthB = this.targetTable.length;
    let relationSvgHeight = lengthA > lengthB ? lengthA : lengthB;

    setTimeout(() => {
      d3.select("#d3addlinkpopup")
        .select("svg")
        .attr("height", relationSvgHeight * 20 + 25);

      this.popupd3RelationSvg
        .append("text")
        .attr("class", "relationHeader")
        .attr("x", 500)
        .attr("y", 10)
        .attr("dx", 45)
        .text("Target Column(s):");

      this.popupd3RelationSvg
        .append("rect")
        .attr("x", 462)
        .attr("y", 20)
        .attr("width", 300)
        .attr("height", lengthB * 20)
        .attr("stroke", "#ccc")
        .attr("fill", "#fff");

      if (this.targetTable.length) {
        this.targetTable.forEach((element, i) => {
          element.lineX = 460;
          element.lineY = i * 20 + 40;
        });
      }

      this.popupd3RelationSvg
        .selectAll(`.textSource`)
        .data(this.targetTable)
        .enter()
        .append("text")
        .attr("x", (d, i) => {
          return d.lineX;
        })
        .attr("y", (d, i) => {
          return d.lineY - 5;
        })
        .attr("dx", 30)
        .text((d: any) => {
          return d.columnName;
        })
        .attr("class", "subnode");

      this.popupd3RelationSvg
        .selectAll(`.bottomLine1`)
        .data(this.targetTable)
        .enter()
        .append("line")
        .attr("class", (d, i) => {
          if (i != this.targetTable.length - 1) return "bottomLine1";
          else return "";
        })
        .attr("x1", (d: any) => {
          return d.lineX;
        })
        .attr("y1", (d: any) => {
          return d.lineY;
        })
        .attr("x2", (d: any) => {
          return d.lineX + 300;
        })
        .attr("y2", (d: any) => {
          return d.lineY;
        });

      flag ? this.d3ConnectionDraw() : "";
    });
  }

  d3ConnectionDraw() {
    this.popupd3LinkConnectionData.forEach((link) => {
      let source = this.sourceTable.filter(
        (f) => link.tranColumnId === f.tranColumnId
      );
      let target = this.targetTable.filter(
        (f) => link.targetColumnId === f.targetColumnId
      );
      if (source.length && target.length) {
        this.popupd3Links.push({
          source: source[0],
          target: target[0],
          connectionName: `${source[0].columnName} -
          ${target[0].columnName}`,
          connectionsourcename: source[0].columnName,
          connectiontargetname: target[0].columnName,
        });
      }
    });
    //-----------D3Links------------------
    this.popupd3RelationSvg
      .selectAll(`.link`)
      .data(this.popupd3Links)
      .enter()
      .append("line")
      .attr("class", "link")
      .attr("id", "move-up")
      .attr("marker-end", "url(#dominating)")
      .attr("marker-start", "url(#dot)")
      .attr("x1", (d: any) => {
        return d.source.lineX + 300;
      })
      .attr("y1", (d: any) => {
        return d.source.lineY - 10;
      })
      .attr("x2", (d: any) => {
        return d.target.lineX;
      })
      .attr("y2", (d: any) => {
        return d.target.lineY - 10;
      })

      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html(d.connectionName)
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", (d) => {
        this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
      })
      .on("click", (d) => this.onClick(d));

    this.popupd3RelationSvg.moveUp();
  }

  onClick(details) {
    const indexOfObject = this.popupd3LinkConnectionData.findIndex((object) => {
      return (
        object.tranColumnId == details.source["tranColumnId"] &&
        object.targetColumnId == details.target["targetColumnId"]
      );
    });

    this.popupd3LinkConnectionData.splice(indexOfObject, 1);

    this.popupd3Links = [];
    this.popupsvg.remove();
    this.d3DataFormSource();
    this.d3DataFormTarget(true);
  }

  autoConnect() {
    this.showConnector = !this.showConnector;
    this.targetFlag = false;
  }

  deleteLink() {
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "Do you want to delete all links ?";
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.popupd3LinkConnectionData = [];
        this.popupconnectedSources = [];
        this.popupconnectedTargets = [];
        this.popupd3Links = [];
        // this.d3Connect('')
        this.popupsvg ? this.popupsvg.remove() : "";
        this.d3DataFormSource();
        this.d3DataFormTarget(true);
        this.clearLink();
      } else {
        modelRef.close();
      }
      modelRef.close();
    });
  }

  d3Connect(data) {
   
    //Remove all connections only
    this.popupd3LinkConnectionData = [];

    this.sourceTable.forEach((source) => {
      source["tranColumnId"] = source["columnId"];

      if (!this.popupconnectedSources.includes(source.columnId)) {
        this.targetTable.forEach((target) => {
          if (data == "Connect by name") {
          
            if (source.columnName == target.columnName) {
              // console.log("source.columnName",source.columnName);
              // console.log("target.columnName",target.columnName);
              // console.log("source.columnName",source.columnId);
              // console.log("target.columnName",target.columnId);
              if (!this.popupconnectedTargets.includes(target.targetColumnId))
                this.popupd3LinkConnectionData.push({
                  tranColumnId: source["tranColumnId"],
                  targetColumnId: target["targetColumnId"],
                  checked: true,
                });
            }
          } else if (data == "Connect by position") {
            // console.log("source.columnName",source.columnId);
            // console.log("target.columnName",target.columnId);
            if (!this.popupconnectedSources.includes(source.columnId)) {
              if (source.columnId == target.columnId) {
                if (!this.popupconnectedTargets.includes(target.targetColumnId))
                  this.popupd3LinkConnectionData.push({
                    tranColumnId: source["tranColumnId"],
                    targetColumnId: target["targetColumnId"],
                    checked: true,
                  });
              }
            }
          }
        });
      }
    });

    // Restore the original connections
    this.popupd3LinkConnectionData = [
      ...this.popupd3LinkConnectionData,
      ...this.popupconnectedSources.map((sColumnId, i) => ({
        tranColumnId: sColumnId,
        targetColumnId: this.popupconnectedTargets[i].toString(),
      })),
    ];

    this.currentTran = data;
    this.popupd3Links = [];
    this.popupsvg.remove();
    this.d3DataFormSource();
    this.d3DataFormTarget(true);
  }

  popupDivCall(item, idx) {
    this.targetPopup.forEach((ele) => (ele["showColumn"] = true));
    this.targetFlag = true;
    this.closeD3Connector();
    this.currentTran = item;

    let tempArr = this.popupd3LinkConnectionData
      .filter((e) => e.tranColumnId == this.currentTran.tranColumnId)
      .map((e) => e.targetColumnId);

    this.targetPopup.forEach((column) => {
      if (tempArr.includes(column.targetColumnId)) {
        column["checked"] = true;
      } else {
        column["checked"] = false;
      }
      let checkConnection = this.popupd3LinkConnectionData.map(
        (e) => e.targetColumnId
      );
      if (checkConnection.includes(column.targetColumnId) && !column.checked)
        column["showColumn"] = false;
    });

    // Sorting
    this.targetPopup.sort((f, s) => {
      if (f.checked) return -1;
      else return 1;
    });
  }

  saveRelationConnection() {
    this.popupd3LinkConnectionData = [];
    this.popupd3LinkConnectionData = this.popupd3LinkConnectionData.filter(
      (item) => item.tranColumnId != this.currentTran.tranColumnId
    );

    this.targetPopup.forEach((column) => {
      if (column["checked"]) {
        this.arrayTemporary.push({
          tranColumnId: this.currentTran.tranColumnId,
          targetColumnId: column["targetColumnId"],
        });
      }
    });
    this.popupd3LinkConnectionData = this.popupd3LinkConnectionData.concat(
      this.arrayTemporary
    );
    this.popupconnectedSources = this.popupd3LinkConnectionData.map(
      (i) => i.tranColumnId
    );
    this.popupconnectedTargets = this.popupd3LinkConnectionData.map(
      (i) => i.targetColumnId
    );
    this.popupsvg.remove();
    this.d3DataFormSource();
    this.d3DataFormTarget(true);
    this.closePopupD3();
  }

  saveAllRelationConnection() {
    if (
      this.tempArrayVariable.length == 0 &&
      this.popupd3LinkConnectionData.length == 0
    ) {
      this.globalService.showToastr.warning("Please Select Target Column");
    }
    if (this.arrayTemporary.length === 0) {
      this.tempArrayVariable.push(this.popupd3LinkConnectionData);
      this.arrayTemporary = this.tempArrayVariable[0];
    }
    this.arrayTemporary.forEach((element) => {
      this.targetPopup.forEach((element2) => {
        if (element.targetColumnId == element2.columnId) {
          element2.checked = true;
        }
      });
    });
    if (this.arrayTemporary.length > 0) {
      let source = this.sourceTableObj["tableId"];
      let target = this.selectedTargetTable;
      let sourcepasssing;
      let targetpasssing;
      this.maximizeData.moduleData.forEach((element) => {
        if (element.tableId == source) {
          sourcepasssing = element;
        }
        if (element.tableId == target) {
          targetpasssing = element;
        }
      });

      this.popupd3Links.forEach((column) => {
        this.realtionString.push({
          tranColumnId:
            this.sourceTableObj["tableName"] +
            "." +
            column.connectionsourcename +
            "=" +
            this.sourcetable +
            "." +
            column.connectiontargetname,
        });
        this.valuesForLabel = this.realtionString
          .map((item) => {
            return item.tranColumnId;
          })
          .join(" AND ");
      });

      this.valuesForLabel = this.realtionString
        .map((item) => {
          return item.tranColumnId;
        })
        .join(" AND ");

      let obj = {
        schema: this.maximizeData.schema.toUpperCase(),
        productName: this.maximizeData.productName.toUpperCase(),
        connectionId: this.maximizeData.connectionId.toUpperCase(),
        selecteLanguage: this.maximizeData.selecteLanguage.toUpperCase(),
        verifierName: this.maximizeData.moduleId,
        tableName: this.sourceTableObj["tableName"],
        relationTablename: this.sourcetable,
        relationName: "key",
        relationString: this.valuesForLabel,
      };
      const api_Modify = environment.updateRelations;
      this.apiService.post(api_Modify, obj).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.realtionString = [];
          this.globalService.showToastr.success(resp["message"]);
          this.modalClose();
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
      this.d3LinksDataUpdate(
        sourcepasssing,
        targetpasssing,
        this.valuesForLabel
      );
    }
  }

  closePopupD3() {
    this.targetFlag = false;
  }

  closeD3Connector() {
    this.showConnector = false;
  }

  searchTargetLst(name) {
    this.targetPopup.forEach((target) => {
      if (this.popupconnectedTargets == target["targetColumnId"])
        target.showColumn = false;
      else target.showColumn = true;
    });
    this.targetPopup.forEach((target) => {
      let element = target.columnName.toLowerCase();
      if (!element.includes(name.toLowerCase()) && target.showColumn)
        target.showColumn = false;
    });
  }

  clearLink() {
    this.tempArrayVariable = [];
    this.arrayTemporary = [];
    this.popupd3LinkConnectionData = [];
    this.popupconnectedSources = [];
    this.popupconnectedTargets = [];
    this.popupd3Links = [];
    this.popupd3RelationSvg = [];
    this.popupsvg ? this.popupsvg.remove() : "";
  }

  checkRecords() {
    return this.targetPopup.every((e) => !e.showColumn);
  }

  modalClose() {
    this.d3ToolTipDiv.style("opacity", 0).style("left", 0).style("top", 0);
    this.clearLink();
    this.targetTableList = [];
    this.modalService.dismissAll();
  }

  //ended popup connection
  zoomIn() {
    this.svgMain.select("g").transition().call(this.d3Zoom.scaleBy, 1.2);
  }
  zoomOut() {
    this.svgMain.select("g").transition().call(this.d3Zoom.scaleBy, 0.8);
  }
  zoomReset() {
    this.svgMain.select("g").transition().call(this.d3Zoom.scaleTo, 1);
  }

  minimizeAll() {
    this.showMinimize = true;
    this.showMaximize = false;
    this.maximizeData.moduleData.forEach((ele) => {
      if (ele.maximum == 1)
        this.minimize(ele);
    });
  }
  maximizeAll() {
    this.showMinimize = false;
    this.showMaximize = true;
    this.maximizeData.moduleData.forEach((ele) => {
      if (ele.maximum == 0) this.maxmize(ele);

    });
  }

  minimizeBtnDisplay() { }

  fullScreen() {
    this.fullScreenDisp
      ? this.goOutFullscreen()
      : this.goFulScreen(document.getElementById("d3FullScreen"));
  }

  goOutFullscreen() {
    let sidebar = document.getElementById("max_sidebar");
    let navbar = document.getElementById("max_nav");
    let mainDiv = document.getElementById("max_parent");
    let mainPanel = document.getElementById("mainPanel");
    let wh20 = document.getElementById("w-h-20");
    let d3FullScreen = document.getElementById("d3FullScreen");
    let slide = document.getElementById("slide_open");

    sidebar.classList.remove("max_sidebar_full");
    navbar.classList.remove("max_nav_full");
    mainDiv.classList.remove("max_parent_full", "maximize-view");
    wh20.classList.remove("w-h-20_full");
    mainPanel.classList.remove("mainPanel_full", "maximize-pannel");
    d3FullScreen.classList.remove("d3FullScreen_full");
    slide.classList.remove("d-none");
    if (window.innerHeight == screen.height) {
      if (this.document1.exitFullscreen) {
        this.document1.exitFullscreen();
      } else if (this.document1.mozCancelFullScreen) {
        this.document1.mozCancelFullScreen();
      } else if (this.document1.webkitExitFullscreen) {
        this.document1.webkitExitFullscreen();
      } else if (this.document1.msExitFullscreen) {
        this.document1.msExitFullscreen();
      }
    }
    this.fullScreenDisp = false;
    this.ngAfterViewInit();
  }

  goFulScreen = (element) => {
    let sidebar = document.getElementById("max_sidebar");
    let navbar = document.getElementById("max_nav");
    let mainDiv = document.getElementById("max_parent");
    let mainPanel = document.getElementById("mainPanel");
    let wh20 = document.getElementById("w-h-20");
    let d3FullScreen = document.getElementById("d3FullScreen");
    let slide = document.getElementById("slide_open");

    sidebar.classList.add("max_sidebar_full");
    navbar.classList.add("max_nav_full");
    mainDiv.classList.add("max_parent_full", "maximize-view");
    wh20.classList.add("w-h-20_full");
    mainPanel.classList.add("mainPanel_full", "maximize-pannel");
    d3FullScreen.classList.add("d3FullScreen_full");
    slide.classList.add("d-none");
    // mainDiv.classList.add('maximize-view');
    // mainPanel.classList.add('maximize-pannel');

    this.fullScreenDisp = true;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    }
    this.ngAfterViewInit();
    // this.fullHeight = false;
    // this.togglePanelHeight("columns-area")
  };

  @HostListener("document:click", ["$event"])
  @HostListener("document:touchstart", ["$event"])
  handleOutsideClick(event) {
    if (event.target.closest(".insideBoxScroll")) {
      this.showiconbar = false;
    }
  }

  opendropdownMenu() {
    this.showiconbar = true;
  }

  targetTableChangeRelation(target) {
    this.targetPopup.forEach((element) => {
      if (element.columnId != target.columnId) {
        element.checked = false;
      }
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }
}
//-----------> old functionalities
//this.dragOptionAdd();
//this.jqPlumbStart();
//declare let jsPlumb: any;
// zoomoverall() {
//   let zoom = d3.zoom().scaleExtent([0.1, 5]).on("zoom", zoomed);
//   let root = d3.selectAll(".zoomoutsideRootDiv").call(zoom);
//   let canvas = d3.selectAll(".gridster-body");
//   function zoomed() {
//     let transform = d3.event.transform;
//     setTimeout(() => {
//       (<any>window).jsp.repaintEverything();
//     }, 1);
//     canvas
//       .style(
//         "transform",
//         "translate(" +
//           transform.x +
//           "px," +
//           transform.y +
//           "px) scale(" +
//           transform.k +
//           ")"
//       )
//       .style("transform-origin", "0 0");
//   }
// }

// zoominside(event, values) {
//   let element = document.getElementById(values.id);
//   element.classList.add("zoomsubRootDiv");
//   let zoom = d3.zoom().scaleExtent([0.1, 5]).on("zoom", zoomed);
//   let root = d3
//     .selectAll(".zoomRootDiv")
//     .call(zoom)
//     .on("wheel", function () {
//       d3.event.preventDefault();
//     });
//   let canvas = d3.selectAll(".zoomsubRootDiv");
//   function zoomed() {
//     let transform = d3.event.transform;
//     canvas
//       .style(
//         "transform",
//         "translate(" +
//           transform.x +
//           "px," +
//           transform.y +
//           "px) scale(" +
//           transform.k +
//           ")"
//       )
//       .style("transform-origin", "0 0");
//   }
// }

// jqPlumbStart() {
//   setTimeout(() => {
//     this.firstInstance = jsPlumb.getInstance({
//       Connector: "Straight",
//       PaintStyle: {
//         strokeWidth: 2,
//         stroke: "#ccc",
//         fill: "none",
//       },
//       Endpoint: [
//         "Rectangle",
//         {
//           width: 5,
//           height: 5,
//           enabled: false,
//         },
//       ],
//       EndpointStyle: {
//         fill: "#ffa500",
//       },
//       Container: "canvas",
//       ConnectionOverlays: [
//         [
//           "Arrow",
//           {
//             location: 1,
//             width: 11,
//             length: 11,
//           },
//         ],
//       ],
//     });
//     (<any>window).jsp = this.firstInstance;
//     (<any>window).jsp.setContainer("canvas");

//     this.makeSourceTarget();
//     setTimeout(() => {
//       // this.startConnection();
//     }, 10);
//   }, 500);
// }

// makeSourceTarget() {
//   let moduleId = "panZoomEle";
//   const lists = jsPlumb.getSelector(
//     "#" + moduleId + " .widthgridster span i"
//   );

//   if (lists.length) {
//     for (let l = 0; l < lists.length; l++) {
//       const isSource = lists[l].getAttribute("source") != null;
//       if (isSource) {
//         (<any>window).jsp.makeTarget(lists[l], {
//           allowLoopback: true,
//           maxConnections: 1,
//           anchor: ["Left", "Right"],
//         });
//       }
//     }
//   }
//   const outList = jsPlumb.getSelector(
//     "#" + moduleId + " .widthgridster span i"
//   );
//   if (outList.length) {
//     for (let l = 0; l < outList.length; l++) {
//       const isTarget = outList[l].getAttribute("target") != null;
//       if (isTarget) {
//         (<any>window).jsp.makeSource(outList[l], {
//           anchor: ["Left", "Right"],
//         });
//       }
//     }
//   }
// }

// startConnection() {
//   if (!isNullOrUndefined(this.maximizeData.jsondatarelation)) {
//     let relations = this.maximizeData.jsondatarelation.split("|");
//     relations.forEach((element, index1) => {
//       if (element != "") {
//         //element = this.replaceAll(element, "'", "");
//         element = element.replaceAll("'", "");
//         let hashremovedvalues = element.replace("#", ",");
//         let sourceDistRealtion = hashremovedvalues.split(",");
//         let sourceRealationString = sourceDistRealtion[2].replaceAll(
//           "And",
//           "<br>"
//         );
//         sourceRealationString = sourceRealationString.replaceAll(
//           "AND",
//           "<br>"
//         );
//         sourceRealationString = sourceRealationString.replaceAll(
//           "and",
//           "<br>"
//         );
//         sourceRealationString = sourceRealationString.replaceAll(
//           "OR",
//           "<br>"
//         );
//         sourceRealationString = sourceRealationString.replaceAll(
//           "or",
//           "<br>"
//         );
//         sourceRealationString = sourceRealationString.replaceAll(
//           "Or",
//           "<br>"
//         );
//         sourceDistRealtion[1] = sourceDistRealtion[1].replaceAll(" ", "");
//         this.connectionNode(
//           `R-_-${sourceDistRealtion[0]}`,
//           `L-_-${sourceDistRealtion[1]}`,
//           sourceRealationString
//         );
//       }
//     });
//   }
// }

// connectionNode(source, target, label) {
//   let src = document.getElementById($("[data-id=" + source + "]").attr("id"));
//   let tgt = document.getElementById($("[data-id=" + target + "]").attr("id"));

//   if (
//     !isNullOrUndefined(src) &&
//     !isNullOrUndefined(tgt) &&
//     !isNullOrUndefined(label)
//   )
//     this.jsPlumbConnectionFn(src, tgt, label);
// }

// jsPlumbConnectionFn(source, target, label) {
//   (<any>window).jsp.connect({
//     source: source,
//     target: target,
//     scope: "someScope",
//     cssClass: "source-endpoint",
//     detachable: false,
//     reattach: false,
//     hoverPaintStyle: { stroke: "#000" },
//     overlays: [
//       [
//         "Label",
//         {
//           label: label,
//           id: "label_" + source + target,
//           cssClass: "edgeLabel",
//         },
//       ],
//     ],
//   });
// }

// endpointClick(id, values) {
//   let connections = [];
//   $.each(this.firstInstance.getConnections(), function (idx, connection) {
//     connections.push({
//       connectionId: connection.id,
//       pageSourceId: connection.sourceId,
//       pageTargetId: connection.targetId.replace("L-_-", ""),
//     });

//   });

//   connections.forEach((element) => {
//     if (element.pageTargetId == id) {
//       this.tablesname.push({
//         objectName: element.pageTargetId,
//       });
//       this.tablesname.push({
//         objectName: element.pageSourceId,
//       });
//     }
//   });

//   let obj;
//   for (let i = 0; i < this.tablesname.length; i++) {
//     obj = {
//       schema: values.schema.toUpperCase(),
//       productName: values.productName.toUpperCase(),
//       connectionId: values.connectionId.toUpperCase(),
//       selecteLanguage: values.selecteLanguage.toUpperCase(),
//       objectName: this.tablesname[i].objectName.toUpperCase(),
//     };

//     if (obj.schema != null || "") {
//       this.apiService
//         .post(environment.getTablecolumns, obj)
//         .subscribe((resp) => {
//           if (resp["status"] == 1) {
//             this.tablecolumnsarrayone = resp["response"];
//             this.tablesname[i].tablevalues = resp["response"];
//           }
//           if (!isNullOrUndefined(this.tablesname) && i == 1) {
//             this.openngbModalTablerelationmappings(this.tablesname);
//           }
//         });
//     }
//   }
// }

// openngbModalTablerelationmappings(objpassing) {
//   const modelRef = this.modalService.open(TablecolumnmappingComponent, {
//     size: "md",
//     backdrop: "static",
//   });
//   modelRef.componentInstance.objectsids = objpassing;
//   modelRef.componentInstance.filterData.subscribe((e) => {
//     if (e.flag == "close") {
//       if (e.data == undefined) {
//         modelRef.close();
//         this.d3ToolTipDiv
//           .style("opacity", 0)
//           .style("left", 0)
//           .style("top", 0);
//       }

//       modelRef.close();
//     }
//   });
// }

// dragOptionAdd() {
//   this.options["draggable"]["start"] = (event, $element, widget) => {
//     let obj = JSON.parse(JSON.stringify(event));
//     this.curentPosition["x"] = obj["x"];
//     this.curentPosition["y"] = obj["y"];
//   };
//   this.options["draggable"]["stop"] = (event, $element, widget) => {
//     setTimeout(() => {
//       if (
//         event.x != this.curentPosition.x ||
//         event.y != this.curentPosition.y
//       ) {
//         setTimeout(() => {
//           (<any>window).jsp.repaintEverything();
//         }, 1);
//       }
//     }, 1);
//   };
//   this.options["resizable"]["stop"] = (event, $element, widget) => {
//     setTimeout(() => {
//       (<any>window).jsp.repaintEverything();
//     }, 1);
//   };
//   this.options["resizable"]["start"] = (event, $element, widget) => {};
// }
// setDropId(values) {
//   this.options["draggable"]["start"] = (event, $element, widget) => {
//     let obj = JSON.parse(JSON.stringify(event));
//     this.curentPosition["x"] = obj["x"];
//     this.curentPosition["y"] = obj["y"];
//   };
// }
// changedOptions() {
//   if (this.options.api && this.options.api.optionsChanged) {
//     this.options.api.optionsChanged();
//   }
// }

// clickTable(item1) {
//   this.maximizeData.moduleData.forEach((element) => {
//     element["tableClicked"] = false;
//   });
//   item1["tableClicked"] = true;
// }

// tableDrag() {
//   this.options["gridType"] = GridType.Fixed;
//   this.options["compactType"] = CompactType.None;
//   this.options["displayGrid"] = DisplayGrid.Always;
//   this.options["draggable"]["start"] = (event, $element, widget) => {
//     let obj = JSON.parse(JSON.stringify(event));
//     this.curentPosition["x"] = obj["x"];
//     this.curentPosition["y"] = obj["y"];
//   };
// }

//-----------> old functionalities