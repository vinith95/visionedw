import { CompactType, DisplayGrid, GridType } from 'angular-gridster2';
export const gridsterConfigVar = {
  gridType: GridType.Fixed,
  compactType: CompactType.None,
  margin: 1,
  outerMargin: true,
  outerMarginTop: null,
  outerMarginRight: null,
  outerMarginBottom: null,
  outerMarginLeft: null,
  mobileBreakpoint: 640,
  minCols: 1,
  maxCols: 2500,
  minRows: 1,
  maxRows: 2500,
  maxItemCols: 50,
  minItemCols: 1,
  maxItemRows: 50,
  minItemRows: 1,
  maxItemArea: 2500,
  minItemArea: 1,
  defaultItemCols: 1,
  defaultItemRows: 1,
  fixedColWidth: 10,
  fixedRowHeight: 10,
  keepFixedHeightInMobile: false,
  keepFixedWidthInMobile: false,
  scrollSensitivity: 10,
  scrollSpeed: 20,
  enableEmptyCellClick: false,
  enableEmptyCellContextMenu: false,
  enableEmptyCellDrop: false,
  enableEmptyCellDrag: false,
  emptyCellDragMaxCols: 50,
  emptyCellDragMaxRows: 50,
  ignoreMarginInRow: false,
  useTransformPositioning: false,
  draggable: {
    enabled: true,
  },
  resizable: {
    enabled: true,
    handles: {
      s: false,
      e: false,
      n: false,
      w: false,
      se: true,
      ne: true,
      sw: true,
      nw: true
    }
  },
  swap: true,
  pushItems: true,
  disablePushOnDrag: false,
  disablePushOnResize: false,
  pushDirections: {
    north: true,
    east: true,
    south: true,
    west: true
  },
  pushResizeItems: false,
  displayGrid: DisplayGrid.None,
  disableWindowResize: false,
  disableWarnings: false,
  scrollToNewItems: false
};

export const dummy1 =[{ "schema ": "DEVUSER ", "borderColor ": " ", "backgroundcolur": "#F0F8FF","showMiniview ":true, "pulsButton ":true, "data ":{}, "moduleName ": "Admin ", "index ":0, "resizeEnabled ":false, "rows ":25, "moduleData ":[{ "schema ": "Devuser ", "offSetY ":10, "offSetX ":10, "tableClicked ":false, "tableData ":[{ "columnId ":1, "dataType ": "NUMBER ", "columnName ": "Alpha_Tab "},{ "columnId ":2, "dataType ": "VARCHAR ", "columnName ": "Alpha_Subtab_Description "},{ "columnId ":3, "dataType ": "NUMBER ", "columnName ": "Alpha_Subtab "}], "rows ":12, "productName ": "Edwadmin ", "tableName ": "Alpha_Sub_Tab ", "selecteLanguage ": "EN ", "width ":175, "x ":1, "tableId ": "Admin_Alpha_Sub_Tab ", "connectionId ": "Devuser ", "y ":1, "cols ":16, "height ":190},{ "schema ": "Devuser ", "offSetY ":210, "offSetX ":10, "tableClicked ":false, "tableData ":[{ "columnId ":1, "dataType ": "VARCHAR ", "columnName ": "Alpha_Tab "},{ "columnId ":2, "dataType ": "VARCHAR ", "columnName ": "Read_Only "}], "rows ":12, "productName ": "Edwadmin ", "tableName ": "Alpha_Tab ", "selecteLanguage ": "EN ", "width ":175, "x ":1, "tableId ": "Admin_Alpha_Tab ", "connectionId ": "Devuser ", "y ":21, "cols ":16, "height ":190},{ "schema ": "Devuser ", "offSetY ":410, "offSetX ":10, "tableClicked ":false, "tableData ":[{ "columnId ":1, "dataType ": "VARCHAR ", "columnName ": "User_Group "},{ "columnId ":2, "dataType ": "VARCHAR ", "columnName ": "User_Profile "},{ "columnId ":3, "dataType ": "NUMBER ", "columnName ": "Menu_Group "}], "rows ":12, "productName ": "Edwadmin ", "tableName ": "Edw_App_Access_Ctrl ", "selecteLanguage ": "EN ", "width ":175, "x ":1, "tableId ": "Admin_Edw_App_Access_Ctrl ", "connectionId ": "Devuser ", "y ":41, "cols ":16, "height ":190},{ "schema ": "Devuser ", "offSetY ":610, "offSetX ":10, "tableClicked ":false, "tableData ":[{ "columnId ":1, "dataType ": "VARCHAR ", "columnName ": "Country "},{ "columnId ":2, "dataType ": "VARCHAR ", "columnName ": "Le_Book "},{ "columnId ":3, "dataType ": "VARCHAR ", "columnName ": "Branch "},{ "columnId ":4, "dataType ": "DATE ", "columnName ": "Holiday_Date "}], "rows ":12, "productName ": "Edwadmin ", "tableName ": "Edw_Holiday ", "selecteLanguage ": "EN ", "width ":175, "x ":1, "tableId ": "Admin_Edw_Holiday ", "connectionId ": "Devuser ", "y ":61, "cols ":16, "height ":190},{ "schema ": "Devuser ", "offSetY ":810, "offSetX ":10, "tableClicked ":false, "tableData ":[{ "columnId ":1, "dataType ": "VARCHAR ", "columnName ": "Menu_Program "},{ "columnId ":2, "dataType ": "NUMBER ", "columnName ": "Menu_Group "}], "rows ":12, "productName ": "Edwadmin ", "tableName ": "Edw_Menu ", "selecteLanguage ": "EN ", "width ":175, "x ":1, "tableId ": "Admin_Edw_Menu ", "connectionId ": "Devuser ", "y ":81, "cols ":16, "height ":190}] }]

export const dummyJson = [
  {
    cols: 25,
    rows: 25,
    x: 2,
    y: 2,

    size: "md",
    dragEnabled: true,
    pulsButton: true,
    showMiniview: true,
    borderColor: '',
    listflag: true,
    backgroundcolur: "#F0F8FF",
    placeholderData: [],
    data: {},
    moduleId: 1,
    moduleName: "FRL",
    moduleData: [
      {
        tableId: 1,
        tableName: "Frl_Lines",
        offSetX: 500,
        offSetY: 150,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 15,
        x: 50,
        tableData: [
          {
            columnId: 1,
            columnName: "Frl_Line",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 2,
        tableName: "Frl_Balance_Types",
        offSetX: 200,
        offSetY: 310,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 31,
        x: 20,
        tableData: [
          {
            columnId: 1,
            columnName: "Frl_Line",
            dataType: "Varchar2"
          },
          {
            columnId: 2,
            columnName: "Bal_Type",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 3,
        tableName: "Frl_Calculations",
        offSetX: 550,
        offSetY: 310,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 31,
        x: 55,
        tableData: [
          {
            columnId: 1,
            columnName: "Frl_Line",
            dataType: "Varchar2"
          },
          {
            columnId: 2,
            columnName: "Bal_Type",
            dataType: "Number"
          },
          {
            columnId: 3,
            columnName: "Frl_Sequence",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 4,
        tableName: "Frl_Attribute_Levels",
        offSetX: 10,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 1,
        tableData: [
          {
            columnId: 1,
            columnName: "Frl_Attribute_Level",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 5,
        tableName: "Frl_Attributes",
        offSetX: 300,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 30,
        tableData: [
          {
            columnId: 1,
            columnName: "Frl_Attribute_Level",
            dataType: "Number"
          },
          {
            columnId: 2,
            columnName: "Frl_Attribute",
            dataType: "Varchar2"
          },
        ]
      },
    ],
  },
  {
    cols: 25,
    rows: 25,
    x: 2,
    y: 40,

    size: "md",
    dragEnabled: true,
    pulsButton: true,
    showMiniview: true,
    borderColor: '',
    listflag: true,
    backgroundcolur: "#FFF0F5",
    data: {},
    moduleId: 2,
    moduleName: "MRL",
    moduleData: [
      {
        tableId: 1,
        tableName: "Mrl_Lines",
        offSetX: 500,
        offSetY: 150,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 15,
        x: 50,
        tableData: [
          {
            columnId: 1,
            columnName: "Mrl_Line",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 2,
        tableName: "Mrl_Balance_Types",
        offSetX: 200,
        offSetY: 310,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 31,
        x: 20,
        tableData: [
          {
            columnId: 1,
            columnName: "Mrl_Line",
            dataType: "Varchar2"
          },
          {
            columnId: 2,
            columnName: "Bal_Type",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 3,
        tableName: "Mrl_Calculations",
        offSetX: 550,
        offSetY: 310,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 31,
        x: 55,
        tableData: [
          {
            columnId: 1,
            columnName: "Mrl_Line",
            dataType: "Varchar2"
          },
          {
            columnId: 2,
            columnName: "Bal_Type",
            dataType: "Number"
          },
          {
            columnId: 3,
            columnName: "Mrl_Sequence",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 4,
        tableName: "MRL_Attribute_Levels",
        offSetX: 10,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 1,
        tableData: [
          {
            columnId: 1,
            columnName: "MRL_Attribute_Level",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 5,
        tableName: "MRL_Attributes",
        offSetX: 300,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 30,
        tableData: [
          {
            columnId: 1,
            columnName: "MRL_Attribute_Level",
            dataType: "Number"
          },
          {
            columnId: 2,
            columnName: "MRL_Attribute",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 6,
        tableName: "Mrl_Mappings",
        offSetX: 800,
        offSetY: 310,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 31,
        x: 80,
        tableData: [
          {
            columnId: 1,
            columnName: "Mrl_Line",
            dataType: "Varchar2"
          },
          {
            columnId: 2,
            columnName: "Bal_Type",
            dataType: "Number"
          },
        ]
      },
    ]
  },
  {
    cols: 25,
    rows: 25,
    x: 40,
    y: 2,

    size: "md",
    dragEnabled: true,
    pulsButton: true,
    showMiniview: true,
    borderColor: '',
    listflag: true,
    backgroundcolur: "#FAFAD2",
    data: {},
    moduleId: 3,
    moduleName: "OUC",
    moduleData: [
      {
        tableId: 1,
        tableName: "Ouc_Codes",
        offSetX: 450,
        offSetY: 150,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 15,
        x: 45,
        tableData: [
          {
            columnId: 1,
            columnName: "Vision_Ouc",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 2,
        tableName: "OUC_Attribute_Levels",
        offSetX: 10,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 1,
        tableData: [
          {
            columnId: 1,
            columnName: "OUC_Attribute_Level",
            dataType: "Number"
          },
        ]
      },
      {
        tableId: 3,
        tableName: "OUC_Attributes",
        offSetX: 300,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 30,
        tableData: [
          {
            columnId: 1,
            columnName: "OUC_Attribute_Level",
            dataType: "Number"
          },
          {
            columnId: 2,
            columnName: "OUC_Attribute",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 4,
        tableName: "OUC_Level_01",
        offSetX: 800,
        offSetY: 10,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 1,
        x: 80,
        tableData: [
          {
            columnId: 1,
            columnName: "OUC_Level_01",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 5,
        tableName: "OUC_Level_02",
        offSetX: 800,
        offSetY: 150,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 15,
        x: 80,
        tableData: [
          {
            columnId: 1,
            columnName: "OUC_Level_02",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 6,
        tableName: "OUC_Level_03",
        offSetX: 800,
        offSetY: 280,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 28,
        x: 80,
        tableData: [
          {
            columnId: 1,
            columnName: "OUC_Level_03",
            dataType: "Varchar2"
          },
        ]
      },
      {
        tableId: 7,
        tableName: "OUC_Level_04",
        offSetX: 800,
        offSetY: 420,
        height: 131,
        width: 175,
        tableClicked: false,
        cols: 16,
        rows: 12,
        y: 42,
        x: 80,
        tableData: [
          {
            columnId: 1,
            columnName: "OUC_Level_04",
            dataType: "Varchar2"
          },
        ]
      },
    ]
  },
  // {
  //   cols: 40,
  //   rows: 15,
  //   y: 46,
  //   x: 33,
  //   size: "md",
  //   dragEnabled: true,
  //   pulsButton: true,
  //   showMiniview: true,
  //   borderColor: '',
  //   listflag: true,
  //   backgroundcolur: "#FFDAB9",
  //   data: {},
  //   moduleId: 4,
  //   moduleName: "Products",
  //   moduleData: [
  //     {
  //       tableId: 1,
  //       tableName: "Product_Codes",
  //       offSetX: 450,
  //       offSetY: 150,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 15,
  //       x: 45,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 2,
  //       tableName: "Product_Attribute_Levels",
  //       offSetX: 10,
  //       offSetY: 10,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 1,
  //       x: 1,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product_Attribute_Level",
  //           dataType: "Number"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 3,
  //       tableName: "Product_Attributes",
  //       offSetX: 300,
  //       offSetY: 10,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 1,
  //       x: 30,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product_Attribute_Level",
  //           dataType: "Number"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Product_Attribute",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 4,
  //       tableName: "Product_Level_01",
  //       offSetX: 800,
  //       offSetY: 10,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 1,
  //       x: 80,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product_Level_01",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 5,
  //       tableName: "Product_Level_02",
  //       offSetX: 800,
  //       offSetY: 150,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 15,
  //       x: 80,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product_Level_02",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 6,
  //       tableName: "Product_Level_03",
  //       offSetX: 800,
  //       offSetY: 280,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 28,
  //       x: 80,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product_Level_03",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 7,
  //       tableName: "Product_Level_04",
  //       offSetX: 800,
  //       offSetY: 420,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 42,
  //       x: 80,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Product_Level_04",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 8,
  //       tableName: "Product_Remaps",
  //       offSetX: 20,
  //       offSetY: 270,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 27,
  //       x: 2,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Gl_Product",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Ouc_Product",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 9,
  //       tableName: "Product_Business_Remaps",
  //       offSetX: 200,
  //       offSetY: 380,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 38,
  //       x: 20,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Gl_Product",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Business_Group",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //   ]
  // },
  // {
  //   cols: 30,
  //   rows: 25,
  //   y: 18,
  //   x: 41,
  //   size: "md",
  //   dragEnabled: true,
  //   pulsButton: true,
  //   showMiniview: true,
  //   borderColor: '',
  //   listflag: true,
  //   backgroundcolur: "#FFF0F5",
  //   data: {},
  //   moduleId: 5,
  //   moduleName: "General Ledger",
  //   moduleData: [
  //     {
  //       tableId: 1,
  //       tableName: "Accounts_Mappings",
  //       offSetX: 800,
  //       offSetY: 200,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 20,
  //       x: 80,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Country",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Le_Book",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 3,
  //           columnName: "Account_No",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 2,
  //       tableName: "Cgl_Translation",
  //       offSetX: 20,
  //       offSetY: 350,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 35,
  //       x: 20,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Country",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Le_Book",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 3,
  //           columnName: "Mapping_Source_Vision_Gl",
  //           dataType: "Varchar2"
  //         },

  //       ]
  //     },
  //     {
  //       tableId: 3,
  //       tableName: "Gl_Codes",
  //       offSetX: 400,
  //       offSetY: 200,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 20,
  //       x: 40,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Country",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Le_Book",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 3,
  //           columnName: "Vision_Gl",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 4,
  //       tableName: "Gl_Enrich_Ids",
  //       offSetX: 50,
  //       offSetY: 20,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 2,
  //       x: 5,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Gl_Enrich_Id",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 5,
  //       tableName: "Gl_Mappings",
  //       offSetX: 650,
  //       offSetY: 20,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 2,
  //       x: 65,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Country",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Le_Book",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 3,
  //           columnName: "Bs_Gl",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 4,
  //           columnName: "Pl_Gl",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 5,
  //           columnName: "Gl_Enrich_Id",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 6,
  //       tableName: "Gl_Translation",
  //       offSetX: 650,
  //       offSetY: 350,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 35,
  //       x: 65,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Country",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Le_Book",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 3,
  //           columnName: "Vision_Gl",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //     {
  //       tableId: 7,
  //       tableName: "Merge_Table",
  //       offSetX: 20,
  //       offSetY: 200,
  //       height: 131,
  //       width: 175,
  //       tableClicked: false,
  //       cols: 16,
  //       rows: 12,
  //       y: 20,
  //       x: 2,
  //       tableData: [
  //         {
  //           columnId: 1,
  //           columnName: "Country",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 2,
  //           columnName: "Le_Book",
  //           dataType: "Varchar2"
  //         },
  //         {
  //           columnId: 3,
  //           columnName: "Bs_Gl",
  //           dataType: "Varchar2"
  //         },
  //       ]
  //     },
  //   ]
  // },
  //vinith


];
