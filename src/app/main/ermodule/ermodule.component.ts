import {
  AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild
} from "@angular/core";
import { ITreeOptions, TreeComponent } from "@circlon/angular-tree-component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ApiService, GlobalService } from "src/app/service";
import { environment } from "src/environments/environment";
import { isNullOrUndefined } from "util";
@Component({
  selector: "app-ermodule",
  templateUrl: "./ermodule.component.html",
  styleUrls: ["./ermodule.component.css"],
})
export class ErmoduleComponent implements OnInit, OnDestroy, AfterViewInit {
  @Output() childData = new EventEmitter();
  @Output() triggersvg = new EventEmitter();
  @Input() RelationSearch: any;
  @ViewChild("tree") tree: TreeComponent;
  @ViewChild(TreeComponent)
  public tree1: TreeComponent;
  @ViewChild("missingObjTemplate", { static: false })
  missingObjSetupTemplate: ElementRef;
  @ViewChild("missingRelationTemplate", { static: false })
  missingRelationSetupTemplate: ElementRef;

  public options1: ITreeOptions = {
    allowDrag: true,
    allowDrop: false,
    useCheckbox: false,
    useVirtualScroll: false,
    displayField: "moduleName",
    idField: "moduleId",
    hasChildrenField: "children",
  };
  pageView: string;
  treenode: any;
  treenode_obj: any;
  leftContainerHeight: any;
  fileExplorer: boolean = true;
  checkingdragvalues: boolean = false;
  mouseOverTree: any = "";
  nodetreevalues: any;
  erAllvalues: any = [];
  maximizeData: any;
  dragOptionIsChanged: any;
  dragOptiontrigger: any = [];
  triggerOption: any = [];
  offset_X: any;
  offset_Y: any;
  relation_table: boolean = true;
  relation_table_sub: boolean = true;
  repositoryValues = [];
  selectedValue: any;
  isOpen_relation = false;
  isOpen_relation_sub = false;
  relationByIndexList: any = [];
  relationByNameList: any = [];
  missingTableObj: any;
  isMasterSel: boolean;
  checkedCategoryList: any = [];
  isMasterSel_byname: boolean;
  checkedCategoryListByName: any = [];
  results: any = [];
  isOpenTreeRepo: boolean = true;
  isOpenTreeMissing: boolean = false;
  activeAccordIds: string[] = [];
  tree_obj_one: any = [];
  tree_obj_two: any = [];
  erDisplayType: any;
  isLoading = false;
  constructor(
    private apiService: ApiService,
    private globalService: GlobalService,
    private modalService: NgbModal,
    // public loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.globalService.erSchema.iconEnabled = true;
    this.explorerJson();
  }

  backToSummaryClick() {
    this.pageView = "O";
    this.checkingdragvalues = false;
  }

  openpage(value) {
    this.pageView = value;
  }

  explorerJson() {
    setTimeout(() => { }, 500);
    this.apiService.get(environment.getAllTree).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.tree_obj_one.push(resp["response"][0]);
        this.tree_obj_two.push(resp["response"][1]);
        this.erDisplayType = resp["response"][2];
        this.tree_obj_one.forEach((element) => {
          element.children.forEach((elementobj) => {
            if (elementobj.moduleName == "Repository") {
              elementobj.children.forEach((element1) => {
                this.repositoryValues.push({
                  moduleName: element1.moduleName,
                  moduleId: element1.moduleId,
                });
              });
            }
          });
        });
        this.treenode = this.tree_obj_one;
        this.treenode_obj = this.tree_obj_two;
        setTimeout(() => {
          this.tree.treeModel
            .getNodeById(this.treenode[0]["moduleId"])
            .expand();
        }, 100);
        let loadallmoduless = this.treenode[0];
        let ermodules = loadallmoduless.jsonDataForER.split(",|");
        ermodules.forEach((element) => {
          if (element != "") {
            this.erAllvalues.push(JSON.parse(element));
          }
        });
        this.pageView = "O";
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  checkChildrens(childs) {
    childs.forEach((ele) => {
      ele.moduleName = ele.moduleName;
      ele.allNodes = ele.children;
      ele.startIndex = 0;
      ele.endIndex = 25;
      ele.children = ele.allNodes.slice(ele.startIndex, ele.endIndex);

      if (ele.allNodes.length > ele.endIndex) {
        ele.children.push({ moduleName: "Load more...", children: [] });
      }
      if (ele.children && ele.children.length) {
        this.checkChildrens(ele.children);
      }
    });
  }

  loadMoreData(node) {
    let parent = node["parent"]["data"];
    parent["startIndex"] = parent["startIndex"] + 25;
    parent["endIndex"] = parent["endIndex"] + 25;
    parent["children"].pop();
    parent["children"] = parent["children"].concat(
      parent["allNodes"].slice(parent["startIndex"], parent["endIndex"])
    );
    parent["children"].push({ moduleName: "Load more...", children: [] });
    if (parent["allNodes"].length + 1 == parent["children"].length) {
      parent["children"].pop();
    }
    this.tree1.treeModel.update();
  }

  setChlidrenNull(resp) {
    if (resp.length) {
      resp.forEach((element, index) => {
        element.children =
          element.children.length == 0
            ? element.moduleType == "T"
              ? element.children
              : resp.splice(index, 1)
            : this.setChlidrenNull(element.children);
      });
      return resp;
    }
  }

  toggleShowDiv(value: string) {
    if (value == "relation") {
      this.isOpen_relation = !this.isOpen_relation;
      this.isOpen_relation_sub = false;
    }
    if (value == "tree_one") {
      this.isOpenTreeRepo = !this.isOpenTreeRepo;
      this.isOpenTreeMissing = false;
    }
    if (value == "tree_two") {
      this.isOpenTreeMissing = !this.isOpenTreeMissing;
      this.isOpenTreeRepo = false;
    }
    if (value == "relation_sub") {
      this.isOpen_relation_sub = !this.isOpen_relation_sub;
      this.isOpen_relation = false;
    }
  }

 
  childrenclicked() {
    this.toggle = !this.toggle;
    this.status = this.toggle ? 'Enable' : 'Disable';
  }

  async wait(ms: number): Promise<void> {
		return new Promise<void>( resolve => setTimeout( resolve, ms) );
	}
  
  tableClicked(node) {
    this.isLoading = true;
		this.wait(2000).then( () => this.isLoading = false );

    if (this.checkingdragvalues == true) {
      this.dragOptiontrigger = node.data;
    } else {
      this.dragOptiontrigger = [];
     // console.log("node.data.moduleType", node.data.moduleType)
      if ((node.data.moduleType == "T" || node.data.moduleType == "V") && (node.data.moduleId != "TABLES")) {
        this.pageView = "T";
        let item = [];
        localStorage.setItem('dataSource', JSON.stringify(item));
       // console.log("node.data",node.data)
        node.data.clearRoutingValues = true;
        this.nodetreevalues = node.data;
        localStorage.setItem('dataSource', JSON.stringify(this.nodetreevalues));
        // this.highlighttreeText(node);
      }

      if (node.data.moduleType == "MT" && node.data.moduleId == "MODIFY_TABLE") {
        this.pageView = "MT";
        this.nodetreevalues = node.data;

      }
      if (node.data.moduleType == "NT" && node.data.moduleId == "NEW_TABLE") {
        this.pageView = "NT";
        node.data.pageView = "NT";
        node.data.repositoryValues = this.repositoryValues;
        this.nodetreevalues = node.data;

      }

      if (node.data.moduleType == "DT" && node.data.moduleId == "DELETED_TABLE") {
        this.pageView = "DT";
        node.data.pageView = "DT";
        this.nodetreevalues = node.data;

      }
      if (node.data.moduleType == "MR" && node.data.moduleId == "MISSIG_RELATION") {
        this.pageView = "MRM";
        let data: any = {};
        data.relationByIndexList = node.data.children;
        this.nodetreevalues = data;

      }

      if (node.data.moduleType == "MR" && node.data.moduleId != "MISSIG_RELATION") {
        this.relationSuggestionPasslist(node.data);

      }

      if (node.data.moduleType == "M") {
        let objects = {
          connectionId: node.data.connectionId,
          moduleId: node.data.moduleId,
          screenName: node.data.screenName,
          productName: node.data.productName,
          schema: node.data.schema,
          selecteLanguage: "EN",
        };
        const api_Modify = environment.getErDiagramByModule;
        this.apiService.post(api_Modify, objects).subscribe((resp) => {
          if (resp["status"] == 1) {
            let nodevalues = resp["otherInfo"];
            // console.log("nodevalues", nodevalues);
            if (!isNullOrUndefined(nodevalues)) {
              let obj = JSON.parse(nodevalues.jsonDataForER);
              obj["screenName"] = nodevalues.screenName;
              obj["jsonDataForER"] = JSON.parse(nodevalues.jsonDataForER);
              obj["jsondatarelation"] = nodevalues.jsonDataForRelations;
              obj["erDisplayType"] = this.erDisplayType;
              this.maximizeData = obj;
              this.childData.emit({
                type: "maximize",
                data: this.maximizeData,
                value: false,
              });
              console.log("obj", obj)
              // obj.forEach(element => {
              //   element.moduleData.forEach(element => {
              //     console.log("tableId",element.tableId);
                  
              //   });
                
                
              // });
              localStorage.setItem('dataSource', JSON.stringify(obj));
              setTimeout(() => {
                this.pageView = "0";
              }, 100);
              setTimeout(() => {
                this.pageView = "M";
              }, 1000);
             
            }
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
       
      }
    }
  }
  toggle = true;
  status = 'Enable';

  refreshData() {
    let objects = "";
    const api_Modify = environment.reloadMissingObjects;
    this.apiService.post(api_Modify, objects).subscribe((resp) => {
      if (resp["status"] == 1) {
        let nodevalues = resp["otherInfo"];
        this.treenode_obj.forEach((element) => {
          if (element.children) {
            element.children.forEach((element1) => {
              if (element1.moduleId == "MISSOBJ") {
                element1.children = nodevalues.children;
              }
            });
          }
        });

        this.tree1.treeModel.update();
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }


  getdata(data) {
    this.apiService
      .post(environment.getRelationSuggestions, data.data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          data.otherInfo = resp["otherInfo"];
          data.relationByIndexList = resp["request"];
          data.relationByNameList = resp["response"];
          this.pageView = "MR";
          this.nodetreevalues = data;
        } else {
          data.otherInfo = resp["otherInfo"];
          data.relationByIndexList = resp["request"];
          data.relationByNameList = resp["response"];
          this.pageView = "MR";
          this.nodetreevalues = data;
          this.globalService.showToastr.warning(resp["message"]);
        }
      });


  }
  getChildData(event) {
    if (event.type == "minimize") {
      event.data.maximize = false;
      this.pageView = "O";
    }
    if (event.type == "maximize") {

      event.data.maximize = true;
      event.data.erDisplayType = this.erDisplayType;
      this.maximizeData = event.data;
      this.fileExplorer = false;
      this.pageView = "M";
      setTimeout(() => {
      }, 100);
    }
    if (event.type == "table") {
      event.data.maximize = true;
      this.nodetreevalues = event.data;
      this.pageView = "T";
      setTimeout(() => {
      }, 100);
    }
  }

  maximizescreen() {
    if (this.fileExplorer) {
      this.fileExplorer = false;
    }
    else {
      this.fileExplorer = true;
      let data = {
        vlaues: true
      }
      this.triggerOption = data;
    }
  }

  openPopUp(data) {
    let value = data.moduleType;
    if (value == "NT" || value == "DT" || value == "MT") {
      if (value == "NT") {
        this.missingTableObj = data;
        this.repositoryValues.forEach((element) => {
          if (element) {
            element.checked = false;
          }
        });
        this.selectedValue = [];
        this.modalService.open(this.missingObjSetupTemplate, {
          size: "sm",
          backdrop: "static",
          windowClass: "missingObjectsPopUp",
        });

      } else {
        const api_Modify = environment.addMissionObjects;
        this.apiService.post(api_Modify, data).subscribe((resp) => {
          if (resp["status"] == 1) {
            this.modalClose();
            this.globalService.showToastr.success(resp["message"]);
          } else {
            this.modalClose();
            this.globalService.showToastr.error(resp["message"]);
          }
        });
      }
    } else {
      this.relationSuggestionPasslist(data);
    }
  }

  relationSuggestionPasslist(data) {
    this.apiService
      .post(environment.getRelationSuggestions, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          data.otherInfo = resp["otherInfo"];
          data.relationByIndexList = resp["request"];
          data.relationByNameList = resp["response"];
          this.pageView = "MR";
          this.nodetreevalues = data;
        } else {
          data.otherInfo = resp["otherInfo"];
          data.relationByIndexList = resp["request"];
          data.relationByNameList = resp["response"];
          this.pageView = "MR";
          this.nodetreevalues = data;
          this.globalService.showToastr.warning(resp["message"]);
        }
      });
  }

  modalClose() {
    this.modalService.dismissAll();
    this.selectedValue = [];
  }

  //---->checking drag option is triggered from maximize component
  checkingDragOptionIsTriggered(data) {
    this.offset_X = "";
    this.offset_Y = "";
    this.offset_X = data.table_X;
    this.offset_Y = data.table_Y;
    if (this.offset_X != null && this.offset_Y != null) {
      this.checkingDragoption();
    }
    if (this.offset_X == "" && this.offset_Y == "") {
      this.checkingdragvalues = false;
    }
  }
  checkingDragoption() {
    if (
      !isNullOrUndefined(this.offset_X) ||
      !isNullOrUndefined(this.offset_Y)
    ) {
      this.checkingdragvalues = true;
    } else {
      this.checkingdragvalues = false;
    }
  }



  clicknodetree(event) {

    this.checkingdragvalues = false;
    let node = event.data;
    if (node.moduleType == "T" || node.moduleType == "V") {
      this.pageView = "T";
      this.nodetreevalues = node;
      // this.highlighttreeText(node);
    }
    if (node.moduleType == "M") {
      let objects = {
        connectionId: node.connectionId,
        moduleId: node.moduleId,
        screenName: node.screenName,
        productName: node.productName,
        schema: node.schema,
        selecteLanguage: "EN",
      };
      const api_Modify = environment.getErDiagramByModule;
      this.apiService.post(api_Modify, objects).subscribe((resp) => {
        if (resp["status"] == 1) {
          let nodevalues = resp["otherInfo"];
          // console.log("nodevalues",nodevalues);

          if (!isNullOrUndefined(nodevalues)) {
            let obj = JSON.parse(nodevalues.jsonDataForER);
            obj["screenName"] = nodevalues.screenName;
            obj["jsonDataForER"] = JSON.parse(nodevalues.jsonDataForER);
            obj["jsondatarelation"] = nodevalues.jsonDataForRelations;
            obj["erDisplayType"] = this.erDisplayType;
            this.maximizeData = obj;
            this.childData.emit({
              type: "maximize",
              data: this.maximizeData,
              value: false,
            });

            setTimeout(() => {
              this.pageView = "0";
            }, 100);
            setTimeout(() => {
              this.pageView = "M";
            }, 1000);
            // this.fileExplorer = false;
          }
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
      //this.highlighttreeText(node);
    }
  }

  //---->checking drag option is triggered from maximize component

  checkUncheckAll() {
    for (var i = 0; i < this.relationByIndexList.length; i++) {
      this.relationByIndexList[i].isSelected = this.isMasterSel;
    }
    this.getCheckedItemList();
  }
  getCheckedItemList() {
    this.checkedCategoryList = [];
    for (var i = 0; i < this.relationByIndexList.length; i++) {
      if (this.relationByIndexList[i].isSelected)
        this.checkedCategoryList.push(this.relationByIndexList[i]);
    }
  }

  isAllSelected() {
    this.isMasterSel = this.relationByIndexList.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }
  checkUncheckAllByName() {
    for (var i = 0; i < this.relationByNameList.length; i++) {
      this.relationByNameList[i].isSelected = this.isMasterSel_byname;
    }
    this.getCheckedItemListByName();
  }
  isAllSelectedByName() {
    this.isMasterSel_byname = this.relationByNameList.every(function (
      item: any
    ) {
      return item.isSelected == true;
    });
    this.getCheckedItemListByName();
  }
  getCheckedItemListByName() {
    this.checkedCategoryListByName = [];
    for (var i = 0; i < this.relationByNameList.length; i++) {
      if (this.relationByNameList[i].isSelected)
        this.checkedCategoryListByName.push(this.relationByNameList[i]);
    }
  }
  addMissingRelationValues() {
    this.results.push(
      ...this.checkedCategoryList,
      ...this.checkedCategoryListByName
    );
    const api_Modify = environment.edwObjinsertRelationsData;
    this.apiService.post(api_Modify, this.results).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.modalClose();
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  repositoryChange(target) {
    this.repositoryValues.forEach((element) => {
      if (element.moduleId != target.moduleId) {
        element.checked = false;
      }
    });
  }


  applyRepository() {
    this.selectedValue = this.repositoryValues.filter(val => val.checked == true);
    if (this.selectedValue.length) {
      let data = this.missingTableObj;
      data.objectRepository = this.selectedValue[0].moduleId;
      const api_Modify = environment.addMissionObjects;
      this.apiService.post(api_Modify, data).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.modalClose();
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    } else {
      this.globalService.showToastr.warning("Please Select Repository");
    }
  }

  ngOnDestroy() {
    this.globalService.erSchema.iconEnabled = false;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      this.leftContainerHeight = getContentAreaHeight - navbarHeight;
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }


  // highlighttreeText(value) {
  //   console.log("id", value.data);
  //   if (value.data.moduleType == "BG" || value.data.moduleType == "M") {
  //     console.log("chek1");
  //     setTimeout(() => {
  //       this.tree.treeModel.getNodeById(value.data.moduleName).setActiveAndVisible();
  //     }, 100);
  //   } else {
  //     console.log("chek2");
  //     setTimeout(() => {
  //       this.tree1.treeModel.getNodeById(value.data.moduleName).setActiveAndVisible();
  //     }, 100);
  //   }
  // }

}
