import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild
} from "@angular/core";
import { GlobalService } from "../../../service";



@Component({
  selector: 'app-missingrepositoryviewmodule',
  templateUrl: './missingrepositoryviewmodule.component.html',
  styleUrls: ['./missingrepositoryviewmodule.component.css']
})

export class MissingrepositoryviewmoduleComponent implements OnInit, OnChanges {
  @ViewChild("viewcontainer") viewcontainer: ElementRef;
  leftContainerHeight: string;
  table_headervalues: any ;
  tablelist_columns: any = [];
  checkedTableList: any = [];
  @Input() nodetreevalues: any;
  
  @Output() backToSummaryClick = new EventEmitter();
  @Output() RelationSearchdata = new EventEmitter();
  @Output() RelationSearch = new EventEmitter();
  
  pageShow: boolean = false;
  pageview: any;
  relationByIndexList: any = [];
  relationByNameList: any = [];
  isOpen_relation = false;
  isOpen_relation_sub = false;
  checkedCategoryList: any = [];
  isMasterSel: boolean;
  isMasterSel_byname: boolean;
  checkedCategoryListByName: any = [];
  results: any = [];
  constructor(
    private changeDetector: ChangeDetectorRef,
    public globalService: GlobalService,
  ) { }

  ngOnInit(): void {
    
  }
  

  ngOnChanges() {
    this.tabletreevalues();
  }

  tabletreevalues() {
    this.table_headervalues = [];
    this.relationByIndexList = [];
    this.relationByNameList = [];
    this.table_headervalues = this.nodetreevalues.otherInfo;
    this.relationByIndexList = this.nodetreevalues.relationByIndexList;
    this.relationByNameList = this.nodetreevalues.relationByNameList;
  
  }

  relationclick(data){
    this.RelationSearch.emit({
      data: data,
      value: true,
    });

    
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.changeDetector.detectChanges();
    });
  }
}

