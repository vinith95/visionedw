import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingrepositoryviewmoduleComponent } from './missingrepositoryviewmodule.component';

describe('MissingrepositoryviewmoduleComponent', () => {
  let component: MissingrepositoryviewmoduleComponent;
  let fixture: ComponentFixture<MissingrepositoryviewmoduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingrepositoryviewmoduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingrepositoryviewmoduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
