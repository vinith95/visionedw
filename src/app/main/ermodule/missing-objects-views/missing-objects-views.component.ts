import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ReviewComponent } from "src/app/shared/review/review.component";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";

@Component({
  selector: 'app-missing-objects-views',
  templateUrl: './missing-objects-views.component.html',
  styleUrls: ['./missing-objects-views.component.css']
})


export class MissingObjectsViewsComponent implements OnInit, OnChanges {
  @ViewChild("viewcontainer") viewcontainer: ElementRef;

  leftContainerHeight: string;
  isMasterSel: any;
  table_headervalues: any = [];
  tablelist_columns: any = [];
  checkedTableList: any = [];
  @Input() nodetreevalues: any;
  @Output() backToSummaryClick = new EventEmitter();
  pageShow: boolean = false;
  pageview: any;
  constructor(
    private changeDetector: ChangeDetectorRef,
    private _apiService: ApiService,
    private globalService: GlobalService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.tabletreevalues();
  }

  tabletreevalues() {

    const pass_list = {
      objectName:
        this.nodetreevalues.moduleName ||
        this.nodetreevalues.tableName ||
        this.nodetreevalues.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: this.nodetreevalues.selecteLanguage,
      objectType:
        this.nodetreevalues.moduleType || this.nodetreevalues.objectType,
    };
    this.pageview = this.nodetreevalues.pageview;
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];

        this.nodetreevalues.children.forEach(element => {
          if (element.childrenCols) {
            // element["is_open"] = false;
            // element["isSelected"] = false;
            element.childrenCols.forEach(element1 => {
              if (element1) {
                // element1.moduleId = element.moduleId;
                //  console.log("element1.moduleId",element1.moduleId);
                this.tablelist_columns.push(element1)
              }
            });
          }
        });
        // if (element) {
        //   this.tablelist_columns.push(element.childrenCols)
        // }
       // console.log("this.tablelist_columns", this.tablelist_columns);
        this.tablelist_columns = this.nodetreevalues.children;
        this.tablelist_columns.forEach(element => {
          if (element) {
            element["is_open"] = false;
            element["isSelected"] = false;
          }
        });
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  toggleShowDiv(value) {
    if (value) {
      this.tablelist_columns.forEach(element => {
        if (element.moduleId == value.moduleId) {
          element["isSelected"] = false;
          if (element["is_open"] == true) {
            element["is_open"] = false;
          }
          else {
            element["is_open"] = true;
          }
        }
        else {
          element["is_open"] = false;
        }

      });
    }
  }
  selectValue() {
    this.isMasterSel = this.tablelist_columns.every(function (item: any) {
      return item.isSelected == true;
    });

    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedTableList = [];
    for (var i = 0; i < this.tablelist_columns.length; i++) {
      if (this.tablelist_columns[i].isSelected)
        this.checkedTableList.push(this.tablelist_columns[i]);
    }

  }

  reviewPopup(data) {
    //console.log("popup click",data);
    
    const modelRef = this.modalService.open(ReviewComponent, {
      size: "lg",
      backdrop: "static",
      windowClass: "missingObjects",
    });
    modelRef.componentInstance.header1 = "Column Properties";
    modelRef.componentInstance.header2 = "Old";
    modelRef.componentInstance.header3 = "New";
    modelRef.componentInstance.title = data.moduleName;

    modelRef.componentInstance.response = data.childrenCols;
    modelRef.componentInstance.closePopup.subscribe(() => {
      modelRef.close();
    });
  }



  backToSummary() {
    this.backToSummaryClick.emit(true);
  }

  missingObjectSubmission() {
    const valueContains = this.checkedTableList.filter(value => value.isSelected === true).length;
    if (valueContains < 1) {
      this.globalService.showToastr.warning("Please Select the Tables");
    }
    else {
      const api_Modify = environment.addMissionMultipleObjects;
      this._apiService.post(api_Modify, this.checkedTableList).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.tablelist_columns = this.tablelist_columns.filter((el) => !this.checkedTableList.includes(el));
          this.cancelBtnAll();
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    }
  }

  cancelBtnAll() {
    this.tablelist_columns.forEach(element => {
      if (element) {
        element["is_open"] = false;
        element["isSelected"] = false;
      }

    });
    this.checkedTableList.forEach(element1 => {
      if (element1) {
        element1["is_open"] = false;
        element1["isSelected"] = false;
      }

    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.changeDetector.detectChanges();
    });
  }
}
