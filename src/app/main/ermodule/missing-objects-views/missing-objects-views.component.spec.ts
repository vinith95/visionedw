import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingObjectsViewsComponent } from './missing-objects-views.component';

describe('MissingObjectsViewsComponent', () => {
  let component: MissingObjectsViewsComponent;
  let fixture: ComponentFixture<MissingObjectsViewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingObjectsViewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingObjectsViewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
