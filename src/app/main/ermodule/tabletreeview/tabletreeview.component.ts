import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild
} from "@angular/core";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";
import { SlideInOutAnimation } from "./../animation";
@Component({
  selector: "app-tabletreeview",
  templateUrl: "./tabletreeview.component.html",
  styleUrls: ["./tabletreeview.component.css"],
  animations: [SlideInOutAnimation],
})
export class TabletreeviewComponent implements OnInit, OnChanges {
  @ViewChild("viewcontainer") viewcontainer: ElementRef;
  isOpen_columns = true;
  isOpen_relation = true;
  isOpen_unique_key = true;
  isOpen_Uses = true;
  isOpen_Usedby = true;
  isOpen_Uses_by_sub = true;
  isOpen_Uses_by_sub2 = true;
  leftContainerHeight: string;
  animationState = "in";
  tablelist_columns: any = [];
  tablelist_relations: any = [];
  tablelist_Indexes: any = [];
  table_headervalues: any = [];
  useslist: any = [];
  usedbylist: any = [];
  columns_table: boolean = true;
  unique_table: boolean = true;
  relation_table: boolean = true;
  use_table: boolean = true;
  usedby_table: boolean = true;
  localStorageOfTableName: any = [];
  @Output() childData = new EventEmitter();
  @Input() nodetreevalues: any;
  @Output() backToSummaryClick = new EventEmitter();
  maximizeData: any;
  pageShow: boolean = false;
  extraHeight: number;
  maxiBtnSet:boolean =false;
  bckButtonShow:boolean = true;
  constructor(
    private changeDetector: ChangeDetectorRef,
    private _apiService: ApiService,
    private globalService: GlobalService
  ) { }

  ngOnInit(): void {

  }

  ngOnChanges() {
    this.tabletreevalues();
  }

  tabletreevalues() {
    const pass_list = {
      objectName:
        this.nodetreevalues.moduleName ||
        this.nodetreevalues.tableName ||
        this.nodetreevalues.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: this.nodetreevalues.selecteLanguage,
      objectType:
        this.nodetreevalues.moduleType || this.nodetreevalues.objectType,
    };
    if (this.nodetreevalues.clearRoutingValues) {
      this.localStorageOfTableName = [];
    }
    else {
      this.localStorageOfTableName.push(pass_list);
    }

    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;
      //  console.log("tablelist_columns",this.tablelist_columns);
        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
        }

        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  toggleShowDiv(value: string) {
    if (value == "Columns") {
      this.isOpen_columns = !this.isOpen_columns;
      this.isOpen_relation = false;
      this.isOpen_unique_key = false;
      this.isOpen_Uses = false;
      this.isOpen_Usedby = false;
      this.classadding();
    }
    if (value == "Relations") {
      this.isOpen_relation = !this.isOpen_relation;
      this.isOpen_unique_key = false;
      this.isOpen_Uses = false;
      this.isOpen_Usedby = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    if (value == "Unique") {
      this.isOpen_unique_key = !this.isOpen_unique_key;
      this.isOpen_relation = false;
      this.isOpen_Uses = false;
      this.isOpen_Usedby = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    if (value == "Uses") {
      this.isOpen_Uses = !this.isOpen_Uses;
      this.isOpen_relation = false;
      this.isOpen_unique_key = false;
      this.isOpen_Usedby = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    if (value == "used_by") {
      this.isOpen_Usedby = !this.isOpen_Usedby;
      this.isOpen_relation = false;
      this.isOpen_unique_key = false;
      this.isOpen_Uses = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    setTimeout(() => {
      let card_body = document.getElementById("card-body");
      let grid_header = document.getElementById("grid_header");
      if (card_body && grid_header) {
        //console.log(card_body.offsetHeight, grid_header.offsetHeight)
        this.extraHeight = 100 + (card_body.offsetHeight - grid_header.offsetHeight);
      }
    },1000)
  }
  classadding() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }
  usesbyFunt(values) {
    this.calltree(values);
  }
  usesFunt(values) {
    this.calltree(values);
  }
  backToSummary() {
    if (this.localStorageOfTableName.length > 1) {
      let values_passing =
        this.localStorageOfTableName[this.localStorageOfTableName.length - 2];
      this.localStorageOfTableName.splice(-1);
      this.calltreeBackRouting(values_passing);
    } else if (this.localStorageOfTableName.length == 1) {
      var item = JSON.parse(localStorage.getItem('dataSource'));
      this.maximizeData = item;
      if(this.maximizeData.moduleType == "T"){
        this.localStorageOfTableName =[];
        this.calltreeBackRouting(this.maximizeData);
      }
      else{
        if (this.maximizeData.moduleId) {
          this.childData.emit({
            type: "maximize",
            data: this.maximizeData,
            value: true,
          });
        }
      }
      // else {
      //   this.backToSummaryClick.emit(true);
      // }
    }
    else if (this.localStorageOfTableName == "") {
      this.backToSummaryClick.emit(true);
    }
  }

  calltreetables(values) {

    const pass_list = {
      objectName: values.guidelines,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: values.selecteLanguage,
      objectType: "T",
    };
    this.localStorageOfTableName.push(pass_list);
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
       // console.log("tablelist_columns",this.tablelist_columns);
        
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }
        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }
  calltree(values) {
    const pass_list = {
      objectName: values.moduleName || values.tableName || values.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: values.selecteLanguage,
      objectType: "T",
    };
    this.localStorageOfTableName.push(pass_list);
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }
        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  calltreeBackRouting(values) {
    const pass_list = {
      objectName: values.moduleName || values.tableName || values.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: values.selecteLanguage,
      objectType: "T",
    };

    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }
        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.changeDetector.detectChanges();
    });
  }

}
