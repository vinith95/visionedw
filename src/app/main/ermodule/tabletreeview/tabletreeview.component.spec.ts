import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletreeviewComponent } from './tabletreeview.component';

describe('TabletreeviewComponent', () => {
  let component: TabletreeviewComponent;
  let fixture: ComponentFixture<TabletreeviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabletreeviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabletreeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
