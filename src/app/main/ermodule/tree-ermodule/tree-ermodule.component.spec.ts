import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeErmoduleComponent } from './tree-ermodule.component';

describe('TreeErmoduleComponent', () => {
  let component: TreeErmoduleComponent;
  let fixture: ComponentFixture<TreeErmoduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreeErmoduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeErmoduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
