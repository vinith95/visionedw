import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ITreeOptions } from '@circlon/angular-tree-component';
import { ApiService } from 'src/app/service';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-tree-ermodule',
  templateUrl: './tree-ermodule.component.html',
  styleUrls: ['./tree-ermodule.component.css']
})
export class TreeErmoduleComponent implements OnInit {

  constructor(private _apiService: ApiService) { }
  selectedValue: any;
  @ViewChild('tree') tree;
  @Input() treeData;
  nodetreevalues: any;
  treenode: any = [];
  buttonshowtablelistview: boolean = false;
  showgridster:boolean = false;
  selectedNode :any =[];
  dashboard:any =[];
  buttonshowmaximize: boolean = false;
  maximizeData:any =[]
  public options1: ITreeOptions = {
    allowDrag: true,
    allowDrop: false,
    useCheckbox: false,
    useVirtualScroll: false,

  };
  ngOnInit(): void {

    this.explorerJson();
  }

  explorerJson() {

    this.dashboard = [];
    this._apiService.get(environment.getAllTree).subscribe(resp => {
      if (resp['status']) {
        this.treenode = this.treeData;
        let loadallmoduless = this.treenode[0];
        let ermodules = loadallmoduless.jsonDataForER.split(",|");
        

        ermodules.forEach((element) => {
          if (element != "") {
            this.dashboard.push(JSON.parse(element))
          }
        });

      }
      else {
       // this.commonService.showToastr.error(resp['message']);
      }
    });
  }

  onUpdateData() {

    this.selectedValue = "modules";
    setTimeout(() => this.tree.treeModel.getNodeById(this.treenode[0]['id']).expand(), 500)
    if (this.selectedValue != '') {
      const node = this.tree.treeModel.getNodeById(this.treenode[0]['children'][0]);
      if (node) {
        node.expand();
      }
    }
  }

  nodeTreeMethods(node) {

    this.nodetreevalues = [];
    if (node.type != "table") {
      this.nodetreevalues = node;
      this.buttonshowtablelistview = false
      setTimeout(() => { this.buttonshowtablelistview = true; }, 10);
      this.showgridster = false;
    }
    else {
      this.buttonshowtablelistview = false;

    }

  }

  onEvent = (event) => {

    if (event.eventName == "activate") {
      //for tree view
      this.selectedNode = event.node.data;
      if (this.selectedNode.moduleId != null && this.selectedNode.productName != null && this.selectedNode.schema != null && this.selectedNode.moduleType == "T") {
        this.nodetreevalues = event.node.data;
        this.buttonshowtablelistview = false
        setTimeout(() => { this.buttonshowtablelistview = true; }, 10);
        this.showgridster = false;
      }

      else if (this.selectedNode.moduleType == "M") {
        let obj = JSON.parse(this.selectedNode.jsonDataForER)
        obj['jsondatarelation'] = (this.selectedNode.jsonDataForRelations)
        this.maximizeData = obj;
        this.buttonshowmaximize = true;
      }

    }
    else {
      this.selectedNode = null
    }
  }
}
