import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";


@Component({
  selector: 'app-missingtreeview',
  templateUrl: './missingtreeview.component.html',
  styleUrls: ['./missingtreeview.component.css']
})



export class MissingtreeviewComponent implements OnInit, OnChanges {
  @ViewChild("viewcontainer") viewcontainer: ElementRef;
  @ViewChild("missingObjTemplate", { static: false }) missingObjSetupTemplate: ElementRef;
  leftContainerHeight: string;
  isMasterSel: any;
  tablelist_columns: any = [];
  table_headervalues: any = [];
  table_headervalueschild: any = [];
  modifyBox: boolean = false;
  modifyBox_two: boolean = false;

  columns_table: boolean = true;
  tablelistcolumns: any = [];
  showTable: boolean = false;
  showColumns: boolean = false;
  titleForBtnBack: any;
  pageShow: boolean = false;
  setLocalHeadersvalues: any = [];
  selectedValue: any;
  repositoryValues = [];
  checkedTableList: any = [];
  pageview: any = [];
  @Input() nodetreevalues: any;
  @Output() backToSummaryClick = new EventEmitter();
  firstBox: boolean = true;
  secondBox: boolean = true;

  toggle: boolean = false;
  status = 'Enable';

  constructor(
    private changeDetector: ChangeDetectorRef,
    private _apiService: ApiService,
    private globalService: GlobalService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    //this.tabletreevalues();
  }

  ngOnChanges() {
    this.tabletreevalues();
  }


  boxClickFn = (type) => {
    if (type == "modify") {
      !this.modifyBox ? this.modifyBox = true : this.modifyBox = false;

      if (this.modifyBox) {
        this.secondBox = false;
      }
      else {
        this.secondBox = true;
      }
    }
    else if (type == "modifytwo") {
      !this.modifyBox_two ? this.modifyBox_two = true : this.modifyBox_two = false;
      if (this.modifyBox_two) {
        this.firstBox = false;
      }
      else {
        this.firstBox = true;
      }
    }

  }

  tabletreevalues() {
    const pass_list = {
      objectName:
        this.nodetreevalues.moduleName ||
        this.nodetreevalues.tableName ||
        this.nodetreevalues.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: this.nodetreevalues.selecteLanguage,
      objectType:
        this.nodetreevalues.moduleType || this.nodetreevalues.objectType,
    };
    this.repositoryValues = this.nodetreevalues.repositoryValues;
    this.pageview = this.nodetreevalues.pageView;
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.setLocalHeadersvalues = resp["otherInfo"];
        this.table_headervalues = resp["otherInfo"];
        this.titleForBtnBack = "Back To Summary";
        this.tablelist_columns = this.nodetreevalues.children;
        let firstIndex = 0;
        this.selectedIndex =firstIndex;
        this.openListData(this.tablelist_columns[firstIndex])
        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
          this.showColumns = false;
          this.showTable = false;

        } else {
          this.columns_table = true;
          this.showColumns = false;
          this.showTable = true;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  openRelationPopup() {
    this.repositoryValues.forEach((element) => {
      if (element) {
        element.checked = false;
      }
    });
    this.selectedValue = [];
    this.modalService.open(this.missingObjSetupTemplate, {
      size: "sm",
      backdrop: "static",
      windowClass: "missingObjectsPopUp",
    });
  }
  selectedIndex:any;
  setRow(_index: number) {
    this.selectedIndex = _index;
  }

  openListData(data) {
   
    this.toggle = true;
    const api_new = environment.edwERSummary_getColumnDataForNewTables;
    this._apiService.post(api_new, data).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.table_headervalueschild = resp["otherInfo"];
        this.tablelistcolumns = resp["response"];
        this.showColumns = true;
        this.titleForBtnBack = "Back";
        this.showTable = false;
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  toggleShowDiv(value) {
    if (value) {
      this.tablelist_columns.forEach(element => {
        if (element.moduleId == value.moduleId) {
          if (element["is_open"] == true) {
            element["is_open"] = false;
          }
          else {
            element["is_open"] = true;
          }
        }
        else {
          element["is_open"] = false;
        }
      });
    }
  }

  backToSummary() {
    if (this.showColumns) {
      this.showColumns = false;
      this.showTable = true;
      // this.table_headervalues = this.setLocalHeadersvalues;
      // this.titleForBtnBack = "Back To Summary";
    }
    else {
      this.backToSummaryClick.emit(true);
    }

  }

  selectValue() {
  
    

    this.isMasterSel = this.tablelist_columns.every(function (item: any) {
      return item.isSelected == true;
    });

    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedTableList = [];
    for (var i = 0; i < this.tablelist_columns.length; i++) {
      if (this.tablelist_columns[i].isSelected)
        this.checkedTableList.push(this.tablelist_columns[i]);
    }
    //this.checkedTableList = JSON.stringify(this.checkedTableList);

  }

  cancelBtnAll() {
    this.tablelist_columns.forEach(element => {
      if (element) {
        element["is_open"] = false;
        element["isSelected"] = false;
      }

    });
    this.checkedTableList.forEach(element1 => {
      if (element1) {
        element1["is_open"] = false;
        element1["isSelected"] = false;
      }

    });
  }

  missingObjSubmission() {


    const valueContains = this.checkedTableList.filter(value => value.isSelected === true).length;
    if (valueContains < 1) {
      this.globalService.showToastr.warning("Please Select the Tables");
    }
    else {
      this.openRelationPopup();
    }
  }
  missingObjSubmissionDT() {
    const valueContains = this.checkedTableList.filter(value => value.isSelected === true).length;
    if (valueContains < 1) {
      this.globalService.showToastr.warning("Please Select the Tables");
    }
    else {
      const api_Modify = environment.addMissionMultipleObjects;
      this._apiService.post(api_Modify, this.checkedTableList).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.tablelist_columns = this.tablelist_columns.filter((el) => !this.checkedTableList.includes(el));
          this.cancelBtnAll();
          this.modalClose();
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    }
  }

  repositoryChange(target) {
    this.repositoryValues.forEach((element) => {
      if (element.moduleId != target.moduleId) {
        element.checked = false;
      }
    });
  }

  applyRepository() {
    this.selectedValue = this.repositoryValues.filter(val => val.checked == true);
    if (this.selectedValue.length) {

      let objectRepository = this.selectedValue[0].moduleId;
      this.checkedTableList.forEach(element => {
        if (element.isSelected) {
          element['objectRepository'] = objectRepository;
        }
      });
      const api_Modify = environment.addMissionMultipleObjects;
      this._apiService.post(api_Modify, this.checkedTableList).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.tablelist_columns = this.tablelist_columns.filter((el) => !this.checkedTableList.includes(el));
          this.cancelBtnAll();
          this.modalClose();
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });

    } else {
      this.globalService.showToastr.warning("Please Select Repository");
    }
  }
  modalClose() {
    this.modalService.dismissAll();
    this.selectedValue = [];
  }


  //   highlight() {
  //     if(!this.query) {
  //         return this.content;
  //     }
  //     return this.content.replace(new RegExp(this.query, "gi"), match => {
  //         return '<span class="highlightText">' + match + '</span>';
  //     });
  // }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.changeDetector.detectChanges();
    });
  }

}
