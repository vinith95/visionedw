import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingtreeviewComponent } from './missingtreeview.component';

describe('MissingtreeviewComponent', () => {
  let component: MissingtreeviewComponent;
  let fixture: ComponentFixture<MissingtreeviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingtreeviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingtreeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
