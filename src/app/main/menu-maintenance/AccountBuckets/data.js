export const tableDetail = {
  headers: [
    {
      name: "Application Code",
      referField: "acBucketApplicationCode",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Application Code Desc",
      referField: "acBucketApplicationCodeDesc",
      filter: false,
      isFilterField: true,
    },
    // {
    // 	name: 'Ac Bucket Code',
    // 	referField: 'acBucketCode',
    // 	filter: false,
    // 	isFilterField: true,
    // },

    // {
    // 	name: 'Ac Bucket Description',
    // 	referField: 'acBucketDescription',
    // 	filter: false,
    // 	isFilterField: true,
    // },

    // {
    // 	name: 'Amount Start',
    // 	referField: 'amountStart',
    // 	filter: false,
    // 	isFilterField: true,
    // },

    // {
    // 	name: 'Amount End',
    // 	referField: 'amountEnd',
    // 	filter: false,
    // 	isFilterField: true,
    // },

    {
      name: "Status",
      referField: "acBucketStatusDesc",
      filter: false,
      isFilterField: true,
    },

    {
      name: "Record Indicator",
      referField: "recordIndicatorDesc",
      filter: false,
      isFilterField: true,
    },

    // {
    // 	name: 'Date Last Modified',
    // 	referField: 'dateLastModified',
    // 	filter: false,
    // 	isFilterField: true,
    // },

    // {
    // 	name: 'Date Creation',
    // 	referField: 'dateCreation',
    // 	filter: false,
    // 	isFilterField: true,
    // },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: true,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "copy",
      icon: "file_copy",
      actionType: "copy",
    },
    {
      name: "edit",
      icon: "mode_edit",
      actionType: "edit",
    },
    {
      name: "view",
      icon: "visibility",
      actionType: "view",
    },
    /*        {
            name: 'delete',
            icon: 'delete',
            actionType: 'delete',
            validations: true,
            statusCondition: {
                  key: 'rsbStatus',
                  value: [9]
                },
             conditon: {
                 key: 'recordIndicator',
                 value: [0]
             }
         },
         {
             name: 'approve',
             icon: 'done',
             actionType: 'approve',
             validations: true,
             conditon: {
                 key: 'recordIndicator',
                 value: [1,2,3]
             }
         },
         {
           name: 'reject',
           icon: 'do_not_disturb_alt',
           actionType: 'reject',
           validations: true,
           conditon: {
               key: 'recordIndicator',
               value: [1, 2, 3]
           }
         },
         {
             name: 'review',
             icon: 'fact_check',
             actionType: 'review',
             validations: true,
             conditon: {
                 key: 'recordIndicator',
                 value: [1]
             }
         }*/
  ],
};

export const accountBucketsDetailsHeaders = [
  {
    name: "",
    type: "checkbox",
    validations: true,
    style: {
      width: "1%",
    },
  },
  {
    name: "Ac Bucket Code",
    type: "text",
    validations: true,
    style: {
      //            width: '10%'
    },
  },
  {
    name: "Ac Bucket Description",
    type: "text",
    validations: true,
    style: {
      width: "20%",
    },
  },
  {
    name: "Amount Start",
    type: "text",
    validations: true,
    style: {
      //            width: '10%'
    },
  },
  {
    name: "Amount End",
    type: "text",
    validations: true,
    style: {
      //            width: '10%'
    },
  },
  {
    name: "Status",
    type: "text",
    validations: true,
    style: {
      //            width: '10%'
    },
  },
  {
    name: "Record Indicator",
    type: "text",
    validations: true,
    style: {
      //            width: '10%'
    },
  },
  {
    name: "Maker Name",
    type: "text",
    validations: true,
    style: {
      //            width: '10%'
    },
  },
  {
    name: "Verifier Name",
    type: "text",
    validations: true,
    style: {
      //           width: '10%'
    },
  },
];
