import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountBucketsComponent } from './AccountBuckets.component';

describe('AccountBucketsComponent', () => {
  let component: AccountBucketsComponent;
  let fixture: ComponentFixture<AccountBucketsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountBucketsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountBucketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
