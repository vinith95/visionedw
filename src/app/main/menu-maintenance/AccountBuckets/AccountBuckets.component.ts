import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ApiService, GlobalService } from 'src/app/service';
import { GenericPopupComponent } from 'src/app/shared/generic-popup/generic-popup.component';
import { MagnifierComponent } from 'src/app/shared/magnifier/magnifier.component';
import { ReviewComponent } from 'src/app/shared/review/review.component';
import { environment } from 'src/environments/environment';
import { tableDetail, accountBucketsDetailsHeaders } from './data.js'


@Component({
				  selector: 'app-accountBuckets',
				  templateUrl: './AccountBuckets.component.html',
  styleUrls: ['./AccountBuckets.component.css']
})


export class AccountBucketsComponent implements OnInit {
  tableDetail: any;
  showFilter: string = 'list';
  ngxTableConfig: any = {
    api: 'accountBucketsAllQueryResults',
    dataToDisplay: [],
    filterApi: 'accountBucketsAllQueryResults',
    count: 10,
    homeDashboard: []
  };
  resetModel: any = { key: '' };
  tabList: any = [
    {
      tabIcon: 'settings',
      tabName: 'accountBuckets',
      selected: true
    }
  ];
  lineList = [];
  pageView: any;
  accountBucketsForm: FormGroup;
  compareForm = {};
  submitted: boolean = false;
  accountBucketsDetailsHeaders: any [];
  submittedArr: boolean;
  editData: any[];
  editIndex: any;

// Define dropdown values
  acBucketApplicationCodeNtList: any[];
  acBucketStatusNtList: any[];
  recordIndicatorNtList: any[];

constructor(private apiService: ApiService,
     private fb: FormBuilder,
     private modalService: NgbModal,
     public globalService: GlobalService,
     private translate: TranslateService) {
      this.globalService.getLanguage().subscribe(lang => {
        this.translate.setDefaultLang(lang);
        this.translate.use(lang);
        // GET LANGUAGE
        // this.translate.currentLoader.getTranslation(this.translate.currentLang)
        //   .subscribe(data => {
        //   });
      });
      }

  ngOnInit(): void {
    this.tableDetail = tableDetail;
    this.accountBucketsDetailsHeaders = accountBucketsDetailsHeaders;
    this.pageLoadValues();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
      document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
      document.getElementById('card-body').children[0]['style'].height = '100%';
    }, 100);
  }

  pageLoadValues() {
    this.apiService.get(environment.accountBucketsPageload).subscribe(resp => {
      if (resp['status'] == 1 && resp['response'] != null) {
        this.acBucketStatusNtList = resp['response'][0];
        this.recordIndicatorNtList = resp['response'][1];
        this.acBucketApplicationCodeNtList = resp['response'][2];
      }
    })
  }
 tableAction(e) {
    this.lineList = e.lineList;
    this.pageView = e.type;
    if (e.type == 'add') {
      this.addForm(e);
      // this.formPageLoadValue('add');
    }
    if (e.type == 'edit' || e.type == 'view'|| e.type == 'copy') {
      this.editIndex = e.index;
      this.initForm();
      this.editForm(e)
    }
    if (e.type == 'refresh' || e.type == 'pagination') {
      this.resetModel = Object.assign({}, { key: 'reload' });
    }
    // if(e.type == 'delete') {
    //   this.openGenericPopup(e);
    // }
  }

  addForm(e) {
    this.initForm();
    this.formArrProduct.push(this.accountBucketsDetails())
    this.showFilter = 'form';
    this.compareForm = this.accountBucketsForm.getRawValue();
  }
// Form Inialize
  initForm() {
    this.accountBucketsForm = this.fb.group({
      acBucketApplicationCode: ['', [Validators.required]],
//      acBucketCode: ['', [Validators.required]],
//      acBucketDescription: ['', [Validators.required]],
//      amountStart: ['', [Validators.required]],
//      amountEnd: ['', [Validators.required]],
//      acBucketStatus: ['', [Validators.required]],
//      recordIndicator: ['', [Validators.required]],
//      maker: ['', [Validators.required]],
//      verifier: ['', [Validators.required]],
//      dateLastModified: ['', [Validators.required]],
//      dateCreation: ['', [Validators.required]],
				headerCheckbox: [false],
      accountBucketsDetails: this.fb.array([]),
    });
  }
// Edit Form
 editForm(e) {
    let data = {
      acBucketApplicationCode: e.data.acBucketApplicationCode,
      acBucketCode: e.data.acBucketCode,
    }
    this.apiService.post(environment.accountBucketsQueryDetails, data).subscribe(resp => {
      if (resp['status'] == 1 ) {
        this.showFilter = 'form';
        this.editData = resp['response'] && resp['response'].length ? resp['response'] : [];
        if(this.editData.length) {
          let accountBucketsDetails = [];
          this.editData.forEach((accountBuckets, arrindx)=> {
            accountBuckets['sequence'] = arrindx +1;
            this.formArrProduct.push(this.accountBucketsDetails());
            if(e.type != 'copy') {
              this.formArrProduct.at(arrindx).get('recordIndicator').disable();
//              this.formArrProduct.at(arrindx).get('acBucketApplicationCode').disable()
              this.formArrProduct.at(arrindx).get('acBucketCode').disable()
            }
            accountBucketsDetails.push({
              checked: false,
              acBucketCode: accountBuckets.acBucketCode,
              acBucketDescription: accountBuckets.acBucketDescription,
              amountStart: accountBuckets.amountStart,
              amountEnd: accountBuckets.amountEnd,
              acBucketStatus: accountBuckets.acBucketStatus.toString(),
              recordIndicator: accountBuckets.recordIndicator.toString(),
							makerName: accountBuckets.makerName,
              verifierName: accountBuckets.verifierName,
              maker: accountBuckets.maker,
              verifier: accountBuckets.verifier,
              sequence: arrindx +1
            })
          })
          this.accountBucketsForm.patchValue({
            acBucketApplicationCode: this.editData[0]['acBucketApplicationCode'].toString(),
            headerCheckbox: false,
            accountBucketsDetails: accountBucketsDetails
          });
        }
        if (e.type != 'edit' && e.type != 'copy') {
          this.accountBucketsForm.disable();
        }
        else {
          if(e.type != 'copy'){
            this.accountBucketsForm.get('acBucketApplicationCode').disable();
//            this.accountBucketsForm.get('acBucketCode').disable();
          }
        }
      }
    })
  }
	// FormArray  Function//
  get formArrProduct() {
    return this.accountBucketsForm.get('accountBucketsDetails') as FormArray;
  }

  accountBucketsDetails() {
    return this.fb.group({
      sequence: [this.formArrProduct.length + 1],
      checked: [false],
      acBucketCode: ['', [Validators.required,Validators.pattern("^[A-Z0-9._-]+$")]],
      acBucketDescription: ['', [Validators.required]],
      amountStart: ['', [Validators.required]],
      amountEnd: ['', [Validators.required]],
      acBucketStatus: ['', 0],
      recordIndicator: [''],
      makerName: [{value: '', disabled: true}],
      verifierName: [{value: '', disabled: true}],
      maker: [''],
      verifier: ['']
    });
  }
 addRow() {
    if (!this.formArrProduct.invalid) {
      this.submittedArr = false;
      this.formArrProduct.push(this.accountBucketsDetails())
      this.accountBucketsForm.patchValue({
        headerCheckbox: this.accountBucketsForm.getRawValue()['accountBucketsDetails'].every(e=> e.checked) ? true : false
      })
    }
    else {
      this.submittedArr = true;
    }
  }
  removeGlMatrix(index) {
    if (this.formArrProduct.value.length > 1) {
      this.formArrProduct.removeAt(index);
    }
    else {
      this.globalService.showToastr.warning('One row mandatory')
    }
  }

  headerChange(type) {
    if (type == 'overall') {
      for (let i = 0; i < this.accountBucketsForm.getRawValue()['accountBucketsDetails'].length; i++) {
        this.formArrProduct.at(i).patchValue({
          'checked': this.accountBucketsForm.getRawValue()['headerCheckbox']
        })
      }
    }
    if (type == 'individual') {
      this.accountBucketsForm.patchValue({
        headerCheckbox: this.accountBucketsForm.getRawValue()['accountBucketsDetails'].every(e=> e.checked) ? true : false
      })
    }
  }
 onSubmit(type) {
    if (type == 'summary') {
      this.submitted = false;
      this.showFilter = 'list';
    }
    if(type == 'save' || type == 'modify'){
      if (this.accountBucketsForm.invalid) {
        this.submitted = true
      }
      else {
        let rawValue = this.accountBucketsForm.getRawValue();
        if(rawValue['accountBucketsDetails'].every(e => !e.checked)) {
          this.globalService.showToastr.warning('No records are selected to perform an action');
        }
        else {
          if (type == 'save' && rawValue['accountBucketsDetails'] &&
            rawValue['accountBucketsDetails'].some(e => e.checked && e.acBucketStatus.toString() == '1')) {
            this.globalService.showToastr.warning('Only Active record is allowed to Add');
          }
          else {
            let checkArr = type == 'modify' ? this.editData.map(e => e.sequence) : [];
            rawValue['accountBucketsDetails'].forEach(ele => {
              if (!checkArr.includes(ele['sequence']))
                ele['newRecord'] = true;
            })
            if (type == 'modify' && rawValue['accountBucketsDetails'] &&
              rawValue['accountBucketsDetails'].some(e => e.checked && e.newRecord && e.acBucketStatus.toString()== '1')) {
              this.globalService.showToastr.warning('Only Active record is allowed to Add');
            }
            else if (type == 'modify' && rawValue['accountBucketsDetails'] &&
              rawValue['accountBucketsDetails'].some(e => e.checked && e.recordIndicator.toString() == '3')) {
              this.globalService.showToastr.warning('Please Uncheck Delete Pending records from your Selection');
            }
            else {
              this.saveLine(type)
            }
          }
        }
      }
    }
    if (type == 'next' || type == 'previous' || type == 'last' || type == 'first') {
      this.navigateFn(type);
    }
    if (type == 'approve' || type == 'reject' || type == 'delete') {
      // this.deleteInForm(type);
      // this.bulkApproveRejectFn(type);
      let data = [];
      let rawValue = this.accountBucketsForm.getRawValue();
      let checkArr = this.editData.map(e=> e.sequence);
      rawValue['accountBucketsDetails'].forEach(ele=> {
        if(!checkArr.includes(ele['sequence']))
        ele['newRecord'] = true;
      })
      if (rawValue['accountBucketsDetails'].every(e => !e.checked)) {
        this.globalService.showToastr.warning('No records are selected to perform an action');
      }
      else if(rawValue['accountBucketsDetails'].some(e=> e.checked && e.newRecord)) {
        this.globalService.showToastr.warning('Invalid records are selected to perform an action');
      }
      else {
        rawValue['accountBucketsDetails'].forEach((element, i) => {
          if (element.checked) {
            data.push({

			        acBucketApplicationCode: rawValue.acBucketApplicationCode,

			        acBucketCode: element.acBucketCode,
			        acBucketDescription: element.acBucketDescription,
			        amountStart: element.amountStart,
			        amountEnd: element.amountEnd,
			        acBucketStatus: element.acBucketStatus,
              checked: true,
              recordIndicator: element.recordIndicator
            })
          }
        })
        // type == 'delete' ? data.forEach(ele => { delete ele['recordIndicator'] }) : ''
        if (type == 'approve') {
          if (Array.isArray(data) && data.length > 0) {
            let isDataContainsApproved = data.some(x => {
              return (x.recordIndicator == '0');
            });
            let differFlag = 0;
            rawValue['accountBucketsDetails'].forEach(element => {
              if (element.checked && element.maker == JSON.parse(localStorage.getItem('themeAndLanguage'))['visionId'])
                differFlag++;
            });
            if (isDataContainsApproved) {
              this.globalService.showToastr.warning('Please Uncheck Approved records from your Selection');
            }
            else if (differFlag != 0) {
              this.globalService.showToastr.warning('Maker cannot Approve Pending Records !! ');
            }
            else {
              this.bulkApproveRejectFn(type, data);
            }
          }
        }
        if (type == 'reject') {
          if (Array.isArray(data) && data.length > 0) {
            let isDataContainsApproved = data.some(x => {
              return (x.recordIndicator == '0');
            });
            if (isDataContainsApproved) {
              this.globalService.showToastr.warning('Please Uncheck Approved records from your Selection');
            }
            else {
              this.bulkApproveRejectFn(type, data);
            }
          }
        }
        if (type == 'delete') {
          if (Array.isArray(data) && data.length > 0) {
            let isDataContainsApproved = data.some(x => {
              return (x.recordIndicator == '1' || x.recordIndicator == '2' || x.recordIndicator == '3' || x.recordIndicator == '4');
            });
            if (isDataContainsApproved) {
              this.globalService.showToastr.warning('Please Uncheck Pending records from your Selection');
            }
            else {
              data.forEach(ele => { delete ele['recordIndicator'] });
              this.bulkApproveRejectFn(type, data);
            }
          }
        }
      }
    }
    if(type == 'clear'){
      if (this.compareForm != JSON.stringify(this.accountBucketsForm.getRawValue())) {
      this.clearField();
      }
    }
  }
clearField() {
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false,
      windowClass: 'genericPopup'
    });
    modelRef.componentInstance.title = 'Alert';
    modelRef.componentInstance.message = 'Are you sure, you want to Clear ?';
    modelRef.componentInstance.popupType = 'Confirm';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.userConfirmation.subscribe(resp => {
      if (resp['type'] == 'Yes') {
        this.accountBucketsForm.reset();
        let e = {type: 'add'}
        this.addForm(e);
      }
      modelRef.close();
    });
  }

  bulkApproveRejectFn (type, data){
    document.body.classList.add('genericPopup');
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false,
      windowClass: 'genericPopup'
    });
    let prompt = '';
    if(type == 'approve') {
      prompt = 'Are you sure, you want to approve these records?'
    }
    if(type == 'reject') {
      prompt = 'Are you sure, you want to reject these records?'
    }
    if(type == 'delete') {
      prompt = 'Are you sure, you want to delete these records?'
    }
    modelRef.componentInstance.title = 'Alert';
    modelRef.componentInstance.message = prompt;
    modelRef.componentInstance.popupType = 'Confirm';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.userConfirmation.subscribe(resp => {
      if (resp['type'] == 'Yes') {
        this.bulkApproveRejectApi(type, data);
        // this.resetModel = Object.assign({}, { key: 'reload' });
      }
      document.body.classList.remove('genericPopup');
      modelRef.close();
    });
  }
 bulkApproveRejectApi (type, data) {
    // let data = [];
    let rawValue = this.accountBucketsForm.getRawValue();
    let apiUrl = type == 'approve' ? environment.bulkApproveAccountBuckets : type == 'reject' ?  environment.bulkRejectAccountBuckets
    : environment.deleteAccountBuckets;
    this.apiService.post(apiUrl, data).subscribe(resp => {
      if (resp['status']) {
        if(rawValue['accountBucketsDetails'].length == data.length) {
          this.resetModel = Object.assign({}, { key: 'reload' });
          this.onSubmit('summary');
        }
        else {
          let e = {
            type: 'edit',
            data: this.editData[0],
            index: this.editIndex
          }
          this.tableAction(e);
        }
        this.globalService.showToastr.success(resp['message'])
      }
      else {
        this.globalService.showToastr.error(resp['message'])
      }
    })
  }
  saveLine(type) {
    let apiUrl = (type == 'modify') ? environment.modifyAccountBuckets : environment.addAccountBuckets;
    let rawValue = this.accountBucketsForm.getRawValue();
    let data = [];
    if(type == 'modify') {
      let checkArr = this.editData.map(e=> e.sequence);
      rawValue['accountBucketsDetails'].forEach(ele=> {
        if(!checkArr.includes(ele['sequence']))
        ele['newRecord'] = true;
      })
    }
    rawValue['accountBucketsDetails'].forEach(formArr=> {
      if(formArr.checked)
      data.push({
        acBucketApplicationCode: rawValue.acBucketApplicationCode,

        acBucketCode: formArr.acBucketCode,
        acBucketDescription: formArr.acBucketDescription,
        amountStart: formArr.amountStart,
        amountEnd: formArr.amountEnd,
        acBucketStatus: formArr.acBucketStatus,

        newRecord: formArr.newRecord ? true : false,
        checked: formArr.checked})
    });
    this.apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp['status'] == 1) {
        if (type == 'save') {
          this.afterSave();
        }
        else {
          let e = {
            type: 'edit',
            data: this.editData[0],
            index: this.editIndex
          }
          this.tableAction(e)
        }
        this.globalService.showToastr.success(resp['message'])
      }
      else {
        this.globalService.showToastr.error(resp['message'])
      }
    })
  }
afterSave() {
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false,
      windowClass: 'genericPopup'
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "Do you want to Add more record ?";
    modelRef.componentInstance.popupType = 'Confirm';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.userConfirmation.subscribe(resp => {
      if (resp['type'] == 'Yes') {
        let e = { type: 'add', lineList: this.lineList }
          this.submitted = false
          this.tableAction(e)
      }
      else {
        this.onSubmit('summary')
      }
      modelRef.close();
    });
  }

  navigateFn(type) {
    this.accountBucketsForm.reset();
    if (type == 'next') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'previous') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'last') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'first') {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
  }

  disableButton(name) {
    if (name == 'first') {
      return !this.editIndex ? true : false;
    }
    if (name == 'last') {
      return this.lineList.length == 1 ? true : this.editIndex == (this.lineList.length - 1) ? true : false;
    }
  }
reviewPopup(index) {
    let rawValue = this.accountBucketsForm.getRawValue();
    let rowVal = rawValue['accountBucketsDetails'][index];
    let data = {
      acBucketApplicationCode: rawValue.acBucketApplicationCode,
      acBucketCode: rowVal.acBucketCode,
    }
    this.apiService.post(environment.reviewAccountBuckets, data).subscribe((resp) => {
      if (resp['status'] == 1) {
        const modelRef = this.modalService.open(ReviewComponent, {
          size: 'lg',
          backdrop: 'static',
          windowClass: 'review-popup'
        });
        modelRef.componentInstance.response = resp["response"];
        modelRef.componentInstance.closePopup.subscribe((e) => {
          modelRef.close();
        });
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    })

  }

  checkIsNew(i) {
    if (this.pageView != 'add') {
      let rawValue = this.accountBucketsForm.getRawValue();
      let checkArr = this.editData.map(e => e.sequence);
      if (!checkArr.includes(rawValue['accountBucketsDetails'][i]['sequence']))
        return true;
      else
        return false;
    }
    else {
      return true;
    }
    // rawValue['accountBucketsDetails'].forEach(ele=> {
    //   if(!checkArr.includes(ele['capLedgerId']))
    //   return true
    // })
  }

  checkApprove() {
    let rawValue = this.accountBucketsForm ? this.accountBucketsForm.getRawValue() : {};
    return rawValue['accountBucketsDetails'] && rawValue['accountBucketsDetails'].some(e=> e.recordIndicator && e.recordIndicator != '' &&
    e.recordIndicator.toString() != '0')
  }

  checkDelete() {
    let rawValue = this.accountBucketsForm ? this.accountBucketsForm.getRawValue() : {};
    return rawValue['accountBucketsDetails'] && rawValue['accountBucketsDetails'].some(e=> e.recordIndicator && e.recordIndicator != '' &&
    e.recordIndicator.toString() == '0')
  }

  keyDisabled(event) {
    event.preventDefault();
  }
}
