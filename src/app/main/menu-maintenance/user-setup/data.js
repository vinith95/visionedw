export const tableDetail = {
  headers: [
    {
      name: "Vision ID",
      referField: "visionId",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Login ID",
      referField: "userLoginId",
      filter: false,
      isFilterField: true,
    },
    {
      name: "User Name",
      referField: "userName",
      filter: false,
      isFilterField: true,
    },
    {
      name: "User Group",
      referField: "userGroup",
      filter: false,
      isFilterField: true,
    },
    {
      name: "User Profile",
      referField: "userProfile",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Status",
      referField: "userStatusDesc",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
    {
      name: "RecordIndicator",
      referField: "recordIndicatorDesc",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Maker",
      referField: "maker",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Verifier",
      referField: "verifier",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Date Creation",
      referField: "dateCreation",
      filter: false,
      isFilterField: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
    },
    {
      name: "delete",
      icon: "delete",
      actionType: "delete",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 3, 4],
      },
    },
  ],
};
