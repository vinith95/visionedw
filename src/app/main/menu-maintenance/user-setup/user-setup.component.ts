import { Component, ElementRef, ChangeDetectorRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { GlobalService, ApiService, JwtService } from '../../../service';
import { OnClickService } from '../../../service/on-click.service';

import { MagnifierComponent } from '../../../shared/magnifier/magnifier.component';
import { tableDetail } from './data.js';
import { environment } from '../../../../environments/environment';
import { GenericPopupComponent } from '../../../shared/generic-popup/generic-popup.component';
import { isNullOrUndefined } from 'util';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as query from '../../../../assets/data/queryDetail.json';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { debug } from 'console';

@Component({
  selector: 'app-user-setup',
  templateUrl: './user-setup.component.html',
  styleUrls: ['./user-setup.component.css']
})


export class UserSetupComponent implements OnInit {
  showFilter: any;
  tableDetail;
  resetModel: any = { key: '' };
  queryIdDetail: any = query['default'];
  leftContainerHeight: string;
  ngxTableConfig: any = {
    api: '',
    dataToDisplay: [],
    filterApi: '',
    count: 10,
    homeDashboard: []
  };
  tabList: any = [
    {
      tabIcon: "person",
      tabName: "UserSetup",
      selected: true,
      showBackbutton: true,
    },
    {
      tabIcon: "settings",
      tabName: "accessControl",
      selected: false
    }
  ];
  private formData: FormData = new FormData();
  lineForm: any;
  userGroupList: any;
  userProfileList: any;
  recordIndicatorList: any;
  statusList: any;
  countryList: any;
  leBookList: any;
  legalVehicleList: any;
  regionProvinceList: any;
  sbuCodeList: any;
  oucAttributeList: any;
  productSuperGroupList: any;
  productAttributeList: any;
  accountOfficerList: any;
  dropdownSettings: IDropdownSettings = {
    singleSelection: true,
    idField: 'alphaSubTab',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    noDataAvailablePlaceholderText: 'No data available',
    enableCheckAll: false,
    itemsShowLimit: 2,
    maxHeight: 150,
    allowSearchFilter: true
  };
  singleDropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'alphaSubTab',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 1,
    noDataAvailablePlaceholderText: 'No data available',
    maxHeight: 150,
    allowSearchFilter: true,
  };
  gcidAccessList = [{
    alphaSubTab: "Y",
    description: "Yes"
  },
  {
    alphaSubTab: "N",
    description: "No"
  }]
  businessGroupList: any;
  selectedTab = 'UserSetup';
  lineList: any;
  pageView: any;
  showForm: boolean;
  submitted: boolean = false;
  compareForm: string;
  editData: any;
  appendOriginText: any;
  stfcountryList: any[];
  stfleBookList: any[];
  staffIdList: any[];
  applicationAccessList: any = [];
  previousPage: any;
  editIndex: any;
  imgSrc: any;
  constructor(
    private fb: FormBuilder,
    private globalService: GlobalService,
    private _apiService: ApiService,
    private http: HttpClient,
    private onClickService: OnClickService,
    private translate: TranslateService,
    private jwtService: JwtService,
    private modalService: NgbModal,
    private cdRef: ChangeDetectorRef,
    private router: Router) {
    this.globalService.getLanguage().subscribe(lang => {
      this.translate.setDefaultLang(lang);
      this.translate.use(lang);
      // GET LANGUAGE
      this.translate.currentLoader.getTranslation(this.translate.currentLang)
        .subscribe(data => {
        });
    });
  }

  ngOnInit() {
    this.globalService.getLanguage().subscribe(lang => {
      this.translate.setDefaultLang(lang);
      this.translate.use(lang);
      // GET LANGUAGE
      this.translate.currentLoader.getTranslation(this.translate.currentLang)
        .subscribe(data => {
        });
      this.tableDetail = tableDetail;
      let data = {
        actionType: "Query",
        currentPage: 1,
        maxRecords: 5000
      }
      this._apiService.post(environment.getAllQueryResults, data).subscribe((resp) => {
        this.lineList = resp['response'];
        this.getPageLoadValues();
      })
    })

  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  getPageLoadValues = () => {
    if (this.showFilter == 'form') {
      this.showFilter = 'form'
    }
    else {
      this.showFilter = 'list';
    }
  }

  // ------------------------------------------------------------
  // table Function
  // ------------------------------------------------------------
  tableAction(e) {
    this.initForm();
    this.pageView = e.type;
    if (e.type == 'add') {
      this.showForm = true;
      this.formPageLoadValue('add');
    }
    if (e.type == 'edit' || e.type == 'view' || e.type == 'delete' || e.type == 'preview' || e.type == 'approve' || e.type == 'reject') {
      this.showForm = true;
      this.editIndex = e.index;
      this.editViewInit(e, e.type)
    }
    if (e.type == 'refresh' || e.type == 'pagination') {
      this.showFilter = ''
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 }
      this._apiService.post(environment.getAllQueryResults, data).subscribe((resp) => {
        this.lineList = resp['response'];
        this.ngxTableConfig['filterData'] = e.filter
        this.getPageLoadValues();
      })
    }
  }

  initForm() {
    this.lineForm = this.fb.group({
      visionId: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      userName: ['', [Validators.required, Validators.pattern('^[a-zA-Z 0-9._-]*$')]],
      userLoginId: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._-]*$')]],
      userEmailId: ['', [Validators.required]],
      userGroup: ['', [Validators.required]],
      userProfile: ['', [Validators.required]],
      stfcountry: [''],
      stfleBook: [''],
      staffId: [''],
      lastActivityDate: [{ value: '', disabled: true }],
      userStatusDate: [{ value: '', disabled: true }],
      applicationAccess: [''],
      userStatus: [''],
      updateRestriction: [true, [Validators.required]],
      legalVehicle: [''],
      country: [''],
      leBook: [''],
      regionProvince: [''],
      businessGroup: [''],
      sbuCode: [''],
      oucAttribute: [''],
      productSuperGroup: [''],
      productAttribute: [''],
      accountOfficer: [''],
      gcidAccess: ['N', [Validators.required]],
    });

  }


  formPageLoadValue(type) {
    this._apiService.get(environment.pageLoadValues).subscribe((resp) => {
      this.statusList = resp['response'][0];
      this.recordIndicatorList = resp['response'][1];
      this.applicationAccessList = resp['response'][2];
      this.appendOriginText = resp['response'][3];

      if (type != 'add') {
        this.editInitForm(type);
      }
      else {
        this.lineForm.patchValue({
          userStatus: "0"
        })
        this.lineForm.get('userStatus').disable();
        this.showFilter = 'form';
        this.compareForm = JSON.stringify(this.lineForm.getRawValue());
      }
    });
  }

  editViewInit(e, type) {
    this.showFilter = '';
    let data = {
      visionId: e.data.visionId,
    };
    data['isReview'] = type == 'preview' ? true : false;
    this._apiService.post(environment.getQueryDetails, data).subscribe((resp) => {
      if (!isNullOrUndefined(resp['response'])) {
        this.editData = resp['response'];
        if (!isNullOrUndefined(this.editData.fileNmae) && this.editData.fileNmae != "") {
          const formData = new FormData();
          formData.append('downloadFileName', this.editData.fileNmae);

        }
        this.userGroupList = this.onClickService.editHitNew('userGroup', 'userSetup', this.editData, []);
        this.userProfileList = this.onClickService.editHitNew('userProfile', 'userSetup', this.editData, []);
        this.sbuCodeList = this.onClickService.editHitNew('sbuCode', 'userSetup', this.editData, []);
        this.stfcountryList = this.onClickService.editHitNew('stfcountry', 'userSetup', this.editData, []);
        this.stfleBookList = this.onClickService.editHitNew('stfleBook', 'userSetup', this.editData, []);
        this.staffIdList = this.onClickService.editHitNew('staffId', 'userSetup', this.editData, []);

        this.legalVehicleList = this.onClickService.editHitNew('legalVehicle', 'userSetup', this.editData, []);


        this.regionProvinceList = this.onClickService.editHitNew('regionProvince', 'userSetup', this.editData, []);
        this.productSuperGroupList = this.onClickService.editHitNew('productSuperGroup', 'userSetup', this.editData, []);
        this.productAttributeList = this.onClickService.editHitNew('productAttribute', 'userSetup', this.editData, []);
        this.accountOfficerList = this.onClickService.editHitNew('accountOfficer', 'userSetup', this.editData, []);
        this.oucAttributeList = this.onClickService.editHitNew('oucAttribute', 'userSetup', this.editData, []);
        this.countryList = this.onClickService.editHitNew('country', 'userSetup', this.editData, []);
        this.leBookList = this.onClickService.editHitNew('leBook', 'userSetup', this.editData, []);
        this.businessGroupList = this.onClickService.editHitNew('businessGroup', 'userSetup', this.editData, []);
        this.applicationAccessList = this.onClickService.editHitNew('applicationAccess', 'userSetup', this.editData, []);
      }
      this.formPageLoadValue(type);
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + 'px';
      document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
      document.getElementById('card-body').children[0]['style'].height = '100%';
    });
  }
  changeFields(fieldName) {
    if (fieldName == 'userGroup') {
      this.lineForm.patchValue({ userProfile: '' })
    }
    if (fieldName == 'stfcountry') {
      this.lineForm.patchValue({ stfleBook: '' })
    }
    if (fieldName == 'country') {
      this.lineForm.patchValue({ oucAttribute: [], leBook: [] })
    }
    if (fieldName == 'leBook') {
      this.lineForm.patchValue({ oucAttribute: [] })
    }
  }


  updateRestrictionChange(value) {
    if (this.lineForm.controls['updateRestriction'].value == false) {
      this.setDefaultValue();
    }
  }




  multiSelectValue(data, field) {
    let arr = data.split(',')
    let listData =
      field == 'applicationAccess' ? this.applicationAccessList :
        field == 'country' ? this.countryList :
          field == 'leBook' ? this.leBookList :
            field == 'productAttribute' ? this.productAttributeList
              : field == 'legalVehicle' ? this.legalVehicleList :
                field == 'accountOfficer' ? this.accountOfficerList
                  : field == 'regionProvince' ? this.regionProvinceList :
                    field == 'oucAttribute' ? this.oucAttributeList

                      : field == 'productSuperGroup' ? this.productSuperGroupList : this.businessGroupList
    let returnArr = [];
    if (listData.length) {
      listData.forEach(element1 => {
        arr.forEach(element => {
          if (element == element1.alphaSubTab)
            returnArr.push(element1)
        });
      });
    }
    return returnArr;
  }

  editInitForm(type) {
    this.lineForm.patchValue({
      visionId: this.editData.visionId,
      userName: this.editData.userName,
      userLoginId: this.editData.userLoginId,
      userEmailId: this.editData.userEmailId,
      lastActivityDate: this.editData.lastActivityDate,
      userGroup: this.editData.userGroup,
      userProfile: this.editData.userProfile,
      legalVehicle: this.multiSelectValue(this.editData.legalVehicle, 'legalVehicle'),
      country: this.multiSelectValue(this.editData.country, 'country'),
      leBook: this.multiSelectValue(this.editData.leBook, 'leBook'),
      regionProvince: this.multiSelectValue(this.editData.regionProvince, 'regionProvince'),
      businessGroup: this.multiSelectValue(this.editData.businessGroup, 'businessGroup'),
      productSuperGroup: this.multiSelectValue(this.editData.productSuperGroup, 'productSuperGroup'),
      oucAttribute: this.multiSelectValue(this.editData.oucAttribute, 'oucAttribute'),
      sbuCode: this.editData.sbuCode,
      productAttribute: this.multiSelectValue(this.editData.productAttribute, 'productAttribute'),
      accountOfficer: this.multiSelectValue(this.editData.accountOfficer, 'accountOfficer'),
      userStatus: this.editData.userStatus.toString(),
      userStatusDate: this.editData.userStatusDate,
      gcidAccess: this.editData.gcidAccess,
      staffId: this.editData.staffId,
      stfcountry: this.editData.stfcountry,
      stfleBook: this.editData.stfleBook,
      applicationAccess: this.multiSelectValue(this.editData.applicationAccess, 'applicationAccess'),
      updateRestriction: this.editData.updateRestriction == 'Y' ? true : false,
    });


    if (type != 'edit') {
      this.formAllDisabled();
    }
    else {
      this.lineForm.get('visionId').disable();
      // if (this.editData.updateRestriction == "Y") {
      //   this.setDefaultValue();
      // } else {

      // }
    }
    this.showFilter = 'form';
    this.compareForm = JSON.stringify(this.lineForm.getRawValue());
    setTimeout(() => {
      this.readURLEdit(this.imgSrc);
    }, 70);

  }

  setDefaultValue() {
    if (this.editData.legalVehicle == "") {
      this.lineForm.patchValue({
        legalVehicle: this.multiSelectValue(this.legalVehicleList[0]['alphaSubTab'], 'legalVehicle')
      })
    }
    if (this.editData.country == "") {
      this.lineForm.patchValue({
        country: this.multiSelectValue(this.countryList[0]['alphaSubTab'], 'country')
      })
      this.editData['country'] = this.multiSelectValue(this.countryList[0]['alphaSubTab'], 'country')[0]['alphaSubTab'];
      this.leBookList = this.onClickService.editHitNew('leBook', 'userSetup', this.editData, []);
    }

    setTimeout(() => {
      if (this.editData.leBook == "") {
        this.lineForm.patchValue({
          leBook: this.multiSelectValue(this.leBookList[0]['alphaSubTab'], 'leBook')
        })
      }
      this.editData['leBook'] = this.multiSelectValue(this.leBookList[0]['alphaSubTab'], 'leBook')[0]['alphaSubTab'];
      this.oucAttributeList = this.onClickService.editHitNew('oucAttribute', 'userSetup', this.editData, []);


      setTimeout(() => {
        if (this.editData.oucAttribute == "") {
          this.lineForm.patchValue({
            oucAttribute: this.multiSelectValue(this.oucAttributeList[0]['alphaSubTab'], 'oucAttribute')
          })
        }

      }, 3000);


    }, 6000)
    if (this.editData.regionProvince == "") {
      this.lineForm.patchValue({
        regionProvince: this.multiSelectValue(this.regionProvinceList[0]['alphaSubTab'], 'regionProvince')
      })
    }
    if (this.editData.businessGroup == "") {
      this.lineForm.patchValue({
        businessGroup: this.multiSelectValue(this.businessGroupList[0]['alphaSubTab'], 'businessGroup')
      })
    }
    if (this.editData.productSuperGroup == "") {
      this.lineForm.patchValue({
        productSuperGroup: this.multiSelectValue(this.productSuperGroupList[0]['alphaSubTab'], 'productSuperGroup')
      })
    }
    if (this.editData.productAttribute == "") {
      this.lineForm.patchValue({
        productAttribute: this.multiSelectValue(this.productAttributeList[0]['alphaSubTab'], 'productAttribute')
      })
    }

    if (this.editData.sbuCode == "") {
      this.lineForm.patchValue({
        sbuCode: this.sbuCodeList[0].alphaSubTab
      })
    }

    if (this.editData.accountOfficer == "") {
      this.lineForm.patchValue({
        accountOfficer: this.multiSelectValue(this.accountOfficerList[0]['alphaSubTab'], 'accountOfficer')
      })
    }

  }

  disabledDropdown() {
    return this.pageView == 'view' ? true : false;
  }

  backGroundFlag(recordIndicator) {
    if (!recordIndicator) {
      return {
        "background": "#448847"
      }
    }
    else {
      return {
        "background": "#ffa828"
      }
    }

  }
  disableButton(name) {
    if (name == 'first') {
      return !this.editIndex ? true : false;
    }
    if (name == 'last') {
      return this.lineList.length == 1 ? true : this.editIndex == (this.lineList.length - 1) ? true : false;
    }
  }
  formAllDisabled() {
    this.lineForm.get('visionId').disable();
    this.lineForm.get('userName').disable();
    this.lineForm.get('userLoginId').disable();
    this.lineForm.get('userEmailId').disable();
    this.lineForm.get('lastActivityDate').disable();
    this.lineForm.get('userGroup').disable();
    this.lineForm.get('userProfile').disable();
    this.lineForm.get('legalVehicle').disable();
    this.lineForm.get('country').disable();
    this.lineForm.get('leBook').disable();
    this.lineForm.get('regionProvince').disable();
    this.lineForm.get('businessGroup').disable();
    this.lineForm.get('productSuperGroup').disable();
    this.lineForm.get('oucAttribute').disable();
    this.lineForm.get('sbuCode').disable();
    this.lineForm.get('productAttribute').disable();
    this.lineForm.get('accountOfficer').disable();
    this.lineForm.get('userStatus').disable();
    this.lineForm.get('userStatusDate').disable();
    this.lineForm.get('staffId').disable();
    this.lineForm.get('stfcountry').disable();
    this.lineForm.get('stfleBook').disable();
    this.lineForm.get('applicationAccess').disable();
    this.lineForm.get('updateRestriction').disable();
  }


  onSubmit(type) {
    if (type == 'preview') {
      this.previousPage = this.pageView;
      this.showFilter = '';
      let e = {
        type: "preview",
        data: this.editData,
        index: this.editIndex
      }
      this.tableAction(e)
    }
    if (type == "close") {
      this.showFilter = '';
      let e = {
        type: this.previousPage,
        data: this.editData,
        index: this.editIndex
      }
      this.tableAction(e)
    }
    if (type == 'next' || type == 'previous' || type == 'last' || type == 'first') {
      this.navigateFn(type);
    }
    if (type == 'clear') {
      if (this.compareForm != JSON.stringify(this.lineForm.getRawValue())) {
        this.clearField();
      }
    }
    if (type == 'summary') {
      this.selectedTab = "UserSetup"
      this.lineForm.reset();
      this.imgSrc = undefined;
      this.submitted = false
      this.lineForm = this.fb.group({})
      this.showFilter = 'list';
      this.ngOnInit();
    }

    if (type == 'delete') {
      let data = {
        visionId: this.editData.visionId
      }
      this._apiService.post(environment.deleteVisionUserSetup, data).subscribe((resp) => {
        if (resp['status']) {
          this.globalService.showToastr.success(resp['message']);
          this.onSubmit('summary')
        }
      })
    }

    if (type == 'approve' || type == 'reject') {
      let data = {
        type: type,
        data: this.editData
      }
      this.approveOrReject(data, false)
    }

    if (type == 'save' || type == 'modify') {
      if (this.lineForm.invalid) {
        this.submitted = true
      }
      else {
        this.saveLine(type);
      }
    }
  }

  navigateFn(type) {
    this.showFilter = '';
    this.lineForm.reset();
    this.submitted = false
    this.selectedTab = "UserSetup"
    this.imgSrc = undefined;
    this.lineForm = this.fb.group({})
    this.showForm = false;
    if (type == 'next') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1
      }
      this.tableAction(data)
    }
    if (type == 'previous') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1
      }
      this.tableAction(data)
    }
    if (type == 'last') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1
      }
      this.tableAction(data)
    }
    if (type == 'first') {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0
      }
      this.tableAction(data)
    }
  }

  clearField() {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "clearRecord";
    modelRef.componentInstance.popupType = 'Confirm';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.userConfirmation.subscribe(resp => {
      if (resp == 'Yes') {
        let e = { type: 'add' }
        this.tableAction(e)

      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }

  unLockUser() {
    const requestData = {
      visionId: this.editData.visionId,
    };
    this._apiService.post(environment.userSetupUnLock, requestData).subscribe((resp) => {
      if (resp['status']) {
        this.globalService.showToastr.success(resp['message']);
        let data = {
          type: this.pageView,
          data: this.lineList[this.editIndex],
          index: this.editIndex
        }
        this.tableAction(data)
      }
      else {
        this.globalService.showToastr.error(resp['message']);
      }
    })
  }
  approveOrReject(data, reload) {
    const requestData = {
      visionId: data.data.visionId,
    };
    let apiUrl = data.type == 'approve' ? environment.approveVisionUserSetup : environment.rejectVisionUserSetup;
    this._apiService.post(apiUrl, requestData).subscribe((resp) => {
      if (resp['status']) {
        this.globalService.showToastr.success(resp['message']);
        if (this.editData.recordIndicator == "2" && data.type == 'reject') {
          this.onSubmit('summary');
        }
        else {
          let e = {
            type: data.type,
            data: this.editData,
            index: this.editIndex
          }
          this.tableAction(e);
          setTimeout(() => {
            this.onSubmit('summary');
          }, 500)
        }
        if (reload)
          this.resetModel = Object.assign({}, { key: 'reload' });
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }
  createUserEmailId() {
    if (this.pageView == "add") {
      let userLoginId = this.lineForm.getRawValue()['userLoginId'];
      if (userLoginId != '') {
        this.lineForm.patchValue({
          userEmailId: userLoginId + this.appendOriginText
        })
      }
    }
  }
  arraySrtingCon(data) {
    if (data.length) {
      let arr = [];
      data.forEach(element => {
        arr.push(`${element["alphaSubTab"]}`)
      });
      return arr.toString();
    }
    else {
      return '';
    }

  }
  saveLine(type) {
    let apiUrl = type == 'modify' ? environment.modifyVisionUserSetup : environment.addVisionUserSetup

    let rawValue = this.lineForm.getRawValue();

    if (rawValue.legalVehicle == ""
      && rawValue.country == ""
      && rawValue.leBook == ""
      && rawValue.regionProvince == ""
      && rawValue.businessGroup == ""
      && rawValue.productSuperGroup == ""
      && rawValue.productAttribute == ""
      && rawValue.oucAttribute == ""
      && rawValue.sbuCode == ""
      && rawValue.accountOfficer == ""
    ) {
      this.globalService.showToastr.warning("Please Choose Any of these fields");
    }
    else {
      if (!isNullOrUndefined(this.imgSrc)) {
        this.formData = new FormData();
        const file = this.imgSrc
        this.formData.append('uploadFiles', file, rawValue.visionId);
        this.formData.append('visionId', rawValue.visionId);
        this._apiService.fileUploads(environment.uploadUserPhoto, this.formData).subscribe(resp => {
          if (resp['status']) {

          }
          else {
            this.globalService.showToastr.error(resp['message'])
          }
        })
      }
      let data = {
        visionId: rawValue.visionId,
        fileNmae: rawValue.visionId + '.png',
        userName: rawValue.userName,
        userLoginId: rawValue.userLoginId,
        userEmailId: rawValue.userEmailId,
        lastActivityDate: rawValue.lastActivityDate,
        userGroup: rawValue.userGroup,
        userProfile: rawValue.userProfile,
        legalVehicle: this.arraySrtingCon(rawValue.legalVehicle),
        country: this.arraySrtingCon(rawValue.country),
        leBook: this.arraySrtingCon(rawValue.leBook),
        regionProvince: this.arraySrtingCon(rawValue.regionProvince),
        businessGroup: this.arraySrtingCon(rawValue.businessGroup),
        productSuperGroup: this.arraySrtingCon(rawValue.productSuperGroup),
        oucAttribute: this.arraySrtingCon(rawValue.oucAttribute),
        sbuCode: rawValue.sbuCode,
        productAttribute: this.arraySrtingCon(rawValue.productAttribute),
        accountOfficer: this.arraySrtingCon(rawValue.accountOfficer),
        userStatus: rawValue.userStatus,
        userStatusDate: rawValue.userStatusDate,
        gcidAccess: rawValue.gcidAccess,
        staffId: rawValue.staffId,
        stfcountry: rawValue.stfcountry,
        stfleBook: rawValue.stfleBook,
        applicationAccess: this.arraySrtingCon(rawValue.applicationAccess),
        updateRestriction: rawValue.updateRestriction ? "Y" : "N",
      }
      this._apiService.post(apiUrl, data).subscribe((resp) => {
        if (resp['status']) {
          if (type == 'save') {
            this.afterSave();
          }
          else {
            this.showFilter = '';
            let e = {
              type: "edit",
              data: this.editData,
              index: this.editIndex
            }
            this.tableAction(e)
          }
          this.globalService.showToastr.success(resp['message'])
        }
        else {
          this.globalService.showToastr.error(resp['message'])
        }
      })
    }


  }
  afterSave() {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>'md',
      backdrop: false
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "addOneMoreRecord";
    modelRef.componentInstance.popupType = 'Confirm';
    modelRef.componentInstance.closeTime = 'No';
    modelRef.componentInstance.userConfirmation.subscribe(resp => {
      if (resp == 'Yes') {
        let e = { type: 'add' }
        this.submitted = false
        this.tableAction(e)
       // document.getElementById('imagePreview').style.cssText = "background-image:url(assets/img/bio-portrait-placeholder.jpg)";
        
      }
      else {
        this.onSubmit('summary')
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  // ngAfterViewInit() {
  //   setTimeout(() => {
  //     const getContentAreaHeight = window.innerHeight;
  //     const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
  //     let leftContainerHeight = getContentAreaHeight + 'px';
  //     document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
  //     document.getElementById('card-body').children[0]['style'].height = '100%';
  //   });
  // }

  checkfield(field, event) {
    if (field == 'applicationAccess' || field == 'country'
      || field == 'leBook' || field == 'productAttribute'
      || field == 'legalVehicle' || field == 'accountOfficer'
      || field == 'regionProvince' || field == 'oucAttribute'
      || field == 'productSuperGroup' || field == 'businessGroup') {
      return event.target.classList.contains('dropdown-btn') || event.target.classList.contains('dropdown-up') || event.target.parentNode.className == 'dropdown-btn'
    }
    else {
      return !event.target.closest('.ng-select-disabled')
    }
  }
  arrayToString(data) {
    if (!isNullOrUndefined(data)) {
      let arr = [];
      data.forEach(element => {
        arr.push(`'${element["alphaSubTab"]}'`)
      });
      return arr.toString().slice(1, -1);
    }
  }

  globalClick(event, field, index?) {
    let rawValues = this.lineForm.getRawValue();
    let rowValue = [];
    let promptArr = [];
    if (rawValues['country'] != '')
      rawValues['country'] = this.arrayToString(rawValues['country']);
    if (rawValues['leBook'] != '')
      rawValues['leBook'] = this.arrayToString(rawValues['leBook']);
    if (this.checkfield(field, event)) {
      if (this.onClickService.checkRequirement(field, 'userSetup', rawValues, rowValue)) {
        this.onClickService.onClickHitNew(field, 'userSetup', rawValues, rowValue).subscribe((resp) => {
          Object.keys(resp['response']).forEach((element, index1) => {
            promptArr.push({ alphaSubTab: element.split('@')[1], description: resp['response'][element] })
          });
          if (field == 'userGroup')
            this.userGroupList = promptArr;
          if (field == 'userProfile')
            this.userProfileList = promptArr;
          if (field == 'sbuCode')
            this.sbuCodeList = promptArr;
          if (field == 'country')
            this.countryList = promptArr;
          if (field == 'leBook')
            this.leBookList = promptArr;
          if (field == 'stfcountry')
            this.stfcountryList = promptArr;
          if (field == 'stfleBook')
            this.stfleBookList = promptArr;
          if (field == 'staffId')
            this.staffIdList = promptArr;
          if (field == 'legalVehicle')
            this.legalVehicleList = promptArr;
          if (field == 'regionProvince')
            this.regionProvinceList = promptArr;
          if (field == 'productSuperGroup')
            this.productSuperGroupList = promptArr;
          if (field == 'productAttribute')
            this.productAttributeList = promptArr;
          if (field == 'accountOfficer')
            this.accountOfficerList = promptArr;
          if (field == 'oucAttribute')
            this.oucAttributeList = promptArr;
          if (field == 'businessGroup')
            this.businessGroupList = promptArr;
          if (field == 'applicationAccess')
            this.applicationAccessList = promptArr;
        })
      }
      else {
        let vFormParam = this.queryIdDetail['userSetup'][field]["vFormParam"];
        vFormParam.forEach(element => {
          if (rawValues[element] == '')
            this.lineForm.controls[element].setErrors({ 'incorrect': true })
        });
      }
    }

  }

  tabClick(tab) {
    this.tabList.forEach((ele) => {
      ele.selected = false;
    })
    tab.selected = true;
    this.selectedTab = tab.tabName;
    if (this.selectedTab == 'UserSetup' && !isNullOrUndefined(this.imgSrc)) {
      this.readURLEdit(this.imgSrc);
    }
    if (this.selectedTab == 'accessControl') {

    }

  }
  readURLEdit(file) {
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        let tempstr = reader.result as string
        document.getElementById('imagePreview').style.cssText = "background-image:url(" + tempstr + ")";
      }
      reader.readAsDataURL(file)
    }
  }
  readURL(input) {
    const file = (input.target as HTMLInputElement).files[0];
    this.imgSrc = file;
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        let tempstr = reader.result as string
        document.getElementById('imagePreview').style.cssText = "background-image:url(" + tempstr + ")";
      }
      reader.readAsDataURL(file)
    }
  }
}
