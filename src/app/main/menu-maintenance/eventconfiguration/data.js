export const tableDetail = {
  headers: [
    // {
    //   name: "S.No",
    //   referField: 'visionId',
    //   filter: false,
    //   isFilterField: true
    // },

    // {
    //   name: "Event Type",
    //   referField: 'eventSubType',
    //   filter: false,
    //   isFilterField: true,
    // },
    {
      name: "Event Type",
      referField: "eventSubTypeDesc",
      filter: false,
      isFilterField: false,
    },
    // {
    //   name: "Event Sub Type",
    //   referField: 'eventSubType',
    //   filter: false,
    //   isFilterField: true,
    // },
    {
      name: "Event Sub Type",
      referField: "eventSubTypeDesc",
      filter: false,
      isFilterField: false,
    },
    {
      name: "Event ID",
      referField: "eventId",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Event Descripition",
      referField: "eventDescription",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Condition",
      referField: "eventCondition",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Status",
      referField: "statusDesc",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
    {
      name: "Record Indicator",
      referField: "recordIndicatorDesc",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Date Last Modified",
      referField: "dateLastModified",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
    },
    {
      name: "view",
      icon: "visibility",
      actionType: "view",
    },
    {
      name: "Schedule",
      icon: "schedule",
      actionType: "schedule",
    },
    {
      name: "delete",
      icon: "delete",
      actionType: "delete",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 3, 4],
      },
    },
    // {
    //   name: 'Review',
    //   icon: 'fa fa-ban',
    //   actionType: 'review',
    //   validationsReview: true,
    //   reviewConditon: {
    //     key: 'recordIndicator',
    //     value: [1]
    //   }
    // }
  ],
};
