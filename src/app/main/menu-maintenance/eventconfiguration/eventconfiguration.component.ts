import { DatePipe } from "@angular/common";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup,
  Validators
} from "@angular/forms";
import { NgbDateStruct, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { TextEditorComponent } from "src/app/shared/text-editor/text-editor.component";
import { UseridSelectionsComponent } from "src/app/shared/userid-selections/userid-selections.component";
import { isNullOrUndefined } from "util";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";
import { tableDetail } from "./data.js";

@Component({
  selector: "app-eventconfiguration",
  templateUrl: "./eventconfiguration.component.html",
  styleUrls: ["./eventconfiguration.component.css"],
})
export class EventconfigurationComponent implements OnInit {
  @ViewChild("updateTemplate", { static: false })
  profileSetupTemplate: ElementRef;
  datePipe: DatePipe = new DatePipe("en-US");
  leftContainerHeight: string;
  tableDetail;
  resetModel: any = { key: "" };
  lineList: any;
  pageView: any;
  editIndex: any;
  ngxTableConfig: any = {
    api: "event_getAllQueryResults",
    dataToDisplay: [],
    filterApi: "event_getAllQueryResults",
    count: 10,
    homeDashboard: [],
  };
  eventTypeList: any;
  eventSubTypeList: any;
  recordIndicatorList: any;
  statusList: any;
  screenLangData;
  eventSetupForm: FormGroup;
  healthSetupForm: FormGroup;
  eventSetup: boolean = false;
  mainPage: boolean = false;
  addEvent: boolean = false;
  emailBodytext: any;
  startDate: NgbDateStruct;
  endDate: NgbDateStruct;
  min_startdate: NgbDateStruct;
  submitted: boolean = false;
  healthSetupformSubmitted: boolean = false;
  eventFormSave: boolean = false;
  eventFormModify: boolean = false;
  mailPicked: boolean = false;
  smsPicked: boolean = false;
  mobilePicked: boolean = false;
  notificationmsg: boolean = false;
  editData: any;
  modifyHealthBtn: boolean = false;
  saveHealthBtn: boolean = false;

  constructor(
    private translate: TranslateService,
    private globalService: GlobalService,
    private _apiService: ApiService,
    private modalService: NgbModal
  ) {
    this.translate.setDefaultLang("en");
    this.translate.use("en");
    this.translate.currentLoader
      .getTranslation(this.translate.currentLang)
      .subscribe((data) => {
        this.screenLangData = data["profileSetup"];
      });
  }
  initilizeEventSetupForm() {
    this.eventSetupForm = new FormGroup({
      eventType: new FormControl("", Validators.required),
      eventSubType: new FormControl("", Validators.required),
      eventId: new FormControl("", Validators.required),
      eventInterval: new FormControl("", [
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
      ]),
      eventDescription: new FormControl(""),
      eventConditionType: new FormControl(""),
      eventCondition: new FormControl(""),
      eventStatus: new FormControl(""),
    });
  }
  initilizeHealthSetupForm() {
    this.healthSetupForm = new FormGroup({
      eventType: new FormControl(""),
      eventId: new FormControl(""),
      eventSubType: new FormControl(""),
      eventStatus: new FormControl("E", Validators.required),
      eventChannel: new FormControl("", Validators.required),
      startDate: new FormControl("", Validators.required),
      endDate: new FormControl(""),
      eventSubject: new FormControl("", Validators.required),
      eventCcEmailId: new FormControl("", Validators.required),
      mailBody: new FormControl("", Validators.required),
      eventMobileNo: new FormControl("", Validators.required),
      notificationMessage: new FormControl("", Validators.required),
      desc: new FormControl("", Validators.required),
      eventToEmailId: new FormControl(""),
      eventSequence: new FormControl(""),
    });
  }

  ngOnInit(): void {
    this.min_startdate = {
      year: new Date().getFullYear(),
      month: new Date().getMonth(),
      day: new Date().getDate(),
    };
    this.tableDetail = tableDetail;
    this.formPageLoadValue();
    this.initilizeEventSetupForm();

    let data = {
      actionType: "Query",
      currentPage: 1,
      maxRecords: 5000,
    };
    this._apiService
      .post(environment.event_getAllQueryResults, data)
      .subscribe((resp) => {
        this.lineList = resp["response"];
      });
  }
  modalClose() {
    this.modalService.dismissAll();
  }
  tableAction(e) {
    this.pageView = e.type;
    if (e.type == "add") {
      this.openModal();
      this.initilizeEventSetupForm();
      this.submitted = false;
      this.eventFormModify = false;
      this.eventFormSave = true;
      this.eventSetupForm.patchValue({
        eventStatus: "0",
      });
    }
    if (
      e.type == "edit" ||
      e.type == "view" ||
      e.type == "preview" ||
      e.type == "approve" ||
      e.type == "reject"
    ) {
      this.editIndex = e.index;
      this.editForm(e);
      this.formPageLoadValue();
    }
    if (e.type == "approve" || e.type == "reject" || e.type == "delete") {
      this.openGenericPopup(e);
    }
    if (e.type == "schedule") {
      this.eventSetup = true;
      this.mainPage = false;
      this.initilizeHealthSetupForm();
      this.formPageLoadValueTwo(e);
      this.healthSetupformSubmitted = false;
    }
    if (e.type == "refresh" || e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.event_getAllQueryResults, data)
        .subscribe((resp) => {
          this.lineList = resp["response"];
          this.ngxTableConfig["filterData"] = e.filter;
          this.formPageLoadValue();
        });
    }
  }
  openGenericPopup(e) {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    let prompt = "";
    if (e.type == "approve") {
      prompt = "Are you sure, you want to approve the record?";
    }
    if (e.type == "reject") {
      prompt = "Are you sure, you want to reject the record?";
    }
    if (e.type == "delete") {
      prompt = "Are you sure, you want to delete the record?";
    }
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = prompt;
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.approveOrReject(e);
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }

  approveOrReject(e) {
    let data = {
      eventType: e.data.eventType,
      eventSubType: e.data.eventSubType,
      eventId: e.data.eventId,
    };
    let apiUrl =
      e.type == "approve"
        ? environment.policy_Setup_ApproveEdwPolConfig
        : e.type == "reject"
          ? environment.policy_Setup_RejectEdwPolConfig
          : e.type == "delete"
            ? environment.event_deleteEdwEvtConfig
            : "";
    this._apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp["status"]) {
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
      this.resetModel = Object.assign({}, { key: "reload" });
      this.backToHmpage("dbhealth");
    });
  }
  formPageLoadValue() {
    this._apiService.get(environment.event_pageLoadValues).subscribe((resp) => {
      this.eventTypeList = resp["response"][0];
      this.eventSubTypeList = resp["response"][1];
      this.statusList = resp["response"][2];
      this.recordIndicatorList = resp["response"][3];
      this.ngxTableConfig.cronStatus = resp["response"][4];
      this.mainPage = true;
    });
  }
  formPageLoadValueTwo(e) {
    let data = {
      eventType: e.data.eventType,
      eventSubType: e.data.eventSubType,
      eventId: e.data.eventId,
    };
    this._apiService
      .post(environment.eventApi_getQueryDetails, data)
      .subscribe((resp) => {
        let values = resp["response"];
        let values_others = resp["otherInfo"];

        if (!isNullOrUndefined(values_others) && isNullOrUndefined(values)) {
          this.modifyHealthBtn = false;
          this.saveHealthBtn = true;
          this.healthSetupForm.patchValue({
            eventStatus: "E",
            eventChannel: "EMAIL",
          });
          this.onChangeChannels("EMAIL");

          this.healthSetupForm.patchValue({
            eventType: values_others.eventType,
            eventSubType: values_others.eventSubType,
            eventId: values_others.eventId,
            runStatus: values_others.runStatus,
            eventSequence: values_others.eventSequence.toString(),

            eventMobileNo: values_others.eventMobileNo,
            notificationMessage: values_others.notificationMessage,
            eventCcEmailId: values_others.eventCcEmailId,
            eventToEmailId: values_others.eventToEmailId,

            eventSubject: values_others.eventSubject,
            mailBody: values_others.mailBody,
          });
        } else if (!isNullOrUndefined(values)) {
          this.modifyHealthBtn = true;
          this.saveHealthBtn = false;
          this.onChangeChannels(values[0].eventChannel);
          let healthStatDate;
          let healthEndDate;
          if (!isNullOrUndefined(values[0].startDate)) {
            healthStatDate = new Date(values[0].startDate);
          }

          if (
            !isNullOrUndefined(values[0].endDate) &&
            values[0].endDate != ""
          ) {
            healthEndDate = new Date(values[0].endDate);
          }
          this.healthSetupForm.patchValue({
            eventType: values[0].eventType,
            eventSubType: values[0].eventSubType,
            eventId: values[0].eventId,
            startDate: healthStatDate,
            endDate: healthEndDate,
            runStatus: values[0].runStatus,
            eventStatus: values[0].eventStatus,
            eventChannel: values[0].eventChannel,
            eventSequence: values[0].eventSequence.toString(),
            eventMobileNo: values[0].eventMobileNo,
            notificationMessage: values[0].notificationMessage,
            eventCcEmailId: values[0].eventCcEmailId,
            eventToEmailId: values[0].eventToEmailId,

            eventSubject: values[0].eventSubject,
            mailBody: values[0].mailBody,
          });
        } else {
          this.modifyHealthBtn = false;
          this.saveHealthBtn = true;
          this.healthSetupForm.patchValue({
            eventStatus: "E",
            eventChannel: "EMAIL",
          });
          this.onChangeChannels("EMAIL");
        }
      });
  }

  editForm(e) {
    let data = {
      eventType: e.data.eventType,
      eventSubType: e.data.eventSubType,
      eventId: e.data.eventId,
    };
    this._apiService
      .post(environment.event_getQueryDetails, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.editData = resp["otherInfo"];
          if (!isNullOrUndefined(this.editData.eventId)) {
            setTimeout(() => {
              this.eventFormSave = false;
              this.eventFormModify = true;
              this.addEvent = true;
            }, 200);

            this.eventSetupForm.get("eventType").disable();
            this.eventSetupForm.get("eventId").disable();
            this.eventSetupForm.get("eventSubType").disable();
            this.eventSetupForm.patchValue({
              eventType: this.editData.eventType,
              eventSubType: this.editData.eventSubType,
              eventId: this.editData.eventId,
              eventInterval: this.editData.eventInterval,
              eventDescription: this.editData.eventDescription,
              eventConditionType: this.editData.eventConditionType,
              eventCondition: this.editData.eventCondition,
              eventStatus: this.editData.eventStatus.toString(),
            });
          }
        }
      });
  }

  saveEventSetup(type) {

    let apiUrl =
      type == "modify"
        ? environment.event_modifyEdwEvtConfig
        : environment.event_addEdwEvtConfig;
    if (this.eventSetupForm.invalid || !this.eventSetupFormValid()) {
      this.submitted = true;
    } else {
      let rawValue = this.eventSetupForm.getRawValue();
      const pass_list = {
        eventType: rawValue.eventType,
        eventSubType: rawValue.eventSubType,
        eventId: rawValue.eventId,
        eventInterval: this.eventSetupForm.value.eventInterval,
        eventDescription: this.eventSetupForm.value.eventDescription,
        eventConditionType: this.eventSetupForm.value.eventConditionType,
        eventStatus: this.eventSetupForm.value.eventStatus,
        eventCondition: this.eventSetupForm.value.eventCondition,
      };
      this._apiService.post(apiUrl, pass_list).subscribe((resp) => {
        if (resp["status"] == 1) {
          if (type == "save" || "modify") {
            this.globalService.showToastr.success(resp["message"]);
            this.modalService.dismissAll();
            let data = {
              type: "refresh",
            };
            this.tableAction(data);
            this.backToHmpage("event_setup");
          }
          else {
            let e = {
              type: "edit",
              data: this.editData,
              index: this.editIndex
            }
            this.tableAction(e)
          }

        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    }
  }

  healthsetupFormsubmission(type) {
    let apiUrl =
      type == "modify"
        ? environment.eventApi_modifyEdwEvtConfig
        : environment.eventApi_addEdwEvtConfig;

    if (this.healthSetupForm.invalid || !this.isHealthSetupFormValid()) {
      this.healthSetupformSubmitted = true;
    } else {
      let rawValue = this.healthSetupForm.getRawValue();
      let healthStatDate;
      let healthEndDate;
      if (!isNullOrUndefined(rawValue.startDate)) {
        healthStatDate = this.datePipe.transform(
          rawValue.startDate,
          "dd-MM-yyyy"
        );
      }
      if (!isNullOrUndefined(rawValue.endDate)) {
        healthEndDate = this.datePipe.transform(rawValue.endDate, "dd-MM-yyyy");
      }
      const pass_list = {
        eventType: rawValue.eventType,
        eventSubType: rawValue.eventSubType,
        eventId: rawValue.eventId,
        runStatus: "F",
        eventStatus: rawValue.eventStatus,
        eventChannel: rawValue.eventChannel,
        eventSequence: rawValue.eventSequence,
        startDate: healthStatDate,
        endDate: healthEndDate,
        eventMobileNo: rawValue.eventMobileNo,
        notificationMessage: rawValue.notificationMessage,
        eventCcEmailId: rawValue.eventCcEmailId,
        eventSubject: rawValue.eventSubject,
        mailBody: rawValue.mailBody,
        eventToEmailId: rawValue.eventToEmailId,
      };
      if (
        !isNullOrUndefined(rawValue.endDate) &&
        !isNullOrUndefined(rawValue.startDate)
      ) {
        if (rawValue.endDate < rawValue.startDate) {
          this.globalService.showToastr.warning(
            "End Date should be lesser than Start Date"
          );
        } else {
          this.healthFormApiHitting(apiUrl, pass_list);
        }
      } else {
        this.healthFormApiHitting(apiUrl, pass_list);
      }
    }
  }

  healthFormApiHitting(apiUrl, pass_list) {
    this._apiService.post(apiUrl, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.globalService.showToastr.success(resp["message"]);
        this.modalService.dismissAll();
        let data = {
          type: "refresh",
        };
        this.tableAction(data);
        this.backToHmpage("dbhealth");
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  eventSetupFormValid() {
    if (Object.keys(this.eventSetupForm).length) {
      return true;
    } else {
      return false;
    }
  }
  isHealthSetupFormValid() {
    if (Object.keys(this.healthSetupForm).length) {
      return true;
    } else {
      return false;
    }
  }

  backToHmpage(value) {
    if (value == "event_setup") {
      this.mainPage = true;
      this.addEvent = false;
    } else if (value == "dbhealth") {
      this.mainPage = true;
      this.eventSetup = false;
    }
  }

  onChangeChannels(event) {
    if (event == "EMAIL") {
      this.mailPicked = true;
      this.smsPicked = false;
      this.mobilePicked = false;
      this.healthSetupForm.get("eventMobileNo").clearValidators();
      this.healthSetupForm.get("eventMobileNo").updateValueAndValidity();
      this.healthSetupForm.get("notificationMessage").clearValidators();
      this.healthSetupForm.get("notificationMessage").updateValueAndValidity();
      this.healthSetupForm.get("desc").clearValidators();
      this.healthSetupForm.get("desc").updateValueAndValidity();
    }
    if (event == "SMS") {
      this.mailPicked = false;
      this.smsPicked = false;
      this.mobilePicked = true;
      this.notificationmsg = false;
      this.healthSetupForm.get("eventSubject").clearValidators();
      this.healthSetupForm.get("eventSubject").updateValueAndValidity();
      this.healthSetupForm.get("eventCcEmailId").clearValidators();
      this.healthSetupForm.get("eventCcEmailId").updateValueAndValidity();
      this.healthSetupForm.get("mailBody").clearValidators();
      this.healthSetupForm.get("mailBody").updateValueAndValidity();
      this.healthSetupForm.get("eventMobileNo").clearValidators();
      this.healthSetupForm.get("eventMobileNo").updateValueAndValidity();
    }
    if (event == "Notification") {
      this.mailPicked = false;
      this.smsPicked = true;
      this.mobilePicked = false;
      this.notificationmsg = true;
      this.healthSetupForm.get("eventSubject").clearValidators();
      this.healthSetupForm.get("eventSubject").updateValueAndValidity();
      this.healthSetupForm.get("eventCcEmailId").clearValidators();
      this.healthSetupForm.get("eventCcEmailId").updateValueAndValidity();
      this.healthSetupForm.get("mailBody").clearValidators();
      this.healthSetupForm.get("mailBody").updateValueAndValidity();
      this.healthSetupForm.get("eventMobileNo").clearValidators();
      this.healthSetupForm.get("eventMobileNo").updateValueAndValidity();
      this.healthSetupForm.get("notificationMessage").clearValidators();
      this.healthSetupForm.get("notificationMessage").updateValueAndValidity();
      this.healthSetupForm.get("desc").clearValidators();
      this.healthSetupForm.get("desc").updateValueAndValidity();
    }
  }

  openModal() {
    this.addEvent = true;
    this.mainPage = false;
  }

  openEditor(event) {
    event.srcElement.blur();
    event.preventDefault();
    this.loadEditor();
  }

  loadEditor() {
    const modelRef = this.modalService.open(TextEditorComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "editor-popup",
    });

    modelRef.componentInstance.bodyText =
      this.healthSetupForm.get("mailBody").value;
    modelRef.componentInstance.title = "Email Body";
    modelRef.componentInstance.emitData.subscribe((resp) => {
      if (resp["type"] == "submit" || resp["type"] == "clear") {
        this.emailBodytext = !isNullOrUndefined(resp["value"])
          ? resp["value"]
          : "";
        this.healthSetupForm.patchValue({
          mailBody: !isNullOrUndefined(resp["value"]) ? resp["value"] : "",
        });
      }
      modelRef.close();
    });
  }
  mailEditor() {
    const modelRef = this.modalService.open(UseridSelectionsComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "editor-popup",
    });

    if (!isNullOrUndefined(this.healthSetupForm.get("eventCcEmailId").value)) {
      const value_one = this.healthSetupForm.get("eventCcEmailId").value;
      var stringArray = new Array();
      var tagsArray_To = new Array();
      stringArray = value_one.replace(/ /g, "").split(",");
      for (var x = 0; x < stringArray.length; x++) {
        var obj = {};
        obj["visionId"] = stringArray[x];
        tagsArray_To.push(obj);
      }
    }

    if (!isNullOrUndefined(this.healthSetupForm.get("eventToEmailId").value)) {
      const value_two = this.healthSetupForm.get("eventToEmailId").value;
      var stringArray_new = new Array();
      var tagsArray_CC = new Array();
      stringArray_new = value_two.replace(/ /g, "").split(",");
      for (var x = 0; x < stringArray_new.length; x++) {
        var obj = {};
        obj["visionId"] = stringArray_new[x];
        tagsArray_CC.push(obj);
      }
    }

    modelRef.componentInstance.inputDetailsUserIds_To = tagsArray_To;
    modelRef.componentInstance.inputDetailsUserIds_Cc = tagsArray_CC;
    modelRef.componentInstance.ccvaluesId =
      this.healthSetupForm.get("eventCcEmailId").value;
    modelRef.componentInstance.TovaluesId =
      this.healthSetupForm.get("eventToEmailId").value;
    modelRef.componentInstance.title = "Email Body";
    modelRef.componentInstance.emitData.subscribe((resp) => {
      if (resp["type"] == "submit" || resp["type"] == "clear") {
        this.healthSetupForm.patchValue({
          eventToEmailId: !isNullOrUndefined(resp["eventToEmailId"])
            ? resp["eventToEmailId"]
            : "",
          eventCcEmailId: !isNullOrUndefined(resp["eventCcEmailId"])
            ? resp["eventCcEmailId"]
            : "",
        });
      }
      modelRef.close();
    });
  }

  backGroundFlag(recordIndicator) {
    if (!recordIndicator) {
      return {
        background: "#448847",
      };
    } else {
      return {
        background: "#ffa828",
      };
    }
  }

  validateQuery() {
    let apiUrl = environment.eventApi_testSqlQuery;
    let rawValue = this.eventSetupForm.getRawValue();
    const pass_list = {
      eventConditionType: rawValue.eventConditionType,
      eventCondition: rawValue.eventCondition,
    };

    if (rawValue.eventConditionType == "") {
      this.globalService.showToastr.warning("Please Select Condition Type");
    }
    else if (rawValue.eventCondition == "") {
      this.globalService.showToastr.warning("Please Enter Condition");
    }
    else if (rawValue.eventCondition != "" && rawValue.eventConditionType != "") {
      this._apiService.post(apiUrl, pass_list).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    }

  }

  disableButton(name) {
    if (name == 'first') {
      return !this.editIndex ? true : false;
    }
    if (name == 'last') {
      return this.lineList.length == 1 ? true : this.editIndex == (this.lineList.length - 1) ? true : false;
    }
  }


  navigateFn(type) {
    //this.eventSetupForm.reset();
    if (type == 'next') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'previous') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'last') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'first') {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
  }


  onSubmit(type) {
    if (type == 'next' || type == 'previous' || type == 'last' || type == 'first') {
      this.navigateFn(type);
    }
  }


  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }



}
