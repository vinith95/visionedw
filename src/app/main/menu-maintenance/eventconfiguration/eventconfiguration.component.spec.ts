import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventconfigurationComponent } from './eventconfiguration.component';

describe('EventconfigurationComponent', () => {
  let component: EventconfigurationComponent;
  let fixture: ComponentFixture<EventconfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventconfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventconfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
