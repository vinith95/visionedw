import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { MenuMaintenanceRoutingModule } from './menu-maintenance-routing.module';
// import { MenuMaintenanceListComponent } from './menu-maintenance-list/menu-maintenance-list.component';
// import { CreateMenuComponent } from './create-menu/create-menu.component';
import { ProfileSetupComponent } from './profile-setup/profile-setup.component';
import { CreateQueryComponent } from './query-module/create-query/create-query.component';
import { ManageQueryComponent } from './query-module/manage-query/manage-query.component';
import { GenericQueryPopupComponent } from './query-module/popup-components/generic-query-popup-component/generic-query-popup-component.component';
import { LoadDataComponent } from './query-module/popup-components/load-data/load-data.component';
import { MagnifierPopupComponent } from './query-module/popup-components/magnifier-popup/magnifier-popup.component';
import { ShowQueryComponent } from './query-module/popup-components/show-query/show-query.component';
import { BustFlagComponent } from './bust-flag/bust-flag.component';
import { ErmoduleComponent } from './../ermodule/ermodule.component';
import { NgxPanZoomModule } from 'ngx-panzoom';
import { MaximizeComponent } from '../ermodule/maximize/maximize.component';
import { TabletreeviewComponent } from '../ermodule/tabletreeview/tabletreeview.component';
import { PolicySetupComponent } from '../sub-modules/policy-setup/policy-setup.component';
import { TreeErmoduleComponent } from '../ermodule/tree-ermodule/tree-ermodule.component';
import { ErDiagramoverallComponent } from '../ermodule/er-diagramoverall/er-diagramoverall.component';
import { UserSetupComponent } from './user-setup/user-setup.component';
import { EventconfigurationComponent } from './eventconfiguration/eventconfiguration.component';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';
import { TablemetadataComponent } from './tablemetadata/tablemetadata.component';
import { BusiessGlossaryComponent } from './busiess-glossary/busiess-glossary.component';
import { CronStatusComponent } from './cron-status/cron-status.component';


import { BusinessGloassarySearchComponent } from './business-gloassary-module/business-gloassary-search/business-gloassary-search.component';
import { BusinessTableTreeViewComponent } from './business-gloassary-module/business-table-tree-view/business-table-tree-view.component';
import { BusinessrepositoryviewComponent } from './business-gloassary-module/businessrepositoryview/businessrepositoryview.component';
import { AccountBucketsComponent } from './AccountBuckets/AccountBuckets.component'
import { MissingObjectsViewsComponent } from '../ermodule/missing-objects-views/missing-objects-views.component';
import { MissingtreeviewComponent } from '../ermodule/missingtreeview/missingtreeview.component';
import { MissingrepositoryviewComponent } from '../ermodule/missingrepositoryview/missingrepositoryview.component';
import { MissingrepositoryviewmoduleComponent } from '../ermodule/missingrepositoryviewmodule/missingrepositoryviewmodule.component';


@NgModule({
  declarations: [ProfileSetupComponent, MaximizeComponent, TabletreeviewComponent, PolicySetupComponent,
    ManageQueryComponent, ShowQueryComponent, GenericQueryPopupComponent, MagnifierPopupComponent,
    LoadDataComponent, CreateQueryComponent, BustFlagComponent, ErmoduleComponent, TreeErmoduleComponent,
    ErDiagramoverallComponent,
    UserSetupComponent, EventconfigurationComponent, AuditTrailComponent, CronStatusComponent,
    BusiessGlossaryComponent, TablemetadataComponent, BusinessGloassarySearchComponent,
    BusinessTableTreeViewComponent, BusinessrepositoryviewComponent,
    MissingrepositoryviewComponent, MissingrepositoryviewmoduleComponent, AccountBucketsComponent, MissingObjectsViewsComponent, MissingtreeviewComponent],
  imports: [
    MenuMaintenanceRoutingModule,
    SharedModule,
    NgxPanZoomModule
  ],
  entryComponents: [
    // CreateMenuComponent,
    ShowQueryComponent,
    ErmoduleComponent,
    PolicySetupComponent,
    GenericQueryPopupComponent,
    MagnifierPopupComponent,
    LoadDataComponent]
})
export class MenuMaintenanceModule { }
