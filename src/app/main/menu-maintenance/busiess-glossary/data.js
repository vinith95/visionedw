export const tableDetail = {
  headers: [
    {
      name: "Application Access",
      referField: "productName",
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },

    {
      name: "Connection Id",
      referField: "connectionId",
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },

    {
      name: "Schema",
      referField: "schema",
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },

    {
      name: "Glossary Id",
      referField: "glossaryId",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },

    {
      name: "Glossary Title",
      referField: "glossaryTitle",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },

    {
      name: "Status",
      referField: "statusDesc",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },

    {
      name: "Record Indicator",
      referField: "recordIndicatorDesc",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },

    // {
    // 	name: 'Maker',
    // 	referField: 'maker',
    // 	filter: false,
    // 	isFilterField: true,
    // 	needIndicator: true,
    // },

    // {
    // 	name: 'Verifier',
    // 	referField: 'verifier',
    // 	filter: false,
    // 	isFilterField: true,
    // 	needIndicator: true,
    // },
    {
      name: "Date Last Modified",
      referField: "dateLastModified",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
    // {
    // 	name: 'actionType',
    // 	referField: 'actionType',
    // 	filter: false,
    // 	isFilterField: false,
    // 	needIndicator: false,
    // },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
    },
    {
      name: "details",
      icon: "description",
      actionType: "details",
    },
    {
      name: "view",
      icon: "visibility",
      actionType: "view",
    },
    // {
    //   name: 'delete',
    //   icon: 'delete',
    //   actionType: 'delete',
    //   validations: true,
    //   conditon: {
    //     key: 'recordIndicator',
    //     value: [1, 2, 3, 4]
    //   }
    // }
  ],
};
