import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusiessGlossaryComponent } from './busiess-glossary.component';

describe('BusiessGlossaryComponent', () => {
  let component: BusiessGlossaryComponent;
  let fixture: ComponentFixture<BusiessGlossaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusiessGlossaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusiessGlossaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
