import {
  Component, ElementRef, EventEmitter, OnInit, Output, ViewChild
} from "@angular/core";
import {
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { ApiService, GlobalService } from "src/app/service";
import { environment } from "../../../../environments/environment";
import { MagnifiernewComponent } from "./../../../shared/magnifiernew/magnifiernew/magnifiernew.component";
import { tableDetail } from "./data.js";

@Component({
  selector: "app-busiess-glossary",
  templateUrl: "./busiess-glossary.component.html",
  styleUrls: ["./busiess-glossary.component.css"],
})
export class BusiessGlossaryComponent implements OnInit {
  @ViewChild("updateTemplate", { static: false })
  profileSetupTemplate: ElementRef;
  @ViewChild("eventtemplate", { static: false }) eventSetupTemplate: ElementRef;
  @Output() magnifiervalues = new EventEmitter<string>();

  constructor(
    private translate: TranslateService,
    private globalService: GlobalService,
    private _apiService: ApiService,
    private modalService: NgbModal
  ) {
    this.translate.setDefaultLang("en");
    this.translate.use("en");
    this.translate.currentLoader
      .getTranslation(this.translate.currentLang)
      .subscribe((data) => {
        this.screenLangData = data["profileSetup"];
      });
  }
  tableDetail;
  screenLangData;
  businessGloaaryList: any;
  mainPage: boolean = false;
  addEvent: boolean = false;
  recordIndicatorList: any;
  statusList: any;
  pageView: any;
  lineList = [];
  editIndex: any;
  submitted: boolean = false;
  leftContainerHeight;
  bussinesGlossaryForm;
  maginerfiervalues: any = [];
  glossaryAdd: boolean = false;
  glossaryEdit: boolean = false;
  maginerfier_column_name: any = [];
  maginerfier_column_name_obj_passing: any = [];
  eventValues: any = [];

  resetModel: any = { key: "" };
  ngxTableConfig: any = {
    api: "businessGlossary_getAllQueryResults",
    dataToDisplay: [],
    filterApi: "businessGlossary_getAllQueryResults",
    count: 10,
    homeDashboard: [],
  };

  ngOnInit(): void {
    this.tableDetail = tableDetail;
    let data = {
      actionType: "Query",
      currentPage: 1,
      maxRecords: 5000,
    };
    this._apiService
      .post(environment.businessGlossary_getAllQueryResults, data)
      .subscribe((resp) => {
        this.businessGloaaryList = resp["response"];
        this.mainPage = true;
      });
  }

  initLeaveManagementForm() {
    this.bussinesGlossaryForm = new FormGroup({
      glossaryId: new FormControl("", [
        Validators.required,
        Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/),
      ]),
      glossaryTitle: new FormControl("", [
        Validators.required
      ]),
      objectIds: new FormControl(""),
      Status: new FormControl("", [
        Validators.required
      ]),
      actionType: new FormControl(""),
    });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || "").trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }

  formPageLoadValue() {
    this._apiService
      .get(environment.businessGlossary_pageLoadValues)
      .subscribe((resp) => {
        this.statusList = resp["response"][0];
        this.recordIndicatorList = resp["response"][1];
      });
  }
  openModal() {
    this.modalService.open(this.profileSetupTemplate, {
      size: "lg",
      windowClass: "width-popup",
      backdrop: "static",
    });
  }

  isMasterSel: boolean = false;
  checkedCategoryList: any;
  editBusinessGloassaryForm: any = [];
  businessGloassaryTableData: any = [];
  showGrid: boolean = false;
  tableAction(e) {
    this.businessGloassaryTableData = [];
    this.lineList = e.lineList;
    this.pageView = e.type;
    if (e.type === "edit" || e.type === "view") {
      this.editIndex = e.index;

      this.pageView = e.type;
      this._apiService
        .post(environment.editBusGlossary_getQueryDetails, e.data)
        .subscribe((resp) => {
          this.initLeaveManagementForm();
          this.eventValues = resp["response"];
          this.editBusinessGloassaryForm = resp["response"];
          this.businessGloassaryTableData =
            this.editBusinessGloassaryForm.children;
          if (this.businessGloassaryTableData) {
            this.isMasterSel = true;
          }
          this.businessGloassaryTableData.forEach((element) => {
            if (element) {
              element["isSelected"] = true;
            }
          });

          this.showGrid = true;
          this.mainPage = false;
          this.addEvent = true;
          this.glossaryAdd = false;
          this.glossaryEdit = true;
          this.initLeaveManagementForm();
          this.submitted = false;
          this.formPageLoadValue();
          this.bussinesGlossaryForm.get("glossaryId").disable();
          this.bussinesGlossaryForm.patchValue({
            glossaryId: this.editBusinessGloassaryForm.glossaryId,
            glossaryTitle: this.editBusinessGloassaryForm.glossaryTitle,
            Status: this.editBusinessGloassaryForm.status.toString(),
            actionType: this.editBusinessGloassaryForm.objectIds,
          });
        });

    }
    if (e.type == "add") {
      this.isMasterSel = false;
      this.mainPage = false;
      this.addEvent = true;
      this.glossaryAdd = true;
      this.glossaryEdit = false;
      this.initLeaveManagementForm();
      this.submitted = false;
      this.showGrid = true;
      this.formPageLoadValue();
    }
    if (
      e.type == "delete" ||
      e.type == "preview" ||
      e.type == "details" ||
      e.type == "approve" ||
      e.type == "reject"
    ) {
      this.mainPage = true;
      this.addEvent = false;
      this.glossaryAdd = false;
      this.glossaryEdit = false;
      this.editForm(e);
    }
    if (e.type == "refresh" || e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.businessGlossary_getAllQueryResults, data)
        .subscribe((resp) => {
          this.businessGloaaryList = resp["response"];
          this.ngxTableConfig["filterData"] = e.filter;
          this.formPageLoadValue();
          this.resetModel = Object.assign({}, { key: "reload" });
        });
    }
  }

  editForm(e) {
    this.eventValues = [];
    this._apiService
      .post(environment.businessGlossary_getTableObjects, e.data)
      .subscribe((resp) => {
        this.eventValues = resp["response"];
        this.openModalevent();
      });
  }

  passingarray = [];
  opneMagniferData(title) {
    this.passingarray = [];
    this.maginerfiervalues = [];
    this.maginerfier_column_name = [];
    let valuesbinding = "";
    let query;

    query = "ERTABLES";
    const modelRef = this.modalService.open(MagnifiernewComponent, {
      size: "lg",
      backdrop: "static",
    });
    modelRef.componentInstance.query = query;
    modelRef.componentInstance.title = title;
    let rawValue = this.bussinesGlossaryForm.getRawValue();
    if (rawValue.actionType) {
      let valuesGuidelines = rawValue.actionType.split(",").map((s) => s.trim());
      this.passingarray = Object.values(valuesGuidelines).map((value) => {
        return { columnNine: value, checked: true };
      });
    }
    this.maginerfiervalues.push(this.passingarray);
    modelRef.componentInstance.inputDetails = this.passingarray;
    modelRef.componentInstance.filterData.subscribe((data) => {
      if (data != "close") {
        this.maginerfiervalues = data;
        this.maginerfiervalues.forEach((element) => {
          if (element.checked == true) {
            this.maginerfier_column_name += element.columnNine + ",";
            (element["objectName"] = element.columnNine),
              (element["isSelected"] = true);
          }
        });
        this.bussinesGlossaryForm.patchValue({
          ["actionType"]: '',
        });
        valuesbinding = this.maginerfier_column_name.slice(0, -1);
        this.bussinesGlossaryForm.patchValue({
          ["actionType"]: valuesbinding,
        });
        this.businessGloassaryTableData = [];
        this.businessGloassaryTableData = this.maginerfiervalues;
        this.isMasterSel = true;
      }
      modelRef.close();
    });
  }

  EventSetup(type) {

    this.maginerfier_column_name_obj_passing = [];

    let apiUrl =
      type == "modify"
        ? environment.businessGlossary_modifyEdwEvtConfig
        : environment.businessGlossary_addEdwEvtConfig;


    if (this.pageView == "add") {
      this.businessGloassaryTableData.forEach((element) => {
        if (element.isSelected == true) {
          this.maginerfier_column_name_obj_passing += element.columnNine + ",";
        }
      });
    } else {
      this.businessGloassaryTableData.forEach((element) => {
        if (element.isSelected == true) {
          element.moduleName = element.columnNine || element.objectName;
          this.maginerfier_column_name_obj_passing += element.moduleName + ",";
        }
      });
    }



    if (!this.glossarySetupForm()) {
      this.submitted = true;
    } else {
      let rawValue = this.bussinesGlossaryForm.getRawValue();
      let objectValues= this.maginerfier_column_name_obj_passing.slice(0, -1);
      const pass_list = {
        glossaryId: rawValue.glossaryId,
        glossaryTitle: rawValue.glossaryTitle,
        Status: rawValue.Status,
        objectIds: objectValues.toUpperCase(),
      };
      if (pass_list.objectIds.length) {
        this._apiService.post(apiUrl, pass_list).subscribe((resp) => {
          if (resp["status"] == 1) {
            this.globalService.showToastr.success(resp["message"]);
            this.modalService.dismissAll();
            let data = {
              type: "refresh",
            };
            this.tableAction(data);
            this.mainPage = true;
            this.addEvent = false;
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
      } else {
        this.globalService.showToastr.warning("Please Select Objects/Tables");

      }


    }
  }

  glossarySetupForm() {
    if (Object.keys(this.bussinesGlossaryForm).length) {
      return true;
    } else {
      return false;
    }
  }
  openModalevent() {
    this.modalService.open(this.eventSetupTemplate, {
      size: "lg",
      backdrop: "static",
    });
  }

  checkUncheckAll() {
    for (var i = 0; i < this.businessGloassaryTableData.length; i++) {
      this.businessGloassaryTableData[i].isSelected = this.isMasterSel;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.isMasterSel = this.businessGloassaryTableData.every(function (
      item: any
    ) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }
  getCheckedItemList() {
    this.checkedCategoryList = [];
    for (var i = 0; i < this.businessGloassaryTableData.length; i++) {
      if (this.businessGloassaryTableData[i].isSelected)
        this.checkedCategoryList.push(this.businessGloassaryTableData[i]);
    }
    this.checkedCategoryList = JSON.stringify(this.checkedCategoryList);
  }

  modalClose() {
    this.modalService.dismissAll();
  }

  backtoMainpage() {
    this.mainPage = true;
    this.addEvent = false;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    }, 500);
  }
  backGroundFlag(recordIndicator) {
    if (!recordIndicator) {
      return {
        background: "#448847",
      };
    } else {
      return {
        background: "#ffa828",
      };
    }
  }
  onSubmit(type) {
    if (type == 'next' || type == 'previous' || type == 'last' || type == 'first') {
      this.navigateFn(type);
    }
  }
  disableButton(name) {
    if (name == 'first') {
      return !this.editIndex ? true : false;
    }
    if (name == 'last') {
      return this.lineList.length == 1 ? true : this.editIndex == (this.lineList.length - 1) ? true : false;
    }
  }

  navigateFn(type) {
    if (type == 'next') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'previous') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'last') {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
    if (type == 'first') {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0,
        lineList: this.lineList
      }
      this.tableAction(data)
    }
  }


}
