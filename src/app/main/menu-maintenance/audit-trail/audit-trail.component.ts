import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { tableDetail, tableDetailPolicy } from "./data.js";
import { ApiService } from "../../../service";
import { environment } from "../../../../environments/environment";
import { FormBuilder } from "@angular/forms";
import { isNullOrUndefined } from "util";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-audit-trail",
  templateUrl: "./audit-trail.component.html",
  styleUrls: ["./audit-trail.component.css"],
})
export class AuditTrailComponent implements OnInit {
  @ViewChild("policytemplate", { static: false }) policySetupTemplate: ElementRef;
  @ViewChild("eventtemplate", { static: false }) eventSetupTemplate: ElementRef;
  tableDetail;
  tableDetailPolicy;
  lineList: any;
  policySetupFormSetupForm;
  eventSetupFormSetupForm;
  eventLoad: any;
  policyLoad: any;
  leftContainerHeight: string;
  policytab: boolean = false;
  eventtab: boolean = false;
  policyValues: any = [];
  eventValues: any = [];
  notificationValues: any = [];
  notificationTable: boolean = false;
  eventTable: boolean = false;

  resetModel: any = { key: "" };
  ngxTableConfig: any = {
    api: "",
    dataToDisplay: [],
    filterApi: "edw_getEvent",
    count: 10,
    homeDashboard: [],
  };
  ngxTableConfig_policy: any = {
    api: "",
    dataToDisplay: [],
    filterApi: "edw_getPolicy",
    count: 10,
    homeDashboard: [],
  };

  constructor(
    private _apiService: ApiService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }




  ngOnInit(): void {
    this.tableDetail = tableDetail;
    this.tableDetailPolicy = tableDetailPolicy;
    this._apiService.get(environment.edwAudit_PageLoad).subscribe((resp) => {
      this.eventLoad = resp["response"][1];
      this.policyLoad = resp["response"][0];
      this.policytab = true;
    });
  }
  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }
  accordionClick(value) {
    if (value == "policy") {
      this.policytab = true;
      this.eventtab = false;
    }
    if (value == "event") {
      this.eventtab = true;
      this.policytab = false;
    }
  }

  initPolicyManagementForm() {
    this.policySetupFormSetupForm = this.fb.group({
      policyId: [""],
      policyType: [""],
    });
  }
  initeventManagementForm() {
    this.eventSetupFormSetupForm = this.fb.group({
      eventId: [""],
      eventType: [""],
      eventSubType: [""],
      notificationMessage: [""],
    });
  }

  tableActionPolicy(e) {
    if (e.type == "view") {
      this.initPolicyManagementForm();
      this.formPageLoadValuesPolicy(e);
    }
    if (e.type == "refresh" || e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.edw_getPolicy, data)
        .subscribe((resp) => {
          this.policyLoad = resp["response"];
          this.ngxTableConfig["filterData"] = e.filter;
        });
    }
  }

  formPageLoadValuesPolicy(e) {
    let data = {
      policyId: e.data.policyId,
      policyType: e.data.policyType,
      policyTypeDesc: e.data.policyTypeDesc,
      policyDescription: e.data.policyDescription,
      processStatus: e.data.processStatus,
      internalStatus: e.data.internalStatus,
    };
    this._apiService
      .post(environment.edw_getPolicyDeatils, data)
      .subscribe((resp) => {
        this.policyValues = resp["response"];

        if (!isNullOrUndefined(e.data)) {
          this.policySetupFormSetupForm.patchValue({
            policyId: e.data.policyId + " - " + e.data.policyDescription,
            policyType: e.data.policyType + " - " + e.data.policyTypeDesc,
          });
        }
        this.policySetupFormSetupForm.get("policyId").disable();
        this.policySetupFormSetupForm.get("policyType").disable();
        this.openModal();
      });
  }

  tableActionEvent(e) {
    if (e.type == "view") {
      this.initeventManagementForm();
      this.formPageLoadValuesEvent(e);
    }
    if (e.type == "refresh" || e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.edw_getEvent, data)
        .subscribe((resp) => {
          this.eventLoad = resp["response"];
          this.ngxTableConfig["filterData"] = e.filter;
        });
    }
  }


  formPageLoadValuesEvent(e) {
    let noificationMessageObj;
    this.notificationValues =[];
    this.eventValues =[];
    let data = {
      eventId: e.data.eventId,
      eventType: e.data.eventType,
      eventSubType: e.data.eventSubType,
      eventTypeDesc: e.data.eventTypeDesc,
      eventSubTypeDesc: e.data.eventSubTypeDesc,
      eventChannel: e.data.eventChannel,
    };
    this._apiService
      .post(environment.edw_getEventDeatils, data)
      .subscribe((resp) => {
        if (e.data.eventChannel.toUpperCase() == 'NOTIFICATION') {
          this.eventTable = false;
          this.notificationTable = true;
          this.notificationValues = resp["response"];
          noificationMessageObj =this.notificationValues[0].notificationMessage
        }
        else {
          this.eventTable = true;
          this.notificationTable = false;
          this.eventValues = resp["response"];
        }
        if (!isNullOrUndefined(e.data)) {
          this.eventSetupFormSetupForm.patchValue({
            eventType: e.data.eventType + " - " + e.data.eventTypeDesc,
            eventSubType: e.data.eventSubType + " - " + e.data.eventSubTypeDesc,
            eventId: e.data.eventId + " - " + e.data.eventDescription,
            notificationMessage:noificationMessageObj
          });
        }
        this.eventSetupFormSetupForm.get("eventType").disable();
        this.eventSetupFormSetupForm.get("eventSubType").disable();
        this.eventSetupFormSetupForm.get("eventId").disable();
        this.eventSetupFormSetupForm.get("notificationMessage").disable();
        this.openModalevent();
      });
  }

  openModal() {
    this.modalService.open(this.policySetupTemplate, {
      size: "lg",
      backdrop: "static",
    });
  }
  openModalevent() {
    this.modalService.open(this.eventSetupTemplate, {
      size: "lg",
      backdrop: "static",
    });
  }

  modalClose() {
    this.modalService.dismissAll();
  }
}
