export const tableDetail = {
  headers: [
    // {
    //   name: "S.No",
    //   referField: 'visionId',
    //   filter: false,
    //   isFilterField: true
    // },

    {
      name: "Event Type",
      referField: "eventType",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Event Type Desc",
      referField: "eventTypeDesc",
      filter: false,
      isFilterField: false,
    },
    {
      name: "Event Sub Type",
      referField: "eventSubType",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Event Sub Type Desc",
      referField: "eventSubTypeDesc",
      filter: false,
      isFilterField: false,
    },
    {
      name: "Event ID",
      referField: "eventId",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Event Channel",
      referField: "eventChannel",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Event Desc",
      referField: "eventDescription",
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },
    {
      name: "Audit Count",
      referField: "internalStatus",
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: false,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    // {
    //   name: 'edit',
    //   icon: 'edit',
    //   actionType: 'edit'
    // },
    {
      name: "Details",
      icon: "description",
      actionType: "view",
    },
    {
      name: "delete",
      icon: "delete",
      actionType: "delete",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 3, 4],
      },
    },
  ],
};

export const tableDetailPolicy = {
  headers: [
    {
      name: "Policy Type",
      referField: "policyType",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Policy Type Desc",
      referField: "policyTypeDesc",
      filter: false,
      isFilterField: false,
    },
    {
      name: "Policy ID",
      referField: "policyId",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Policy Desc",
      referField: "policyDescription",
      filter: false,
      isFilterField: false,
    },
    {
      name: "Audit Count",
      referField: "internalStatus",
      filter: false,
      isFilterField: false,
    },
    // {
    //   name: "From Date",
    //   referField: 'dateCreation',
    //   filter: false,
    //   isFilterField: true,
    // },
    // {
    //   name: "To Date",
    //   referField: 'dateLastModified',
    //   filter: false,
    //   isFilterField: true,
    // },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: false,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    //   {
    //   name: 'edit',
    //   icon: 'edit',
    //   actionType: 'edit'
    // },
    {
      name: "Details",
      icon: "description",
      actionType: "view",
    },
    {
      name: "delete",
      icon: "delete",
      actionType: "delete",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 3, 4],
      },
    },
  ],
};
