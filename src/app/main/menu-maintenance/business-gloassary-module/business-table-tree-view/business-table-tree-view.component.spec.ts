import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessTableTreeViewComponent } from './business-table-tree-view.component';

describe('BusinessTableTreeViewComponent', () => {
  let component: BusinessTableTreeViewComponent;
  let fixture: ComponentFixture<BusinessTableTreeViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessTableTreeViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessTableTreeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
