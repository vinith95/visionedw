import {
  ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild
} from "@angular/core";
import { environment } from "../../../../../environments/environment";
import { ApiService, GlobalService } from "../../../../service";

@Component({
  selector: "app-business-table-tree-view",
  templateUrl: "./business-table-tree-view.component.html",
  styleUrls: ["./business-table-tree-view.component.css"],
})
export class BusinessTableTreeViewComponent implements OnInit, OnChanges {
  @ViewChild("viewcontainer") viewcontainer: ElementRef;
  isOpen_columns = true;
  isOpen_relation = true;
  isOpen_unique_key = true;
  isOpen_Uses = true;
  isOpen_Usedby = true;
  isOpen_Uses_by_sub = true;
  isOpen_Uses_by_sub2 = true;
  leftContainerHeight: string;
  animationState = "in";

  tablelist_columns: any = [];
  tablelist_relations: any = [];
  tablelist_Indexes: any = [];
  table_headervalues: any = [];
  useslist: any = [];
  usedbylist: any = [];
  columns_table: boolean = true;
  unique_table: boolean = true;
  relation_table: boolean = true;
  use_table: boolean = true;
  usedby_table: boolean = true;

  @Output() childData = new EventEmitter();
  @Input() nodetreevalues: any;
  @Output() backToSummaryClick = new EventEmitter();
  pageShow: boolean = false;
  localStorageOfTableName: any = [];
  constructor(
    private changeDetector: ChangeDetectorRef,
    private _apiService: ApiService,
    private globalService: GlobalService
  ) { }

  ngOnInit(): void { }

  ngOnChanges() {
    this.tabletreevalues();
  }

  tabletreevalues() {
    let objectname =
      this.nodetreevalues.moduleType || this.nodetreevalues.objectType;
    if (objectname == undefined || "") {
      objectname = "T";
    }
    const pass_list = {
      objectName:
        this.nodetreevalues.moduleName ||
        this.nodetreevalues.tableName ||
        this.nodetreevalues.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: this.nodetreevalues.selecteLanguage,
      objectType: objectname,
    };
    this.localStorageOfTableName.push(pass_list);
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }
        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  toggleShowDiv(value: string) {

    if (value == "Columns") {
      this.isOpen_columns = !this.isOpen_columns;
      this.isOpen_relation = false;
      this.isOpen_unique_key = false;
      this.isOpen_Uses = false;
      this.isOpen_Usedby = false;
      this.classadding();
    }
    if (value == "Relations") {
      this.isOpen_relation = !this.isOpen_relation;
      this.isOpen_unique_key = false;
      this.isOpen_Uses = false;
      this.isOpen_Usedby = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    if (value == "Unique") {
      this.isOpen_unique_key = !this.isOpen_unique_key;
      this.isOpen_relation = false;
      this.isOpen_Uses = false;
      this.isOpen_Usedby = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    if (value == "Uses") {
      this.isOpen_Uses = !this.isOpen_Uses;
      this.isOpen_relation = false;
      this.isOpen_unique_key = false;
      this.isOpen_Usedby = false;
      this.isOpen_columns = false;
      this.classadding();
    }
    if (value == "used_by") {
      this.isOpen_Usedby = !this.isOpen_Usedby;
      this.isOpen_relation = false;
      this.isOpen_unique_key = false;
      this.isOpen_Uses = false;
      this.isOpen_columns = false;
      this.classadding();
    }
  }
  classadding() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }
  usesbyFunt(values) {
    this.calltree(values);
  }
  usesFunt(values) {
    this.calltree(values);
  }

  backToSummary() {
    if (this.localStorageOfTableName.length > 1) {
      let values_passing =
        this.localStorageOfTableName[this.localStorageOfTableName.length - 2];
      this.localStorageOfTableName.splice(-1);
      this.calltreeback(values_passing);
    } else if (this.localStorageOfTableName.length == 1) {
      this.backToSummaryClick.emit(true);
    }
  }

  calltree(values) {
    const pass_list = {
      objectName: values.moduleName || values.tableName || values.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: values.selecteLanguage,
      objectType: "T",
    };

    this.localStorageOfTableName.push(pass_list);
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }

        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  calltreeColumns(values) {
    const pass_list = {
      objectName: values.guidelines,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: values.selecteLanguage,
      objectType: "T",
    };
    this.localStorageOfTableName.push(pass_list);
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }

        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  calltreeback(values) {
    const pass_list = {
      objectName: values.moduleName || values.tableName || values.objectName,
      productName: this.nodetreevalues.productName,
      schema: this.nodetreevalues.schema,
      connectionId: this.nodetreevalues.connectionId,
      selecteLanguage: values.selecteLanguage,
      objectType: "T",
    };
    const api_new = environment.gettreetables;
    this._apiService.post(api_new, pass_list).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.pageShow = true;
        this.table_headervalues = resp["otherInfo"];
        this.tablelist_columns = this.table_headervalues.edwObjColumnsList;
        this.tablelist_relations = this.table_headervalues.edwObjRelationsList;
        this.tablelist_Indexes = this.table_headervalues.edwObjIndexList;
        this.useslist = this.table_headervalues.edwObjUseList;
        this.usedbylist = this.table_headervalues.edwObjUseByList;

        if (this.tablelist_columns == "" || this.tablelist_columns == null) {
          this.columns_table = false;
        } else {
          this.columns_table = true;
          this.isOpen_columns = false;
        }

        if (
          this.tablelist_relations == "" ||
          this.tablelist_relations == null
        ) {
          this.relation_table = false;
        } else {
          this.relation_table = true;
          this.isOpen_relation = false;
        }

        if (this.tablelist_Indexes == "" || this.tablelist_Indexes == null) {
          this.unique_table = false;
        } else {
          this.unique_table = true;
          this.isOpen_unique_key = false;
        }

        if (this.useslist == "" || this.useslist == null) {
          this.use_table = false;
        } else {
          this.use_table = true;
          this.isOpen_Uses = false;
        }
        if (this.usedbylist == "" || this.usedbylist == null) {
          this.usedby_table = false;
        } else {
          this.usedby_table = true;
          this.isOpen_Usedby = false;
        }
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.changeDetector.detectChanges();
    });
  }
}
