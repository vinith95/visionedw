import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessrepositoryviewComponent } from './businessrepositoryview.component';

describe('BusinessrepositoryviewComponent', () => {
  let component: BusinessrepositoryviewComponent;
  let fixture: ComponentFixture<BusinessrepositoryviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessrepositoryviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessrepositoryviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
