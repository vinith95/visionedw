import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessGloassarySearchComponent } from './business-gloassary-search.component';

describe('BusinessGloassarySearchComponent', () => {
  let component: BusinessGloassarySearchComponent;
  let fixture: ComponentFixture<BusinessGloassarySearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessGloassarySearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessGloassarySearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
