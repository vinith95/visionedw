import {
  Component, ElementRef, HostListener, OnInit, ViewChild,Renderer2 
} from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { ApiService, GlobalService } from "src/app/service";
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { environment } from "../../../../../environments/environment";
import { MagnifiernewComponent } from "./../../../../shared/magnifiernew/magnifiernew/magnifiernew.component";
import { SlideInOutAnimation } from "./animations";
@Component({
  selector: "app-business-gloassary-search",
  templateUrl: "./business-gloassary-search.component.html",
  styleUrls: ["./business-gloassary-search.component.css"],
  animations: [SlideInOutAnimation],
})
export class BusinessGloassarySearchComponent implements OnInit {
  tablelist_columns: any = [];
  tablelist_relations: any = [];
  datatablelist_table: any = [];
  datatablelist_index: any = [];
  isOpen_columns_tab: boolean = false;
  isOpen_relation_tab: boolean = false;
  isOpen_table_tab: boolean = false;
  isOpen_index_tab: boolean = false;
  treeview_repository: boolean = false;
  noRecord: boolean = false;
  isOpen_columns = false;
  isOpen_relation = false;
  isOpen_table = false;
  isOpen_index = false;
  pageView: any;
  animationState = "in";
  searchTypeList: any;
  dataTypeList: any;
  treeview_erm: boolean = false;
  treeview_business: boolean = true;
  leftContainerHeight: string;
  BusinessetupFormSetupForm;
  nodetreevalues: any;
  submitted_Column: boolean = false;
  objectMandatoryList: any;
  selecteLanguage = "EN";
  maginerfiervalues: any = [];
  maginerfier_column_name: any = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: "alphaSubTab",
    textField: "description",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    noDataAvailablePlaceholderText: "No data available",
    enableCheckAll: false,
    itemsShowLimit: 2,
    maxHeight: 150,
    allowSearchFilter: true,
  };
  @ViewChild("addColumnPopup") private addColumnPopup: ElementRef;
  columnSetupForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private renderer: Renderer2,
    private translate: TranslateService,
    private globalService: GlobalService,
    private _apiService: ApiService,
    private modalService: NgbModal
  ) {
    this.translate.setDefaultLang("en");
    this.translate.use("en");
    this.translate.currentLoader.getTranslation(this.translate.currentLang);
  }

  ngOnInit(): void {
    this.formPageLoadValue();
    this.initBusinessManagementForm();
  }

  disabledDropdown() {
    return this.pageView == "view" ? true : false;
  }

  btnClearAll() {
    this.initBusinessManagementForm();
    this.tablelist_columns = [];
    this.tablelist_relations = [];
    this.datatablelist_table = [];
    this.datatablelist_index = [];
    this.isOpen_columns = false;
    this.isOpen_relation = false;
    this.isOpen_table = false;
    this.isOpen_index = false;
    this.isOpen_columns_tab = false;
    this.isOpen_relation_tab = false;
    this.isOpen_table_tab = false;
    this.isOpen_index_tab = false;
    this.noRecord = false;
  }

  openChildTable(data) {
    this.nodetreevalues = data;
    this.treeview_business = false;
    this.treeview_erm = true;
  }

  openChildTableRepository(data) {
    data.statusDesc = "repository";
    this.nodetreevalues = data;
    this.treeview_business = false;
    this.treeview_erm = false;
    this.treeview_repository = true;
  }

  @HostListener("document:keypress", ["$event"])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.btnSubmission();
    }
  }



  setHeightTable:any;
  objHeightForTable:any;
  btnSubmission() {
    this.noRecord = false;
    if (this.BusinessetupFormSetupForm.value.searchValue == "") {
      this.globalService.showToastr.warning("Please Enter Search Value");
    } else {
      let searchKey;
      if (this.BusinessetupFormSetupForm.value.searchKey != "" || null) {
        searchKey = this.BusinessetupFormSetupForm.value.searchKey
          .map((item) => {
            return item.alphaSubTab;
          })
          .join(",");
      } else {
        searchKey = "ALL";
      }
      let data = {
        searchValue: this.BusinessetupFormSetupForm.value.searchValue,
        searchKey: searchKey,
      };
           
      this._apiService
        .post(environment.businessGlossaryNew_getAllQueryResults, data)
        .subscribe((resp) => {
          if (resp["status"] == 1) {
            // debugger
            
            const ArrayVal = null;
            this.datatablelist_table = resp["response"][0];
            this.tablelist_columns = resp["response"][1];
            this.tablelist_relations = resp["response"][2];
            this.datatablelist_index = resp["response"][3];
            this.setHeightTable = 0;
            resp["response"] && resp["response"].forEach(e=> {
              if(e && e.length) {
                this.setHeightTable++;
              }
            })
            // this.setHeightTable = this.datatablelist_table.length +  this.tablelist_columns.length +
            // this.tablelist_relations.length +   this.datatablelist_index.length
            // debugger
            //  console.log("thisdsds",this.setHeightTable)
            //  console.log(resp)
            // console.log("thisdsds",this.datatablelist_table)
            if (
             this.datatablelist_table && this.datatablelist_table.length == 0  &&
              this.tablelist_columns && this.tablelist_columns.length == 0 &&
              this.tablelist_relations && this.tablelist_relations.length == 0 &&
              this.datatablelist_index && this.datatablelist_index.length ==0
            
            ) {
              this.noRecord = true;
            }
            if (
              this.tablelist_columns == "" ||
              this.tablelist_columns == ArrayVal
            ) {
              this.isOpen_columns = false;
              this.isOpen_columns_tab = false;
             
            } else {
              this.isOpen_columns_tab = true;
              this.isOpen_columns = false;
            }

            if (
              this.tablelist_relations == "" ||
              this.tablelist_relations == ArrayVal
            ) {
             // console.log("checking");

              
              this.isOpen_relation = false;
              this.isOpen_relation_tab = false;
            } else {
              this.isOpen_relation = false;
              this.isOpen_relation_tab = true;
            }
            if (
              this.datatablelist_table == "" ||
              this.datatablelist_table == ArrayVal
            ) {
             
              this.isOpen_table = false;
              this.isOpen_table_tab = false;
            } else {
              this.isOpen_table = false;
              this.isOpen_table_tab = true;
            }

            if (
              this.datatablelist_index == "" ||
              this.datatablelist_index == ArrayVal
            ) {
             
              this.isOpen_index = false;
              this.isOpen_index_tab = false;
            } else {
              this.isOpen_index = false;
              this.isOpen_index_tab = true;
            }
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
    }
  }

  toggleShowDiv(value: string, event) {
    if (!event.target.closest(".align-icon-edit")) {
      if (value == "Columns") {
        this.isOpen_columns = !this.isOpen_columns;
        this.isOpen_relation = false;
        this.isOpen_table = false;
        this.isOpen_index = false;
        this.classadding();
      }
      if (value == "Relations") {
        this.isOpen_relation = !this.isOpen_relation;
        this.isOpen_columns = false;
        this.isOpen_table = false;
        this.isOpen_index = false;

        this.classadding();
      }
      if (value == "table") {
        this.isOpen_table = !this.isOpen_table;
        this.isOpen_columns = false;
        this.isOpen_relation = false;
        this.isOpen_index = false;
        this.classadding();
      }
      if (value == "index") {
        this.isOpen_index = !this.isOpen_index;
        this.isOpen_columns = false;
        this.isOpen_relation = false;
        this.isOpen_table = false;
        this.classadding();
      }
    }
  }
  initBusinessManagementForm() {
    this.BusinessetupFormSetupForm = this.fb.group({
      searchValue: [""],
      searchKey: [""],
    });
  }
  classadding() {
    this.animationState = this.animationState === "out" ? "in" : "out";
  }

  backToSummaryClick() {
    this.treeview_business = true;
    this.treeview_erm = false;
    this.ngAfterViewInit();
  }

  backToSummaryClickRepository() {
    this.treeview_business = true;
    this.treeview_erm = false;
    this.treeview_repository = false;
    this.ngAfterViewInit();
  }

  formPageLoadValue() {
    this._apiService
      .get(environment.businessGlossaryNew_pageLoadValues)
      .subscribe((resp) => {
        this.searchTypeList = resp["response"][0];
        // console.log(resp)
        // console.log( this.searchTypeList.alphaSubTab)

      });
  }

  tableActionColumn() {
    this.formPageLoadValue();
    this.openPopupAddcolumn();
  }
  modalClose() {
    this.modalService.dismissAll();
  }

  openPopupAddcolumn() {
    this.submitted_Column = false;
    this.initColumnManagementForm();
    this.formPageLoadValueColumn();
    this.modalService.open(this.addColumnPopup, {
      size: "lg",
      backdrop: "static",
    });

    this.columnSetupForm.patchValue({
      description: this.tablelist_columns[0].description,
      dataType: this.tablelist_columns[0].dataType,
      dataLength: this.tablelist_columns[0].dataLength,
      mandatoryFlag: this.tablelist_columns[0].description,
      auditEnableFlag: this.tablelist_columns[0].auditEnableFlag,
      displayColumn: this.tablelist_columns[0].displayColumn,
      guidelines: this.tablelist_columns[0].tableName,
      dataIndex: this.tablelist_columns[0].dataIndex
    });
  }

  formPageLoadValueColumn() {
    this._apiService
      .get(environment.edwTable_pageLoadValues)
      .subscribe((resp) => {
        this.objectMandatoryList = resp["response"][2];
        this.dataTypeList = resp["response"][6];
      });
  }

  initColumnManagementForm() {
    this.columnSetupForm = new FormGroup({
      dataType: new FormControl(""),
      dataLength: new FormControl("", [
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
      ]),
      dataIndex: new FormControl("", [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      mandatoryFlag: new FormControl(""),
      guidelines: new FormControl(""),
      auditEnableFlag: new FormControl(true),
      displayColumn: new FormControl(true),
      colDataSensitivity: new FormControl(true),
      description: new FormControl(""),
      selecteLanguage: new FormControl("EN"),
    });
  }

  submitColumnSetup() {
    if (this.columnSetupForm.valid) {
      let rawValue = this.columnSetupForm.getRawValue();
      if (
        rawValue.dataType == "" &&
        rawValue.dataLength == "" &&
        rawValue.dataIndex == "" &&
        rawValue.mandatoryFlag == "" &&
        rawValue.guidelines == "" &&
        rawValue.description == ""
      ) {
        this.globalService.showToastr.warning("Please Choose Any Values");
      } else {
        let searchKey;
        if (this.BusinessetupFormSetupForm.value.searchKey != "" || null) {
          searchKey = this.BusinessetupFormSetupForm.value.searchKey
            .map((item) => {
              return item.alphaSubTab;
            })
            .join(",");
        } else {
          searchKey = "ALL";
        }

        if (this.tablelist_columns[0].description != rawValue.description) {
          rawValue.descriptionChange = true;
        } else {
          rawValue.descriptionChange = false;
        }
        if (rawValue.auditEnableFlag == false) {
          rawValue.auditEnableFlag = "N";
        } else {
          rawValue.auditEnableFlag = "Y";
        }
        if (rawValue.colDataSensitivity == false) {
          rawValue.colDataSensitivity = "N";
        } else {
          rawValue.colDataSensitivity = "Y";
        }
        if (rawValue.displayColumn == false) {
          rawValue.displayColumn = "N";
        } else {
          rawValue.displayColumn = "Y";
        }

        let data = {
          dataType: rawValue.dataType,
          dataLength: rawValue.dataLength,
          dataIndex: rawValue.dataIndex,
          mandatoryFlag: rawValue.mandatoryFlag,
          guidelines: rawValue.guidelines,
          selecteLanguage: rawValue.selecteLanguage,
          description: rawValue.description,
          descriptionChange: rawValue.descriptionChange,
          makerName: this.BusinessetupFormSetupForm.value.searchValue,
          bulkUpdate: "Y",
          auditEnableFlag: rawValue.auditEnableFlag,
          displayColumn: rawValue.displayColumn,
          colDataSensitivity: rawValue.colDataSensitivity,
        };
        this.updateColumndetails(data);
      }
    }
  }

  updateColumndetails(data) {
    const modRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    modRef.componentInstance.title = "Alert";
    modRef.componentInstance.popupType = "Confirm";
    modRef.componentInstance.message =
      "Do you want to proceed with all the records?";
    modRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp.type == "Yes") {
        setTimeout(() => {
          this._apiService
            .post(environment.edwBusGlossaryNew_modifyColumnData, data)
            .subscribe((resp) => {
              if (resp["status"] == 1) {
                this.globalService.showToastr.success(resp["message"]);
                this.modalClose();
                this.btnSubmission();
              } else {
                this.globalService.showToastr.error(resp["message"]);
              }
            });
          modRef.close();
        }, 10);
      }
      if (resp.type == "No") {
        modRef.close();
        this.modalClose();
      }
      modRef.close();
    });
  }


  opneMagniferData(title) {
    let query;
    query = "ERTABLES";
    const modelRef = this.modalService.open(MagnifiernewComponent, {
      size: "lg",
      backdrop: "static",
    });

    modelRef.componentInstance.query = query;
    modelRef.componentInstance.title = title;
    modelRef.componentInstance.inputDetails = this.maginerfiervalues;
    modelRef.componentInstance.filterData.subscribe((data) => {
      if (data != "close") {
        this.maginerfier_column_name = [];
        this.maginerfiervalues = [];
        let valuesbinding = "";
        this.maginerfiervalues = data;
        this.maginerfiervalues.forEach((element) => {
          if (element.checked == true) {
            this.maginerfier_column_name += element.columnNine + ",";
          }
        });
        valuesbinding = this.maginerfier_column_name.slice(0, -1);
        this.columnSetupForm.patchValue({
          ["guidelines"]: valuesbinding,

        });
      }

      modelRef.close();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }
}
