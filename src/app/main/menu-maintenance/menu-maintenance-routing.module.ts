import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BustFlagComponent } from './bust-flag/bust-flag.component';

// import { MenuMaintenanceListComponent } from './menu-maintenance-list/menu-maintenance-list.component';
import { ProfileSetupComponent } from './profile-setup/profile-setup.component';
import { CreateQueryComponent } from './query-module/create-query/create-query.component';
import { ManageQueryComponent } from './query-module/manage-query/manage-query.component';
import { ErmoduleComponent } from './../ermodule/ermodule.component';
import { PolicySetupComponent } from '../sub-modules/policy-setup/policy-setup.component';
import { UserSetupComponent } from './user-setup/user-setup.component';
import { EventconfigurationComponent } from './eventconfiguration/eventconfiguration.component';
import { AuditTrailComponent } from './audit-trail/audit-trail.component';
import { TablemetadataComponent } from './tablemetadata/tablemetadata.component';
import { BusiessGlossaryComponent } from './busiess-glossary/busiess-glossary.component';
import { CronStatusComponent } from './cron-status/cron-status.component';

import { BusinessGloassarySearchComponent } from './business-gloassary-module/business-gloassary-search/business-gloassary-search.component';
import { BusinessTableTreeViewComponent } from './business-gloassary-module/business-table-tree-view/business-table-tree-view.component';
import {AccountBucketsComponent} from './AccountBuckets/AccountBuckets.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'profile-setup',
  },

  {
    path: 'profile-setup',
    pathMatch: 'full',
    component: ProfileSetupComponent,
  },
  {
    path: 'erSchemaBrowser',
    pathMatch: 'full',
    component: ErmoduleComponent,
  },
  {
    path: 'burstFlag',
    pathMatch: 'full',
    component: BustFlagComponent,
  },
  {
    path: 'queryDesign',
    pathMatch: 'full',
    component: ManageQueryComponent,
  },
  {
    path: 'interactivecolumn',
    pathMatch: 'full',
    component: CreateQueryComponent,
  },
  {
    path: 'policySetup',
    pathMatch: 'full',
    component: PolicySetupComponent,
  },
  {
    path: 'visionUsers',
    pathMatch: 'full',
    component: UserSetupComponent,
  },
  {
    path: 'eventConfiguration',
    pathMatch: 'full',
    component: EventconfigurationComponent,
  },{
    path: 'auditTrail',
    pathMatch: 'full',
    component: AuditTrailComponent,
  },
  {
    path: 'tableMetaData',
    pathMatch: 'full',
    component: TablemetadataComponent,
  },
  {
    path: 'edwBusGlossary',
    pathMatch: 'full',
    component: BusiessGlossaryComponent,
  },
  {
    path: 'cronStatus',
    pathMatch: 'full',
    component: CronStatusComponent,
  },
  {
    path: 'edwBusGlossaryTreeView',
    pathMatch: 'full',
    component: BusinessTableTreeViewComponent,
  },
  {
    path:'edwBusGlossaryNew',
    pathMatch:'full',
    component: BusinessGloassarySearchComponent,

  },
  {
    path: 'AccountBuckets',
    pathMatch: 'full',
    component: AccountBucketsComponent,
  },


];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuMaintenanceRoutingModule { }
