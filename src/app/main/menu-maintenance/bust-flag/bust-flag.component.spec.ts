import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BustFlagComponent } from './bust-flag.component';

describe('BustFlagComponent', () => {
  let component: BustFlagComponent;
  let fixture: ComponentFixture<BustFlagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BustFlagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BustFlagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
