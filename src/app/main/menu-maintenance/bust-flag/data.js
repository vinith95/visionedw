export const tableDetail = {
  headers: [
    {
      name: "Seq No",
      referField: "sequence",
      filter: false,
      isFilterField: true,
      style: {
        // width: "4%"
      },
    },
    {
      name: "Email To",
      referField: "emailto",
      filter: false,
      isFilterField: true,
      style: {
        // width: "18%",
        "max-width": "150px",
      },
    },
    {
      name: "Email Subject",
      referField: "emailsubject",
      filter: false,
      isFilterField: true,
      style: {
        // width: "15%",
        "max-width": "150px",
      },
    },
    {
      name: "Email Body",
      referField: "emailbody",
      filter: false,
      isFilterField: true,
      style: {
        // width: "18%",
        "max-width": "150px",
      },
    },
    {
      name: "Email Status",
      referField: "emailstatus",
      filter: false,
      isFilterField: true,
      style: {
        // width: "8%"
      },
    },
    {
      name: "Prompts",
      referField: "prompts",
      filter: false,
      prompt: true,
      isFilterField: true,
      style: {
        // width: "3%"
      },
    },
    {
      name: "Status",
      referField: "rsbStatusDesc",
      filter: false,
      isFilterField: true,
      style: {
        // width: "8%"
      },
    },
    {
      name: "Record Indicator",
      referField: "recordIndicatorDesc",
      filter: false,
      isFilterField: true,
      style: {
        // width: "8%"
      },
    },
    {
      name: "Maker",
      referField: "makerName",
      filter: false,
      isFilterField: true,
      style: {
        // width: "8%"
      },
    },
    {
      name: "Verifier",
      referField: "verifierName",
      filter: false,
      isFilterField: true,
      style: {
        // width: "8%"
      },
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: true,
      actionName: "",
    },
    bulkApproveButton: {
      enable: true,
      actionName: "",
    },
    bulkRejectButton: {
      enable: true,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    showPagination: true,
    showCheckBox: true,
    showButton: true,
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
  },
  buttons: [
    {
      name: "copy",
      icon: "file_copy",
      actionType: "copy",
    },
    {
      name: "edit",
      icon: "mode_edit",
      actionType: "edit",
    },
    {
      name: "view",
      icon: "visibility",
      actionType: "view",
    },
    // {
    //     name: 'share',
    //     icon: 'share',
    //     actionType: 'share'
    // },
    {
      name: "delete",
      icon: "delete",
      actionType: "delete",
      validations: true,
      statusCondition: {
        key: "rsbStatus",
        value: [9],
      },
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 3, 4],
      },
    },
    {
      name: "approve",
      icon: "done",
      actionType: "approve",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [0],
      },
    },
    {
      name: "reject",
      icon: "do_not_disturb_alt",
      actionType: "reject",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [0],
      },
    },
    {
      name: "review",
      icon: "fact_check",
      actionType: "review",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [0, 2, 3, 4],
      },
    },
  ],
};
