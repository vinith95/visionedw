import {
  ChangeDetectorRef,
  Component,
  HostListener,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { ApiService, GlobalService, JwtService } from "src/app/service";
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { GenericTreeComponent } from "src/app/shared/generic-tree/generic-tree.component";
import { MagnifierComponent } from "src/app/shared/magnifier/magnifier.component";
import { ReviewComponent } from "src/app/shared/review/review.component";
import { TextEditorComponent } from "src/app/shared/text-editor/text-editor.component";
import { environment } from "src/environments/environment";
import { isNullOrUndefined } from "util";
import { tableDetail } from "./data.js";
// import KothingEditor from 'kothing-editor';
// import plugins from 'kothing-editor/lib/plugins';
import { distinctUntilChanged, pairwise } from "rxjs/operators";
import { FilterPromptComponent } from "src/app/shared/filter-prompt/filter-prompt.component";

@Component({
  selector: "app-bust-flag",
  templateUrl: "./bust-flag.component.html",
  styleUrls: ["./bust-flag.component.css"],
})
export class BustFlagComponent implements OnInit {
  leftContainerHeight: string;
  showFilter;
  tableDetail;
  ngxTableConfig: any = {
    api: "burstFlagQueryResults",
    dataToDisplay: [],
    filterApi: "burstFlagQueryResults",
    count: 10,
    homeDashboard: [],
  };
  lineList = [];
  pageView: any;
  resetModel: any = { key: "" };
  tabList: any = [
    {
      tabIcon: "settings",
      tabName: "Burst Flag",
      selected: true,
    },
  ];
  burstForm: FormGroup;
  submitted: boolean = false;
  reportIdList = [];
  burstIdList = [];
  editIndex: any;
  statusList = [];
  emailStatusList = [];
  // promptsList: any = [];
  // filterVal: any;
  // overallPromptValues: any = {};
  // filterForm: FormGroup;
  // magnifierId: any = {};
  // treeId: any = {};
  // showPrompt: boolean = false;
  // dropdownSettings: IDropdownSettings = {
  //   singleSelection: false,
  //   idField: 'id',
  //   textField: 'description',
  //   selectAllText: 'Select All',
  //   unSelectAllText: 'UnSelect All',
  //   itemsShowLimit: 1,
  //   noDataAvailablePlaceholderText: 'No data available',
  //   allowSearchFilter: true,
  //   maxHeight: 160
  // };

  // dashboardFilterSettings: IDropdownSettings = {
  //   singleSelection: true,
  //   idField: 'id',
  //   textField: 'description',
  //   selectAllText: 'Select All',
  //   unSelectAllText: 'UnSelect All',
  //   itemsShowLimit: 1,
  //   allowSearchFilter: true,
  //   noDataAvailablePlaceholderText: 'No data available',
  //   closeDropDownOnSelection: true,
  //   maxHeight: 160
  // };
  // promptDescriptions: any = {};
  // promptValues: any = {};
  editData: any = {};
  compareForm: any = {};
  emailBodytext: any;
  filterFormValues: any = {};
  filterFormDescriptions: any = {};

  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    public globalService: GlobalService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.tableDetail = tableDetail;
    this.pageLoadValues();
    this.initForm();

    // this.editor = new Editor();
  }

  initForm() {
    this.burstForm = this.formBuilder.group({
      reportId: ["", [Validators.required]],
      // reportIdDesc: [{value: '', disabled: true}],
      burstId: [
        "",
        [Validators.required, Validators.pattern("^[a-zA-Z0-9._-]+$")],
      ],
      burstIdDesc: [""],
      sequence: [
        "",
        [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
      ],
      emailTo: ["", [Validators.required]],
      emailStatus: [{ value: "", disabled: true }],
      emailSubject: ["", [Validators.required]],
      emailBody: ["", [Validators.required]],
      status: [""],
      // prompts: ['',[Validators.required]],
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
      this.cdr.detectChanges();
    }, 100);
    this.cdr.detectChanges();
  }

  // @HostListener('window: resize', ['$event']) onResize(event) {
  //   const getContentAreaHeight = window.innerHeight;
  //   const navbarHeight = !isNullOrUndefined(document.getElementsByClassName('navbar')) ?
  //     document.getElementsByClassName('navbar')[0].clientHeight : 0;
  //   this.leftContainerHeight = getContentAreaHeight + 'px';
  //   document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
  //   document.getElementById('card-body').children[0]['style'].height = '100%';
  // }

  tableAction(e) {
    this.lineList = e.lineList;
    this.pageView = e.type;
    if (e.type == "add") {
      this.addForm(e);
      // this.showForm = true;
      // this.formPageLoadValue('add');
    }
    if (
      e.type == "edit" ||
      e.type == "view" ||
      e.type == "copy" ||
      e.type == "preview"
    ) {
      // this.showForm = true;
      this.editIndex = e.index;
      this.editForm(e);
    }
    if (e.type == "refresh" || e.type == "pagination") {
      this.resetModel = Object.assign({}, { key: "reload" });
      // let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 }
      // this.apiService.post(environment['etlManagerList'], data).subscribe((resp) => {
      //   this.lineList = resp['response'];
      //   this.ngxTableConfig['filterData'] = e.filter
      // })
    }
    if (e.type == "approve" || e.type == "reject" || e.type == "delete") {
      this.openGenericPopup(e);
    }
    if (e.type == "review") {
      this.reviewRow(e.data);
    }
    if (e.type == "bulkApprove") {
      if (Array.isArray(e.data) && e.data.length > 0) {
        let isDataContainsApproved = e.data.some((x) => {
          return x.recordIndicator == 0;
        });
        let differFlag = 0;
        e.data.forEach((element) => {
          if (element.maker == localStorage.getItem("visionId")) differFlag++;
        });
        if (isDataContainsApproved) {
          this.globalService.showToastr.warning(
            "Please uncheck approved record from your selected records!"
          );
        } else if (differFlag != 0) {
          this.globalService.showToastr.warning(
            "You can't approve/reject your records!"
          );
        } else {
          this.bulkApproveRejectFn(e);
        }
      }
    }
    if (e.type == "bulkReject") {
      if (Array.isArray(e.data) && e.data.length > 0) {
        let isDataContainsApproved = e.data.some((x) => {
          return x.recordIndicator == 0;
        });
        if (isDataContainsApproved) {
          this.globalService.showToastr.warning(
            "Please uncheck approved record from your selected records!"
          );
        } else {
          this.bulkApproveRejectFn(e);
        }
      }
    }
    if (e.type == "prompt") {
      let data = {
        reportId: e.data.reportId,
        burstId: e.data.burstId,
        burstIdDesc: e.data.burstIdDesc,
        sequence: e.data.sequence,
      };
      this.apiService
        .post(environment.getQueryDetailsburst, data)
        .subscribe((resp) => {
          if (resp["status"]) {
            this.editData = resp["response"][0];
            let filterRefCode = "";
            this.reportIdList.forEach((filter) => {
              if (filter.alphaSubTab == e.data.reportId) {
                filterRefCode = filter.makerName;
              }
            });
            this.promptLoad(filterRefCode, "", true);
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
    }
  }

  addForm(e) {
    this.initForm();
    this.showFilter = "form";
    // this.tableFormData = e.formData
    this.filterFormValues = {};
    this.filterFormDescriptions = {};
    this.emailBodytext = "";
    this.burstForm.patchValue({
      reportId: e.formData.reportId,
      status: "0",
      // burstId: e.formData.burstId
    });
    this.burstForm.get("status").disable();
    // this.getSequenceNumber();
    // this.promptsList = [];
    // this.filterForm = undefined;
    this.compareForm = this.burstForm.getRawValue();
  }

  editForm(e) {
    let data = {
      reportId: e.data.reportId,
      burstId: e.data.burstId,
      burstIdDesc: e.data.burstIdDesc,
      sequence: e.data.sequence,
    };
    this.apiService
      .post(environment.getQueryDetailsburst, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.showFilter = "form";
          this.editData = resp["response"][0];
          this.burstForm.patchValue({
            reportId: this.editData.reportId,
            // reportIdDesc: [{value: '', disabled: true}],
            burstId: this.editData.burstId,
            burstIdDesc: this.editData.burstIdDesc,
            sequence: this.editData.sequence,
            emailTo: this.editData.emailto,
            emailStatus: this.editData.emailstatus,
            emailSubject: this.editData.emailsubject,
            emailBody: this.editData.emailbody,
            status: this.editData.rsbStatus.toString(),
            // prompts: ['',[Validators.required]],
          });
          this.emailBodytext = this.editData.emailbody;
          // this.loadEditor(false);
          // this.promptLoad();
          this.editFilterForm(e);
          if (e.type != "edit" && e.type != "copy") {
            this.burstForm.disable();
          } else {
            if (e.type != "copy") {
              this.burstForm.get("reportId").disable();
              this.burstForm.get("burstId").disable();
              this.burstForm.get("burstIdDesc").disable();
            } else {
              // this.burstForm.get('status').disable();
              // this.tableFormData = {reportId: this.editData.reportId}
              this.getSequenceNumber();
            }
          }
        }
      });
  }

  editFilterForm(e) {
    let filterRefCode = "";
    this.reportIdList.forEach((filter) => {
      if (filter.alphaSubTab == this.burstForm.getRawValue()["reportId"]) {
        filterRefCode = filter.makerName;
      }
    });
    this.promptLoad(filterRefCode, "edit", false);
  }

  openGenericPopup(e) {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    let prompt = "";
    if (e.type == "approve") {
      prompt = "Are you sure, you want to approve the record?";
    }
    if (e.type == "reject") {
      prompt = "Are you sure, you want to reject the record?";
    }
    if (e.type == "delete") {
      prompt = "Are you sure, you want to delete the record?";
    }
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = prompt;
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.approveOrReject(e);
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }

  approveOrReject(e) {
    let data = {
      reportId: e.data.reportId,
      burstId: e.data.burstId,
      burstIdDesc: e.data.burstIdDesc,
      sequence: e.data.sequence,
    };
    let apiUrl =
      e.type == "approve"
        ? environment.approveBurstId
        : e.type == "reject"
        ? environment.rejectBurstId
        : e.type == "delete"
        ? environment.deleteBurstId
        : "";
    this.apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp["status"]) {
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
      this.resetModel = Object.assign({}, { key: "reload" });
      this.onSubmit("summary");
    });
  }

  reviewRow(cData) {
    let data = {
      reportId: cData.reportId,
      burstId: cData.burstId,
      burstIdDesc: cData.burstIdDesc,
      sequence: cData.sequence,
    };
    this.apiService.post(environment.reviewBurstId, data).subscribe((resp) => {
      if (resp["status"] == 1) {
        const modelRef = this.modalService.open(ReviewComponent, {
          size: "lg",
          backdrop: "static",
          windowClass: "review-popup",
        });
        modelRef.componentInstance.response = resp["response"];
        modelRef.componentInstance.closePopup.subscribe((e) => {
          modelRef.close();
        });
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  bulkApproveRejectFn(e) {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    let prompt = "";
    if (e.type == "bulkApprove") {
      prompt = "Are you sure, you want to approve these records?";
    }
    if (e.type == "bulkReject") {
      prompt = "Are you sure, you want to reject these records?";
    }
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = prompt;
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.bulkApproveRejectApi(e);
        // this.resetModel = Object.assign({}, { key: 'reload' });
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }

  bulkApproveRejectApi(e) {
    let data = [];
    e.data.forEach((element) => {
      data.push({
        reportId: element.reportId,
        burstId: element.burstId,
        burstIdDesc: element.burstIdDesc,
        sequence: element.sequence,
        recordIndicator: element.recordIndicator,
        checked: element.checked,
      });
    });
    let apiUrl =
      e.type == "bulkApprove"
        ? environment.bulkApproveBurstId
        : environment.bulkRejectBurstId;
    this.apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp["status"]) {
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
      this.resetModel = Object.assign({}, { key: "reload" });
      this.onSubmit("summary");
    });
  }

  disableButton(name) {
    if (name == "first") {
      return !this.editIndex ? true : false;
    }
    if (name == "last") {
      return this.lineList.length == 1
        ? true
        : this.editIndex == this.lineList.length - 1
        ? true
        : false;
    }
  }

  pageLoadValues() {
    this.apiService
      .get(environment.burstFlagpageLoadValues)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.statusList = resp["response"][0];
          this.emailStatusList = resp["response"][2];
          this.ngxTableConfig.reportId = resp["response"][3];
          this.reportIdList = resp["response"][4];
        }
        this.showFilter = "list";
      });
  }

  onSubmit(type) {
    if (type == "summary") {
      this.submitted = false;
      this.pageLoadValues();
      this.initForm();
      this.filterFormValues = {};
      this.filterFormDescriptions = {};
      this.emailBodytext = "";
      // this.promptsList = [];
      // this.filterForm = undefined;
    }
    if (type == "save" || type == "modify") {
      if (this.burstForm.invalid || !this.isFilterFormValid()) {
        this.submitted = true;
      } else {
        this.saveLine(type);
      }
    }
    if (
      type == "next" ||
      type == "previous" ||
      type == "last" ||
      type == "first"
    ) {
      this.navigateFn(type);
    }
    if (type == "approve" || type == "reject" || type == "delete") {
      this.approveOrRejectInForm(type);
    }
    if (type == "review") {
      let data = {
        reportId: this.editData.reportId,
        burstId: this.editData.burstId,
        burstIdDesc: this.editData.burstIdDesc,
        sequence: this.editData.sequence,
      };
      this.reviewRow(data);
    }
    if (type == "clear") {
      if (this.compareForm != JSON.stringify(this.burstForm.getRawValue())) {
        this.clearField();
      }
    }
  }

  approveOrRejectInForm(type) {
    let data = {
      reportId: this.editData.reportId,
      burstId: this.editData.burstId,
      burstIdDesc: this.editData.burstIdDesc,
      sequence: this.editData.sequence,
    };
    let apiUrl =
      type == "approve"
        ? environment.approveBurstId
        : type == "reject"
        ? environment.rejectBurstId
        : type == "delete"
        ? environment.deleteBurstId
        : "";
    this.apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp["status"]) {
        this.globalService.showToastr.success(resp["message"]);
        this.ngxTableConfig["formField"] = {
          formPage: true,
          reportId: this.burstForm.getRawValue()["reportId"],
          burstId: this.burstForm.getRawValue()["burstId"],
        };
        this.onSubmit("summary");
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  clearField() {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "Are You sure, you want to Clear ?";
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.burstForm.reset();
        this.filterFormValues = {};
        this.filterFormDescriptions = {};
        this.emailBodytext = "";
        this.burstForm.patchValue({
          status: "0",
        });
        this.burstForm.get("status").disable();
        // this.filterForm = undefined;
        // this.promptsList = [];
        // let e = { type: 'add', formData: this.tableFormData, lineList: this.lineList }
        //   this.submitted = false
        //   this.tableAction(e)
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }
  navigateFn(type) {
    // let checkIndex;
    // this.lineList.forEach((element, index) => {
    //   if (element.extractionSequence == this.editData.extractionSequence) {
    //     checkIndex = index;
    //   }
    // });
    this.burstForm.reset();
    if (type == "next") {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
    if (type == "previous") {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
    if (type == "last") {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
    if (type == "first") {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
  }

  saveLine(type) {
    let apiUrl =
      type == "modify" ? environment.modifyBurstId : environment.addBurstId;
    let rawValue = this.burstForm.getRawValue();
    let data = {
      reportId: rawValue.reportId,
      burstId: rawValue.burstId,
      burstIdDesc: rawValue.burstIdDesc,
      sequence: rawValue.sequence,
      emailto: rawValue.emailTo,
      emailsubject: rawValue.emailSubject,
      emailbody: rawValue.emailBody,
      emailstatus: rawValue.emailStatus,
      rsbStatus: rawValue.status,
    };
    data = {
      ...data,
      ...this.filterFormValues,
      ...this.filterFormDescriptions,
    };
    this.apiService.post(apiUrl, data).subscribe((resp) => {
      if (resp["status"]) {
        if (type == "save") {
          this.afterSave();
        } else {
          let e = {
            type: "edit",
            data: this.editData,
            index: this.editIndex,
          };
          this.tableAction(e);
        }
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  afterSave() {
    document.body.classList.add("genericPopup");
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "Do you want to Add more record ?";
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        // this.burstForm.reset();
        let e = {
          type: "add",
          formData: { reportId: this.burstForm.getRawValue()["reportId"] },
          lineList: this.lineList,
        };
        this.submitted = false;
        this.tableAction(e);
      } else {
        this.ngxTableConfig["formPage"] = true;
        this.ngxTableConfig["formField"] = {
          formPage: true,
          reportId: this.burstForm.getRawValue()["reportId"],
          burstId: this.burstForm.getRawValue()["burstId"],
        };
        this.onSubmit("summary");
      }
      document.body.classList.remove("genericPopup");
      modelRef.close();
    });
  }
  reportIdChange() {
    // this.getSequenceNumber();
    // this.promptsList = [];
    // this.filterForm = undefined;
  }

  burstIdBlur() {
    this.getSequenceNumber();
  }

  getSequenceNumber() {
    let req = {
      reportId: this.burstForm.getRawValue()["reportId"],
      burstId: this.burstForm.getRawValue()["burstId"],
      // burstIdDesc: this.burstForm.getRawValue()['burstIdDesc'],
    };
    this.apiService
      .post(environment.getSequenceNumber, req)
      .subscribe((resp) => {
        this.burstForm.patchValue({
          sequence: resp["response"],
        });
      });
  }

  promptClick(event) {
    event.srcElement.blur();
    event.preventDefault();
    // this.showPrompt = true;
    let filterRefCode = "";
    this.reportIdList.forEach((filter) => {
      if (filter.alphaSubTab == this.burstForm.getRawValue()["reportId"]) {
        filterRefCode = filter.makerName;
      }
    });
    this.promptLoad(filterRefCode, "", true);
  }

  promptLoad(filterRefCode, type, clicked) {
    const modelRef = this.modalService.open(FilterPromptComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: type == "edit" ? "prompt-close" : "prompt-popup",
    });
    modelRef.componentInstance.filterRefCode = filterRefCode;
    modelRef.componentInstance.pageView = this.pageView;
    modelRef.componentInstance.editData = this.editData;
    modelRef.componentInstance.clicked = clicked;
    modelRef.componentInstance.filterFormValues = this.filterFormValues;
    modelRef.componentInstance.filterFormDescriptions =
      this.filterFormDescriptions;
    modelRef.componentInstance.filterData.subscribe((resp) => {
      if (resp["type"] == "submit" || resp["type"] == "getValue") {
        this.filterFormValues = resp["promptValues"];
        this.filterFormDescriptions = resp["promptDescriptions"];
      }
      if (resp["type"] == "clear") {
        this.filterFormValues = {};
        this.filterFormDescriptions = {};
      }
      modelRef.close();
    });
    // type == 'edit' ? this.modalService.dismissAll() : '';
  }

  backGroundFlag(recordIndicator) {
    if (!recordIndicator) {
      return {
        background: "#448847",
      };
    } else {
      return {
        background: "#ffa828",
      };
    }
  }

  openEditor(event) {
    event.srcElement.blur();
    event.preventDefault();
    this.loadEditor();
  }

  loadEditor() {
    const modelRef = this.modalService.open(TextEditorComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "editor-popup",
    });
    modelRef.componentInstance.bodyText = this.emailBodytext;
    modelRef.componentInstance.emitData.subscribe((resp) => {
      if (resp["type"] == "submit" || resp["type"] == "clear") {
        this.emailBodytext = !isNullOrUndefined(resp["value"])
          ? resp["value"]
          : "";
        this.burstForm.patchValue({
          emailBody: !isNullOrUndefined(resp["value"]) ? resp["value"] : "",
        });
      }
      modelRef.close();
    });
  }

  // ngOnDestroy(): void {
  //   this.editor.destroy();
  // }

  isFilterFormValid() {
    if (Object.keys(this.filterFormValues).length) {
      return true;
    } else {
      return false;
    }
  }
}
