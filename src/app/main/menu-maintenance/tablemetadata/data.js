export const tableDetail = {
  headers: [
    {
      name: "Repository",
      referField: "objectRepositoryDesc",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Type",
      referField: "objectTypeDesc",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Table Name",
      referField: "objectName",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Table Alias Name",
      referField: "objectName",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Table Description",
      referField: "description",
      style: { "max-width": "200px" },
      filter: false,
      isFilterField: true,
    },
    {
      name: "Status",
      referField: "statusDesc",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
    {
      name: "Record Indicator",
      referField: "recordIndicatorDesc",
      filter: false,
      isFilterField: true,
    },

    {
      name: "Audit Date",
      referField: "objHistoryRefreshDated",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Data Last Modified",
      referField: "dateLastModified",
      filter: false,
      isFilterField: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "reload",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 0],
      },
    },
    {
      name: "view",
      icon: "visibility",
      actionType: "view",
    },
    {
      name: "refresh",
      icon: "refresh",
      actionType: "refresh",
      validations: true,
      conditon: {
        key: "objHistoryRefreshFlag",
        value: ["Y"],
      },
    },
  ],
};

export const columnDetail = {
  headers: [
    {
      name: "Order",
      referField: "columnOrder",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Column Id",
      referField: "columnId",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Column Name",
      referField: "columnName",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Description",
      referField: "description",
      style: { "max-width": "200px" },
      filter: false,
      isFilterField: true,
    },
    {
      name: "Index",
      referField: "dataIndex",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Mandatory Flag",
      referField: "mandatoryFlag",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Data Sensitivity",
      referField: "colDataSensitivity",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
    {
      name: "Data Last Modified",
      referField: "dateLastModified",
      filter: false,
      isFilterField: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: false,
      actionName: "add",
    },
    filterButton: {
      enable: false,
      actionName: "",
    },
    refreshButton: {
      enable: false,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: false,
      currentPage: 1,
    },
    showPagination: false,
    showCheckBox: false,
    showButton: false,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
    },
  ],
};
export const relationDetail = {
  headers: [
    {
      name: "From Table",
      referField: "tableName",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Relation Name",
      referField: "relationName",
      filter: false,
      isFilterField: true,
    },

    {
      name: "To Table",
      referField: "relationTablename",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Relation String",
      referField: "relationString",
      style: { "max-width": "230px" },
      filter: false,
      isFilterField: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: false,
      actionName: "add",
    },
    filterButton: {
      enable: false,
      actionName: "",
    },
    refreshButton: {
      enable: false,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: false,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: false,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
    },
  ],
};

export const indexDetail = {
  headers: [
    {
      name: "Order Index",
      referField: "orderIndex",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Key Name",
      referField: "indexName",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Index Type",
      referField: "indexType",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Description",
      referField: "description",
      filter: false,
      isFilterField: true,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: false,
      actionName: "add",
    },
    filterButton: {
      enable: false,
      actionName: "",
    },
    refreshButton: {
      enable: false,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: false,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: false,
  },
  buttons: [
    {
      name: "edit",
      icon: "edit",
      actionType: "edit",
    },
  ],
};
