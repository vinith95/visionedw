import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablemetadataComponent } from './tablemetadata.component';

describe('TablemetadataComponent', () => {
  let component: TablemetadataComponent;
  let fixture: ComponentFixture<TablemetadataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablemetadataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablemetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
