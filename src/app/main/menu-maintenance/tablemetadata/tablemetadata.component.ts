import { CdkVirtualScrollViewport } from "@angular/cdk/scrolling";
import { DatePipe } from "@angular/common";
import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { VirtualScrollerComponent } from "ngx-virtual-scroller";
import { GenericPopupComponent } from "src/app/shared/generic-popup/generic-popup.component";
import { isNullOrUndefined } from "util";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";
import { MagnifiernewComponent } from "./../../../shared/magnifiernew/magnifiernew/magnifiernew.component";
import {
  columnDetail,
  indexDetail,
  relationDetail,
  tableDetail,
} from "./data.js";
declare let d3: any;
declare let $: any;

@Component({
  selector: "app-tablemetadata",
  templateUrl: "./tablemetadata.component.html",
  styleUrls: ["./tablemetadata.component.css"],
})
export class TablemetadataComponent implements OnInit {
  tableDetail;
  columnDetail;
  indexDetail;
  relationDetail;

  columnlistDetail = [];
  indexlistDetail = [];
  relationlistDetail = [];
  lineList = [];
  metaDataList: any;
  resetModel: any = { key: "" };
  ngxTableConfig: any = {
    api: "edwTable_getAllQueryResults",
    dataToDisplay: [],
    filterApi: "edwTable_getAllQueryResults",
    count: 10,
    homeDashboard: [],
  };

  ngxTableConfig2: any = {
    api: "",
    dataToDisplay: [],
    filterApi: "",
    count: 10,
    homeDashboard: [],
  };

  ngxTableConfig3: any = {
    api: "",
    dataToDisplay: [],
    filterApi: "",
    count: 10,
    homeDashboard: [],
  };
  ngxTableConfig4: any = {
    api: "",
    dataToDisplay: [],
    filterApi: "",
    count: 10,
    homeDashboard: [],
  };

  leftContainerHeight: string;
  addAnalayze: boolean = false;
  landingPage: boolean = false;

  tableSetupForm: FormGroup;
  columnSetupForm: FormGroup;
  relationSetupForm: FormGroup;
  indexSetupForm: FormGroup;
  submitted: boolean = true;
  submitted_Column: boolean = false;
  submitted_Relation: boolean = false;
  submitted_index: boolean = false;

  objectTypeList: any;
  objectCatelgoryList: any;
  objectMandatoryList: any;
  objectRelationList: any;

  objectAccessList: any;
  objectHistoryList: any;
  objectYesOrNoList: any;

  recordIndicatorList: any;
  statusList: any;
  tablesTabValues: boolean = false;
  initEditor: any;

  tabletab: boolean = false;
  columnstab: boolean = false;
  relationtab: boolean = false;
  indextab: boolean = false;

  addTableTab: boolean = false;
  columnTabTable: boolean = false;
  indexTabTable: boolean = false;
  relationTabTable: boolean = false;
  tablecolumnvalues: any = [];
  editData: any = {};
  addTableBtn: boolean = false;
  modifyTableBtn: boolean = false;
  showbuttonModify: boolean = false;
  //relations --d3
  sourceTableObj: any = {};
  targetTableList: any[];
  targetTable: any[];
  sourceTable: any[];
  popupd3ForeignObject: any;
  popupsvg: any;
  popupd3RelationSvg: any = [];
  popupd3LinkConnectionData: any = [];
  popupd3Links = [];
  showConnector: boolean;
  editDataIndex: any;
  connection = [
    {
      type: "Connect by position",
      active: "",
    },
    {
      type: "Connect by name",
      active: "",
    },
  ];
  popupconnectedSources = [];
  popupconnectedTargets = [];
  targetPopup = [];
  targetFlag: boolean = false;
  currentTran: any = {};
  //relations --d3

  selectedTargetTable: any;
  fullScreenDisp: boolean = false;
  elem: HTMLElement;
  dragDisable: boolean = false;
  arrayTemporary = [];
  d3ToolTipDiv: any;
  d3PopupDiv: any;
  d3sourceId: any;
  d3targetId: any;

  source_Table: any = [];
  target_Table: any = [];

  relationDispalyFlag: boolean = true;
  realtionString: any = [];
  selecteLanguage = "EN";
  values: any;
  newArray: any = [];
  fromTableName: any;
  promptValue1: any;
  promptValue2: any;
  global_productName: any;
  global_connectionId: any;
  global_schema: any;
  global_selecteLanguage: any;
  query: any;
  branchList1;
  selectedData;
  startIndex;
  lastIndex;
  totalRows;
  branchList;

  disabledcheck: boolean = false;
  showSpinner: boolean = false;
  targettablename: any;
  temparray = [];
  newarray = [];
  @ViewChild("connectionPopupRelation")
  private connectionPopupRelation: ElementRef;
  @ViewChild("indexPopup") private indexPopup: ElementRef;
  @ViewChild("addColumnPopup") private addColumnPopup: ElementRef;
  @ViewChild("perfectScroll", { static: false })
  perfectScroll: VirtualScrollerComponent;

  @ViewChild("missingRelationTemplate", { static: false })
  missingRelationSetupTemplate: ElementRef;

  selectedTab: any;
  editDataRelations: any;
  separateArrayvalues: any = [];
  showContainer: boolean = false;
  pageView: any;
  relationByIndexList: any = [];
  relationByNameList: any = [];
  tabList: any = [
    {
      tabIcon: "Tables",
      tabName: "table",
      tabNameobj: "Table",
      selected: false,
      showBackbutton: true,
    },
    {
      tabIcon: "Column",
      tabName: "column",
      tabNameobj: "Column",
      selected: false,
    },
    {
      tabIcon: "Relations",
      tabName: "relations",
      tabNameobj: "Relations",
      selected: false,
    },
    {
      tabIcon: "Indexes",
      tabName: "indexes",
      tabNameobj: "Indexes",
      selected: false,
    },
    {
      tabIcon: "Table Script",
      tabName: "script",
      tabNameobj: "Script",
      selected: false,
    },
  ];
  @Output() filterData: EventEmitter<any> = new EventEmitter<any>();

  mainArr = [];
  editDatacolumn: any;
  myId: any;
  mYIdColumns: any;
  datePipe: DatePipe = new DatePipe("en-US");
  dataTypeList: any = [];
  btnShowForColumns: boolean = false;
  columnDataShow: boolean = false;
  editColumnDataShow: boolean = false;
  isOpen_relation = false;
  isOpen_relation_sub = false;
  isMasterSel: boolean;
  checkedCategoryList: any = [];
  isMasterSel_byname: boolean;
  checkedCategoryListByName: any = [];
  results: any = [];
  editIndex: any;
  constructor(
    private globalService: GlobalService,
    private modalService: NgbModal,
    private _apiService: ApiService
  ) {}
  initilizetableSetupForm() {
    this.tableSetupForm = new FormGroup({
      objectType: new FormControl("", Validators.required),
      objectName: new FormControl("", [
        Validators.required,
        Validators.pattern("^[a-zA-Z _]*$"),
      ]),
      objectAliasName: new FormControl("", [Validators.required]),
      objectRepository: new FormControl("", Validators.required),
      historyAuditType: new FormControl("", Validators.required),

      objectPurgeFlag: new FormControl(true, Validators.required),
      objectBackupFlag: new FormControl(true, Validators.required),
      objectOptimizeFlag: new FormControl(true, Validators.required),
      objectSensitivity: new FormControl(true, Validators.required),
      objHistoryRefreshFlag: new FormControl(true, Validators.required),
      objectAcccess: new FormControl("", Validators.required),
      description: new FormControl(""),
      productName: new FormControl(""),
      schema: new FormControl(""),
      connectionId: new FormControl(""),
      selecteLanguage: new FormControl("EN"),
    });
  }

  initColumnManagementForm() {
    this.columnSetupForm = new FormGroup({
      columnName: new FormControl("", [Validators.required]),
      dataType: new FormControl("", [Validators.required]),

      guidelines: new FormControl(""),
      dataLength: new FormControl("", [
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
      ]),
      dataIndex: new FormControl("", [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      columnOrder: new FormControl("", [
        Validators.required,
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
      ]),
      mandatoryFlag: new FormControl("", Validators.required),
      auditEnableFlag: new FormControl(true),
      displayCoumn: new FormControl(true),
      colDataSensitivity: new FormControl(true),
      description: new FormControl(""),
      productName: new FormControl(""),
      schema: new FormControl(""),
      connectionId: new FormControl(""),
      selecteLanguage: new FormControl("EN"),
      tableName: new FormControl(""),
      dataScaling: new FormControl(""),

      columnId: new FormControl(""),
    });
  }

  initRelationManagementForm() {
    this.relationSetupForm = new FormGroup({
      relationName: new FormControl("", Validators.required),
      sourceTable: new FormControl("", Validators.required),
      targetTable: new FormControl("", Validators.required),
      relationTablename: new FormControl(""),
      relationString: new FormControl(""),
      selecteLanguage: new FormControl("EN"),
      relationDispalyFlag: new FormControl(true),
      description: new FormControl(""),
      productName: new FormControl(""),
      schema: new FormControl(""),
      connectionId: new FormControl(""),
    });
  }

  initIndexManagementForm() {
    this.indexSetupForm = new FormGroup({
      name: new FormControl("", Validators.required),
      selecteLanguage: new FormControl("EN"),
      description: new FormControl(""),
      Type: new FormControl("Unique"),
      productName: new FormControl(""),
      schema: new FormControl(""),
      connectionId: new FormControl(""),
      orderIndex: new FormControl("Unique"),
      tableName: new FormControl(""),
    });
  }
  @ViewChild(CdkVirtualScrollViewport, { static: false })
  viewport: CdkVirtualScrollViewport;
  ngOnInit(): void {
    this.formPageLoadValue();

    this.initRelationManagementForm();
    this.tableDetail = tableDetail;
    this.columnDetail = columnDetail;
    this.indexDetail = indexDetail;
    this.relationDetail = relationDetail;
    let data = {
      actionType: "Query",
      currentPage: 1,
      maxRecords: 5000,
    };
    this._apiService
      .post(environment.edwTable_getAllQueryResults, data)
      .subscribe((resp) => {
        this.metaDataList = resp["response"];
        this.landingPage = true;
      });
  }

  tabClick(tab) {
    this.tabList.forEach((ele) => {
      if (tab.tabName == ele.tabName) {
        this.selectedTab = tab.tabName;
        ele.selected = true;
      } else {
        ele.selected = false;
      }
    });
  }

  cancelBtnAll() {
    this.backToHmpage();
  }
  cancelColumnPopup() {
    this.modalClose();
  }

  dropDownValues() {
    this.objectAccessList = [
      { objId: "RW", description: "Read Write" },
      { objId: "R", description: "Read Only" },
    ];

    this.objectHistoryList = [
      { objId: "I", description: "Individual" },
      { objId: "F", description: "Full" },
    ];

    this.objectYesOrNoList = [
      { objId: "Y", description: "Yes" },
      { objId: "N", description: "No" },
    ];
  }

  formPageLoadValue() {
    this._apiService
      .get(environment.edwTable_pageLoadValues)
      .subscribe((resp) => {
        this.objectTypeList = resp["response"][0];
        this.objectCatelgoryList = resp["response"][1];
        this.objectMandatoryList = resp["response"][2];
        this.objectRelationList = resp["response"][3];
        this.statusList = resp["response"][4];
        this.recordIndicatorList = resp["response"][3];
        this.dataTypeList = resp["response"][6];
      });
  }

  tableSetupFormValid() {
    if (Object.keys(this.tableSetupForm).length) {
      return true;
    } else {
      return false;
    }
  }

  accordionClick(value) {
    if (value == "column") {
      this.columnstab = true;
      this.relationtab = false;
      this.indextab = false;
      this.tabletab = false;

      this.columnTabTable = true;
      this.tablesTabValues = false;
      this.indexTabTable = false;
      this.relationTabTable = false;
    } else if (value == "table") {
      this.columnstab = false;
      this.relationtab = false;
      this.indextab = false;
      this.tabletab = true;

      this.tablesTabValues = true;
      this.columnTabTable = false;
      this.indexTabTable = false;
      this.relationTabTable = false;
    } else if (value == "indexes") {
      this.columnstab = false;
      this.relationtab = false;
      this.indextab = true;
      this.tabletab = false;

      this.tablesTabValues = false;
      this.columnTabTable = false;
      this.indexTabTable = true;
      this.relationTabTable = false;
    } else if (value == "relations") {
      this.relationtab = true;
      this.columnstab = false;
      this.indextab = false;
      this.tabletab = false;

      this.tablesTabValues = false;
      this.columnTabTable = false;
      this.indexTabTable = false;
      this.relationTabTable = true;
    }
  }

  backToHmpage() {
    this.showContainer = false;
    this.landingPage = true;
  }

  openPopupAddcolumnAdd() {
    // console.log("this.pageView", this.pageView);
    if (this.pageView == "edit") {
      this.showbuttonModify = true;
      this.labelTxt = "Apply";
    }
    if (this.tableSetupForm.valid) {
      this.initColumnManagementForm();
      this.modalService.open(this.addColumnPopup, {
        size: "lg",
        backdrop: "static",
      });
    } else {
      this.globalService.showToastr.warning("Please Add Table Details");
      let tab = {
        tabIcon: "Tables",
        tabName: "table",
        selected: false,
      };
      this.tabClick(tab);
      this.btnShowForColumns = false;
    }
  }

  labelTxt: any;
  tableAction(e) {
    console.log(e);
    this.lineList = e.lineList;
    this.pageView = e.type;
    // console.log("this.pageView", this.pageView);
    if (e.type == "add") {
      this.labelTxt = "Apply";
      this.initilizetableSetupForm();
      this.columnlistDetail = [];
      this.indexlistDetail = [];
      this.relationlistDetail = [];
      this.editData = "";
      this.myId = "";
      this.mYIdColumns = "";
      this.columnDataShow = true;
      this.editColumnDataShow = false;
      if (this.relationlistDetail.length == 0) {
        this.myId = "chooseDatasourceSec3";
      }

      if (this.columnlistDetail.length) {
        this.mYIdColumns = "chooseDatasourcecolumn2";
      }
      this.showContainer = true;
      this.selectedTab = "table";
      let tab = {
        tabIcon: "Tables",
        tabName: "table",
        selected: false,
      };
      this.tabClick(tab);

      this.dropDownValues();
      this.submitted = false;
      this.landingPage = false;
      this.addTableTab = true;
      this.tabletab = true;
      this.accordionClick("table");

      this.addTableBtn = true;
      this.modifyTableBtn = false;
      this.showbuttonModify = false;
      this.btnShowForColumns = false;
    }
    if (
      e.type == "edit" ||
      e.type == "view" ||
      e.type == "preview" ||
      e.type == "approve" ||
      e.type == "reject"
    ) {
      this.labelTxt = "Modify";
      this.editIndex = e.index;
      this.showContainer = true;
      this.selectedTab = "table";
      this.columnDataShow = false;
      this.editColumnDataShow = true;
      this.btnShowForColumns = false;
      let tab = {
        tabIcon: "Tables",
        tabName: "table",
        selected: false,
      };
      this.tabClick(tab);
      this.addTableBtn = false;
      this.modifyTableBtn = true;
      this.editForm(e);
      this.dropDownValues();
      this.submitted = false;
      this.landingPage = false;
      this.addTableTab = true;
      this.tabletab = true;
      this.accordionClick("table");

      this.initilizetableSetupForm();
    }
    if (e.type == "refresh") {
      this._apiService
        .post(environment.edwTable_refreshHistoryAudit, e.data)
        .subscribe((resp) => {
          if (resp["status"] == "1") {
            let data = {
              actionType: "Query",
              currentPage: 1,
              maxRecords: 5000,
            };
            this._apiService
              .post(environment.edwTable_getAllQueryResults, data)
              .subscribe((resp) => {
                this.metaDataList = resp["response"];
                this.landingPage = true;
              });
            this.globalService.showToastr.success(resp["message"]);
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
    }
    if (e.type == "reload") {
      this.resetModel = Object.assign({}, { key: "reload" });
    }
    if (e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.edwTable_getAllQueryResults, data)
        .subscribe((resp) => {
          this.metaDataList = resp["response"];
          this.ngxTableConfig["filterData"] = e.filter;
          this.formPageLoadValue();
        });
    }
  }

  tableActionColumn(data) {
    console.log(data);
    // this.pageView = "edit";
    this.showbuttonModify = true;
    this.labelTxt = "Modify";
    if (data) {
      this.formPageLoadValue();
      this.editFormColumn(data);
      this.openPopupAddcolumn();
      this.submitted_Column = false;
    }
  }

  tableActionColumnEdit(data) {
    this.openPopupAddcolumn();
    this.editDatacolumn = data;
    this.formPageLoadValue();
    let rawValue = this.columnSetupForm.getRawValue();
    if (this.editDatacolumn.auditEnableFlag == "Y") {
      rawValue.auditEnableFlag = true;
    } else {
      rawValue.auditEnableFlag = false;
    }
    if (this.editDatacolumn.colDataSensitivity == "Y") {
      rawValue.colDataSensitivity = true;
    } else {
      rawValue.colDataSensitivity = false;
    }
    if (this.editDatacolumn.displayCoumn == "Y") {
      rawValue.displayCoumn = true;
    } else {
      rawValue.displayCoumn = false;
    }

    this.columnSetupForm.patchValue({
      columnName: this.editDatacolumn.columnName,
      columnId: this.editDatacolumn.columnId,
      schema: this.editDatacolumn.schema,
      columnOrder: this.editDatacolumn.columnOrder,
      dataType: this.editDatacolumn.dataType,
      guidelines: this.editDatacolumn.guidelines,
      charUsed: this.editDatacolumn.charUsed,
      dataLength: this.editDatacolumn.dataLength,
      dataScaling: this.editDatacolumn.dataScaling,
      mandatoryFlag: this.editDatacolumn.mandatoryFlag.toString(),
      auditEnableFlag: rawValue.auditEnableFlag,
      displayCoumn: rawValue.displayCoumn,
      colDataSensitivity: rawValue.colDataSensitivity,
      auditStartDate: this.editDatacolumn.auditStartDate,
      dataIndex: this.editDatacolumn.dataIndex,
      description: this.editDatacolumn.description,
    });
  }

  tableActionRelation(data) {
    if (data) {
      this.submitted_Relation = false;
      this.editFormRelation(data);
    }
  }

  deleFormRelation(data) {
    this._apiService
      .post(environment.edwTable_deleteRelationsData, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          this.modalClose();
          this.editDataRelations = resp["response"][0];
          this.resetModel = Object.assign({}, { key: "reload" });
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }

  tableActionIndex(e) {
    this.pageView = e.type;
    if (e.type == "edit") {
      this.editFormIndex(e);
    }
    if (e.type == "refresh" || e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.edwTable_getIndexDetails, data)
        .subscribe((resp) => {
          this.indexlistDetail = resp["response"];
          this.ngxTableConfig["filterData"] = e.filter;
          this.formPageLoadValue();
        });
    }
  }

  editFormIndex(e) {
    this.pageView = "edit";
    this.openPopupIndexx();
    this._apiService
      .post(environment.edwTable_getIndexDetails, e.data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.editDataIndex = resp["response"][0];
          this.indexSetupForm.get("name").disable();
          this.indexSetupForm.patchValue({
            name: this.editDataIndex.indexName,
            Type: this.editDataIndex.indexType,
            orderIndex: this.editDataIndex.orderIndex,
            schema: this.editDataIndex.schema,
            productName: this.editDataIndex.productName,
            tableName: this.editDataIndex.tableName,
            description: this.editDataIndex.description,
            selecteLanguage: this.editDataIndex.selecteLanguage,
            connectionId: this.editDataIndex.connectionId,
          });
        }
      });
  }

  editFormColumn(data) {
    this.editDatacolumn = [];
    this._apiService
      .post(environment.edwTable_getColumnDetails, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.editDatacolumn = resp["otherInfo"];

          if (this.editDatacolumn.displayCoumn == "Y") {
            this.editDatacolumn.displayCoumn = true;
          } else {
            this.editDatacolumn.displayCoumn = false;
          }

          if (this.editDatacolumn.auditEnableFlag == "Y") {
            this.editDatacolumn.auditEnableFlag = true;
          } else {
            this.editDatacolumn.auditEnableFlag = false;
          }
          if (this.editDatacolumn.colDataSensitivity == "Y") {
            this.editDatacolumn.colDataSensitivity = true;
          } else {
            this.editDatacolumn.colDataSensitivity = false;
          }

          this.columnSetupForm.get("columnName").disable();
          this.columnSetupForm.get("columnOrder").disable();
          this.columnTypeChange(this.editDatacolumn.dataType);
          this.columnSetupForm.patchValue({
            columnName: this.editDatacolumn.columnName,
            columnId: this.editDatacolumn.columnId,
            schema: this.editDatacolumn.schema,
            columnOrder: this.editDatacolumn.columnOrder,
            dataType: this.editDatacolumn.dataType,
            guidelines: this.editDatacolumn.guidelines,
            charUsed: this.editDatacolumn.charUsed,
            dataLength: this.editDatacolumn.dataLength,
            dataScaling: this.editDatacolumn.dataScaling,
            mandatoryFlag: this.editDatacolumn.mandatoryFlag.toString(),
            auditEnableFlag: this.editDatacolumn.auditEnableFlag,
            auditStartDate: this.editDatacolumn.auditStartDate,
            colDataSensitivity: this.editDatacolumn.colDataSensitivity,
            dataIndex: this.editDatacolumn.dataIndex,
            productName: this.editDatacolumn.productName,
            tableName: this.editDatacolumn.tableName,
            description: this.editDatacolumn.description,
            selecteLanguage: this.editDatacolumn.selecteLanguage,
            displayCoumn: this.editDatacolumn.displayCoumn,
            connectionId: this.editDatacolumn.connectionId,
          });
        }
      });
  }

  maginerfiervalues: any = [];
  maginerfier_column_name: any = [];
  opneMagniferData(title) {
    let query;
    query = "ERTABLES";
    const modelRef = this.modalService.open(MagnifiernewComponent, {
      size: "lg",
      backdrop: "static",
    });
    let rawValue = this.columnSetupForm.getRawValue();

    let valuesGuidelines = rawValue.guidelines.split(",").map((s) => s.trim());
    // let passingvalues = valuesGuidelines.slice(0, -1);
    this.maginerfiervalues = Object.values(valuesGuidelines).map((value) => {
      return { columnNine: value, checked: true };
    });
    modelRef.componentInstance.query = query;
    modelRef.componentInstance.title = title;
    modelRef.componentInstance.inputDetails = this.maginerfiervalues;
    modelRef.componentInstance.filterData.subscribe((data) => {
      if (data != "close") {
        this.maginerfier_column_name = [];
        this.maginerfiervalues = [];
        let valuesbinding = "";
        this.maginerfiervalues = data;
        this.maginerfiervalues.forEach((element) => {
          if (element.checked == true) {
            this.maginerfier_column_name += element.columnNine + ",";
          }
        });

        valuesbinding = this.maginerfier_column_name.slice(0, -1);
        this.columnSetupForm.patchValue({
          ["guidelines"]: valuesbinding,
        });
      }

      modelRef.close();
    });
  }

  editFormRelation(data) {
    this.mainArr = [];
    this.initRelationManagementForm();
    this._apiService
      .post(environment.edwTable_getRelationsDetails, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.initRelationManagementForm();
          this.editDataRelations = resp["response"][0];

          if (this.editDataRelations.relationDispalyFlag == "Y") {
            this.editDataRelations.relationDispalyFlag = true;
          } else {
            this.editDataRelations.relationDispalyFlag = false;
          }
          this.targettablename = this.editDataRelations.relationTablename;

          this.relationSetupForm.get("sourceTable").disable();
          this.relationSetupForm.get("targetTable").disable();
          this.relationSetupForm.patchValue({
            relationName: this.editDataRelations.relationName,
            sourceTable: this.editDataRelations.tableName,
            targetTable: this.editDataRelations.relationTablename,
            relationTablename: this.editDataRelations.relationTablename,
            relationString: this.editDataRelations.relationString,
            productName: this.editDataRelations.productName,
            schema: this.editDataRelations.schema,
            description: this.editDataRelations.description,
            selecteLanguage: this.editDataRelations.selecteLanguage,
            connectionId: this.editDataRelations.connectionId,
            relationDispalyFlag: this.editDataRelations.relationDispalyFlag,
          });

          this.modalService.open(this.connectionPopupRelation, {
            size: "xl",
            backdrop: "static",
            windowClass: "addLinkpopup",
          });
          this.loadMagnifierData();
          let data = {
            objectName: this.editDataRelations.relationTablename,
          };
          this.loadPopConnectionvalues(data);
          let arr = [];
          this.popupd3Links = [];
          this.separateArrayvalues = [];
          this.separateArrayvalues.push(this.editDataRelations.relationString);
          let values = this.editDataRelations.relationString;
          // console.log("values",values);

          arr = values.split("AND");
          arr.forEach((element) => {
            if (element) {
              let values = element.split("=");
              let src = values[0];
              let trg = values[1];
              this.mainArr.push({
                source: src.split(".")[1].replace(/\s*\,\s*/g, ""),
                target: trg.split(".")[1].replaceAll(" ", ""),
              });
            }
          });
        }
      });
  }

  editForm(e) {
    this.columnlistDetail = [];
    this.relationlistDetail = [];

    this._apiService
      .post(environment.edwTable_getQueryDetails, e.data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.editData = resp["otherInfo"];
          this.tableSetupForm.get("objectName").disable();
          this.tableSetupForm.get("objectAliasName").disable();
          //this.tableSetupForm.get("objectRepository").disable();
          this.tableSetupForm.get("objectType").disable();

          if (this.editData.objectBackupFlag == "N") {
            this.editData.objectBackupFlag = false;
          } else {
            this.editData.objectBackupFlag = true;
          }
          if (this.editData.objHistoryRefreshFlag == "N") {
            this.editData.objHistoryRefreshFlag = false;
          } else {
            this.editData.objHistoryRefreshFlag = true;
          }

          if (this.editData.objectPurgeFlag == "N") {
            this.editData.objectPurgeFlag = false;
          } else {
            this.editData.objectPurgeFlag = true;
          }

          if (this.editData.objectOptimizeFlag == "N") {
            this.editData.objectOptimizeFlag = false;
          } else {
            this.editData.objectOptimizeFlag = true;
          }

          if (this.editData.objectSensitivity == "N") {
            this.editData.objectSensitivity = false;
          } else {
            this.editData.objectSensitivity = true;
          }
          this.sourceTable = this.editData.objectName;
          this.relationSetupForm.patchValue({
            sourceTable: this.editData.objectName,
          });
          this.fromTableName = this.editData.objectName;
          this.tableSetupForm.patchValue({
            objectType: this.editData.objectType,
            objectName: this.editData.objectName,
            objectAliasName: this.editData.objectAliasName,
            objectRepository: this.editData.objectRepository,
            historyAuditType: this.editData.historyAuditType,
            objectPurgeFlag: this.editData.objectPurgeFlag,
            objectBackupFlag: this.editData.objectBackupFlag,
            objectOptimizeFlag: this.editData.objectOptimizeFlag,
            objectSensitivity: this.editData.objectSensitivity,
            objHistoryRefreshFlag: this.editData.objHistoryRefreshFlag,
            objectAcccess: this.editData.objectAcccess,
            description: this.editData.description,
            productName: this.editData.productName,
            connectionId: this.editData.connectionId,
            schema: this.editData.schema,
            selecteLanguage: this.editData.selecteLanguage,
          });

          (this.global_productName = this.editData.productName),
            (this.global_connectionId = this.editData.connectionId),
            (this.global_schema = this.editData.schema),
            (this.global_selecteLanguage = this.editData.selecteLanguage),
            (this.promptValue1 = this.editData.objectRepository);
          this.promptValue2 = this.editData.objectName;
          this.columnlistDetail = this.editData.edwObjColumnsList;
          this.relationlistDetail = this.editData.edwObjRelationsList;
          if (!isNullOrUndefined(this.relationlistDetail)) {
            if (this.relationlistDetail.length) {
              this.myId = "chooseDatasourceSec2";
            }
          }
          if (!isNullOrUndefined(this.columnlistDetail)) {
            if (this.columnlistDetail.length) {
              this.mYIdColumns = "chooseDatasourcecolumn";
            }
          }

          this.indexlistDetail = this.editData.edwObjIndexList;
          this.accordionClick("table");
        }
      });
  }

  isTableSetupFormValid() {
    if (Object.keys(this.tableSetupForm).length) {
      return true;
    } else {
      return false;
    }
  }
  arrayListColumnDetails: any = [];
  TableSetupSubmisssion(type) {
    if (type == "add") {
      if (this.columnlistDetail.length == 0) {
        this.globalService.showToastr.warning("Please Enter Column Details");
        let tab = {
          tabIcon: "Column",
          tabName: "column",
          selected: false,
        };
        this.tabClick(tab);
      } else {
        let apiUrl =
          type == "modify"
            ? environment.edwTable_modifyEdwObj
            : environment.edwTable_addEdwObj;
        if (this.tableSetupForm.invalid) {
          this.submitted = true;
        } else {
          let rawValue = this.tableSetupForm.getRawValue();
          let value = rawValue.objectName.toUpperCase();
          rawValue.objectName = value;
          if (this.editData.selecteLanguage != rawValue.selecteLanguage) {
            rawValue.descriptionChange = true;
          } else if (this.editData.description != rawValue.description) {
            rawValue.descriptionChange = true;
          } else {
            rawValue.descriptionChange = false;
          }

          if (rawValue.objectBackupFlag == false) {
            rawValue.objectBackupFlag = "N";
          } else {
            rawValue.objectBackupFlag = "Y";
          }

          if (rawValue.objHistoryRefreshFlag == false) {
            rawValue.objHistoryRefreshFlag = "N";
          } else {
            rawValue.objHistoryRefreshFlag = "Y";
          }
          if (rawValue.objectOptimizeFlag == false) {
            rawValue.objectOptimizeFlag = "N";
          } else {
            rawValue.objectOptimizeFlag = "Y";
          }
          if (rawValue.objectPurgeFlag == false) {
            rawValue.objectPurgeFlag = "N";
          } else {
            rawValue.objectPurgeFlag = "Y";
          }
          if (rawValue.objectSensitivity == false) {
            rawValue.objectSensitivity = "N";
          } else {
            rawValue.objectSensitivity = "Y";
          }
          rawValue.edwObjColumnsList = this.columnlistDetail;
          this._apiService.post(apiUrl, rawValue).subscribe((resp) => {
            if (resp["status"] == 1) {
              this.globalService.showToastr.success(resp["message"]);
              this.modalService.dismissAll();
              let data = {
                type: "pagination",
              };
              this.tableAction(data);
              this.cancelBtnAll();
            } else {
              this.globalService.showToastr.error(resp["message"]);
            }
          });
        }
      }
    } else {
      let apiUrl =
        type == "modify"
          ? environment.edwTable_modifyEdwObj
          : environment.edwTable_addEdwObj;
      if (this.tableSetupForm.invalid) {
        this.submitted = true;
      } else {
        let rawValue = this.tableSetupForm.getRawValue();
        let value = rawValue.objectName.toUpperCase();
        rawValue.objectName = value;
        if (this.editData.selecteLanguage != rawValue.selecteLanguage) {
          rawValue.descriptionChange = true;
        } else if (this.editData.description != rawValue.description) {
          rawValue.descriptionChange = true;
        } else {
          rawValue.descriptionChange = false;
        }
        if (rawValue.objectBackupFlag == false) {
          rawValue.objectBackupFlag = "N";
        } else {
          rawValue.objectBackupFlag = "Y";
        }

        if (rawValue.objHistoryRefreshFlag == false) {
          rawValue.objHistoryRefreshFlag = "N";
        } else {
          rawValue.objHistoryRefreshFlag = "Y";
        }
        if (rawValue.objectOptimizeFlag == false) {
          rawValue.objectOptimizeFlag = "N";
        } else {
          rawValue.objectOptimizeFlag = "Y";
        }
        if (rawValue.objectPurgeFlag == false) {
          rawValue.objectPurgeFlag = "N";
        } else {
          rawValue.objectPurgeFlag = "Y";
        }
        if (rawValue.objectSensitivity == false) {
          rawValue.objectSensitivity = "N";
        } else {
          rawValue.objectSensitivity = "Y";
        }
        this._apiService.post(apiUrl, rawValue).subscribe((resp) => {
          if (resp["status"] == 1) {
            this.globalService.showToastr.success(resp["message"]);
            this.modalService.dismissAll();
            let data = {
              type: "pagination",
            };
            this.tableAction(data);
            this.cancelBtnAll();
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
      }
    }
  }

  showRequiredFiled: boolean = false;
  columnTypeChange(event) {
    this.columnSetupForm.patchValue({
      ["dataLength"]: "",
    });
    if (event == "VARCHAR2" || event == "CHAR") {
      this.showRequiredFiled = true;
      this.columnSetupForm
        .get("dataLength")
        .setValidators([Validators.required]);
      this.columnSetupForm.controls["dataLength"].updateValueAndValidity();
    } else {
      this.showRequiredFiled = false;
      this.columnSetupForm.controls["dataLength"].clearValidators();
      this.columnSetupForm.controls["dataLength"].updateValueAndValidity();
    }
  }

  addColumnSetupEdit() {
    this.btnShowForColumns = true;

    let rawValue = this.columnSetupForm.getRawValue();
    let currentDateTime = this.datePipe.transform(
      new Date(),
      "MM-dd-yyyy h:mm:ss"
    );
    const found = this.columnlistDetail.find(
      (x) => x.columnId === rawValue.columnId
    );

    if (!isNullOrUndefined(found)) {
      this.columnlistDetail.forEach((element) => {
        if (element.columnId == rawValue.columnId) {
          if (this.editData.selecteLanguage != rawValue.selecteLanguage) {
            element.descriptionChange = true;
          } else if (this.editData.description != rawValue.description) {
            element.descriptionChange = true;
          } else {
            element.descriptionChange = false;
          }

          if (rawValue.auditEnableFlag == false) {
            element.auditEnableFlag = "N";
          } else {
            element.auditEnableFlag = "Y";
          }
          if (rawValue.colDataSensitivity == false) {
            element.colDataSensitivity = "N";
          } else {
            element.colDataSensitivity = "Y";
          }
          if (rawValue.displayCoumn == false) {
            element.displayCoumn = "N";
          } else {
            element.displayCoumn = "Y";
          }

          (element.columnName = rawValue.columnName.toUpperCase()),
            (element.columnOrder = rawValue.columnOrder),
            (element.newValueAdded = "Y"),
            (element.description = rawValue.description),
            (element.dataIndex = rawValue.dataIndex),
            (element.mandatoryFlag = rawValue.mandatoryFlag.toString()),
            (element.dateLastModified = currentDateTime),
            (element.guidelines = rawValue.guidelines),
            (element.dataLength = rawValue.dataLength),
            (element.dataType = rawValue.dataType),
            (element.auditEnableFlag = element.auditEnableFlag);
          (element.colDataSensitivity = element.colDataSensitivity),
            (element.displayCoumn = element.displayCoumn);
        }
      });

      this.modalClose();
    } else {
      if (this.columnSetupForm.invalid) {
        this.submitted_Column = true;
      } else {
        if (this.editData.selecteLanguage != rawValue.selecteLanguage) {
          rawValue.descriptionChange = true;
        } else if (this.editData.description != rawValue.description) {
          rawValue.descriptionChange = true;
        } else {
          rawValue.descriptionChange = false;
        }
        if (rawValue.auditEnableFlag == false) {
          rawValue.auditEnableFlag = "N";
        } else {
          rawValue.auditEnableFlag = "Y";
        }
        if (rawValue.colDataSensitivity == false) {
          rawValue.colDataSensitivity = "N";
        } else {
          rawValue.colDataSensitivity = "Y";
        }
        if (rawValue.displayCoumn == false) {
          rawValue.displayCoumn = "N";
        } else {
          rawValue.displayCoumn = "Y";
        }

        const data = {
          columnName: rawValue.columnName.toUpperCase(),
          columnOrder: rawValue.columnOrder,
          description: rawValue.description,
          dataIndex: rawValue.dataIndex,
          newValueAdded: "Y",
          mandatoryFlag: rawValue.mandatoryFlag.toString(),
          displayCoumn: rawValue.displayCoumn,
          colDataSensitivity: rawValue.colDataSensitivity,
          dateLastModified: currentDateTime,
          guidelines: rawValue.guidelines,
          dataLength: rawValue.dataLength,
          dataType: rawValue.dataType,
          auditEnableFlag: rawValue.auditEnableFlag,
        };

        this.columnlistDetail.push(data);
        this.columnlistDetail.forEach((element, i) => {
          element.columnId = i + 1;
        });

        this.modalClose();
      }
    }
  }

  modifyColumnSetup() {
    let apiUrl = environment.edwTable_modifyColumnData;
    let rawValue = this.columnSetupForm.getRawValue();
    if (this.columnSetupForm.invalid) {
      this.submitted_Column = true;
    } else {
      if (this.editData.selecteLanguage != rawValue.selecteLanguage) {
        rawValue.descriptionChange = true;
      } else if (this.editData.description != rawValue.description) {
        rawValue.descriptionChange = true;
      } else {
        rawValue.descriptionChange = false;
      }
      if (rawValue.auditEnableFlag == false) {
        rawValue.auditEnableFlag = "N";
      } else {
        rawValue.auditEnableFlag = "Y";
      }
      if (rawValue.colDataSensitivity == false) {
        rawValue.colDataSensitivity = "N";
      } else {
        rawValue.colDataSensitivity = "Y";
      }
      if (rawValue.displayCoumn == false) {
        rawValue.displayCoumn = "N";
      } else {
        rawValue.displayCoumn = "Y";
      }
      if (rawValue.columnId == "" || null) {
        rawValue.columnId = this.columnlistDetail.length + 1;
      }
      const data = {
        auditEnableFlag: rawValue.auditEnableFlag,
        colDataSensitivity: rawValue.colDataSensitivity,
        columnId: rawValue.columnId,
        columnName: rawValue.columnName.toUpperCase(),
        columnOrder: rawValue.columnOrder,
        connectionId: rawValue.connectionId || this.global_connectionId,
        dataIndex: rawValue.dataIndex,
        dataLength: rawValue.dataLength,
        dataScaling: rawValue.dataScaling,
        dataType: rawValue.dataType,
        guidelines: rawValue.guidelines,
        description: rawValue.description,
        descriptionChange: rawValue.descriptionChange,
        displayCoumn: rawValue.displayCoumn,
        mandatoryFlag: rawValue.mandatoryFlag,
        productName: rawValue.productName || this.global_productName,
        schema: rawValue.schema || this.global_schema,
        selecteLanguage:
          rawValue.selecteLanguage || this.global_selecteLanguage,
        tableName: rawValue.tableName || this.fromTableName,
      };

      this._apiService.post(apiUrl, data).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.columnlistDetail = resp["response"];
          if (this.columnlistDetail.length) {
            this.mYIdColumns = "chooseDatasourcecolumn";
          }
          this.globalService.showToastr.success(resp["message"]);
          this.modalService.dismissAll();
          this.modalClose();
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    }
  }

  saveindexSetup() {
    let apiUrl = environment.edwTable_modifyIndexData;
    if (this.indexSetupForm.invalid) {
      this.submitted_Relation = true;
    } else {
      let rawValue = this.indexSetupForm.getRawValue();
      //this.indexSetupForm.get("indexName").disable();
      let data = {
        tableName: this.sourceTable,
        indexName: rawValue.name,
        selecteLanguage: rawValue.selecteLanguage,
        indexType: rawValue.Type,
        description: rawValue.description,
        orderIndex: 1,
        productName: this.global_productName,
        connectionId: this.global_connectionId,
        schema: this.global_schema,
        descriptionChange: true,
      };

      this._apiService.post(apiUrl, data).subscribe((resp) => {
        if (resp["status"] == 1) {
          this.modalClose();
          this.indexlistDetail = resp["response"];
          this.resetModel = Object.assign({}, { key: "reload" });
          this.globalService.showToastr.success(resp["message"]);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
    }
  }

  openPopupRelations() {
    this.initRelationManagementForm();
    this.relationSetupForm.patchValue({
      sourceTable: this.editData.objectName,
    });
    this.mainArr = [];
    this.popupd3LinkConnectionData = [];
    this.modalService.open(this.connectionPopupRelation, {
      size: "xl",
      backdrop: "static",
      windowClass: "addLinkpopup",
    });
    this.loadMagnifierData();
  }
  relation_table: boolean = false;
  relation_table_sub: boolean = false;
  toggleShowDiv(value: string) {
    if (value == "relation") {
      this.isOpen_relation = !this.isOpen_relation;
      this.isOpen_relation_sub = false;
    }
    if (value == "relation_sub") {
      this.isOpen_relation_sub = !this.isOpen_relation_sub;
      this.isOpen_relation = false;
    }
  }

  checkUncheckAll() {
    for (var i = 0; i < this.relationByIndexList.length; i++) {
      this.relationByIndexList[i].isSelected = this.isMasterSel;
    }
    this.getCheckedItemList();
  }
  getCheckedItemList() {
    this.checkedCategoryList = [];
    for (var i = 0; i < this.relationByIndexList.length; i++) {
      if (this.relationByIndexList[i].isSelected)
        this.checkedCategoryList.push(this.relationByIndexList[i]);
    }
  }

  isAllSelected() {
    this.isMasterSel = this.relationByIndexList.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }
  checkUncheckAllByName() {
    for (var i = 0; i < this.relationByNameList.length; i++) {
      this.relationByNameList[i].isSelected = this.isMasterSel_byname;
    }
    this.getCheckedItemListByName();
  }
  isAllSelectedByName() {
    this.isMasterSel_byname = this.relationByNameList.every(function (
      item: any
    ) {
      return item.isSelected == true;
    });
    this.getCheckedItemListByName();
  }
  getCheckedItemListByName() {
    this.checkedCategoryListByName = [];
    for (var i = 0; i < this.relationByNameList.length; i++) {
      if (this.relationByNameList[i].isSelected)
        this.checkedCategoryListByName.push(this.relationByNameList[i]);
    }
  }
  addMissingRelationValues() {
    this.results = [];
    this.results.push(
      ...this.checkedCategoryList,
      ...this.checkedCategoryListByName
    );
    const api_Modify = environment.edwObjinsertRelationsData;
    this._apiService.post(api_Modify, this.results).subscribe((resp) => {
      if (resp["status"] == 1) {
        this.modalClose();
        this.globalService.showToastr.success(resp["message"]);
      } else {
        this.globalService.showToastr.error(resp["message"]);
      }
    });
  }

  openRelationpopup() {
    let data = {
      objectName: this.editData.objectName,
    };
    this._apiService
      .post(environment.getRelationSuggestions, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          this.relationByIndexList = resp["request"];
          this.relationByNameList = resp["response"];

          if (this.relationByIndexList.length) {
            this.relation_table = true;
          } else {
            this.relation_table = false;
          }
          if (this.relationByNameList.length) {
            this.relation_table_sub = true;
          } else {
            this.relation_table_sub = false;
          }

          this.modalService.open(this.missingRelationSetupTemplate, {
            size: "lg",
            backdrop: "static",
          });
        } else {
          this.globalService.showToastr.warning(resp["message"]);
        }
      });
  }
  currentPage: any;
  maxRecords: any;
  loadMagnifierData() {
    this.selectedTargetTable = "";
    this.query = "TABLSBYCAT";
    this.startIndex = 0;
    this.lastIndex = 1000;
    this.currentPage = 1;
    this.maxRecords = 1000;

    this.promptValue1 = this.promptValue1;
    this.promptValue2 = this.promptValue2;
    let data: any = {};

    if (typeof this.query == "object") {
      data = this.query;
      data["startIndex"] = this.startIndex;
      data["lastIndex"] = this.lastIndex;
      data["promptValue1"] = this.promptValue1;
    } else {
      data = {
        startIndex: this.startIndex,
        lastIndex: this.lastIndex,
        query: this.query,
        promptValue1: this.promptValue1,
        promptValue2: this.promptValue2,
        maxRecords: this.maxRecords,
        currentPage: this.currentPage,
      };
    }

    this._apiService
      .post(environment.get_Branch_List, data)
      .subscribe((resp) => {
        if (resp["status"]) {
          this.targetTableList = resp["response"];
        }
      });
  }

  openPopupIndexx() {
    this.initIndexManagementForm();
    this.modalService.open(this.indexPopup, {
      size: "lg",
      backdrop: "static",
      windowClass: "indexPopup",
    });
  }
  openPopupAddcolumn() {
    this.initColumnManagementForm();
    this.modalService.open(this.addColumnPopup, {
      size: "lg",
      backdrop: "static",
    });
  }
  modalClose() {
    this.modalService.dismissAll();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }

  //start popup connection --->d3
  d3DataFormSource() {
    let lengthA = this.source_Table.length;
    let lengthB = this.target_Table.length;
    let relationSvgHeight = lengthA > lengthB ? lengthA : lengthB;
    // svg height //
    setTimeout(() => {
      let mainDiv = document
        .getElementById("d3addlinkpopup")
        .getBoundingClientRect();
      this.popupsvg = d3
        .select("#d3addlinkpopup")
        .append("svg")
        .attr("width", mainDiv.width)
        // .attr("height", mainDiv.height);
        .attr("height", relationSvgHeight * 20 + 25);
      this.popupd3RelationSvg = this.popupsvg.append("g");
      d3.selection.prototype.moveUp = function () {
        return this.each(function () {
          this.parentNode.appendChild(this);
        });
      };

      this.d3ToolTipDiv = d3
        .select("body")
        .append("div")
        .attr("class", "d3_tooltip")
        .style("opacity", 0)
        .style("left", 0)
        .style("top", 0);

      this.d3PopupDiv = d3
        .select("#d3PopupDiv1")

        .style("opacity", 0)
        .style("width", "0px")
        .style("height", "0px")
        .style("left", "0px")
        .style("top", "0px");

      this.popupd3RelationSvg
        .append("defs")
        .append("marker")
        .attr("id", "dominating")
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 10)
        .attr("refY", 0)
        .attr("orient", "auto-start-reverse")
        .attr("markerWidth", 5)
        .attr("markerHeight", 10)
        .attr("class", "test2")
        .append("path")
        .attr("d", "M0,-5L10,0L0,5")
        .attr("fill", "#4285f4")
        .on("click", (d) => this.onClick(d));

      this.popupd3RelationSvg
        .append("defs")
        .append("marker")
        .attr("id", "dot")
        .attr("viewBox", [0, 0, 20, 20])
        .attr("refX", 10)
        .attr("refY", 10)
        .attr("markerWidth", 5)
        .attr("markerHeight", 5)
        .append("circle")
        .attr("cx", 10)
        .attr("cy", 10)
        .attr("r", 10)
        .style("fill", "#4285f4");

      this.popupd3ForeignObject = this.popupd3RelationSvg
        .append("foreignObject")
        .attr("x", 0)
        .attr("y", -5)
        .attr("class", "icon_block");

      this.popupd3ForeignObject
        .append("xhtml:div")
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("class", "connection_icon-top")
        .on("click", () => this.autoConnect())
        .append("i")
        .attr("class", "fa fa-link")
        .attr("aria-hidden", "true")
        .on("mouseover", () => {
          this.d3ToolTipDiv
            .html("Auto Connect")
            .style("opacity", 0.9)
            .style("left", d3.event.pageX - 44 + "px")
            .style("top", d3.event.pageY - 40 + "px");
        })
        .on("mouseout", () => {
          this.d3ToolTipDiv.style("opacity", 0);
        });
      //------------------888----------------
      this.popupd3ForeignObject = this.popupd3RelationSvg
        .append("foreignObject")
        .attr("x", 20)
        .attr("y", -5)
        .attr("class", "icon_block");

      this.popupd3ForeignObject
        .append("xhtml:div")
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("class", "connection_icon-top")
        .on("click", () => this.deleteLink())
        .append("i")
        .attr("class", "fa fa-trash-o")
        .attr("aria-hidden", "true")
        .on("mouseover", () => {
          this.d3ToolTipDiv
            .html("Delete All")
            .style("opacity", 0.9)
            .style("left", d3.event.pageX - 34 + "px")
            .style("top", d3.event.pageY - 40 + "px");
        })
        .on("mouseout", () => {
          this.d3ToolTipDiv.style("opacity", 0);
        });

      this.popupd3RelationSvg
        .append("text")
        .attr("class", "relationHeader")
        .attr("x", 80)
        .attr("y", 10)
        .attr("dx", 45)
        .text("Source Column(s):");

      this.popupd3RelationSvg
        .append("rect")
        .attr("x", 0)
        .attr("y", 20)
        .attr("width", 300)
        .attr("height", lengthA * 20)
        .attr("stroke", "#ccc")
        .attr("fill", "#fff");

      if (this.source_Table.length) {
        this.source_Table.forEach((element, i) => {
          element.lineX = 0;
          element.lineY = i * 20 + 40;
        });
      }

      this.popupd3RelationSvg
        .selectAll(`.textSource`)
        .data(this.source_Table)
        .enter()
        .append("text")
        .attr("id", (d) => {
          return `subnode${d.columnId}`;
        })
        .attr("x", (d) => {
          return d.lineX;
        })
        .attr("y", (d) => {
          return d.lineY - 5;
        })
        .attr("dx", 30)
        .text((d: any) => {
          return d.columnName;
        })
        .attr("class", "subnode");

      this.popupd3RelationSvg
        .selectAll(`.textSource`)
        .data(this.source_Table)
        .enter()
        .append("foreignObject")
        .attr("x", (d) => {
          return d.lineX + 270;
        })
        .attr("y", (d) => {
          return d.lineY - 22;
        })
        .attr("class", "icon_block")
        .append("xhtml:div")
        .append("a")
        .attr("href", "javascript:void(0);")
        .attr("class", "connection_icon")
        .on("click", (d) => this.popupDivCall(d))
        .append("i")
        .attr("class", "fa fa-link")
        .attr("aria-hidden", "true")
        .on("mouseover", () => {
          this.d3ToolTipDiv
            .html("Link")
            .style("opacity", 0.9)
            .style("left", d3.event.pageX - 17 + "px")
            .style("top", d3.event.pageY - 40 + "px");
        })
        .on("mouseout", () => {
          this.d3ToolTipDiv.style("opacity", 0);
        });

      this.popupd3RelationSvg
        .selectAll(`.bottomLine`)
        .data(this.source_Table)
        .enter()
        .append("line")
        .attr("class", (i) => {
          if (i != this.source_Table.length - 1) return "bottomLine";
          else return "";
        })
        .attr("x1", (d: any) => {
          return d.lineX;
        })
        .attr("y1", (d: any) => {
          return d.lineY;
        })
        .attr("x2", (d: any) => {
          return d.lineX + 300;
        })
        .attr("y2", (d: any) => {
          return d.lineY;
        });
    });
  }

  d3DataFormTarget(flag) {
    let lengthA = this.source_Table.length;
    let lengthB = this.target_Table.length;
    let relationSvgHeight = lengthA > lengthB ? lengthA : lengthB;

    setTimeout(() => {
      d3.select("#d3addlinkpopup")
        .select("svg")
        .attr("height", relationSvgHeight * 20 + 25);

      this.popupd3RelationSvg
        .append("text")
        .attr("class", "relationHeader")
        .attr("x", 500)
        .attr("y", 10)
        .attr("dx", 45)
        .text("Target Column(s):");

      this.popupd3RelationSvg
        .append("rect")
        .attr("x", 500)
        .attr("y", 20)
        .attr("width", 300)
        .attr("height", lengthB * 20)
        .attr("stroke", "#ccc")
        .attr("fill", "#fff");

      if (this.target_Table.length) {
        this.target_Table.forEach((element, i) => {
          element.lineX = 500;
          element.lineY = i * 20 + 40;
        });
      }

      this.popupd3RelationSvg
        .selectAll(`.textSource`)
        .data(this.target_Table)
        .enter()
        .append("text")
        .attr("x", (d) => {
          return d.lineX;
        })
        .attr("y", (d) => {
          return d.lineY - 5;
        })
        .attr("dx", 30)
        .text((d: any) => {
          return d.columnName;
        })
        .attr("class", "subnode");

      this.popupd3RelationSvg
        .selectAll(`.bottomLine1`)
        .data(this.target_Table)
        .enter()
        .append("line")
        .attr("class", (i) => {
          if (i != this.target_Table.length - 1) return "bottomLine1";
          else return "";
        })
        .attr("x1", (d: any) => {
          return d.lineX;
        })
        .attr("y1", (d: any) => {
          return d.lineY;
        })
        .attr("x2", (d: any) => {
          return d.lineX + 300;
        })
        .attr("y2", (d: any) => {
          return d.lineY;
        });

      flag ? this.d3ConnectionDraw() : "";
    });
  }

  targetTableChangeRelation(target) {
    this.targetPopup.forEach((element) => {
      if (element.columnId != target.columnId) {
        element.checked = false;
      }
    });
  }

  d3ConnectionDraw() {
    this.popupd3Links = [];
    this.popupd3LinkConnectionData.forEach((link) => {
      let source = this.source_Table.filter(
        (f) => link.tranColumnId === f.columnId
      );
      let target = this.target_Table.filter(
        (f) => link.targetColumnId === f.columnId
      );
      if (source.length && target.length) {
        this.popupd3Links.push({
          source: source[0],
          target: target[0],
          connectionName: `${source[0].columnName} -
        ${target[0].columnName}`,
          connectionsourcename: source[0].columnName,
          connectiontargetname: target[0].columnName,
        });
      }
    });

    //-----------D3Links------------------
    this.popupd3RelationSvg
      .selectAll(`.link`)
      .data(this.popupd3Links)
      .enter()
      .append("line")
      .attr("class", "link")
      .attr("id", "move-up")
      .attr("marker-end", "url(#dominating)")
      .attr("marker-start", "url(#dot)")
      .attr("x1", (d: any) => {
        return d.source.lineX + 300;
      })
      .attr("y1", (d: any) => {
        return d.source.lineY - 10;
      })
      .attr("x2", (d: any) => {
        return d.target.lineX;
      })
      .attr("y2", (d: any) => {
        return d.target.lineY - 10;
      })

      .on("mouseover", (d) => {
        this.d3ToolTipDiv
          .html(d.connectionName)
          .style("opacity", 0.9)
          .style("left", d3.event.pageX - 34 + "px")
          .style("top", d3.event.pageY - 40 + "px");
      })
      .on("mouseout", () => {
        this.d3ToolTipDiv.style("opacity", 0);
      })
      .on("click", (d) => this.onClick(d));

    this.popupd3RelationSvg.moveUp();
  }

  onClick(details) {
    const indexOfObject = this.popupd3LinkConnectionData.findIndex((object) => {
      return (
        object.tranColumnId == details.source["tranColumnId"] &&
        object.targetColumnId == details.target["targetColumnId"]
      );
    });

    this.popupd3LinkConnectionData.splice(indexOfObject, 1);

    this.popupd3Links = [];
    this.popupsvg.remove();
    this.d3DataFormSource();
    this.d3DataFormTarget(true);
  }

  autoConnect() {
    this.showConnector = !this.showConnector;
    this.targetFlag = false;
  }

  deleteLink() {
    const modelRef = this.modalService.open(GenericPopupComponent, {
      size: <any>"md",
      backdrop: false,
      windowClass: "genericPopup",
    });
    modelRef.componentInstance.title = "Alert";
    modelRef.componentInstance.message = "Do you want to delete all links ?";
    modelRef.componentInstance.popupType = "Confirm";
    modelRef.componentInstance.closeTime = "No";
    modelRef.componentInstance.userConfirmation.subscribe((resp) => {
      if (resp["type"] == "Yes") {
        this.clearLink();
        this.popupsvg ? this.popupsvg.remove() : "";
        this.d3DataFormSource();
        this.d3DataFormTarget(true);
      } else {
        modelRef.close();
      }
      modelRef.close();
    });
  }

  d3Connect(data) {
    this.popupd3LinkConnectionData = [];

    this.source_Table.forEach((source) => {
      source["columnId"] = source["columnId"];
      if (!this.popupconnectedSources.includes(source.columnId)) {
        this.target_Table.forEach((target) => {
          if (data == "Connect by name") {
            if (source.columnName == target.columnName) {
              if (!this.popupconnectedTargets.includes(target.columnId))
                this.popupd3LinkConnectionData.push({
                  tranColumnId: source["columnId"],
                  targetColumnId: target["columnId"],
                  checked: true,
                });
            }
          } else if (data == "Connect by position") {
            if (!this.popupconnectedSources.includes(source.columnId)) {
              if (source.columnId == target.columnId) {
                if (!this.popupconnectedTargets.includes(target.columnId))
                  this.popupd3LinkConnectionData.push({
                    tranColumnId: source["columnId"],
                    targetColumnId: target["columnId"],
                    checked: true,
                  });
              }
            }
          }
        });
      }
    });

    // Restore the original connections
    this.popupd3LinkConnectionData = [
      ...this.popupd3LinkConnectionData,
      ...this.popupconnectedSources.map((sColumnId, i) => ({
        tranColumnId: sColumnId,
        targetColumnId: this.popupconnectedTargets[i].toString(),
      })),
    ];
    this.currentTran = data;
    this.popupd3Links = [];
    this.popupsvg.remove();
    this.d3DataFormSource();
    this.d3DataFormTarget(true);
  }

  popupDivCall(item) {
    this.targetPopup.forEach((ele) => (ele["showColumn"] = true));
    this.targetFlag = true;
    this.closeD3Connector();
    this.currentTran = item;
    let tempArr = this.popupd3LinkConnectionData
      .filter((e) => e.tranColumnId == this.currentTran.tranColumnId)
      .map((e) => e.tranColumnId);

    this.targetPopup.forEach((column) => {
      if (tempArr.includes(column.columnId)) {
        column["checked"] = true;
      } else {
        column["checked"] = false;
      }
      let checkConnection = this.popupd3LinkConnectionData.map(
        (e) => e.columnId
      );
      if (checkConnection.includes(column.columnId) && !column.checked)
        column["showColumn"] = false;
    });

    // Sorting
    this.targetPopup.sort((f) => {
      if (f.checked) return -1;
      else return 1;
    });
  }

  closePopupD3() {
    this.targetFlag = false;
  }

  closeD3Connector() {
    this.showConnector = false;
  }

  searchTargetLst(name) {
    this.targetPopup.forEach((target) => {
      if (this.popupconnectedTargets == target["targetColumnId"])
        target.showColumn = false;
      else target.showColumn = true;
    });
    this.targetPopup.forEach((target) => {
      let element = target.columnName.toLowerCase();
      if (!element.includes(name.toLowerCase()) && target.showColumn)
        target.showColumn = false;
    });
  }

  clearLink() {
    this.arrayTemporary = [];
    this.popupd3RelationSvg = [];
    this.popupd3LinkConnectionData = [];
    this.popupconnectedSources = [];
    this.popupconnectedTargets = [];
    this.popupd3Links = [];
    this.popupsvg ? this.popupsvg.remove() : "";
  }

  targetTableChange(event) {
    this.clearLink();
    this.targettablename = event.columnNine;
    this.showSpinner = true;
    let data = {
      objectName: event.columnNine,
    };
    this.loadPopConnectionvalues(data);
  }

  loadPopConnectionvalues(data) {
    this._apiService
      .post(environment.edwObj_getColumnByTable, data)
      .subscribe((resp) => {
        if (resp["status"] == 1) {
          setTimeout(() => {
            this.showSpinner = false;
            this.source_Table = this.columnlistDetail;
            this.target_Table = resp["response"];
            this.targetPopup = JSON.parse(JSON.stringify(this.target_Table));

            if (this.mainArr.length != 0) {
              this.popupd3LinkConnectionData = [];
              this.source_Table.forEach((element) => {
                this.mainArr.forEach((element1) => {
                  if (
                    element.columnName.replace(/\s/g, "") ==
                    element1.source.replace(/\s/g, "")
                  ) {
                    element.tranColumnId = element.columnId;
                    element1.tranColumnId = element.columnId;
                  }
                });
              });

              this.target_Table.forEach((checkingelem) => {
                this.mainArr.forEach((element2) => {
                  if (
                    element2.target.replace(/\s/g, "") ==
                    checkingelem.columnName.replace(/\s/g, "")
                  ) {
                    checkingelem.targetColumnId = checkingelem.columnId;
                    element2.targetColumnId = checkingelem.columnId;
                  }
                });
              });

              this.popupd3LinkConnectionData = this.mainArr;
            }

            this.d3DataFormSource();
            this.d3DataFormTarget(true);
          }, 1000);
        } else {
          this.globalService.showToastr.error(resp["message"]);
        }
      });
  }

  checkRecords() {
    return this.targetPopup.every((e) => !e.showColumn);
  }

  saveConnection() {
    this.popupd3LinkConnectionData = [];
    this.popupd3LinkConnectionData = this.popupd3LinkConnectionData.filter(
      (item) => item.columnId != this.currentTran.columnId
    );
    this.targetPopup.forEach((column) => {
      if (column["checked"]) {
        this.arrayTemporary.push({
          tranColumnId: this.currentTran.columnId,
          targetColumnId: column["columnId"],
        });
      }
    });

    this.popupd3LinkConnectionData = this.popupd3LinkConnectionData.concat(
      this.arrayTemporary
    );
    this.popupconnectedSources = this.popupd3LinkConnectionData.map(
      (i) => i.tranColumnId
    );
    this.popupconnectedTargets = this.popupd3LinkConnectionData.map(
      (i) => i.targetColumnId
    );
    this.popupsvg.remove();
    this.d3DataFormSource();
    this.d3DataFormTarget(true);
    this.closePopupD3();
  }

  saveallNewConnection() {
    this.realtionString = [];
    if (this.relationSetupForm.invalid) {
      this.submitted_Relation = true;
    } else {
      if (this.arrayTemporary.length === 0) {
        this.temparray.push(this.popupd3LinkConnectionData);
        this.arrayTemporary = this.temparray[0];
      }

      this.arrayTemporary.forEach((element) => {
        this.targetPopup.forEach((element2) => {
          if (element.targetColumnId == element2.columnId) {
            element2.checked = true;
          }
        });
      });
      this.popupd3Links.forEach((column) => {
        this.realtionString.push({
          tranColumnId:
            this.sourceTable +
            "." +
            column.connectionsourcename +
            "=" +
            this.targettablename +
            "." +
            column.connectiontargetname,
        });
      });
      this.values = this.realtionString
        .map((item) => {
          return item.tranColumnId;
        })
        .join(" AND ");
      if (this.arrayTemporary.length > 0) {
        let rawValue = this.relationSetupForm.getRawValue();
        let displayflag;
        if (rawValue.relationDispalyFlag == true) {
          displayflag = "Y";
        } else {
          displayflag = "N";
        }

        if (this.editData.selecteLanguage != rawValue.selecteLanguage) {
          rawValue.descriptionChange = true;
        } else if (this.editData.description != rawValue.description) {
          rawValue.descriptionChange = true;
        } else {
          rawValue.descriptionChange = false;
        }

        let obj = {
          selecteLanguage: rawValue.selecteLanguage,
          relationName: rawValue.relationName,
          tableName: rawValue.sourceTable,
          relationTablename: this.targettablename,
          relationDispalyFlag: displayflag,
          description: rawValue.description,
          descriptionChange: rawValue.descriptionChange,
          relationString: this.values,
          productName: this.global_productName,
          connectionId: this.global_connectionId,
          schema: this.global_schema,
          relationType: "OTO",
        };

        const api_Modify = environment.edwTable_modifyRelationsData;
        this._apiService.post(api_Modify, obj).subscribe((resp) => {
          if (resp["status"] == 1) {
            this.relationlistDetail = resp["response"];
            if (this.relationlistDetail.length) {
              this.myId = "chooseDatasourceSec2";
            }
            this.globalService.showToastr.success(resp["message"]);

            this.modalClose();
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
      }
    }
  }

  //End popup connection --->d3

  emitSelectedData(data) {
    this.filterData.emit(data);
  }

  backGroundFlag(recordIndicator) {
    if (!recordIndicator) {
      return {
        background: "#448847",
      };
    } else {
      return {
        background: "#ffa828",
      };
    }
  }

  changeFn() {
    let rawValue = this.columnSetupForm.getRawValue();
    const found = this.columnlistDetail.find(
      (x) => x.columnName === rawValue.columnName
    );
    if (!isNullOrUndefined(found)) {
      this.globalService.showToastr.warning("Column Name Already Exits");
      this.columnSetupForm.patchValue({
        columnName: "",
      });
    }
  }

  disableButton(name) {
    if (name == "first") {
      return !this.editIndex ? true : false;
    }
    if (name == "last") {
      return this.lineList.length == 1
        ? true
        : this.editIndex == this.lineList.length - 1
        ? true
        : false;
    }
  }

  navigateFn(type) {
    if (type == "next") {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex + 1],
        index: this.editIndex + 1,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
    if (type == "previous") {
      let data = {
        type: this.pageView,
        data: this.lineList[this.editIndex - 1],
        index: this.editIndex - 1,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
    if (type == "last") {
      let data = {
        type: this.pageView,
        data: this.lineList[this.lineList.length - 1],
        index: this.lineList.length - 1,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
    if (type == "first") {
      let data = {
        type: this.pageView,
        data: this.lineList[0],
        index: 0,
        lineList: this.lineList,
      };
      this.tableAction(data);
    }
  }

  onSubmit(type) {
    if (
      type == "next" ||
      type == "previous" ||
      type == "last" ||
      type == "first"
    ) {
      this.navigateFn(type);
    }
  }
}
