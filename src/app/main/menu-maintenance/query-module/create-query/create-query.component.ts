import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core'; import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isUndefined, isArray, isNullOrUndefined } from 'util';
// SERVICE
import { ApiService } from 'src/app/service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
// COMPONENT
import { ITreeOptions } from '@circlon/angular-tree-component';
import { ShowQueryComponent } from '../popup-components/show-query/show-query.component';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { GenericQueryPopupComponent } from '../popup-components/generic-query-popup-component/generic-query-popup-component.component';
import { creteria_con } from '../modals/creteria';
import { MagnifierPopupComponent } from '../popup-components/magnifier-popup/magnifier-popup.component';
import { TreeComponent } from '@circlon/angular-tree-component';

import { LoadDataComponent } from '../popup-components/load-data/load-data.component';
import { SaveQueryService } from '../query-service/save-query.service';
import { CommonService } from 'src/app/service/common.service';
import { Subscription } from 'rxjs';
import { CommonFilterPaginationService } from 'src/app/service/common-filter-pagination.service';
import { element } from 'protractor';



@Component({
    selector: 'app-create-query',
    templateUrl: './create-query.component.html',
    styleUrls: ['./create-query.component.css'],

})
export class CreateQueryComponent implements OnInit, OnDestroy, AfterViewInit {



    //------------------------------------------------------------
    // PUBLIC VARIABLES AND DECLARATIONS
    //------------------------------------------------------------
    viewMode:any ='FILE'
    draggedTree: any = [];
    formatDownload:any={
        excel:false,
        csv:false,
        pdf:false,
    }
    overAll_check: any = false;
    magnifierPopup: boolean = false;
    showDownloadDropdown: boolean = false;
    updatePopupModifier: any = {};
    distinctFlag:boolean=true;
    // popupDetail: any = [];
    // dropDownDet: any = {};
    shallow_copy: any = {};
    deep_copy: any = {};
    noReturnPredicate: any = false;
    creteria: any = [];
    visibleTree = true;
    dummyTree = [];
    hashArray = [];
    hashValueArray = [];
    reportDescription: any = '';
    reportId: any = '';
    cpy_reportDescription: any = '';
    cpy_reportId: any = '';
    saveQueryArray: any = {};
    subscription: Subscription;
    treenode: any = [];
    treeView: boolean = false;
    scaling: boolean = false;

    selectedCatalogId: any;
    selectedCatalogName: any;

    public scaling_type = [
        { id: "1000", text: "1,000" },
        { id: "1000000", text: "1,000,000" },
        { id: "1000000000", text: "1,000,000,000" }
    ]
    catlogDetails: any = {};
    @ViewChild('overallDisplaycheck') overallDisplaycheck: ElementRef;
    @ViewChild('overallGroupBycheck') overallGroupBycheck: ElementRef;
    @ViewChild('addTemplate') addTemplate: ElementRef;
    @ViewChild('downloadZipLink') private downloadZipLink: ElementRef;
    @ViewChild(TreeComponent)
    public tree: TreeComponent;
    scheduleTypeList = [];
    config;
    eventNameList;
    //------------------------------------------------------------
    // 
    //------------------------------------------------------------
    public options1: ITreeOptions = {
        allowDrag: true,
        allowDrop: false,
        useCheckbox: false,
        useVirtualScroll: false,
        // getChildren: this.getChildren.bind(this),
    };
    copyFlag: boolean = true;
    leftContainerHeight: string;
    reportOpen: boolean = false;

    //------------------------------------------------------------
    //  CONSTRUCTOR
    //------------------------------------------------------------
    constructor(
        private service: ApiService,
        private modalService: NgbModal,
        private routes: ActivatedRoute,
        private commonService: CommonService,
        private filterService: CommonFilterPaginationService,
        private saveService: SaveQueryService,

    ) {
        this.routes.queryParams.subscribe(params => {
            let data = JSON.parse(params['data'])
            this.selectedCatalogName = data['name'];
            this.selectedCatalogId = data['id'];
            this.catlogDetails['catalogId'] = data['id'];
            this.catlogDetails['joinClause'] = data['joinClause'];
        });

        this.service.post(this.service.getEnvironment().get_data_design_analysis_par, this.catlogDetails).subscribe(resp => {
            this.getTableList(resp['response']);

        });
        //  this.filterService.limited_data.subscribe(resp=>{
        // })
        this.subscription = this.commonService.reportDetails.subscribe(resp => {
            if ('retrive' in resp) {
                this.reportDescription = resp['reportDescription'] || '';
                this.distinctFlag= resp['distinctFlag'];
                this.reportId = resp['reportId'] || '';
                this.draggedTree = resp['reportFields']['originalList'] || [];
                this.dummyTree = resp['reportFields']['dummyList'] || [];
                this.hashValueArray = resp['hashValueArray'] || [];
                this.hashArray = resp['hashArray'] || [];
                this.joinCondition();
                this.initializeCondition();
                // this.groupByConditionCheck();
            }
            else if ('inlineSave' in resp) {
                this.reportDescription = resp['reportDescription'] || '';
                this.hashValueArray = resp['hashValueArray'] || [];
                this.hashArray = resp['hashArray'] || [];
                this.reportId = resp['reportId'] || '';
            }
            else {
                this.hashValueArray = resp['hashValueArray'] || [];
                this.hashArray = resp['hashArray'] || [];
            }
        });
    }

    //------------------------------------------------------------
    //  INITIALIZATION
    //------------------------------------------------------------
    ngOnInit() {
        this.creteria = creteria_con;
    }

    //------------------------------------------------------------
    //  INITIALIZATION
    //------------------------------------------------------------
    ngAfterViewInit() {
        this.overallDisplaycheck.nativeElement.checked = this.draggedTree.length ? this.draggedTree.every(function (num) { return num.display; }) : false;
        //this.overAllGroupCheckBox();
        if (this.draggedTree.length) {
            if (this.draggedTree.some(element_1 => !this.creteriaLevelCondition(element_1.aggrigate))) {
                this.overallGroupBycheck.nativeElement.disabled = true;
            }
        }
        setTimeout(() => {
            const getContentAreaHeight = window.innerHeight;
            const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
            this.leftContainerHeight = getContentAreaHeight + 'px';
            document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
            // document.getElementById('card-body').children[0]['style'].height = '100%';
        }, 500);
    }

    //------------------------------------------------------------
    //  INITIALIZATION
    //------------------------------------------------------------
    getTableList(data: any) {
        if (data != null && Array.isArray(data)) {
            data.forEach((element, index) => {
                element['name'] = element['aliasName'];
                element['index'] = index + 1;
                element['hasChildren'] = true;
                this.getChildren(element).then(resp => {
                    element['children'] = resp;
                })
            });
            this.treenode = data;
            this.treeView = true;
        }
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    public getChildren(node: any) {
        var data = {
            "catalogId": this.catlogDetails['catalogId'],
            "tableId": node.tableId
        }
        return new Promise((resolve, reject) => {

            this.service.post(this.service.getEnvironment().get_data_design_analysis_child, data).toPromise().then(
                resp => {

                    resolve(this.getData(resp['response']));
                }
            );
        });
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    getData(data: any) {
        data = data == null ? [] : data;
        if(Array.isArray(data)){
            data = data.filter(element => {
                if ((element.colType == 'T') || (element.colType == 'D' && (element.experssionText != null && element.experssionText != ""))) {
                    element['name'] = element['aliasName'];
                    return true;
                }
                else {
                    return false;
                }
            });
        }
        return data;
    }

    initializeCondition() {
        this.draggedTree.forEach((element, index, array) => {
            this.draggedTree[index].whereCondition = element.colType == 'D' && (element.colExperssionType == "DBSQL" || element.colExperssionType == "EXP") ? false : true
        });
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    updateERDiagram(event: any) {
        
        var data = event.element.data;
        if (data['children'] != null) {
            data.children.forEach((element, index, array) => {
                let dummy_obj = {};
                dummy_obj = {
                    fields: element.colName,
                    tableName: data.tableName,
                    tableAliasName: data.aliasName,
                    tableId: data.tableId,
                    colExperssionType: element.colExperssionType,
                    colId: element.colId,
                    alias: element.aliasName,
                    whereCondition: element.colType == 'D' && (element.colExperssionType == "DBSQL" || element.colExperssionType == "EXP") ? false : true,
                    magType: element.magType,
                    magSelectionType: element.magSelectionType,
                    includeGroupCol: element.includeGroupCol,
                    dateConfig: (element.colDisplayType == 'D') ? this.configDate(element.creteria) : '',
                    magQueryId: element.magQueryId,
                    maskingFlag: element.maskingFlag,
                    magEnableFlag: element.magEnableFlag,
                    displayColumn: element.magDisplayColumn,
                    useColumn: element.magUseColumn,
                    display: true,
                    join: false,
                    uiqueIdentifier: data.tableId + '_' + element.colId,
                    multiple: false,
                    colType: element.colType,
                    experssionText: element.experssionText,
                    joinData: 'And',
                    sortType: '',
                    promptValueCheck: false,
                    creteria: '',
                    aggrigate:'',
                    summeryFilter:'',
                    startData: '',
                    endData: '',
                    endDataDisTog: 'plain',
                    startDataDisTog: 'plain',
                    groupByDisable: (element.colType == 'D' && element.colExperssionType == "DBSQL") ? true : false,
                    startDataTog: false,
                    endDataTog: false,
                    summeryStartData:'',
                    summeryEndData:'',
                    summeryEndDataDisTog: 'plain',
                    summeryStartDataDisTog: 'plain',
                    summeryStartDataTog: false,
                    summeryEndDataTog: false,
                    group: false,
                    type_txt: element.colDisplayType == 'N' || element.colDisplayType == 'C' ? 'number' : 'text',
                    magnifierValues: [],
                    formatType: element.formatTypeDesc,
                    dateConversionSyntax: element.dateConversionSyntax,
                    dateFormattingSyntax: element.dateFormattingSyntax,
                    creteriaDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
                    aggrigateDropDown: (element.colDisplayType == 'D') ? creteria_con['forDateAgg'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCNAgg'] : creteria_con['notforCNAgg'],
                    summeryDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
                    colDisplayType: element.colDisplayType,
                    action: 'delete',
                    scalingFlag: true,
                    scalingFormat: "1000",
                    numberFormat: true,
                    decimalFlag: true,
                    decimalCount: 2,
                    javaFormatDesc: element.javaFormatDesc,
                }
                this.uniqueTree(dummy_obj);

            });
        } else if (data['children'] == null) {
            if (!isUndefined(data.colName)) {
                let dummy_obj = {}
                dummy_obj = {
                    fields: data.colName,
                    tableName: this.gettableName('tableName', data.tableId),
                    tableAliasName: this.gettableName('aliasName', data.tableId),
                    alias: data.aliasName,
                    whereCondition: data.colType == 'D' && (data.colExperssionType == "DBSQL" || data.colExperssionType == "EXP") ? false : true,
                    display: true,
                    tableId: data.tableId,
                    colId: data.colId,
                    uiqueIdentifier: data.tableId + '_' + data.colId,
                    sortType: '',
                    joinData: 'And',
                    type_txt: data.colDisplayType == 'N' || data.colDisplayType == 'C' ? 'number' : 'text',
                    multiple: false,
                    groupByDisable: (data.colType == 'D' && data.colExperssionType == "DBSQL") ? true : false,
                    promptValueCheck: false,
                    creteria: '',
                    aggrigate:'',
                    summeryFilter:'',
                    dateConfig: (data.colDisplayType == 'D') ? this.configDate(data.creteria) : '',
                    startData: '',
                    endData: '',
                    colType: data.colType,
                    colExperssionType: data.colExperssionType,
                    experssionText: data.experssionText,
                    magType: data.magType,
                    magQueryId: data.magQueryId,
                    magSelectionType: data.magSelectionType,
                    includeGroupCol: data.includeGroupCol,
                    maskingFlag: data.maskingFlag,
                    magEnableFlag: data.magEnableFlag,
                    displayColumn: data.magDisplayColumn,
                    useColumn: data.magUseColumn,
                    creteriaDropDown: (data.colDisplayType == 'D') ? creteria_con['forDate'] : (data.colDisplayType == 'C' || data.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
                    aggrigateDropDown: (data.colDisplayType == 'D') ? creteria_con['forDateAgg'] : (data.colDisplayType == 'C' || data.colDisplayType == 'N') ? creteria_con['forCNAgg'] : creteria_con['notforCNAgg'],
                    summeryDropDown: (data.colDisplayType == 'D') ? creteria_con['forDate'] : (data.colDisplayType == 'C' || data.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
                    formatType: data.formatTypeDesc,
                    dateConversionSyntax: data.dateConversionSyntax,
                    dateFormattingSyntax: data.dateFormattingSyntax,
                    colDisplayType: data.colDisplayType,
                    endDataDisTog: 'plain',
                    startDataDisTog: 'plain',
                    group: false,
                    magnifierValues: [],
                    join: false,
                    startDataTog: false,
                    endDataTog: false,
                    summeryStartData:'',
                    summeryEndData:'',
                    summeryEndDataDisTog: 'plain',
                    summeryStartDataDisTog: 'plain',
                    summeryStartDataTog: false,
                    summeryEndDataTog: false,
                    action: 'delete',
                    scalingFlag: true,
                    scalingFormat: "1000",
                    numberFormat: true,
                    decimalFlag: true,
                    decimalCount: 2,
                    javaFormatDesc: data.javaFormatDesc
                }
                this.uniqueTree(dummy_obj);
            }
        }
        if (data.hasChildren && data['children'] == null) {
            var datas = {
                "catalogId": this.catlogDetails['catalogId'],
                "tableId": data.tableId
            }

            this.service.post(this.service.getEnvironment().get_data_design_analysis_child, datas).subscribe(
                resp => {
                    this.overallDisplaycheck.nativeElement.checked = resp['response'].length ? true : false;
                    this.getData(resp['response']).forEach((element, index, array) => {

                        let dummy_obj = {};
                        dummy_obj = {
                            fields: element.colName,
                            tableName: data.tableName,
                            tableAliasName: data.aliasName,
                            tableId: data.tableId,
                            alias: element.aliasName,
                            colId: element.colId,
                            whereCondition: element.colType == 'D' && (element.colExperssionType == "DBSQL" || element.colExperssionType == "EXP") ? false : true,
                            display: true,
                            sortType: '',
                            groupByDisable: (element.colType == 'D' && element.colExperssionType == "DBSQL") ? true : false,
                            multiple: false,
                            joinData: 'And',
                            type_txt: element.colDisplayType == 'N' || element.colDisplayType == 'C' ? 'number' : 'text',
                            promptValueCheck: false,
                            creteria: '',
                            aggrigate:'',
                            summeryFilter:'',
                            dateConfig: (element.colDisplayType == 'D') ? this.configDate(element.creteria) : '',
                            startData: '',
                            endData: '',
                            summeryStartData:'',
                            summeryEndData:'',
                            summeryEndDataDisTog: 'plain',
                            summeryStartDataDisTog: 'plain',
                            summeryStartDataTog: false,
                            summeryEndDataTog: false,
                            colType: element.colType,
                            experssionText: element.experssionText,
                            magType: element.magType,
                            colExperssionType: element.colExperssionType,
                            magSelectionType: element.magSelectionType,
                            includeGroupCol: element.includeGroupCol,
                            magQueryId: element.magQueryId,
                            maskingFlag: element.maskingFlag,
                            magEnableFlag: element.magEnableFlag,
                            displayColumn: element.magDisplayColumn,
                            useColumn: element.magUseColumn,
                            startDataTog: false,
                            endDataTog: false,
                            formatType: element.formatTypeDesc,
                            dateConversionSyntax: element.dateConversionSyntax,
                            dateFormattingSyntax: element.dateFormattingSyntax,
                            creteriaDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
                            aggrigateDropDown: (element.colDisplayType == 'D') ? creteria_con['forDateAgg'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCNAgg'] : creteria_con['notforCNAgg'],
                            summeryDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
                            colDisplayType: element.colDisplayType,
                            group: false,
                            join: false,
                            magnifierValues: {},
                            uiqueIdentifier: data.tableId + '_' + element.colId,
                            endDataDisTog: 'plain',
                            startDataDisTog: 'plain',
                            action: 'delete',
                            scalingFlag: true,
                            scalingFormat: "1000",
                            numberFormat: true,
                            decimalFlag: true,
                            decimalCount: 2,
                            javaFormatDesc: element.javaFormatDesc
                        }
                        this.uniqueTree(dummy_obj)
                    });

                }
            );

        }
        if (this.draggedTree.length)
            this.overallDisplaycheck.nativeElement.checked = this.overAlldisplayCheck();

    }

    overAlldisplayCheck() {
        let bol: any = false;
        bol = this.draggedTree.every(element_1 => element_1.display) ? true : false;
        return bol;
    }

    overAllGroupCheckBox() {
        if (this.draggedTree.some(element_1 => element_1.group)) {
            this.overallGroupBycheck.nativeElement.checked = true;
        }
    }

    displayBasedGroupCheck(index, tree, overAllGroupCheck) {
        if (tree[index].display == false) {
            tree[index].group = false;
        }
        else {
            this.groupByConditionCheck();
            this.displayLevelGroupBy();
        }
    }

    displayLevelGroupBy() {
        this.draggedTree.forEach((element, index, array) => {
            if (!element.display) {
                array[index].group = false;
            }
        });
    }



    aggregateFunctionCount() {
        let creteria_count = 0;
        this.draggedTree.forEach((element, index, array) => {
            if (!this.creteriaLevelCondition(element.aggrigate)) creteria_count++;
        })
        return creteria_count;
    }
    //------------------------------------------------------------
    //   Group By Condition
    //------------------------------------------------------------
    groupByConditionCheck() {
        let creteria_count = 0;
        let group_count = 0;
        this.draggedTree.forEach((element, index, array) => {
            if (!this.creteriaLevelCondition(element.aggrigate)) creteria_count++;
            if (!this.creteriaLevelCondition(element.aggrigate)) group_count++;
            if (element.colType == 'D' && element.colExperssionType == "DBSQL") {
                array[index].groupByDisable = true;
                array[index].group = false;
            }
        });
        if (creteria_count > 0) {
            this.draggedTree.forEach((element, index, array) => {
                array[index].groupByDisable = true;
                if (this.creteriaLevelCondition(element.aggrigate)) {
                    array[index].group = true;
                    array[index].group = (array[index].display) ? array[index].group : false;
                    if (element.colType == 'D' && element.colExperssionType == "DBSQL") {
                        array[index].group = true;
                    }
                }
                else {
                    array[index].group = false;
                }
            })
        }
        else {
            this.draggedTree.forEach((element, index, array) => {
                if (element.colType == 'D' && element.colExperssionType == "DBSQL") {
                    array[index].groupByDisable = true;
                    array[index].group = true;
                }
                else {
                    array[index].groupByDisable = false;
                }
            })
        }

        if (!isUndefined(this.overallGroupBycheck)) {
            //   this.overallGroupBycheck.nativeElement.checked = (group_count > 0) ? true : false;
            this.overallGroupBycheck.nativeElement.disabled = (creteria_count == 0) ? false : true;
        }
        if (group_count <= 0) {
            this.draggedTree.forEach((element, index, array) => {
                array[index].group = false;
            })
        }

        if (this.overallGroupBycheck.nativeElement.checked) {
            this.draggedTree.forEach((element, index, array) => {
                if (element.colType == 'D' && element.colExperssionType == "DBSQL") {
                }
                else {
                    if (element.display && this.creteriaLevelCondition(element.aggrigate)) {
                        array[index].group = true;
                    }
                }
            })
        }
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    uniqueTree(dummy_obj) {
        this.dummyTree.push(dummy_obj);
        if (!this.draggedTree.some(element_1 => element_1.alias == dummy_obj.alias)) {
            let uniquId = Object.assign({}, dummy_obj);

            dummy_obj.uniqueId = uniquId.alias + '_' + 0;
            dummy_obj.aliasBackup = dummy_obj.alias;
            this.draggedTree.push(dummy_obj);
        }
        else {
            this.arrayCount(Object.assign({}, dummy_obj))
        }
    }

    arrayCount(obj: any) {
        let count = 0;
        this.dummyTree.forEach(element => {
            if (element.alias == obj.alias) {
                count++;
            }
        })
        count -= 1;
        obj.alias = obj.alias + '_' + count;
        obj.uniqueId = obj.alias + '~' + count;
        obj.aliasBackup = obj.alias;
        this.uniqueTree(obj)
    }

    //------------------------------------------------------------
    //  Hover Details  
    //------------------------------------------------------------
    checkingForAliasName(event, index) {
        let alias_name = event.target.value;
        alias_name = alias_name.replace(/[^a-zA-Z0-9_]/g, "");
        this.draggedTree[index]['alias'] = alias_name;
        this.draggedTree.forEach((x, i, a) => {
            if (x.alias == alias_name && i != index) {
                const modelRef = this.modalService.open(GenericQueryPopupComponent, {
                    size: 'sm',
                    backdrop: 'static'
                });
                modelRef.componentInstance.title = 'confirmation';
                modelRef.componentInstance.message = 'Alias Name Already Exist';
                modelRef.componentInstance.popupType = 'alert';
                modelRef.componentInstance.userConfirmation.subscribe((e) => {
                    if (e == 'Yes') {
                        this.draggedTree[index]['alias'] = this.draggedTree[index]['aliasBackup'];
                        modelRef.close();
                    } else {

                        modelRef.close();
                    }
                });
            }
        });
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    gettableName(type: any, data: any) {
        var str = '';
        this.treenode.forEach(element => {

            if (data.toUpperCase() == element.tableId) {

                str = (type == 'tableName') ? element.tableName : element.aliasName;
            }
        });
        return str;
    }


    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    creteriaBasedJoin(i) {
        this.draggedTree.forEach((element, index, array) => {
            if (!this.creteriaLevelCondition(element['aggrigate']) && element['group']) {
                this.draggedTree[i]['creteria'] = '';
            }
        })
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    public closeModal(x?, filterKeyup?) {
        this.modalService.dismissAll(x);
        this.viewMode='FILE'
    }

    //------------------------------------------------------------
    //  clear DataList
    //------------------------------------------------------------
    clear() {
        this.draggedTree = [];
        this.dummyTree = [];
        this.overallDisplaycheck.nativeElement.checked = false;
        this.overallGroupBycheck.nativeElement.checked = false;
        this.reportDescription = '';
        this.reportId = '';
        document.getElementById('loadTog')['disabled'] = false;
    }


    //------------------------------------------------------------
    //  delete
    //------------------------------------------------------------
    delete(index: any, selected: any, overallcheck: any) {
        let key = this.draggedTree[index]['uiqueIdentifier'];
        let unique_key = this.draggedTree[index]['uniqueId'];
        this.draggedTree.splice(index, 1);
        if (index >= 1) {
            this.joinCondition();
        }


        var pos = this.dummyTree.map(function (e) { return e.uiqueIdentifier; }).indexOf(key);
        this.dummyTree.splice(pos, 1)

        this.overallDataCheck(selected, overallcheck);
        this.groupByConditionCheck();
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.draggedTree, event.previousIndex, event.currentIndex);
    }

    //------------------------------------------------------------
    //  Join Condition
    //------------------------------------------------------------
    joinCondition() {
        var count = 0;
        for (var i = 0; i < this.draggedTree.length; i++) {
            this.draggedTree[i]['creteria'] = this.draggedTree[i]['creteria'] == null ? '' : this.draggedTree[i]['creteria']
            if (this.draggedTree[i]['creteria'].length &&
                this.creteriaLevelCondition(this.draggedTree[i]['creteria'])) {
                for (var j = i; j >= 0; j--) {
                    this.draggedTree[j]['creteria'] = this.draggedTree[j]['creteria'] == null ? '' : this.draggedTree[j]['creteria']
                    if (this.draggedTree[j]['creteria'].length &&
                        this.creteriaLevelCondition(this.draggedTree[j]['creteria'])) {
                        if (j != i)
                            this.draggedTree[j]['join'] = true;
                    }
                }
                for (var k = i; k < this.draggedTree.length; k++) {
                    this.draggedTree[k]['creteria'] = this.draggedTree[k]['creteria'] == null ? '' : this.draggedTree[k]['creteria']
                    if (this.draggedTree[k]['creteria'].length &&
                        this.creteriaLevelCondition(this.draggedTree[k]['creteria'])) {
                        count++;
                    }
                }
                if (count > 0) {
                    this.draggedTree[i]['join'] = false;
                }
            } else {
                this.draggedTree[i]['join'] = false;
            }
        }
    }

    //------------------------------------------------------------
    //  cretiria condition
    //------------------------------------------------------------
    creteriaLevelCondition(creteria: any) {
        if (
            creteria != 'SUM' && creteria != 'AVG' &&
            creteria != 'MIN' &&
            creteria != "COUNT(Distinct)" &&
            creteria != 'MAX' && creteria != 'COUNT'
        ) {
            return true;
        } else {
            return false;
        }
    }

    summeryCondition(index: any, element: any) {

        let summeryFilter = this.draggedTree[index]['summeryFilter'];
        
        if (summeryFilter == 'between') {
            this.draggedTree[index]['summeryStartDataTog'] = true;
            this.draggedTree[index]['summeryEndDataTog'] = true;
            // this.draggedTree[index]['endData'] = '';
            // this.draggedTree[index]['startData'] = '';
        }

        else if (summeryFilter == 'is not null' || summeryFilter == 'is null') {
            this.draggedTree[index]['summeryStartDataTog'] = false;
            this.draggedTree[index]['summeryEndDataTog'] = false;
            this.draggedTree[index]['summeryEndData'] = '';
            this.draggedTree[index]['summeryStartData'] = '';
        }

        else if (this.creteriaLevelCondition(summeryFilter)) {
            this.draggedTree[index]['summeryStartDataTog'] = true;
            this.draggedTree[index]['summeryEndDataTog'] = false;
            this.draggedTree[index]['summeryEndData'] = '';
        }

        else {
            this.draggedTree[index]['summeryStartDataTog'] = false;
            this.draggedTree[index]['summeryEndDataTog'] = false;
            this.draggedTree[index]['summeryEndData'] = '';
            this.draggedTree[index]['summeryStartData'] = '';
        }

        if (summeryFilter == 'in') {
            this.draggedTree[index]['multiple'] = true;
            this.draggedTree[index]['dateConfig'] = (this.draggedTree[index]['colDisplayType'] == 'D') ? this.configDate(this.draggedTree[index]['creteria']) : ''
        }
        else {
            this.draggedTree[index]['multiple'] = false;
            this.draggedTree[index]['dateConfig'] = (this.draggedTree[index]['colDisplayType'] == 'D') ? this.configDate(this.draggedTree[index]['creteria']) : ''
        }
        if (!('dropdown' in (this.draggedTree[index]['magnifierValues']))) {
            this.magnifierAPICall(index);
        }
        if (this.draggedTree[index].magEnableFlag == 'Y' && this.draggedTree[index].magQueryId != null) {
            if (this.draggedTree[index].magType == 1) {
                this.draggedTree[index]['summeryStartDataDisTog'] = "plain";
                this.draggedTree[index]['summeryEndDataDisTog'] = "plain";
            }
            if (this.draggedTree[index].magType == 2) {
                this.draggedTree[index]['summeryStartDataDisTog'] = "plain";
                this.draggedTree[index]['summeryEndDataDisTog'] = "plain";
            }
        }
        else{
            if (this.draggedTree[index]['colDisplayType'] != 'D') {
                this.draggedTree[index]['summeryStartDataDisTog'] = "plain";
                this.draggedTree[index]['summeryEndDataDisTog'] = "plain";
            }
            else {
                this.draggedTree[index]['summeryStartDataDisTog'] = "date";
                this.draggedTree[index]['summeryEndDataDisTog'] = "date";
            }
        }
        this.cretiriaListCondition(element, 'summeryStartData', index);
        let count = 0;
        this.draggedTree.forEach(element => {
            !this.creteriaLevelCondition(element['summeryFilter']) ? count++ : '';
        });
        this.overallGroupBycheck.nativeElement.disabled = (count) ? true : false;
        if (this.shallow_copy.summeryFilter == 'not like' || this.shallow_copy.summeryFilter == 'like' || this.shallow_copy.summeryFilter == 'MIN' || this.shallow_copy.summeryFilter == 'MAX' || this.shallow_copy.summeryFilter == 'in' || this.shallow_copy.summeryFilter == 'not in' || this.shallow_copy.summeryFilter == 'is null' || this.shallow_copy.summeryFilter == 'is not null' || this.shallow_copy.summeryFilter == 'COUNT') {
            this.shallow_copy.dynamicStartFlag = false;
            this.shallow_copy.dynamicEndFlag = false;
        }
        if (this.shallow_copy.summeryFilter == 'between') {
            this.shallow_copy.dynamicEndFlag = false;
        }
         
        if(!['in','not in'].includes(summeryFilter) && this.draggedTree[index]['summeryStartData'].split("|").length>1){
            this.draggedTree[index]['summeryStartData'] = '';
        }
    }
    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    creteriaCondition(index: any, element: any,fieldType:any) {

        let creteria = this.draggedTree[index]['creteria'];
        if(fieldType =='creteria'){
            if (creteria == 'between') {
                this.draggedTree[index]['startDataTog'] = true;
                this.draggedTree[index]['endDataTog'] = true;
                // this.draggedTree[index]['endData'] = '';
                // this.draggedTree[index]['startData'] = '';
            }

            else if (creteria == 'is not null' || creteria == 'is null') {
                this.draggedTree[index]['startDataTog'] = false;
                this.draggedTree[index]['endDataTog'] = false;
                this.draggedTree[index]['endData'] = '';
                this.draggedTree[index]['startData'] = '';
            }

            else if (this.creteriaLevelCondition(creteria)) {
                this.draggedTree[index]['startDataTog'] = true;
                this.draggedTree[index]['endDataTog'] = false;
                this.draggedTree[index]['endData'] = '';
            }

            else {
                this.draggedTree[index]['startDataTog'] = false;
                this.draggedTree[index]['endDataTog'] = false;
                this.draggedTree[index]['endData'] = '';
                this.draggedTree[index]['startData'] = '';
            }
        }

        if (creteria == 'in') {
            this.draggedTree[index]['multiple'] = true;
            this.draggedTree[index]['dateConfig'] = (this.draggedTree[index]['colDisplayType'] == 'D') ? this.configDate(this.draggedTree[index]['creteria']) : ''
        }
        else {
            this.draggedTree[index]['multiple'] = false;
            this.draggedTree[index]['dateConfig'] = (this.draggedTree[index]['colDisplayType'] == 'D') ? this.configDate(this.draggedTree[index]['creteria']) : ''
        }
        if (!('dropdown' in (this.draggedTree[index]['magnifierValues']))&& fieldType =='creteria') {
            this.magnifierAPICall(index);
        }
        if(fieldType =='creteria'){
            if (this.draggedTree[index].magEnableFlag == 'Y' && this.draggedTree[index].magQueryId != null) {
                if (element.magType == 1) {
                    this.draggedTree[index]['startDataDisTog'] = "dropdown";
                    this.draggedTree[index]['endDataDisTog'] = "dropdown";
                }
                if (element.magType == 2) {
                    this.draggedTree[index]['startDataDisTog'] = "popup";
                    this.draggedTree[index]['endDataDisTog'] = "popup";
                }
            }
            else{
                if (this.draggedTree[index]['colDisplayType'] != 'D') {
                    this.draggedTree[index]['startDataDisTog'] = "plain";
                    this.draggedTree[index]['endDataDisTog'] = "plain";
                }
                else {
                    this.draggedTree[index]['startDataDisTog'] = "date";
                    this.draggedTree[index]['endDataDisTog'] = "date";
                }
            }
        }
        this.cretiriaListCondition(element, 'startData', index);
        if(fieldType =='aggrigate'){
            let count = 0;
            this.draggedTree.forEach(element => {
                !this.creteriaLevelCondition(element['creteria']) ? count++ : '';
            });
            this.overallGroupBycheck.nativeElement.disabled = (count) ? true : false;
        }
        if (this.shallow_copy.creteria == 'not like' || this.shallow_copy.creteria == 'like' || this.shallow_copy.creteria == 'MIN' || this.shallow_copy.creteria == 'MAX' || this.shallow_copy.creteria == 'in' || this.shallow_copy.creteria == 'not in' || this.shallow_copy.creteria == 'is null' || this.shallow_copy.creteria == 'is not null' || this.shallow_copy.creteria == 'COUNT') {
            this.shallow_copy.dynamicStartFlag = false;
            this.shallow_copy.dynamicEndFlag = false;
        }
        if (this.shallow_copy.creteria == 'between') {
            this.shallow_copy.dynamicEndFlag = false;
        }
         
        if(!['in','not in'].includes(creteria) && this.draggedTree[index]['startData'].split("|").length>1){
            this.draggedTree[index]['startData'] = '';
        }
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    configDate(creteria: any) {
        let config = {};
        if (creteria == 'in' || creteria == 'not in') {
            config['allowMultiSelect'] = true;
            config['format'] = "DD-MMM-YYYY";
        }
        else {
            config['format'] = "DD-MMM-YYYY";
        };
        return config;
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    onClearLang(index) {
        this.draggedTree[index]['startDataTog'] = false;
        this.draggedTree[index]['endDataTog'] = false;
        this.draggedTree[index]['endData'] = '';
        this.draggedTree[index]['startData'] = '';
        this.draggedTree[index]['dynamicStartFlag'] = false;
        this.draggedTree[index]['dynamicEndFlag'] = false;
    }

    onClearLangSummery(index,type){
        if(type == 'aggrigate'){
            this.draggedTree[index]['aggrigate'] = '';
        }
        this.draggedTree[index]['summeryFilter'] = '';
        this.draggedTree[index]['summeryStartDataTog'] = false;
        this.draggedTree[index]['summeryEndDataTog'] = false;
        this.draggedTree[index]['summeryEndData'] = '';
        this.draggedTree[index]['summeryStartData'] = '';
        this.draggedTree[index]['summeryStartFlag'] = false;
        this.draggedTree[index]['summeryEndFlag'] = false;
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    magnifierAPICall(index) {
        let element = this.draggedTree[index];
        if (element.magEnableFlag == 'Y' && element.magQueryId != null) {
            let obj = {
                "queryId": element.magQueryId,
                "useColumn": element.useColumn,
                "displayColumn": element.displayColumn
            }
            this.service.post(environment.get_designAnalysis_magData, obj).subscribe(resp => {
                if (resp['status']) {
                    this.draggedTree[index]['magnifierValues'] = {
                        key: element.useColumn,
                        value: element.displayColumn,
                        dropdown: resp["response"]
                    }
                } else {

                }
            });
        }
    }

    //------------------------------------------------------------
    //  Popup for Magnifier type Popup
    //------------------------------------------------------------
    openModal(index: any, type: any, element: any) {
        const modelRef = this.modalService.open(MagnifierPopupComponent, {
            size: <any>'lg',
            backdrop: 'static'
        });

        this.updatePopupModifier = {
            selected_index: index,
            input_key: type,
            useColumn: element.useColumn,
            selected_data: element,
            selectedList: this.cretiriaListCondition(element, type, index),
            selection_type: element['creteria'] == 'in' ? 'multiple' : 'single',
            filter_key: element.displayColumn,
            unique_key: element.uniqueId,
            magList: element.magnifierValues['dropdown'] || [] //query
        }
        modelRef.componentInstance.getDetails = this.updatePopupModifier;
        modelRef.componentInstance.afterSelected.subscribe((selected_element) => {
            this.draggedTree[selected_element['selected_index']][selected_element['input_key']] = selected_element['selectedList']
        });
        modelRef.componentInstance.repBack.subscribe((e) => {
            if (e) {
                modelRef.close();
            }
        });

    }


    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    cretiriaListCondition(element: any, type, index: any) {
        if (element['creteria'] != 'in' && element[type].length > 1 && Array.isArray(element[type])) {
            element[type] = [],
                this.draggedTree[index][type] = '';
        } else {
            this.draggedTree[index][type] = element[type];
        }
        return element[type];
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    individualCheck(index: any, selected: any, overallcheck: any, mag?) {
        selected[index].display = !selected[index].display;
        overallcheck = (mag) ? document.getElementById(overallcheck) : overallcheck;
        this.overallDataCheck(selected, overallcheck)
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    overallDisplaySelect(event: any, selected: any) {
        selected.forEach(element => {
            element['display'] = event.target.checked ? true : false;
            event.target.checked == false ? element['group'] = false : '';
        });
        event.target.checked == false ? this.overallGroupBycheck.nativeElement.checked = false : '';
        this.groupByConditionCheck()
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    overallGroupSelect(event: any, selected: any) {
        selected.forEach(element => {
            element.colType == 'D' && element.colExperssionType == "DBSQL"
            element['group'] = event.target.checked ?
                element.colType == 'D' && element.colExperssionType == "DBSQL" ? false : this.creteriaLevelCondition(element.aggrigate) ? true : false : false;
        });
        this.displayLevelGroupBy();
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    overallDataCheck(selected, overallcheck: any) {
        if (overallcheck != '') {
            var count = 0;
            selected.forEach(element => {
                if (element.display) {
                    count++;
                }
            });

            if (count && count == selected.length) {
                overallcheck.checked = true;
            } else {
                overallcheck.checked = false;
            }
        }
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    startDrag(event) {
        this.dragDropAnimation().add('droppableStyle');
    }

    dragEnd(event) {
        this.dragDropAnimation().remove('droppableStyle');
    }

    dragDropAnimation() {
        var droparea = document.getElementsByClassName('droparea');
        return droparea[0].classList;
    }
    @HostListener('document:click', ['$event'])
    @HostListener('document:touchstart', ['$event'])
    handleOutsideClick(event) {
        if (!event.target.closest('.queryDesignDownloadClk')) {
            this.showDownloadDropdown=false
            this.formatDownload={
                excel:false,
                csv:false,
                pdf:false,
            }
        }
        setTimeout(()=>{
            if(event.target.closest('.dashboardBg') || event.target.closest('.reportBg') || 
            event.target.closest('app-report-slide') || event.target.closest('app-dashboard-tree-slide')){
                this.reportOpen = true;
            }
            else{
                this.reportOpen = false;
            }
        },400)
     }

    downloadQuery(){
        let formatArr=[]
        // if(this.formatDownload.pdf)
        //     formatArr.push('pdf')
        // if(this.formatDownload.csv)
        //     formatArr.push('csv')
        // if(this.formatDownload.excel)
            formatArr.push('excel')
        // if(formatArr.length){
            if(this.draggedTree.length){
                let formatType =formatArr.join(',')
                if (this.doValidation() == 0 || this.doValidation() != 0) {
                    let dragged_finalTree = JSON.parse(JSON.stringify(this.draggedTree));
                    let condition_detail: any = {};
                    let finalTree = [];
                    this.draggedTree.forEach((element, index, array) => {
                        (element.colDisplayType == 'C' || element.colDisplayType == 'N') ?
                            finalTree.push(
                                {
                                    tabelId: element.tableId,
                                    tableName: element.tableName,
                                    tabelAliasName: element.tableAliasName,
                                    sortOrder: index + 1,
                                    colId: element.colId,
                                    colName: element.fields,
                                    colType: element.colType,
                                    alias: element.alias,
                                    colDisplayType: element.colDisplayType,
                                    operator: element.creteria,
                                    aggFunction: element.aggrigate,
                                    summaryCriteria : element.summeryFilter,
                                    displayFlag: element.display ? 'Y' : 'N',
                                    sortType: element.sortType,
                                    includeGroupCol: element.includeGroupCol,
                                    joinCondition: element.joinData,
                                    groupBy: element.group ? 'Y' : 'N',
                                    value1: (element.creteria == 'like') ? `%${element.startData}%` : Array.isArray(element.startData) ? element.startData.join() : element.startData,
                                    value2: (element.creteria == 'like') ? `%${element.endData}%` :Array.isArray(element.endData) ? element.endData.join() : element.endData,
                                    summaryValue1 : (element.summeryFilter == 'like') ? `%${element.summeryStartData}%` : Array.isArray(element.summeryStartData) ? element.summeryStartData.join() : element.summeryStartData,
                                    summaryValue2 : (element.summeryFilter == 'like') ? `%${element.summeryEndData}%` : Array.isArray(element.summeryEndData) ? element.summeryEndData.join() : element.summeryEndData,
                                    scalingFlag: element.scalingFlag ? "Y" : "N",
                                    scalingFormat: element.scalingFormat,
                                    numberFormat: element.numberFormat ? "Y" : "N",
                                    decimalFlag: element.decimalFlag ? "Y" : "N",
                                    decimalCount: element.decimalCount,
                                })
                            :
                            finalTree.push(
                                {
                                    tabelId: element.tableId,
                                    tableName: element.tableName,
                                    tabelAliasName: element.tableAliasName,
                                    sortOrder: index + 1,
                                    colId: element.colId,
                                    colName: element.fields,
                                    colType: element.colType,
                                    alias: element.alias,
                                    colDisplayType: element.colDisplayType,
                                    operator: element.creteria,
                                    aggFunction: element.aggrigate,
                                    summaryCriteria : element.summeryFilter,
                                    displayFlag: element.display ? 'Y' : 'N',
                                    sortType: element.sortType,
                                    includeGroupCol: element.includeGroupCol,
                                    joinCondition: element.joinData,
                                    groupBy: element.group ? 'Y' : 'N',
                                    value1: (element.creteria == 'like') ? `%${element.startData}%` : Array.isArray(element.startData) ? element.startData.join() : element.startData.split('|').join(','),
                                    value2: (element.creteria == 'like') ? `%${element.endData}%` : Array.isArray(element.endData) ? element.endData.join() : element.endData,
                                    summaryValue1 : (element.summeryFilter == 'like') ? `%${element.summeryStartData}%` : Array.isArray(element.summeryStartData) ? element.summeryStartData.join() : element.summeryStartData,
                                    summaryValue2 : (element.summeryFilter == 'like') ? `%${element.summeryEndData}%` : Array.isArray(element.summeryEndData) ? element.summeryEndData.join() : element.summeryEndData,
                                    dynamicStartFlag: (element.dynamicStartFlag) ? "Y" : "N",
                                    dynamicEndFlag: (element.dynamicEndFlag) ? "Y" : "N",
                                    // dynamicDateFormat: (element.formatType != null) ? element.formatType : '',
                                    dateConversionSyntax: (element.dateConversionSyntax != null) ? element.dateConversionSyntax : '',
                                    dateFormattingSyntax: (element.dateFormattingSyntax != null) ? element.dateFormattingSyntax : '',
                                    dynamicStartDate: element.dynamicStartDate,
                                    javaFormatDesc: element.javaFormatDesc,
                                    dynamicEndDate: element.dynamicEndDate,
                                    dynamicStartOperator: (element.dynamicStartFlag) ? `${element.dynamicStartOperator}${element.dy_start_op_val}` : '',
                                    dynamicEndOperator: (element.dynamicEndFlag) ? `${element.dynamicEndOperator}${element.dy_end_op_val}` : '',
                                    summaryDynamicStartFlag: (element.summeryStartFlag) ? "Y" : "N",
                                    summaryDynamicEndFlag: (element.summeryEndFlag) ? "Y" : "N",
                                    summaryDynamicStartDate: element.summeryStartDate,
                                    summaryDynamicEndDate: element.summeryEndDate,
                                    summaryDynamicStartOperator: (element.summeryStartOperator) ? `${element.summeryStartOperator}${element.sum_start_op_val}` : '',
                                    summaryDynamicEndOperator: (element.summeryEndOperator) ? `${element.summeryEndOperator}${element.sum_end_op_val}` : '',
                                });
                    });
                    let getJoinQuery = {
                        mainModel: {
                            distinctFlag:this.distinctFlag ?'Y':'N',
                            catalogId: this.catlogDetails['catalogId'],
                            formatType:formatType
                        },
                        reportFields: finalTree
                    }
                    this.service.post('designAnalysis/returnJoinCondition', getJoinQuery).subscribe(resp => {
                        if (resp['status']) {
                            condition_detail['joinCondition'] = resp['response']['WHERE'] || '';
                            condition_detail['fromCondition'] = resp['response']['FROM'] || '';
                            condition_detail['joinClause'] = this.catlogDetails['joinClause'];
                            let reportQuery = Object.assign({}, getJoinQuery);
                            reportQuery['mainModel']['reportDescription'] = this.reportDescription;
                            reportQuery['mainModel']['reportId'] = this.reportId;
                            reportQuery['hashArray'] = this.hashArray;
                            reportQuery['hashValueArray'] = this.hashValueArray;
                            reportQuery['reportFields'].forEach((element)=>{
                                element.alias = element.alias.replaceAll(" ", "_");
                            })
                            this.saveQueryArray = condition_detail;
                            this.saveQueryArray['getDetails'] = dragged_finalTree;
                            this.saveQueryArray['extraModel'] = reportQuery;
                            this.saveQueryArray['viewTable'] = 'download';
                            let saveQueryArray =this.saveService.exportQueryFormation(this.saveQueryArray)
                            let filename=(!isNullOrUndefined(reportQuery['mainModel']['reportId']) && reportQuery['mainModel']['reportId'] !='')? `${reportQuery['mainModel']['reportId']}.xlsx`:'Export.xlsx';
                            this.service.fileDownloads(environment.get_designAnalysis_Download+'/'+filename,saveQueryArray).subscribe(resp => {
                                const blob = new Blob([resp], { type: 'text/csv' });
                                const url = window.URL.createObjectURL(blob);
                                const link = this.downloadZipLink.nativeElement;
                                link.href = url;
                                link.download =(!isNullOrUndefined(reportQuery['mainModel']['reportId']) && reportQuery['mainModel']['reportId'] !='') ? `${reportQuery['mainModel']['reportId']}.xlsx`:'Export.xlsx';
                                link.click();
                                window.URL.revokeObjectURL(url);
                            }, (error: any) => {
                                if (error.substr(12, 3) == '204') {
                                this.commonService.showToastr.error('No Records to Export');
                                }
                                if (error.substr(12, 3) == '420') {
                                this.commonService.showToastr.error('Error Generating Report');
                                }
                                if (error.substr(12, 3) == '417') {
                                this.commonService.showToastr.error('Error while exporting the report');
                                }
                            })
                            this.showDownloadDropdown=false
                            this.formatDownload={
                                excel:false,
                                csv:false,
                                pdf:false,
                            }
                            this.copyFlag = true;
                        }
                        else {
                            this.commonService.showToastr.error(resp['message'])
                        }
                    });
                }
                else {
                    this.commonService.showToastr.error("Creteria condition Value should not be empty")
                }
            }
        // }
        // else{
        //     this.commonService.showToastr.warning("Atleast select one file format")
        // }
    }
    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    queryExecution(executionType: any, viewTable?) {
        if (this.doValidation() == 0 || this.doValidation() != 0) {
            let dragged_finalTree = JSON.parse(JSON.stringify(this.draggedTree));
            let condition_detail: any = {};
            let finalTree = [];
            this.draggedTree.forEach((element, index, array) => {
                (element.colDisplayType == 'C' || element.colDisplayType == 'N') ?
                    finalTree.push(
                        {
                            tabelId: element.tableId,
                            tableName: element.tableName,
                            tabelAliasName: element.tableAliasName,
                            sortOrder: index + 1,
                            colId: element.colId,
                            colName: element.fields,
                            colType: element.colType,
                            alias: element.alias,
                            colDisplayType: element.colDisplayType,
                            operator: element.creteria,
                            aggFunction: element.aggrigate,
                            summaryCriteria : element.summeryFilter,
                            displayFlag: element.display ? 'Y' : 'N',
                            sortType: element.sortType,
                            includeGroupCol: element.includeGroupCol,
                            joinCondition: element.joinData,
                            groupBy: element.group ? 'Y' : 'N',
                            value1: (element.creteria == 'like') ? `%${element.startData}%` : Array.isArray(element.startData) ? element.startData.join() : element.startData,
                            value2: (element.creteria == 'like') ? `%${element.endData}%` :Array.isArray(element.endData) ? element.endData.join() : element.endData,
                            summaryValue1 : (element.summeryFilter == 'like') ? `%${element.summeryStartData}%` : Array.isArray(element.summeryStartData) ? element.summeryStartData.join() : element.summeryStartData,
                            summaryValue2 : (element.summeryFilter == 'like') ? `%${element.summeryEndData}%` : Array.isArray(element.summeryEndData) ? element.summeryEndData.join() : element.summeryEndData,
                            scalingFlag: element.scalingFlag ? "Y" : "N",
                            scalingFormat: element.scalingFormat,
                            numberFormat: element.numberFormat ? "Y" : "N",
                            decimalFlag: element.decimalFlag ? "Y" : "N",
                            decimalCount: element.decimalCount,
                        })
                    :
                    finalTree.push(
                        {
                            tabelId: element.tableId,
                            tableName: element.tableName,
                            tabelAliasName: element.tableAliasName,
                            sortOrder: index + 1,
                            colId: element.colId,
                            colName: element.fields,
                            colType: element.colType,
                            alias: element.alias,
                            colDisplayType: element.colDisplayType,
                            operator: element.creteria,
                            aggFunction: element.aggrigate,
                            summaryCriteria : element.summeryFilter,
                            displayFlag: element.display ? 'Y' : 'N',
                            sortType: element.sortType,
                            includeGroupCol: element.includeGroupCol,
                            joinCondition: element.joinData,
                            groupBy: element.group ? 'Y' : 'N',
                            value1: (element.creteria == 'like') ? `%${element.startData}%` : Array.isArray(element.startData) ? element.startData.join() : element.startData.split('|').join(','),
                            value2: (element.creteria == 'like') ? `%${element.endData}%` : Array.isArray(element.endData) ? element.endData.join() : element.endData,
                            summaryValue1 : (element.summeryFilter == 'like') ? `%${element.summeryStartData}%` : Array.isArray(element.summeryStartData) ? element.summeryStartData.join() : element.summeryStartData,
                            summaryValue2 : (element.summeryFilter == 'like') ? `%${element.summeryEndData}%` : Array.isArray(element.summeryEndData) ? element.summeryEndData.join() : element.summeryEndData,
                            dynamicStartFlag: (element.dynamicStartFlag) ? "Y" : "N",
                            dynamicEndFlag: (element.dynamicEndFlag) ? "Y" : "N",
                            // dynamicDateFormat: (element.formatType != null) ? element.formatType : '',
                            dateConversionSyntax: (element.dateConversionSyntax != null) ? element.dateConversionSyntax : '',
                            dateFormattingSyntax: (element.dateFormattingSyntax != null) ? element.dateFormattingSyntax : '',
                            dynamicStartDate: element.dynamicStartDate,
                            javaFormatDesc: element.javaFormatDesc,
                            dynamicEndDate: element.dynamicEndDate,
                            dynamicStartOperator: (element.dynamicStartFlag) ? `${element.dynamicStartOperator}${element.dy_start_op_val}` : '',
                            dynamicEndOperator: (element.dynamicEndFlag) ? `${element.dynamicEndOperator}${element.dy_end_op_val}` : '',
                            summaryDynamicStartFlag: (element.summeryStartFlag) ? "Y" : "N",
                            summaryDynamicEndFlag: (element.summeryEndFlag) ? "Y" : "N",
                            summaryDynamicStartDate: element.summeryStartDate,
                            summaryDynamicEndDate: element.summeryEndDate,
                            summaryDynamicStartOperator: (element.summeryStartOperator) ? `${element.summeryStartOperator}${element.sum_start_op_val}` : '',
                            summaryDynamicEndOperator: (element.summeryEndOperator) ? `${element.summeryEndOperator}${element.sum_end_op_val}` : '',
                        });
            });
            let getJoinQuery = {
                mainModel: {
                    distinctFlag:this.distinctFlag ?'Y':'N',
                    catalogId: this.catlogDetails['catalogId'],
                },
                reportFields: finalTree
            }
            // dragged_finalTree.forEach((element, index, array) => {
            //     if (element.creteria == 'in' && isArray(element.startData)) {
            //         element.startData.forEach((ele, ind, ar) => {
            //             ele = ele.substr(1);
            //             ar[ind] = ele.replace("'", "")
            //         });
            //         // array[index]['startData']= element;
            //     }
            //     if (element.creteria == 'in' && !isArray(element.startData)) {
            //         let data = (element.startData).split(",");
            //         data.forEach((ele, ind, ar) => {
            //             ar[ind] = `'${ele}'`;
            //         });
            //         array[index]['startData'] = data.join(',')
            //     }
            // });
            this.service.post('designAnalysis/returnJoinCondition', getJoinQuery).subscribe(resp => {
                if (resp['status']) {

                    condition_detail['joinCondition'] = resp['response']['WHERE'] || '';
                    condition_detail['fromCondition'] = resp['response']['FROM'] || '';
                    condition_detail['joinClause'] = this.catlogDetails['joinClause'];
                    if (executionType == 'showQuery') {
                        this.finalShowQuery(condition_detail, dragged_finalTree);
                    }
                    else {
                        let reportQuery = Object.assign({}, getJoinQuery);
                        reportQuery['mainModel']['reportDescription'] = this.reportDescription;
                        reportQuery['mainModel']['reportId'] = this.reportId;
                        reportQuery['hashArray'] = this.hashArray;
                        reportQuery['hashValueArray'] = this.hashValueArray;
                        reportQuery['reportFields'].forEach((element)=>{
                            element.alias = element.alias.replaceAll(" ", "_");
                        })
                        viewTable = viewTable == 'save' ? 'save' : 'execute';
                        this.saveQueryArray = condition_detail;
                        this.saveQueryArray['getDetails'] = dragged_finalTree;
                        this.saveQueryArray['extraModel'] = reportQuery;
                        this.saveQueryArray['viewTable'] = viewTable;
                        this.saveService.getQueryDetails(this.saveQueryArray)
                    }
                    this.copyFlag = true;
                }
                else {

                    this.commonService.showToastr.error(resp['message'])
                }
            });
        }
        else {
            // const modelRef = this.modalService.open(GenericQueryPopupComponent, {
            //     size: 'sm',
            //     backdrop: 'static'
            // });
            // modelRef.componentInstance.title = 'Condition Checking';
            // modelRef.componentInstance.message = 'where condition fields should not be empty';
            // modelRef.componentInstance.popupType = 'alert';
            // modelRef.componentInstance.userConfirmation.subscribe((e) => {
            //     if (e == 'Yes') {
            //         modelRef.close();
            //     } else {

            //         modelRef.close();
            //     }
            // });
            this.commonService.showToastr.error("Creteria condition Value should not be empty")
        }
    }


    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    coppyQuery() {
        this.cpy_reportDescription = this.reportDescription;
        this.cpy_reportId = this.reportId;
        this.commonService.reportDetails.next({});
        this.reportId = '';
        this.reportDescription = `Copy of ${this.reportDescription}`
        document.getElementById('loadTog')['disabled'] = true;
        this.copyFlag = false;
        this.commonService.showToastr.success("Copied Successfully");
    };

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    quotesChecking(type: any, index: any, event: any, creteria: any, dataType: any) {
        let value = event.target.value;
        // value = (dataType != 'number') ? value : value.replace(/[^0-9_,]/g, '');
        // value = value.replace(/[&\/\\#+()$~.":*?<>{}@^!]/gi, "");
        if (creteria != 'in' && creteria != 'not in') {
            if (value.startsWith('\'')) {
                value = value.slice(1);
            }
            if (value.endsWith('\'')) {
                value = value.slice(0, -1);
            }
        }
        else {
            value = value.split("\'");
            value = value.join("");
            value = `'${value.split(',').join('\',\'')}'`;
            value = value.split(",").filter(ele => { if (ele.length > 2) { return true } })
        }
        this.draggedTree[index][type] = value;
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    toggleTree() {
        const treeContainer = document.getElementById('tree_hide')
        const detailsContainer = document.getElementById('tree_hide1')
        const toggleChevron = document.getElementById('toggleChevron')

        if (this.visibleTree) {
            treeContainer.classList.add('col-md-3a')
            detailsContainer.classList.add('col-md-9a');
            toggleChevron.classList.remove("fa-caret-right");
            toggleChevron.classList.add("fa-caret-left");
            this.visibleTree = false;
        }
        else {
            detailsContainer.classList.remove('col-md-9a');
            treeContainer.classList.remove('col-md-3a')
            toggleChevron.classList.remove("fa-caret-left");
            toggleChevron.classList.add("fa-caret-right");
            this.visibleTree = true;
        }
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    doValidation() {
        let count = 0;
        this.draggedTree.forEach(element => {
            // if(element.startDataDisTog == 'Date' && element.dynamicStartFlag || element.endDataDisTog == 'Date' && element.dynamicEndFlag ){

            // }
            if ((element.startDataTog && !element.startData.length) || (element.endDataTog && !element.endData.length)) {
                count++;
            }
        });
        return count;
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    selectedItem(index: any, dialogBox: any) {
    
        this.shallow_copy = this.draggedTree[index];
        !this.shallow_copy.hasOwnProperty('dynamicStartDate') || this.shallow_copy.hasOwnProperty('dynamicStartDate') && this.shallow_copy.dynamicStartDate == null ? this.shallow_copy['dynamicStartDate'] = 'D' : '';
        !this.shallow_copy.hasOwnProperty('dynamicStartOperator') || this.shallow_copy.hasOwnProperty('dynamicStartOperator') && this.shallow_copy.dynamicStartOperator == "" ? this.shallow_copy['dynamicStartOperator'] = '+' : '';
        !this.shallow_copy.hasOwnProperty('dy_start_op_val') || this.shallow_copy.hasOwnProperty('dy_start_op_val') && this.shallow_copy.dy_start_op_val == "" ? this.shallow_copy['dy_start_op_val'] = '0' : '';
        !this.shallow_copy.hasOwnProperty('dynamicEndDate') || this.shallow_copy.hasOwnProperty('dynamicEndDate') && this.shallow_copy.dynamicEndDate == null ? this.shallow_copy['dynamicEndDate'] = 'D' : '';
        !this.shallow_copy.hasOwnProperty('dynamicEndOperator') || this.shallow_copy.hasOwnProperty('dynamicEndOperator') && this.shallow_copy.dynamicEndOperator == "" ? this.shallow_copy['dynamicEndOperator'] = '+' : '';
        !this.shallow_copy.hasOwnProperty('dy_end_op_val') || this.shallow_copy.hasOwnProperty('dy_end_op_val') && this.shallow_copy.dy_end_op_val == "" ? this.shallow_copy['dy_end_op_val'] = '0' : '';

        !this.shallow_copy.hasOwnProperty('summeryStartDate') || this.shallow_copy.hasOwnProperty('summeryStartDate') && this.shallow_copy.summeryStartDate == null ? this.shallow_copy['summeryStartDate'] = 'D' : '';
        !this.shallow_copy.hasOwnProperty('summeryStartOperator') || this.shallow_copy.hasOwnProperty('summeryStartOperator') && this.shallow_copy.summeryStartOperator == "" ? this.shallow_copy['summeryStartOperator'] = '+' : '';
        !this.shallow_copy.hasOwnProperty('sum_start_op_val') || this.shallow_copy.hasOwnProperty('sum_start_op_val') && this.shallow_copy.sum_start_op_val == "" ? this.shallow_copy['sum_start_op_val'] = '0' : '';
        !this.shallow_copy.hasOwnProperty('summeryEndDate') || this.shallow_copy.hasOwnProperty('summeryEndDate') && this.shallow_copy.summeryEndDate == null ? this.shallow_copy['summeryEndDate'] = 'D' : '';
        !this.shallow_copy.hasOwnProperty('summeryEndOperator') || this.shallow_copy.hasOwnProperty('summeryEndOperator') && this.shallow_copy.summeryEndOperator == "" ? this.shallow_copy['summeryEndOperator'] = '+' : '';
        !this.shallow_copy.hasOwnProperty('sum_end_op_val') || this.shallow_copy.hasOwnProperty('sum_end_op_val') && this.shallow_copy.sum_end_op_val == "" ? this.shallow_copy['sum_end_op_val'] = '0' : '';
        this.shallow_copy['index'] = index;
        this.shallow_copy['strError'] = false;
        this.shallow_copy['endError'] = false;
        this.deep_copy = Object.assign({}, this.draggedTree[index]);
        this.deep_copy['index'] = index;
        // if (!('dropdown' in (this.draggedTree[index]['magnifierValues']))) {
        //     this.magnifierAPICall(index);
        // }
        this.modalService.open(dialogBox, {
            backdrop: 'static',
            windowClass: 'configure-popup'
        });
    }


    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    applyDataForm() {
        let dynamicDataGenerate;
        let summeryDataGenerate;
        if(this.dateErrorCheck()){
        if (this.shallow_copy.colDisplayType == 'D') {
            dynamicDataGenerate = (Object.assign({}, {}));
            dynamicDataGenerate['dynamicStartFlag'] = (this.shallow_copy.hasOwnProperty('dynamicStartFlag') && this.shallow_copy.dynamicStartFlag) ? 'Y' : 'N';
            dynamicDataGenerate['dynamicEndFlag'] = (this.shallow_copy.hasOwnProperty('dynamicEndFlag') && this.shallow_copy.dynamicEndFlag) ? 'Y' : 'N';
            dynamicDataGenerate['dynamicStartDate'] = (this.shallow_copy.hasOwnProperty('dynamicStartFlag') && this.shallow_copy.dynamicStartFlag) ? this.shallow_copy.dynamicStartDate : '';
            dynamicDataGenerate['dynamicEndDate'] = (this.shallow_copy.hasOwnProperty('dynamicEndFlag') && this.shallow_copy.dynamicEndFlag) ? this.shallow_copy.dynamicEndDate : '';
            dynamicDataGenerate['dynamicStartOperator'] = (this.shallow_copy.hasOwnProperty('dynamicStartFlag') && this.shallow_copy.dynamicStartFlag) ? `${this.shallow_copy.dynamicStartOperator}${this.shallow_copy.dy_start_op_val}` : '';
            dynamicDataGenerate['dynamicEndOperator'] = (this.shallow_copy.hasOwnProperty('dynamicEndFlag') && this.shallow_copy.dynamicEndFlag) ? `${this.shallow_copy.dynamicEndOperator}${this.shallow_copy.dy_end_op_val}` : '';

            // dynamicDataGenerate['dynamicDateFormat'] = (this.shallow_copy.formatType != null) ? this.shallow_copy.formatType : '';
            dynamicDataGenerate['dateConversionSyntax'] = (this.shallow_copy.dateConversionSyntax != null) ? this.shallow_copy.dateConversionSyntax : '';
            dynamicDataGenerate['dateFormattingSyntax'] = (this.shallow_copy.dateFormattingSyntax != null) ? this.shallow_copy.dateFormattingSyntax : '';
            dynamicDataGenerate['javaFormatDesc'] = (this.shallow_copy.dynamicStartFlag || this.shallow_copy.dynamicEndFlag) ? this.shallow_copy.javaFormatDesc : '';
            dynamicDataGenerate['operator'] = this.shallow_copy.creteria;
            (dynamicDataGenerate['dynamicStartFlag'] == 'Y' || dynamicDataGenerate['dynamicEndFlag'] == 'Y') ?
                this.service.post(environment.getDynamicDate, dynamicDataGenerate).subscribe(resp => {
                    if (resp['status'] == 1) {
                        this.shallow_copy['startData'] = (resp['response'].value1 == '' || resp['response'].value1 == null) ? this.shallow_copy['startData'] : resp['response'].value1;
                        this.shallow_copy['endData'] = (resp['response'].value2 == '' || resp['response'].value2 == null) ? this.shallow_copy['endData'] : resp['response'].value2;;
                        this.draggedTree[this.shallow_copy['index']] = this.shallow_copy;
                    }
                }) : '';
            summeryDataGenerate = (Object.assign({}, {}));
            summeryDataGenerate['dynamicStartFlag'] = (this.shallow_copy.hasOwnProperty('summeryStartFlag') && this.shallow_copy.summeryStartFlag) ? 'Y' : 'N';
            summeryDataGenerate['dynamicEndFlag'] = (this.shallow_copy.hasOwnProperty('summeryEndFlag') && this.shallow_copy.summeryEndFlag) ? 'Y' : 'N';
            summeryDataGenerate['dynamicStartDate'] = (this.shallow_copy.hasOwnProperty('summeryStartFlag') && this.shallow_copy.summeryStartFlag) ? this.shallow_copy.summeryStartDate : '';
            summeryDataGenerate['dynamicEndDate'] = (this.shallow_copy.hasOwnProperty('summeryEndFlag') && this.shallow_copy.summeryEndFlag) ? this.shallow_copy.summeryEndDate : '';
            summeryDataGenerate['dynamicStartOperator'] = (this.shallow_copy.hasOwnProperty('summeryStartFlag') && this.shallow_copy.summeryStartFlag) ? `${this.shallow_copy.summeryStartOperator}${this.shallow_copy.sum_start_op_val}` : '';
            summeryDataGenerate['dynamicEndOperator'] = (this.shallow_copy.hasOwnProperty('summeryEndFlag') && this.shallow_copy.summeryEndFlag) ? `${this.shallow_copy.summeryEndOperator}${this.shallow_copy.sum_end_op_val}` : '';
            summeryDataGenerate['dateConversionSyntax'] = (this.shallow_copy.dateConversionSyntax != null) ? this.shallow_copy.dateConversionSyntax : '';
            summeryDataGenerate['dateFormattingSyntax'] = (this.shallow_copy.dateFormattingSyntax != null) ? this.shallow_copy.dateFormattingSyntax : '';
            summeryDataGenerate['javaFormatDesc'] = (this.shallow_copy.summeryStartFlag || this.shallow_copy.summeryEndFlag) ? this.shallow_copy.javaFormatDesc : '';
            summeryDataGenerate['operator'] = this.shallow_copy.summeryFilter;
            (summeryDataGenerate['dynamicStartFlag'] == 'Y' || summeryDataGenerate['dynamicEndFlag'] == 'Y') ?
                this.service.post(environment.getDynamicDate, summeryDataGenerate).subscribe(resp => {
                    if (resp['status'] == 1) {
                        this.shallow_copy['summeryStartData'] = (resp['response'].value1 == '' || resp['response'].value1 == null) ? this.shallow_copy['startData'] : resp['response'].value1;
                        this.shallow_copy['summeryEndData'] = (resp['response'].value2 == '' || resp['response'].value2 == null) ? this.shallow_copy['summeryEndData'] : resp['response'].value2;;
                        this.draggedTree[this.shallow_copy['index']] = this.shallow_copy;
                    }
                }) : '';
        }
        this.joinCondition();
        this.groupByConditionCheck();
        this.modalService.dismissAll();
        this.viewMode='FILE';
        //this.overAllGroupCheckBox();
        }
    }

    dateErrorCheck(){
       let stbol = true;
       let enbol = true;
       if(this.shallow_copy.colDisplayType == 'D'){
        if(this.shallow_copy.startDataTog && !this.shallow_copy['dynamicStartFlag']){
           this.shallow_copy['strError'] =  errorChecking(this.shallow_copy['startData'].split(" | "));
           stbol = !this.shallow_copy['strError'];
        }
        if(this.shallow_copy.endDataTog && !this.shallow_copy['dynamicEndFlag']){
           this.shallow_copy['endError'] =  errorChecking(this.shallow_copy['endData'].split(" | "));
           enbol = !this.shallow_copy['endError'];
        }
       }
       function errorChecking(dates){
        let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
         let bolArray = dates.map(ele=>{
            let item = ele.split('-');
            return item.length==3 && (item[0].length == 2 && !isNaN(item[0])) && months.includes(item[1]) && 
            (item[2].length == 4 && !isNaN(item[2]));
         });
         return !bolArray.every(el=>el);
       }

       

       return (stbol && enbol);
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    closeDataForm() {
        this.draggedTree[this.deep_copy['index']] = this.deep_copy;
        let count = 0;
        this.draggedTree.forEach(element => {
            !this.creteriaLevelCondition(element['aggrigate']) ? count++ : '';
        });
        this.overallGroupBycheck.nativeElement.disabled = (count) ? true : false;
        this.modalService.dismissAll();
        this.viewMode='FILE';
    }

    //------------------------------------------------------------
    //  
    //------------------------------------------------------------
    finalShowQuery(condition_detail: any, dragged_finalTree: any) {
        condition_detail['distinctCondition']=this.distinctFlag;
        const modelRef = this.modalService.open(ShowQueryComponent, {
            size: 'lg',
            backdrop: 'static',
            windowClass: 'show-query'
        });
        modelRef.componentInstance.getDetails = dragged_finalTree;
        modelRef.componentInstance.getJoinsDetail = condition_detail
        modelRef.componentInstance.qyueryToShow.subscribe((e) => {
            if (e) {
                this.closeModal(ShowQueryComponent);
            }
        })
    }


    //------------------------------------------------------------
    //  loading existing Report
    //------------------------------------------------------------
    loadData() {
        let loadData = {
            "catalogId": this.catlogDetails['catalogId']
        }

        this.service.post(environment.get_designAnalysis_Load, loadData).subscribe(resp => {
            if (resp['status']) {
                const modelRef = this.modalService.open(LoadDataComponent, {
                    size: <any>'md',
                    backdrop: 'static'
                });
                modelRef.componentInstance.reportList = resp['response']['response'];
                modelRef.componentInstance.queryList.subscribe((e) => {
                    this.draggedTree = e['reportFields']['originalList'];
                    this.dummyTree = e['reportFields']['dummyList'];
                    this.reportId = e['mainModel']['reportId'];
                    this.reportDescription = e['mainModel']['reportDescription'];
                    this.distinctFlag=e['mainModel']['distinctFlag'];
                    this.joinCondition();
                    // this.groupByConditionCheck();
                    this.overallDisplaycheck.nativeElement.checked = this.draggedTree.length ? this.draggedTree.every(function (num) { return num.display; }) : false;
                    modelRef.close();
                    this.copyFlag = true;
                });

            }
            else {

                this.commonService.showToastr.error(resp['message']);
            }
        })
    }

    stringColmnn(data,columnName){
        let returnStr='';
        if(columnName == 'creteria'){
            if(data.creteria!='' && data.creteria !='between'){
                let valueName=data.creteriaDropDown.filter((items)=>{ return items.key == data.creteria })
                if(valueName.length)
                    returnStr = valueName[0].value+' '+data.startData
            }
            else{
                let valueName=data.creteriaDropDown.filter((items)=>{ return items.key == data.creteria })
                if(valueName.length)
                    returnStr = valueName[0].value+' '+data.startData+' and '+data.endData
            }
        }
        if(columnName == 'summery' && !isNullOrUndefined(data.summeryFilter)){
            if(data.summeryFilter!='' && data.summeryFilter !='between'){
                let valueName=data.summeryDropDown.filter((items)=>{ return items.key == data.summeryFilter })
                if(valueName.length)
                    returnStr = valueName[0].value+' '+data.summeryStartData
            }
            else{
                let valueName=data.summeryDropDown.filter((items)=>{ return items.key == data.summeryFilter })
                if(valueName.length)
                    returnStr = valueName[0].value+' '+data.summeryStartData+' and '+data.summeryEndData
            }
        }
        return returnStr
    }

    //------------------------------------------------------------
    //  return new Promise((resolve, reject) => {
    //------------------------------------------------------------
    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.draggedTree = [];
        this.dummyTree = [];
        this.modalService.dismissAll();
        this.clear();
    }
    closeModel() {
        this.modalService.dismissAll();
    }
    openDealEntryModal() {
        this.modalService.open(this.addTemplate, { size: 'lg', backdrop: 'static' });
    }
}
