
import { Component, OnInit, Renderer2, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService, GlobalService } from 'src/app/service';
import { forkJoin, Subscription } from 'rxjs';
import { Router } from '@angular/router';
// import { columnDetail } from '../modals/designQuery';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { CommonFilterPaginationService } from 'src/app/service/common-filter-pagination.service';
import { CommonService } from 'src/app/service/common.service';
import { isUndefined, isNull, isNullOrUndefined } from 'util';
import { creteria_con } from '../modals/creteria'
import { environment } from 'src/environments/environment';
import { tableDetail } from './data.js';

// import { ExecuteQueryComponent } from '../../sharedModules/execute-query/execute-query.component';
import { ExecuteQueryNewComponent } from 'src/app/shared/execute-query-new/execute-query-new.component';
// import { SearchComponent } from 'src/app/sharedModules/search/search.component';

@Component({
  selector: 'app-manage-query',
  templateUrl: './manage-query.component.html',
  styleUrls: ['./manage-query.component.css']
})
export class ManageQueryComponent implements OnInit, AfterViewInit, OnDestroy {


  //------------------------------------------------------------
  // PUBLIC VARIABLES AND DECLARATIONS
  //------------------------------------------------------------
  catalogListing: any = [];
  catalogFilterList: any = [];
  reportModel: any = {};
  draggedTree: any = [];
  dummyTree: any = [];

  ngxTableConfig: any = {
    api: 'get_all_designQuery',
    dataToDisplay: [],
    filterApi: 'designAnalysis_search',
    addLable: 'Add',
    routingLable: 'Manual Query Listing',
    title: 'Design Query Listing',
    count: 30
  }
  filterList: any = [];
  resetModel: any = { key: '' };

  @ViewChild('catalogList') catalogList: any;
  tableDetail;
  leftContainerHeight: string;
  @ViewChild('downloadZipLink') private downloadZipLink: ElementRef;

  //------------------------------------------------------------
  //  CONSTRUCTOR
  //------------------------------------------------------------
  constructor(
    private modalService: NgbModal,
    private service: ApiService,
    private filterService: CommonFilterPaginationService,
    private router: Router,
    private renderer: Renderer2,
    private commonService: CommonService,
    private globalService:GlobalService
  ) {

  }

  //------------------------------------------------------------
  // INITIALIZATION
  //------------------------------------------------------------
  ngOnInit() {
    this.tableDetail = tableDetail;
    // this.ngxTableConfig['dataToDisplay'] = columnDetail;
    this.commonService.loadspinnerService.next(true);
    this.service.get(this.service.getEnvironment().get_listOf_catalog).subscribe(resp => {
      if (resp['status']) {
        this.dropDownDatas(resp);
        this.commonService.loadspinnerService.next(false);
      } else {
        this.commonService.loadspinnerService.next(false);
      }
    })
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + 'px';
      document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
      document.getElementById('card-body').children[0]['style'].height = '100%';
    }, 500);
  }


  //------------------------------------------------------------
  //  
  //------------------------------------------------------------

  dropDownDatas(resp?) {
    const catalogList = isNull(resp['response']) ? [] : resp['response'];
    catalogList.forEach(element => {
      this.catalogListing.push({ id: element.catalogId, name: element.catalogDesc, joinClause: element.joinClause })
      this.catalogFilterList.push({ id: element.catalogId, name: element.catalogDesc, joinClause: element.joinClause })
    });
  }

  sharableDet = {};
  sharable: boolean = false;
  startIndex: any = 1;
  lastIndex: any = 1000;
  selecteddata(type: any, shareView?) {
    if (type['type'] == 'share') {
      var obj = {
        type: 'DesignQuery',
        name: type['data'].vcqdQueryId,
        details: {
          macroVar: type['data'].vcqdQueryId,
          scriptType: 'D_QUERY'
        },
        apiLink: environment.get_dataConnector_shareDet,
        saveLinK: environment.post_saveDataSource,
      }
      this.sharableDet = obj;
      this.sharable = true;
      this.sharePopup(shareView, '');
    }
    if (type['type'] == 'execute') {
      let data = {
        "catalogId": type['data']['catalogId'],
        "reportId": type['data']['vcqdQueryId'],
        "startIndex": this.startIndex,
        "lastIndex": this.lastIndex,
      }
      this.commonService.loadspinnerService.next(true);
      this.reportPopup(data, 'execute')
      // this.service.post(environment.get_getHashVariableListForSavedReport, data).subscribe(resp => {
      //   if (resp['status']) {
      //     this.commonService.loadspinnerService.next(true);
      //   }
      //   else {
      //     this.commonService.showToastr.error[resp['message']]
      //   }
      // })
    }
    if (type['type'] == 'edit') {
      let reportDetails = {
        catalogId: type['data']['catalogId'],
        reportId: type['data']['vcqdQueryId']
      }
      this.commonService.loadspinnerService.next(true);
      this.service.post(environment.get_designAnalysis_Render, reportDetails).subscribe(resp => {

        if (resp['status']) {
          if (!isNull(resp['response'])) {
            this.dataFormation(resp['response'], reportDetails['catalogId']);
            this.commonService.loadspinnerService.next(false);
          }
        }
        else {
          this.commonService.loadspinnerService.next(false);
          this.globalService.showToastr.error(resp['message'])
        }
      })
    }
    if (type['type'] == 'delete') {
      this.commonService.loadspinnerService.next(true);
      let detail = {
        vcqdQueryId: type['data']['vcqdQueryId']
      };
      this.service.post(environment.delete_designAnalysis, detail).subscribe(resp => {
        if (resp['status']) {
          this.resetModel = Object.assign({}, { key: 'reload' });
          this.globalService.showToastr.success(resp["message"]);
          this.commonService.loadspinnerService.next(false);
          //need to implement
        }
        else
          this.globalService.showToastr.error(resp['message']);
        this.commonService.loadspinnerService.next(false);
      })
    }
    // if (type['type'] == 'routing') {
    //   this.router.navigate(['/welcome/interactive/manualquerylist'], { skipLocationChange: true })
    // }
    if (type['type'] == 'add') {
      this.openCatalog(this.catalogList)
    }
    if(type['type'] == 'refresh' || type['type'] == 'pagination'){
      this.resetModel = Object.assign({}, { key: 'reload' });
    }
    if(type['type'] == 'download'){
      let data = {
        "catalogId": type['data']['catalogId'],
        "reportId": type['data']['vcqdQueryId'],
        "startIndex": this.startIndex,
        "lastIndex": this.lastIndex,
      }
      this.commonService.loadspinnerService.next(true);
      this.reportPopup(data, 'download')
    }
  }

  sharePopup(shareView: any, apiLink: any) {
    this.modalService.open(shareView, {
      size: 'lg',
      backdrop: 'static',
      windowClass: 'share-popup'
    });
  }

  reportPopup(catlogDet: any, type) {
    if (type == 'execute') {
      let obj = {};
      obj['mainModel'] = catlogDet
      obj['hashArray'] = [];
      obj['hashValueArray'] = [];
      this.service.post(environment.get_designAnalysis_ExecuteSaved, obj).subscribe(resp => {
        if (resp['status']) {
          let totalRows = resp['otherInfo']['mainModel'].totalRows;
          let tableName = resp['otherInfo']['mainModel'].tableName;
          obj['reportFields'] = resp['otherInfo']['reportFields'];
          this.executePopup(resp['response'], obj, totalRows, tableName);
        }
        else {
          this.commonService.loadspinnerService.next(false);
          this.globalService.showToastr.error(resp['message'])
        }
      })
    }
    if(type == 'download'){
      let obj = {};
      obj['mainModel'] = catlogDet
      obj['hashArray'] = [];
      obj['hashValueArray'] = [];
      let filename=(!isNullOrUndefined(obj['mainModel']['reportId']) && obj['mainModel']['reportId'] !='')? `${obj['mainModel']['reportId']}.xlsx`:'Export.xlsx';
      this.service.fileDownloads(environment.get_designAnalysis_DownloadSaved+'/'+filename, obj).subscribe(resp => {
        const blob = new Blob([resp], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);
        const link = this.downloadZipLink.nativeElement;
        link.href = url;
        link.download =(!isNullOrUndefined(obj['mainModel']['reportId']) && obj['mainModel']['reportId'] !='') ? `${obj['mainModel']['reportId']}.xlsx`:'Export.xlsx';
        link.click();
        window.URL.revokeObjectURL(url);
    }, (error: any) => {
        if (error.substr(12, 3) == '204') {
        this.commonService.showToastr.error('No Records to Export');
        }
        if (error.substr(12, 3) == '420') {
        this.commonService.showToastr.error('Error Generating Report');
        }
        if (error.substr(12, 3) == '417') {
        this.commonService.showToastr.error('Error while exporting the report');
        }
    })

    }

    //ExecuteQueryComponent
    // const modelRef = this.modalService.open(ExecuteQueryComponent, {
    //   size: <any>'md',
    //   backdrop: 'static'
    // });
    // resp['mainModel'] = catlogDet;
    // resp['apiLink'] = environment.get_designAnalysis_ExecuteSaved;
    // modelRef.componentInstance.getDetails = resp;
    // modelRef.componentInstance.closeModelPopup.subscribe(res=>{
    //   modelRef.close();
    // })
  }

  executePopup(resp, obj, totalRows, tableName) {
    const modelRef = this.modalService.open(ExecuteQueryNewComponent, {
      size: <any>'lg',
      backdrop: 'static',
      windowClass: 'execute-query'
    });
    resp['reportId'] = obj['mainModel']['reportId'];
    resp['catalogId'] = obj['mainModel']['catalogId'];
    resp['totalRows'] = totalRows;
    resp['startIndex'] = this.startIndex;
    resp['lastIndex'] = this.lastIndex;
    resp['tableName'] = tableName;
    resp['mainModel'] = obj;
    resp['apilink'] = 'designAnalysis/executeSaved';
    modelRef.componentInstance.getDetails = resp;
    modelRef.componentInstance.closeModelPopup.subscribe(res => {
      modelRef.close();
    })
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  columnDetails(data: any, modal) {
    this.commonService.reportDetails.next({});
    this.modalService.dismissAll(modal);
    this.router.navigate(['/main/menu-maintenance/interactivecolumn'], { queryParams: { data: JSON.stringify(data) }, skipLocationChange: true })
  }


  openCatalog(x) {
    this.modalService.open(x, {
      windowClass: 'xlModal',
      backdrop: false
    });
  }

  closeModel() {
    this.modalService.dismissAll();
    this.catalogListing = this.catalogFilterList;
  }

  filterData(event: any) {
    let fillter_target = event.target.value;
    let items = this.catalogFilterList;
    items = items.filter(element => {
      if (element.name.toLowerCase().indexOf(fillter_target.toLowerCase()) != -1 || element.id.toLowerCase().indexOf(fillter_target.toLowerCase()) != -1) {
        return true;
      }
    });
    this.catalogListing = items;
  }



  dataFormation(resp: any, catalogId: any) {
    let modalData = !isNull(resp['mainModel']) ? resp['mainModel'] : {};
    this.reportModel['reportId'] = !isNull(modalData['reportId']) ? modalData['reportId'] : '',
    this.reportModel['reportDescription'] = !isNull(modalData['reportDescription']) ? modalData['reportDescription'] : '';
    this.reportModel['distinctFlag'] = modalData['distinctFlag'] == 'Y' ? true : false;
    this.loadQueryDetails(resp, catalogId);
  }

  loadQueryDetails(resp: any, catalogId: any) {
    let QueryArray = !isNull(resp['reportFields']) ? resp['reportFields'] : [];
    QueryArray.forEach(element => {
      let dummy_data = {
        alias: element.alias,
        colId: element.colId,
        fields: element.colName,
        colType: element.colType,
        formatType: element.formatTypeDesc,
        dateConversionSyntax: element.dateConversionSyntax,
        dateFormattingSyntax: element.dateFormattingSyntax,
        colDisplayType: element.colDisplayType,
        display: element.displayFlag == 'Y' ? true : false,
        uiqueIdentifier: element.tabelId + '_' + element.colId,
        aliasBackup: element.alias,
        creteriaDropDown: (element.colDisplayType != 'C' && element.colDisplayType != 'N') ? creteria_con['notForCN'] : creteria_con['forCN'],
        aggrigateDropDown: (element.colDisplayType == 'D') ? creteria_con['forDateAgg'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCNAgg'] : creteria_con['notforCNAgg'],
        summeryDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
        group: element.groupBy == 'Y' ? true : false,
        joinData: !isNull(element.joinCondition) ? element.joinCondition : 'And',
        //joinData: 'And',
        dateConfig: (element.colDisplayType == 'D') ? this.configDate(element.creteria) : '',
        type_txt: element.colDisplayType == 'N' || element.colDisplayType == 'C' ? 'number' : 'text',
        tableName: element.tableName,
        promptValueCheck: false,

        creteria: !isNull(element.operator) ? element.operator : '',
        aggrigate:!isNull(element.aggFunction) ? element.aggFunction : '',
        summeryFilter:!isNull(element.summaryCriteria) ? element.summaryCriteria : '',
        sortType: !isNull(element.sortType) ? element.sortType : '',
        startData: !isNull(element.value1) ? element.value1 : '',
        endData: !isNull(element.value2) ? element.value2 : '',
        endDataDisTog: (element.colDisplayType == 'D') ? 'date' : 'plain',
        startDataDisTog: (element.colDisplayType == 'D') ? 'date' : 'plain',
        startDataTog: !isNull(element.value1) ? element.value1.length ? true : false : false,
        endDataTog: !isNull(element.value2) ? element.value2.length ? true : false : false,

        summeryStartData:!isNull(element.summaryValue1 ) ? element.summaryValue1  : '',
        summeryEndData: !isNull(element.summaryValue2 ) ? element.summaryValue2  : '',

        summeryEndDataDisTog:  (element.colDisplayType == 'D') ? 'date' : 'plain',
        summeryStartDataDisTog:(element.colDisplayType == 'D') ? 'date' : 'plain',
        summeryStartDataTog: !isNull(element.summaryValue1) ? element.summaryValue1.length ? true : false : false,
        summeryEndDataTog: !isNull(element.summaryValue2) ? element.summaryValue2.length ? true : false : false,

        multiple: element.operator == 'in' ? true : false,
        tableId: element.tabelId,
        action: 'delete',
        tableAliasName: element.tabelAliasName,
        colExperssionType: element.colExpressionType,
        whereCondition: element.colType == 'D' && (element.colExperssionType == "DBSQL" || element.colExperssionType == "EXP") ? false : true,
        magType: element.magType,
        magSelectionType: element.magSelectionType,
        includeGroupCol: element.includeGroupCol,
        magQueryId: element.magQueryId,
        maskingFlag: element.maskingFlag,
        magEnableFlag: element.magEnableFlag,
        displayColumn: element.magDisplayColumn,
        useColumn: element.magUseColumn,
        join: false,
        magnifierValues: {},
        experssionText: element.experssionText,
        scalingFlag: element.scalingFlag == 'Y' ? true : '',
        scalingFormat: element.scalingFormat,
        numberFormat: element.numberFormat == 'Y' ? true : '',
        decimalFlag: element.decimalFlag == 'Y' ? true : '',
        decimalCount: element.decimalCount,
        dynamicStartFlag: element.dynamicStartFlag == 'Y' ? true : false,
        dynamicEndFlag: element.dynamicEndFlag == 'Y' ? true : false,
        // dynamicDateFormat: element.formatType,
        dynamicStartDate: element.dynamicStartDate,
        dynamicEndDate: element.dynamicEndDate,
        javaFormatDesc: element.javaFormatDesc,
        dynamicStartOperator: (element.dynamicStartFlag == 'Y') ? element.dynamicStartOperator.toString().substr(0, 1) : '',
        dy_start_op_val: (element.dynamicStartFlag == 'Y') ? element.dynamicStartOperator.toString().substr(1, 4) : '',
        dynamicEndOperator: (element.dynamicEndFlag == 'Y') ? element.dynamicEndOperator.toString().substr(0, 1) : '',
        dy_end_op_val: (element.dynamicEndFlag == 'Y') ? element.dynamicEndOperator.toString().substr(1, 4) : '',

        summeryStartFlag: element.summaryDynamicStartFlag == 'Y' ? true : false,
        summeryEndFlag: element.summaryDynamicEndFlag == 'Y' ? true : false,
        // dynamicDateFormat: element.formatType,
        summeryStartDate: element.summaryDynamicStartDate,
        summeryEndDate: element.summaryDynamicEndDate,

        summeryStartOperator: (element.summaryDynamicStartFlag == 'Y') ? element.summaryDynamicStartOperator.toString().substr(0, 1) : '',
        sum_start_op_val: (element.summaryDynamicStartFlag == 'Y') ? element.summaryDynamicStartOperator.toString().substr(1, 4) : '',
        summeryEndOperator: (element.summaryDynamicEndFlag == 'Y') ? element.summaryDynamicEndOperator.toString().substr(0, 1) : '',
        sum_end_op_val: (element.summaryDynamicEndFlag == 'Y') ? element.summaryDynamicEndOperator.toString().substr(1, 4) : '',
      }
      this.uniqueTree(dummy_data);
      this.magnifierAPICall(dummy_data);

    });

    this.reportModel['reportFields'] = {
      'originalList': this.draggedTree,
      'dummyList': this.dummyTree
    }

    this.reportModel['hashArray'] = resp['hashArray'];
    this.reportModel['hashValueArray'] = resp['hashValueArray']
    this.reportModel['retrive'] = true;
    this.commonService.reportDetails.next(this.reportModel);
    let catalog: any = {};
    this.catalogFilterList.forEach(element => {
      if (element.id == catalogId) {
        catalog = element
      }
    });
    if (Object.keys(catalog).length) {
      this.router.navigate(['/main/menu-maintenance/interactivecolumn'], { queryParams: { data: JSON.stringify(catalog) }, skipLocationChange: true })
    }
    else {
      this.globalService.showToastr.error('Invalid configration contact system Admin..')
    }
  }

  configDate(creteria: any) {
    let config = {};
    if (creteria == 'in') {
      config['allowMultiSelect'] = true;
      config['format'] = "DD-MMM-YYYY";
    }
    else {
      config['format'] = "DD-MMM-YYYY";
    };
    return config;
  }

  uniqueTree(dummy_obj) {
    this.dummyTree.push(dummy_obj);
    if (!this.draggedTree.some(element_1 => element_1.alias == dummy_obj.alias)) {
      let uniquId = Object.assign({}, dummy_obj);

      dummy_obj.uniqueId = uniquId.alias + '_' + 0;
      dummy_obj.aliasBackup = dummy_obj.alias;
      this.draggedTree.push(dummy_obj);
    }
    else {
      this.arrayCount(Object.assign({}, dummy_obj))
    }
  }

  arrayCount(obj: any) {
    let count = 0;
    this.dummyTree.forEach(element => {
      if (element.alias == obj.alias) {
        count++;
      }
    })
    count -= 1;
    obj.alias = obj.alias + '_' + count;
    obj.uniqueId = obj.alias + '~' + count;
    obj.aliasBackup = obj.alias;
    this.uniqueTree(obj)
  }

  magnifierAPICall(element) {

    if (element.magEnableFlag == 'Y' && element.magQueryId != null) {

      let obj = {
        "queryId": element.magQueryId,
        "useColumn": element.useColumn,
        "displayColumn": element.displayColumn
      }
      this.service.post(environment.get_designAnalysis_magData, obj).subscribe(resp => {
        if (resp['status']) {
          element['magnifierValues'] = {
            key: element.useColumn,
            value: element.displayColumn,
            dropdown: resp["response"]
          }

          if (element.magType == 1) {
            element['startDataDisTog'] = "dropdown";
            element['endDataDisTog'] = "dropdown";
          }
        }
      });
      if (element.magType == 2) {

        element['startDataDisTog'] = "popup";
        element['endDataDisTog'] = "popup";
      }
    }
    else {
      if (element['colDisplayType'] != 'D') {
        element['startDataDisTog'] = "plain";
        element['endDataDisTog'] = "plain";
      }
      else {
        element['startDataDisTog'] = "date";
        element['endDataDisTog'] = "date";
      }
    }
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
  }

}





