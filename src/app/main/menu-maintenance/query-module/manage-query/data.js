export const tableDetail = {
  headers: [
    {
      name: "Catalog ID",
      referField: "catalogId",
      filter: false,
      isFilterField: true,
      // style:{
      //     width:"3%"
      // },
    },
    {
      name: "Query ID",
      referField: "vcqdQueryId",
      filter: false,
      isFilterField: true,
      // style:{
      //     width:"3%"
      // },
    },
    {
      name: "Description",
      referField: "vcqdQueryDesc",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Date Created",
      referField: "dateCreation",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Date Modified",
      referField: "dateLastModified",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Maker",
      referField: "makerName",
      filter: false,
      isFilterField: true,
    },
  ],
  secheaders: [],
  options: {
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: true,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "edit",
      icon: "fa fa-pencil",
      actionType: "edit",
    },
    {
      name: "share",
      icon: "fa fa-share",
      actionType: "share",
    },
    {
      name: "download",
      icon: "fa fa-download",
      actionType: "download",
    },
    {
      name: "execute",
      icon: "fa fa-check-circle",
      actionType: "execute",
    },
    {
      name: "delete",
      icon: "fa fa-trash-o",
      actionType: "delete",
    },
  ],
};
