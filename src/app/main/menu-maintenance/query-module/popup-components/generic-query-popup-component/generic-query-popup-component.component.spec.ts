import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericQueryPopupComponent } from './generic-query-popup-component.component';

describe('GenericQueryPopupComponentComponent', () => {
  let component: GenericQueryPopupComponent;
  let fixture: ComponentFixture<GenericQueryPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericQueryPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericQueryPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
