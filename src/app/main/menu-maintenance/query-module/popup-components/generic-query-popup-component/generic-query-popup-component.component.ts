import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-generic-query-popup-component',
  templateUrl: './generic-query-popup-component.component.html',
  styleUrls: ['./generic-query-popup-component.component.css']
})
export class GenericQueryPopupComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() popupType: string = 'alert';
  @Output() userConfirmation: EventEmitter<any> = new EventEmitter<any>();

  constructor(private modalService: NgbModal,
    ) { }

  ngOnInit() {
  }

  userCofirm(e) {
    this.userConfirmation.emit(e);
  }

}
