import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isNull } from 'util';
import { report_Query } from '../../modals/report';
import { ApiService } from 'src/app/service';
import { environment } from 'src/environments/environment';

import { creteria_con } from '../../modals/creteria';
import { CommonService } from 'src/app/service/common.service';


@Component({
  selector: 'app-load-data',
  templateUrl: './load-data.component.html',
  styleUrls: ['./load-data.component.css']
})
export class LoadDataComponent implements OnInit, AfterViewInit {

  @Input() reportList;
  @Output() queryList = new EventEmitter();
  dataToDisplay = [];
  dummyTree: any = [];
  dataLoaded: boolean = false;
  reportFilterList: any = [];
  draggedTree: any = [];
  reportModel: any = {};


  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  constructor(
    private modalService: NgbModal,
    private service: ApiService,

    private commonService: CommonService
  ) {

  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  ngOnInit() {
    this.dataToDisplay = report_Query;
    this.reportList = isNull(this.reportList) ? [] : this.reportList;
    this.reportFilterList = [...this.reportList];
    this.dataLoaded = true;
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  ngAfterViewInit() {

  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  closeModel() {
    this.modalService.dismissAll();
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  filterData(event: any) {
    let fillter_target = event.target.value;
    let items = this.reportFilterList;
    items = items.filter(element => {
      if (element.reportId.toLowerCase().indexOf(fillter_target.toLowerCase()) != -1 || element.reportDescription.toLowerCase().indexOf(fillter_target.toLowerCase()) != -1) {
        return true;
      }
    });
    this.reportList = items;
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  columnDetails(report: any, reportlist) {

    let repo = {
      reportId: report.reportId,
      catalogId: report.catalogId
    }
    this.service.post(environment.get_designAnalysis_Render, repo).subscribe(resp => {
      if (resp['status']) {

        if (!isNull(resp['response'])) {
          this.dataFormation(resp['response'])
        }
      }
      else {

        this.commonService.showToastr.error(resp['message'])
      }
    })
    // get_designAnalysis_Render
  }


  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  dataFormation(resp: any) {
    let modalData = !isNull(resp['mainModel']) ? resp['mainModel'] : {};
    this.reportModel['hashArray'] = !isNull(resp['hashArray']) ? resp['hashArray'] : [];
    this.reportModel['hashValueArray'] = !isNull(resp['hashArray']) ? resp['hashArray'] : [];
    this.reportModel['mainModel'] = {
      'select': !isNull(modalData['select']) ? modalData['select'] : '',
      'where': !isNull(modalData['where']) ? modalData['where'] : '',
      'groupBy': !isNull(modalData['groupBy']) ? modalData['groupBy'] : '',
      'orderBy': !isNull(modalData['orderBy']) ? modalData['orderBy'] : '',
      'reportId': !isNull(modalData['reportId']) ? modalData['reportId'] : '',
      'reportDescription': !isNull(modalData['reportDescription']) ? modalData['reportDescription'] : '',
      'distinctFlag' :modalData['distinctFlag'] == 'Y' ? true : false,
    }
    this.loadQueryDetails(resp['reportFields']);
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  loadQueryDetails(resp: any) {
    let QueryArray = !isNull(resp) ? resp : [];
    QueryArray.forEach((element) => {
      let dummy_data = {
        alias: element.alias,
        colId: element.colId,
        fields: element.colName,
        colType: element.colType,
        formatType: element.formatTypeDesc,
        dateConversionSyntax: element.dateConversionSyntax,
        dateFormattingSyntax: element.dateFormattingSyntax,
        includeGroupCol: element.includeGroupCol,
        colDisplayType: element.colDisplayType,
        display: element.displayFlag == 'Y' ? true : false,
        uiqueIdentifier: element.tabelId + '_' + element.colId,
        aliasBackup: element.alias,
        creteriaDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
        aggrigateDropDown: (element.colDisplayType == 'D') ? creteria_con['forDateAgg'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCNAgg'] : creteria_con['notforCNAgg'],
        summeryDropDown: (element.colDisplayType == 'D') ? creteria_con['forDate'] : (element.colDisplayType == 'C' || element.colDisplayType == 'N') ? creteria_con['forCN'] : creteria_con['notForCN'],
        group: element.groupBy == 'Y' ? true : false,
        joinData: !isNull(element.joinCondition) ? element.joinCondition : 'And',
        dateConfig: (element.colDisplayType == 'D') ? this.configDate(element.creteria) : '',
        type_txt: element.colDisplayType == 'N' || element.colDisplayType == 'C' ? 'number' : 'text',
        tableName: element.tableName,

        creteria: !isNull(element.operator) ? element.operator : '',
        aggrigate:!isNull(element.aggFunction) ? element.aggFunction : '',
        summeryFilter:!isNull(element.summaryCriteria) ? element.summaryCriteria : '',
        sortType: !isNull(element.sortType) ? element.sortType : '',
        startData: !isNull(element.value1) ? element.value1 : '',
        endData: !isNull(element.value2) ? element.value2 : '',
        endDataDisTog: (element.colDisplayType == 'D') ? 'date' : 'plain',
        startDataDisTog: (element.colDisplayType == 'D') ? 'date' : 'plain',
        startDataTog: !isNull(element.value1) ? element.value1.length ? true : false : false,
        endDataTog: !isNull(element.value2) ? element.value2.length ? true : false : false,

        summeryStartData:!isNull(element.summaryValue1 ) ? element.summaryValue1  : '',
        summeryEndData: !isNull(element.summaryValue2 ) ? element.summaryValue2  : '',
        summeryEndDataDisTog:  (element.colDisplayType == 'D') ? 'date' : 'plain',
        summeryStartDataDisTog:(element.colDisplayType == 'D') ? 'date' : 'plain',
        summeryStartDataTog: !isNull(element.summaryValue1) ? element.summaryValue1.length ? true : false : false,
        summeryEndDataTog: !isNull(element.summaryValue2) ? element.summaryValue2.length ? true : false : false,

        multiple: element.operator == 'in' ? true : false,
        tableId: element.tabelId,
        action: 'delete',
        tableAliasName: element.tabelAliasName,
        colExperssionType: element.colExpressionType,
        whereCondition: element.colType == 'D' && (element.colExperssionType == "DBSQL" || element.colExperssionType == "EXP") ? false : true,
        magType: element.magType,
        magSelectionType: element.magSelectionType,
        magQueryId: element.magQueryId,
        maskingFlag: element.maskingFlag,
        magEnableFlag: element.magEnableFlag,
        displayColumn: element.magDisplayColumn,
        useColumn: element.magUseColumn,
        magnifierValues: {},
        join: false,
        experssionText: element.experssionText,
        scalingFlag: element.scalingFlag == 'Y' ? true : '',
        scalingFormat: element.scalingFormat,
        numberFormat: element.numberFormat == 'Y' ? true : '',
        decimalFlag: element.decimalFlag == 'Y' ? true : '',

        decimalCount: element.decimalCount, dynamicStartFlag: element.dynamicStartFlag == 'Y' ? true : false,
        dynamicEndFlag: element.dynamicEndFlag == 'Y' ? true : false,
        // dynamicDateFormat: element.formatType,        
        dynamicStartDate: element.dynamicStartDate,
        dynamicEndDate: element.dynamicEndDate,
        javaFormatDesc: element.javaFormatDesc,
        dynamicStartOperator: (element.dynamicStartFlag == 'Y') ? element.dynamicStartOperator.toString().substr(0, 1) : '',
        dy_start_op_val: (element.dynamicStartFlag == 'Y') ? element.dynamicStartOperator.toString().substr(1, 4) : '',
        dynamicEndOperator: (element.dynamicEndFlag == 'Y') ? element.dynamicEndOperator.toString().substr(0, 1) : '',
        dy_end_op_val: (element.dynamicEndFlag == 'Y') ? element.dynamicEndOperator.toString().substr(1, 4) : '',
        summeryStartFlag: element.summaryDynamicStartFlag == 'Y' ? true : false,
        summeryEndFlag: element.summaryDynamicEndFlag == 'Y' ? true : false,
        // dynamicDateFormat: element.formatType,
        summeryStartDate: element.summaryDynamicStartDate,
        summeryEndDate: element.summaryDynamicEndDate,

        summeryStartOperator: (element.summaryDynamicStartFlag == 'Y') ? element.summaryDynamicStartOperator.toString().substr(0, 1) : '',
        sum_start_op_val: (element.summaryDynamicStartFlag == 'Y') ? element.summaryDynamicStartOperator.toString().substr(1, 4) : '',
        summeryEndOperator: (element.summaryDynamicEndFlag == 'Y') ? element.summaryDynamicEndOperator.toString().substr(0, 1) : '',
        sum_end_op_val: (element.summaryDynamicEndFlag == 'Y') ? element.summaryDynamicEndOperator.toString().substr(1, 4) : '',
      }
      this.uniqueTree(dummy_data);
      this.magnifierAPICall(dummy_data);
    });

    this.reportModel['reportFields'] = {
      'originalList': this.draggedTree,
      'dummyList': this.dummyTree
    }
    this.queryList.emit(this.reportModel);
  }


  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  configDate(creteria: any) {
    let config = {};
    if (creteria == 'in') {
      config['allowMultiSelect'] = true;
      config['format'] = "DD-MMM-YYYY";
    }
    else {
      config['format'] = "DD-MMM-YYYY";
    };
    return config;
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  uniqueTree(dummy_obj) {
    this.dummyTree.push(dummy_obj);
    if (!this.draggedTree.some(element_1 => element_1.alias == dummy_obj.alias)) {
      let uniquId = Object.assign({}, dummy_obj);
      dummy_obj.uniqueId = uniquId.alias + '_' + 0;
      dummy_obj.aliasBackup = dummy_obj.alias;
      this.draggedTree.push(dummy_obj);
    }
    else {
      this.arrayCount(Object.assign({}, dummy_obj))
    }
  }

  //------------------------------------------------------------
  //  
  //------------------------------------------------------------
  arrayCount(obj: any) {
    let count = 0;
    this.dummyTree.forEach(element => {
      if (element.alias == obj.alias) {
        count++;
      }
    })
    count -= 1;
    obj.alias = obj.alias + '_' + count;
    obj.uniqueId = obj.alias + '~' + count;
    obj.aliasBackup = obj.alias;
    this.uniqueTree(obj)
  }


  magnifierAPICall(element) {

    if (element.magEnableFlag == 'Y' && element.magQueryId != null) {

      let obj = {
        "queryId": element.magQueryId,
        "useColumn": element.useColumn,
        "displayColumn": element.displayColumn
      }
      this.service.post(environment.get_designAnalysis_magData, obj).subscribe(resp => {
        if (resp['status']) {
          element['magnifierValues'] = {
            key: element.useColumn,
            value: element.displayColumn,
            dropdown: resp["response"]
          }

          if (element.magType == 1) {
            element['startDataDisTog'] = "dropdown";
            element['endDataDisTog'] = "dropdown";
          }
        }
      });
      if (element.magType == 2) {

        element['startDataDisTog'] = "popup";
        element['endDataDisTog'] = "popup";
      }
    }
    else {
      if (element['colDisplayType'] != 'D') {
        element['startDataDisTog'] = "plain";
        element['endDataDisTog'] = "plain";
      }
      else {
        element['startDataDisTog'] = "date";
        element['endDataDisTog'] = "date";
      }
    }
  }

}
