import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagnifierPopupComponent } from './magnifier-popup.component';

describe('MagnifierPopupComponent', () => {
  let component: MagnifierPopupComponent;
  let fixture: ComponentFixture<MagnifierPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MagnifierPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagnifierPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
