import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { isArray, isNull } from "util";
import { keyword_con } from '../../modals/keyword'

@Component({
  selector: "app-show-query",
  templateUrl: "./show-query.component.html",
  styleUrls: ["./show-query.component.css"]
})
export class ShowQueryComponent implements OnInit {
  //------------------------------------------------------------
  // PUBLIC VARIABLES AND DECLARATIONS
  //------------------------------------------------------------
  fromCondition: any = "";
  joinCondition: any = "";
  joinClause: any = "";
  @Output() qyueryToShow = new EventEmitter();
  @Input() getDetails;
  distinctFlag: any;
  @Input() set getJoinsDetail(detail) {
    this.fromCondition = detail.fromCondition;
    this.joinCondition = detail.joinCondition;
    this.joinClause = detail.joinClause;
    this.distinctFlag= detail.distinctCondition;
  }
  query = "";
  // getDetails: any = [];
  // overLayQuery: any = [];

  //------------------------------------------------------------
  // CONSTRUCTOR
  //------------------------------------------------------------
  constructor() { }

  //------------------------------------------------------------
  // INITIALIZATION
  //------------------------------------------------------------
  ngOnInit() {
    this.queryFormation();
  }

  //------------------------------------------------------------
  // CLOSE MODAL
  //------------------------------------------------------------
  closeModal() {
    this.qyueryToShow.emit(true);
  }

  //------------------------------------------------------------
  // QUERY FORMATION
  //------------------------------------------------------------
  queryFormation() {
    this.query = "SELECT ";
    this.query = this.distinctFlag ? this.query + 'DISTINCT ':this.query;
    let data = "";
    let creteriaCount = 0;
    let aggrigateCount = 0;
    let groupByCount = 0;
    let summeryCount=0
    let orderByCount = 0;
    let overAllSelect = 0;
    for (var i = 0; i < this.getDetails.length; i++) {
      let creteria = this.getDetails[i]["creteria"];
      let aggrigate = this.getDetails[i]["aggrigate"];
      let summery = this.getDetails[i]["summeryFilter"];
      if (this.getDetails[i].display) {
        data = data + this.aggregateFunctionality(aggrigate, i, "fields");
        //data = data + this.aggregateFunctionality(creteria, i, "fields");
        data = data + " AS " + this.getDetails[i]["alias"].replaceAll(" ", "_") + " , ";
      }

      creteria != "" && this.creteriaCondition(creteria) ? creteriaCount++ : "";
      aggrigate != "" && this.creteriaCondition(aggrigate) ? aggrigateCount++ : "";
      summery != "" && this.creteriaCondition(summery) ? summeryCount++ : "";

      this.getDetails[i]["group"] ? groupByCount++ : "";
      this.getDetails[i]["sortType"] ? orderByCount++ : "";
      this.getDetails[i]["colType"] == "D" &&
        !isNull(this.getDetails[i]["experssionText"])
        ? overAllSelect++
        : "";
    }
    data = data.slice(0, -2) + " FROM " + this.fromCondition;
    this.query = this.query + data;
    creteriaCount || this.joinCondition.length
      ? this.whereCondition(creteriaCount)
      : "";
    groupByCount ? this.groupByCondition() : "";
    summeryCount ? this.havingCondition(summeryCount) : "";
    orderByCount ? this.orderByCondition() : "";
    let QueryData = this.query.split(" ");
    QueryData.forEach((element, index, array) => {
      if (element.length) {
        if (keyword_con.includes(element)) {
          array[index] = `<code> ${element} </code>`
        }
        else {
          array[index] = `<span> ${element} </span>`
        }
      }
    })
    this.query = QueryData.join(" ");
    // overAllSelect ? this.overAllSelect() : '';
  }

  //------------------------------------------------------------
  // WHERE CONDITION
  //------------------------------------------------------------
  whereCondition(count: any) {
    let Wherequery = " WHERE ";
    Wherequery = Wherequery + this.standerdJoinConditiion(count);
    for (var i = 0; i < this.getDetails.length; i++) {
      //  if (this.getDetails[i].display) {
      this.getDetails[this.getDetails.length - 1]["joinData"] = "";
      let creteria = this.getDetails[i]["creteria"];
      if (creteria != "" && this.creteriaCondition(creteria) && !isNull(creteria)) {
        // let col_type_D = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE( ${this.getDetails[i]["tableAliasName"] + "." + this.getDetails[i]["fields"]}, 'DD-MON-RRRR')` : this.getDetails[i]["tableAliasName"] + "." + this.getDetails[i]["fields"];
        let col_type_D = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#', `${this.getDetails[i]["tableAliasName"] + "." + this.getDetails[i]["fields"]}`) : this.getDetails[i]["tableAliasName"] + "." + this.getDetails[i]["fields"];
        Wherequery = Wherequery + col_type_D + " ";
        Wherequery = Wherequery + `${this.getDetails[i]["creteria"]}` + " ";
        if (this.getDetails[i]["creteria"] == "between") {
          this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#',`'${this.getDetails[i]['startData']}'`) : `'${this.getDetails[i]['startData']}'`;
          this.getDetails[i]['endData'] = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#',`'${this.getDetails[i]['endData']}'`) : `'${this.getDetails[i]['endData']}'`;
          Wherequery = Wherequery +
            `${this.getDetails[i]["startData"]}` +
            " AND " +
            `${this.getDetails[i]["endData"]}` + " ";
        } else if (this.getDetails[i]["creteria"] == "in" || this.getDetails[i]["creteria"] == "not in") {
          let arra_data = this.getDetails[i]["startData"];
          let new_fomation = "";
          new_fomation = Array.isArray(arra_data)
            ? this.arrayToString(arra_data)
            : this.inDateFormat(arra_data, this.getDetails[i]['colDisplayType'], this.getDetails[i]['dateConversionSyntax']);

          Wherequery = Wherequery + " ( ";
          Wherequery = Wherequery + new_fomation + " ) ";
        } else if (
          this.getDetails[i]["creteria"] == "like" ||
          this.getDetails[i]["creteria"] == "not like"
        ) {
          let arra_data = Array.isArray(this.getDetails[i]["startData"]) ? this.getDetails[i]["startData"].join() : this.getDetails[i]["startData"];
          arra_data = (!arra_data.startsWith("%") && !arra_data.endsWith("%")) ? ` '%${arra_data}%' ` : `'${arra_data}' `;
          // arra_data = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE(${arra_data}, 'DD-MON-RRRR') ` : arra_data;
          Wherequery = Wherequery + arra_data;
        } else if (
          this.getDetails[i]["creteria"] != "between" &&
          this.getDetails[i]["creteria"] != "is not null" &&
          this.getDetails[i]["creteria"] != "is null" &&
          this.getDetails[i]["creteria"] != "in" &&
          this.getDetails[i]["creteria"] != "not in" &&
          this.getDetails[i]["creteria"] != "like" &&
          this.getDetails[i]["creteria"] != "not like"
        ) {
          // this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE( '${this.getDetails[i]['startData']}', 'DD-MON-RRRR')` : `'${this.getDetails[i]['startData']}'`;
          this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#', `'${this.getDetails[i]['startData']}'`) : `'${this.getDetails[i]['startData']}'`;
          Wherequery = Wherequery + " ";
          Wherequery = Wherequery + `${this.getDetails[i]["startData"]}` + " ";
        }

        if (this.getDetails[i]["join"]) {
          Wherequery = Wherequery + `${this.getDetails[i]["joinData"]}`;
        }
        Wherequery = Wherequery + " ";
      } else {
        if (this.getDetails[i]["join"]) {
          Wherequery = Wherequery + `${this.getDetails[i]["joinData"]}` + " '";
        }
      }
      //  }
    }
    if (Wherequery.length > 7) {
      this.query = this.query + Wherequery;
    }
  }

  //------------------------------------------------------------
  // 
  //------------------------------------------------------------
  inDateFormat(arra_data, colDisplayType: any, conversion: any) {
    let data_set = '';
    let array_set = arra_data.split(" | ");
    if (colDisplayType == 'D') {
      array_set.forEach((element, index, array) => {        
        // array[index] = `'${element}'`;
        array[index] =conversion.replace('#VALUE#',`'${element}'`);
      });
      data_set = array_set.join(',');
    }
    else {
      let aar = arra_data.split(",");
      aar.forEach((element, ind, arr) => {
        if (element[0] == "'" && element[element.length - 1] == "'") {
          // arr[ind] = `${element}`
          arr[ind] =conversion.replace('#VALUE#',`'${element}'`);
        } else {
          // arr[ind] = `'${element}'`
          arr[ind] =conversion.replace('#VALUE#',`'${element}'`);
        }
      });
      arra_data = aar.join(",")
      data_set = `${arra_data}`;
    }
    return data_set;
  }


  arrayToString(arra_data: any) {
    let cmb_array = "";
    arra_data.forEach(element => {
      cmb_array = cmb_array + " " + this.commaConditionCheck(element) + ",";
    });
    cmb_array = cmb_array.substr(0, cmb_array.length - 1);
    return cmb_array;
  }


  commaConditionCheck(element) {
    if (element[0] == "'" && element[element.length - 1] == "'") {
      element = element
    }
    if (element[0] != "'" && element[element.length - 1] != "'") {
      element = `'${element}'`
    }
    return element;
  }

  standerdJoinConditiion(count) {
    let standerd_JoinConditiion = "";
    if (count == 0 && this.joinCondition.length) {
      standerd_JoinConditiion += standerd_JoinConditiion + this.joinCondition;
    } else if (count != 0 && this.joinCondition.length == 0) {
      standerd_JoinConditiion += standerd_JoinConditiion + "";
    } else if (count != 0 && this.joinCondition.length) {
      standerd_JoinConditiion +=
        standerd_JoinConditiion + this.joinCondition + " AND ";
    }
    return standerd_JoinConditiion;
  }

  // arrayToString(arra_data: any) {
  //   let cmb_array = "";
  //   arra_data.forEach(element => {
  //     cmb_array = cmb_array + " '" + element + "' ,";
  //   });
  //   cmb_array = cmb_array.substr(0, cmb_array.length - 1);
  //   return cmb_array;
  // }

  //------------------------------------------------------------
  // GROUP BY CONDITION
  //------------------------------------------------------------

  groupByCondition() {
    let groupBy = " GROUP BY ";
    let grouped_columns = [];
    for (var i = 0; i < this.getDetails.length; i++) {
      if (this.getDetails[i].display) {
        let creteria = this.getDetails[i]["aggrigate"];
        if (this.creteriaCondition(creteria)) {
          if (this.columnTypeCondition(i)) {
            if (
              this.getDetails[i]["colType"] == "D" &&
              this.getDetails[i]["colExperssionType"] == "EXP" &&
              !isNull(this.getDetails[i]["experssionText"])
            ) {
              if (
                !grouped_columns.includes(
                  this.getDetails[i]["tableName"] +
                  "_" +
                  this.getDetails[i]["fields"] +
                  "_" +
                  this.creteriaCondition(creteria)
                )
              )
                groupBy =
                  groupBy + " " + this.getDetails[i]["experssionText"] + ", ";
            } else {
              if (
                !grouped_columns.includes(
                  this.getDetails[i]["tableName"] +
                  "_" +
                  this.getDetails[i]["fields"] +
                  "_" +
                  this.creteriaCondition(creteria)
                )
              )
                groupBy =
                  groupBy +
                  " " +
                  this.getDetails[i]["tableAliasName"] +
                  "." +
                  this.getDetails[i]["fields"] +
                  ", ";
            }
          }
          else{
            groupBy =
            groupBy +
            " " +
            this.getDetails[i]["includeGroupCol"] +
            ", ";
          }
        } else {
        }
        grouped_columns.push(
          this.getDetails[i]["tableName"] +
          "_" +
          this.getDetails[i]["fields"] +
          "_" +
          this.creteriaCondition(creteria)
        );
      }
    }
    groupBy = groupBy.slice(0, -2);

    if (groupBy.length > 7) this.query = this.query + groupBy;

  }
  havingCondition(summeryCount){
    let having = " HAVING ";
    let having_array=[]
    for (var i = 0; i < this.getDetails.length; i++) {
      let aggrigate = this.getDetails[i]["aggrigate"];
      if (this.getDetails[i].display && !this.creteriaCondition(aggrigate) && this.getDetails[i].summeryFilter != '') {
        having_array.push(this.getDetails[i])
      }
    }
    if(having_array.length){
      for (var i = 0; i < having_array.length; i++) {
        let aggrigate = having_array[i]["aggrigate"];
        having = having + this.aggregateFunctionalityForHaving(aggrigate, i, "fields",having_array) + this.getSummeryFilter(aggrigate,i,having_array)+this.andString(i,having_array);
      }
    }

    this.query = this.query + having;
  }

  andString(i,having_array){
    let returnData=''
    if(i < having_array.length-1)
      returnData=returnData+' AND ';
    else
      returnData='';
    return  returnData
  }
  getSummeryFilter(creteria,i,having_array){
    let returnData=''
    if (!this.creteriaCondition(creteria)) {
      if (having_array[i]["summeryFilter"] == "between") {
        having_array[i]['summeryStartData'] = having_array[i]['colDisplayType'] == 'D' ? having_array[i]['dateConversionSyntax'].replace('#VALUE#',`'${having_array[i]['summeryStartData']}'`) : `'${having_array[i]['summeryStartData']}'`;
        having_array[i]['summeryEndData'] = having_array[i]['colDisplayType'] == 'D' ? having_array[i]['dateConversionSyntax'].replace('#VALUE#',`'${having_array[i]['summeryEndData']}'`) : `'${having_array[i]['summeryEndData']}'`;
        returnData = returnData +
          `${having_array[i]["summeryStartData"]}` +
          " AND " +
          `${having_array[i]["summeryEndData"]}` + " ";
      } else if (having_array[i]["summeryFilter"] == "in" || having_array[i]["summeryFilter"] == "not in") {
        let arra_data = having_array[i]["summeryStartData"];
        let new_fomation = "";
        new_fomation = Array.isArray(arra_data)
          ? this.arrayToString(arra_data)
          : this.inDateFormat(arra_data, having_array[i]['colDisplayType'], having_array[i]['dateConversionSyntax']);
  
        returnData = returnData + " ( ";
        returnData = returnData + new_fomation + " ) ";
      } else if (
        having_array[i]["summeryFilter"] == "like" ||
        having_array[i]["summeryFilter"] == "not like"
      ) {
        let arra_data = Array.isArray(having_array[i]["summeryStartData"]) ? having_array[i]["summeryStartData"].join() : having_array[i]["summeryStartData"];
        arra_data = (!arra_data.startsWith("%") && !arra_data.endsWith("%")) ? ` '%${arra_data}%' ` : `'${arra_data}' `;
        // arra_data = having_array[i]['colDisplayType'] == 'D' ? `TO_DATE(${arra_data}, 'DD-MON-RRRR') ` : arra_data;
        returnData = returnData + arra_data;
      } else if (
        having_array[i]["summeryFilter"] != "between" &&
        having_array[i]["summeryFilter"] != "is not null" &&
        having_array[i]["summeryFilter"] != "is null" &&
        having_array[i]["summeryFilter"] != "in" &&
        having_array[i]["summeryFilter"] != "not in" &&
        having_array[i]["summeryFilter"] != "like" &&
        having_array[i]["summeryFilter"] != "not like"
      ) {
        // having_array[i]['summeryStartData'] = having_array[i]['colDisplayType'] == 'D' ? `TO_DATE( '${having_array[i]['summeryStartData']}', 'DD-MON-RRRR')` : `'${having_array[i]['summeryStartData']}'`;
        having_array[i]['summeryStartData'] = having_array[i]['colDisplayType'] == 'D' ? having_array[i]['dateConversionSyntax'].replace('#VALUE#', `'${having_array[i]['summeryStartData']}'`) : `'${having_array[i]['summeryStartData']}'`;
        returnData = returnData + " ";
        returnData = returnData + `${having_array[i]["summeryStartData"]}` + " ";
      }
      return having_array[i]["summeryFilter"]+ " "+returnData+' ';
    }
    else{
      return '';
    }
  }

  columnTypeCondition(i) {
    if (
      this.getDetails[i]["colType"] == "D" &&
      this.getDetails[i]["colExperssionType"] == "DBSQL"
    ) {
      return false;
    } else {
      return true;
    }
  }

  //------------------------------------------------------------
  // ORDER BY CONDITION
  //------------------------------------------------------------
  orderByCondition() {
    let Orderby = " ORDER BY ";
    for (var i = 0; i < this.getDetails.length; i++) {
      this.getDetails[i]["sortType"] = isNull(this.getDetails[i]["sortType"]) ? '' : this.getDetails[i]["sortType"];
      if (this.getDetails[i]["sortType"].length) {
        // let creteria = this.getDetails[i]['creteria']
        Orderby = Orderby + this.getDetails[i]["alias"].replaceAll(" ", "_") + " ";
        Orderby = Orderby + this.getDetails[i]["sortType"] + " , ";
      }
    }
    Orderby = Orderby.slice(0, -2);
    this.query = this.query + Orderby;
  }

  //------------------------------------------------------------
  // AGGREGATE FUNCTIONALITY
  //------------------------------------------------------------
  aggregateFunctionality(creteria: any, i: any, type: any) {
    var data = "";
    let expression_con = this.getDetails[i]["colType"] == "D" && !isNull(this.getDetails[i]["experssionText"]) &&
      this.getDetails[i]["experssionText"].length > 0;
    if (this.creteriaCondition(creteria)) {
      if (expression_con) {
        data = data + " ( " + this.getDetails[i]["experssionText"] + " ) ";
      } else {
        data =
          data +
          this.getDetails[i]["tableAliasName"] +
          "." +
          this.getDetails[i][type];
      }

    } else {
      if (expression_con) {
        if(creteria == 'COUNT(Distinct)'){
          data =
          data + 'COUNT' + " ( DISTINCT " + this.getDetails[i]["experssionText"] + " ) ";
        }
        else{
          data =
          data + creteria + " ( " + this.getDetails[i]["experssionText"] + " ) ";
        }
      } else {
        if(creteria == 'COUNT(Distinct)'){
          data =
          data +
          'COUNT' +
          " ( DISTINCT " +
          this.getDetails[i]["tableAliasName"] +
          "." +
          this.getDetails[i][type] +
          " ) ";
        }
        else{
        data =
          data +
          creteria +
          " ( " +
          this.getDetails[i]["tableAliasName"] +
          "." +
          this.getDetails[i][type] +
          " ) ";
        }
      }
    }
    // data = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_CHAR(TO_DATE( ${data}, '${this.getDetails[i]['formatType']}'),  '${this.getDetails[i]['formatType']}')`
    // : data;
    data = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateFormattingSyntax'].replace('#VALUE#', this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#', data)) : data
    return data;
  }


  aggregateFunctionalityForHaving(creteria: any, i: any, type: any,having_array) {
    var data = "";
    let expression_con = having_array[i]["colType"] == "D" && !isNull(having_array[i]["experssionText"]) &&
      having_array[i]["experssionText"].length > 0;
    if (!this.creteriaCondition(creteria)) {
      if (expression_con) {
        if(creteria == 'COUNT(Distinct)'){
          data =
          data + 'COUNT' + " ( DISTINCT " + having_array[i]["experssionText"] + " ) ";
        }
        else{
          data =
          data + creteria + " ( " + having_array[i]["experssionText"] + " ) ";
        }
      } else {
        if(creteria == 'COUNT(Distinct)'){
          data =
          data +
          'COUNT' +
          " ( DISTINCT " +
          having_array[i]["tableAliasName"] +
          "." +
          having_array[i][type] +
          " ) ";
        }
        else{
          data =
          data +
          creteria +
          " ( " +
          having_array[i]["tableAliasName"] +
          "." +
          having_array[i][type] +
          " ) ";
        }
      }
      
    } 
    data = having_array[i]['colDisplayType'] == 'D' ? having_array[i]['dateFormattingSyntax'].replace('#VALUE#', having_array[i]['dateConversionSyntax'].replace('#VALUE#', data)) : data
    return data;
  }
  //------------------------------------------------------------
  // CRETERIA CONDITION
  //------------------------------------------------------------
  creteriaCondition(creteria: any) {
    if (
      creteria != "SUM" &&
      creteria != "AVG" &&
      creteria != "MIN" &&
      creteria != "MAX" &&
      creteria != "COUNT(Distinct)" &&
      creteria != "COUNT"
    ) {
      return true;
    } else {
      return false;
    }
  }

  copyContent() {
    let range = document.createRange();
    let id = document.getElementById("copyText");
    id.style.backgroundColor = "#3399ff";
    id.style.color = "white";
    range.selectNode(id);
    window.getSelection().removeAllRanges(); // clear current selection
    window.getSelection().addRange(range); // to select text
    document.execCommand("copy");
    window.getSelection().removeAllRanges(); // to deselect
    setTimeout(() => {
      id.removeAttribute("style");
    }, 100);
  }
}
