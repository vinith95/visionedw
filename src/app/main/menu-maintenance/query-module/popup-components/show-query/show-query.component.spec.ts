import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowQueryComponent } from './show-query.component';

describe('ShowQueryComponent', () => {
  let component: ShowQueryComponent;
  let fixture: ComponentFixture<ShowQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
