// import { distinct } from "rxjs-compat/operator/distinct";

export const creteria_con = {
    forDate: [
        {
            key: '=',
            value: 'Equals To',
            placeholder: 'Enter value'
        },
        {
            key: '!=',
            value: 'Not Equals To'
        },
        {
            key: '>',
            value: 'Greater Than',
            placeholder: 'Numbers only'
        },
        {
            key: '>=',
            value: 'Greater Than Or Equals To',
            placeholder: 'Numbers only'
        },
        {
            key: '<',
            value: 'Less Than',
            placeholder: 'Numbers only'
        },
        {
            key: '<=',
            value: "Less Than Or Equals To",
            placeholder: 'Numbers only'
        },
        {
            key: 'in',
            value: 'IN',
            placeholder: "'v1','v2','v3'"
        },
        {
            key: 'not in',
            value: 'Not IN'
        },
        {
            key: 'between',
            value: 'Between',
            placeholder: ''
        },
        {
            key: 'is null',
            value: 'Is Null'
        },
        {
            key: 'is not null',
            value: 'Is Not Null',
            placeholder: ''
        }
    ],
    notForCN: [{
            key: '=',
            value: 'Equals To',
            placeholder: 'Enter value'
        },
        {
            key: '!=',
            value: 'Not Equals To'
        },
        {
            key: '>',
            value: 'Greater Than',
            placeholder: 'Numbers only'
        },
        {
            key: '>=',
            value: 'Greater Than Or Equals To',
            placeholder: 'Numbers only'
        },
        {
            key: '<',
            value: 'Less Than',
            placeholder: 'Numbers only'
        },
        {
            key: '<=',
            value: "Less Than Or Equals To",
            placeholder: 'Numbers only'
        },
        {
            key: 'like',
            value: 'Like',
            placeholder: 'Alpha Numeric'
        },
        {
            key: 'not like',
            value: 'Not Like',
            placeholder: 'Alpha Numeric'
        },
        {
            key: 'in',
            value: 'IN',
            placeholder: "'v1','v2','v3'"
        },
        {
            key: 'not in',
            value: 'Not IN'
        },
        {
            key: 'between',
            value: 'Between',
            placeholder: ''
        },
        {
            key: 'is null',
            value: 'Is Null'
        },
        {
            key: 'is not null',
            value: 'Is Not Null',
            placeholder: ''
        }
    ],
    forCN: [{
            key: '=',
            value: 'Equals To',
            placeholder: 'Enter value'
        },
        {
            key: '>',
            value: 'Greater Than',
            placeholder: 'Numbers only'
        },
        {
            key: '<=',
            value: "Less Than Or Equals To",
            placeholder: 'Numbers only'
        },
        {
            key: '<',
            value: 'Less Than',
            placeholder: 'Numbers only'
        },
        {
            key: '>=',
            value: 'Greater Than Or Equals To',
            placeholder: 'Numbers only'
        },
        {
            key: '!=',
            value: 'Not Equals To'
        },
        {
            key: 'like',
            value: 'Like',
            placeholder: 'Alpha Numeric'
        },
        {
            key: 'not like',
            value: 'Not Like',
            placeholder: 'Alpha Numeric'
        },
        {
            key: 'in',
            value: 'IN',
            placeholder: "'v1','v2','v3'"
        },
        {
            key: 'not in',
            value: 'Not IN'
        },
        {
            key: 'between',
            value: 'Between',
            placeholder: ''
        },
        {
            key: 'is not null',
            value: 'Is Not Null',
            placeholder: ''
        },
        {
            key: 'is null',
            value: 'Is Null'
        },
    ],
    forDateAgg:[
        {
            key: 'COUNT',
            value: 'COUNT'
        },
        {
            key: 'MIN',
            value: 'MIN'
        },
        {
            key: 'MAX',
            value: 'MAX'
        },
        {
            key: 'COUNT(Distinct)',
            value: 'COUNT(Distinct)'
        },
       
    ],
    notforCNAgg:[
        {
            key: 'COUNT',
            value: 'COUNT'
        },
        {
            key: 'COUNT(Distinct)',
            value: 'COUNT(Distinct)'
        },
    ],
    forCNAgg:[
        {
            key: 'SUM',
            value: 'SUM'
        },
        {
            key: 'AVG',
            value: 'AVG'
        },
        {
            key: 'MIN',
            value: 'MIN'
        },
        {
            key: 'MAX',
            value: 'MAX'
        },
        {
            key: 'COUNT',
            value: 'COUNT'
        },
        {
            key: 'COUNT(Distinct)',
            value: 'COUNT(Distinct)'
        },
    ]
}