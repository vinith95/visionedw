export const report_Query=[
    {
        id: 'reportId',
        columnName: 'Report ID',
        parentId:'designquery',
        name: 'Report ID',
        textalign: 'center'
      },
      {
        id: 'reportDescription',
        columnName: 'Report Description',
        parentId:'designquery',
        name: 'Report Description',
        textalign: 'center'
      },
]