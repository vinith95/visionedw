import { TestBed } from '@angular/core/testing';

import { ExecutedService } from './executed.service';

describe('ExecutedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExecutedService = TestBed.get(ExecutedService);
    expect(service).toBeTruthy();
  });
});
