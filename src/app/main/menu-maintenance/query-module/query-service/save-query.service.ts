import { Injectable } from '@angular/core';
import { isNull } from 'util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/service/common.service';
import { ApiService, GlobalService } from 'src/app/service';

import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';
import { ExecuteQueryNewComponent } from 'src/app/shared/execute-query-new/execute-query-new.component';
import { ExecuteQueryComponent } from 'src/app/shared/execute-query/execute-query.component';

@Injectable({
  providedIn: 'root'
})
export class SaveQueryService {

  column_details: any = {};
  joinClause: any = "";
  hashTagTog: any = false;
  getDetails: any = [];
  extraModel: any = {};
  saveQueryDetails: any = {};
  theadDetails: any = [];
  tbodyDetails: any = [];
  selected_type: any = '';
  hashValue: any = [];
  viewTable: boolean = false;
  totalItem: any = 0;
  saveQueryDesc: boolean = false;
  hashTag: any = [];
  startIndex: any = 1;
  lastIndex: any = 1000;
  reportDetails: any = new BehaviorSubject({});


  //------------------------------------------------------------
  // CONSTRUCTOR
  //------------------------------------------------------------
  constructor(private service: ApiService,
    private modalService: NgbModal,
    private commonService: CommonService,
    private globalService:GlobalService
  ) {
    this.commonService.hashTagValue.subscribe(resp => {
      this.hashValue = resp;
    })
  }

  //------------------------------------------------------------
  // INITIALIZATION
  //------------------------------------------------------------
  ngOnInit() {
  }

  //------------------------------------------------------------
  // GROUP BY CONDITION
  //------------------------------------------------------------
  getQueryDetails(detail: any) {
    this.getDetails = detail.getDetails;
    this.joinClause = detail.joinClause;
    this.saveQueryDetails = detail.extraModel;
    this.selected_type = detail.viewTable;
    this.queryFormation();
  }

  exportQueryFormation(detail: any) {
    this.getDetails = detail.getDetails;
    this.joinClause = detail.joinClause;
    this.saveQueryDetails = detail.extraModel;
    this.selected_type = detail.viewTable;
    let query = "SELECT ";
    let data = "";
    let creteriaCount = 0;
    let groupByCount = 0;
    let orderByCount = 0;
    let overAllSelect = 0;
    query = this.saveQueryDetails['mainModel'].distinctFlag == 'Y' ? query + 'DISTINCT ':query;
    for (var i = 0; i < this.getDetails.length; i++) {
      let creteria = this.getDetails[i]["aggrigate"];
      let creteria1 = this.getDetails[i]["creteria"];
      if (this.getDetails[i].display) {
        data = data + this.aggregateFunctionality(creteria, i, "fields");
        data = data + " As " + this.getDetails[i]["alias"].replaceAll(" ", "_") + " , ";
      }

      creteria != "" && this.creteriaCondition(creteria1) ? creteriaCount++ : "";
      this.getDetails[i]["group"] ? groupByCount++ : "";
      this.getDetails[i]["sortType"] ? orderByCount++ : "";
      this.getDetails[i]["colType"] == "D" &&
        !isNull(this.getDetails[i]["experssionText"])
        ? overAllSelect++
        : "";
    }
    data = data.slice(0, -2);
    query = query + data;
    this.saveQueryDetails["mainModel"]["select"] = query || "";
    creteriaCount ? this.whereCondition(creteriaCount) : this.saveQueryDetails["mainModel"]["where"] = "";
    groupByCount ? this.groupByCondition() : this.saveQueryDetails["mainModel"]["groupBy"] = "";
    orderByCount ? this.orderByCondition() : this.saveQueryDetails["mainModel"]["orderBy"] = "";
    this.saveQueryDetails["mainModel"]['startIndex'] = this.startIndex;
    this.saveQueryDetails["mainModel"]['lastIndex'] = this.lastIndex;
    return this.saveQueryDetails
  }

  //------------------------------------------------------------
  // QUERY FORMATION
  //------------------------------------------------------------
  queryFormation() {
    let query = "SELECT ";
    let data = "";
    let creteriaCount = 0;
    let groupByCount = 0;
    let orderByCount = 0;
    let overAllSelect = 0;
    query = this.saveQueryDetails['mainModel'].distinctFlag == 'Y' ? query + 'DISTINCT ':query;
    for (var i = 0; i < this.getDetails.length; i++) {
      let creteria = this.getDetails[i]["aggrigate"];
      let creteria1 = this.getDetails[i]["creteria"];
      if (this.getDetails[i].display) {
        data = data + this.aggregateFunctionality(creteria, i, "fields");
        data = data + " As " + this.getDetails[i]["alias"].replaceAll(" ","_") + " , ";
      }

      creteria != "" && this.creteriaCondition(creteria1) ? creteriaCount++ : "";
      this.getDetails[i]["group"] ? groupByCount++ : "";
      this.getDetails[i]["sortType"] ? orderByCount++ : "";
      this.getDetails[i]["colType"] == "D" &&
        !isNull(this.getDetails[i]["experssionText"])
        ? overAllSelect++
        : "";
    }
    data = data.slice(0, -2);
    query = query + data;
    this.saveQueryDetails["mainModel"]["select"] = query || "";
    creteriaCount ? this.whereCondition(creteriaCount) : this.saveQueryDetails["mainModel"]["where"] = "";
    groupByCount ? this.groupByCondition() : this.saveQueryDetails["mainModel"]["groupBy"] = "";
    orderByCount ? this.orderByCondition() : this.saveQueryDetails["mainModel"]["orderBy"] = "";
    this.saveQueryDetails["mainModel"]['startIndex'] = this.startIndex;
    this.saveQueryDetails["mainModel"]['lastIndex'] = this.lastIndex;
    if (this.selected_type == 'execute') {
      this.service.post(environment.get_designAnalysis_Execute, this.saveQueryDetails).subscribe(resp => {
        if (resp['status']) {
          this.executeReport(resp, this.saveQueryDetails);
        }
        else {
          this.globalService.showToastr.error(resp['message'])
        }
      })
    }
    else if(this.selected_type == 'download') {
      return this.service.post(environment.get_designAnalysis_Download, this.saveQueryDetails)
    }
    else {
      this.service.post(environment.get_designAnalysis_Hash, this.saveQueryDetails).subscribe(resp => {
        if (resp['status']) {
          let details: any = {};
          let report_details: any = {};
          report_details = this.saveQueryDetails;
          report_details.apiLink = environment.get_designAnalysis_Save,
            report_details['response'] = resp['response']['response'];
          report_details.save = true;
          details = report_details;
          const modelRef = this.modalService.open(ExecuteQueryComponent, {
            size: <any>'md',
            backdrop: 'static',
            windowClass: 'execute-popup'
          });
          modelRef.componentInstance.getDetails = details;
          modelRef.componentInstance.closeModelPopup.subscribe(res => {
            modelRef.close();
          })
        }
        else {
          this.globalService.showToastr.error(resp['message'])
        }
      });
    }




  }
  executeReport(resp, obj) {
    const modelRef = this.modalService.open(ExecuteQueryNewComponent, {
      size: <any>'lg',
      backdrop: 'static',
      windowClass: 'execute-query'
    });
    let totalRows = resp['otherInfo']['mainModel'].totalRows;
    let tableName = resp['otherInfo']['mainModel'].tableName;
    resp['response']['catalogId'] = obj['mainModel']['catalogId'];
    resp['response']['reportId'] = obj['mainModel']['reportId'];
    resp['response']['totalRows'] = totalRows;
    resp['response']['startIndex'] = this.startIndex;
    resp['response']['lastIndex'] = this.lastIndex;
    resp['response']['tableName'] = tableName;
    resp['response']['apilink'] = 'designAnalysis/execute';
    resp['response']['mainModel'] = obj;
    modelRef.componentInstance.getDetails = resp['response'];
    modelRef.componentInstance.closeModelPopup.subscribe(res => {
      modelRef.close();
    })
  }

  //------------------------------------------------------------
  // WHERE CONDITION
  //------------------------------------------------------------
  whereCondition(count: any) {
    let Wherequery = " ";
    for (var i = 0; i < this.getDetails.length; i++) {
      // if (this.getDetails[i].display) {
      this.getDetails[this.getDetails.length - 1]["joinData"] = "";
      let creteria = this.getDetails[i]["creteria"];
      if (creteria != "" && this.creteriaCondition(creteria) && !isNull(creteria)) {
        // let col_type_D = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE($\{#TABLE_ALIAS_${this.getDetails[i]["tableId"] + "#}." + this.getDetails[i]["fields"]}, 'DD-MON-RRRR')` : this.getDetails[i]["tableAliasName"] + "." + this.getDetails[i]["fields"];
        let col_type_D = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#',`$\{#TABLE_ALIAS_${this.getDetails[i]["tableId"] + "#}." + this.getDetails[i]["fields"]}`) : this.getDetails[i]["tableAliasName"] + "." + this.getDetails[i]["fields"];
        Wherequery = Wherequery + col_type_D + " ";
        Wherequery = Wherequery + `${this.getDetails[i]["creteria"]}` + " ";
        if (this.getDetails[i]["creteria"] == "between") {
          // this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE('${this.getDetails[i]['startData']}', 'DD-MON-RRRR')` : `'${this.getDetails[i]['startData']}'`;
          this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#',`'${this.getDetails[i]['startData']}'`):`'${this.getDetails[i]['startData']}'`;
          // this.getDetails[i]['endData'] = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE('${this.getDetails[i]['endData']}', 'DD-MON-RRRR')` : `'${this.getDetails[i]['endData']}'`;
          this.getDetails[i]['endData'] = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#',`'${this.getDetails[i]['endData']}'`):`'${this.getDetails[i]['endData']}'`;
          Wherequery = Wherequery +
            `${this.getDetails[i]["startData"]}` +
            " AND " +
            `${this.getDetails[i]["endData"]}` + " ";
        } else if (this.getDetails[i]["creteria"] == "in" || this.getDetails[i]["creteria"] == "not in") {
          let arra_data = this.getDetails[i]["startData"];
          let new_fomation = "";
          new_fomation = Array.isArray(arra_data)
            ? this.arrayToString(arra_data)
            : this.inDateFormat(arra_data, this.getDetails[i]['colDisplayType']);

          Wherequery = Wherequery + " (";
          Wherequery = Wherequery + new_fomation + ") ";
        } else if (
          this.getDetails[i]["creteria"] == "like" ||
          this.getDetails[i]["creteria"] == "not like"
        ) {
          let arra_data = Array.isArray(this.getDetails[i]["startData"]) ? this.getDetails[i]["startData"].join() : this.getDetails[i]["startData"];
          arra_data = (!arra_data.startsWith("%") && !arra_data.endsWith("%")) ? ` '%${arra_data}%' ` : `'${arra_data}' `;
          // arra_data = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE(${arra_data}, 'DD-MON-RRRR') ` : arra_data;
          Wherequery = Wherequery + arra_data;
        } else if (
          this.getDetails[i]["creteria"] != "between" &&
          this.getDetails[i]["creteria"] != "is not null" &&
          this.getDetails[i]["creteria"] != "is null" &&
          this.getDetails[i]["creteria"] != "in" &&
          this.getDetails[i]["creteria"] != "not in" &&
          this.getDetails[i]["creteria"] != "like" &&
          this.getDetails[i]["creteria"] != "not like"
        ) {
          // this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_DATE( '${this.getDetails[i]['startData']}', 'DD-MON-RRRR')` : `'${this.getDetails[i]['startData']}'`;
          this.getDetails[i]['startData'] = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#',`'${this.getDetails[i]['startData']}'`):`'${this.getDetails[i]['startData']}'`;
          Wherequery = Wherequery + " ";
          Wherequery = Wherequery + `${this.getDetails[i]["startData"]}` + " ";
        }

        if (this.getDetails[i]["join"]) {
          Wherequery = Wherequery + `${this.getDetails[i]["joinData"]}`;
        }
        Wherequery = Wherequery + " ";
      }
      else {
        if (this.getDetails[i]["join"]) {
          Wherequery = Wherequery + `${this.getDetails[i]["joinData"]}` + " '";
        }
      }
      //  }
    } if (Wherequery.length > 7) {
      this.saveQueryDetails["mainModel"]["where"] = Wherequery;
    }
  }

  //------------------------------------------------------------
  // 
  //------------------------------------------------------------
  inDateFormat(arra_data, colDisplayType: any) {
    let data_set = '';
    let array_set = arra_data.split(" | ");
    if (colDisplayType == 'D') {
      array_set.forEach((element, index, array) => {
        array[index] = `'${element}'`
      });
      data_set = array_set.join(',');
    }
    else {
      let aar = arra_data.split(",");
      aar.forEach((element, ind, arr) => {
        if (element[0] == "'" && element[element.length - 1] == "'") {
          arr[ind] = `${element}`
        } else {
          arr[ind] = `'${element}'`
        }
      });
      arra_data = aar.join(",")
      data_set = `${arra_data}`;
    }
    return data_set;
  }


  arrayToString(arra_data: any) {
    let cmb_array = "";
    arra_data.forEach(element => {
      cmb_array = cmb_array + " " + this.commaConditionCheck(element) + ",";
    });
    cmb_array = cmb_array.substr(0, cmb_array.length - 1);
    return cmb_array;
  }


  commaConditionCheck(element) {
    if (element[0] == "'" && element[element.length - 1] == "'") {
      element = element
    }
    if (element[0] != "'" && element[element.length - 1] != "'") {
      element = `'${element}'`
    }
    return element;
  }
  //------------------------------------------------------------
  // GROUP BY CONDITION
  //------------------------------------------------------------
  groupByCondition() {
    let groupBy = "";
    let grouped_columns = [];
    for (var i = 0; i < this.getDetails.length; i++) {
      if (this.getDetails[i].display) {
        let creteria = this.getDetails[i]["aggrigate"];
        if (this.creteriaCondition(creteria)) {
          if (this.columnTypeCondition(i)) {
            if (
              this.getDetails[i]["colType"] == "D" &&
              this.getDetails[i]["colExperssionType"] == "EXP" &&
              !isNull(this.getDetails[i]["experssionText"])
            ) {
              if (
                !grouped_columns.includes(
                  this.getDetails[i]["tableName"] +
                  "_" +
                  this.getDetails[i]["fields"] +
                  "_" +
                  this.creteriaCondition(creteria)
                )
              )
                groupBy =
                  groupBy + " " + this.getDetails[i]["experssionText"] + ", ";
            } else {
              if (
                !grouped_columns.includes(
                  this.getDetails[i]["tableName"] +
                  "_" +
                  this.getDetails[i]["fields"] +
                  "_" +
                  this.creteriaCondition(creteria)
                )
              )
                groupBy =
                  groupBy +
                  " " +
                  "${#TABLE_ALIAS_" + this.getDetails[i]["tableId"] + "#}" +
                  "." +
                  this.getDetails[i]["fields"] +
                  ", ";
            }
          }
          else{
            groupBy =
            groupBy +
            " " +
            this.getDetails[i]["includeGroupCol"] +
            ", ";
          }
        } else {
        }
        grouped_columns.push(
          this.getDetails[i]["tableName"] +
          "_" +
          this.getDetails[i]["fields"] +
          "_" +
          this.creteriaCondition(creteria)
        );
      }
    }
    groupBy = groupBy.slice(0, -2);

    if (groupBy.length > 7) this.saveQueryDetails["mainModel"]["groupBy"] = groupBy;
  }

  //------------------------------------------------------------
  // 
  //------------------------------------------------------------
  columnTypeCondition(i) {
    if (
      this.getDetails[i]["colType"] == "D" &&
      this.getDetails[i]["colExperssionType"] == "DBSQL"
    ) {
      return false;
    } else {
      return true;
    }
  }

  //------------------------------------------------------------
  // ORDER BY CONDITION
  //------------------------------------------------------------
  orderByCondition() {
    let orderBy = "";
    for (var i = 0; i < this.getDetails.length; i++) {
      this.getDetails[i]["sortType"] = isNull(this.getDetails[i]["sortType"]) ? '' : this.getDetails[i]["sortType"];
      if (this.getDetails[i]["sortType"].length) {
        // if (this.getDetails[i].display && this.getDetails[i]["sortType"].length) {
        // let creteria = this.getDetails[i]['creteria']
        orderBy = orderBy + this.getDetails[i]["alias"].replaceAll(" ", "_");
        orderBy = orderBy + " " + this.getDetails[i]["sortType"] + " , ";
        // }
      }
    }
    orderBy = orderBy.slice(0, -2);
    this.saveQueryDetails["mainModel"]["orderBy"] = orderBy;
  }

  //------------------------------------------------------------
  // AGGREGATE FUNCTIONALITY
  //------------------------------------------------------------
  aggregateFunctionality(creteria: any, i: any, type: any) {
    var data = "";
    let expression_con =
      this.getDetails[i]["colType"] == "D" &&
      !isNull(this.getDetails[i]["experssionText"]) &&
      this.getDetails[i]["experssionText"].length;

    if (this.creteriaCondition(creteria)) {
      if (expression_con) {
        data = data + " ( " + this.getDetails[i]["experssionText"] + " ) ";
      } else {
        data =
          data +
          "${#TABLE_ALIAS_" + this.getDetails[i]["tableId"] + "#}" +
          "." +
          this.getDetails[i][type];
      }
    } else {
      if (expression_con) {
        if(creteria == 'COUNT(Distinct)'){
          data =
          data + 'COUNT' + "( DISTINCT " + this.getDetails[i]["experssionText"] + ")";
        }
        else{
          data =
          data + creteria + "(" + this.getDetails[i]["experssionText"] + ")";
        }
      } else {
        if(creteria == 'COUNT(Distinct)'){
          data =
          data +
          'COUNT' +
          "( DISTINCT " +
          "${#TABLE_ALIAS_" + this.getDetails[i]["tableId"] + "#}" +
          "." +
          this.getDetails[i][type] +
          ")";
        }
        else{
          data =
          data +
          creteria +
          "(" +
          "${#TABLE_ALIAS_" + this.getDetails[i]["tableId"] + "#}" +
          "." +
          this.getDetails[i][type] +
          ")";
        }
      }
    }
    // data = this.getDetails[i]['colDisplayType'] == 'D' ? `TO_CHAR(TO_DATE( ${data}, '${this.getDetails[i]['formatType']}'),  '${this.getDetails[i]['formatType']}')`
    //   : data;
      data = this.getDetails[i]['colDisplayType'] == 'D' ? this.getDetails[i]['dateFormattingSyntax'].replace('#VALUE#', this.getDetails[i]['dateConversionSyntax'].replace('#VALUE#', data)) : data
    return data;
  }

  //------------------------------------------------------------
  // CRETERIA CONDITION
  //------------------------------------------------------------
  creteriaCondition(creteria: any) {
    if (
      creteria != "SUM" &&
      creteria != "AVG" &&
      creteria != "MIN" &&
      creteria != "COUNT(Distinct)" &&
      creteria != "MAX" &&
      creteria != "COUNT"
    ) {
      return true;
    } else {
      return false;
    }
  }
}
