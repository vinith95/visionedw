import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../../../environments/environment";
import { ApiService, GlobalService } from "../../../service";
import { tableDetail } from "./data.js";

@Component({
  selector: "app-cron-status",
  templateUrl: "./cron-status.component.html",
  styleUrls: ["./cron-status.component.css"],
})
export class CronStatusComponent implements OnInit {
  screenLangData;
  tableDetail;
  lineList: any;
  leftContainerHeight: string;
  showtable: boolean = false;
  resetModel: any = { key: "" };
  ngxTableConfig: any = {
    api: "cron_getAllQueryResults",
    dataToDisplay: [],
    filterApi: "cron_getAllQueryResults",
    count: 10,
    homeDashboard: [],
  };
  constructor(
    private translate: TranslateService,
    private globalService: GlobalService,
    private _apiService: ApiService
  ) {
    this.translate.setDefaultLang("en");
    this.translate.use("en");
    this.translate.currentLoader
      .getTranslation(this.translate.currentLang)
      .subscribe((data) => {
        this.screenLangData = data["profileSetup"];
      });
  }

  ngOnInit() {
    this.tableDetail = tableDetail;
    let data = {
      actionType: "Query",
      currentPage: 1,
      maxRecords: 5000,
    };
    this._apiService
      .post(environment.cron_getAllQueryResults, data)
      .subscribe((resp) => {
        this.lineList = resp["response"];
        this.ngxTableConfig.cronStatus_Show = resp["otherInfo"];
        this.showtable = true;
      });
  }

  tableAction(e) {
    if (e.type == "refresh" || e.type == "pagination") {
      let data = { actionType: "Query", currentPage: 1, maxRecords: 5000 };
      this._apiService
        .post(environment.cron_getAllQueryResults, data)
        .subscribe((resp) => {
          this.lineList = resp["response"];

          this.resetModel = Object.assign({}, { key: "reload" });
          this.ngxTableConfig["filterData"] = e.filter;
        });
    }
    if (e.type == "cronStartStop") {
      this.lineList = [];
      if (e.data.cronRunStatus == "Y") {
        e.data.cronRunStatus = "N";
      } else {
        e.data.cronRunStatus = "Y";
      }
      let data = e.data;
      this._apiService
        .post(environment.cron_startOrStopEventCron, data)
        .subscribe((resp) => {
          if (resp["status"]) {
            this.globalService.showToastr.success(resp["message"]);

            let data1 = {
              actionType: "Query",
              currentPage: 1,
              maxRecords: 5000,
            };
            this._apiService
              .post(environment.cron_getAllQueryResults, data1)
              .subscribe((resp) => {
                this.lineList = resp["response"];
                this.resetModel = Object.assign({}, { key: "reload" });
              });
          } else {
            this.globalService.showToastr.error(resp["message"]);
          }
        });
    }
  }
  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight =
        document.getElementsByClassName("navbar")[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + "px";
      document.getElementById("card-body").style.height =
        getContentAreaHeight - navbarHeight + "px";
      document.getElementById("card-body").children[0]["style"].height = "100%";
    });
  }
}
