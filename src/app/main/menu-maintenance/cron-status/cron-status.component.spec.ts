import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CronStatusComponent } from './cron-status.component';

describe('CronStatusComponent', () => {
  let component: CronStatusComponent;
  let fixture: ComponentFixture<CronStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CronStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CronStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
