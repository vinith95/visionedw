export const tableDetail = {
  headers: [
    // {
    //   name: "S.No",
    //   referField: 'visionId',
    //   filter: false,
    //   isFilterField: true
    // },

    {
      name: "Cron Type",
      referField: "cronType",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Cron Name",
      referField: "cronTypeDesc",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Status",
      referField: "cronRunStatusDesc",
      filter: false,
      isFilterField: false,
      needIndicator: true,
    },
    {
      name: "Max Thread",
      referField: "maxThread",
      align: "right",
      filter: false,
      isFilterField: true,
    },
    {
      name: "Run Thread",
      referField: "runThread",
      align: "right",
      filter: false,
      isFilterField: false,
    },
  ],
  options: {
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: false,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    highLightRowIndex: {
      index: null,
      currentPage: 1,
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "refresh",
      icon: "refresh",
      actionType: "cronStartStop",
    },
    // {
    //   name: 'Review',
    //   icon: 'fa fa-ban',
    //   actionType: 'review',
    //   validationsReview: true,
    //   reviewConditon: {
    //     key: 'recordIndicator',
    //     value: [1]
    //   }
    // }
  ],
};
