import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { isNull } from 'util';
import { environment } from '../../../../environments/environment';
import { ApiService, GlobalService } from '../../../service';
import { tableDetail } from './data.js';

@Component({
  selector: 'app-profile-setup',
  templateUrl: './profile-setup.component.html',
  styleUrls: ['./profile-setup.component.css']
})
export class ProfileSetupComponent implements OnInit {


  @ViewChild('updateTemplate', { static: false }) profileSetupTemplate: ElementRef;
  screenLangData;
  profileSetupForm;
  tableDetail;
  leftContainerHeight;
  resetModel: any = { key: '' };
  ngxTableConfig: any = {
    api: 'profile_get_all_values',
    dataToDisplay: [],
    filterApi: 'profile_get_all_values',
    count: 10,
    homeDashboard: []
  };

  userGroupFilterData;
  userProfileFilterData;

  profileSetupFormErrors = {
    userGroup: '',
    userProfile: '',
    menuGroup: '',
    excludeMenu: '',
    profileAdd: '',
    profileModify: '',
    profileDelete: '',
    profileInquiry: '',
    profileVerification: '',
    profileUpload: '',
    profileDownload: '',
    applicationId:''
  };
  actionType = 'add';
  actionButtonList = [
    { checked: false, description: 'Add', controlName: 'profileAdd' },
    { checked: false, description: 'Modify', controlName: 'profileModify' },
    { checked: false, description: 'Delete', controlName: 'profileDelete' },
    { checked: false, description: 'View', controlName: 'profileInquiry' },
    { checked: false, description: 'Verify', controlName: 'profileVerification' },
    { checked: false, description: 'Upload', controlName: 'profileUpload' },
    { checked: false, description: 'Download', controlName: 'profileDownload' }
  ];
  userGroupList;
  userProfileList;
  menuGroupList;
  screenNameList = {};
  userGroupFilerList;
  userprofileFilerList = {};
  reportTable: any;
  dashboardTable: any[];
  checkboxValue: any = {
    "dashboard": false,
    "report": false
  };
  applicationList = [];
  tempReportTable = [];
  tempDashboardTable = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'alphaSubTab',
    textField: 'description',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    noDataAvailablePlaceholderText: 'No data available',
    enableCheckAll: true,
    itemsShowLimit: 1,
    maxHeight: 150,
    allowSearchFilter: false
  };
  disabledDropdown = true;

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  constructor(
    private fb: FormBuilder,
    private globalService: GlobalService,
    private _apiService: ApiService,
    private translate: TranslateService,
    private modalService: NgbModal) {
    this.translate.setDefaultLang('en');
    this.translate.use('en');
    this.translate.currentLoader.getTranslation(this.translate.currentLang)
      .subscribe(data => {
        this.screenLangData = data['profileSetup'];
      });
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngOnInit() {
    this.tableDetail = tableDetail;
    this.getPageLoadValues();
    this.initLeaveManagementForm();
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  ngAfterViewInit() {
    setTimeout(() => {
      const getContentAreaHeight = window.innerHeight;
      const navbarHeight = document.getElementsByClassName('navbar')[0].clientHeight;
      this.leftContainerHeight = getContentAreaHeight + 'px';
      document.getElementById('card-body').style.height = (getContentAreaHeight - navbarHeight) + 'px';
      document.getElementById('card-body').children[0]['style'].height = '100%';
    }, 500);
  }


  showFilter = false;
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  getPageLoadValues = () => {
    this._apiService.get(environment.profile_page_load_values).subscribe((resp) => {
      if (resp['status']) {
        this.userGroupList = resp['response'][2];
        this.userProfileList = resp['response'][3];
        this.menuGroupList = resp['response'][4];
        this.menuGroupList.forEach(element => {
          this.screenNameList[element.numSubTab] = element.children;
        });
        this.dashboardTable = [...resp['response'][6]];
        this.tempDashboardTable = [...resp['response'][6]];
        this.reportTable = resp['response'][7];
        this.tempReportTable = resp['response'][7];
        this.ngxTableConfig.data = resp['response'][5];
        this.ngxTableConfig.dataApplication = resp['response'][8];
        this.applicationList = resp['response'][8];
        this.ngxTableConfig.homeDashboard = resp['response'][6] || [];
        this.dashboardTable.forEach((element, index) => {
          if (element.alphaSubTab == "NA") {
            this.dashboardTable.splice(index, 1);
          }
          element.check = false;
          this.dashboardFormArray.clear()
          this.applicationList.forEach((app) => {
            if (element.applicationId == app.alphaSubTab) {
              element.applicationIdDesc = app.description;
            }
          })
        });
        this.reportTable.forEach((element) => {
          element.check = false;
          this.reportFormArray.clear();
          this.applicationList.forEach((app) => {
            if (element.applicationId == app.alphaSubTab) {
              element.applicationIdDesc = app.description;
            }
          })
        });

        this.addDashCheck();
        this.showFilter = true;
      }
    });
  }

  addDashCheck() {

    this.dashboardTable.forEach(() => this.dashboardFormArray.push(new FormControl(false)));
    this.reportTable.forEach(() => this.reportFormArray.push(new FormControl(false)));
  }

  get dashboardFormArray() {
    return this.profileSetupForm.controls.dashboardSelect as FormArray;
  }
  get reportFormArray() {
    return this.profileSetupForm.controls.reportSelect as FormArray;
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  initLeaveManagementForm() {
    this.profileSetupForm = this.fb.group({
      userGroup: [''],
      userProfile: ['', [Validators.required]],
      menuGroup: ['', [Validators.required]],
      excludeMenu: [{value: '', disabled: true}],
      applicationId: [''],
      profileAdd: [''],
      profileModify: [''],
      profileDelete: [''],
      profileInquiry: [''],
      profileVerification: [''],
      profileUpload: [''],
      profileDownload: [''],
      dashboardSelect: new FormArray([]),
      reportSelect: new FormArray([])
    });

    setTimeout(()=>{
      this.profileSetupForm.valueChanges
      .subscribe(data => {
        if (data) {
          this.globalService.validation(this.profileSetupForm, this.profileSetupFormErrors, this.screenLangData.profileSetupForm);
        }
      });
    },100)
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  checkValue(e, i) {
    this.actionButtonList[i].checked = (e.target.checked) ? true : false;
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  tableAction(e) {
    if (e.type === 'edit') {
      this.edit(e.data);
    } else if (e.type === 'add') {
      this.createNewUserProfile();
    } else if (e.type === 'delete') {
      this.deleteUserProfile(e.data);
    } else if (e.type == 'approve' || e.type == 'reject') {
      // this.approveOrReject(e);
    } else if (e.type == 'pagination' || e.type == 'refresh') {
      this.resetModel = Object.assign({}, { key: 'reload' });
    }
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  createNewUserProfile() {
    //this.profileSetupForm.controls['userGroup'].enable();
    this.profileSetupForm.controls['applicationId'].enable();
    this.profileSetupForm.controls['userProfile'].enable();
    this.profileSetupForm.controls['menuGroup'].enable();
    this.profileSetupForm.controls['excludeMenu'].enable();
    this.resetForm();
    this.openModal();
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  showPage:boolean =false;
  showGrid:boolean = true;
  openModal() {
    this.showPage = true;
    this.showFilter =false;
   // this.modalService.open(this.profileSetupTemplate, { size: 'lg', backdrop: 'static' });

  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  edit(data) {
    this.resetForm();
    this.profileSetupForm.controls['applicationId'].disable();
   // this.profileSetupForm.controls['userGroup'].disable();
    this.profileSetupForm.controls['userProfile'].disable();
    this.profileSetupForm.controls['menuGroup'].disable();
    this.profileSetupForm.controls['excludeMenu'].disable();
    this.actionType = 'Modify';
    this.patchLeaveManagementForm(data);
    this.openModal();
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  patchLeaveManagementForm(data) {

    let dashboardSelect = [];
    let reportSelect = [];
    this.dashboardTable.forEach(element => {
      element.check = false;
    });
    this.reportTable.forEach(element => {
      element.check = false;
    });
    if (data.menuGroup == 9999) {
      this.dashboardTable.forEach((element, index) => {
        data.dashboardReportlst.forEach(element1 => {
          if (element1.alphaSubTab == element.alphaSubTab) {
            element.check = true;
            dashboardSelect[index] = true
          }
        });
      });
    }
    if (data.menuGroup == 9998) {
      this.reportTable.forEach((element, index) => {
        data.dashboardReportlst.forEach(element1 => {
          if (element1.alphaSubTab == element.alphaSubTab) {
            element.check = true;
            reportSelect[index] = true
          }
        });
      });
    }
    this.actionButtonList[0].checked = this.convertTrueFalseReverse(data.profileAdd);
    this.actionButtonList[2].checked = this.convertTrueFalseReverse(data.profileDelete);
    this.actionButtonList[6].checked = this.convertTrueFalseReverse(data.profileDownload);
    this.actionButtonList[3].checked = this.convertTrueFalseReverse(data.profileInquiry);
    this.actionButtonList[1].checked = this.convertTrueFalseReverse(data.profileModify);
    this.actionButtonList[5].checked = this.convertTrueFalseReverse(data.profileUpload);
    this.actionButtonList[4].checked = this.convertTrueFalseReverse(data.profileVerification);

    this.profileSetupForm.patchValue({
     // userGroup: data.userGroup,
      userProfile: data.userProfile,
      menuGroup: data.menuGroup,
      applicationId :data.applicationId,
      // excludeMenu: this.multiSelectValue(data.excludeMenu, 'excludeMenu'),
      profileAdd: this.convertTrueFalseReverse(data.profileAdd),
      profileDelete: this.convertTrueFalseReverse(data.profileDelete),
      profileDownload: this.convertTrueFalseReverse(data.profileDownload),
      profileInquiry: this.convertTrueFalseReverse(data.profileInquiry),
      profileModify: this.convertTrueFalseReverse(data.profileModify),
      profileUpload: this.convertTrueFalseReverse(data.profileUpload),
      profileVerification: this.convertTrueFalseReverse(data.profileVerification),
      dashboardSelect: dashboardSelect,
      reportSelect: reportSelect
    });
    this.profileSetupForm.patchValue({
      excludeMenu: this.multiSelectValue(data.excludeMenu, 'excludeMenu'),
    });
    if(this.screenNameList[this.profileSetupForm.get('menuGroup').value] != undefined &&
    this.screenNameList[this.profileSetupForm.get('menuGroup').value] != null &&
    this.screenNameList[this.profileSetupForm.get('menuGroup').value].length) {
      this.disabledDropdown = false;
    }
    else {
      this.disabledDropdown = true;
    }
    this.enableCheckbox(data.menuGroup, false);
    this.selectedAll();

  }

  screenNamePatch() {
    this.profileSetupForm.patchValue({
      'screenName': ''
    })
  }
  enableCheckbox(value, patch) {
    if (patch) {
      this.profileSetupForm.patchValue({
        'profileAdd': '',
        'profileDelete': '',
        'profileDownload': '',
        'profileInquiry': '',
        'profileModify': '',
        'profileUpload': '',
        'profileVerification': '',

      })
    }

    if (value == 9999) {
      this.profileSetupForm.get('profileAdd').disable();
      this.profileSetupForm.get('profileDelete').disable();
      this.profileSetupForm.get('profileDownload').disable();
      this.profileSetupForm.get('profileInquiry').enable();
      this.profileSetupForm.get('profileModify').disable();
      this.profileSetupForm.get('profileUpload').disable();
      this.profileSetupForm.get('profileVerification').disable();
    }
    else if (value == 9998) {
      this.profileSetupForm.get('profileAdd').disable();
      this.profileSetupForm.get('profileDelete').disable();
      this.profileSetupForm.get('profileDownload').enable();
      this.profileSetupForm.get('profileInquiry').enable();
      this.profileSetupForm.get('profileModify').disable();
      this.profileSetupForm.get('profileUpload').disable();
      this.profileSetupForm.get('profileVerification').disable();
    }
    else {
      this.profileSetupForm.get('profileAdd').enable();
      this.profileSetupForm.get('profileDelete').enable();
      this.profileSetupForm.get('profileDownload').enable();
      this.profileSetupForm.get('profileInquiry').enable();
      this.profileSetupForm.get('profileModify').enable();
      this.profileSetupForm.get('profileUpload').enable();
      this.profileSetupForm.get('profileVerification').enable();
    }
  }

  actionDisabled(value, data) {
    if (value == 'dashboards' || value == 'DASHBOARDS' || value == 'reports' || value == 'REPORTS') {
      if (data.description != 'View' && (value == 'dashboards' || value == 'DASHBOARDS')) {
        return false;
      }
      if (data.description != 'View' || data.description != 'Download' && (value == 'dashboards' || value == 'DASHBOARDS')) {
        return false;
      }
    }
    else {
      return true;
    }

  }
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  saveProfileSetup() {    
    if (!this.profileSetupForm.valid) {
      this.globalService.markAllDirty(this.profileSetupForm);
      return false;
    }
    const requestData = this.profileSetupForm.getRawValue();
    const dashboardTableIds = this.dashboardTable
      .map((check, i) => check.check ? { "alphaSubTab": this.dashboardTable[i].alphaSubTab, "description": this.dashboardTable[i].description, applicationId: this.dashboardTable[i].applicationId } : null)
      .filter(v => v !== null);

    const reportTableIds = this.reportTable
      .map((check, i) => check.check ? { "alphaSubTab": this.reportTable[i].alphaSubTab, "description": this.reportTable[i].description, applicationId: this.reportTable[i].applicationId } : null)
      .filter(v => v !== null);

    if (requestData["menuGroup"] == 9998) {
      requestData['dashboardReportlst'] = reportTableIds;
    }
    if (requestData["menuGroup"] == 9999) {
      requestData['dashboardReportlst'] = dashboardTableIds;
    }
    requestData['excludeMenu'] = this.arraySrtingCon(requestData['excludeMenu']),
    requestData.profileAdd = this.convertTrueFalse(requestData.profileAdd);
    requestData.profileDelete = this.convertTrueFalse(requestData.profileDelete);
    requestData.profileDownload = this.convertTrueFalse(requestData.profileDownload);
    requestData.profileInquiry = this.convertTrueFalse(requestData.profileInquiry);
    requestData.profileModify = this.convertTrueFalse(requestData.profileModify);
    requestData.profileUpload = this.convertTrueFalse(requestData.profileUpload);
    requestData.profileVerification = this.convertTrueFalse(requestData.profileVerification);

    const api = this.actionType == 'add' ? environment.profile_add : environment.profile_modify;
    this._apiService.post(api, requestData).subscribe((resp) => {
      if (resp['status'] == 1) {
        this.actionType = 'add';
        this.globalService.showToastr.success(resp['message']);
        this.modalService.dismissAll();
        this.resetModel = Object.assign({}, { key: 'reload' });
      } else {
        this.globalService.showToastr.error(resp['message']);
      }
    });
  }


  convertTrueFalse(data) {
    return (data == true) ? 'Y' : 'N';
  }


  convertTrueFalseReverse(data) {
    return (data == 'Y') ? true : false;
  }


  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  deleteUserProfile(data) {
    data['actionType'] = 'Delete';
    this._apiService.post(environment.profile_delete, data).subscribe((resp) => {
      if (resp['status']) {
        this.globalService.showToastr.success(resp['message']);
        this.resetModel = Object.assign({}, { key: 'reload' });
      } else {
        this.globalService.showToastr.warning(resp['message']);
      }
    });
  }

  resetForm() {
    this.actionType = 'add';
    this.profileSetupForm.reset();
    this.actionButtonList.forEach((element) => {
      element.checked = false;
    });

  }
  selectAll(value, arr) {
    if (arr == 'report') {
      this.reportTable.forEach((element) => {
        element.check = value;
      });
    }
    if (arr == 'dash') {
      this.dashboardTable.forEach((element) => {
        element.check = value;
      });
    }
  }
  selectedAll() {
    this.checkboxValue['report'] = this.reportTable.every((element) => element.check);
    this.checkboxValue['dashboard'] = this.dashboardTable.every((element) => element.check);
  }
  selectIndividual(value, alphaSubTab, type) {
    if (type == 'rep') {
      this.reportTable.forEach(element => {
        if (element.alphaSubTab == alphaSubTab) {
          element.check = value;
        }
      });
      this.checkboxValue['report'] = this.reportTable.every((element) => element.check);
    }
    if (type == 'dash') {
      this.dashboardTable.forEach(element => {
        if (element.alphaSubTab == alphaSubTab) {
          element.check = value;
        }
      });
      this.checkboxValue['dashboard'] = this.dashboardTable.every((element) => element.check);
    }
  }

  selectApp() {
    let rawVal = this.profileSetupForm.getRawValue();
    if (!isNull(rawVal['applicationId']) && rawVal['applicationId'] != "") {
      this.dashboardTable = JSON.parse(JSON.stringify(this.tempDashboardTable)).filter(item => item.applicationId == rawVal['applicationId']);
      this.reportTable = JSON.parse(JSON.stringify(this.tempReportTable)).filter(item => item.applicationId == rawVal['applicationId']);
    }
    else {
      this.dashboardTable = this.tempDashboardTable;
      this.reportTable = this.tempReportTable;
    }
    this.dashboardTable.forEach((element, index) => {
      if (element.alphaSubTab == "NA") {
        this.dashboardTable.splice(index, 1);
      }
      // element.check = false;
      this.applicationList.forEach((app) => {
        if (element.applicationId == app.alphaSubTab) {
          element.applicationIdDesc = app.description;
        }
      })
    });
    this.dashboardFormArray.clear()
    this.reportTable.forEach((element) => {
      // element.check = false;
      this.applicationList.forEach((app) => {
        if (element.applicationId == app.alphaSubTab) {
          element.applicationIdDesc = app.description;
        }
      })
    });
    this.reportFormArray.clear()
    this.addDashCheck();
  }

  modalClose() {
    this.modalService.dismissAll();
    this.dashboardTable = this.tempDashboardTable;
    this.reportTable = this.tempReportTable;
    this.dashboardTable.forEach((element, index) => {
      if (element.alphaSubTab == "NA") {
        this.dashboardTable.splice(index, 1);
      }
      // element.check = false;
      this.applicationList.forEach((app) => {
        if (element.applicationId == app.alphaSubTab) {
          element.applicationIdDesc = app.description;
        }
      })
    });
    this.dashboardFormArray.clear()
    this.reportTable.forEach((element) => {
      // element.check = false;
      this.applicationList.forEach((app) => {
        if (element.applicationId == app.alphaSubTab) {
          element.applicationIdDesc = app.description;
        }
      })
    });
    this.reportFormArray.clear()
    this.addDashCheck();
    this.showPage = false;
    this.showFilter =true;
  }

  multiSelectValue(data, type) {
    let returnArr = []
    if (data != null && data != '') {
      let arr = data.split(',')
      if (type == 'excludeMenu') {
        this.screenNameList[this.profileSetupForm.get('menuGroup').value].forEach(element1 => {
          arr.forEach(element => {
            if (element == element1.alphaSubTab)
              returnArr.push(element1)
          });
        });
      }
    }
    return returnArr;
  }

  arraySrtingCon(data) {
    let arr = [];
    if(data != null && data.length)
    data.forEach(element => {
      arr.push(element["alphaSubTab"])
    });
    return arr.toString();
  }

  changeMenuGroup() {
    this.profileSetupForm.patchValue({
      excludeMenu: ''
    })
    if(this.screenNameList[this.profileSetupForm.get('menuGroup').value] != undefined &&
    this.screenNameList[this.profileSetupForm.get('menuGroup').value] != null &&
    this.screenNameList[this.profileSetupForm.get('menuGroup').value].length) {
      this.disabledDropdown = false;
    }
    else {
      this.disabledDropdown = true;
    }
  }

}
