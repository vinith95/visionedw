export const tableDetail = {
  headers: [
    // {
    //       name: "User Group",
    //       referField: 'userGroupDesc',
    //       filter: false,
    //       isFilterField: true
    //   }, {
    //       name: "User Profile",
    //       referField: 'userProfileDesc',
    //       filter: false,
    //       isFilterField: true
    //   },
    {
      name: "Menu Group",
      referField: "menuGroupDesc",
      filter: false,
      isFilterField: true,
    },
    // {
    //     name: "Menu Name",
    //     referField: 'screenName',
    //     filter: false,
    //     isFilterField: true
    // },
    {
      name: "Add",
      referField: "profileAdd",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "Modify",
      referField: "profileModify",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "Delete",
      referField: "profileDelete",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "View",
      referField: "profileInquiry",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "Verify",
      referField: "profileVerification",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "Upload",
      referField: "profileUpload",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "Download",
      referField: "profileDownload",
      filter: false,
      isFilterField: true,
      viewType: "icon",
    },
    {
      name: "Status",
      referField: "statusDesc",
      filter: false,
      isFilterField: true,
      needIndicator: true,
    },
    {
      name: "Date Creation",
      referField: "dateCreation",
      filter: false,
      isFilterField: true,
    },
  ],
  options: {
    // highLightRowIndex:{
    //     index:null,
    //     currentPage:1
    //   },
    deleteButton: {
      enable: false,
      actionName: "",
    },
    bulkDeleteButton: {
      enable: false,
      actionName: "",
    },
    bulkApproveButton: {
      enable: false,
      actionName: "",
    },
    bulkRejectButton: {
      enable: false,
      actionName: "",
    },
    addButton: {
      enable: true,
      actionName: "add",
    },
    filterButton: {
      enable: true,
      actionName: "",
    },
    refreshButton: {
      enable: true,
      actionName: "refresh",
    },
    compactTableButton: {
      enable: false,
      actionName: "",
    },
    fullScreenButton: {
      enable: false,
      actionName: "",
    },
    showPagination: true,
    showCheckBox: false,
    showButton: true,
  },
  buttons: [
    {
      name: "edit",
      icon: "fa fa-pencil",
      actionType: "edit",
      validations: false,
    },
    {
      name: "delete",
      icon: "fa fa-trash-o",
      actionType: "delete",
      validations: true,
      conditon: {
        key: "recordIndicator",
        value: [1, 2, 3, 4],
      },
    },
  ],
};
