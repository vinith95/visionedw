import { Injectable } from '@angular/core';

import { ApiService } from '../../service/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

    public private_key = this.apiService.private_key;
    public public_key = this.apiService.public_key;
  // ------------------------------------------------------------
  // CONSTRUCTOR
  // ------------------------------------------------------------
  constructor(private apiService: ApiService) { }


  // ------------------------------------------------------------
  // AUTHENTICATION FUNCTION
  // ------------------------------------------------------------
  public authCheck(postData: any): Observable<any> {
    return this.apiService.get('authenticate?username=' + postData['user_name'] + '&password=' + window.btoa(postData['password']));
  }


  // ------------------------------------------------------------
  // GET TOKEN
  // ------------------------------------------------------------
  getToken(): Observable<any> {
    return this.apiService.get('generateKeypair');
  }
}
