import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JSEncrypt } from 'jsencrypt';
import { TranslateService } from '@ngx-translate/core';

import { GlobalService, JwtService } from '../../service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  // ------------------------------------------------------------
  // PUBLIC VARIABLES AND DECLARATIONS
  // ------------------------------------------------------------
  public validationMsg: string;
  public loginForm: FormGroup;
  showSpinner: boolean = false;
  public landingpage_data = [];
  public landingpage_data1 = [];
  private encrypt: any;
  private private_key: any;
  private public_key: any;
  elem: HTMLElement;
  showLoginError = false;
  errorKey = [];
  showLoginInvalid = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private jwtService: JwtService,
    private router: Router,
    private translate: TranslateService,
    private globalService: GlobalService) {
    this.buildLoginForm();
    this.translate.setDefaultLang('en');
    this.translate.use('en');
    // GET LANGUAGE
    this.translate.currentLoader.getTranslation(this.translate.currentLang)
      .subscribe(data => {
      });
  }


  // ------------------------------------------------------------
  // INITIALIZATION
  // ------------------------------------------------------------
  ngOnInit() {
    this.elem = document.documentElement;
    this.getPublicKey();
    const authToken = localStorage.getItem('VisionAuthenticate');

    if (authToken) {
      this.globalService.pageTitle = '';
      if (localStorage.getItem('pageTitle') != '') {
        localStorage.setItem('pageTitle', localStorage.getItem('pageTitle'));
      }
      else {
        localStorage.setItem('pageTitle', 'Home');
      }
      this.globalService.pageTitle = localStorage.getItem('pageTitle');
      this.router.navigate(['/main/VisionEdwAdmin']);
    }
    window.onload = (ev) => {
      if (localStorage.getItem('pageTitle') != '') {
        localStorage.setItem('pageTitle', localStorage.getItem('pageTitle'));
      }
      else {
        localStorage.setItem('pageTitle', 'Home');
      }
      this.globalService.pageTitle = localStorage.getItem('pageTitle');
      this.router.navigate(['/main/VisionEdwAdmin']);
    };

    this.encrypt = new JSEncrypt();
    this.private_key = this.authService.private_key;
    this.public_key = this.authService.public_key;
    this.loginForm.valueChanges.subscribe(() => {
      this.showLoginInvalid = false;
      this.showLoginError = false;
      this.errorKey = [];
    });
  }


  // ------------------------------------------------------------
  // BUILD LOGIN FORM
  // ------------------------------------------------------------
  private buildLoginForm(): void {
    this.loginForm = this.fb.group(
      {
        user_name: ['', [Validators.required, Validators.minLength(2)]],
        password: ['', Validators.required]
      }
    );
  }


  // ------------------------------------------------------------
  // GET PUBLIC KEY TOKEN
  // ------------------------------------------------------------
  /*  */
  private getPublicKey() {
    this.authService.getToken().subscribe(
      (successResponse) => {
        this.jwtService.set('temporary-token', successResponse.response['temporary-token']);
      },
      (errorResponse) => {
        // this.validationMsg = "Technical Issue, Please Try Again Later";
      }
    );
  }


  // ------------------------------------------------------------
  // FOR LOGIN CHECK WITH GIVEN CREDENTIALS
  // ------------------------------------------------------------
  public authCheck() {
    this.showLoginInvalid = false;
    this.errorKey = [];
    if(this.loginForm.value.user_name == '' || this.loginForm.value.password == '') {
      this.showLoginError = true;
      if(this.loginForm.value.user_name == '' && this.loginForm.value.password !== '') {
        this.errorKey.push('User Name.');
      } else if(this.loginForm.value.user_name == '') {
        this.errorKey.push('User Name');
      }
      if(this.loginForm.value.password == '') {
        this.errorKey.push('Password.');
      }
      return;
    }
    this.showLoginError = false;
    const loginData = {
      user_name: this.loginForm.value.user_name,
      password: this.loginForm.value.password
    };
    
    this.showSpinner = true;
    var jsonInfoarr = localStorage.getItem('Dashboard') ? JSON.parse(localStorage.getItem('Dashboard')) : [];
    this.authService.authCheck(loginData).subscribe(
      (success) => {
        this.openFullscreen();
        this.showSpinner = false;
        
        let theme = {
          "appTheme": success.response.user_details['appTheme'],
          "reportSliderTheme": success.response.user_details['reportSliderTheme'],
          "visionId": success.response.user_details['visionId'],
        }
        this.jwtService.clear();
        this.jwtService.set('VisionAuthenticate', success.response['token']);
        this.jwtService.set('maker', success.response['user_details'].maker);
        this.jwtService.set('verifier', success.response['user_details'].verifier);
        this.jwtService.set('clientName', success.response['user_details'].clientName);
        this.jwtService.set('User_Name', success.response['user_details'].userName);
        this.jwtService.set('themeAndLanguage', JSON.stringify(theme))
        this.jwtService.set('menu_hierarchy', JSON.stringify(success.response['menu_hierarchy']));
        this.globalService.pageTitle = '';
        this.jwtService.set('pageTitle', 'Home');
        this.jwtService.set('Dashboard_Id', success['response']['user_details']['homeDashboard']);
        this.jwtService.set('lg_Dashboard_Id', success['response']['user_details']['homeDashboard']);
        // localStorage.setItem('pageTitle', 'Home');
        this.jwtService.set('enableWidgets', success['response']['user_details']['enableWidgets']);
        this.router.navigate(['/main/VisionEdwAdmin']);
        localStorage.setItem('menu_hierarchy', JSON.stringify(success.response['menu_hierarchy']));
        this.jwtService.set('businessDate', JSON.stringify(success.response['businessDay']));
      },
      (error) => {
        this.showLoginInvalid = true;
        this.errorKey = [];
        this.errorKey.push(error);
      }
    );
  }

  openFullscreen() {
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    }
    // else if (this.elem.mozRequestFullScreen) {
    //   this.elem.mozRequestFullScreen();
    // } else if (this.elem.webkitRequestFullscreen) {
    //   this.elem.webkitRequestFullscreen();
    // } else if (this.elem.msRequestFullscreen) {
    //   this.elem.msRequestFullscreen();
    // }
  }


}
