import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rendering',
  templateUrl: './rendering.component.html',
  styleUrls: ['./rendering.component.css']
})
export class RenderingComponent implements OnInit, OnDestroy {
  type;
  templateData = {
    templateSelected: false
  };
  queryParam;
  constructor(private route: ActivatedRoute) {
    this.queryParam = this.route.queryParams.subscribe(params => {
      this.type = params['type'];
      // this.templateData = dataSource;
      this.templateData = params['dataSource'] ? JSON.parse(params['dataSource']) : this.templateData = {
        templateSelected: false
      };
    });
  }

  ngOnInit(): void {
  }

  borderTopStyle(item) {
    if (item.initialRequest && item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] != 'Y') {
        return {
          'border-top': `solid 2px #${tempJson.HEAD["MID-FONT-COLOR"]}`
        }
      }
    }
  }

  tileStyle(item) {
    if (item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      let bgType = tempJson.HEAD["BG-COLOR"].split(",");
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] == 'Y') {
        return {
          'border': `2px solid #${tempJson.HEAD["HEADER-BORDER-COLOR"]}`,
          'color': '#' + tempJson.HEAD["HEADER-BORDER-COLOR"],
        }
      }
      if (bgType && bgType.length == 1) {
        return {
          'background-color': '#' + bgType[0],
          'color': '#' + tempJson.HEAD["FONT-COLOR"],
        }
      }
      if (bgType && bgType.length == 2) {
        return {
          'background-image': `linear-gradient(to right, #${bgType[0]}, #${bgType[1]}`,
          'color': '#' + tempJson.HEAD["FONT-COLOR"],
        }
      }
    }
  }

  titleColorStyle(item) {
    if (item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] != 'Y') {
        return {
          'color': '#' + tempJson["TITLE-COLOR"]
        }
      }
    }
  }

  centerFontStyle(item) {
    if (item.initialRequest.reportDesignXml != null && item.initialRequest.reportDesignXml != undefined && item.initialRequest.reportDesignXml != "") {
      let tempJson = JSON.parse(item.initialRequest.reportDesignXml);
      if (tempJson.HEAD["HEADER-BORDER-ENABLE"] != 'Y') {
        return {
          'color': '#' + tempJson.HEAD["MID-FONT-COLOR"]
        }
      }
    }
  }

  scalingFactorText(data){
    let scale = data == '1' ? "" : data == '1000' ? "(In Thousands)" : 
        data == '1000000' ? "(In Millions)" : data == '1000000000' ? "(In Billions)" : "";
        return scale
  }

  ngOnDestroy(){
    if(this.queryParam)
    this.queryParam.unsubscribe();
  }
}
