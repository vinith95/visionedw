import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RenderingComponent } from './rendering/rendering.component';
import { AuthGaurdGuard } from './service/auth-gaurd.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then(m => m.MainModule),
    canActivate: [AuthGaurdGuard]
  },
  {
    path: 'render',
    component: RenderingComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
