import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
 import { CanDeactivateService } from './can-deactivate.service';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}
@Injectable({
  providedIn: 'root'
})
export class AuthGaurdGuard implements CanActivate, CanDeactivate<any> {

  constructor(private router: Router , private dialogService: CanDeactivateService) { }


  // ------------------------------------------------------------
  //  CAN ACTIVATE ROUTE
  // ------------------------------------------------------------
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let path = route.data[0];
   // this.common.toolBarTitle.next(path);

    const authToken = localStorage.getItem('VisionAuthenticate');
    if (authToken) {
      return true;
    }

    this.router.navigate(['login']);
    return false;
  }

  canDeactivate(component: any, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): any {
    if (!component.isUpdating) {
      component.isUpdating = false;
      return this.dialogService.confirm('Discard changes for Country?',currentState.url);
    }
    return true;
  }


}
