import { TestBed } from '@angular/core/testing';

import { XmltoJsonService } from './xmlto-json.service';

describe('XmltoJsonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XmltoJsonService = TestBed.get(XmltoJsonService);
    expect(service).toBeTruthy();
  });
});
