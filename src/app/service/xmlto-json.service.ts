import { Injectable } from '@angular/core';
import { isArray, isNullOrUndefined, isObject } from 'util';

@Injectable({
  providedIn: 'root'
})
export class XmltoJsonService {

  constructor() { }

  xmlToJson(xml) {
    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) {
      // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj = {};
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          obj[attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType == 3) {
      // text
      obj = xml.nodeValue;
    }

    // do children
    // If all text nodes inside, get concatenated text from them.
    var textNodes = [].slice.call(xml.childNodes).filter(function (node) {
      return node.nodeType === 3;
    });
    if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
      obj = [].slice.call(xml.childNodes).reduce(function (text, node) {
        return text + node.nodeValue;
      }, "");
    } else if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof obj[nodeName] == "undefined") {
          obj[nodeName] = this.xmlToJson(item);
        } else {
          if (typeof obj[nodeName].push == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(this.xmlToJson(item));
        }
      }
    }
    return obj;
  }

  conversionJson(xml, data?, type?, index?, theme?, themeNoww?, gradient?) {
    var xml2 = xml.replace(/&/g, '&amp;');
    const parser = new DOMParser();
    const xml1 = parser.parseFromString(xml2, 'text/xml');
    const json = this.xmlToJson(xml1);

    if (type == 'multiple')
      if (isNullOrUndefined(data.childTileslst[index]["theme"]))
        data.childTileslst[index]["theme"] = "defaultTheme";

    let themeNow = type == 'multiple' ? data.childTileslst[index]["theme"] : '';
    json['chart'] ? delete json['chart']['#text'] : "";


    if (json['chart'] && json['chart']['useRoundEdges'] && themeNoww != 'DS_DEFAULT') {
      json['chart']['useRoundEdges'] = 0;
    }

    //let themeNow = type == 'multiple' ? (!isNullOrUndefined(data.childTileslst[index]["theme"])) ? data.childTileslst[index]["theme"] : 'defaultTheme' : '';
    if (json['chart'] && json['chart']['usePlotGradientColor'] && gradient) {
      json['chart']['usePlotGradientColor'] = 1;
      json['chart']['plotGradientColor'] = theme[themeNoww].bgColorChart;
    }
    else {
      if (json['chart'] && json['chart']['use3DLighting'] && themeNoww != 'DS_DEFAULT') {
        json['chart']['use3DLighting'] = 0;
      }
    }

    if (json['chart'] && json['chart']['set']) {
      if (isArray(json['chart']['set'])) {
        json['data'] = json['chart']['set'];
      }
      else {
        json['data'] = []
        json['data'].push(json['chart']['set']);
      }
      if (!isNullOrUndefined(type)) {
        !isNullOrUndefined(json['data']) && json['data'].forEach((element, ind) => {
          element['labelFontColor'] = type == 'single' ? theme[themeNoww].labelColor : theme[themeNoww]['multiTile'][themeNow].labelColor;
          element['valueFontColor'] = type == 'single' ? theme[themeNoww].labelColor : theme[themeNoww]['multiTile'][themeNow].labelColor;
        })
        json['chart']['bgColor'] = type == 'single' ? theme[themeNoww].bgColorChart : theme[themeNoww]['multiTile'][themeNow].bgColorChart;
        json['chart']['baseFontColor'] = type == 'single' ? theme[themeNoww].labelColor : theme[themeNoww]['multiTile'][themeNow].labelColor;
      }
    }
    if (json['chart'] && json['chart']['categories']) {
      delete json['chart']['categories']['#text'];
      json['categories'] = []
      json['categories'].push(json['chart']['categories']);
      json['dataset'] = json['chart']['dataset'];
      json['dataset'] = json['chart']['dataset'];
      !isNullOrUndefined(json['dataset']) && isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
        if (isArray(json['categories'][0]['category'])) {
          json['categories'][0]['category'] = json['categories'][0]['category'];
        }
        else {
          let tempCat = json['categories'][0]['category'];
          json['categories'][0]['category'] = [];
          json['categories'][0]['category'].push(tempCat);
        }
        if (isArray(element['set'])) {
          element['data'] = element['set'];
        }
        else {
          element['data'] = []
          element['data'].push(element['set']);
        }
      });

      if (!isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isObject(json['dataset'])) {
        json['dataset']['data'] = [];
        json['dataset']['data'] = json['dataset']['set'];
        delete json['dataset']['set'];
        delete json['chart']['categories'];
        delete json['chart']['dataset'];
        delete json['chart']['styles'];
        if (isArray(json['categories'][0]['category'])) {
          json['categories'][0]['category'] = json['categories'][0]['category'];
        }
        else {
          let tempCat = json['categories'][0]['category'];
          json['categories'][0]['category'] = [];
          json['categories'][0]['category'].push(tempCat);
        }
        var dataset1 = JSON.parse(JSON.stringify(json));
        json['dataset'] = [];
        if (isArray(dataset1['dataset']['data'])) {
          json['dataset'].push({ "seriesName": dataset1['dataset']['seriesName'], "color": dataset1['dataset']['color'], "data": dataset1['dataset']['data'] });
        }
        else {
          json['dataset'].push({ "seriesName": dataset1['dataset']['seriesName'], "color": dataset1['dataset']['color'], "data": [dataset1['dataset']['data']] });
        }
      } else {
        !isNullOrUndefined(json['dataset']) && !isNullOrUndefined(json['dataset']['set']) && isArray(json['dataset']) && json['dataset'].forEach(element => {
          element['data'] = element['set']
        });
      }
      if (!isNullOrUndefined(type)) {
        json['chart']['bgColor'] = type == 'single' ? theme[themeNoww].bgColorChart : theme[themeNoww]['multiTile'][themeNow].bgColorChart;
        json['chart']['baseFontColor'] = type == 'single' ? theme[themeNoww].labelColor : theme[themeNoww]['multiTile'][themeNow].labelColor;
      }
    }
    if (json['chart'] && json['chart']['category']) {
      json['category'] = []
      json['category'].push(json['chart']['category']);
    }
    return json
  }
}
