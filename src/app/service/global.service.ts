import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";
import {
  FormArray,
  FormGroup,
  Validators,
  FormBuilder,
  FormControl,
  AbstractControl,
} from "@angular/forms";
import { isArray, isNullOrUndefined } from "util";
import { Router } from "@angular/router";
import { Subject, BehaviorSubject, Observable } from "rxjs";
import { dateFormat } from "../../assets/data/globaldata.json";
declare var $: any;
import { GenericPopupComponent } from "../shared/generic-popup/generic-popup.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Injectable({
  providedIn: "root",
})
export class GlobalService {
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  pageTitle = "";
  last_toolTip;
  pageLevelAccess;
  validNumber = true;
  sideNumber = true;
  dateFormat = dateFormat;
  static dateFormat = dateFormat;
  selectedReport;
  accordinValue;
  filterList: any = [];
  accordinSubValue;
  accordinValueDash;
  accordinSubValueDash;
  selectedDashboard;
  widget = {
    iconEnabled: false,
    enableDelete: false,
    enableTheme: true,
  };
  erSchema = {
    iconEnabled: false,
  };
  topbarSlider = new BehaviorSubject({});
  activeIcon = "";
  selectedTheme = {};
  def_lang = new BehaviorSubject("en");
  menuProgram: any = "";
  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  constructor(
    private toastr: ToastrService,
    private modalService: NgbModal,
    private _translate: TranslateService,
    private router: Router
  ) {
    // INITIALIZE TRANSLATE SERVICE
    this._translate.addLangs(["en", "ar"]);
    this._translate.setDefaultLang("en");
    this.useLang("en");
  }

  getTodayDate() {
    const currentDate = new Date(new Date().getTime());
    const day = currentDate.getDate();
    const month = currentDate.getMonth() + 1;
    const year = currentDate.getFullYear();
    return { year: year, month: month, day: day + 1 };
  }
  setLanguage(data) {
    this.def_lang.next(data);
  }

  getLanguage() {
    return this.def_lang;
  }

  getDateFormat(date) {
    let dateReplace = date.replaceAll("-", "/");
    let myDate = new Date(dateReplace);
    if (!isNullOrUndefined(date)) {
      let getIndex = this.dateFormat.search(/[*?:\/-]/g);
      let getFormat = this.dateFormat.slice(getIndex, getIndex + 1);
      let getDate = "" + myDate.getDate();
      if (getDate.toString().length == 1) {
        getDate = "0" + getDate.toString();
      }
      let month = myDate.getMonth() + 1;
      let getMonth = "" + month;
      if (getMonth.toString().length == 1) {
        getMonth = "0" + getMonth.toString();
      }
      let getYear = myDate.getFullYear();
      let monthList = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ];
      let format =
        this.dateFormat == "DD-MMM-YYYY" || this.dateFormat == "DD/MM/YYYY"
          ? myDate
              .toLocaleDateString("en-GB", {
                day: "2-digit",
                month: this.dateFormat == "DD-MMM-YYYY" ? "short" : "2-digit",
                year: "numeric",
              })
              .split(" ")
              .join(getFormat)
          : this.dateFormat == "MM-DD-YYYY" ||
            this.dateFormat == "MM/DD/YYYY" ||
            this.dateFormat == "MMM-DD-YYYY" ||
            this.dateFormat == "MMM/DD/YYYY"
          ? (this.dateFormat == "MM-DD-YYYY" || this.dateFormat == "MM/DD/YYYY"
              ? getMonth
              : monthList[myDate.getMonth()]) +
            getFormat +
            getDate +
            getFormat +
            getYear
          : this.dateFormat == "DD-MM-YYYY"
          ? getDate + "-" + getMonth + "-" + getYear
          : this.dateFormat == "DD/MMM/YYYY"
          ? getDate + "/" + monthList[myDate.getMonth()] + "/" + getYear
          : "";
      return format;
    }
  }

  dateReConversion(date) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ][myDate.getMonth()];
      str = myDate.getDate() + "-" + month + "-" + myDate.getFullYear();
      return str;
    }
    return "";
  }

  dateReConversionWithDoubleDigitDate(date) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ][myDate.getMonth()];
      str =
        ("0" + myDate.getDate()).slice(-2) +
        "-" +
        month +
        "-" +
        myDate.getFullYear();
      return str;
    }
    return "";
  }

  dateConversion(date, format) {
    if (date !== undefined && date !== "") {
      let str;
      let myDate = new Date(date);
      let month = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ][myDate.getMonth()];
      if (format == "YYYY") {
        str = myDate.getFullYear();
      } else if (format == "Mon-YYYY") {
        str = month + "-" + myDate.getFullYear();
      } else if (format == "YYYYMM") {
        let month =
          (myDate.getMonth() + 1).toString().length == 1
            ? `0${myDate.getMonth() + 1}`
            : myDate.getMonth() + 1;
        str = `${myDate.getFullYear()}${month}`;
      } else {
        str = myDate.getDate() + "-" + month + "-" + myDate.getFullYear();
      }

      return str;
    } else {
      return "";
    }
  }

  // ------------------------------------------------------------
  // USE LANGUAGE
  // ------------------------------------------------------------
  useLang(langCode) {
    this._translate.use(langCode);
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  pattern = {
    number: "^[0-9]*",
    numberWithScaling: "^[0-9.,]*",
    string: "^[a-zA-Z]*",
  };

  // ------------------------------------------------------------
  // Toastr Service
  // -------------------------------------------------------------
  showToastr = {
    success: (msg: any) => {
      this.toastr.success(msg);
    },
    warning: (msg: any) => {
      this.toastr.warning(msg);
    },
    error: (msg: any) => {
      let isPinMsg: boolean = false;
      const modelRef = this.modalService.open(GenericPopupComponent, {
        size: "sm",
        backdrop: false,
        windowClass: "errorPopup",
      });
      modelRef.componentInstance.title = "Error";
      modelRef.componentInstance.message = msg;
      modelRef.componentInstance.popupType = "error";
      modelRef.componentInstance.userConfirmation.subscribe((e) => {
        if (e["type"] == "copy") {
          this.showToastr.success("Error Copied");
        }
        if (e["type"] == "No") {
          modelRef.close();
        }
        if (e == "pin") {
          isPinMsg = !isPinMsg;
          setTimeout(() => {
            if (!isPinMsg) {
              modelRef.close();
            }
          }, 1000);
        }
      });
      setTimeout(() => {
        if (!isPinMsg) {
          modelRef.close();
        }
      }, 5000);
    },
  };

  //------------------------------------------------------------
  // COMMON VALIDATION
  //------------------------------------------------------------
  validation(formControl, fromError, formMsgs) {
    const form = formControl;

    for (const field in fromError) {
      if (isArray(fromError[field])) {
        //VALIDATION FOR DYNAMIC FIELDS
        const tempForm = <FormArray>form.controls[field];
        let tempLength = tempForm.length;

        //CHECK ALL ROWS
        for (let i = 0; i <= tempLength - 1; i++) {
          const errorMsg = fromError[field][i];
          for (const dynamicField in errorMsg) {
            errorMsg[dynamicField] = "";
            const tmpControl = form.controls[field]["controls"];
            const control = tmpControl[i].controls[dynamicField];
            if (control && control.dirty && !control.valid) {
              const messages = formMsgs.validationMessages[field][dynamicField];
              for (const key in control.errors) {
                errorMsg[dynamicField] = messages[key];
              }
            }
          }
        }
      } else {
        //VALIDATION FOR STATIC FIELDS
        fromError[field] = "";
        const control1 = form.get(field);

        if (control1 && control1.dirty && !control1.valid) {
          const messages1 = formMsgs.validationMessages[field];
          for (const key in control1.errors) {
            fromError[field] = messages1[key] + " ";
          }
        }
      }
    }
  }

  // ------------------------------------------------------------
  // COMMON VALIDATION
  // ------------------------------------------------------------
  validation1(formControl, fromError, formMsgs) {
    const form = formControl;
    for (const field in fromError) {
      if (isArray(fromError[field])) {
        const tempForm = <FormArray>form.controls[field];
        let tempLength = tempForm.length;
        for (let i = 0; i <= tempLength - 1; i++) {
          for (const checkField1 in fromError[field][i]) {
            if (checkField1 == "confLevellst") {
              this.validateArray(
                form.controls["dealStagelst"]["controls"][i]["controls"][
                  "confLevellst"
                ],
                i,
                "confLevellst",
                fromError[field][i]["confLevellst"],
                formMsgs.validationMessages[field].confLevellst
              );
            } else {
              const errorMsg = fromError[field][i];
              for (const dynamicField in errorMsg) {
                if (dynamicField != "confLevellst") {
                  errorMsg[dynamicField] = "";
                  const tmpControl = form.controls[field]["controls"];
                  const control = tmpControl[i].controls[dynamicField];
                  if (control && control.dirty && !control.valid) {
                    const messages =
                      formMsgs.validationMessages[field][dynamicField];
                    for (const key in control.errors) {
                      errorMsg[dynamicField] = messages[key];
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        //VALIDATION FOR STATIC FIELDS
        fromError[field] = "";
        const control1 = form.get(field);
        if (control1 && control1.dirty && !control1.valid) {
          const messages1 = formMsgs.validationMessages[field];
          for (const key in control1.errors) {
            fromError[field] = messages1[key] + " ";
          }
        }
      }
    }
  }

  validateArray(tempForm, index, field, fromError, formMsgs) {
    const tempForm1 = <FormArray>tempForm;
    let tempLength = tempForm1.length;
    for (let i = 0; i <= tempLength - 1; i++) {
      const errorMsg = fromError[i];
      for (const dynamicField in errorMsg) {
        errorMsg[dynamicField] = "";
        const tmpControl = tempForm1;
        const control = <FormGroup>(
          tmpControl.controls[i]["controls"][dynamicField]
        );
        if (control && control.dirty && !control.valid) {
          const messages = formMsgs[dynamicField];
          for (const key in control.errors) {
            errorMsg[dynamicField] = messages[key];
          }
        }
      }
    }
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  getLabelValue = (array, checkField1, checkField2, outputValue) => {
    let index;
    index = array.findIndex((element) => element[checkField1] == checkField2);
    return array[index][outputValue];
  };

  // ------------------------------------------------------------
  // CHECK CONTROLS
  // ------------------------------------------------------------
  markAllDirty(control: AbstractControl) {
    if (control.hasOwnProperty("controls")) {
      control.markAsDirty({ onlySelf: true }); // mark group
      let ctrl = <any>control;
      for (let inner in ctrl.controls) {
        this.markAllDirty(ctrl.controls[inner] as AbstractControl);
      }
    } else {
      (<FormControl>control).updateValueAndValidity();
      (<FormControl>control).markAsDirty({ onlySelf: true });
    }
  }

  // ------------------------------------------------------------
  //
  // ------------------------------------------------------------
  convertStringToArray(str) {
    if (str != null) {
      return str.split();
    } else {
      return "";
    }
  }

  // ------------------------------------------------------------
  //  Pagination
  // ------------------------------------------------------------
  activePage: number = 1;
  pageSize: number = 15;

  showLoader = new Subject<boolean>();
  themeNow = new BehaviorSubject("");
  reportSliderTheme = new BehaviorSubject("");
  sidebarDectect = new BehaviorSubject(0);

  // ------------------------------------------------------------
  // SHOW LOADER
  // ------------------------------------------------------------
  show() {
    this.showLoader.next(true);
  }

  // ------------------------------------------------------------
  // HIDE LOADER
  // ------------------------------------------------------------
  hide() {
    this.showLoader.next(false);
  }
  sideBarChangesDectect(data) {
    this.sidebarDectect.next(data);
  }

  getsideBarChangesDectect() {
    return this.sidebarDectect;
  }

  topBarChangesDectect(data) {
    this.topbarSlider.next(data);
  }

  gettopBarChangesDectect(): Observable<any> {
    return this.topbarSlider.asObservable();
  }

  silderChanges(data) {
    if (data != "") {
      this.reportSliderTheme.next(data);
      if (this.router.url == "/main/reports") {
        if (data == "dark") {
          document.body.classList.add("dark");
          document
            .getElementById("report-form")
            .classList.add("dark-validation");
        }
        if (data == "light") {
          document.body.classList.add("light");
          document
            .getElementById("report-form")
            .classList.remove("dark-validation");
        }
      } else {
        document.body.classList.remove("dark");
        document.body.classList.remove("light");
      }
    } else {
      document.body.classList.remove("dark");
      document.body.classList.remove("light");
    }
  }
  themeChanges(data: any) {
    if (data != "") {
      this.themeNow.next(data);
      let dataLower = data.toLowerCase();
      if (this.router.url == "/main/VisionReportSuitedash") {
        if (dataLower == "ds_default")
          document.body.classList.add("ds_default");
        if (dataLower == "ds_gray") document.body.classList.add("ds_gray");
        if (dataLower == "ds_purple") document.body.classList.add("ds_purple");
      } else {
        document.body.classList.remove("ds_default");
        document.body.classList.remove("ds_gray");
        document.body.classList.remove("ds_purple");
      }
    } else {
      document.body.classList.remove("ds_default");
      document.body.classList.remove("ds_gray");
      document.body.classList.remove("ds_purple");
    }
  }

  getReportSliderTheme() {
    return this.reportSliderTheme;
  }
  getThemeChanges() {
    return this.themeNow;
  }

  isNumberKey(evt) {
    const charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  isCharacterOnly(evt) {
    const charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    }
    return false;
  }

  isNumberKeyWithDot(evt) {
    if (evt.keyCode === 46 && evt.target.value.split(".").length === 2) {
      return false;
    }

    const val = evt.target.value.split(".");
    if (val && val[1] && val[1].length) {
      if (val[1].length === 4) {
        return false;
      }
    }

    const charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode == 110 || charCode == 190 || charCode == 46) {
      return true;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  isNumberKeyWithDott(evt, digit, digit1) {
    // const decimalPatternMatch = '^[0-9]{1,4}(\.[0-9][0-9])?$';

    const charCode = evt.which ? evt.which : evt.keyCode;
    if (evt.keyCode === 46 && evt.target.value.split(".").length === 2) {
      return false;
    }

    const val = evt.target.value.split(".");

    // const regexpTest =  /^[0-9]{10}\.[0-9]{0, 2}?$/;
    const regexpTest = /^\d{0,7}(,\d{0,3})?(\.\d{1,4})?$/;
    this.validNumber = regexpTest.test(evt.target.value);

    if (val && val[0] && val[0].length) {
      if (val[0].length > digit) {
        return false;
      }
      if (val[0].length === digit) {
        if (charCode == 110 || charCode == 190 || charCode == 46) {
          return true;
        } else {
          return false;
        }
      }
    }

    if (val && val[1] && val[1].length) {
      if (val[1].length === digit1) {
        return false;
      }
    }

    if (charCode == 110 || charCode == 190 || charCode == 46) {
      return true;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }

    return true;
  }

  removeComma(event) {
    const removeComma = event.target.value.replace(/,/g, "");
    event.target.value = removeComma;
  }
  addComma(event) {
    const num = event.target.value;
    const numParts = num.toString().split(".");
    numParts[0] = numParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    // return numParts.join('.');
    event.target.value = numParts.join(".");
  }

  checkAccess(data) {
    if (data == "edit") {
      if (this.pageLevelAccess.profileModify == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data == "copy" || data == "add") {
      if (this.pageLevelAccess.profileAdd == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (
      data == "view" ||
      data == "childrenView" ||
      data == "acountOfficerView"
    ) {
      if (this.pageLevelAccess.profileView == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data == "delete") {
      if (this.pageLevelAccess.profileDelete == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data == "download") {
      if (this.pageLevelAccess.profileDownload == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data == "upload") {
      if (this.pageLevelAccess.profileUpload == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (data == "approve" || data == "reject") {
      if (this.pageLevelAccess.profileVerification == "Y") {
        return true;
      } else {
        return false;
      }
    }

    if (
      data == "update" ||
      data == "dealstage" ||
      data == "dealcalls" ||
      data == "confidentlevel" ||
      data == "statusupdate"
    ) {
      if (this.pageLevelAccess.profileModify == "Y") {
        return true;
      } else {
        return false;
      }
    }

    return true;
  }
}
