import { Injectable, Injector } from "@angular/core";
import { HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { environment } from 'src/environments/environment';

// CUSTOM IMPORTS
import { EnvService } from '../env.service';
import { LoaderService } from "./loader.service";
import { JwtService } from "./jwt.service";

@Injectable()

export class Interceptor implements HttpInterceptor {


  private baseUrl: string = "";
  private requests: HttpRequest<any>[] = [];
  loaderService = this.injector.get(LoaderService);

  // ------------------------------------------------------------
  // CONSTRUCTOR
  // ------------------------------------------------------------
  constructor(
    private jwtService:JwtService,
    private injector: Injector,
    private env: EnvService) {
    this.baseUrl = env.apiUrl
  }


  // ------------------------------------------------------------
  // INTERCEPT METHOD
  // ------------------------------------------------------------
  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i != -1) { this.requests.splice(i, 1); }
    if (this.requests.length - 1 < 0) {
      this.loaderService.hide();
    }
  }


  // ------------------------------------------------------------
  // INTERCEPT METHOD
  // ------------------------------------------------------------
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderService.show();
    if (req.url == `${this.baseUrl}${environment.updateNotification}` ||
      req.url == `${this.baseUrl}${environment.getNotification}` || 
      ( req.url == `${this.baseUrl}${environment.getRMReport}` && this.jwtService.get('reportTemplate') == 'MULTIPLE') || 
      ( (req.url == `${this.baseUrl}${environment.getRMReport}` || req.url == `${this.baseUrl}${environment.getInterActive}`) && this.jwtService.get('reportType') == 'I') || 
      req.url == `${this.baseUrl}${environment.getTileData}` || req.url == `${this.baseUrl}${environment.getDrillDownData}`) {
      this.loaderService.hide();
    } else {
      this.requests.push(req);
    }
    return Observable.create(observer => {
      const subscription = next.handle(req)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              this.removeRequest(req);
              observer.next(event);
            }
          },
          err => { this.removeRequest(req); observer.error(err); },
          () => { this.removeRequest(req); observer.complete(); });
      return () => {
        this.removeRequest(req);
        subscription.unsubscribe();
      };
    });
  }
}

