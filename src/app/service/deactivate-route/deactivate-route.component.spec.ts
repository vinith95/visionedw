import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeactivateRouteComponent } from './deactivate-route.component';

describe('DeactivateRouteComponent', () => {
  let component: DeactivateRouteComponent;
  let fixture: ComponentFixture<DeactivateRouteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeactivateRouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeactivateRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
