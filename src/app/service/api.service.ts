import { Injectable,Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, retry, mergeMap, map } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
// CUSTOM IMPORT
import { JwtService } from './jwt.service';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { EnvService } from '../env.service';
import { GlobalService } from './global.service';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  // ------------------------------------------------------------
  // PUBLIC VARIABLES AND DECLARATIONS
  // ------------------------------------------------------------
  private baseUrl: string = "";
  private header: any;
  public private_key: string = environment.private_key;
  public public_key: string = environment.public_key;
  public groupPageTitle: string;


  // ------------------------------------------------------------
  // CONSTRUCTOR
  // ------------------------------------------------------------
  constructor(
    private http: HttpClient,
    private jwtService: JwtService,
    private router: Router,
    private env: EnvService,
    private globalService: GlobalService,
    @Inject(DOCUMENT) public document: any,
  ) {
    this.baseUrl = env.apiUrl
  }

  // ------------------------------------------------------------
  // GET ENVIRONMENT
  // ------------------------------------------------------------
  public getEnvironment(): any {
    return environment;
  }


  // ------------------------------------------------------------
  // SET CONTENT TYPE
  // ------------------------------------------------------------
  private setContentType() {
    this.header = {
      'content-type': 'application/json',
      // 'Cache-Control': 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
      // 'Pragma': 'no-cache',
      // 'Expires': '0'
    }
  }


  // ------------------------------------------------------------
  // SET FILE CONTENT TYPE
  // ------------------------------------------------------------
  private setFileContentType() {
    this.header = {
      reportProgress: true,
      responseType: 'text'
    }
  }


  // ------------------------------------------------------------
  // SET TEMP TOKEN
  // ------------------------------------------------------------
  private setTemproryToken() {
    const temprory_token: string = this.jwtService.get('temporary-token');
    if (temprory_token) {
      this.header['temporary-token'] = temprory_token;
    }
  }


  // ------------------------------------------------------------
  // SET AUTH TOKEN
  // ------------------------------------------------------------
  private setAuthToken() {
    const token: string = this.jwtService.get('VisionAuthenticate');
    if (token) {
      this.header['VisionAuthenticate'] = token;
    }
  }


  // ------------------------------------------------------------
  // HEADERS
  // ------------------------------------------------------------
  private get headers(): any {
    this.setContentType();
    this.setTemproryToken();
    this.setAuthToken();
    return this.header;
  }


  // ------------------------------------------------------------
  // FILE HEADERS
  // ------------------------------------------------------------
  private get fileHeaders(): any {
    this.setFileContentType();
    this.setTemproryToken();
    this.setAuthToken();
    return this.header;
  }


  // ------------------------------------------------------------
  // GET METHOD
  // ------------------------------------------------------------
  public get(relativeUrl: string): Observable<any> {
    return this.http.get(this.baseUrl + relativeUrl, { headers: new HttpHeaders(this.headers) }).pipe(
      catchError(this.handleError)
    )
  }


  // ------------------------------------------------------------
  // POST METHOD
  // ------------------------------------------------------------
  public post(relativeUrl: string, data: any) {
    return this.http.post(this.baseUrl + relativeUrl, data,
      { headers: new HttpHeaders(this.headers) }).pipe(
        catchError(this.handleError)
      );

  }


  // ------------------------------------------------------------
  // PUT METHOD
  // ------------------------------------------------------------
  public put(relativeUrl: string, data: any) {
    return this.http.put(this.baseUrl + relativeUrl, data,
      { headers: new HttpHeaders(this.headers) })
      .pipe(

        mergeMap((data: Response) => {
          return of(data && data['success'] === true)
        }
        ), catchError(this.handleError));
  }


  // ------------------------------------------------------------
  // DELETE METHOD
  // ------------------------------------------------------------
  public delete(relativeUrl: any, data: any) {
    const options = {
      headers: new HttpHeaders(this.headers),
      body: data
    };
    return this.http.delete(this.baseUrl + relativeUrl, options)
      .pipe(
        mergeMap((data: Response) => {
          return of(data)
        }), catchError(this.handleError));
  }


  // ------------------------------------------------------------
  // FILE UPLOAD METHOD
  // ------------------------------------------------------------
  public fileUploads(relativeUrl: string, data: any) {
    return this.http.post(this.baseUrl + relativeUrl, data,
      { headers: new HttpHeaders(this.fileHeaders) }).pipe(catchError(this.handleError));

  }

  // ------------------------------------------------------------
  // FILE UPLOAD METHOD
  // ------------------------------------------------------------
  public fileDownloads(relativeUrl: string, data: any) {
    return this.http.post(this.baseUrl + relativeUrl, data,
      { responseType: 'blob', headers: new HttpHeaders(this.fileHeaders) }).pipe(catchError(this.handleError));

  }

  private headerParams = new HttpParams();

  // MPR report excel download
  public fileDownloadwithParam(relativeUrl: string, data: any) {
    this.headerParams = new HttpParams().set('groupReportTitle', localStorage.getItem('pageTitle'));
    return this.http.post(this.baseUrl + relativeUrl, data,
      { responseType: 'blob', headers: new HttpHeaders(this.fileHeaders), params: this.headerParams }).pipe(catchError(this.handleError));

  }


  // ------------------------------------------------------------
  // HANDLE ERROR
  // ------------------------------------------------------------
  // private handleError = (error: any) => {
  //   let errorMessage = '';
  //   if (error.error instanceof ErrorEvent) {
  //     // client-side error
  //     errorMessage = `Error: ${error.error.message}`;
  //   } else {
  //     // server-side error
  //     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  //   }
  //   return throwError(errorMessage);
  // }
  logout() {

    this.jwtService.unset('VisionAuthenticate');
    this.jwtService.unset('User_Name');
    this.jwtService.unset('Dashboard_Id');
    this.jwtService.unset('lg_Dashboard_Id');
    this.jwtService.unset('menu_hierarchy');
    this.jwtService.unset('businessDate');
    this.jwtService.unset('reportTemplate');
    this.jwtService.unset('reportType');
    this.jwtService.unset('enableWidgets');
    localStorage.setItem('pageTitle', '');
    this.router.navigate(['/']);
    this.globalService.pageLevelAccess = '';
    this.globalService.accordinValue = '';
    this.globalService.accordinSubValue = '';
    this.globalService.selectedReport = {};
    this.globalService.accordinValueDash = '';
    this.globalService.accordinSubValueDash = '';
    this.globalService.selectedDashboard = {};
    this.globalService.themeChanges("")
    this.globalService.silderChanges("")
    this.closeFullscreen();
    document.body.classList.add('sidebar-mini');
  }

  closeFullscreen() {
    if (window.innerHeight == screen.height) {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        this.document.msExitFullscreen();
      }
    }
  }
  private handleError = (error: any) => {
    let errorMessage = '';
    if (error.status == 0) {
      this.globalService.showToastr.error('API Service is down.Contact System Admin');
      errorMessage = `API Service is down.Contact System Admin`;
      this.logout();
    }
    else if (error.status == 500 && error.error.path == '/VisionEdwAdmin/authenticate') {
      errorMessage = error.error.message;
    }
    else {
      if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
    }
    return throwError(errorMessage);
  }

  // Get country list api
  getCountryList() {
    const data = {
      lastIndex: 1000,
      query: 'M0005',
      startIndex: 1
    };

    return this.http.post(this.baseUrl + environment.get_Branch_List, data,
      { headers: new HttpHeaders(this.headers) }).pipe(
        catchError(this.handleError)
      );
  }

  // Get SBU list api
  getSBUList() {
    const data = {
      lastIndex: 1000,
      query: 'SBU',
      startIndex: 1
    };

    return this.http.post(this.baseUrl + environment.get_Branch_List, data,
      { headers: new HttpHeaders(this.headers) }).pipe(
        catchError(this.handleError)
      );
  }

}
