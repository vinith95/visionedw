import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';
import * as query from '../../assets/data/queryDetail.json';

@Injectable({
  providedIn: 'root'
})
export class OnClickService {
  queryIdDetail:any=query['default'];
  constructor(private _apiService: ApiService,) { }
  onClickDropDown(field,pageName, rawValues, rowValue){
    let data={};
    data['filterSourceId']=this.queryIdDetail[pageName][field]["queryId"];
    let vFormParam=this.queryIdDetail[pageName][field]["vFormParam"];
    let vFormArrayParam=this.queryIdDetail[pageName][field]["vFormArrayParam"];
    if(vFormParam.length || vFormArrayParam.length){
      let filter1Val=[]
      if(vFormParam.length){
        vFormParam.forEach(element => {
          filter1Val.push(rawValues[element]);
        });
      }
      if(vFormArrayParam.length){
        vFormArrayParam.forEach(element => {
          filter1Val.push(rowValue[element]);
        });
      }
      data['filter1Val']=`'${filter1Val.join('-')}'`
    }
    let promptArr = [];
    this._apiService.post(environment.getReportChildDependentFilter, data).subscribe((resp) => {
      Object.keys(resp['response']).forEach((element, index1) => {
        promptArr.push({alphaSubTab: element,description: resp['response'][element]})
      });
      return promptArr;
    });
    return promptArr;
  }
  checkRequirement(field,pageName,fullFormValue,arrayValue) {
    let flag = 0;
    let vFormParam=this.queryIdDetail[pageName][field]["vFormParam"];
    let vFormArrayParam=this.queryIdDetail[pageName][field]["vFormArrayParam"]
    if(vFormParam.length){
      vFormParam.forEach(element => {
        if (fullFormValue[element] == '')
          flag++;
      });
    }
    if(vFormArrayParam.length){
        vFormArrayParam.forEach(element => {
        if (arrayValue[element] == '')
          flag++;
      });
    }
    return !flag ? true : false;
  }
  onClickHit(field,pageName, rawValues, rowValue){
    let data={};
    data['filterSourceId']=this.queryIdDetail[pageName][field]["queryId"];
    
    let vFormParam=this.queryIdDetail[pageName][field]["vFormParam"];
    let vFormArrayParam=this.queryIdDetail[pageName][field]["vFormArrayParam"];
    if(vFormParam.length || vFormArrayParam.length){
      let filter1Val=[]
      if(vFormParam.length){
        vFormParam.forEach(element => {
          filter1Val.push(rawValues[element]);
        });
      }
      if(vFormArrayParam.length){
        vFormArrayParam.forEach(element => {
          filter1Val.push(rowValue[element]);
        });
      }
      data['filter1Val']=`'${filter1Val.join('-')}'`
    }
    return this._apiService.post(environment.getReportChildDependentFilter, data);
  }
  onClickHitNew(field,pageName, rawValues, rowValue){
    let data={};
    data['filterSourceId']=this.queryIdDetail[pageName][field]["queryId"];
    let vFormParam=this.queryIdDetail[pageName][field]["vFormParam"];
    let vFormArrayParam=this.queryIdDetail[pageName][field]["vFormArrayParam"];
    if(vFormParam.length || vFormArrayParam.length){
      let filter1Val=[]
      if(vFormParam.length){
        vFormParam.forEach(element => {
          filter1Val.push(rawValues[element]);
        });
      }
      if(vFormArrayParam.length){
        vFormArrayParam.forEach(element => {
          filter1Val.push(rowValue[element]);
        
          
        });
      }
      filter1Val.forEach((element1,index1) => {
        data[`filter${index1+1}Val`]=`'${element1}'`;
      });
      //console.log(data);
    }
    return this._apiService.post(environment.getReportChildDependentFilter, data);
  }

  editHitNew(field,pageName, rawValues, rowValue){
    let data={};
    data['filterSourceId']=this.queryIdDetail[pageName][field]["queryId"];
    let vFormParam=this.queryIdDetail[pageName][field]["vFormParam"];
    let vFormArrayParam=this.queryIdDetail[pageName][field]["vFormArrayParam"];
    if(vFormParam.length || vFormArrayParam.length){
      let filter1Val=[]
      if(vFormParam.length){
        vFormParam.forEach(element => {
          filter1Val.push(rawValues[element]);
        });
      }
      if(vFormArrayParam.length){
        vFormArrayParam.forEach(element => {
          filter1Val.push(rowValue[element]);
        });
      }
      filter1Val.forEach((element1,index1) => {
        data[`filter${index1+1}Val`]=`'${element1}'`;
      });
    }
    let promptArr = [];
    this._apiService.post(environment.getReportChildDependentFilter, data).subscribe((resp) => {
      Object.keys(resp['response']).forEach((element, index1) => {
       // promptArr.push({alphaSubTab: element,description: resp['response'][element]})
        promptArr.push({ alphaSubTab: element.split('@')[1], description: resp['response'][element] })
      });
      return promptArr;
    });
    return promptArr;
    //return this._apiService.post(environment.getReportChildDependentFilter, data);
  }

}


