import { TestBed } from '@angular/core/testing';

import { OnClickService } from './on-click.service';

describe('OnClickService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OnClickService = TestBed.get(OnClickService);
    expect(service).toBeTruthy();
  });
});
